﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace gManager
{
    public class objetosResponse
    {

        public enum AccionTipo { objetos, destinatarios, movimientos, usuarios, objetosmovidos }
        Hashtable _param = new Hashtable();

        public EventHandler Terminado {get;set;}

        public objetosResponse(string server, string acc)
        {

            Server = server;
            Accion = acc;
            Verb = "GET";
            Content = "text/json";

        }
       
             
        public void execute(System.ComponentModel.BackgroundWorker worker)
        {
            bool cont = true;
            bool multipages = false;
            worker.ReportProgress(1);
            Result = "";
            while (cont)
            {
                Error = null;
                CreateRequest();
               
                try
                {
                    HttpResponse = (HttpWebResponse)HttpRequest.GetResponse();
                    if (HttpResponse != null)
                        using (var streamReader = new StreamReader(HttpResponse.GetResponseStream()))
                            Result += streamReader.ReadToEnd();
                }
                catch (WebException error)
                {
                    Error = error;
                    cont = false;
                    HttpResponse = (HttpWebResponse)error.Response;
                    if (HttpResponse == null)
                    {
                       
                       
                    }
                    else
                    {
                        using (var streamReader = new StreamReader(error.Response.GetResponseStream()))
                            Result = streamReader.ReadToEnd();
                        if (HttpResponse.StatusCode == HttpStatusCode.OK
                            || HttpResponse.StatusCode == HttpStatusCode.Unauthorized
                            || HttpResponse.StatusCode == HttpStatusCode.Forbidden)
                           
                            reportError(Result);
                        else
                        {
                            Error = new Exception(HttpResponse.StatusDescription);
                        }
                    }


                }
                worker.ReportProgress(80);
                if (HttpResponse != null) iniciarPaginacion();

                if (TotalPages > 0 && CurrentPage < TotalPages)
                {
                    addParametros("page", (CurrentPage + 1).ToString());
                    if (worker != null)
                    {
                        worker.ReportProgress((CurrentPage * 100 / TotalPages) - 1);
                    }
                    multipages = true;
                    Result += ",";//hacemos lista de listas
                    cont = true;
                }
                else
                {
                    cont = false;
                }
            }
            if(multipages)
            {
                Result = "[" + Result + "]";
            }
            worker.ReportProgress(100);
        }
        public void executePost(System.ComponentModel.BackgroundWorker worker)
        {
            bool cont = ListaObjetosParaEnviar!=null && ListaObjetosParaEnviar.Count>0;
            int i = 0;
            bool delete = Verb == "DELETE";
            Result = "";
            while (cont)
            {
                Error = null;
                ObjetoParaEnviar = ListaObjetosParaEnviar[i];
                
                CreateRequest();
                
               
                try
                {
                    HttpResponse = (HttpWebResponse)HttpRequest.GetResponse();
                    if (HttpResponse != null)
                        using (var streamReader = new StreamReader(HttpResponse.GetResponseStream()))
                            Result += streamReader.ReadToEnd();
                    //imagen
                   /* if (!delete && ObjetoParaEnviar != null && ObjetoParaEnviar is gcObjeto && ImageManager.HasImage(ObjetoParaEnviar))
                    {
                        gcObjeto o = ObjetoParaEnviar as gcObjeto;
                        // o.Imagen64 = ImageManager.getStringImage(o);
                        NameValueCollection nvc = new NameValueCollection();
                        nvc.Add("producto", o.Codigo);
                        WebManager.Singlenton.HttpUploadFile(Server + Accion, ImageManager.getPath(ObjetoParaEnviar), "file", "image/png", nvc);
                    }*/
                }
                catch (WebException error)
                {
                    Error = error;
                    cont = false;
                    HttpResponse = (HttpWebResponse)error.Response;
                    if (HttpResponse == null)
                    {


                    }
                    else
                    {
                        using (var streamReader = new StreamReader(error.Response.GetResponseStream()))
                            Result = streamReader.ReadToEnd();
                        if (HttpResponse.StatusCode == HttpStatusCode.OK
                            || HttpResponse.StatusCode == HttpStatusCode.Unauthorized
                            || HttpResponse.StatusCode == HttpStatusCode.Forbidden)

                            reportError(Result);
                        else
                        {
                            Error = new Exception(HttpResponse.StatusDescription);
                        }
                    }


                }
                if (HttpResponse != null) iniciarPaginacion();
                if(delete)
                    worker.ReportProgress(99-(i * 100 / ListaObjetosParaEnviar.Count) );
                else
                    worker.ReportProgress((i * 100 / ListaObjetosParaEnviar.Count) - 1);
               
                i++;
                cont = i < ListaObjetosParaEnviar.Count;
            }
           
            worker.ReportProgress(100);
        }
       
       
        public void execute(object obj = null, string titulo = "Sincronizando con internet")
        {
            if(obj is  IList)
            {

                ListaObjetosParaEnviar = obj as IList;
                gLogger.TaskManager.CrearTareaAislada(executePost, terminado, (percent, desc) => CoreManager.Singlenton.InformarProgreso(percent, desc), titulo);
                
            }
            else
            {
                ObjetoParaEnviar = obj;
                gLogger.TaskManager.CrearTareaAislada(execute, terminado, (percent, desc) => CoreManager.Singlenton.InformarProgreso(percent, desc), titulo);

               
            }
            


        }
        private void terminado(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {

                if (Terminado != null) Terminado(e.Error, null);
          
        }
        private void reportError(string r)
        {
            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(r);
            if (values.ContainsKey("message"))
                Error = new Exception(values["message"]);
        }
        public bool executeNext()
        {
            if (CurrentPage == 0 || CurrentPage < TotalPages)
            {
                addParametros("page", (CurrentPage + 1).ToString());
                execute();
                return (HttpResponse.StatusCode == HttpStatusCode.OK);
            }


            return false;
        }
        internal void CreateRequest()
        {
            HttpRequest = (HttpWebRequest)WebRequest.Create(URL);
            HttpRequest.ContentType = Content;
            HttpRequest.Method = Verb;
            HttpRequest.Accept = Accept;
            HttpRequest.CookieContainer = new CookieContainer();
            
            if (Credentials != null)
            {

                HttpRequest.Headers.Add(WebManager.Singlenton.EncodeCredentials(Credentials));
            }
           
            WriteStream(ObjetoParaEnviar);

           
        }
        
        public Credentials Credentials { get; set; }
        public HttpWebRequest HttpRequest { get; internal set; }
        public HttpWebResponse HttpResponse { get; internal set; }
        internal void WriteStream(object obj)
        {
            if (obj != null)
            {
                using (var streamWriter = new StreamWriter(HttpRequest.GetRequestStream()))
                {
                    if (obj is string || obj is System.Drawing.Bitmap)
                    {
                        streamWriter.Write(obj);
                       
                    }
                    else
                    {
                        string v = JsonConvert.SerializeObject(obj);
                        streamWriter.Write(v);
                    }
                      
                }
               
            }
        }
        public object ObjetoParaEnviar { get; set; }
        public IList ListaObjetosParaEnviar { get; set; }
        public String Verb { get; set; }
        public String Accept { get; set; }
        public String Content { get; set; }
        public String Accion { get; set; }
        public String Server { get; set; }
        public string URL { get { return Server + Accion + Parametros; } }
        public string Parametros
        {
            get
            {
                string ret = "";
                if (_param.Count > 0) ret = "?";
                foreach (string r in _param.Keys)
                {
                    ret += r + "=" + _param[r] + "&";

                }
                return ret;
            }
        }
        public void addParametros(string clave, string valor)
        {
            if (_param.ContainsKey(clave))
                _param.Remove(clave);
            _param.Add(clave, valor);
        }
        public void removeParametro(string clave)
        {
            if (_param.ContainsKey(clave))
                _param.Remove(clave);
        }
        public void ClearParametros()
        {
            _param.Clear();
        }
        public string Result { get; internal set; }


        void iniciarPaginacion()
        {

            string val = HttpResponse.Headers["X-Pagination-Current-Page"];
            int num;
            int.TryParse(val, out num);
            CurrentPage = num;
            //total de paginas
            val = HttpResponse.Headers["X-Pagination-Page-Count"];
            int.TryParse(val, out num);
            TotalPages = num;
            //Registros
            val = HttpResponse.Headers["X-Pagination-Per-Page"];
            int.TryParse(val, out num);
            RegistrosPerPage = num;
            //Total Registros
            val = HttpResponse.Headers["X-Pagination-Total-Count"];
            int.TryParse(val, out num);
            TotalRegistros = num;
            Link = HttpResponse.Headers["Link"];
        }
        public string Link { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public int RegistrosPerPage { get; set; }
        public int TotalRegistros { get; set; }
        public object Response { get; set; }
        public System.Net.HttpStatusCode Estado
        {
            get
            {
                if (HttpResponse != null)
                {
                    return HttpResponse.StatusCode;
                }
                return HttpStatusCode.NotFound;
            }
        }
        public bool OK
        {
            get
            {
                return Estado == HttpStatusCode.OK;
            }
        }
        public Exception Error { get; set; }
       

    }
    public class Credentials
    {
        public Credentials(keyObject k)
        {
            UserName = k.Usuario;
            Password = k.Password;
            Token = k.Token;
            ServerToken = k.ServerToken;
        }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string ServerToken { get; set; }
    }
}
