﻿using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gManager
{
    public class LectorExcel
    {//https://www.microsoft.com/es-es/download/details.aspx?id=13255
        public List<ProductoUpdate> ToEntidadHojaExcelList(string pathDelFicheroExcel)
        {
           
            try
            {
                var book = new ExcelQueryFactory(pathDelFicheroExcel);
                var resultado = (from row in book.Worksheet<ProductoUpdate>("prods")
                                 select row).ToList();

                book.Dispose();
                if(resultado.Count > 0)
                {
                    return resultado;
                }
            }catch(Exception e){
                gLogger.logger.Singlenton.addErrorMsg(e.Message);
            }

            return new List<ProductoUpdate>() ;
        }
    }
}
