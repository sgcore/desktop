﻿using gLogger;
using System.Collections.Generic;
using System.Data;
using gConnector;
using System.Collections;
using System.Linq;
namespace gManager
{
    internal class UsuarioManager
    {
        private static UsuarioManager _instance;
        private System.Collections.Hashtable _usuarios = new System.Collections.Hashtable();
        public delegate void UsuarioDelegate(gcUsuario u);
            
        private UsuarioManager() 
        { 
            
        }
        public bool PermisoUsuario(gManager.gcUsuario.PermisoEnum tipo,gcUsuario user = null, bool msgbox = false)
        {
            if (user == null) user = UsuarioActual;
            if(user == null && tipo!=gcUsuario.PermisoEnum.Visitante)
            {
                if (msgbox) logger.Singlenton.addWarningMsg("No hay una sesión iniciada");
                return false;
            }
            if (user != null && user.Permiso < tipo)
            {
                if (msgbox) logger.Singlenton.addWarningMsg("No tiene los permisos nesesarios.");
                return false;
            }


            return true;
            
        }
        public static UsuarioManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new UsuarioManager();
                return _instance;
            }
        }
       
        public void reload(System.ComponentModel.BackgroundWorker worker, DataTable dat, ref int cont, int totalcount)
        {
            _usuarios.Clear();
            foreach (DataRow row in dat.Rows)
            {
                gcUsuario u=new gcUsuario(row);

                _usuarios.Add(u.Id,u);
                //prograss
                cont++;
                if (worker != null)
                {
                    worker.ReportProgress((cont * 100 / totalcount) - 1);
                }
               
            }
             
        }
        public gcUsuario GuardarUsuarioEnDb(gcUsuario u)
        {
            Hashtable t = new Hashtable();
            t.Add("id", u.Id);
            t.Add ("nombre",u.Nombre);
            t.Add("descripcion", u.Descripcion);
            t.Add("pass", u.Pass);
            t.Add("usuario", u.User);
            t.Add("permiso",(int) u.Permiso);
            t.Add("central", u.CentralId);
            t.Add("responsable", u.ResponsableId);
            u.Id = Connector.Singlenton.ConexionActiva.GuardarENtity(t, "gcusuarios",u.Id>0);
            return u;
           
        }
        public bool login()
        {
            if (UsuarioActual != null) return true;
            if (!Connector.Singlenton.EstaConectado)
                return false ;
            Connector.Singlenton.RequiereRelog = false;
            DataRow r = Connector.Singlenton.ConexionActiva.login();
            if (r != null)
            {
                _usuarioActual = new gcUsuario(r);
                return true;
            }
            if (Connector.Singlenton.RequiereRelog)
                return login();
            return false;
        }
        public void logout()
        {
            _usuarioActual = null;
        }
        gcUsuario _usuarioActual = null;
        public gcUsuario UsuarioActual
        {
            get
            {
                 return _usuarioActual;
            }
        }
        public gcUsuario getUsuario(int id)
        {
            return (gcUsuario)_usuarios[id];
        }
        public List<gcUsuario> getUsuarios()
        {
            return _usuarios.Values.OfType<gcUsuario>().ToList<gcUsuario>();
        }
        public bool Logueado
        {
            get
            {
                return UsuarioActual != null;
            }
        }

    }
}
