﻿using gLogger;
using System;
using System.Management;
using System.Text;
using System.Reflection;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;
using System.IO.Compression;
using System.Drawing;

namespace gManager
{
    public static class Utils
    {
        public static decimal ActualizarDolar(string url1 = "http://www.netcomsatelital.com.ar/dolar.xml")
            {
                decimal pd = 0;
                string xmlResult = null;
                string url;
                ////http://www.webservicex.net/CurrencyConvertor.asmx
                ///ciber friend view-source:http://www.ciberfriends.com.ar/cotizacion.php?xml
                //Dim result = client.ConversionRate(Currency.EUR, Currency.USD)

                url = url1;
                //EUR A MXN
                //USD A MXN
                try
                {
                    System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                    request.Timeout = 5000;
                    System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
                    System.IO.StreamReader resStream = new System.IO.StreamReader(response.GetResponseStream());
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    xmlResult = resStream.ReadToEnd();
                    doc.LoadXml(xmlResult);
                    pd = decimal.Parse(((doc.ChildNodes[0].ChildNodes.Item(1))).LastChild.Value.Replace(".", ","), System.Globalization.CultureInfo.InstalledUICulture);
                }
                catch
                {

                    logger.Singlenton.addErrorMsg("No se pudo obtener la cotización desde:\n" + url);
                }
                return pd;
            }
        public static gcDestinatario.DestinatarioTipo TipoDestinatarioSegunMovimiento(gcMovimiento.TipoMovEnum t)
            {
                switch (t)
                {
                    case gcMovimiento.TipoMovEnum.Compra:
                    case gcMovimiento.TipoMovEnum.Retorno:
                    case gcMovimiento.TipoMovEnum.Pedido:
                        return gcDestinatario.DestinatarioTipo.Proveedor;
                    case gcMovimiento.TipoMovEnum.Venta:
                    case gcMovimiento.TipoMovEnum.Devolucion:
                    case gcMovimiento.TipoMovEnum.Presupuesto:
                        return gcDestinatario.DestinatarioTipo.Cliente;
                    case gcMovimiento.TipoMovEnum.Entrada:
                    case gcMovimiento.TipoMovEnum.Salida:
                        return gcDestinatario.DestinatarioTipo.Sucursal;
                    default:
                        return gcDestinatario.DestinatarioTipo.Responsable;

                }
            }



        public static string UnidadMedida(gcObjeto.ObjetoMedida med, decimal count)
        {
            
            switch (med)
            {
                case gcObjeto.ObjetoMedida.Kilo:
                    string metrica = "kg";
                    decimal cant = count;
                    if (cant < 1)
                    {
                        cant = cant * 1000;
                        metrica = "g";
                    }
                    if (cant < 1)
                    {
                        cant = cant * 1000;
                        metrica = "mg";
                    }
                    if(count < 0)
                    {
                        cant = count;
                        metrica = "kg";
                    }
                    return cant.ToString("0.## " + metrica);

                case gcObjeto.ObjetoMedida.Litro:
                    string metrica1 = "lts";
                    decimal cant1 = count;
                    if (cant1 < 1)
                    {
                        cant1 = cant1 * 1000;
                        metrica1 = "ml";
                    }
                    if (count < 0)
                    {
                        cant1 = count;
                        metrica1 = "lts";
                    }
                    return cant1.ToString("0.## " + metrica1);
                case gcObjeto.ObjetoMedida.Metro:
                    string metrica2 = "mts";
                    decimal cant2 = count;
                    if (cant2 < 1)
                    {
                        cant2 = cant2 * 100;
                        metrica2 = "cm";
                    }
                    if (cant2 < 1)
                    {
                        cant2 = cant2 * 10;
                        metrica2 = "mm";
                    }
                    if (count < 0)
                    {
                        cant2 = count;
                        metrica2 = "mts";
                    }
                    return cant2.ToString("0.## " + metrica2);
                case gcObjeto.ObjetoMedida.Minuto:
                    string metrica3 = "min";
                    decimal cant3 = count;
                    if (cant3 < 1)
                    {
                        cant3 = cant3 * 60;
                        metrica3 = "seg";
                    }
                    if (count < -1)
                    {
                        cant3 = count;
                        metrica3 = "min";
                    }
                    return cant3.ToString("0.## " + metrica3);
                default:
                    return count.ToString("0.## u");

            }
        }
        public static string ImageToHtml(Bitmap bitmap, int width = 16, int height = 16)
        {
            // Convert the image to byte[]
            if (bitmap == null)
            {
                return "";
            }
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
            byte[] imageBytes = stream.ToArray();
            string base64String = Convert.ToBase64String(imageBytes);

            return ("<img class=\"image\" width=" + width.ToString() + " height=" + height.ToString() + " src=\"data:image/png;base64," + base64String + "\" />");
        }

        #region GENERAL
        /// <summary>
        /// Obtiene el directorio de la aplicacion.
        /// </summary>
        /// <returns></returns>
        public static string GetProgramDirectory()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            return Path.GetDirectoryName(assembly.Location);
        }
        public static string Key
        {
            get
            {
                return CpuInfo() + "@" + Environment.MachineName; ///HddInfo()+CpuInfo();
            }
        }
        public static string HddInfo()
        {

            string HDD = System.Environment.CurrentDirectory.Substring(0, 1);
            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + HDD + ":\"");
            disk.Get();
            return disk["VolumeSerialNumber"].ToString();

        }
        public static string CpuInfo()
        {
            string cpuInfo = String.Empty;
            ManagementClass managementClass =
                    new ManagementClass("Win32_Processor");
            ManagementObjectCollection managementObjCol =
                    managementClass.GetInstances();

            foreach (ManagementObject managementObj in managementObjCol)
            {
                if (cpuInfo == String.Empty)
                {
                    cpuInfo =
                    managementObj.Properties["ProcessorId"].Value.ToString();
                }
            }
            return cpuInfo;
        }
      
       

        #endregion



        /// <summary>
        /// Cambia la fecha para guardarla en mysql
        /// </summary>
        /// <param name="d">Datetime</param>
        /// <returns>Fecha con formato YYYY-MM-DD HH:mm:ss</returns>
        public static string fechaparaSQLCE(DateTime d)
        {
            string t = "'";
            t += d.Year.ToString();
            t += "-";
            t += d.Month.ToString();
            t += "-";
            t += d.Day.ToString();
            t += " ";
            t += d.Hour.ToString();
            t += ":";
            t += d.Minute.ToString();
            t += ":";
            t += d.Second.ToString();
            t += "'";
            return t;
        }
        
        public static string randomGUID()
        {
            return Guid.NewGuid().ToString("N");
        }

        /// <summary>
        /// Muesta la fecha y hora con formato amigable
        /// </summary>
        /// <param name="f">el objeto de fecha a formatear</param>
        /// <returns>Cadena formateada</returns>
        public static string MostrarHora(DateTime f)
        {
            DateTime now = DateTime.Now;
            TimeSpan p = now.Subtract(f);
            if (p.TotalMinutes < 1) return "Hace menos de un minuto";
            else if (p.TotalMinutes < 60) return "Hace " + p.Minutes + " minutos";
            else if (p.TotalHours < 5) return "Hace " + p.Hours + " horas";
            else if (p.TotalDays < now.TimeOfDay.TotalDays) return "Hoy a las " + f.ToShortTimeString();
            else if (p.TotalDays < now.TimeOfDay.TotalDays + 1) return "Ayer a las " + f.ToShortTimeString();
            else if (p.TotalDays < now.TimeOfDay.TotalDays + 6) return "El " + f.ToString("dddd") + " a las " + f.ToShortTimeString();
            else return f.ToLongDateString() + " a las " + f.ToShortTimeString();

        }
        public static string MostrarTiempo(TimeSpan t)
        {
            string ret="";
            if (Math.Abs(t.Days) > 0) ret += t.Days + " días, ";
            if (Math.Abs(t.Hours) > 0) ret += t.Hours + " horas, ";
            if (Math.Abs(t.Minutes) > 0) ret += t.Minutes + " minutos, ";
            if (Math.Abs(t.Seconds) > 0) ret += t.Seconds + " segundos.";
            return ret;

        }
        /// <summary>
        /// Describe un número con palabras
        /// </summary>
        /// <param name="num">Número para describir</param>
        /// <returns>Cadena con la descripcíon</returns>
        public static String describirMonto(decimal num)
        {
            return changeToWords(num.ToString())
                .Replace("uno mil", "Mil")
                .Replace("uno ciento", "Ciento")
                .Replace("dos ciento", "docientos")
                .Replace("tres ciento", "trecientos")
                .Replace("cuatro ciento", "cuatrocientos")
                .Replace("cinco ciento", "quinientos")
                .Replace("seis ciento", "seiscientos")
                .Replace("siete ciento", "setecientos")
                .Replace("ocho ciento", "ochocientos")
                .Replace("nueve ciento", "novecientos")
                .Replace("Ciento  pesos", "Cien Pesos")
                .ToUpper()
                ;
        }
        private static String changeToWords(String numb)
        {
            int decimalPlace = 0;
            numb = numb.Replace(",", ".");
            String result = "";
            int c = numb.Length;
            if (numb.Contains(".") == true)
            {
                decimalPlace = numb.IndexOf(".");
            }
            else
            {
                numb = numb + ".00";
                decimalPlace = numb.IndexOf(".");
            }
            if (decimalPlace == c)
            {
                numb = numb + "00";
            }
            String wn = numb.Substring(0, decimalPlace);
            int wn_1 = wn.Length;
            String cents = numb.Substring(decimalPlace + 1);
            switch (wn_1)
            {
                //ones 
                case 1:
                    result = ones(numb.Substring(0, wn_1)) + " pesos con " + cents + " centavos";
                    break;
                //tens 
                case 2:
                    result = tens(numb.Substring(0, wn_1)) + " pesos con " + cents + " centavos";
                    break;
                //cientos 
                case 3:
                    result = ones(numb.Substring(0, 1)) + " ciento " + tens(numb.Substring(1, 2)) + " pesos con " + cents + " centavos";
                    break;
                //mils 
                case 4:
                    result = ones(numb.Substring(0, 1)) + " mil " + ones(numb.Substring(1, 1)) + " ciento " + tens(numb.Substring(2, 2)) + " pesos con " + cents + " centavos";
                    break;
                //ten mils 
                case 5:
                    result = tens(numb.Substring(0, 2)) + " mil " + ones(numb.Substring(2, 1)) + " ciento " + tens(numb.Substring(3, 2)) + " pesos con " + cents + " centavos";
                    break;
                //ciento mils 
                case 6:
                    result = ones(numb.Substring(0, 1)) + " ciento " + tens(numb.Substring(1, 2)) + " mil " + ones(numb.Substring(3, 1)) + " ciento " + tens(numb.Substring(4, 2)) + " pesos con " + cents + " centavos";
                    break;
                // millones 
                case 7:
                    result = ones(numb.Substring(0, 1)) + " millones " + ones(numb.Substring(1, 1)) + " ciento " + tens(numb.Substring(2, 2)) + " mil " + ones(numb.Substring(4, 1)) + " ciento " + tens(numb.Substring(5, 2)) + " pesos con " + cents + " centavos";
                    break;
                // ten milloness 
                case 8:
                    result = tens(numb.Substring(0, 2)) + " millones " + ones(numb.Substring(2, 1)) + " ciento " + tens(numb.Substring(3, 2)) + " mil " + ones(numb.Substring(5, 1)) + " ciento " + tens(numb.Substring(6, 2)) + " pesos con " + cents + " centavos";
                    break;
                case 9:
                    result = ones(numb.Substring(0, 1)) + " ciento " + tens(numb.Substring(1, 2)) + " millones " + ones(numb.Substring(3, 1)) + " ciento " + tens(numb.Substring(4, 2)) + " mil " + ones(numb.Substring(6, 1)) + " ciento " + tens(numb.Substring(7, 2)) + " pesos con " + cents + " centavos";
                    break;
                case 10:
                    result = " this is a lot of money contact IT for a fix ";
                    break;
                case 11:
                    result = " this is a lot of money contact IT for a fix ";
                    break;
                case 12:
                    result = " this is a lot of money contact IT for a fix ";
                    break;
            }
            return result;
        }
        public static string ObtenerPlantilla(string path)
        {
            string txt = "";
            if (System.IO.File.Exists(path))
            {

                System.IO.StreamReader objReader = new System.IO.StreamReader(path);
                string sLine = "";

                while (sLine != null)
                {
                    sLine = objReader.ReadLine();
                    if (sLine != null) txt += sLine;

                }
                objReader.Close();
            }

            return txt;

        }
        private static String tens(String digit)
        {
            int digt = Convert.ToInt32(digit);
            String name = null;
            switch (digt)
            {
                case 10:
                    name = "Dieci";
                    break;
                case 11:
                    name = "once";
                    break;
                case 12:
                    name = "doce";
                    break;
                case 13:
                    name = "trece";
                    break;
                case 14:
                    name = "catorce";
                    break;
                case 15:
                    name = "quince";
                    break;
                case 16:
                    name = "dieciseis";
                    break;
                case 17:
                    name = "diecisiete";
                    break;
                case 18:
                    name = "dieciocho";
                    break;
                case 19:
                    name = "diecinueve";
                    break;
                case 20:
                    name = "veinte";
                    break;
                case 30:
                    name = "treinta";
                    break;
                case 40:
                    name = "cuarenta";
                    break;
                case 50:
                    name = "cincuenta";
                    break;
                case 60:
                    name = "sesenta";
                    break;
                case 70:
                    name = "setenta";
                    break;
                case 80:
                    name = "ochenta";
                    break;
                case 90:
                    name = "noventa";
                    break;
                default:
                    if (digt > 0)
                    {
                        name = tens(digit.Substring(0, 1) + "0") + " y " + ones(digit.Substring(1));
                    }
                    break;
            }
            return name;
        }
        private static String ones(String digit)
        {
            int digt = Convert.ToInt32(digit);
            String name = "";
            switch (digt)
            {
                case 1:
                    name = "uno";
                    break;
                case 2:
                    name = "dos";
                    break;
                case 3:
                    name = "tres";
                    break;
                case 4:
                    name = "cuatro";
                    break;
                case 5:
                    name = "cinco";
                    break;
                case 6:
                    name = "seis";
                    break;
                case 7:
                    name = "siete";
                    break;
                case 8:
                    name = "ocho";
                    break;
                case 9:
                    name = "nueve";
                    break;
                case 0:
                    name = "cero";
                    break;
            }
            return name;
        }


        private static FieldInfo GetEventField(this Type type, string eventName)
        {
            FieldInfo field = null;
            while (type != null)
            {
                /* Find events defined as field */
                field = type.GetField(eventName, BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic);
                if (field != null && (field.FieldType == typeof(MulticastDelegate) || field.FieldType.IsSubclassOf(typeof(MulticastDelegate))))
                    break;

                /* Find events defined as property { add; remove; } */
                field = type.GetField("EVENT_" + eventName.ToUpper(), BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic);
                if (field != null)
                    break;
                type = type.BaseType;
            }
            return field;
        }

        public static class Crypto
        {
            public static string Encrypt(string toEncrypt, bool useHashing)
            {
                byte[] keyArray;
                byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

             
                // Get the key from config file

                string key = Properties.Settings.Default.Usuario;
                //System.Windows.Forms.MessageBox.Show(key);
                //If hashing use get hashcode regards to your key
                if (useHashing)
                {
                    MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    //Always release the resources and flush data
                    // of the Cryptographic service provide. Best Practice

                    hashmd5.Clear();
                }
                else
                    keyArray = UTF8Encoding.UTF8.GetBytes(key);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                //set the secret key for the tripleDES algorithm
                tdes.Key = keyArray;
                //mode of operation. there are other 4 modes.
                //We choose ECB(Electronic code Book)
                tdes.Mode = CipherMode.ECB;
                //padding mode(if any extra byte added)

                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateEncryptor();
                //transform the specified region of bytes array to resultArray
                byte[] resultArray =
                    cTransform.TransformFinalBlock(toEncryptArray, 0,
                    toEncryptArray.Length);
                //Release resources held by TripleDes Encryptor
                tdes.Clear();
                //Return the encrypted data into unreadable string format
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            public static string Decrypt(string cipherString, bool useHashing)
        {
            if (String.IsNullOrEmpty(cipherString)) return "";
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

           
            //Get your key from config file to open the lock!
            string key = Properties.Settings.Default.Usuario;

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                    toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
            
        }
        public static string CompressFile(string path)
        {
            
            FileStream sourceFile = File.OpenRead(path);
            FileStream destinationFile = File.Create(path + ".gz");
            string retpath = destinationFile.Name;
            byte[] buffer = new byte[sourceFile.Length];
            sourceFile.Read(buffer, 0, buffer.Length);

            using (GZipStream output = new GZipStream(destinationFile,
                CompressionMode.Compress))
            {
                Console.WriteLine("Compressing {0} to {1}.", sourceFile.Name,
                    destinationFile.Name, false);

                output.Write(buffer, 0, buffer.Length);
            }

            // Close the files.
            sourceFile.Close();
            destinationFile.Close();
            return retpath;
        }
        
        
    }
}
