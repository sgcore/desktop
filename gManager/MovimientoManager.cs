﻿using System.Data;
using gManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using gConnector;
using gLogger;
using System.Linq;
namespace gManager
{
    internal class MovimientoManager
    {
        private static MovimientoManager _instance;
        private System.Collections.Hashtable _movimientos = new System.Collections.Hashtable();
        private Hashtable _stock = new Hashtable();
       
       
        public MovimientoManager() { }
        public static MovimientoManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new MovimientoManager();
                return _instance;
            }
        }
        
        public DataTable stockToDatatable()
        {
            System.Data.DataTable dat = new System.Data.DataTable();
            dat.Columns.Add("Producto");
            dat.Columns.Add("Stock");
            var stockenum=_stock.GetEnumerator();
            while(stockenum.MoveNext()){
                DataRow r = dat.NewRow();
                object[] items={ProductoManager.Singlenton.getObjeto((int)stockenum.Key).Nombre,stockenum.Value };
                r.ItemArray = items;
                dat.Rows.Add(r);
            }
            
            return dat;
        }
        public Hashtable StockTable
        {
            get
            {
                return _stock;
            }
        }

       
        public void reloadMovimientos(System.ComponentModel.BackgroundWorker worker,DataTable dat, ref int cont, int totalcount)
        {
             _movimientos.Clear();
            
            //DataTable dat = Connector.Singlenton.ConexionActiva.ConsultarMovimientos("order by id desc");
            foreach (var r in Enum.GetValues(typeof(gcMovimiento.TipoMovEnum)))
                _movimientos.Add((int)r, new Hashtable());
            foreach (DataRow row in dat.Rows)
            {
                gcMovimiento u = new gcMovimiento(row);
                ((Hashtable)_movimientos[(int)u.Tipo]).Add(u.Id, u);
               
                //agregamos el movimiento a la caja
                CajaManager.Singlenton.obtenerCaja(u.Caja).addMovimiento(u);
                 //prograss
                cont++;
                if (worker != null)
                {
                    worker.ReportProgress((cont * 100 / totalcount) - 1);
                }

            }
          
           
           
        }
        public void reloadObjetosMovidos(System.ComponentModel.BackgroundWorker worker, DataTable dat,ref int cont, int totalcount)
        {
            _stock.Clear();
            foreach (DataRow row in dat.Rows)
            {
                gcObjetoMovido om = new gcObjetoMovido(row);
                gcMovimiento m = updateStock(om);
                if (m != null)
                {
                    m.insertObjetoMovidoFromDB(om);
                }

                //prograss
                cont++;
                if (worker != null)
                {
                    worker.ReportProgress((cont * 100 / totalcount) - 1);
                }
               
            }
        }
        public System.Collections.ICollection Ventas
        {
            get
            {
                return ((Hashtable)_movimientos[(int)gcMovimiento.TipoMovEnum.Venta]).Values;
            }
        }
        public System.Collections.ICollection Compras
        {
            get
            {
                return ((Hashtable)_movimientos[(int)gcMovimiento.TipoMovEnum.Compra]).Values;
            }
        }
        public System.Collections.ICollection Depositos
        {
            get
            {
                return ((Hashtable)_movimientos[(int)gcMovimiento.TipoMovEnum.Deposito]).Values;
            }
        }
        public System.Collections.ICollection Extracciones
        {
            get
            {
                return ((Hashtable)_movimientos[(int)gcMovimiento.TipoMovEnum.Extraccion]).Values;
            }
        }
        
        public gcMovimiento  updateStock(gcObjetoMovido om,bool remove=false)
        {
            gcMovimiento m = getMovimiento(om.Movimiento);
            decimal cant= om.Cantidad;
            //si es remove resta
            if (remove) cant = -cant;
            if (m != null &&om.Objeto!=null)
            {
                if (m.EsEntradaMercaderia)
                {
                    addStock(om.Objeto.Id,cant);
                }
                else if (m.EsSalidaMercaderia)
                {
                    removeStock(om.Objeto.Id, cant);
                }
            }
            return m;
        }
        private decimal addStock(int obj,decimal cant){
            decimal exist = 0;
            if(_stock.ContainsKey(obj))
            {
                exist = (decimal)_stock[obj];
                _stock.Remove(obj);
            }
            if(exist+cant!=0)
                _stock.Add(obj, exist + cant);
            return exist + cant;
        }
        private decimal removeStock(int obj, decimal cant)
        {
            return addStock(obj, -cant);
        }
        private void reloadStock()
        {
            _stock.Clear();
            //usamos and tipo<8 para evitar presupuestos y pedidos
            string sqlE = "select sum(Cantidad) as cantidad, objeto from gcobjetosmovidos where movimiento in (select id from gcmovimientos where Destino=1 and tipo<8) group by objeto";
            string sqlS = "select sum(Cantidad) as cantidad, objeto from gcobjetosmovidos where movimiento in (select id from gcmovimientos where origen=1  and tipo<8) group by objeto";
            Hashtable _entradas = new Hashtable();
            Hashtable _salidas = new Hashtable();
            DataTable dat = Connector.Singlenton.ConexionActiva.Consulta(sqlE);
            foreach (DataRow row in dat.Rows)
            {
                _entradas.Add((int)row["objeto"], (decimal)row["cantidad"]);
            }
            dat = Connector.Singlenton.ConexionActiva.Consulta(sqlS);
            foreach (DataRow row in dat.Rows)
            {
                _salidas.Add((int)row["objeto"], (decimal)row["cantidad"]);
            }
            var enu=_entradas.GetEnumerator();
            while(enu.MoveNext())
            {
                decimal e =(decimal) _entradas[enu.Key];
                decimal s = 0;
                if (_salidas.ContainsKey(enu.Key))
                {
                    s = (decimal)_salidas[enu.Key];
                    _salidas.Remove(enu.Key);
                }
                 
               
                _stock.Add(enu.Key,(decimal)(e - s));
            }
            _entradas.Clear();
           var enus = _salidas.GetEnumerator();
            while (enus.MoveNext())
            {
                 decimal s = (decimal)_salidas[enus.Key];
               
               
                _stock.Add(enus.Key, (decimal)(0- s));
            }
            _salidas.Clear();
            logger.Singlenton.addDebugMsg("Productos", "Se cargaron " + _stock.Count + " productos con stock.");

        }
        public void updateStock(gcObjeto o,decimal increment)
        {
            if ( increment == 0)
            {
                //logger.Singlenton.addWarningMsg("Se intento hacer una operacion que no produce cambios en el stock.");
                return;
            }
            if (increment<0)
            {
                //aqui usa addStock por que se maneja con positivos y negativos;
                logger.Singlenton.addMessage("Se extrajeron " + Math.Abs(increment) + " unidades de " + o.Nombre + ". Stock actual: " + addStock(o.Id, increment), "INFORME DE STOCK");
            }
            else if (increment>0)
            {
                logger.Singlenton.addMessage("Se ingresaron " + Math.Abs(increment) + " unidades de " + o.Nombre + ". Stock actual: " + addStock(o.Id,increment), "INFORME DE STOCK");
            }
            //informe
           

          

        }
        public decimal getStock(int objId)
        {
            if(_stock.ContainsKey(objId))return (decimal)_stock[objId];
            return 0;
        }
        public gcObjetoMovido[] UltimosPrecios(gcMovimiento.TipoMovEnum tipo,gcObjeto ob)
        {
              List<gcObjetoMovido> l = new List<gcObjetoMovido>();
              int cont = 0;
              var ms = getMovimientos(tipo);
              foreach (gcMovimiento m in ms)
              {
                  if (cont > 4) break;
                  var om = m.getObjetoIfExist(ob);
                  if(om!=null)
                  {
                      l.Add(om);
                      cont++;
                  }
              }
              return l.ToArray();
           
           

        }
        public List<ProductoResumen> ConsumoEnMovimientos(Filtros.FiltroMovimiento f)
        {

            Hashtable t = new Hashtable();
            var ms = getMovimientos(f);
            foreach (gcMovimiento m in ms)
            {
                int a = m.EsDevolucionORetorno ? -1 : 1;
                foreach (gcObjetoMovido om in m.ObjetosMovidos)
                {
                    if (t.ContainsKey(om.ObjetoID))
                    {
                        ((ProductoResumen)t[om.ObjetoID]).Cantidad += a*om.Cantidad;
                        ((ProductoResumen)t[om.ObjetoID]).Total += a*om.TotalCotizado;
                    }
                    else
                    {
                        ProductoResumen p = new ProductoResumen();
                        p.Producto = om.Objeto;
                        p.Cantidad = om.Cantidad;
                        p.Total = om.TotalCotizado;
                        t.Add(om.ObjetoID, p);
                    }
                }
            }


            return t.Values.OfType<ProductoResumen>().ToList();
        }

        public List<gcMovimiento> getMovimientos(Filtros.FiltroMovimiento FiltroMov)
        {
            List<gcMovimiento> l = new List<gcMovimiento>();
            if (FiltroMov == null) return l;
            foreach (int t in FiltroMov.TiposAFiltrar)
            {
                foreach (gcMovimiento m in getMovimientos(t))
                {
                    if (FiltroMov.NumeroEspecifico > 0 && m.Numero != FiltroMov.NumeroEspecifico) continue;
                    if (FiltroMov.porCaja)
                    {
                        if (FiltroMov.CajaFinal < m.Caja || FiltroMov.CajaInicial > m.Caja) continue;

                    }
                    if (FiltroMov.porFecha)
                    {
                        if (FiltroMov.FechaInicial > m.Fecha || FiltroMov.FechaFinal < m.Fecha) continue;
                    }
                    if (FiltroMov.Incompletos && m.Estado == gcMovimiento.EstadoEnum.Terminado) continue;
                    if (FiltroMov.Destinatario != null && m.Destinatario != null && FiltroMov.Destinatario.Id != m.Destinatario.Id) continue;
                    if (FiltroMov.Producto != null && m.getObjetoIfExist(FiltroMov.Producto) == null) continue;
                    l.Add(m);

                }
            }
            return l;
        }
       
        public gcMovimiento getMovimiento(int id,int tipo)
        {
            return (gcMovimiento)((Hashtable)_movimientos[tipo])[id];
        }
        public gcMovimiento getMovimiento(int id)
        {
            foreach (var r in Enum.GetValues(typeof(gcMovimiento.TipoMovEnum)))
            {
                 gcMovimiento m=(gcMovimiento)((Hashtable)_movimientos[(int)r])[id];
                 if (m != null) return m;
                                
            }
            return null;
            
        }
        public gcMovimiento getUltimoMovimiento(gcMovimiento.TipoMovEnum tipo)
        {
            DataTable dat = Connector.Singlenton.ConexionActiva.Consulta("select max(id) as id from gcmovimientos where tipo=" + (int)tipo);
            foreach (DataRow row in dat.Rows)
            {
                if (row["id"] != DBNull.Value)
                {
                    return (gcMovimiento)((Hashtable)_movimientos[(int)tipo])[int.Parse((row["id"]).ToString())];
                }
            }
            return null;
        }
        public System.Collections.ICollection getMovimientos(int tipo)
        {
            if(_movimientos.ContainsKey(tipo))
            return ((Hashtable)_movimientos[tipo]).Values;
            return new List<gcMovimiento>();
        }
        public System.Collections.ICollection getMovimientos(gcMovimiento.TipoMovEnum tipo)
        {
            return getMovimientos((int)tipo);
        }
        public List<gcMovimiento> getMovimientos()
        {
            List<gcMovimiento> l = new List<gcMovimiento>();
            foreach (Hashtable h in _movimientos.Values)
            {
                l.AddRange (h.Values.OfType<gcMovimiento>());
            }


            return l;
        }
        public gcMovimiento GuardarMovimientoEnDB(gcMovimiento m)
        {
            m.LastMod = DateTime.Now;
            if (m.Numero == 0) m.Numero = SiguienteNumero(m.Tipo);

            Hashtable t = new Hashtable();
            t.Add("id", m.Id);
            t.Add("Origen", m.OrigenId);
            t.Add("Destino", m.DestinoId);
            t.Add("observaciones", m.Observaciones);
            t.Add("caja", m.Caja);
            t.Add("tipo", (int)m.Tipo);
            t.Add("usuario", m.UsuarioId);
            t.Add("Fecha", m.Fecha);
            t.Add("LastMod", m.LastMod );
            t.Add("Estado",(int) m.Estado );
            t.Add("Numero", m.Numero);
            t.Add("Codigo", m.Codigo);
            t.Add("facnum", m.FacturaNumero);
            t.Add("factipo", m.FacturaTipo);
            m.Id = Connector.Singlenton.ConexionActiva.GuardarENtity(t,"gcmovimientos",m.Id>0);
            if (addMovimiento(m))
            {
                logger.Singlenton.addMessage("Movimiento: Se creó  " + m.Tipo.ToString() + " " + m.Numero);
                CoreManager.Singlenton.agregarMovimiento(m); 
            }
            else
                logger.Singlenton.addMessage("Movimiento: Se actualizó  " + m.Tipo.ToString() + " " + m.Numero);
             return m;
        }
        private bool addMovimiento(gcMovimiento m)
        {
            gcMovimiento em = getMovimiento(m.Id, (int)m.Tipo);
            if (em != null)
            {
                em.reloadMontos();
                return false;
            }
            else
            {
                ((Hashtable)_movimientos[(int)m.Tipo]).Add(m.Id, m);
                //agregamos el movimiento a la caja
                CajaManager.Singlenton.obtenerCaja(m.Caja).addMovimiento(m);
                return true;
            }
        }

       
        public int SiguienteNumero(gcMovimiento.TipoMovEnum tipo)
        {
            DataTable dat = Connector.Singlenton.ConexionActiva.Consulta("SELECT MAX(numero) as numero from gcmovimientos where tipo="+(int)tipo);
            foreach (DataRow row in dat.Rows)
            {
                if (row[0] != DBNull.Value)
                {
                    return (int.Parse((row["numero"]).ToString())) + 1;
                }
            }
               
            return 1;
        }
        public gManager.gcMovimiento crearNuevoMovimiento(gManager.gcMovimiento.TipoMovEnum tipo,gcDestinatario destin, gcUsuario user = null)
        {
            if (!gManager.CajaManager.Singlenton.PuedeCrearMovimientos)
            {
                logger.Singlenton.addWarningMsg("No se pueden crear movimientos en esta caja.");
                return null;
            }
            if (CoreManager.Singlenton.CajaBloqueada())
            {
                return null;
            }
            if (!gManager.UsuarioManager.Singlenton.PermisoUsuario(gManager.gcUsuario.PermisoEnum.Vendedor))
            {
                logger.Singlenton.addWarningMsg("No tiene permiso para crear un movimiento");
                return null;
            }
            if (UsuarioManager.Singlenton.UsuarioActual.Central == null)
            {
                logger.Singlenton.addWarningMsg("No tiene asignada una central. No se puede crear el movimiento.");
                return null;
            }
            
            
            gManager.gcMovimiento m = new gManager.gcMovimiento();
            m.Tipo = tipo;
            m.setOriginario(UsuarioManager.Singlenton.UsuarioActual.Central);
            m.Usuario = UsuarioManager.Singlenton.UsuarioActual;
            m.Caja = gManager.CajaManager.Singlenton.CajaActual.Caja;

            if (!m.setDestinatario(destin))
            {
                logger.Singlenton.addWarningMsg("No tiene asignado un destinatario. No se puede crear el movimiento.");
                return null;
            }
           m.Observaciones = m.DescripcionTipo;

            return m;
        }
        public gcObjetoMovido guardarObjetoMovidoEnDB(gcObjetoMovido om)
        {
            Hashtable t = new Hashtable();
            t.Add("id", om.Id);
            t.Add("Movimiento", om.Movimiento);
            t.Add("cantidad", om.Cantidad);
            t.Add("Objeto", om.ObjetoID);
            t.Add("monto", om.Monto);
            t.Add("Observaciones", om.Observaciones);
            t.Add("Parent", om.Parent);
            t.Add("Moneda", om.Moneda);
            t.Add("Cotizacion", om.Cotizacion);
            om.Id= Connector.Singlenton.ConexionActiva.GuardarENtity(t,"gcobjetosmovidos",om.Id>0);
            return om;
        }
        public string eliminarObjetoMovidoDeDB(gcObjetoMovido om,bool ret =false)
        {
            if (om == null) return "";
            string sql = "delete from gcObjetosMovidos where id=" + om.Id+";";
            if (ret)
            {
                return sql;
            }
            logger.Singlenton.addDebugMsg("Movimiento", "Eliminando ObjetoMovido por " + om.Monto.ToString("0.00") + " del movimiento " + om.Movimiento);
            Connector.Singlenton.ConexionActiva.Consulta(sql);
            return "";
        }
      
        public bool deleteMovimiento(gcMovimiento m)
        {
           if (m == null) return true;
           if (gManager.CajaManager.Singlenton.CajaActual.Caja != m.Caja && !UsuarioManager.Singlenton.PermisoUsuario(gcUsuario.PermisoEnum.Administrador))
           {
               logger.Singlenton.addWarningMsg("El Movimiento " + m.DescripcionAuto + " no pertenece a esta caja.\n No puede eliminarse.");
               return false;
           }
           if (m.UsuarioId != UsuarioManager.Singlenton.UsuarioActual.Id && !UsuarioManager.Singlenton.PermisoUsuario(gcUsuario.PermisoEnum.Administrador))
           {
               logger.Singlenton.addWarningMsg("El Movimiento " + m.DescripcionAuto + " no fue creado por usted.\n No puede eliminarse.");
               return false;
           }
           m.clearPagos();
            
            foreach (gcObjetoMovido om in m.ObjetosMovidos) {
                updateStock(om, true);
                 MovimientoManager.Singlenton.eliminarObjetoMovidoDeDB(om);
            }
                
            string t="delete from gcmovimientos where id="+m.Id+";\n";
            t += "delete from gcpagos where movimiento=" + m.Id + ";\n";
            t += "delete from gcobjetosmovidos where movimiento=" + m.Id + ";\n";
           
            Connector.Singlenton.ConexionActiva.Consulta(t);
            logger.Singlenton.addMessage("Se elimino el movimiento " + m.Id);
           
            ((Hashtable)_movimientos[(int)m.Tipo]).Remove(m.Id);
            CajaManager.Singlenton.obtenerCaja(m.Caja).removeMovimiento(m);
            CoreManager.Singlenton.eliminarMovimiento(m);
            return true;
            
        }

        
       
       
    }
}
