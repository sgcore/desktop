﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager.Filtros
{
    public class FiltroMovimiento : Filtro
    {
        public FiltroMovimiento()
        {
            Reset();
        }
        public override void Reset()
        {
           
            TiposAFiltrar = new List<int>();
            foreach (var r in Enum.GetValues(typeof(gcMovimiento.TipoMovEnum)))
            {
                TiposAFiltrar.Add((int)r);
            }
            FechaFinal = DateTime.Now;
            FechaInicial = DateTime.Now;
            CajaInicial = CajaManager.Singlenton.CajaActual.Caja;
            CajaFinal = CajaManager.Singlenton.CajaActual.Caja;
            porCaja = true;
            Destinatario = null;
            Producto = null;
            Incompletos = false;
            NumeroEspecifico = 0;
        }
        public void setTodasLasCajas()
        {
            porCaja = true;
            CajaFinal = CajaManager.Singlenton.UltimaCaja.Caja;
            CajaInicial = 0;
        }
        public override Filtro Clone()
        {
            var f = new FiltroMovimiento();
            f.CajaFinal = CajaFinal;
            f.CajaInicial = CajaInicial;
            f.Destinatario = Destinatario;
            f.Producto = Producto;
            
            f.FechaFinal = FechaFinal;
            f.FechaInicial = f.FechaInicial;
            f.Incompletos = Incompletos;
            f.NumeroEspecifico = NumeroEspecifico;
            f.porCaja = porCaja;
            f.porFecha = porFecha;
            f.Texto = Texto;
            f.TiposAFiltrar = TiposAFiltrar;
            return f;
        }
        
        public override bool Filtrado
        {
            get { return Incompletos || porFecha || Destinatario!=null || Producto !=null || NumeroEspecifico>0
                || !(Enum.GetValues(typeof(gcMovimiento.TipoMovEnum)).Length ==TiposAFiltrar.Count && porCaja && CajaInicial == CajaManager.Singlenton.CajaActual.Caja);
            }
        }
        public override string Descripcion
        {
            get
            {
                string d = "";
                if (Destinatario != null) d = " de " + Destinatario.Nombre;
                return tiposfiltradosdesc + " " + FechaCajafiltradoDesc + d;
            }
        }
        
        
        public bool Incompletos { get; set; }
        public bool porCaja { get; set; }
        public bool porFecha { get; set; }
        public int CajaInicial { get; set; }
        public int CajaFinal { get; set; }
        public DateTime FechaInicial { get; set; }
        public DateTime FechaFinal { get; set; }
        public string FiltroSQL { get; set; }
        public int NumeroEspecifico { get; set; }
        public string FiltroSQLconAND
        {
            get
            {
                if (FiltroSQL != null && FiltroSQL != "") return " and " + FiltroSQL;
                return "";
            }
        }
        public string FiltroSQLconWhere
        {
            get
            {
                if (FiltroSQL != null || FiltroSQL != "") return " where " + FiltroSQL;
                return "";
            }
        }
        public string FechaCajafiltradoDesc
        {
            get
            {
                if (Filtrado)
                {
                    if (NumeroEspecifico > 0) return " Número " + NumeroEspecifico;
                    if (porCaja)
                    {
                        if (CajaInicial == 0 && CajaFinal == CajaManager.Singlenton.CajaActual.Caja) return "todas las cajas";
                        if (CajaInicial == CajaFinal)
                        {
                            if (CajaInicial == CajaManager.Singlenton.CajaActual.Caja) return "caja actual";
                            return "en caja " + CajaInicial;
                        }
                        return "entre caja " + CajaInicial + " y " + CajaFinal;
                    }
                    if (porFecha)
                    {
                        return "entre el " + FechaInicial.ToShortDateString() + " a las " + FechaInicial.ToShortTimeString() + "y el " + FechaFinal.ToShortDateString() + " a las " + FechaFinal.ToShortTimeString();
                    }
                }
                return "Sin filtro";
            }
        }
        public string tiposfiltradosdesc
        {
            get
            {
                if (Enum.GetValues(typeof(gcMovimiento.TipoMovEnum)).Length == TiposAFiltrar.Count) return "Todos los tipos";
                string t = "";
                foreach (int i in TiposAFiltrar)
                {
                    t += Enum.GetName(typeof(gManager.gcMovimiento.TipoMovEnum), i).Replace("_", " ") + ",";
                }

                return t;
            }
        }
        public List<int> TiposAFiltrar { get; set; }
        public gManager.gcDestinatario Destinatario { get; set; }
        public gManager.gcObjeto Producto { get; set; }
    }
       
}
