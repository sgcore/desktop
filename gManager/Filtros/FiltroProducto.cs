﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager.Filtros
{
    public class FiltroProducto : Filtro
    {
        private List<gcObjeto.ObjetoTIpo> _tipos = new List<gcObjeto.ObjetoTIpo>();
        public FiltroProducto()
        {
            Reset();

        }
        
        public List<gcObjeto.ObjetoTIpo> Tipos
        {
            get
            {
                return _tipos;
            }
           

        }
        public int Parent { get; set; }
        public int Grupo { get; set; }
        public override void Reset()
        {
            Parent = -1;
            Grupo = -1;
            Texto = "";
            _tipos.Clear();

        }
        public override Filtro Clone()
        {
            var f = new FiltroProducto();
            f.Parent = Parent;
            f.Grupo = Grupo;
            f.Texto = Texto;
            foreach (var t in Tipos)
            {
                f.Tipos.Add(t);
            }
            return f;
        }
        public override bool Filtrado
        {
            get 
            {
                return Parent > -1 || Grupo > -1 || Texto.Length > 0 || _tipos.Count > 0; 
            }
        }
        public override string Descripcion
        {
            get
            {
                if (!Filtrado) return "Buscar todos";
                string t = "Buscar ";
                if (Texto != "")
                {
                    t += "'" + Texto + "'; ";
                }
                if (Parent > -1)
                {
                    t += "en categoria ";
                    if (Grupo > -1)
                        t += " y grupo";
                }
                else if (Grupo > -1)
                {
                    t += "en grupo ";
                }
                if (_tipos.Count > 0)
                {
                    t += "(";
                    foreach (gcObjeto.ObjetoTIpo tp in _tipos)
                    {
                        t+=tp.ToString()+" ";

                    }
                    t += ")";
                }




                return t;
            }
        }

        public bool SinStock { get; set; }
        public bool StockNegativo { get; set; }
        public bool StockBajo { get; set; }
        public bool StockAlto { get; set; }
        public bool FiltraStock { get {
                return SinStock
                    || StockNegativo
                    || StockAlto
                    || StockBajo;
                    } }
        public bool EstadoDiscontinuo { get; set; }
        public bool EstadoPublico { get; set; }
        public bool EstadoNormal { get; set; }
        public bool FiltraEstado { get
            {
                return EstadoDiscontinuo
                    || EstadoNormal
                    || EstadoPublico;
            } }
        public bool TieneDolar { get; set; }
        public decimal PercioMayorQue { get; set; }
        public decimal PercioMenorQue { get; set; }
        public decimal GananciaMayorQue { get; set; }
        public decimal GananciaMenorQue { get; set; }



    }
       
}
