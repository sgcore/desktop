﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager.Filtros
{
    public class FiltroCaja : Filtro
    {
        public enum TipoFiltroCaja { CajaActual,UltimaCaja,UnaCaja,EntreCajas,EntreFechas,Todas,SinFiltrar,Custom}
        public FiltroCaja(TipoFiltroCaja t) {
            Tipo = t;
            switch (t)
            {
                case TipoFiltroCaja.EntreCajas:
                case TipoFiltroCaja.EntreFechas:
                case TipoFiltroCaja.Todas:
                    CajaInicial = 0;
                    CajaFinal = CajaManager.Singlenton.UltimaCaja.Caja;
                    break;
                case TipoFiltroCaja.UltimaCaja:
                case TipoFiltroCaja.UnaCaja:
                    CajaInicial = CajaManager.Singlenton.UltimaCaja.Caja;
                    CajaFinal = CajaManager.Singlenton.UltimaCaja.Caja;
                    break;
                case TipoFiltroCaja.CajaActual:
                    CajaInicial= CajaManager.Singlenton.CajaActual.Caja;
                    CajaFinal = CajaManager.Singlenton.CajaActual.Caja;
                    break;
                
            }
            if (t == TipoFiltroCaja.CajaActual)
                CajaSola = CajaManager.Singlenton.CajaActual.Caja;
            else if (t == TipoFiltroCaja.UnaCaja)
                CajaSola = CajaManager.Singlenton.UltimaCaja.Caja;
        }
        public TipoFiltroCaja Tipo { get; set; }
        public FiltroCaja(DateTime fechaini,DateTime fechafin) 
        {
            if (fechaini > fechafin)
            {
                var f = fechafin;
                fechafin = fechaini;
                fechaini = f;
            }
            FechaInicial = fechaini;
            FechaFinal = fechafin;
            Tipo=TipoFiltroCaja.EntreFechas;
        }
        public FiltroCaja(int cajaini, int cajafin)
        {
            if (cajafin < cajaini)
            {
                int c = cajafin;
                cajafin = cajaini;
                cajaini = c;
            }
            else if (cajaini == cajafin)
            {
                CajaSola = cajaini;
                Tipo=TipoFiltroCaja.UnaCaja;
            }
            else
            {
                CajaInicial = cajaini;
                CajaFinal = cajafin;
               Tipo=TipoFiltroCaja.EntreCajas;
            }
            
        }
        public FiltroCaja(int caja)
        {
            CajaSola = caja;
            Tipo = TipoFiltroCaja.UnaCaja;
        }
        public FiltroCaja(List<gcCaja> cajas)
        {
            ListaDeCajasCustom = cajas;
        }
        public override bool Filtrado
        {
            get { return Tipo != TipoFiltroCaja.SinFiltrar; }
        }
        List<gcCaja> _lcajas=new List<gcCaja>();
        public List<gcCaja> ListaDeCajasCustom
        {
            get { return _lcajas; }
            set { _lcajas = value; Tipo = TipoFiltroCaja.Custom; }
        }
        public bool esCajaActual { get { return Tipo == TipoFiltroCaja.CajaActual; } }
        public bool esUltimaCaja { get { return Tipo == TipoFiltroCaja.UltimaCaja; } }
        public bool esUnaCaja { get { return Tipo == TipoFiltroCaja.UnaCaja; } }
        public bool esListaCustom { get { return Tipo == TipoFiltroCaja.Custom; } }
        public bool esTodas { get { return Tipo == TipoFiltroCaja.Todas; } }
        public int CajaSola { get; set; }
        public bool esEntreCajas { get { return Tipo == TipoFiltroCaja.EntreCajas; } }
        public bool esEntreFechas { get { return Tipo == TipoFiltroCaja.EntreFechas; } }
        public bool EstaFiltradoPorNumero
        {
            get
            {
                return Tipo == TipoFiltroCaja.CajaActual || Tipo == TipoFiltroCaja.EntreCajas ||
                    Tipo == TipoFiltroCaja.Todas || Tipo == TipoFiltroCaja.UnaCaja || Tipo == TipoFiltroCaja.UltimaCaja;
            }
        }

        public gcUsuario UsuarioCreador { get; set; }
        public gcUsuario UsuarioFinalizador { get; set; }
        public DateTime FechaInicial {get;set;}
        public DateTime FechaFinal { get; set; }
        public int CajaInicial { get; set; }
        public int CajaFinal { get; set; }
        public override string Descripcion
        {
            get
            {
                switch (Tipo)
                {
                    case TipoFiltroCaja.Todas:
                        return "Todas las cajas";
                    case TipoFiltroCaja.EntreCajas:
                        return "Entre caja " + CajaInicial + " y " + CajaFinal;
                    case TipoFiltroCaja.EntreFechas:
                        return "Entre fecha " + FechaInicial.ToShortDateString() + " a las " + FechaInicial.ToShortTimeString()
                            + " y " + FechaFinal.ToShortDateString() + " a las " + FechaFinal.ToShortTimeString();
                    case TipoFiltroCaja.UltimaCaja:
                        return "Caja " + CajaManager.Singlenton.UltimaCaja.Caja;
                    case TipoFiltroCaja.CajaActual:
                        return "Caja " + CajaManager.Singlenton.CajaActual.Caja;
                    case TipoFiltroCaja.UnaCaja:
                        return "Caja " + CajaSola;
                    default:
                        return "Cualquier caja";

                }
            }
        }
        public override void Reset()
        {
            throw new NotImplementedException();
        }
        public override Filtro Clone()
        {
            throw new NotImplementedException();
        }
    }
}
