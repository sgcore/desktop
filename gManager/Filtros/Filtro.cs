﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager.Filtros
{
    public abstract class Filtro
    {
        public abstract void Reset();
        public abstract bool Filtrado { get; }
        public abstract string Descripcion { get; }
        string _t = "";
        public string Texto { get { return _t; } set { _t = value; } }
        public abstract Filtro Clone();
        
    }
}
