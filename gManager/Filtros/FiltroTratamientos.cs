﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager.Filtros
{
    public class FiltroTratamientos : Filtro
    {
        public FiltroTratamientos()
        {
            Reset();
        }

        public FiltroTratamientos(gcDestinatario d)
        {
            Reset();
            addDestinatario(d);
        }
        public FiltroDestinatario DestinatariosFiltrados { get; set; }
        public List<gcDestinatario> Destinatarios
        {
            get
            {
                //devuelve una vacia si no estan filtrados
                if (DestinatariosFiltrados == null) return _misdest;
                var l= DestinatarioManager.Singlenton.getDestinatarios(DestinatariosFiltrados);

                l.AddRange(_misdest.ToArray());
                return l;
            }
        }
        private List<gcDestinatario> _misdest = new List<gcDestinatario>();
        public void addDestinatario(gcDestinatario d)
        {
            _misdest.Add(d);
        }
        private List<gcTratamiento.TipoTratamiento> _misTipos=new List<gcTratamiento.TipoTratamiento>();
        public List<gcTratamiento.TipoTratamiento> Tipos
        {
            get
            {
                return _misTipos;
            }
        }
        private List<gcTratamiento.EstadoSeguimientoEnum> _misEstados=new List<gcTratamiento.EstadoSeguimientoEnum>();
       
        public List<gcTratamiento.EstadoSeguimientoEnum> Estados
        {
            get
            {
                return _misEstados;
            }
        }
       
        public override void Reset()
        {
            _misdest.Clear();
            DestinatariosFiltrados = null;
            _misEstados.Clear();
            _misTipos.Clear();
            Texto = "";


        }
       

        public override bool Filtrado
        {
            get
            {
                return _misdest.Count > 0 ||
                    _misEstados.Count > 0 ||
                    _misTipos.Count > 0 ||
                    DestinatariosFiltrados != null ||
                    String.IsNullOrEmpty(Texto)
                    ;
            }
        }

        public override string Descripcion
        {
            get { return "Buscar "+Texto+" en tratamientos"; }
        }

        public override Filtro Clone()
        {
            throw new NotImplementedException();
        }
    }
}
