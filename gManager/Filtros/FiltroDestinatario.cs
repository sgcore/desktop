﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager.Filtros
{
    public class FiltroDestinatario : Filtro
    {
        List<gcDestinatario.DestinatarioTipo> _tipos = new List<gcDestinatario.DestinatarioTipo>();
        public FiltroDestinatario()
        {
            Reset();
        }
        public FiltroDestinatario(gManager.gcDestinatario.DestinatarioTipo tipo)
        {
            Tipos.Add(tipo);
           
        }

        public List<gcDestinatario.DestinatarioTipo> Tipos { get { return _tipos; } }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Cuit { get; set; }
        public int Parent { get; set; }

        public override void Reset()
        {
            Tipos.Clear();
            Texto = "";
            Nombre = "";
            Direccion = "";
            Cuit = "";
        }
        public override Filtro Clone()
        {
            var f = new FiltroDestinatario();
            f.Cuit = Cuit;
            f.Direccion = Direccion;
            f.Nombre = Nombre;
            f.Texto = Texto;
            foreach (var t in Tipos)
            {
                f.Tipos.Add(t);
            }
            return f;
        }

        public override bool Filtrado
        {
            get { return Texto!="" || Tipos.Count > 0 || Nombre != "" || Direccion != "" || Cuit !="" ; }
        }

        public override string Descripcion
        {
            get 
            { 
                if(!Filtrado)return "Buscar Todos";
                string t = "Buscar ";
                if (Texto != "")
                {
                    t += "'"+Texto+"'";
                }
                if (Nombre !=null || Nombre != "")
                {
                    t += " '" + Nombre + "'";
                }
                if (Direccion !=null || Direccion != "")
                {
                    t += " '" + Direccion + "'";
                }
                if (Cuit !=null || Cuit != "")
                {
                    t += " Cuit: " + Cuit;
                }
                if (Tipos.Count > 0)
                {
                    t += " (";
                    foreach (gcDestinatario.DestinatarioTipo tp in Tipos)
                    {
                        t += tp.ToString() + " ";
                    }

                    t += ") ";
                }
                return t;
            }
        }
    }
}
