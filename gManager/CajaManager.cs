﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using gConnector;
using gLogger;
using gManager.Resumenes;
namespace gManager
{
    
    internal class CajaManager
    {
        private static CajaManager _instance;
        private gcPago _efectivo=new gcPago(0,gcPago.TipoPago.Efectivo);
        private gcPago _tarjeta = new gcPago(0, gcPago.TipoPago.Tarjeta);
        private gcPago _cheque = new gcPago(0, gcPago.TipoPago.Cheque);
        private gcPago _cuenta = new gcPago(0, gcPago.TipoPago.Cuenta);
        private int _pcajaActual;
        private int _ultimaCaja;
        private SortedDictionary <int, gcCaja> _cajas = new SortedDictionary<int, gcCaja>();
        
        private CajaManager() { }
        public static CajaManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new CajaManager();
                return _instance;
            }
           
        }
        /// <summary>
        /// Resetea las cajas, debe ejecutarse antes de cargar los movimientos
        /// </summary>
        internal void resetCajas()
        {
            _efectivo=new gcPago(0,gcPago.TipoPago.Efectivo);
           _tarjeta = new gcPago(0, gcPago.TipoPago.Tarjeta);
           _cheque = new gcPago(0, gcPago.TipoPago.Cheque);
            _cuenta = new gcPago(0, gcPago.TipoPago.Cuenta);
           _pcajaActual =0;
            _ultimaCaja=0;
           _cajas.Clear();
        }
        
        /// <summary>
        /// Recarga los pagos en las cajas, requiere movimientos cargados.
        /// </summary>
        public void reloadPagos(System.ComponentModel.BackgroundWorker worker, DataTable dat,ref int cont, int totalcount)
        {
          
            foreach (DataRow row in dat.Rows)
            {
                gcPago p = new gcPago(row);
                gcMovimiento m = MovimientoManager.Singlenton.getMovimiento(p.Movimiento);
               
                
                if (m != null)
                {
                  gcCaja  c= obtenerCaja(m.Caja);
                  c.AddPago(m, p,false);
                  c.addMovimiento(m);
                  m.insertPagoFromDB(p);
                    
                   
                    
                }

                //prograss
                cont++;
                if (worker != null)
                {
                    worker.ReportProgress((cont * 100 / totalcount) - 1);
                }

            }
           
        }
        
        public gcCaja obtenerCaja(int numCaja)
        {
            if (!_cajas.ContainsKey(numCaja))
            {
                gcCaja caja = new gcCaja(numCaja);
                _cajas.Add(numCaja, caja);
                return caja;
            }
            gcCaja c = (gcCaja)_cajas[numCaja];
            if (_ultimaCaja == 0 || _ultimaCaja < c.Caja) _ultimaCaja = c.Caja;
            return c;
        }
        public List<gcCaja> getCajas(Filtros.FiltroCaja filtro)
        {
            if (filtro == null) filtro = new Filtros.FiltroCaja(CajaActual.Caja);
            if (filtro.esListaCustom)
            {
                return filtro.ListaDeCajasCustom;
            }
            List<gcCaja> l = new List<gcCaja>();
            if (filtro.esUnaCaja)
            {
                if(_cajas.ContainsKey(filtro.CajaSola))
                {
                    gcCaja solo = _cajas[filtro.CajaSola];
                    l.Add(solo);
                }
                    
                return l;
            }
            if (filtro.esUltimaCaja)
            {
                l.Add(UltimaCaja);
                return l;
            }
            if (filtro.esCajaActual)
            {
                l.Add(CajaActual);
                return l;
            }
            //varias cajas;
            if (filtro.esEntreFechas)
            {
                foreach (gcCaja c in _cajas.Values)
                {
                    if (c.FechaDeInicio >= filtro.FechaInicial && c.FechaDeFinalizacion <= filtro.FechaFinal) l.Add(c);
                }
            }
            else if (filtro.esEntreCajas)
            {
                foreach (gcCaja c in _cajas.Values)
                {
                    if (c.Caja >= filtro.CajaInicial && c.Caja <= filtro.CajaFinal) l.Add(c);
                }
            }
            if (filtro.esTodas)
            {
                l= _cajas.Values.ToList<gcCaja>();
            }
            
            return l;
        }
        public gcCaja UltimaCaja
        {
            get
            {
                return obtenerCaja(_ultimaCaja);
            }
        }
        public bool PuedeCrearMovimientos
        {
            get
            {
                return CajaActual.EsUltimaCaja;
            }
        }
        public bool PuedeCerrarCaja
        {
            get
            {
                return CajaActual.PuedeCerrar;
            }
        }
        
        public List<ProductoResumen> ResumenVentas(List<gcCaja> cajas)
        {
            var l = new List<ProductoResumen>();

            return l;
        }
        public List<ResumenBase> ResumenVentasPorCats(List<gcCaja> cajas)
        {
            var l = new List<ResumenBase>();
            if (cajas == null) return l;
            Hashtable princ = new Hashtable();
            foreach (gcCaja c in cajas)
            {
                foreach (gcMovimiento m in c.Movimientos)
                {
                    if (m.Tipo == gcMovimiento.TipoMovEnum.Venta 
                        || m.Tipo ==gcMovimiento.TipoMovEnum.Devolucion)
                    {
                        foreach (gcObjetoMovido om in m.ObjetosMovidos)
                        {
                            gcCategoria cp=om.Objeto.CategoriaPrincipal;
                            if (cp != null)
                            {
                                if (!princ.ContainsKey(cp.Id))
                                {
                                    CategoriaResumen newcat = new CategoriaResumen();
                                    newcat.Categoria = cp;
                                    princ.Add(cp.Id, newcat);
                                }
                                ((CategoriaResumen)princ[cp.Id]).addSubObjeto(om, m.Tipo == gcMovimiento.TipoMovEnum.Devolucion);
                            }
                            else
                            {
                                if (!princ.ContainsKey(om.Objeto.Id))
                                {
                                    ProductoResumen newcat = new ProductoResumen();
                                    newcat.Producto = om.Objeto;
                                    princ.Add(om.Objeto.Id, newcat);
                                }
                                decimal monto = om.TotalCotizado;
                                decimal cant = om.Cantidad;
                                if (m.Tipo == gcMovimiento.TipoMovEnum.Devolucion)
                                {
                                    monto = -monto;
                                    cant = -cant;
                                }
                                ((ProductoResumen)princ[om.Objeto.Id]).Cantidad += cant;
                                ((ProductoResumen)princ[om.Objeto.Id]).Total += monto;
                               
                            }
                           

                        }
                    }
                    else if (m.Tipo == gcMovimiento.TipoMovEnum.Devolucion)
                    {
                        foreach (gcObjetoMovido om in m.ObjetosMovidos)
                        {

                        }
                    }
                }
            }

            return princ.Values.OfType<ResumenBase>().ToList(); ;
        }
        public gcCaja CajaActual
        {
            get
            {
                return obtenerCaja(_pcajaActual);
            }
           
        }
        public bool CambiarACaja(int nuevaCaja)
        {
            if (nuevaCaja == CajaActual.Caja) return true;
            
             if (_cajas.ContainsKey(nuevaCaja))
            {
                 var c=(gcCaja) _cajas[nuevaCaja];
                 _pcajaActual = c.Caja;
                 CoreManager.Singlenton.seleccionarCaja(c);
                logger.Singlenton.addMessage("Se estableció la caja a:" + nuevaCaja);
                return true;
            }
             logger.Singlenton.addWarningMsg("La caja " + nuevaCaja+" no es válida");
             return false;
        }
        public bool IniciarCaja()
        {
            //reloadPagos();
            return CambiarACaja(UltimaCaja.Caja);
            
        }
        public List<tuplaPagos> SeguimientoDestinatario(gcDestinatario d,List<gcCaja> cajas)
        {
           
            return MovimientosEnCuentaCorrDestinatario(d,cajas).Pagos;
            
        }
        public System.Collections.ICollection Cajas
        {
            get
            {
                //if (_cajas == null)
                //{
                //    System.Collections.Generic.List<int> l = new System.Collections.Generic.List<int>();
                //    DataTable dat = gManager.Connector.Singlenton.ConexionActiva.Consulta("select caja from gcmovimientos group by caja");
                //    foreach (DataRow row in dat.Rows)
                //    {
                //        l.Add((int)row["caja"]);
                //    }
                //    _cajas = l;
                //    if (_cajas.Count == 0) _cajas.Add(0);
                //}

                return _cajas.Values;
            }
        }
        public List<CuentaCorrienteResumen> MovimientosEnCuentasCorrientes(List<gcCaja> cajas,gcDestinatario destinatario=null)
        {
           
                List<CuentaCorrienteResumen> l = new List<CuentaCorrienteResumen>();
                if (cajas == null) return l;
                Hashtable desti = new Hashtable();
              
                foreach (gcCaja caja in cajas)
                {
                    foreach (gcMovimiento m in caja.Movimientos)
                    {
                        if (m.EsMovimientoMercaSolo || m.EsPresupuestooPedido) continue;
                        if (destinatario != null && destinatario.Id != m.Destinatario.Id) continue;
                       
                        foreach (gcPago p in m.Pagos)
                        {
                            if (p.Tipo == gcPago.TipoPago.Cuenta)
                            {
                                CuentaCorrienteResumen cu = (CuentaCorrienteResumen)desti[p.Parent];
                                if (cu == null)
                                {
                                    cu = new CuentaCorrienteResumen(DestinatarioManager.Singlenton.getDestinatario(p.Parent));
                                    desti.Add(p.Parent, cu);
                                    l.Add(cu);
                                }
                                cu.addPago(p,m);
                            }

                        }
                    }
                   
                }
          

                return l;
           
        }
        public CuentaCorrienteResumen MovimientosEnCuentaCorrDestinatario(gcDestinatario d,List<gcCaja> cajas)
        {
            var cuentas = CajaManager.Singlenton.MovimientosEnCuentasCorrientes(cajas, d);
            foreach (CuentaCorrienteResumen c in cuentas)
                return c;
            return new CuentaCorrienteResumen(d);
        }
        //resumenes
        //public class ProductoResumen
        //{
        //    List<ProductoResumen> _sub = new List<ProductoResumen>();
        //    public gcObjeto Producto { get; set; }
        //    public decimal Monto { get; set; }
        //    public decimal Cantidad { get; set; }
        //    public List<ProductoResumen> subProductos { get { return _sub; } } 
        //}
      
       
       
        
        
    }
    
    
}
