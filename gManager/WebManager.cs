﻿using gManager.html;
using Ionic.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace gManager
{
    public class WebManager
    {
        private static WebManager _instance;
        private readonly HttpClient client = new HttpClient();
        private string _host = "localhost";
        private string _subhost = "/sgapi/";
        private string _negocio = "";
        private bool _cancelated = false;
        private bool _logged;
        private htmlObject _home = new html.htmlPantallaPrincipal();
        private WebManager() {
            //descomentar cuando este en internet
            if (!gLogger.logger.Singlenton.showDebug)
            {
                _host = "mascocitas.com";
            }
            _host = "mascocitas.com";
        }
        public static WebManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new WebManager();
                return _instance;
            }
        }
        public event EventHandler Registrado;
        public event EventHandler<string> atHome;
        public EventHandler ProductosListados { get; set; }
        public bool PuedeEfectuarConsultas
        {
            get
            {
                return !String.IsNullOrEmpty(_negocio) && _logged;
            }
        }
        public string Server { get { return "http://" + _host + _subhost; } }
        public bool testInternet()
        {
            return false;
            try
            {
                Ping myPing = new Ping();
                String host = _host;
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                bool c = (reply.Status == IPStatus.Success);
                if (!c) {
                    gLogger.logger.Singlenton.addWarningMsg("No se estableció la conexión con el servidor.\n" + reply.Status.ToString());
                }
                return c;
            }
            catch (Exception)
            {
                gLogger.logger.Singlenton.addWarningMsg("No se estableció la conexión con el servidor");
                return false;
            }
        }
        
        public NameValueCollection EncodeCredentials(Credentials credentials)
        {
           /* var strCredentials = string.Format("{0}:{1}:{2}", credentials.UserName, credentials.Password, credentials.Token);
            string encodedCredentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(strCredentials));*/
            NameValueCollection nvc = new NameValueCollection();
            if (String.IsNullOrWhiteSpace(credentials.ServerToken))
            {
                nvc.Add("token", credentials.ServerToken);
            }
            
            return nvc;
           
        }
       
        private Task<HttpResponseMessage> CallMethod(string method, string action, FormUrlEncodedContent content = null )
        {
            switch(method.ToLower())
            {
                case "post":
                    return client.PostAsync(Server + action, content);
                case "delete":
                    return client.DeleteAsync(Server + action);
                case "put":
                    return client.PutAsync(Server + action, content);
                default:
                    return client.GetAsync(Server + action,HttpCompletionOption.ResponseContentRead);
            }
        }
        private async Task CallAction(string method, string action, Dictionary<string, string> values, EventHandler<string> success, EventHandler<string> error = null)
        {
            if (!String.IsNullOrEmpty(action) && !action.EndsWith("login") && !PuedeEfectuarConsultas)
            {
                gLogger.logger.Singlenton.addErrorMsg("No puede realizar consultas hasta que inicie sesión con el nombre de un negocio.");
                return;
                
            }

            client.DefaultRequestHeaders.Clear();
            if(!action.EndsWith("login"))client.DefaultRequestHeaders.Add("token", KeyManager.Singlenton.Key.ServerToken);

            var content = values != null ? new FormUrlEncodedContent(values) :  null ;
            var response = await CallMethod(method, action, content);
            
            var str = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                success?.Invoke(response, str);
            }
            else
            {
                if(error != null)
                {
                    error(response, str);
                } else
                {
                    gLogger.logger.Singlenton.addWarningMsg(str);
                }
               
                
            }
        }
        public async Task Post(string action, Dictionary<string, string> values, EventHandler<string> success, EventHandler<string> error = null)
        {
            await CallAction("post", action, values, success, error);
        }
        public async Task Get(string action, EventHandler<string> success, EventHandler<string> error = null)
        {
            await CallAction("get", action, null, success, error);
        }
        public async Task LoginServerAsync(bool pedirlogin, bool reiniciar)
        {
            if (pedirlogin && !KeyManager.Singlenton.Login()) return;
            var credentials = new Credentials(KeyManager.Singlenton.Key);
             await Post("users/login",
                    new Dictionary<string, string>
                    {
                       { "email", credentials.UserName },
                       { "key", credentials.Token },
                       { "pass", credentials.Password }
                    }, (resp, str) =>
                    {
                        var kr = JsonConvert.DeserializeObject<KeyResponse>(str);
                        var k = KeyManager.Singlenton.Key;
                        if(kr == null)
                        {
                            gLogger.logger.Singlenton.addErrorMsg("El servidor devolvio un mensaje incorrecto. Por favor vuelva a intentarlo:" + str);
                        }
                        else if (kr.expire < k.Tiempo)
                        {
                            gLogger.logger.Singlenton.addWarningMsg("La cuenta " + k.Usuario + " se quedo sin tiempo de administración.");
                            KeyManager.Singlenton.saveKey(kr);
                            
                        }
                        else
                        {
                            Registrado?.Invoke(kr, null);
                            ///solo reinicia si no tiene accion final
                            if (reiniciar) CoreManager.Singlenton.reiniciar();
                            _negocio = kr.negocio;
                            _logged = true;
                            //aqui sincroniza
                            ProductoManager.Singlenton.updateSincronizados();


                        }
                    }
                );
            
           
        }
        public async Task SincronizarProducto(gcObjeto prod, EventHandler<gcObjeto> done = null)
        {
            if (!prod.SePuedeSincronizar(true, true)) return;

            await Post(_negocio + "/product", prod.toDictionary(), (r, s) =>
            {
                prod.Sincronizado = true;
                var o = JsonConvert.DeserializeObject<gcObjetoGenerico>(s); 

                // tiene imagen y no esta sincronizada
                if(String.IsNullOrEmpty(o.image) && !String.IsNullOrEmpty(prod.ImagenPath))
                {// existe en local pero no en servidor
                    UploadImagenProducto(prod, (old1, old2) =>
                    {
                        done?.Invoke(prod, o);
                    });
                   
                }
                else if (!String.IsNullOrEmpty(o.image) && (String.IsNullOrEmpty(prod.ImagenPath) ||String.IsNullOrEmpty(prod.image) || prod.image != o.image))
                {// existe en servidor pero no en local
                    DownloadFile(_negocio + "/images/" + o.image, ImageManager.getPath(prod,"jpg"), "descargar imagen de " + prod.Nombre, (oo, ee) => { done?.Invoke(o, prod); });
                }
                else
                {
                    done?.Invoke(prod, o);
                }
                
            },

            (r, s) =>
            {
                var res = r as HttpResponseMessage;
                if (res != null)
                {
                    switch (res.StatusCode)
                    {
                        case HttpStatusCode.BadRequest:
                            gLogger.logger.Singlenton.addWarningMsg(s);
                            prod.Sincronizado = true; //bad reques sincroniza igual;
                            break;

                    }
                }

            }
            );
        }
        public void UploadImagenProducto(gcObjeto prod, EventHandler<gcObjeto> done = null)
        {
            UploadFile(
                        _negocio + "/prodimg", prod.ImagenPath,
                        new NameValueCollection()
                        {
                             { "Content-Type", "application/jpeg" },
                            { "token", KeyManager.Singlenton.Key.ServerToken },
                            {"codigo", prod.Codigo }
                        },
                        "subir imagen de " + prod.Nombre,
                        (oo, result) => {
                            try
                            {
                                var x = JsonConvert.DeserializeObject<gcObjetoGenerico>(result);
                                prod.image = x.image;
                                prod.SaveMe();
                            }
                            catch (Exception e)
                            {
                                gLogger.logger.Singlenton.addWarningMsg(e.Message);
                            }

                            done?.Invoke(prod, prod);
                        },
                         (oo, result) => {
                             gLogger.logger.Singlenton.addWarningMsg("No puede subir la imagen de " + prod.Nombre);
                             done?.Invoke(prod, prod);
                         });
        }
        public async Task DesincronizarProducto(gcObjeto prod, EventHandler<gcObjeto> done = null)
        {
            if (String.IsNullOrEmpty(prod.Codigo.Trim()))
            {
                gLogger.logger.Singlenton.addWarningMsg("No puede desincronizar " + prod.Nombre + " porque no tiene codigo");
                return;
            }
            await Post(_negocio + "/remprod", new Dictionary<string, string>() { { "Codigo", prod.Codigo } }, (r, s) =>
            {
                prod.Sincronizado = false;
                var o = s;
                done?.Invoke(o, prod);
            });
        }
        public async Task SincronizarProductos(Queue<gcObjeto> lista, int total, bool desync = false)
        {
            if (!_cancelated && lista.Count > 0 && total > 0)
            {
                var p = lista.Dequeue();
                if (desync)
                {
                    await p.Desincronizar();
                } else
                {
                    await p.Sincronizar();
                }
                int curr = 100 - (lista.Count * 100 / total);
                CoreManager.Singlenton.InformarProgreso(curr, desync ? "Eliminando del servidor a " : "Agregando al servidor a " + p.Nombre);
                await SincronizarProductos(lista,total, desync);
                
            }
        }
        public async Task getProductos(EventHandler<List<gcObjetoGenerico>> done, bool resumido = false)
        {
            string show = resumido ? "" : "all";
            await Get(_negocio + "/products?show=" + show, (r, s) =>
            {
                List<gcObjetoGenerico> l = new List<gcObjetoGenerico>();
                try
                {
                    l = JsonConvert.DeserializeObject<List<gcObjetoGenerico>>(s);
                    done?.Invoke(null, l);
                }
                catch (Exception ex)
                {
                    gLogger.logger.Singlenton.addErrorMsg(ex.Message);
                }
            });
        }
        public void setLocalHome(htmlObject local)
        {
            _home = local;
        }
        public void goHome()
        {
            var local = "";
            if (_home != null)
            {
                local = _home.ToString();
            }
            atHome?.Invoke(null, local);
            if(testInternet())
            {
                var x = Get("", atHome);
            }
           

        }
        public void HttpUploadFile(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
           
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
           // wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
            wr.Headers.Add(EncodeCredentials(new Credentials(KeyManager.Singlenton.Key)));

            Stream rs = wr.GetRequestStream();

            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                //harcodeado
                wr.Headers.Add(key, nvc[key]);
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            try
            {
                wresp = wr.GetResponse();
                /*Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);*/
                string Result = "";
                using (var streamReader = new StreamReader(wresp.GetResponseStream()))
                    Result = streamReader.ReadToEnd();
               
            }
            catch (WebException ex)
            {
                wresp = (HttpWebResponse)ex.Response;
                 string Result="";
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                   Result= streamReader.ReadToEnd();
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
            }
            finally
            {
                wr = null;
            }
        }
        public void SincronizarTodo()
        {
            NameValueCollection nvc = new NameValueCollection();
           // nvc.Add("producto", o.Codigo);
            if (testInternet())
            {
                WebManager.Singlenton.HttpUploadFile(Server + "uploads", CoreManager.Singlenton.Syncdb.Path, "file", "application/x-gzip", nvc);
            }
            
           
        }
        public void UploadFile(string action, string filepath, NameValueCollection headers, string title= "carga de archivos al servidor", EventHandler<string> done = null, EventHandler<string> error = null)
        {
            CoreManager.Singlenton.InformarProgreso(0, "iniciando " + title);
            System.Net.WebClient Client = new System.Net.WebClient();
            Client.UploadProgressChanged += (o,e) => {
                CoreManager.Singlenton.InformarProgreso(e.ProgressPercentage, title + "...");
            };
            Client.Headers.Add(headers);
            Client.UploadFileAsync(new Uri(Server + action), "POST", filepath);
            Client.UploadFileCompleted += (o, e) =>
            {
                if(e.Error == null)
                {
                    var s = System.Text.Encoding.UTF8.GetString(e.Result);
                    done?.Invoke(o, s);
                } else
                {
                    error?.Invoke(o, e.Error.Message);
                }
                
                CoreManager.Singlenton.InformarProgreso(100, "terminado " + title);
            };
        }
        public void DownloadFile(string action, string filepath, string title = "carga de archivos al servidor", EventHandler finished = null)
        {
            CoreManager.Singlenton.InformarProgreso(0, "iniciando " + title);
            WebClient Client = new WebClient();
            Client.DownloadProgressChanged += (o, e) => {
                CoreManager.Singlenton.InformarProgreso(e.ProgressPercentage, title + "...");
            };
            Client.Headers.Add("Content-Type", "application/x-gzip");
            Client.Headers.Add(EncodeCredentials(new Credentials(KeyManager.Singlenton.Key)));
            Client.DownloadFileAsync(new Uri(Server + action), filepath);
            Client.DownloadFileCompleted += (o, e) =>
            {
                finished?.Invoke(o, e);
                CoreManager.Singlenton.InformarProgreso(100, "terminado " + title);
            };
        }



    }
}
