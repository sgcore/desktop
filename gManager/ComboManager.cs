﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gManager
{
    internal class ComboManager
    {
        private static ComboManager _instance;
        private Hashtable _combos = new Hashtable();
        private ComboManager() { }
        public static ComboManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new ComboManager();
                return _instance;
            }
        }
        public void reload(System.ComponentModel.BackgroundWorker worker, DataTable dat, ref int cont, int totalcount)
        {
            _combos.Clear();
            
            foreach (DataRow row in dat.Rows)
            {
                gcCombo combo = new gcCombo(row);
                _combos.Add(combo.Id, combo);

                //prograss
                cont++;
                if (worker != null)
                {
                    worker.ReportProgress((cont * 100 / totalcount) - 1);
                }

            }
        }
        public List<gcCombo> Combos
        {
            get
            {
                return _combos.Values.Cast<gcCombo>().ToList();
            }
        }

        public List<gcCombo> ApplycableCombos(gcMovimiento m)
        {
            List<gcCombo> list = new List<gcCombo>();

            return list;
        }
    }
}
