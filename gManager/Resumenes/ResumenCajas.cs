﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager
{
    public class ResumenCajas
    {
        List<gcCaja> _cajas = new List<gcCaja>();
        List<gcMovimiento> _movimientos = new List<gcMovimiento>();
        List<ResumenBase> _ventas = null;
        public ResumenCajas(List<gcCaja> lista)
        {
            _cajas = lista;
            Efectivo = new gcPago();
            EfectivoEnExtracciones = new gcPago();
            EfectivoGastadoEnCompras = new gcPago();
            EfectivoObtenidoEnDepositos = new gcPago();
            EfectivoObtenidoEnVentas = new gcPago();
            EfectivoEnDepositoInicial = new gcPago();
            EfectivoEnExtraccionFinal = new gcPago();
            PagosCuentasCLientes = new gcPago();
            DineroEnCuentasProveedor = new gcPago();
            DineroEnCuentasResponsables = new gcPago();
            SiguienteDepositoInicial = new gcPago();
            Recaudacion = new gcPago();
            DineroEnTarjetas = new gcPago();
            if (_cajas != null)
            {
                foreach (gcCaja c in _cajas)
                {
                    Efectivo +=c.Efectivo;
                    EfectivoEnExtracciones +=c.EfectivoEnExtracciones;
                    EfectivoGastadoEnCompras +=c.EfectivoGastadoEnCompras;
                    EfectivoObtenidoEnDepositos +=c.EfectivoObtenidoEnDepositos;
                    EfectivoObtenidoEnVentas+=c.EfectivoObtenidoEnVentas;
                    EfectivoEnDepositoInicial +=c.EfectivoEnDepositoInicial;
                    EfectivoEnExtraccionFinal +=c.EfectivoEnExtraccionFinal;
                    EfectivoPerdidoEnDescuentos +=c.EfectivoPerdidoEnDescuentos;
                    PagosCuentasCLientes +=c.PagosCuentasCLientes;
                    SiguienteDepositoInicial += c.SiguienteDepositoInicial;
                    DineroEnCuentasProveedor +=c.DineroEnCuentasProveedor;
                   DineroEnCuentasResponsables+= c.DineroEnCuentasResponsables;
                   Recaudacion += c.EfectivoRecaudacion;
                   DineroEnTarjetas +=c.DineroEnTarjetas;
                   _movimientos.AddRange(c.Movimientos);
                }
            }
        }
        public int CajasCount
        {
            get
            {
                return _cajas != null ? _cajas.Count : 0;
            }
        }
        public string descripcion
        {
            get
            {
                if (_cajas == null || _cajas.Count==0)
                    return "Sin cajas";
                if(_cajas.Count ==1)
                {
                    return "Caja " + _cajas[0].Caja;
                }
                if (_cajas.Count == (Math.Abs(_cajas[0].Caja - _cajas[_cajas.Count - 1].Caja)))
                {
                    return "Desde caja " + _cajas[_cajas.Count - 1].Caja + " hasta " + _cajas[0].Caja;
                }
                return "Multiples cajas entre " + _cajas[_cajas.Count - 1].Caja + " y " + _cajas[0].Caja;
            }
        }
        public List<gcMovimiento> Movimientos { get { return _movimientos; } }
        public gcPago Efectivo { get; set; }
        public gcPago EfectivoEnExtracciones { get; set; }
        public gcPago EfectivoGastadoEnCompras { get; set; }
        public gcPago EfectivoObtenidoEnDepositos { get; set; }
        public gcPago EfectivoObtenidoEnVentas { get; set; }
        public gcPago EfectivoTotalEntrante { get { return EfectivoObtenidoEnVentas + EfectivoObtenidoEnDepositos + EfectivoEnDepositoInicial; } }
        public gcPago EfectivoTotalSaliente { get { return EfectivoEnExtracciones + EfectivoGastadoEnCompras; } }
        public gcPago EfectivoEnDepositoInicial { get; set; }
        public gcPago EfectivoEnExtraccionFinal { get; set; }
        public gcPago EfectivoPerdidoEnDescuentos { get; set; }
        public gcPago PagosCuentasCLientes { get; set; }
        public gcPago DineroEnCuentasProveedor { get; set; }
        public gcPago DineroEnCuentasResponsables { get; set; }
        public gcPago DineroEnTarjetas { get; set; }
        public List<gcCaja> Cajas
        {
            get { return _cajas != null ? _cajas : new List<gcCaja>(); }
        }
        public List<ResumenBase> VentasPorCategorias
        {
            get
            {
                if (_ventas == null) _ventas = CajaManager.Singlenton.ResumenVentasPorCats(_cajas); return _ventas;
            }
        }

        public gcPago SiguienteDepositoInicial { get; set; }

        public gcPago Recaudacion { get; set; }
    }
}
