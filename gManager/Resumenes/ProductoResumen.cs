﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager
{
    public class ProductoResumen:ResumenBase
    {
        public ProductoResumen() { }
        public ProductoResumen(gcObjetoMovido om) 
        {
            Producto = om.Objeto;
            Cantidad = om.Cantidad;
            Total = om.TotalCotizado;
            Movimiento = om.MovimientoObj;
        }
        public gcMovimiento Movimiento { get; set; }
        public gcObjeto Producto { get; set; }
        public override string Nombre
        {
            get { return ProductoDescripcion; }
        }
        public string DescripcionMovimiento
        {
            get { return Movimiento != null ? Movimiento.DescripcionResumen : "No hay un movimiento relacionado"; }
        }
        public DateTime Fecha
        {
            get
            {
                return Movimiento != null ? Movimiento.Fecha : (new DateTime());
            }
        }
               
        public string ProductoDescripcion
        {
            get
            {
                if (Producto == null) return "Desconocido";
                return Producto.Nombre;
            }
        }
        public string ProductoCodigo
        {
            get
            {
                if (Producto == null) return "";
                return Producto.Codigo;
            }
        }
        public override string Codigo
        {
            get { return ProductoCodigo; }
        }
        public override string Serial
        {
            get
            {
                if (Producto == null) return "";
                return Producto.Serial;
            }
        }
        public override string Link
        {
            get
            {
                if (Producto == null) return "";
                return Producto.Link;
            }
        }
    }
}
