﻿namespace gManager
{
    public abstract class ResumenBase
    {
        public decimal Cantidad { get; set; }
        public decimal Saldo { get; set; }
        public decimal Total { get; set; }
        public decimal UnitarioEstimado
        {
            get
            {
                if (Cantidad != 0) return Total / Cantidad;
                return 0;
            }
        }
        public abstract string Nombre { get; }
        public abstract string Codigo { get; }
        public abstract string Serial { get; }
        public abstract string Link { get; }
    }
}
