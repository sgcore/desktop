﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager.Resumenes
{
    public class CuentaCorrienteResumen
    {
        gcDestinatario _dest;
        bool _ordenada = false;
        decimal _total = 0;
        List<tuplaPagos> _list = new List<tuplaPagos>();
        public CuentaCorrienteResumen(gcDestinatario dest)
        {
            _dest = dest;
        }
        public string DescripcionDestinatario { get { return _dest != null ? _dest.Nombre : "desconocido"; } }
        public gcDestinatario Destinatario { get { return _dest; } }
        public List<tuplaPagos> Pagos { get { if (!_ordenada)OrdenarLista(); return _list; } }
        public void addPago(gcPago p, gcMovimiento m)
        {
            var tp = new tuplaPagos();
            tp.Movimiento = m;
            tp.Pago = p;
            tp.Saldo = Total;
            _list.Add(tp);
            _ordenada = false;
        }
        public decimal Total
        {
            get
            {
                if (!_ordenada) OrdenarLista();
                return _total;
            }
        }
        private void OrdenarLista()
        {
            _list = _list.OrderBy(o => o.Movimiento.Id).ToList();
            _total = 0;
            foreach (var tp in _list)
            {
                decimal monto = tp.Pago.Monto;
                if (tp.Movimiento.EsSalidaDinero)
                    monto = monto * -1;
                _total += monto;
                tp.Saldo = _total;

            }
            _ordenada = true;


        }
    }
    public class tuplaPagos
    {
        public gcMovimiento Movimiento;
        public gcPago Pago;
        public gcDestinatario Destinatario;
        public decimal Saldo;
        public string MontoPago
        {
            get
            {
                return Pago.Monto.ToString("$0.00");
            }
        }
        public string MontoSaldo
        {
            get
            {
                return Saldo.ToString("$0.00");
            }
        }
        public string DescripcionMovimiento
        {
            get
            {
                return Movimiento.Numero.ToString("00000") + "-" + Movimiento.DescripcionTipo;
            }
        }
        public string ObservacionesMovimiento
        {
            get
            {
                return Movimiento.Observaciones;
            }
        }
        public string DescripcionFecha
        {
            get
            {
                if (Movimiento != null)
                    return Utils.MostrarHora(Movimiento.Fecha);
                return "no determinada";
            }
        }

        public string DescripcionDestinatario
        {
            get
            {
                if (Destinatario != null) return Destinatario.Nombre;
                return Movimiento.DescripcionDestinatario;
            }
        }
    }
}
