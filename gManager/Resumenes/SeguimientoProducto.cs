﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager.Resumenes
{
    public class SeguimientoProducto
    {
        gcObjeto _obj;
        List<ProductoResumen> _movs = new List<ProductoResumen>();
        public SeguimientoProducto(gcObjeto obj)
        {
            _obj = obj;
        }
        public gcObjeto Producto
        {
            get { return _obj; }
        }
        
        public void setCajas(List<gcCaja> cajas)
        {
            if (_obj == null) return;
            if (cajas == null) return;
            _movs.Clear();
            Vendidos = 0;
            Comprados = 0;
            Devueltos = 0;
            Retornados = 0;
            Entrados = 0;
            Salidos = 0;
            stock = 0;
            MontoVendidos = 0;
            MontoComprados = 0;
            MontoDevueltos = 0;
            MontoRetornados = 0;
            List<gcMovimiento> Movis = new List<gcMovimiento>();
            foreach (gcCaja c in cajas)
            {
                Movis.AddRange(c.Movimientos);
            }
            Movis.Sort(delegate(gcMovimiento c1, gcMovimiento c2) { return c1.Fecha.CompareTo(c2.Fecha); });
            foreach (gcMovimiento m in Movis)
                {
                    foreach (gcObjetoMovido om in m.ObjetosMovidos)
                    {
                        if (om.ObjetoID == _obj.Id)
                        {
                            switch (m.Tipo)
                            {
                                case gcMovimiento.TipoMovEnum.Compra:
                                    Comprados += om.Cantidad;
                                    stock += om.Cantidad;
                                    MontoComprados += om.TotalCotizado;
                                    break;
                                case gcMovimiento.TipoMovEnum.Venta:
                                    Vendidos += om.Cantidad;
                                    stock -= om.Cantidad;
                                    MontoVendidos += om.TotalCotizado;
                                    break;
                                case gcMovimiento.TipoMovEnum.Devolucion:
                                    Devueltos += om.Cantidad;
                                    stock += om.Cantidad;
                                    MontoDevueltos += om.TotalCotizado;
                                    break;
                                case gcMovimiento.TipoMovEnum.Retorno :
                                    Retornados += om.Cantidad;
                                    stock -= om.Cantidad;
                                    MontoRetornados += om.TotalCotizado;
                                    break;
                                case gcMovimiento.TipoMovEnum.Entrada :
                                    Entrados += om.Cantidad;
                                    stock += om.Cantidad;
                                    break;
                                case gcMovimiento.TipoMovEnum.Salida:
                                    Salidos += om.Cantidad;
                                    stock -= om.Cantidad;
                                     break;
                            }
                            var pr = new ProductoResumen(om);
                            pr.Saldo=stock;
                            _movs.Add(pr);
                        }
                    }
                }
           
           // _movs.Sort(delegate(ProductoResumen c1, ProductoResumen c2) { return c1.Movimiento.Fecha.CompareTo(c2.Movimiento.Fecha); });
        }
        public decimal Vendidos { get; set; }
        public decimal Comprados { get; set; }
        public decimal Devueltos { get; set; }
        public decimal Retornados { get; set; }
        public decimal Entrados { get; set; }
        public decimal Salidos { get; set; }

        public decimal stock { get; set; }

        public decimal MontoVendidos { get; set; }
        public decimal MontoComprados { get; set; }
        public decimal MontoDevueltos { get; set; }
        public decimal MontoRetornados { get; set; }
        

        public List<ProductoResumen> Resumenes
        {
            get { return _movs; }
        }
    }
}
