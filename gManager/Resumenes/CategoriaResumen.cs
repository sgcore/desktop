﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager
{
    public class CategoriaResumen :ResumenBase
    {
        public gcCategoria Categoria { get; set; }
        
        public string CategoriaDescripcion
        {
            get
            {
                if (Categoria == null) return "Desconocido";
                return Categoria.Nombre;
            }
        }
        public string CategoriaCodigo
        {
            get
            {
                if (Categoria == null) return "";
                return Categoria.Codigo;
            }
        }
        private List<ResumenBase> _subo = new List<ResumenBase>();
        public List<ResumenBase> SubObjetos { get { return _subo; } }
        private List<ResumenBase> _subcat = new List<ResumenBase>();
        public List<ResumenBase> SubCats { get { return _subcat; } }
        public List<ResumenBase> Childrens
        {
            get
            {
                List<gManager.ResumenBase> l = SubCats;
                return l.Concat(SubObjetos).ToList();
            }
        }
        public void addSubObjeto(gcObjeto o)
        {
            //cambio signo
            decimal monto = o.Precio;
            decimal cantidad = o.Stock;

            //insertar
            var lc = o.RamaCategorias;
            if (o.Parent != Categoria.Id)
            {
                foreach (CategoriaResumen cr in SubCats)
                {
                    if (o.PerteneceACategoria(cr.Categoria))
                    {
                        cr.addSubObjeto(o);
                        Total += monto;
                        Cantidad += cantidad;
                        return;
                    }
                }
                //no tiene la sub creamos todas
                var ncat = new CategoriaResumen();
                lc.Reverse();
                int i = lc.IndexOf(Categoria);
                if (i >= 0 && i + 1 < lc.Count)
                {
                    ncat.Categoria = lc[i + 1];
                    SubCats.Add(ncat);
                    //si no es esta hay que sumar
                    Total += monto;
                    Cantidad += cantidad;

                    ncat.addSubObjeto(o);
                    return;
                }

            }

            if (o.Parent == Categoria.Id)
            {
                foreach (ProductoResumen p in SubObjetos)
                {
                    if (p.Producto.Id == o.Id)
                    {
                        p.Cantidad += cantidad;
                        p.Total += monto;
                        Total += monto;
                        Cantidad += cantidad;
                        return;
                    }
                }
                var np = new ProductoResumen();
                np.Total = monto;
                np.Producto = o;
                np.Cantidad = cantidad;
                _subo.Add(np);
                Total += monto;
                Cantidad += cantidad;

            }
            else
            {
                foreach (CategoriaResumen cat in SubCats)
                {

                }
            }
        }
        public void addSubObjeto(gcObjetoMovido om, bool resta)
        {
            //cambio signo
            decimal monto = om.TotalCotizado;
            decimal cantidad = om.Cantidad;
            if (resta) { monto = -monto; cantidad = -cantidad; }

            //insertar
            var lc = om.Objeto.RamaCategorias;
            if (om.Objeto.Parent != Categoria.Id)
            {
                foreach (CategoriaResumen cr in SubCats)
                {
                    if (om.Objeto.PerteneceACategoria(cr.Categoria))
                    {
                        cr.addSubObjeto(om,resta);
                        Total += monto;
                        Cantidad += cantidad;
                        return;
                    }
                }
                //no tiene la sub creamos todas
                var ncat = new CategoriaResumen();
                lc.Reverse();
                int i=lc.IndexOf(Categoria);
                if (i >= 0 && i+1 < lc.Count)
                {
                    ncat.Categoria = lc[i + 1];
                    SubCats.Add(ncat);
                    //si no es esta hay que sumar
                    Total += monto;
                    Cantidad += cantidad;
                    
                    ncat.addSubObjeto(om, resta);
                    return;
                }
               
            }

            if (om.Objeto.Parent == Categoria.Id)
            {
                foreach(ProductoResumen p in SubObjetos){
                    if(p.Producto.Id ==om.Objeto.Id){
                        p.Cantidad +=cantidad;
                        p.Total +=monto;
                        Total += monto;
                        Cantidad += cantidad;
                        return;
                    }
                }
                var np=new ProductoResumen();
                np.Total=monto;
                np.Producto=om.Objeto ;
                np.Cantidad=cantidad;
                _subo.Add(np);
                Total += monto;
                Cantidad += cantidad;

            }
            else
            {
                foreach (CategoriaResumen cat in SubCats)
                {

                }
            }
        }
        public override string Nombre
        {
            get { return CategoriaDescripcion; }
        }
        public override string Codigo
        {
            get { return CategoriaCodigo; }
        }
        public override string Serial
        {
            get
            {
                if (Categoria == null) return "";
                return Categoria.Serial;
            }
        }
        public override string Link
        {
            get
            {
                if (Categoria == null) return "";
                return Categoria.Link;
            }
        }
    }
}
