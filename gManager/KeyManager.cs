﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager
{
    internal class KeyManager
    {

        private KeyManager() { WebManager.Singlenton.Registrado += 
            (o, e) => 
            {
                var kr = o as KeyResponse;
                saveKey(kr);
            }; 
        }
        static KeyManager _instance;
        public static KeyManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new KeyManager();
                return _instance;
            }
        }

        private keyObject getKey()
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.Key)) return new keyObject() { Usuario = "Desconocido" };
            string sk = Utils.Crypto.Decrypt(Properties.Settings.Default.Key,true);
            var k= JsonConvert.DeserializeObject<keyObject>(sk);
            if (k == null) k = new keyObject();
            return k;

        }
        private  keyObject _k;
        public  keyObject Key
        { get{
            if (_k == null) _k=getKey();
            return _k;
            }
        }
        private void saveKey(keyObject k)
        {
            var nk=JsonConvert.SerializeObject(k);
            _k = k;
            
            Properties.Settings.Default.Key = Utils.Crypto.Encrypt(nk,true);
            Properties.Settings.Default.Save();
        }
        public void saveKey(KeyResponse kr)
        {
           
            var k = Key;
            k.ServerToken = kr.token;
            k.Tiempo = kr.expire;
            k.Negocio = kr.negocio;
            k.Terminal = kr.terminal;


            saveKey(k);
        }
        public void resetKey()
        {
            saveKey(new keyObject() { Usuario = "", Password = "", Tiempo = new DateTime(2010,1,1) });
        }
        public bool Login()
        {
            var k = Key
                ;
            Hashtable t = gConnector.Connector.Singlenton.LoginWindows("Ingrese los datos de su cuenta en internet.",true, k.Usuario, k.Password);
            if (t.Count > 0)
            {
                if (t.ContainsKey("Usuario")) k.Usuario = t["Usuario"].ToString();
                if (t.ContainsKey("Pass")) k.Password = t["Pass"].ToString();
                saveKey(k);
                return true;
            }
            return false;
        }
      
        


    }
}
