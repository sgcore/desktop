﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gManager.Containers
{
    public class ContainerMovimiento
    {
        List<gcMovimiento> _movs;
        public ContainerMovimiento(List<gcMovimiento> movs)
        {
            if(movs != null)
            {
                _movs = movs;
            } else
            {
                _movs = new List<gcMovimiento>();
            }
        }
        public List<gcMovimiento> getMovimientosByTipo(gcMovimiento.TipoMovEnum tipo)
        {
            return _movs.Where(m => m.Tipo == tipo).ToList<gcMovimiento>();
        }
        public List<gcMovimiento> Todos
        {
            get
            {
                return _movs;
            }
        }
        public List<KeyValuePair<string,ContainerMovimiento>> Coleccion
        {
            get
            {
                List<KeyValuePair<string, ContainerMovimiento>> list = new List<KeyValuePair<string, ContainerMovimiento>>();
                foreach (gcMovimiento.TipoMovEnum r in Enum.GetValues(typeof(gcMovimiento.TipoMovEnum)))
                {
                    var l = getMovimientosByTipo(r);
                    if (l.Count > 0)
                    {
                        list.Add(new KeyValuePair<string, ContainerMovimiento>(r.ToString(), new ContainerMovimiento(l)));
                    }


                }
                return list;
            }
        }
       
        public List<gcMovimiento> Ventas
        {
            get
            {
                return getMovimientosByTipo(gcMovimiento.TipoMovEnum.Venta);
            }
        }
        public List<gcMovimiento> Compras
        {
            get
            {
                return getMovimientosByTipo(gcMovimiento.TipoMovEnum.Compra);
            }
        }

    }
}
