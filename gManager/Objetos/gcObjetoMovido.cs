﻿using System;
using System.Runtime.Serialization;

namespace gManager
{
    [DataContract]
    public class gcObjetoMovido
    {
        private gcObjeto _obj;
        private int _idobjt;
        private decimal _cant = 1;
        private decimal _monto;
        private bool _fraccionado = false;
        private bool _alcosto;
        private gcMovimiento _movi;

        //JSON
        [DataMember]
        public int id { get { return Id; } }
        [DataMember]
        public decimal count { get { return Cantidad; } }
        [DataMember]
        public decimal price { get { return Monto ; } }
        [DataMember]
        public string name { get { return Objeto.Nombre; } }



        public enum MonedaEnum{Peso,Dolar,Euro,Real}
        public gcObjetoMovido(gcObjeto o,decimal cant=1,bool usarcosto=false,decimal cotizac=1)
        {
            Objeto = o;
            Observaciones = "";
            if (Objeto != null)
            {
                if(usarcosto)
                {
                    Monto = cotizac > 1 ? Objeto.Dolar : Objeto.Costo;
                }
                else
                {
                    Monto = cotizac > 1 ? Objeto.PrecioDolar : Objeto.Precio;
                }
               
            }
            Cotizacion = cotizac;
            _cant = cant;
            Moneda = MonedaEnum.Peso;
           
            
        }
        public gcObjetoMovido(System.Data.DataRow row)
        {
            setFromRow(row);
            
        }
        public void setFromRow(System.Data.DataRow row)
        {
            if (row["id"] != DBNull.Value) Id = int.Parse(row["id"].ToString());
            if (row["Movimiento"] != DBNull.Value) Movimiento = (int)row["Movimiento"];
            if (row["cantidad"] != DBNull.Value) _cant = decimal.Parse(row["cantidad"].ToString());
            if (row["Objeto"] != DBNull.Value) _idobjt = ((int)row["Objeto"]);
            if (row["monto"] != DBNull.Value) Monto = decimal.Parse(row["Monto"].ToString());
            if (row["Observaciones"] != DBNull.Value) Observaciones = (string)row["Observaciones"];
            if (row["Parent"] != DBNull.Value) Parent = (int)row["Parent"];
            if (row["Moneda"] != DBNull.Value) Moneda = (MonedaEnum)row["Moneda"];
            if (row["Cotizacion"] != DBNull.Value) Cotizacion = decimal.Parse(row["Cotizacion"].ToString());
        }
        public int Id { get; set; }
        public int Movimiento { get; set; }
        public gcMovimiento MovimientoObj { get { if (_movi == null)_movi = MovimientoManager.Singlenton.getMovimiento(Movimiento); return _movi; } }
        public gcObjeto Objeto
        {
            get { if (_obj == null && _idobjt > 0)Objeto = ProductoManager.Singlenton.getObjeto(_idobjt); return _obj; }
            set
            {
                _obj = value;
                if (_obj != null)
                {
                    _idobjt = _obj.Id;

                    //if (AlCosto)
                    //{
                    //    Monto = _obj.Costo;
                    //}
                    //else { Monto = _obj.Precio; }
                }
                else _idobjt = 0;
            }
        }
        public void saveMe()
        {
           Id= MovimientoManager.Singlenton.guardarObjetoMovidoEnDB(this).Id;
        }
        public decimal Cantidad
        {
            get { return _cant; }
            //set 
            //{ 
            //    _cant = value; 
            //    if (_cant < 0)
            //        _cant = 0; 
            //    //anula las fracciones
            //    Fracciones = 1;
            //    CantidadFracciones = 1;
            //}
        }
        public string CantidadProducto
        {
            get
            {
                
                return Utils.UnidadMedida(Objeto.Metrica, _cant);
            }
        }
        public string NombreProducto
        {
            get
            {
                if (Objeto != null) return Objeto.Nombre;
                return "Desconocido";
            }
        }
        public string CodigoProducto
        {
            get
            {
                if (Objeto != null) return Objeto.Codigo;
                return "Desconocido";
            }
        }
        /// <summary>
        /// Porcentaje de impuesto del producto
        /// ej: %21
        /// </summary>
        public decimal IVAProducto
        {
            get
            {
                if (Objeto != null) return Objeto.Iva;
                return 0;
            }
        }
        /// <summary>
        /// Es la cotización para aplicar
        /// ej: el %21 es 1.21;
        /// </summary>
        public decimal IVACotizacion
        {
            get
            {
                return 1 + (IVAProducto / 100);
            }
        }
        public int ObjetoID
        {
            get
            {
                return _idobjt;
            }
            
        }
        public void incrementar(decimal i = 1)
        {
            if (MovimientoObj != null && MovimientoObj.EsDevolucionORetorno)
            {
                gLogger.logger.Singlenton.addWarningMsg("No se puede modificar la cantidad. Debe agregar el producto con la cantidad deseada.");
                return;
            }
            if (_cant + i <= 0)
            {
                gLogger.logger.Singlenton.addWarningMsg("La cantidad no puede ser 0");
                return;
            }
            _cant += i;
            if (MovimientoObj != null)
            {
                if (MovimientoObj.EsSalidaMercaderia) i = -i;
                if (MovimientoObj.EsMovimientoMercaderia && !MovimientoObj.EsPresupuestooPedido) MovimientoManager.Singlenton.updateStock(Objeto, i);
                saveMe();
            }

          
        }
        /// <summary>
        /// Cotizacion de la moneda extrangera
        /// </summary>
        public decimal Cotizacion { get; set; }
        /// <summary>
        /// Moneda extranjera
        /// </summary>
        public MonedaEnum Moneda { get; set; }
        public bool AlCosto
        {
            get { return _alcosto; }
            set
            {
                _alcosto = value;
                if (value)
                {
                    Monto = Objeto.Costo;

                }
            }
        }
        public string DescripcionObjMovi
        {
            get
            {
                if (Objeto == null) return "incompleto";
                return Objeto.Nombre + "\nPrecio: " + Objeto.Precio.ToString("$0.00") + "\nCosto: " + Objeto.Costo.ToString("$0.00") + "\nÚltima Compra: ......." +
                    "\nObservaciones: " + Observaciones;
            }
        }
        public int Parent { get; set; }
        /// <summary>
        /// Monto del Producto
        /// </summary>
        public decimal Monto
        {
            get { return _monto; }
            set 
            {
                _monto = value; 
            }
        }
        /// <summary>
        /// Devuelve el monto sin la cotizacion del iva
        /// </summary>
        public decimal MontoSIVA
        {
            get
            {
                return MontoCotizado / IVACotizacion;
                
            }
        }
        /// <summary>
        /// el monto remanente del iva Monto - MontoSinIVa;
        /// </summary>
        public decimal MontoImpuestoIva
        {
            get
            {
                return Monto - MontoSIVA;
            }
        }
        /// <summary>
        /// Devuelve el monto con la cotizacion de la moneda
        /// </summary>
        public decimal MontoCotizado
        {
            get
            {
                return Monto * Cotizacion;
            }
        }
        /// <summary>
        /// Devuelve el monto sin iva con la cotizacion de moneda
        /// </summary>
        public decimal MontoSIVACot
        {
            get
            {
                return MontoSIVA * Cotizacion;
            }
            
        }

        /// <summary>
        /// Monto total iva incluido
        /// </summary>
        public decimal Total { get { return Monto * Cantidad; } }
        /// <summary>
        /// Monto total sin el iva
        /// </summary>
        public decimal TotalSIVA { get { return TotalCotizado / IVACotizacion; } }
        /// <summary>
        /// Monto Total iva incluido cotizado con moneda
        /// </summary>
        public decimal TotalCotizado { get { return Total * Cotizacion; } }
        /// <summary>
        /// Monto Total sin el iva cotizadon con moneda.
        /// </summary>
        public decimal TotalSIVACot
        {
            get
            {
                return TotalSIVA * Cotizacion;
            }

        }
        /// <summary>
        /// Monto Total del impuesto agregado
        /// </summary>
        public decimal TotalImpuestoIva { get { return TotalCotizado - TotalSIVA; } }
        public string Observaciones { get; set; }
        public string Html
        {
            get
            {
                if (CSSClass == null) CSSClass = "trpar";
                string plantilla = null;// gManager.Utils.ObtenerPlantilla(gManager.Properties.Settings.Default.PlantillaObjetoMovido);

                if (plantilla == null || plantilla == "") plantilla = ("<tr class='{CssClase}'><td>{CantidadObjetoMov}</td><td>{CodigoObjetoMov}</td><td style='text-align: left;'>{NombreObjetoMov}</td><td>{PrecioObjetoMov}</td><td>{TotalObjetoMov}</td></tr>");
                return plantilla.Replace("{CssClase}", CSSClass).Replace("{CantidadObjetoMov}", Cantidad.ToString()).Replace("{CodigoObjetoMov}", Objeto.Codigo).Replace("{NombreObjetoMov}", Objeto.Nombre).Replace("{PrecioObjetoMov}", Monto.ToString("0.00")).Replace("{TotalObjetoMov}", Total.ToString("0.00"));

            }
        }
        public string CSSClass { get; set; }
        public bool Fraccionado
        {
            get { return _fraccionado; }
            set
            {
                _fraccionado = value;
                if (_fraccionado)
                    updateFracciones();
            }
        }
        void updateFracciones()
        {
            decimal a=Math.Truncate(Cantidad);
            decimal b = (Cantidad - a)*1000;
            int arr = arr= (int)(a * 1000 + b);
            int aba = 1000;
          
            var mcd = MaximoComunDivisor(arr, aba);
            arr = arr/mcd;
            aba = aba / mcd;
            Fracciones = aba;
            PrecioFraccion = decimal.Divide(Monto, aba);
            CantidadFracciones = arr;
            if (CantidadFracciones < 1) CantidadFracciones = 1;
        }
        int MaximoComunDivisor(int u, int v)
{
            while (u > 0)
            {
                if (u < v)
                {   // intercambiamos valores
                    var tmp = u;
                    u = v;
                    v = tmp;
                }
                u -= v; // u = u-v por el algoritmo de Euclides
            }

            return v;
        }
        public int Fracciones { get; set; }
        public decimal PrecioFraccion { get; set; }
        public int CantidadFracciones { get; set; }
        public void Fraccionar(int cant, int frac, decimal monto)
        {
            Fraccionado = true;
            if (frac == 0) return;
            Fracciones = frac;
            PrecioFraccion = monto;
            CantidadFracciones = cant;
            Monto = decimal.Multiply((decimal)monto, (decimal)frac); ;
            _cant = decimal.Divide((decimal) cant ,(decimal) frac);

        }

        
        

    }
}
