﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using gLogger;
using gConnector;


namespace gManager
{
    public class gcCaja
    {
        private gcPago _efectivo = new gcPago(0, gcPago.TipoPago.Efectivo);
        private gcPago _tarjeta = new gcPago(0, gcPago.TipoPago.Tarjeta);
        private gcPago _cheque = new gcPago(0, gcPago.TipoPago.Cheque);
        private gcPago _cuenta = new gcPago(0, gcPago.TipoPago.Cuenta);
        private gcMovimiento _primerMovi=null;
        private gcMovimiento _UltimoMovi=null;
        private int _caja;
        private SortedDictionary<int,gcMovimiento> _movimientos = new  SortedDictionary<int,gcMovimiento>();

       
        
        public gcCaja(int caja)
        {
            _caja = caja;

        }
        public int Caja
        {
            get { return _caja; }
        }
        public gcMovimiento PrimerMovimiento { get { return _primerMovi; } }
        public gcMovimiento UltimoMovimiento { get { return _UltimoMovi; } }
        public string CreadaPor
        {
            get
            {
                if (PrimerMovimiento != null)
                    return PrimerMovimiento.DescripcionUsuario;
                return "Desconocido";
            }
        }
        public string CerradaPor
        {
            get
            {
                if (!estaCerrada)
                    return "Nadie aún";
                else
                {
                    return UltimoMovimiento.Usuario.Nombre;
                }
            }
        }
        public string DescripcionFechaInicio
        {
            get
            {
                return FechaDeInicio.ToShortDateString();
            }
        }
        public string DescripcionFechaCierre
        {
            get
            {
                if(estaCerrada)
                    return FechaDeFinalizacion.ToShortDateString();
                return "En curso";
            }
        }
        public decimal DepositoInicial
        {
            get
            {
                if (_primerMovi == null || _primerMovi.Tipo != gcMovimiento.TipoMovEnum.Deposito)
                {
                    return 0;
                }
                return _primerMovi.TotalPagado;
            }
        }
        public decimal ExtraccionFinal
        {
            get
            {
                if (_UltimoMovi == null || _UltimoMovi.Tipo != gcMovimiento.TipoMovEnum.Extraccion || CajaManager.Singlenton.UltimaCaja.Caja<=Caja)
                {
                    return 0;
                }
                return _UltimoMovi.TotalPagado;
            }
        }
        public decimal Produccion
        {
            get
            {
                if (estaCerrada)
                    return ExtraccionFinal;
                return Efectivo.Monto + Tarjeta.Monto + Cheque.Monto;
            }
        }
        public bool estaCerrada
        {
            get
            {
                return Caja < CajaManager.Singlenton.UltimaCaja.Caja ;
            }
        }
        public bool EsUltimaCaja
        {
            get
            {
                return _caja == CajaManager.Singlenton.UltimaCaja.Caja;
            }
        }
        
        public bool PuedeCerrar
        {
            get
            {
                if (!EsUltimaCaja)
                {
                    logger.Singlenton.addErrorMsg("Esta caja no puede terminar.\n No puede realizar el cierre.");
                    return false;
                }
                
                if ((Ventas.Count+Compras.Count) == 0)
                {
                    logger.Singlenton.addErrorMsg("En esta caja aun no se realizaron movimientos que justifiquen el cierre(compras o ventas).\n No puede realizar el cierre.");
                    return false;
                }

                if (!(_efectivo.Monto > 0 || _tarjeta.Monto > 0 || _cheque.Monto > 0))
                {
                    logger.Singlenton.addErrorMsg("No hay ningún movimiento de dinero.\n No puede realizar el cierre.");
                    return false;
                }
                if (!UsuarioManager.Singlenton.PermisoUsuario(gcUsuario.PermisoEnum.Encargado))
                {
                    logger.Singlenton.addErrorMsg("Solo los encargados del sistema pueden cerrar la caja.\n No puede realizar el cierre.");
                    return false;
                }
                else if (UsuarioManager.Singlenton.UsuarioActual.Responsable == null || UsuarioManager.Singlenton.UsuarioActual.Central == null)
                {
                    logger.Singlenton.addErrorMsg("No tiene asignado un responsable o una central en esta cuenta de usuario.\n No puede realizar el cierre.");
                    return false;
                }
                return true;
            }
        }
        public bool cerrarCaja(gcPago final,gcPago inicial)
        {
            if (PuedeCerrar && inicial.Monto <= 0)
            {
                logger.Singlenton.addErrorMsg("El deposito inicial debe ser mayor que cero.\n No puede realizar el cierre.");
                return false;
            }
            //salida
            gcMovimiento extraccion = MovimientoManager.Singlenton.crearNuevoMovimiento(gcMovimiento.TipoMovEnum.Extraccion, UsuarioManager.Singlenton.UsuarioActual.Responsable);
            extraccion.saveMe();
            decimal monto = final.SacarTotal();
            extraccion.addPago(final);
            extraccion.addPagoCuenta(-monto);
            if (extraccion.TerminarOperacion())
            {
                //aqui sube la caja
                var nc = CajaManager.Singlenton.obtenerCaja(_caja + 1);
                CajaManager.Singlenton.CambiarACaja(nc.Caja);
                
                gcMovimiento cajaini = MovimientoManager.Singlenton.crearNuevoMovimiento(gcMovimiento.TipoMovEnum.Deposito, UsuarioManager.Singlenton.UsuarioActual.Responsable);
                cajaini.saveMe();
                cajaini.addPago(inicial);
                cajaini.addPagoCuenta(-inicial.Monto);
                if (cajaini.TerminarOperacion())
                {
                   
                    return true;
                } return false;

            }
            return false;
        }
        public gcPago CajaReal { get; set; }
        
        
        private List<gcMovimiento> getMovimietosTipo(gcMovimiento.TipoMovEnum t)
        {
            List<gcMovimiento> l = new List<gcMovimiento>();
            foreach (gcMovimiento m in _movimientos.Values)
            {
                if (m.Tipo == t)
                {
                    l.Add(m);
                }
            }
            return l;
        }
        public List<gcMovimiento> Ventas
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Venta);
            }
        }
        public List<gcMovimiento> Compras
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Compra);
            }
        }
        public List<gcMovimiento> Depositos
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Deposito);
            }
        }
        public List<gcMovimiento> Extracciones
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Extraccion);
            }
        }
        public List<gcMovimiento> Devoluciones
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Devolucion);
            }
        }
        public List<gcMovimiento> Retornos
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Retorno);
            }
        }
        public List<gcMovimiento> Entradas
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Entrada);
            }
        }
        public List<gcMovimiento> Salidas
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Salida);
            }
        }
        public List<gcMovimiento> Presupuestos
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Presupuesto);
            }
        }
        public List<gcMovimiento> Pedidos
        {
            get
            {
                return getMovimietosTipo(gcMovimiento.TipoMovEnum.Pedido);
            }
        }
        public gcPago EfectivoGastadoEnCompras
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Compras)
                {
                    ret += m.EfectivoPagado;
                }
                return ret;
            }
        }
        public gcPago EfectivoObtenidoEnVentas
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Ventas)
                {
                    ret += m.EfectivoPagado;
                }
                return ret;
            }
        }
        public gcPago EfectivoObtenidoEnDepositos
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Depositos)
                {
                    if (m.Id == _primerMovi.Id)
                    {
                        continue;
                    }
                    ret += m.EfectivoPagado;
                }
                return ret;
            }
        }
        public gcPago EfectivoEnExtracciones
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Extracciones)
                {
                    if (m.Id == _UltimoMovi.Id && estaCerrada)
                    {
                        continue;
                    }
                    ret += m.EfectivoPagado;
                }
                return ret;
            }
        }
        public gcPago EfectivoPerdidoEnDescuentos
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Movimientos)
                {
                    
                    ret += m.PagosEnDescuentos;
                }
                return ret;
            }
        }
        public gcPago PagosCuentasCLientes
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Movimientos)
                {
                    if(m.Tipo==gcMovimiento.TipoMovEnum.Venta
                        &&m.PagosEnCuentas.Monto!=0)
                     ret += m.PagosEnCuentas;
                }
                return ret;
            }
        }
        public gcPago DineroEnCuentasProveedor
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Movimientos)
                {
                    if (m.Destinatario.Tipo == gcDestinatario.DestinatarioTipo.Proveedor)
                        ret += m.PagosEnCuentas;
                }
                return ret;
            }
        }
        public gcPago DineroEnCuentasResponsables
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Movimientos)
                {
                    if (m.Destinatario.Tipo == gcDestinatario.DestinatarioTipo.Responsable)
                        ret += m.PagosEnCuentas;
                }
                return ret;
            }
        }
        public gcPago DineroEnTarjetas
        {
            get
            {
                gcPago ret = new gcPago();
                foreach (gcMovimiento m in Movimientos)
                {
                    ret += m.PagosEnTarjeta;
                }
                return ret;
            }
        }
        public gcPago EfectivoEnDepositoInicial
        {
            get
            {
                if (_primerMovi == null || _primerMovi.Tipo != gcMovimiento.TipoMovEnum.Deposito)
                {
                    return new gcPago();
                }
                return _primerMovi.EfectivoPagado;
            }
        }
        public gcPago EfectivoEnExtraccionFinal
        {
            get
            {
                if (_UltimoMovi == null || _UltimoMovi.Tipo != gcMovimiento.TipoMovEnum.Extraccion || CajaManager.Singlenton.UltimaCaja.Caja <= Caja)
                {
                    return new gcPago();
                }
                return _UltimoMovi.EfectivoPagado;
            }
        }
        public gcPago SiguienteDepositoInicial
        {
            get
            {
                var sig = CajaManager.Singlenton.getCajas(new Filtros.FiltroCaja(Caja + 1));
                if (sig != null && sig.Count > 0 && sig[0]!=null)
                {
                    gcCaja c = sig[0];
                    return c.EfectivoEnDepositoInicial;
                }
                return new gcPago();
            }
        }
        public gcPago EfectivoRecaudacion
        {
            get
            {
                return EfectivoEnExtraccionFinal - SiguienteDepositoInicial;
            }
        }
        public string DescripcionRecaudacion
        {
            get
            {
                return EfectivoEnExtraccionFinal.Monto.ToString("$0.00");
            }
        }
        public string DescripcionInicial
        {
            get
            {
                return EfectivoEnDepositoInicial.Monto.ToString("$0.00");
            }
        }
        public DateTime FechaDeInicio { get { if (_primerMovi != null)return _primerMovi.Fecha; return DateTime.Now; } }
        public DateTime FechaDeFinalizacion { get { if (_UltimoMovi != null)return _UltimoMovi.Fecha; return DateTime.Now; } }
        public bool addMovimiento(gcMovimiento m)
        {
            if (_movimientos.Count == 0) _primerMovi = m;
            if (_UltimoMovi == null)
            {
                _UltimoMovi = m;
            }else if( m.Fecha > _UltimoMovi.Fecha) _UltimoMovi = m;
            
            if (_movimientos.ContainsKey(m.Id)) return false;
            _movimientos.Add(m.Id, m);
            
            return true;
        }
        public bool updateMovimiento(gcMovimiento m)
        {
            if (_movimientos.ContainsKey(m.Id))
            {
                _movimientos[m.id] = m;
                return true;
            }

            return false;
        }
        public bool removeMovimiento(gcMovimiento m)
        {
            if (_movimientos.ContainsKey(m.Id))
            {
                _movimientos.Remove(m.Id);
                return true;
            }
            return false;
        }
        
        public gcPago AddPago(gcMovimiento m, gcPago p, bool guardar = true)
        {

            if (!(m.EsEntradaDinero || m.EsSalidaDinero))
            {
                logger.Singlenton.addErrorMsg("El pago no corresponde al movimiento");
                return p;
            }
            if (m.EsEntradaDinero) addPagoEntrada(p);
            else if (m.EsSalidaDinero) addPagoSalida(p);
            if (guardar)
            {
                logger.Singlenton.addDebugMsg("Caja", "Se agrego el pago " + p.Monto + " " + p.Tipo.ToString() + " a " + m.DescripcionTipo);
                p = guardarPagoEnDB(p);
                

            }
            if (p.Tipo == gcPago.TipoPago.Cuenta)
            {

                DestinatarioManager.Singlenton.destinatarioAddCredito(p, m);

            }
           
            return p;
        }
        public void RemovePago(gcMovimiento m, gcPago p)
        {
            //descontamos al destinatario
            if (p.Tipo == gcPago.TipoPago.Cuenta)
            {

                DestinatarioManager.Singlenton.destinatarioRemoveCredito(p, m);

            }
            bool esEntra = m.Tipo == gcMovimiento.TipoMovEnum.Deposito ||
                m.Tipo == gcMovimiento.TipoMovEnum.Venta ||
                m.Tipo == gcMovimiento.TipoMovEnum.Retorno;
            bool esSalida = m.Tipo == gcMovimiento.TipoMovEnum.Extraccion ||
                m.Tipo == gcMovimiento.TipoMovEnum.Compra ||
                m.Tipo == gcMovimiento.TipoMovEnum.Devolucion;
            if (!(esEntra || esSalida))
            {
                logger.Singlenton.addErrorMsg("ERROR: el pago no corresponde al movimiento");
                return;
            }
            logger.Singlenton.addMessage("Se Elimino el pago " + p.Monto + " " + p.Tipo.ToString() + " a " + m.DescripcionTipo, "INFORME DE CAJA");
            //aqui cambia el orden
            if (esSalida) addPagoEntrada(p);
            else if (esEntra) addPagoSalida(p);
            eliminarPagoDeDB(p);
            CoreManager.Singlenton.EliminarPago(p);

        }
        private void eliminarPagoDeDB(gcPago p)
        {

            string sql = "delete from gcpagos where id=" + p.Id + ";";
            Connector.Singlenton.ConexionActiva.Consulta(sql);


        }
        public gcPago guardarPagoEnDB(gcPago p)
        {
            Hashtable t = new Hashtable();
            
            t.Add("id", p.Id);
            t.Add("Movimiento", p.Movimiento);
            t.Add("Tipo", (int)p.Tipo);
            t.Add("Observaciones", p.Observaciones);
            t.Add("Monto",p.Monto  );
            t.Add("m1000", p.m1000);
            t.Add("m500", p.m500);
            t.Add("m200", p.m200);
            t.Add("m100", p.m100);
            t.Add("m50", p.m50 );
            t.Add("m20", p.m20 );
            t.Add("m10", p.m10 );
            t.Add("m5", p.m5 );
            t.Add("m2", p.m2 );
            t.Add("m1", p.m1 );
            t.Add("m050", p.m050 );
            t.Add("m025", p.m025 );
            t.Add("m010", p.m010 );
            t.Add("m005", p.m005 );
            t.Add("m001", p.m001 );
            t.Add("parent", p.Parent );

            p.Id= Connector.Singlenton.ConexionActiva.GuardarENtity(t, "gcpagos",p.Id>0);
            return p;
           
        }
        public Hashtable getPagos(gcMovimiento m)
        {
            Hashtable _pagos = new Hashtable();
            DataTable dat = Connector.Singlenton.ConexionActiva.Consulta("select * from gcpagos where movimiento=" + m.Id);
            foreach (DataRow row in dat.Rows)
            {
                gcPago p = new gcPago(row);
                _pagos.Add(p.Id, p);
            }

            logger.Singlenton.addDebugMsg("Movimiento" , "Obteniendo " + _pagos.Count + " pagos del movimiento " + m.DescripcionAuto);
            return _pagos;
        }
        public gcMovimiento[] Movimientos
        {
            get
            {
                //gcMovimiento[] l=new gcMovimiento[_movimientos.Values.Count];
                //_movimientos.Values.CopyTo(l, 0);
                return _movimientos.Values.ToArray<gcMovimiento>();
                
            }
        }
        public ICollection Incompletos
        {
            get
            {
                return _movimientos.Where(kvp => !kvp.Value.Terminado).ToList();
               
             
            }
        }
        
        private void addPagoEntrada(gcPago p)
        {
            switch (p.Tipo)
            {
                case gcPago.TipoPago.Efectivo:
                    _efectivo += p;
                    break;
                case gcPago.TipoPago.Vuelto:
                    _efectivo -= p;
                    break;
                case gcPago.TipoPago.Tarjeta:
                    _tarjeta += p;
                    break;
                case gcPago.TipoPago.Cheque:
                    _cheque += p;
                    break;
                case gcPago.TipoPago.Cuenta:
                    _cuenta += p;
                    break;
            }
        }
        private void addPagoSalida(gcPago p)
        {
            switch (p.Tipo)
            {
                case gcPago.TipoPago.Efectivo:
                    _efectivo -= p;
                    break;
                case gcPago.TipoPago.Vuelto:
                    _efectivo += p;
                    break;
                case gcPago.TipoPago.Tarjeta:
                    _tarjeta -= p;
                    break;
                case gcPago.TipoPago.Cheque:
                    _cheque -= p;
                    break;
                case gcPago.TipoPago.Cuenta:
                    _cuenta -= p;
                    break;
            }
        }
        public gcPago Efectivo
        {
            get
            {
                if (!estaCerrada || _UltimoMovi == null || _UltimoMovi.Tipo != gcMovimiento.TipoMovEnum.Extraccion || CajaManager.Singlenton.UltimaCaja.Caja <= Caja)
                {
                    return _efectivo;
                }
                //devuelve el pago en efectivo de la ultima extraccion
                foreach (gcPago p in _UltimoMovi.Pagos)
                    if (p.Tipo == gcPago.TipoPago.Efectivo) return p;
                return _efectivo;
            }
        }
        public gcPago Cuenta
        {
            get
            {
                return _cuenta;
            }
        }
        public gcPago Tarjeta
        {
            get
            {
                return _tarjeta;
            }
        }
        public gcPago Cheque
        {
            get
            {
                return _cheque;
            }
        }
    }
}
