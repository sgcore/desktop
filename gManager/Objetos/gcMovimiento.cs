﻿using gLogger;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace gManager
{
    [DataContract]
    public class gcMovimiento: IComparable<gcMovimiento>
    {
        private gcDestinatario _origen;
        private gcDestinatario _destino;
        private gcUsuario _usuario;
        private int _iddestino = 0;
        private int _idorigen = 0;
        private int _idusuario = 0;
        private string _obs = "";
        private EstadoEnum _estado=EstadoEnum.Creado;
        private gcObjetoMovido.MonedaEnum _moneda = gcObjetoMovido.MonedaEnum.Peso;
        private decimal _cotizacion = 1;
       
        
       
        public enum TipoMovEnum
        {
            Compra, //sale dinero 0
            Retorno, //entra dinero 1
            Venta, //entra dinero 2
            Devolucion, //sale dinero 3
            Salida, //no hay dinero 4
            Entrada, //no hay dinero 5
            Deposito, //entra dinero 6
            Extraccion,//sale dinero 7
            Presupuesto,//no hay dinero 8
            Pedido//no hay dinero 9
            
        }
        public enum EstadoEnum{
            Creado,
            Terminado,
            Modificado
        }
        public enum TipoFacturaEnum
        {
            C,A,R,X
        }
        public TipoFacturaEnum TipoFactura
        {
            get
            {
                if (EsMovimientoMercaSolo || EsPresupuestooPedido) return TipoFacturaEnum.R;
                if (EsMovimientoDinero) return TipoFacturaEnum.X;
                if (Destinatario != null && Destinatario.TipoIva == gcDestinatario.DestinatarioIVA.Responsable_Inscripto) return TipoFacturaEnum.A;
                return TipoFacturaEnum.C;
            }
        }
        public int CompareTo(gcMovimiento m)
        {
            return Fecha.CompareTo(m.Fecha);
        }
       
         public gcMovimiento()
        {
            Id = 0;
            //crear Movimiento
            Observaciones = "Ninguna";
            Fecha = DateTime.Now;
            LastMod = DateTime.Now;
            
            Codigo = Utils.randomGUID(); ;

        }
        public gcMovimiento(System.Data.DataRow row)
        {
            setFromRow(row);
        }
        private void setFromRow(System.Data.DataRow row)
        {
            Id = int.Parse(row["id"].ToString());
            _idorigen = (int)row["Origen"];
            _iddestino = (int)row["Destino"];
            if (DBNull.Value != row["observaciones"]) Observaciones = (string)row["observaciones"];
            if (DBNull.Value != row["caja"]) Caja = (int)row["caja"];
            if (DBNull.Value != row["facnum"]) FacturaNumero = (int)row["facnum"];
            if (DBNull.Value != row["factipo"]) FacturaTipo = (TipoFacturaEnum)row["facnum"];
            Tipo = (TipoMovEnum)row["tipo"];
            _idusuario = (int)row["usuario"];
            Fecha = DateTime.Parse(row["Fecha"].ToString());
            if (DBNull.Value != row["Codigo"])
                Codigo = (string)row["Codigo"];
            else Codigo = Utils.randomGUID();
            LastMod = DateTime.Parse(row["LastMod"].ToString());
            _estado = (EstadoEnum)(int)row["Estado"];
            if (DBNull.Value != row["Numero"]) Numero = (int)row["Numero"];


            
           
           
        }
        
        public int Id { get; set; }
        public int DestinoId
        {
            get
            {
                return _iddestino;
            }
        }
        public int OrigenId
        {
            get
            {
                return _idorigen;
            }
        }
        public int UsuarioId
        {
            get
            {
                return _idusuario;
            }
        }
        public gcDestinatario Origen
        {
            get
            {
                if (_origen == null) _origen = DestinatarioManager.Singlenton.getDestinatario(_idorigen);
                return _origen;
            }
            set { _origen = value;
            if (_origen != null) _idorigen = _origen.Id; else _idorigen = 0;
            }
        }
        public gcDestinatario Destino
        {
            get
            {
                if (_destino == null) _destino = DestinatarioManager.Singlenton.getDestinatario(_iddestino);
                 return _destino ;
            }
            set
            {
                _destino  = value;
                if (_destino  != null) _iddestino  = _destino.Id; else _iddestino = 0;
            }
        }
        public gcUsuario Usuario
        {
            get
            {
                if (_usuario == null) _usuario = UsuarioManager.Singlenton.getUsuario(_idusuario);
                return _usuario ;
            }
            set
            {
                _usuario = value;
                if (_usuario  != null) _idusuario  = _usuario.Id; else _idusuario  = 0;
            }
        }
        public DateTime Fecha { get; set; }
        public DateTime LastMod { get; set; }
        public string Codigo { get; set; }
        public int FacturaNumero { get; set; }
        public TipoFacturaEnum FacturaTipo { get; set; }
        public EstadoEnum Estado { get { return _estado; } }

        //JSON
        [DataMember]
        public int id { get { return Id; } }
        [DataMember]
        public string code { get { return Codigo; } }
        [DataMember]
        public int box { get { return Caja; } }
        [DataMember]
        public int number { get { return Numero; } }
        [DataMember]
        public decimal price { get { return TotalMovimiento; } }
        [DataMember]
        public string type { get { return Tipo.ToString(); } }
        [DataMember]
        public gcDestinatario client { get { return Destinatario; } }
        [DataMember]
        public List<gcObjetoMovido> movitems { get { return ObjetosMovidos.Cast<gcObjetoMovido>().ToList(); } }


        public int Caja { get; set; }
        public int Numero { get; set; }
        public TipoMovEnum Tipo { get; set; }
        public string Observaciones { get { return _obs; } set { _obs = value;} }
        public decimal Monto
        {
            get
            {
                if (!EsMovimientoDinero) return TotalCotizado;
                return TotalPagado;
            }
        }
        public gcDestinatario Destinatario
        {
            get
            {
                switch (Tipo)
                {
                    case gcMovimiento.TipoMovEnum.Compra:
                    case gcMovimiento.TipoMovEnum.Devolucion:
                    case gcMovimiento.TipoMovEnum.Entrada:
                    
                    case gcMovimiento.TipoMovEnum.Deposito:
                    case TipoMovEnum.Pedido:
                        return Origen;

                    case gcMovimiento.TipoMovEnum.Venta:
                    case gcMovimiento.TipoMovEnum.Salida:
                    case gcMovimiento.TipoMovEnum.Retorno:
                    case gcMovimiento.TipoMovEnum.Extraccion:
                    case TipoMovEnum.Presupuesto:
                        return Destino;
                }
                return Origen;
            }

        }
        public bool setDestinatario(gcDestinatario value)
        {
            if (value == null) return false;
            if (Pagos.Count > 0)
            {
                logger.Singlenton.addWarningMsg("no se puede cambiar el destinatario de un movimiento con pagos ");
                return false;
            }
            if ((Tipo == TipoMovEnum.Compra && value.Tipo == gcDestinatario.DestinatarioTipo.Proveedor)
                || (Tipo == TipoMovEnum.Devolucion && value.Tipo == gcDestinatario.DestinatarioTipo.Cliente)
                || (Tipo == TipoMovEnum.Entrada && value.Tipo == gcDestinatario.DestinatarioTipo.Sucursal && (Destino == null || value.Id != Destino.Id))
                || (Tipo == TipoMovEnum.Pedido && value.Tipo == gcDestinatario.DestinatarioTipo.Proveedor)
                || (Tipo == TipoMovEnum.Deposito && value.PuedeDepositar)
                )
            {
                Origen = value;
            }
            else if ((Tipo == TipoMovEnum.Venta && value.Tipo == gcDestinatario.DestinatarioTipo.Cliente)
               || (Tipo == TipoMovEnum.Retorno && value.Tipo == gcDestinatario.DestinatarioTipo.Proveedor)
               || (Tipo == TipoMovEnum.Salida && value.Tipo == gcDestinatario.DestinatarioTipo.Sucursal && (Origen == null || value.Id != Origen.Id))
               || (Tipo == TipoMovEnum.Presupuesto && value.Tipo == gcDestinatario.DestinatarioTipo.Cliente)
               || (Tipo == TipoMovEnum.Extraccion && value.PuedeDepositar)
               )
            {
                Destino = value;
            }
            else
            {
                logger.Singlenton.addErrorMsg("No se puede asignar un destinatario " + value.Tipo.ToString() + "(" + value.Nombre + ") a un movimiento de tipo " + Tipo.ToString());
                return false;
            }


            return true;
        }
        public gcDestinatario Originario
        {
            get
            {
                switch (Tipo)
                {
                    case gcMovimiento.TipoMovEnum.Compra:
                    case gcMovimiento.TipoMovEnum.Devolucion:
                    case gcMovimiento.TipoMovEnum.Entrada:
                    case gcMovimiento.TipoMovEnum.Extraccion:
                    case TipoMovEnum.Pedido:
                        return Destino;

                    case gcMovimiento.TipoMovEnum.Venta:
                    case gcMovimiento.TipoMovEnum.Salida:
                    case gcMovimiento.TipoMovEnum.Retorno:
                    case gcMovimiento.TipoMovEnum.Deposito:
                    case TipoMovEnum.Presupuesto:
                        return Origen;
                }
                return Origen;
            }

        }
        public bool setOriginario(gcDestinatario value)
        {
            if (value == null) return false;
            if (Tipo == TipoMovEnum.Deposito)
            {
                Destino = value;
                return true;
            }
            if (Tipo == TipoMovEnum.Extraccion)
            {
                Origen = value;
                return true;
            }
            if ((EsEntradaMercaderia || EsSalidaDinero) &&value.Tipo==gcDestinatario.DestinatarioTipo.Sucursal )
            {
                Destino = value;
            }
            else if ((EsSalidaMercaderia || EsEntradaDinero || EsPresupuestooPedido)&&value.Tipo==gcDestinatario.DestinatarioTipo.Sucursal)
            {
                Origen = value;
            }
            else
            {
                logger.Singlenton.addErrorMsg("No se puede asignar el Origen a este movimiento por que no hay coherencia.");
                return false;
            }



            return true;

        }
        public bool EsMovimientoDinero
        {
            get
            {
                switch (Tipo)
                {
                    //entrada de dinero

                    case gcMovimiento.TipoMovEnum.Extraccion:
                    case gcMovimiento.TipoMovEnum.Deposito:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public bool EsMovimientoMercaSolo
        {
            get
            {
                switch (Tipo)
                {
                    //entrada de dinero

                    case gcMovimiento.TipoMovEnum.Entrada:
                    case gcMovimiento.TipoMovEnum.Salida:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public bool EsMovimientoMercaderia
        {
            get
            {
                return EsEntradaMercaderia || EsSalidaMercaderia;
            }
        }
        public bool EsPresupuestooPedido
        {
            get
            {
                switch (Tipo)
                {
                    //entrada de dinero

                    case gcMovimiento.TipoMovEnum.Presupuesto:
                    case gcMovimiento.TipoMovEnum.Pedido:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public bool EsEntradaMercaderia
        {
            get { return Tipo == TipoMovEnum.Devolucion || Tipo == TipoMovEnum.Compra || Tipo == TipoMovEnum.Entrada; }
        }
        public bool EsEntradaDinero
        {
            get { return Tipo == TipoMovEnum.Deposito || Tipo == TipoMovEnum.Venta || Tipo == TipoMovEnum.Retorno; }
        }
        public bool EsDevolucionORetorno
        {
            get
            {
                return Tipo == TipoMovEnum.Devolucion || Tipo == TipoMovEnum.Retorno;
            }
        }
        public bool EsIncrementoDeDeuda
        {
            get
            {
                return (EsEntradaDinero && PagosEnCuentas.Monto>0)
                    || (EsSalidaDinero && PagosEnCuentas.Monto<0)
                    
                    ;
            }
        }
        public bool EsSalidaMercaderia
        {
            get { return Tipo == TipoMovEnum.Retorno || Tipo == TipoMovEnum.Venta || Tipo == TipoMovEnum.Salida; }
        }
        public bool EsSalidaDinero
        {
            get { return Tipo == TipoMovEnum.Extraccion || Tipo == TipoMovEnum.Compra || Tipo == TipoMovEnum.Devolucion; }
        }
        public bool EsDepositoOExtraccion
        {
            get { return EsDeposito || EsExtraccion; }
        }
        public bool EsDeposito
        {
            get { return Tipo == TipoMovEnum.Deposito; }
        }
        public bool EsExtraccion
        {
            get { return Tipo == TipoMovEnum.Extraccion; }
        }
        public bool EsConDineroInvolucrado
        {
            get
            {
                return !(EsPresupuestooPedido || EsMovimientoMercaSolo);
            }
        }
        public bool EstaCancelado
        {
            get
            {
                return Math.Round(TotalCotizado,2) == Math.Round(TotalPagado, 2);
            }
        }
        public bool EstaFacturado
        {
            get
            {
                return FacturaNumero > 0;
            }
        }
        public bool EsResponsableInscripto
        {
            get
            {
                return Destinatario != null
                    && (Destinatario.Tipo == gcDestinatario.DestinatarioTipo.Cliente || Destinatario.Tipo == gcDestinatario.DestinatarioTipo.Proveedor)
                    && Destinatario.TipoIva == gcDestinatario.DestinatarioIVA.Responsable_Inscripto;
            }
        }
        public string DescripcionPago
        {
            get
            {

                string dest = "Se recibió de ";
                if (EsSalidaDinero)
                    dest = "Se entregó a ";
                return dest + DescripcionDestinatario + "\n la suma de: \n" + Utils.describirMonto(TotalPagado) + " \nen concepto de:\n" + Observaciones;
            }
        }
        public string DescripcionTotal
        {
            get
            {
                return Utils.describirMonto(TotalPagado);
            }
        }
        public string DescripcionAuto
        {
            get
            {
                if(EsMovimientoMercaSolo)
                    return DescripcionTipo+" "+Numero+" " + " de " + DescripcionOriginario +" a "+DescripcionDestinatario+ " creado por " + DescripcionUsuario;
                else if(EsMovimientoDinero)
                    return DescripcionTipo + " " + Numero + " efectuado por " + DescripcionDestinatario + " creado por " + DescripcionUsuario;
                else
                    return DescripcionTipo + " " + Numero + " efectuado por " + DescripcionDestinatario + " creado por " + DescripcionUsuario;
            }
        }
        public string DescripcionResumen
        {
            get
            {
                if (EsMovimientoDinero)
                    return DescripcionTipo + Numero + " - " + DescripcionDestinatario + " $" + TotalPagado.ToString("0.00");
                else if (EsMovimientoMercaderia)
                {
                    return DescripcionTipo + Numero + " - " + DescripcionDestinatario + " $" + TotalCotizado.ToString("0.00");
                }
                else
                    return DescripcionAuto;
            }
        }
        public string DescripcionTipo
        {
            get
            {
                if (Destinatario == null) return "Tipo Desconocido";
                gcDestinatario.DestinatarioTipo FlagTipoDestina = Destinatario.Tipo;
                if (Tipo == TipoMovEnum.Deposito)
                {
                    switch (FlagTipoDestina)
                    {
                        case gcDestinatario.DestinatarioTipo.Banco:

                            return "Retiro del Banco";
                        case gcDestinatario.DestinatarioTipo.Cliente:
                            return "Pago de Cliente";
                        case gcDestinatario.DestinatarioTipo.Financiera:
                            return "Cobrar a Financiera";
                        case gcDestinatario.DestinatarioTipo.Proveedor:
                            return "Reintegro de Proveedor";
                        case gcDestinatario.DestinatarioTipo.Responsable:
                            return "Deposito de Responsable";
                        default:
                            return Tipo.ToString();
                    }
                }
                else if (Tipo == TipoMovEnum.Extraccion)
                {
                    switch (FlagTipoDestina)
                    {
                        case gcDestinatario.DestinatarioTipo.Banco:
                            return "Depositar en el Banco";
                        case gcDestinatario.DestinatarioTipo.Cliente:
                            return "Reintegro a Cliente";
                        case gcDestinatario.DestinatarioTipo.Financiera:
                            return "Depositar en Financiera";
                        case gcDestinatario.DestinatarioTipo.Proveedor:
                            return "Deposito en Proveedor";
                        case gcDestinatario.DestinatarioTipo.Responsable:
                            return "Retiro de Responsable";
                        default:
                            return Tipo.ToString();
                    }
                }
                return Tipo.ToString();
            }
        }
        public string DescripcionDestinatario
        {
            get
            {
                if (Destinatario != null) return Destinatario.Nombre;
                return "Destino Desconocido";
            }
        }
        public string DescripcionOriginario
        {
            get
            {
                if (Originario != null) return Originario.Nombre;
                return "Origen Desconocido";
            }
        }
        public string DescripcionUsuario
        {
            get
            {
                if (Usuario != null) return Usuario.Nombre;
                return "Usuario Desconocido";
            }
        }
        public gcDestinatario.DestinatarioTipo DestinatarioTipo
        {
            get
            {
                if (Destinatario != null) return Destinatario.Tipo;
                switch (Tipo)
                {
                    case TipoMovEnum.Compra:
                    case TipoMovEnum.Retorno:
                    case TipoMovEnum.Pedido:
                        return gcDestinatario.DestinatarioTipo.Proveedor;
                    case TipoMovEnum.Venta:
                    case TipoMovEnum.Devolucion:
                    case TipoMovEnum.Presupuesto:
                        return gcDestinatario.DestinatarioTipo.Cliente;
                    case TipoMovEnum.Entrada:
                    case TipoMovEnum.Salida:
                        return gcDestinatario.DestinatarioTipo.Sucursal;
                    default:
                        return gcDestinatario.DestinatarioTipo.Responsable;
                }
            }
        }
        //estado
        
        public bool SePuedeDescontar
        {
            get { return Tipo==TipoMovEnum.Venta && DescuentoPorcentual >= 0 && MaximoDescuentoPermitido > 0; }
        }
        public bool SePuedeTerminar
        {
            get
            {
                if (Terminado) return false;
                if (!EsOwnerOrContador) return false;
                //movimiento dinero
                if (EsMovimientoDinero) return  TotalPagado > 0 && Diferencia == 0;
                //movimiento mercaderia
                if(EsMovimientoMercaSolo || EsPresupuestooPedido)return ObjetosMovidos.Count>0;
                if (EsMovimientoMercaderia)
                {
                    foreach (gcObjetoMovido o in ObjetosMovidos)
                        if (o.TotalCotizado == 0) return false;

                }
                

                return ((Diferencia == 0) && ObjetosMovidos.Count > 0);
                    
            }
        }
        public bool SePuedeEditar
        {
            get
            {
                return Terminado && ((EsOwnerOrContador && EsCreadoEnCajaActual) || CoreManager.Singlenton.ElUsuarioSuperaElPermiso(gcUsuario.PermisoEnum.Administrador));
            }
        }
        public bool SePuedeAgregarOEliminarOM
        {
            get
            {
                return !Terminado && EsOwnerOrContador;
            }
        }
        public bool EsOwnerOrContador
        {
            get
            {
                return CoreManager.Singlenton.ElUsuarioSuperaElPermiso(gcUsuario.PermisoEnum.Contador) ||
                    (CoreManager.Singlenton.Usuario != null && Usuario.Id == CoreManager.Singlenton.Usuario.Id && EsCreadoEnCajaActual);
            }
        }
        public bool EsCreadoEnCajaActual
        {
            get
            {
                return Caja == CoreManager.Singlenton.CajaActual.Caja;
            }
        }
        public bool TerminarOperacion()
        {
             if(CoreManager.Singlenton.CajaBloqueada())
            {
                return false;
            }
            if (SePuedeTerminar)
            {
                _estado = EstadoEnum.Terminado;
                LastMod = DateTime.Now;
                saveMe();
                logger.Singlenton.addMessage("Se ha terminado el movimiento " + DescripcionTipo);
                
                return true;
            }
            else
            {
                logger.Singlenton.addWarningMsg("El movimiento "+DescripcionTipo+" no cumple los requisitos para terminarlo. ");
                return false;
            }
           
        }
        public void EditarMovimiento()
        {

            if (!SePuedeEditar || CoreManager.Singlenton.CajaBloqueada()) return;
            _estado = EstadoEnum.Modificado;
           
            saveMe();
            logger.Singlenton.addDebugMsg("Movimiento","Se habilitó la edición del movimiento ");
            
        }
        //Montos
        public decimal Diferencia
        {
            get
            {
                if (EsMovimientoDinero)
                    return Math.Round(TotalAcreditado - TotalPagado, 2);
                else
                    return Math.Round(TotalCotizado - TotalPagado, 2);
            }
        }
        //pagos
        private Hashtable _pagos = new Hashtable();
        public decimal TotalMovimientoSinIva { get; set; }
        public decimal TotalPagado { get; set; }
        public decimal TotalPagodoSinDescuentos
        {
            get
            {
                return TotalPagado - TotalDescontado;
            }
        }
        public decimal TotalAcreditado { get; set; }
        public decimal TotalAPagar
        {
            get
            {
                return Math.Round(TotalMovimiento - TotalDescontado,2);
            }
        }
        public ICollection Pagos
        {
            get
            {
                 return _pagos.Values;
            }
        }
        public bool addPago(gcPago p)
        {
            if (CoreManager.Singlenton.CajaBloqueada())
            {
                return false;
            }
            //aun no se guardo
            if (Id == 0)
            {
                logger.Singlenton.addErrorMsg("El movimiento aun no esta guardado.\n No se puede agregar un pago.");
                return false;
            }
            if(Destinatario ==null)
            {
                logger.Singlenton.addErrorMsg("El movimiento no tiene destinatario.\n No se puede agregar un pago.");
                return false;
            }
            if (p != null && p.Monto != 0)
            {
                p.Movimiento = Id;
                p.Parent = Destinatario.Id;
                if (p.Observaciones == "") p.Observaciones = p.Tipo.ToString() + " en " + DescripcionTipo;
                pagar(p);
                p = CajaManager.Singlenton.CajaActual.AddPago(this, p);
                _pagos.Add(p.Id, p);
                CoreManager.Singlenton.AgregarPago(p);
                return true;
               
            }
            logger.Singlenton.addErrorMsg("Esta intentando agregar un pago vacio.");
            return false;
        }
        public bool addPagoUnique(gcPago p)
        {
            if (CoreManager.Singlenton.CajaBloqueada())
            {
                return false;
            }
            if (p == null) return false;
            //dinero crea pagocuenta.
            if (EsMovimientoDinero && (p.Tipo!=gcPago.TipoPago.Cuenta || p.Tipo!=gcPago.TipoPago.Vuelto))
            {
                clearPagos();
                addPago(p);
                addPagoCuenta(Diferencia);
                return true;
            }
            removePago(p.Tipo);
            return addPago(p);
            
        }
        public void reloadMontos()
        {
            TotalMovimiento = 0;
            TotalCotizado = 0;
            TotalAcreditado = 0;
            TotalPagado = 0;
            TotalDescontado = 0;
            TotalMovimientoSinIva = 0;
            foreach (gcObjetoMovido om in _objetosMovidos.Values)
            {
                TotalMovimiento += om.Total;
                TotalMovimientoSinIva += om.TotalSIVA;
                
                TotalCotizado += om.TotalCotizado;
            }
            foreach (gcPago p in _pagos.Values)
            {
                pagar(p);
            }
        }
        public bool addPagoCuenta(decimal monto)
        {
            DestinatarioManager.Singlenton.CreditoEnCUentaCorriente(Destinatario);
            return addPago(new gcPago(monto, gcPago.TipoPago.Cuenta));
        }
        public bool removePago(int pagoid)
        {
            if (CoreManager.Singlenton.CajaBloqueada())
            {
                return false;
            }
            gcPago p = (gcPago)_pagos[pagoid];
            if (p != null)
            {
                despagar(p);
                CajaManager.Singlenton.CajaActual.RemovePago(this, p);
               
                _pagos.Remove(pagoid);

                return true;
            }
           
            return false;
        }
        public void removePago(gcPago.TipoPago t)
        {
            Hashtable ps = (Hashtable)_pagos.Clone();
            foreach (gcPago cp in ps.Values)
            {
                if (cp.Tipo != t) continue;
                gcPago p = (gcPago)_pagos[cp.Id];
                if (p != null)
                {
                    despagar(p);
                    _pagos.Remove(cp.Id);
                    CajaManager.Singlenton.CajaActual.RemovePago(this, p);

                    

                   
                }

            }
           
           
        }
        public void clearPagos()
        {
            if (CoreManager.Singlenton.CajaBloqueada())
            {
                return;
            }
            Hashtable ps = (Hashtable)_pagos.Clone();
            foreach (gcPago cp in ps.Values)
            {

                gcPago p = (gcPago)_pagos[cp.Id];
                if (p != null)
                {
                    despagar(p);
                    _pagos.Remove(cp.Id);
                    CajaManager.Singlenton.CajaActual.RemovePago(this, p);

                    


                }

            }
           // _pagos.Clear();

        }

        public gcPago EfectivoPagado
        {
            get
            {
                gcPago ret = new gcPago(0, gcPago.TipoPago.Efectivo);
                foreach (gcPago p in Pagos)
                {
                    if (p.Tipo == gcPago.TipoPago.Efectivo)
                    {
                        ret += p;
                    }
                    else if (p.Tipo == gcPago.TipoPago.Vuelto)
                    {
                        ret -= p;
                    }
                }
                return ret;
            }
        }
        public gcPago PagosEnEfectivo
        {
            get
            {
                gcPago ret = new gcPago(0, gcPago.TipoPago.Efectivo);
                foreach (gcPago p in Pagos)
                {
                    if (p.Tipo == gcPago.TipoPago.Efectivo)
                    {
                        ret += p;
                    }
                   
                }
                return ret;
            }
        }
        public gcPago PagosEnTarjeta
        {
            get
            {
                gcPago ret = new gcPago(0, gcPago.TipoPago.Tarjeta);
                foreach (gcPago p in Pagos)
                {
                    if (p.Tipo == gcPago.TipoPago.Tarjeta)
                    {
                        ret += p;
                    }
                    
                }
                return ret;
            }
        }
        public gcPago PagosEnCheques
        {
            get
            {
                gcPago ret = new gcPago(0, gcPago.TipoPago.Cheque);
                foreach (gcPago p in Pagos)
                {
                    if (p.Tipo == gcPago.TipoPago.Cheque)
                    {
                        ret += p;
                    }

                }
                return ret;
            }
        }
        public gcPago PagosEnVueltos
        {
            get
            {
                gcPago ret = new gcPago(0, gcPago.TipoPago.Vuelto);
                foreach (gcPago p in Pagos)
                {
                    if (p.Tipo == gcPago.TipoPago.Vuelto)
                    {
                        ret += p;
                    }

                }
                return ret;
            }
        }
        public gcPago PagosEnCuentas
        {
            get
            {
                gcPago ret = new gcPago(0, gcPago.TipoPago.Cuenta);
                foreach (gcPago p in Pagos)
                {
                    if (p.Tipo == gcPago.TipoPago.Cuenta)
                    {
                        ret += p;
                    }

                }
                return ret;
            }
        }
        public gcPago PagosEnTarjetas
        {
            get
            {
                gcPago ret = new gcPago(0, gcPago.TipoPago.Tarjeta);
                foreach (gcPago p in Pagos)
                {
                    if (p.Tipo == gcPago.TipoPago.Tarjeta)
                    {
                        ret += p;
                    }

                }
                return ret;
            }
        }
        public gcPago PagosEnDescuentos
        {
            get
            {
                gcPago ret = new gcPago(0, gcPago.TipoPago.Descuento);
                foreach (gcPago p in Pagos)
                {
                    if (p.Tipo == gcPago.TipoPago.Descuento)
                    {
                        ret += p;
                    }

                }
                return ret;
            }
        }

        /// <summary>
        /// Obtiene la caja al que pertenece
        /// </summary>
        public gcCaja CajaContenedora
        {
            get
            {
                return CajaManager.Singlenton.obtenerCaja(Caja);
            }
        }
       
        private void pagar(gcPago p){
            
            switch (p.Tipo)
            {
               
                case gcPago.TipoPago.Cheque:
                case gcPago.TipoPago.Tarjeta:
                case gcPago.TipoPago.Efectivo:
                case gcPago.TipoPago.Descuento:
                case gcPago.TipoPago.Cuenta:
                    if (EsMovimientoDinero && p.Monto<0 )
                    {
                        //agrega al total cambiando de signo
                        TotalMovimiento -= p.Monto;
                        if (p.Tipo == gcPago.TipoPago.Cuenta) TotalAcreditado -= p.Monto;
                    }
                    else
                    {
                        TotalPagado += p.Monto;
                        if (p.Tipo == gcPago.TipoPago.Descuento) TotalDescontado += p.Monto;
                    }
                    
                    break;
                case gcPago.TipoPago.Vuelto:
                    TotalPagado -= p.Monto;
                    break;
            }
        }
        public void insertPagoFromDB(gcPago p)
        {
            if (p != null && !_pagos.ContainsKey(p.Id))
            {
                _pagos.Add(p.Id, p);
                pagar(p);
            }
        }
        private void despagar(gcPago p)
        {
            switch (p.Tipo)
            {
                case gcPago.TipoPago.Cheque:
                case gcPago.TipoPago.Tarjeta:
                case gcPago.TipoPago.Efectivo:
                case gcPago.TipoPago.Descuento:
                case gcPago.TipoPago.Cuenta:
                    if (EsMovimientoDinero && p.Monto < 0)
                    {
                        //agrega al total cambiando de signo
                        TotalMovimiento += p.Monto;
                        if (p.Tipo == gcPago.TipoPago.Cuenta) TotalAcreditado += p.Monto;
                    }
                    else
                    {
                        TotalPagado -= p.Monto;
                        if (p.Tipo == gcPago.TipoPago.Descuento) TotalDescontado -= p.Monto;
                    }
                    break;
               case gcPago.TipoPago.Vuelto:
                    TotalPagado += p.Monto;
                    break;


            }
        }
        public void setMonedaYCotizacion(gcObjetoMovido.MonedaEnum mon,decimal coti)
       {
           _moneda = mon;
           _cotizacion = coti;
           //aqui lo actualizamos en la lista
           gcMovimiento ml = MovimientoManager.Singlenton.getMovimiento(Id, (int)Tipo);
           ml.Moneda = mon;
           ml.Cotizacion = coti;
           //-------------------------------------
           if (coti < 1)
           {
               logger.Singlenton.addErrorMsg("Se intento establecer una cotizacion incorrecta, por defecto es 1");
               coti = 1;
           }
          
           foreach (gcObjetoMovido om in ObjetosMovidos)
           {
                if( coti > 1)
                {
                    // resetea el precio a dolar
                    om.Monto = om.Objeto.PrecioDolar * coti;
                } else
                {
                    // resetea el precio
                    om.Monto = om.Objeto.Precio;
                }
               om.Cotizacion = coti;
               om.Moneda = mon;
               MovimientoManager.Singlenton.guardarObjetoMovidoEnDB(om);
           }
           logger.Singlenton.addMessage("Se establecio una nueva cotizacion: "+mon+" ·"+coti.ToString("0.000")+"en "+DescripcionTipo);
          
       }

       public gcObjetoMovido.MonedaEnum Moneda
       {
           get
           {
               //en teoria todos tienen la misma moneda
               foreach (gcObjetoMovido om in ObjetosMovidos)
                   return om.Moneda;
               return _moneda;
           }
           set { _moneda = value; }
       }
       public decimal Cotizacion
        {
            get{
                //en teoria todos tienen la misma cotizacion
                foreach (gcObjetoMovido om in ObjetosMovidos)
                    return om.Cotizacion;
                return _cotizacion;
            }
            set
            {
                _cotizacion = value;
                foreach (gcObjetoMovido om in ObjetosMovidos)
                {
                    om.Cotizacion = _cotizacion;
                    om.saveMe();
                }
            }
            
        }
       public List<Discriminacion> Discriminaciones
       {
           get
           {
               Hashtable t = new Hashtable();
               foreach (gManager.gcObjetoMovido om in ObjetosMovidos)
               {
                   if (t.ContainsKey(om.IVAProducto))
                   {
                       Discriminacion c = (Discriminacion)t[om.IVAProducto];
                       c.Impuesto += om.TotalImpuestoIva;
                       c.Subtotal += om.TotalSIVA;
                       
                      
                   }
                   else
                   {
                       Discriminacion c = new Discriminacion();
                       c.Impuesto = om.TotalImpuestoIva;
                       c.Subtotal = om.TotalSIVA;
                       
                       c.Iva = om.IVAProducto;
                      
                       t.Add(om.IVAProducto, c);
                   }
               }
               return t.Values.OfType<Discriminacion>().ToList<Discriminacion>();
           }
       }
       public class Discriminacion
       {
           public decimal Subtotal { get; set; }
           public decimal Iva { get; set; }
           public decimal Impuesto { get; set; }
           public decimal Total { get { return Subtotal + Impuesto; } }
       }
       public bool Terminado
       {
           get
           {
               return Estado == EstadoEnum.Terminado;
           }
       }
       public gcMovimiento saveMe(gcUsuario owner=null)
       {
            Usuario = owner == null ? CoreManager.Singlenton.Usuario : owner;
           Id = MovimientoManager.Singlenton.GuardarMovimientoEnDB(this).Id;
           return this;
           
       }
       public bool deleteMe()
       {
           if (!SePuedeEliminar) return false;
           if (MovimientoManager.Singlenton.deleteMovimiento(this))
           {
              
               return true;
           }
               return false;
           
       }
       public bool SePuedeEliminar
       {
           get
           {
               return !Terminado && EsOwnerOrContador;
              
           }
                    
       }


        //objetos movidos
       private SortedDictionary<int, gcObjetoMovido> _objetosMovidos = new SortedDictionary<int, gcObjetoMovido>();
      
      
        /// <summary>
        /// Monto total del movimiento. Suma de los totales de los objetos movidos se actualiza solo.(NO ASIGNAR)
        /// </summary>
        public decimal TotalMovimiento { get; set; }
        public decimal TotalCotizado { get; set; }
        public ICollection ObjetosMovidos
        {
            get
            {
                return _objetosMovidos.Values;
            }
        }
        public void addNewObjetoMovido(gcObjetoMovido om){
            if (CoreManager.Singlenton.CajaBloqueada())
            {
                return;
            }
            if (Id == 0)
            {
                logger.Singlenton.addWarningMsg("Intentó agregar un producto a un movimiento que no se guardó");
                return;
            }
            if (EsMovimientoDinero)
            {
                logger.Singlenton.addWarningMsg("Intentó agregar un producto a un movimiento de dinero.");
                return;
            }
            if (Terminado)
            {
                logger.Singlenton.addWarningMsg("Intentó agregar un producto a un movimiento inactivo.");
                return;
            }
            
            if (om != null)
            {
                if(om.Objeto!=null && 
                    ( om.Objeto.Tipo==gcObjeto.ObjetoTIpo.Categoria ||
                      om.Objeto.Tipo==gcObjeto.ObjetoTIpo.Grupo     || 
                      (om.Objeto.Tipo==gcObjeto.ObjetoTIpo.Servicio && (EsMovimientoMercaSolo || Tipo==TipoMovEnum.Compra))
                    )
                   )
                {
                    logger.Singlenton.addWarningMsg("No puede agregar " + om.Objeto.Tipo.ToString() + " a este tipo de movimiento.");
                    return;
                }
                om.Movimiento = Id;
                om.Cotizacion = Cotizacion;
                
                om.Moneda = Moneda;
                if (EsMovimientoMercaSolo) om.Monto = 0;
               
                //overlap
                gcObjetoMovido ox=getObjetoIfExist(om.Objeto);
                if (ox!=null)
                {

                    ox.incrementar(om.Cantidad);
                    
                    reloadMontos();

                        
                    return;
                }
                   
               
               //no guarda cuando carga la primera ves
                MovimientoManager.Singlenton.updateStock(om);
                MovimientoManager.Singlenton.guardarObjetoMovidoEnDB(om);
                TotalMovimiento += om.Total;
                TotalMovimientoSinIva += om.TotalSIVA;
                TotalCotizado += om.TotalCotizado;
                _objetosMovidos.Add(om.Id, om);
            }
        }
        public gcObjetoMovido getObjetoIfExist(gcObjeto o)
        {
            foreach (gcObjetoMovido om in ObjetosMovidos)
                if (om.Objeto.Id == o.Id) return om;
            return null ;
        }
        public gcObjetoMovido getObjetoMovidoById(int omid)
        {
            foreach (gcObjetoMovido om in ObjetosMovidos)
                if (om.Id == omid) return om;
            return null;
        }
        public void insertObjetoMovidoFromDB(gcObjetoMovido om)
        {
            if (om != null && !_objetosMovidos.ContainsKey(om.Id))
            {
                TotalMovimiento += om.Total;
                TotalMovimientoSinIva += om.TotalSIVA;
                TotalCotizado += om.TotalCotizado;
                _objetosMovidos.Add(om.Id, om);
            }
        }
       
        public bool removeObjetoMovido(int omid)
        {
            if (Estado == EstadoEnum.Terminado) return false;
            gcObjetoMovido om = null;
            if(_objetosMovidos.ContainsKey(omid))
                 om =(gcObjetoMovido) _objetosMovidos[omid];
               
            if (om != null)
            {
                TotalMovimiento -= om.Total;
                TotalMovimientoSinIva -= om.TotalSIVACot;
                TotalCotizado -= om.TotalCotizado;
                //en la cantidad va negativo por que sale
                MovimientoManager.Singlenton.updateStock(om,true);
                 _objetosMovidos.Remove(omid);
                 MovimientoManager.Singlenton.eliminarObjetoMovidoDeDB(om);
           
                return true;
            }
            return false;
        }
        public List<gcPago> Descuentos
        {
            get
            {
                return Pagos.Cast<gcPago>().Where(p => p.Tipo == gcPago.TipoPago.Descuento).ToList();
            }
        }
        public decimal DescuentoPorcentual
        {
            get
            {
                if (TotalCotizado > 0)
                    return Diferencia * 100 / TotalCotizado;
                return 0;
            }
           
        }
        public decimal MaximoDescuentoPermitido
        {
            get
            {
                decimal ret = 0;
                foreach(gcObjetoMovido om in ObjetosMovidos)
                {
                    ret += om.Objeto.MaximoMontoDescuento * om.Cantidad;
                }

                return ret - TotalDescontado;
            }
        }
        public decimal TotalDescontado { get; set; }
        public decimal DescuentoForzado
        {
            get
            {
                if (SePuedeDescontar)
                {
                    return Diferencia;
                }
                return 0;
            }

        }


        //combos
        List<gcCombo> _combos = new List<gcCombo>();
        public List<gcCombo> Combos()
        {
            return _combos;
        }
        public bool applyCombo(gcCombo combo)
        {
            return false;
        }
    }
  
}
