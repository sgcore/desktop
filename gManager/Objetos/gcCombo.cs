﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gManager
{
    public class gcCombo
    {
        List<gcObjeto> _lista = new List<gcObjeto>();
        public enum tipoCantidad
        {
            Igual,
            MayorIgual,
        }
        public enum tipoTotal
        {
            Exacto,
            Porcentaje
        }
        public gcCombo(DataRow row)
        {

        }
        public int Id { get; set; }
        public decimal Total { get; set; }
        public List<gcObjeto> Productos
        {
            get
            {
                return _lista;  
            }
        }
        public decimal Cantidad { get; set; }
        public tipoCantidad CantidadTipo { get; set; }
        public tipoTotal TotalTipo { get; set; }
        /// <summary>
        /// Si se aplica
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public bool AplycableCombo(gcMovimiento m)
        {

            return false;
        }
    }
}
