﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Threading.Tasks;

namespace gManager
{

    public abstract class gcObjeto
    {
        
        public gcObjeto()
        {
            ini();
        }
        public gcObjeto(ObjetoTIpo tip, int parent)
        {
            ini(tip, parent);
        }
        private void ini(ObjetoTIpo t = ObjetoTIpo.Producto, int p = 0)
        {
            Id = -1;
            Nombre = "Objeto sin asignar";
            Descripcion = "Sin Descripcion";
            Codigo = "";
            Serial = "";
            Link = "";
            Grupo = 0;
            Costo = 0;
            Ganancia = 0;
            Dolar = 0;
            Ideal = 10;
            Iva = 21;
            Cara0 = "";
            Cara1 = "";
            Cara2 = "";
            Cara3 = "";
            Cara4 = "";
            Cara5 = "";
            Cara6 = "";
            Cara7 = "";
            Cara8 = "";
            Cara9 = "";
            Metrica = ObjetoMedida.Unidad;
            Parent = p;
            Tipo = t;
            State = ObjetoState.Normal;
            image = "";
            updated_at = new DateTime(2015,12,1);
        }
        public gcObjeto(System.Data.DataRow row)
        {
            ini();
            setFromRow(row);
        }
        public gcObjeto Clone()
        {
            gcObjeto o;
            switch (Tipo)
            {
                case ObjetoTIpo.Categoria:
                    o = new gcCategoria();
                    break;
                case ObjetoTIpo.Producto:
                    o = new gcProducto();
                    break;
                case ObjetoTIpo.Servicio:
                    o = new gcServicio();
                    break;
                case ObjetoTIpo.Grupo:
                    o = new gcGrupo();
                    break;
                default:
                    o = new gcProducto();
                    break;

            }

            o.Nombre = Nombre;
            o.Descripcion = Descripcion;
            o.Costo = Costo;
            o.Ganancia = Ganancia;
            o.Grupo = Grupo;
            o.Link = Link;
            o.Parent = Parent;

            o.Cara0 = Cara0;
            o.Cara1 = Cara1;
            o.Cara2 = Cara2;
            o.Cara3 = Cara3;
            o.Cara4 = Cara4;
            o.Cara5 = Cara5;
            o.Cara6 = Cara6;
            o.Cara7 = Cara7;
            o.Cara8 = Cara8;
            o.Cara9 = Cara9;
            o.Metrica = Metrica;
            o.Dolar = Dolar;
            o.Ideal = Ideal;
            o.image = image;
            o.updated_at = updated_at;
            return o;

        }
        public void merge(gcObjeto o)
        {
            if (!String.IsNullOrEmpty(o.Nombre)) Nombre = o.Nombre;
            if (!String.IsNullOrEmpty(o.Descripcion)) Descripcion = o.Descripcion;
            if (!String.IsNullOrEmpty(o.Codigo)) Codigo = o.Codigo;
            if (!String.IsNullOrEmpty(o.Serial)) Serial = o.Serial;
            if (!String.IsNullOrEmpty(o.Link)) Link =o.Link;

            if (!String.IsNullOrEmpty(o.Cara0)) Cara0 = o.Cara0;
            if (!String.IsNullOrEmpty(o.Cara1)) Cara1 = o.Cara1;
            if (!String.IsNullOrEmpty(o.Cara2)) Cara2 = o.Cara2;
            if (!String.IsNullOrEmpty(o.Cara3)) Cara3 = o.Cara3;
            if (!String.IsNullOrEmpty(o.Cara4)) Cara4 = o.Cara4;
            if (!String.IsNullOrEmpty(o.Cara5)) Cara5 = o.Cara5;
            if (!String.IsNullOrEmpty(o.Cara6)) Cara6 = o.Cara6;
            if (!String.IsNullOrEmpty(o.Cara7)) Cara7 = o.Cara7;
            if (!String.IsNullOrEmpty(o.Cara8)) Cara8 = o.Cara8;
            if (!String.IsNullOrEmpty(o.Cara9)) Cara9 = o.Cara9;

            if (!String.IsNullOrEmpty(o.image)) image = o.image;

            if (o.Costo > 0) Costo = o.Costo;
            if (o.Ganancia > 0) Ganancia = o.Ganancia;
            if (o.Iva > 0) Iva = o.Iva;
            if (o.Parent > 0) Parent = o.Parent;
            if (o.Tipo > 0) Tipo = o.Tipo;
            if (o.Metrica > 0) Metrica = o.Metrica;
            if (o.Dolar > 0) Dolar = o.Dolar;
            if (o.Ideal > 0) Ideal = o.Ideal;
        }

        public void SaveMe(bool update=true)
        {
            int id = Id;
            if(update)updated_at = updated_at.AddSeconds(1);
            Id = ProductoManager.Singlenton.guardarObjetoEnDb(this, Id > 0).Id;
           
        }
        public Task Sincronizar()
        {
            return WebManager.Singlenton.SincronizarProducto(
                this,
                (o, obj) =>
                {
                    ActualizarSiNecesita(obj);
                    gLogger.logger.Singlenton.addDebugMsg("Sincronizacion", Nombre + " ahora esta en internet");
                });
        }
        public Task Desincronizar()
        {
            return WebManager.Singlenton.DesincronizarProducto(
                this,
                (o, obj) =>
                {
                    gLogger.logger.Singlenton.addDebugMsg("Sincronizacion", Nombre + " ya no esta en internet");
                });
        }
        public enum ObjetoTIpo { Producto, Categoria, Servicio, Grupo }
        public enum ObjetoMedida { Unidad, Metro, Litro, Kilo, Minuto }
        public enum ObjetoState { Normal, Publico, Discontinuo }
        private void setFromRow(System.Data.DataRow row)
        {
            if (row["id"] != DBNull.Value) Id = int.Parse(row["id"].ToString());
            if (row["Nombre"] != DBNull.Value) Nombre = (string)row["Nombre"];
            if (row["Descripcion"] != DBNull.Value) Descripcion = (string)row["Descripcion"];
            if (row["Codigo"] != DBNull.Value) Codigo = (string)row["Codigo"];
            if (row["Serial"] != DBNull.Value) Serial = (string)row["Serial"];
            if (row["Link"] != DBNull.Value) Link = (string)row["Link"];
            if (row["Grupo"] != DBNull.Value) Grupo = (int)row["Grupo"];

            decimal co;
            decimal.TryParse((row["Costo"].ToString()), out co);
            if (row["Costo"] != DBNull.Value) Costo = co;
            if (row["Ganancia"] != DBNull.Value) Ganancia = decimal.Parse(row["Ganancia"].ToString());
            if (row["iva"] != DBNull.Value) Iva = decimal.Parse(row["iva"].ToString());
            if (row["Cara0"] != DBNull.Value) Cara0 = (string)row["Cara0"];
            if (row["Cara1"] != DBNull.Value) Cara1 = (string)row["Cara1"];
            if (row["Cara2"] != DBNull.Value) Cara2 = (string)row["Cara2"];
            if (row["Cara3"] != DBNull.Value) Cara3 = (string)row["Cara3"];
            if (row["Cara4"] != DBNull.Value) Cara4 = (string)row["Cara4"];
            if (row["Cara5"] != DBNull.Value) Cara5 = (string)row["Cara5"];
            if (row["Cara6"] != DBNull.Value) Cara6 = (string)row["Cara6"];
            if (row["Cara7"] != DBNull.Value) Cara7 = (string)row["Cara7"];
            if (row["Cara8"] != DBNull.Value) Cara8 = (string)row["Cara8"];
            if (row["Cara9"] != DBNull.Value) Cara9 = (string)row["Cara9"];
            if (row["Parent"] != DBNull.Value) Parent = (int)row["Parent"];
            if (row["Tipo"] != DBNull.Value) Tipo = (ObjetoTIpo)row["Tipo"];
            if (row["Metrica"] != DBNull.Value) Metrica = (ObjetoMedida)row["Metrica"];
            if (row["Dolar"] != DBNull.Value) Dolar = decimal.Parse(row["Dolar"].ToString());
            if (row["Ideal"] != DBNull.Value) Ideal = decimal.Parse(row["Ideal"].ToString());
            if (row["State"] != DBNull.Value) State = (ObjetoState)row["State"];
            if (row["image"] != DBNull.Value) image = (string)row["image"];
            if (row["updated_at"] != DBNull.Value) updated_at = (DateTime)row["updated_at"];
            //depende de la busqueda
            /* try
             {
                 if (row["Diferencia"] != DBNull.Value) Stock = (decimal)row["Diferencia"];
             }
             catch { }

            */

        }
        public int Id { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "Nombre")]
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public string Serial { get; set; }
        public string Link { get; set; }
        public int Grupo { get; set; }
        public decimal Costo { get; set; }
        public decimal Ganancia { get; set; }
        public decimal Iva { get; set; }
        public decimal Dolar { get; set; }
        public decimal PrecioDolar
        {
            get
            {
                return Math.Round((decimal)(Dolar * (1 + ((decimal)Ganancia / 100))), 2);
            }
        }
        public decimal Ideal { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "Precio")]
        public decimal Precio
        {
            get
            {
                return Math.Round((decimal)(Costo * (1 + ((decimal)Ganancia / 100))), 2);
            }
        }
        public string Cara0 { get; set; }
        public string Cara1 { get; set; }
        public string Cara2 { get; set; }
        public string Cara3 { get; set; }
        public string Cara4 { get; set; }
        public string Cara5 { get; set; }
        public string Cara6 { get; set; }
        public string Cara7 { get; set; }
        public string Cara8 { get; set; }
        public string Cara9 { get; set; }
        public string Categoria
        {
            get
            {
                return CategoriaContenedora != null ? CategoriaContenedora.Codigo : "";
            }
        }
        public string image { get; set; }
        public DateTime updated_at { get; set; }
        public string ImagenPath { get { return ImageManager.getPathIfExist(this); } }
        public string getCaracteristica(int i)
        {
            switch (i)
            {
                case 0:
                    return Cara0;
                case 1:
                    return Cara1;
                case 2:
                    return Cara2;
                case 3:
                    return Cara3;
                case 4:
                    return Cara4;
                case 5:
                    return Cara5;
                case 6:
                    return Cara6;
                case 7:
                    return Cara7;
                case 8:
                    return Cara8;
                case 9:
                    return Cara9;
                default:
                    return "";
            }
        }
        public void setCaracteristica(int i, string value)
        {
            switch (i)
            {
                case 0:
                    Cara0 = value;
                    break;
                case 1:
                    Cara1 = value;
                    break;
                case 2:
                    Cara2 = value;
                    break;
                case 3:
                    Cara3 = value;
                    break;
                case 4:
                    Cara4 = value;
                    break;
                case 5:
                    Cara5 = value;
                    break;
                case 6:
                    Cara6 = value;
                    break;
                case 7:
                    Cara7 = value;
                    break;
                case 8:
                    Cara8 = value;
                    break;
                case 9:
                    Cara9 = value;
                    break;
            }
        }
        
        public decimal Stock { get { return gManager.MovimientoManager.Singlenton.getStock(Id); } }
        public string StockEnUnidades
        {
            get
            {
                return Utils.UnidadMedida(Metrica, Stock);
            }
        }
        public string StockDescripcion
        {
            get
            {
                if (EsServicio)
                {
                    if (HayStockNegativo)
                    {
                        return "producido";
                    } else
                    {
                        return "sin producir";
                    }
                    
                }
                if (HayMuchoStock)
                {
                    return "en stock";
                }else if(HayPocoStock){
                    return "stock reducido";
                }
                else
                {
                    return "sin stock";
                }
            }
        }
        public bool EsServicio
        {
            get
            {
                return Tipo == ObjetoTIpo.Servicio;
            }
        }
        public bool EsProducto
        {
            get
            {
                return Tipo == ObjetoTIpo.Producto;
            }
        }
        public bool EsCategoria
        {
            get
            {
                return Tipo == ObjetoTIpo.Categoria;
            }
        }
        /// <summary>
        /// devuelve la version local actualizada a los valores de esta instancia.
        /// la nueva copia no esta guardada.
        /// </summary>
        public gcObjeto getMyMergedLocalCopy()
        {
            gcObjeto temp = ProductoManager.Singlenton.getObjeto(Codigo);
            if (temp == null)
            {
                temp = ProductoManager.Singlenton.getObjeto(Serial);
            }
            if (temp != null)
            {
                if (temp.Id != Id)
                   temp.merge(this);

            }
            else //si es null devolvemos esta
            {
                temp = this;
            }
            return temp;
        }
        public bool EsGrupo
        {
            get
            {
                return Tipo == ObjetoTIpo.Grupo;
            }
        }
        public bool EsProductoOServicio
        {
            get
            {
                return EsProducto || EsServicio;
            }
        }
        public bool HayStock
        {
            get { return Stock > 0; }
        }
        public bool HayStockNegativo
        {
            get
            {
                return Stock < 0;
            }
        }
        public bool HayPocoStock
        {
            get { return Stock < Ideal && HayStock; }
        }
        public bool HayMuchoStock
        {
            get { return Stock >= Ideal && HayStock; }
        }
        public bool EsPublico
        {
            get
            {
                return State == ObjetoState.Publico;
            }
        }
        /// <summary>
        /// Verifica si se puede sincronizar
        /// </summary>
        /// <param name="show">Determina si se generan mensajes de error</param>
        /// <returns></returns>
        public bool SePuedeSincronizar(bool show, bool force = false)
        {
            if (String.IsNullOrEmpty(Codigo.Trim()))
            {
                if(show)gLogger.logger.Singlenton.addWarningMsg("No puede sincronizar " + Nombre + " porque no tiene codigo");
                return false;
            }
            if (Sincronizado && !force)
            {
                if (show) gLogger.logger.Singlenton.addWarningMsg("No puede sincronizar " + Nombre + " porque ya esta sincronizado");
                return false;
            }
            if (!EsPublico)
            {
                if (show) gLogger.logger.Singlenton.addWarningMsg("No puede sincronizar " + Nombre + " porque no esta marcado como publico");
                return false;
            }
            return true;
        }
        public bool ActualizarSiNecesita(gcObjeto o)
        {
            if(o.updated_at > updated_at)
            {
                updated_at = o.updated_at;
                if (!String.IsNullOrEmpty(o.Nombre)) Nombre = o.Nombre;
                if (!String.IsNullOrEmpty(o.Descripcion)) Descripcion = o.Descripcion;
                SaveMe(false);
                return true;
            }
            return false;
        }
        public int Parent
        {
            get;
            set;
        }
        public ObjetoTIpo Tipo { get; set; }
        public ObjetoMedida Metrica { get; set; }
        public ObjetoState State { get; set; }
        public gcGrupo GrupoContenedor
        {
            get
            {
                return ProductoManager.Singlenton.getObjeto(Grupo, (int)ObjetoTIpo.Grupo) as gcGrupo;
            }
        }
        public string toJSON()
        {
            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(this.GetType());
            ser.WriteObject(stream1, this);
            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);
            return sr.ReadToEnd();
        }
        public Dictionary<string, string> toDictionary()
        {
            return new Dictionary<string, string>
            {
                { "Id", Id.ToString() },
                { "Nombre", Nombre },
                { "Descripcion", Descripcion },
                { "Codigo", Codigo },
                { "Serial", Serial },
                { "Link", Link },
                { "Grupo", Grupo.ToString() },
                { "Costo", Costo.ToString("0.00") },
                { "Ganancia", Ganancia.ToString("0.00") },
                { "Dolar", Dolar.ToString("0.00") },
                { "Ideal", Ideal.ToString("0.00") },
                { "Iva", Iva.ToString("0.00") },
                { "Cara0", Cara0 },
                { "Cara1", Cara1 },
                { "Cara2", Cara2 },
                { "Cara3", Cara3 },
                { "Cara4", Cara4 },
                { "Cara5", Cara5 },
                { "Cara6", Cara6 },
                { "Cara7", Cara7 },
                { "Cara8", Cara8 },
                { "Cara9", Cara9 },
                { "Metrica", ((int)Metrica).ToString() },
                { "Parent", Parent.ToString() },
                { "Tipo", ((int)Tipo).ToString() },
                { "State", ((int)State).ToString() },
                { "Stock", Stock.ToString() },
                { "updated_at", updated_at.ToString("yyyy-M-d h:m:s") },
            };
        }

        public decimal MontoGanado
        {
            get
            {
                return Costo - Precio;
            }
        }
        public decimal MaximoMontoDescuento
        {
            get
            {
                return Precio - (Costo * (decimal)1.3);
            }
        }
        public decimal MaximoPorcientoDescuento
        {
            get
            {
                return Precio * MaximoMontoDescuento / 100;
            }
        }

        public virtual List<gcObjeto> SubItems
        {
            get { return new List<gcObjeto>(); }
        }
        public bool DeleteMe()
        {
            return ProductoManager.Singlenton.removeObjetoFromDB(this);
        }

        public gcCategoria CategoriaPrincipal
        {
            get
            {
                if (RamaCategorias.Count == 0) return null;
                return RamaCategorias[RamaCategorias.Count - 1];
            }
        }
        public List<gcCategoria> RamaCategorias
        {
            get
            {
                List<gcCategoria> l = new List<gcCategoria>();
                gcCategoria c = CategoriaContenedora;
                int t = 0;
                //limitamos a 50 ramas
                while (c != null && t < 50)
                {
                    t++;
                    l.Add(c);
                    c = c.CategoriaContenedora;
                }
                return l;
            }
        }
        public gcCategoria CategoriaContenedora
        { get { return ProductoManager.Singlenton.getObjeto(Parent) as gcCategoria; } }

        public bool PerteneceACategoria(gcCategoria cat)
        {

            return RamaCategorias.Contains(cat);

        }
        public bool Sincronizado { get; set; }
        public bool Validate(Filtros.FiltroProducto f)
        {
            /*if(f.EstadoDiscontinuo && State == ObjetoState.Discontinuo) return true;
             if (f.EstadoNormal && State == ObjetoState.Normal) return true;
             if (f.EstadoPublico && State == ObjetoState.Publico) return true;
             if (f.TieneDolar && Dolar > 0) return true;
             if (f.SinStock && Stock == 0) return true;
             if (f.StockNegativo && Stock < 0) return true;
             if (f.StockBajo && Stock < Ideal) return true;
             if (f.StockAlto && Stock >= Ideal) return true;
             return false;*/


            /*bool result = true;
            if (f.EstadoDiscontinuo) result &= State == ObjetoState.Discontinuo;
            if (f.EstadoNormal) result &= State == ObjetoState.Normal;
            if (f.EstadoPublico) result &= State == ObjetoState.Publico;
            if (f.TieneDolar) result &= Dolar > 0;
            if (f.SinStock) result |= Stock == 0;
            if (f.StockNegativo) result |= Stock < 0;
            if (f.StockBajo) result |= Stock <= Ideal && Stock > 0;
            if (f.StockAlto) result |= Stock > Ideal;
            return result;*/
            bool stok = !f.FiltraStock || (f.SinStock && Stock == 0)
            || (f.StockNegativo && Stock < 0)
            || (f.StockBajo && Stock < Ideal && Stock > 0)
            || (f.StockAlto &&  Stock >= Ideal && Stock > 0);

            bool st =!f.FiltraEstado || (f.EstadoDiscontinuo && State == ObjetoState.Discontinuo)
             || (f.EstadoNormal && State == ObjetoState.Normal)
            || (f.EstadoPublico && State == ObjetoState.Publico);
            return stok && st;

            /*return (!f.EstadoDiscontinuo || f.EstadoDiscontinuo && State == ObjetoState.Discontinuo)
             || (!f.EstadoNormal || f.EstadoNormal && State == ObjetoState.Normal)
            || (!f.EstadoPublico || f.EstadoPublico && State == ObjetoState.Publico)
            || (!f.TieneDolar || f.TieneDolar && Dolar > 0)
            || (!f.SinStock || f.SinStock && Stock == 0)
            || (!f.StockNegativo || f.StockNegativo && Stock < 0)
            || (!f.StockBajo || f.StockBajo && Stock < Ideal)
            || (!f.StockAlto || f.StockAlto && Stock >= Ideal);*/
        }


    }
    [DataContract]
    public class gcProducto : gcObjeto
    {
        public gcProducto() { Id = 0; Nombre = "Nuevo Producto"; Tipo = ObjetoTIpo.Producto; }
        public gcProducto(System.Data.DataRow row)
            : base(row)
        {
            Tipo = ObjetoTIpo.Producto;
        }
        
        
        [DataMember]
        public string code { get { return Codigo; } }
        [DataMember]
        public decimal price { get { return Precio; } }
        [DataMember]
        public decimal stock { get { return Stock; } }
        [DataMember]
        public int id { get { return Id; } }
    }
    public class gcCategoria : gcObjeto
    {
        public gcCategoria() { Id = 0; Nombre = "Nueva categoría"; Tipo = ObjetoTIpo.Categoria ; }
       
        public gcCategoria(System.Data.DataRow row)
            : base(row)
        {
            Tipo = ObjetoTIpo.Categoria ;
        }
        public override List<gcObjeto> SubItems
        {
            get
            {
                var f=new  Filtros.FiltroProducto();
                f.Parent = Id;
                return ProductoManager.Singlenton.getObjetos(f);
            }
        }
        private List<gcObjeto> recursiveProd(gcObjeto o, bool cats)
        {
            List<gcObjeto> list = new List<gcObjeto>();
            foreach (gcObjeto i in o.SubItems)
            {
                if (!(i.Tipo == ObjetoTIpo.Grupo)
                   || (i.Tipo == ObjetoTIpo.Categoria && cats)
                    )
                {
                    list.Add(i);
                }
                list.AddRange(recursiveProd(i, cats));
            }
            return list;
        }
        public List<gcObjeto> subProductosYServicios()
        {
            return recursiveProd(this, false);
        }
    }
    public class gcServicio : gcObjeto
    {
        public gcServicio() { Id = 0; Nombre = "Nuevo Servicio"; Tipo = ObjetoTIpo.Servicio ; }
        public gcServicio(System.Data.DataRow row)
            : base(row)
        {
            Tipo = ObjetoTIpo.Servicio ;
        }
    }
    public class gcGrupo : gcObjeto
    {
        public gcGrupo() { Id = 0; Nombre = "Nuevo Grupo"; Tipo = ObjetoTIpo.Grupo; }
        public gcGrupo(System.Data.DataRow row)
            : base(row)
        {
            Tipo = ObjetoTIpo.Grupo;
        }
        public override List<gcObjeto> SubItems
        {
            get
            {
                var f = new Filtros.FiltroProducto();
                f.Grupo = Id;
                return ProductoManager.Singlenton.getObjetos(f);
            }
        }
    }
    public class gcObjetoGenerico : gcObjeto
    {
        public gcObjetoGenerico() { }
        public gcObjetoGenerico(gcObjeto o)
        {
            Id = o.Id;
            Nombre = o.Nombre;
            Descripcion = o.Descripcion;
            Codigo = o.Codigo;
            Serial = o.Serial;
            Link = o.Link;
            Cara0 = o.Cara0;
            Cara1 = o.Cara1;
            Cara2 = o.Cara2;
            Cara3 = o.Cara3;
            Cara4 = o.Cara4;
            Cara5 = o.Cara5;
            Cara6 = o.Cara6;
            Cara7 = o.Cara7;
            Cara8 = o.Cara8;
            Cara9 = o.Cara9;
            Parent = o.Parent;
            Tipo = o.Tipo;
            Costo = o.Costo;
            Ganancia = o.Ganancia;
            Metrica = o.Metrica;
            Dolar = o.Dolar;
            Ideal = o.Ideal;
            Iva = o.Iva;
            Stock = o.Stock;
            updated_at = o.updated_at;
            image = o.image;

        }
        decimal stk=0;
        decimal pre = 0;
        [DataMember]
        new public decimal Stock
        {
            get { return stk; }
            set { stk = value; }
        }
        [DataMember]
        new public decimal Precio
        {
            get { return pre; }
            set { pre = value; }
        }


    }
}
