﻿using System;
using System.Collections.Generic;

namespace gManager
{
    public class gcPago
    {
        private int _m1000 = 0;
        private int _m500 = 0;
        private int _m200 = 0;
        private int _m100 = 0;
        private int _m50 = 0;
        private int _m20 = 0;
        private int _m10 = 0;
        private int _m5 = 0;
        private int _m2 = 0;
        private int _m1 = 0;
        private int _m050 = 0;
        private int _m025 = 0;
        private int _m010 = 0;
        private int _m005 = 0;
        private int _m001 = 0;
        private string _obs = "";
        public enum TipoPago { Efectivo, Vuelto, Tarjeta, Cheque, Cuenta, Descuento }
        public gcPago(int[] monto,TipoPago tipo) {
            EstablecerTotal(monto);
            Tipo=tipo;
        }
        public gcPago(decimal monto, TipoPago tipo)
        {
            EstablecerTotal(monto);
            Tipo = tipo;
        }
        public gcPago()
        {
            Tipo = TipoPago.Efectivo;
            Monto = 0;
        }
        public decimal[] Nominaciones
        {
            get
            {
               decimal[] b ={1000,
                            500,
                            200,
                            100 ,
                             50 ,
                             20 ,
                             10 ,
                             5 ,
                             2 ,
                             1 ,
                             (decimal)0.5 ,
                             (decimal)0.25 ,
                             (decimal)0.1 ,
                             (decimal)0.05,
                             (decimal)0.01

                        };
                return b;
            }
        }
        public int[] Billetes
        {
            get
            {
                int[] b ={
                            _m1000 ,
                            _m500 ,
                            _m200 ,
                            _m100 ,
                             _m50 ,
                             _m20 ,
                             _m10 ,
                             _m5 ,
                             _m2 ,
                             _m1 ,
                             _m050 ,
                             _m025 ,
                             _m010 ,
                             _m005 ,
                             _m001 

                        };
                return b;
            }
        }
        public gcPago(System.Data.DataRow row)
        {
            fromRow(row);
        }
        public void fromRow(System.Data.DataRow row)
        {

            try
            {
                Id = int.Parse(row["id"].ToString());
                Movimiento = (int)row["Movimiento"];
                Tipo = (TipoPago)row["Tipo"];
                Observaciones = (string)row["Observaciones"];
            }
            catch { }
            // if(row["Monto"]!= null ) Monto = (decimal)row["Monto"]  ;
            if (row["m1000"] != System.DBNull.Value) _m1000 = (int)row["m1000"];
            if (row["m500"] != System.DBNull.Value) _m500 = (int)row["m500"];
            if (row["m200"] != System.DBNull.Value) _m200 = (int)row["m200"];
            if (row["m100"] != System.DBNull.Value) _m100 = (int)row["m100"];
            if (row["m50"] != System.DBNull.Value) _m50 = (int)row["m50"];
            if (row["m20"] != System.DBNull.Value) _m20 = (int)row["m20"];
            if (row["m10"] != System.DBNull.Value) _m10 = (int)row["m10"];
            if (row["m5"] != System.DBNull.Value) _m5 = (int)row["m5"];
            if (row["m2"] != System.DBNull.Value) _m2 = (int)row["m2"];
            if (row["m1"] != System.DBNull.Value) _m1 = (int)row["m1"];
            if (row["m050"] != System.DBNull.Value) _m050 = (int)row["m050"];
            if (row["m025"] != System.DBNull.Value) _m025 = (int)row["m025"];
            if (row["m010"] != System.DBNull.Value) _m010 = (int)row["m010"];
            if (row["m005"] != System.DBNull.Value) _m005 = (int)row["m005"];
            if (row["m001"] != System.DBNull.Value) _m001 = (int)row["m001"];
            if (row["parent"] != System.DBNull.Value) Parent = (int)row["parent"];
            Monto = SacarTotal();
        }
        public void EstablecerTotal(decimal monto)
        {
            Monto = monto;
            monto = Math.Round(monto, 2);

            m1000 = (int)Math.Truncate(monto / 1000);
            monto = monto - (1000 * m1000);

            m500 = (int)Math.Truncate(monto / 500);
            monto = monto - (500 * m500);

            m200 = (int)Math.Truncate(monto / 200);
            monto = monto - (200 * m200);

            m100 = (int)Math.Truncate(monto / 100);
            monto = monto - (100 * m100);

            m50 = (int)Math.Truncate(monto / 50);
            monto = monto - (50 * m50);

            m20 = (int)Math.Truncate(monto / 20);
            monto = monto - (20 * m20);

            m10 = (int)Math.Truncate(monto / 10);
            monto = monto - (10 * m10);

            m5 = (int)Math.Truncate(monto / 5);
            monto = monto - (5 * m5);

            m2 = (int)Math.Truncate(monto / 2);
            monto = monto - (2 * m2);

            m1 = (int)Math.Truncate(monto / 1);
            monto = monto - (1 * m1);

            m050 = (int)Math.Truncate(monto / (decimal)0.5);
            monto = monto - ((decimal)0.5 * m050);

            
            m025 = (int)Math.Truncate(monto / (decimal)0.25);
            monto = monto - ((decimal)0.25 * m025);

            m010 = (int)Math.Truncate(monto / (decimal)0.1);
            monto = monto - ((decimal)0.1 * m010);


            m005 = (int)Math.Truncate(monto / (decimal)0.05);
            monto = monto - ((decimal)0.05 * m005);

            m001 = (int)Math.Truncate(Math.Round(monto, 2) / (decimal)0.01);
            monto = monto - ((decimal)0.01 * m001);

        }
        public void EstablecerTotal(int[] monto)
        {
            _m1000 = monto[0];
            _m500 = monto[1];
            _m200 = monto[2];
            _m100 = monto[3];
            _m50 = monto[4];
            _m20 = monto[5];
            _m10 = monto[6];
            _m5 = monto[7];
            _m2 = monto[8];
            _m1 = monto[9];
            _m050 = monto[10];
            _m025 = monto[11];
            _m010 = monto[12];
            _m005 = monto[13];
            _m001 = monto[14];
            Monto = SacarTotal();

        }
        public decimal SacarTotal()
        {
            decimal total = 0;
            total += m1000 * 1000;
            total += m500 * 500;
            total += m200 * 200;
            total += m100 * 100;
            total += m50 * 50;
            total += m20 * 20;
            total += m10 * 10;
            total += m5 * 5;
            total += m2 * 2;
            total += m1 * 1;
            total += m050 * (decimal)0.5;
            total += m025 * (decimal)0.25;
            total += m010 * (decimal)0.1;
            total += m005 * (decimal)0.05;
            total += m001 * (decimal)0.01;
            return total;

        }
        public int Id { get; set; }
        public int Movimiento { get; set; }
        public TipoPago Tipo { get; set; }
        public string Html(string Titulo)
        {

            System.Text.StringBuilder t = new System.Text.StringBuilder();
            t.Append("<table style='width: 164px;  border-collapse: collapse; text-align: left; margin-left: 0px; margin-right: 0px;'border='2' cellpadding='1' cellspacing='1'><tbody align='left'>  <tr align='center'>  <td style='background-color: #ccccff;' colspan='4' rowspan='1'><span style='text-decoration: underline;'><span    style='font-weight: bold;'>" + Titulo + "<br>  </span></span></td>  </tr><td style='background-color: #339999;'><span style='font-style: italic;'><span style='color: #99ffff;'>$100</span></span></td><td style='background-color: #99ffff; text-align: center; width: 45px;'><span style='font-weight: bold;'>");
            t.Append(m100 + "</span></span></td><td style='background-color: #339999; height: 20px; width: 38px;'><span style='color: #99ffff;'>$1</span><span style='text-decoration: underline;'><span style='font-weight: bold;'><span style='color: #99ffff;'></span></span></span></span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m1 + "</span></span></td></tr><tr><td style='background-color: #339999; width: 41.533px;'><span style='font-style: italic;'><span style='color: #99ffff;'>$50</span></span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m50 + "</span></span></td><td style='background-color: #339999;'><span style='color: #99ffff;'>$0.50</span><span style='text-decoration: underline;'><span style='font-style: italic;'><span style='font-weight: bold;'><span style='color: #99ffff;'></span></span></span></span></td><td style='width: 28.867px; background-color: #99ffff; text-align: center;'><span style='font-weight: bold;'>");
            t.Append(m050 + "</span></span></td></tr><tr><td style='background-color: #339999;'><span style='font-style: italic;'><span style='color: #99ffff;'>$20</span></span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m20 + "</span></span></td><td style='background-color: #339999;'><span style='color: #99ffff;'>$0.25</span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m025 + "</span></span></td></tr><tr><td style='background-color: #339999;'><span style='font-style: italic;'><span style='color: #99ffff;'>$10</span></span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m10 + "</span></span></td><td style='background-color: #339999;'><span style='color: #99ffff;'>$0.10</span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m010 + "</span></span></td></tr><tr><td style='background-color: #339999;'><span style='font-style: italic;'><span style='color: #99ffff;'>$5</span></span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m5 + "</span></span></td><td style='background-color: #339999;'><span style='color: #99ffff;'>$0.05</span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m005 + "</span></span></td></tr><tr><td style='background-color: #339999;'><span style='font-style: italic;'><span style='color: #99ffff;'>$2</span></span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m2 + "</span></span></td><td style='background-color: #339999;'><span style='color: #99ffff;'>$0.01</span></td><td style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(m001 + "</span></span></td></tr><tr><td colspan=4 style='background-color: #99ffff; text-align: center;'><span style='font-style: italic;'><span style='font-weight: bold;'>");
            t.Append(SacarTotal().ToString("0.00") + "</span></span></td></tr></tbody></table>");
            return t.ToString();

        }
        public decimal Monto { get; set; }
        public int m1000 { get { return _m1000; } set { _m1000 = value; ; } }
        public int m500 { get { return _m500; } set { _m500 = value; ; } }
        public int m200 { get { return _m200; } set { _m200 = value; ; } }
        public int m100 { get { return _m100; } set { _m100 = value; ; } }
        public int m50 { get { return _m50; } set { _m50 = value; ;} }
        public int m20 { get { return _m20; } set { _m20 = value; ;} }
        public int m10 { get { return _m10; } set { _m10 = value; ;} }
        public int m5 { get { return _m5; } set { _m5 = value; ;} }
        public int m2 { get { return _m2; } set { _m2 = value; ;} }
        public int m1 { get { return _m1; } set { _m1 = value; ;} }
        public int m050 { get { return _m050; } set { _m050 = value; ;} }
        public int m025 { get { return _m025; } set { _m025 = value; ;} }
        public int m010 { get { return _m010; } set { _m010 = value; ;} }
        public int m005 { get { return _m005; } set { _m005 = value; ;} }
        public int m001 { get { return _m001; } set { _m001 = value; ;} }
        public string Observaciones { get { return _obs; } set { _obs = value; } }
        public int Parent { get; set; }
        public object[] propiedadesarray { get { object[] o1 = { Id, Movimiento, Tipo, Monto, Observaciones, m100, m50, m20, m10, m5, m2, m1, m050, m025, m010, m005, m001,Parent }; return o1; } }
        public static gcPago operator +(gcPago a, gcPago b)
        {
            if (a == null)
            {
                if (b == null) return new gcPago(0,TipoPago.Efectivo);
                return b;
            }
            if (b == null) return a;
            //if (a.Tipo != b.Tipo)
            //{
            //    logger.Singlenton.addErrorMsg("Intento sumar dos pagos de distinto tipo.");
            //    return a;
            //}
            gcPago p = new gcPago(0, a.Tipo);
            if (a.Tipo == TipoPago.Efectivo || a.Tipo == TipoPago.Vuelto)
            {
                p.m1000 = a.m1000 + b.m1000;
                p.m500 = a.m500 + b.m500;
                p.m200 = a.m200 + b.m200;
                p.m100 = a.m100 + b.m100;
                p.m50 = a.m50 + b.m50;
                p.m20 = a.m20 + b.m20;
                p.m10 = a.m10 + b.m10;
                p.m5 = a.m5 + b.m5;
                p.m2 = a.m2 + b.m2;
                p.m1 = a.m1 + b.m1;
                p.m050 = a.m050 + b.m050;
                p.m025 = a.m025 + b.m025;
                p.m010 = a.m010 + b.m010;
                p.m005 = a.m005 + b.m005;
                p.m001 = a.m001 + b.m001;
               
            }
            p.Monto = a.Monto + b.Monto;
            
            

            return p;
        }
        public static gcPago operator -(gcPago a, gcPago b)
        {
            if (a == null)
            {
                if (b == null) return new gcPago(0, TipoPago.Efectivo);
                //devuelve negativo
                return new gcPago(0, b.Tipo) - b;
            }
            if (b == null) return a;
            //if (a.Tipo != b.Tipo)
            //{
            //    logger.Singlenton.addMessage("ERROR: Intento restar dos pagos de distinto tipo.", logger.MessageType.ERROR);
            //    return a;
            //}
            gcPago p = new gcPago(0, a.Tipo);
            if (a.Tipo == TipoPago.Efectivo || a.Tipo == TipoPago.Vuelto)
            {
                p.m1000 = a.m1000 - b.m1000;
                p.m500 = a.m500 - b.m500;
                p.m200 = a.m200 - b.m200;
                p.m100 = a.m100 - b.m100;
                p.m50 = a.m50 - b.m50;
                p.m20 = a.m20 - b.m20;
                p.m10 = a.m10 - b.m10;
                p.m5 = a.m5 - b.m5;
                p.m2 = a.m2 - b.m2;
                p.m1 = a.m1 - b.m1;
                p.m050 = a.m050 - b.m050;
                p.m025 = a.m025 - b.m025;
                p.m010 = a.m010 - b.m010;
                p.m005 = a.m005 - b.m005;
                p.m001 = a.m001 - b.m001;

            }
            p.Monto = a.Monto - b.Monto;



            return p;
        }
        public override string ToString()
        {
            if (Tipo == TipoPago.Efectivo || Tipo == TipoPago.Vuelto)
            {
                return Tipo.ToString() + ": " + Monto.ToString("$0.00")+
                    " Billetes: "+"(1000):"+m1000+" "+
                                    "(500):" + m500 + " " +
                                    "(200):" + m200 + " " +
                                    "(50):" +m50+"; "+
                                    "(20):"+m20+"; "+
                                    "(10):"+m10+"; "+
                                    "(5):"+m5+"; "+
                                    "(2):"+m2+"; "+
                                    "(1):"+m1+"; "+
                                    "(0.50):"+m050+"; "+
                                    "(0.25):"+m025+" "+
                                    "(0.10):"+m010+" "+
                                    "(0.05):"+m005+" "+
                                    "(0.01):"+m001;

            }
            return Tipo.ToString()+": "+Monto.ToString("·0.00");
        }
    }
   
    
}
