﻿using gConnector;
using gLogger;
using System;
using System.Collections;

namespace gManager
{
    public class gcTratamiento
    {
      

        public gcTratamiento() { }
        public gcTratamiento(gcDestinatario paciente, TipoTratamiento tipo)
        {
            Tipo = tipo;
            Fecha = DateTime.Now;
            Proximo = Fecha;
            UsuarioID =CoreManager.Singlenton.Usuario!=null? CoreManager.Singlenton.Usuario.Id:0;
            PacienteID = paciente != null ? paciente.Id : 0;

           
        }

        public gcTratamiento(System.Data.DataRow row)
        {
            if (row["id"] != DBNull.Value) Id = int.Parse(row["id"].ToString());
            if (row["Titulo"] != DBNull.Value) Titulo = (string)row["Titulo"];
            if (row["Descripcion"] != DBNull.Value) Descripcion = (string)row["Descripcion"];
            if (row["Usuario"] != DBNull.Value) UsuarioID = (int)row["Usuario"];
            if (row["Paciente"] != DBNull.Value) PacienteID = (int)row["Paciente"];
            if (row["Tipo"] != DBNull.Value) Tipo = (TipoTratamiento)row["Tipo"];
            if (row["Estado"] != DBNull.Value) _Estado = (EstadoSeguimientoEnum)(int)row["Estado"];
            if (row["Parent"] != DBNull.Value) Parent = (int)row["Parent"];
            if (row["Fecha"] != DBNull.Value) Fecha = DateTime.Parse(row["Fecha"].ToString());
            if (row["Proximo"] != DBNull.Value) Proximo = DateTime.Parse(row["Proximo"].ToString());
            if (row["TipoCustom"] != DBNull.Value) TipoGenerico = (int)row["TipoCustom"];
          
        }
        public enum EstadoSeguimientoEnum { Terminado,Pendiente,Cancelado,Futuro}
        public enum TipoTratamiento { Tratamiento, Recordatorio, Periodico, Tarea, Aviso}
        public int Id { get;set; }
        public int UsuarioID { get; set; }
        public gcUsuario Usuario { get { return UsuarioManager.Singlenton.getUsuario(UsuarioID); } }
        public string UsuarioNombre
        {
            get
            {
                var u = Usuario;
                if (u != null)
                    return u.Nombre;
                return "Desconocido";
            }
        }
        public string Descripcion { get; set; }
        public string Titulo { get; set; }
        public int PacienteID { get; set; }
        public TipoTratamiento Tipo { get;set; }
        public int TipoGenerico { get; set; }
        
        public int Parent { get; set; }
        public gcDestinatario Paciente
        {
            get { return DestinatarioManager.Singlenton.getDestinatario(PacienteID); }
        }
        public string PacienteNombre
        {
            get
            {
                var p = Paciente;
                if (p != null) return p.Nombre;
                return "Desconocido";
            }
        }
        public DateTime Fecha { get; set; }
        public DateTime Proximo { get; set; }
        public TimeSpan  Periodo
        {
            get
            {
                return Proximo.Subtract(Fecha);
            }
        }
        public int Intervalo
        {
            get
            {
                int n = (int) Periodo.TotalMilliseconds;
                return n > 0 ? n : 0;
                
            }
        }
        public TimeSpan TiempoRestante
        {
            get
            {
                return Proximo.Subtract(DateTime.Now);
            }
        }
        public string TiempoRestanteDesc
        {
            get
            {
                return gManager.Utils.MostrarTiempo(TiempoRestante);
            }
        }
        public string FechaDesc
        {
            get
            {
                if (Caducado)
                    return gManager.Utils.MostrarHora(Proximo);
                else
                    return "en " + TiempoRestanteDesc;

            }
        }
        
        public bool Caducado
        {
            get { return Proximo < DateTime.Now; }
        }
        
        public bool esValido
        {
            get
            {
                //si no puso el titulo es invalido
                if (Titulo.Trim() == "") return false;
                //si es tratamiento es valido siempre
                if (Tipo == TipoTratamiento.Tratamiento) return true;

                //si el proximo no esta asignado es invalido;
                if (Periodo.TotalMinutes<1) return false;
                
                return true;
            }
        }
        private EstadoSeguimientoEnum _Estado = EstadoSeguimientoEnum.Pendiente;
        public EstadoSeguimientoEnum Estado
        {
            get
            {
                if (_Estado != EstadoSeguimientoEnum.Terminado || _Estado != EstadoSeguimientoEnum.Cancelado)
                {
                    if (Caducado)
                    {
                        _Estado = EstadoSeguimientoEnum.Pendiente;

                        
                    }
                    else
                    {
                        _Estado = EstadoSeguimientoEnum.Futuro;
                    }
                }
                return _Estado;
            }
        }
        public void Terminar()
        {
            _Estado = EstadoSeguimientoEnum.Terminado;
            saveMe();
        }
        public void Cancelar()
        {
            _Estado = EstadoSeguimientoEnum.Cancelado;
            saveMe();
        }
        public void Pendiente()
        {
            if (Caducado)
            {
                _Estado = EstadoSeguimientoEnum.Pendiente;


            }
            else
            {
                _Estado = EstadoSeguimientoEnum.Futuro;
            }
            saveMe();
        }
        public void saveMe()
        {
            Hashtable t = new Hashtable();
            t.Add("id", Id);
            t.Add("Usuario", UsuarioID);
            t.Add("Titulo", Titulo);
            t.Add("Descripcion", Descripcion);
            t.Add("Paciente", PacienteID);
            t.Add("Fecha", Fecha);
            t.Add("Proximo", Proximo);
            t.Add("Estado", Estado);
            t.Add("Parent", Parent);
            t.Add("Tipo", Tipo);
            t.Add("TipoCustom", TipoGenerico);
            var oldid = Id;
            Id = Connector.Singlenton.ConexionActiva.GuardarENtity(t, "gcTratamientos", Id > 0);
            DestinatarioManager.Singlenton.addTratamiento(this);
            if (oldid==0)
            {
                logger.Singlenton.addMessage("Se creo un nuevo " + Tipo.ToString() + ": " + Titulo.ToString(), Tipo.ToString());
                CoreManager.Singlenton.CrearTratamiento(this);
            }
            else
                logger.Singlenton.addMessage( "Se se actualizó un " + Tipo.ToString() + ": " + Titulo.ToString(), Tipo.ToString());
           

        }
        public void DeleteMe()
        {
            if (Paciente != null) Paciente.removeTratamiento(this);
            CoreManager.Singlenton.EliminarTratamiento(this);
            DestinatarioManager.Singlenton.removeTratamientoFromDB(this);
        }
       
    }
}
