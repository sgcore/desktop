﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gManager
{
    public class ProductoUpdate
    {
        public ProductoUpdate(string nombre)
        {
            Nombre = nombre;
            if (String.IsNullOrEmpty(Nombre)) Nombre = "Desconocido";
        }
        public ProductoUpdate()
        {
            Nombre = "Desconocido";
        }
        [ExcelColumn("Nombre")]
        public string Nombre { get; set; }
        [ExcelColumn("Descripcion")]
        public string Descripcion { get; set; }
        [ExcelColumn("Codigo")]
        public string Codigo { get; set; }
        [ExcelColumn("Costo")]
        public decimal Costo { get; set; }
        [ExcelColumn("Ganancia")]
        public decimal Ganancia
        {
            get;
            set;
        }
        [ExcelColumn("Cotizacion")]
        public decimal Cotizacion { get; set; }
        private gcObjeto _prod = null;
        public gcObjeto Producto { get { if (_prod == null)_prod = ProductoManager.Singlenton.getObjeto(Codigo); return _prod; } }
       public string ProductoDescripcion
        {
            get
            {
                return Producto != null ? Producto.Nombre : "No tiene un producto asociado";
            }
        }
        public decimal CostoCotizado { get { return Costo * Cotizacion; } }
        public decimal Diferencia
        {
            get
            {
                return Producto != null ? (Costo - Producto.Costo) : 0;
            }
        }
        List<ProductoUpdate> _list = new List<ProductoUpdate>();
        public bool EsContenedor { get { return _list.Count > 0; } }
        public bool EsConocido { get { return Producto != null; } }
        public void SaveMe()
        {
            if (!EsConocido)
            {
                _prod = new gcProducto();
                _prod.Nombre = Nombre;
                _prod.Codigo = Codigo;
                _prod.Costo = CostoCotizado;
                _prod.Ganancia = Ganancia;
                

            }
            else
            {
                if (CostoCotizado > 0) Producto.Costo = CostoCotizado;
                if (Ganancia > 0) Producto.Ganancia = Ganancia;
            }
            Producto.SaveMe();
        }
        public List<ProductoUpdate> SubItems
        {
            get
            {
                return _list;
            }
        }
    }
}
