﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace gManager
{
    [DataContract]
    public class gcDestinatario
    {
        private Hashtable _tratamientos = new Hashtable();
        protected int _dobletipo = 0;
        public enum DestinatarioTipo { Cliente, Proveedor, Sucursal, Responsable, Financiera, Banco, Paciente,Contacto }
        public enum DestinatarioIVA { Consumidor_Final, Monotributo, Responsable_Inscripto,Exento}
        public enum especieEnum { Canino, Felino, Equino, Ave, Granja, Ganado, Exotico }
        /**
         * JSON
         * */
        [DataMember]
        public string name { get { return Nombre; } }
        [DataMember]
        public int id { get { return Id; } }


        public gcDestinatario(System.Data.DataRow row)
        {
            setFromRow(row);
        }
        
        public gcDestinatario(DestinatarioTipo tipo)
        {
            Tipo = tipo;
            init();

        }
        
        private void init()
        {
            Id = 0;
            Nombre = "Nuevo " + Tipo.ToString() ;
            Direccion = "Desconocida";
            Celular = "Desconocido";
            Telefono = "Desconocido";
            Mail = "No tiene";
            Cuit = "Sin asignar";
            Observaciones = "";
            TipoIva = DestinatarioIVA.Consumidor_Final;
            Parent = 0;
            Nacimiento = DateTime.Now;
            LastMod = DateTime.Now;
            Estado = 0;
            foreach (var r in Enum.GetValues(typeof(gcTratamiento.TipoTratamiento)))
                _tratamientos.Add((int)r, new Hashtable());
        }
        protected virtual void setFromRow(System.Data.DataRow row)
        {
            init();
            if (row["id"] != DBNull.Value) Id = int.Parse(row["id"].ToString());
            if (row["Nombre"] != DBNull.Value) Nombre = (string)row["Nombre"];
            if (row["Direccion"] != DBNull.Value) Direccion = (string)row["Direccion"];
            if (row["Telefono"] != DBNull.Value) Telefono = (string)row["Telefono"];
            if (row["Celular"] != DBNull.Value) Celular = (string)row["Celular"];
            if (row["Mail"] != DBNull.Value) Mail = (string)row["Mail"];
            if (row["Cuit"] != DBNull.Value) Cuit = (string)row["Cuit"];
            if (row["Observaciones"] != DBNull.Value) Observaciones = (string)row["Observaciones"];
            if (row["Tipo"] != DBNull.Value) Tipo = (DestinatarioTipo)(int)row["Tipo"];
            //se lo usa para pacientes
            if (row["TipoIva"] != DBNull.Value) _dobletipo = (int)row["TipoIva"];
            if (row["Parent"] != DBNull.Value) Parent = (int)row["Parent"];
            if (row["Nacimiento"] != DBNull.Value) Nacimiento = DateTime.Parse(row["Nacimiento"].ToString());
            if (row["Estado"] != DBNull.Value) Estado = (int)row["Estado"];
            if (row["LastMod"] != DBNull.Value) LastMod = DateTime.Parse(row["LastMod"].ToString());

        }
        public especieEnum Especie { get { return (especieEnum)_dobletipo; } set { _dobletipo = (int)value; } }
        public bool EsPaciente { get { return Tipo == DestinatarioTipo.Paciente; } }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Mail { get; set; }
        public string Cuit { get; set; }
        public string Observaciones { get; set; }
        public DestinatarioTipo Tipo { get; set; }
        public int Parent { get; set; }
        public DateTime Nacimiento { get; set; }
        public int Estado { get; set; }
        public DestinatarioIVA TipoIva { get { return (DestinatarioIVA)_dobletipo; } set { _dobletipo = (int)value; } }
        public int TipoGenerico { get { return _dobletipo; } set { _dobletipo = value; } }
        public DateTime LastMod { get; set; }
        public string Html
        {
            get
            {
                System.Text.StringBuilder t = new System.Text.StringBuilder();
                t.Append(" <table style='width: 500px; border-color: green;' border='5'><tbody><tr><td><table style='width: 500px; height: 48px;' border='0' cellpadding='0'cellspacing='2'><tbody><tr><td><span style='font-style: italic;'>Titular: <br></span> </td><td><span style='font-weight: bold;'>");
                t.Append(Nombre);
                t.Append("</span></td></tr><tr><td style='width: 71px;'><span style='font-style: italic;'>Direccion:</span></td><td style='width: 440px;'><span style='font-weight: bold;'>");
                t.Append(Direccion);
                t.Append("</span></td></tr><tr><td><span style='font-style: italic;'>Cuit:</span></td><td><span style='font-weight: bold;'>");
                t.Append(Cuit);
                t.Append("</span></td></tr><tr><td><span style='font-style: italic;'>Contacto:<br></span> </td><td><span style='font-weight: bold;'>");
                t.Append(Telefono+" - "+Celular+" - "+Mail);
                t.Append("</span></td></tr></tbody></table></td></tr></tbody></table>");
                return t.ToString();
            }
        }
        public string html(string plantilla)
        {
            if (plantilla == null || plantilla == "") return Html;
            return plantilla.Replace("{titular}", Nombre).Replace("{direccion}", Direccion).Replace("{cuit}", Cuit).Replace("{telefono}", Telefono).Replace("{celular}", Celular).Replace("{correo}", Mail);
        }
        public bool PuedeDepositar
        {
            get
            {
                return Tipo == DestinatarioTipo.Banco || 
                    Tipo == DestinatarioTipo.Cliente || 
                    Tipo == DestinatarioTipo.Financiera || 
                    Tipo == DestinatarioTipo.Proveedor || 
                    Tipo == DestinatarioTipo.Responsable;
            }
        }
        public decimal SaldoEnCuenta { get; set; }
        public void SaveMe()
        {
            int tmp = Id;
           
            Id=DestinatarioManager.Singlenton.GuardarDestinatarioEnDB(this).Id;

            if (tmp == 0) CoreManager.Singlenton.CreadoDestinatario(this);
        }
        public gcDestinatario Propietario
        {
            get
            {
                return DestinatarioManager.Singlenton.getDestinatario(Parent);
            }
        }
        public List<gcDestinatario> Pacientes
        {
            get
            {
                var f = new Filtros.FiltroDestinatario(DestinatarioTipo.Paciente);
                f.Parent = Id;
                return DestinatarioManager.Singlenton.getDestinatarios(f);
                
            }
        }
        public string PropietarioNombre
        {
            get
            {
                if (Propietario != null)
                {
                    return Propietario.Nombre;
                }
                return "Desconocido";
            }
        }
        public void DeleteMe()
        {

        }
        //tratamientos
        
        public void addTratamiento(gcTratamiento t)
        {
            if (t == null) return;
            var tr = (Hashtable)_tratamientos[(int)t.Tipo];
            if (!tr.ContainsKey(t.Id))
                tr.Add(t.Id, t);
            else
            {
                tr[t.Id] = t;
            }
        }
        public void removeTratamiento(gcTratamiento t)
        {
            if (t == null) return;
            var tr = (Hashtable)_tratamientos[(int)t.Tipo];
            if (tr.ContainsKey(t.Id))
                tr.Remove(t.Id);
        }
        public List<gcTratamiento> Avisos { get { return retTrata(gcTratamiento.TipoTratamiento.Aviso); } }
        public List<gcTratamiento> Periodicos { get { return retTrata(gcTratamiento.TipoTratamiento.Periodico); } }
        public List<gcTratamiento> Recordatorios { get { return retTrata(gcTratamiento.TipoTratamiento.Recordatorio); } }
        public List<gcTratamiento> Tareas { get { return retTrata(gcTratamiento.TipoTratamiento.Tarea); } }
        public List<gcTratamiento> Tratamientos { get { return retTrata(gcTratamiento.TipoTratamiento.Tratamiento); } }
        private List<gcTratamiento> retTrata(gcTratamiento.TipoTratamiento t)
        {
            return ((Hashtable)_tratamientos[(int)t]).Values.OfType<gcTratamiento>().OrderByDescending(x => x.Proximo).ToList(); 
        }
        public object[] propiedadesArray
        {
            get
            {
                object[] o = { Id, Nombre, Direccion, Telefono, Celular, Mail, Cuit, Observaciones, Tipo, Parent, Nacimiento, Estado };
                return o;
            }
        }
       
    }
}
