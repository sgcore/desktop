﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace gManager
{
    [DataContract]
    public class gcUsuario
    {
        private int _centralId = -1;
        private int _responsableId = -1;
        private gcDestinatario _central;
        private gcDestinatario _responsable;
        public enum PermisoEnum { Visitante,Vendedor,Encargado,Contador,Administrador}
        public gcUsuario(System.Data.DataRow row)
        {
            setFromRow(row);
        }
        public gcUsuario() { }
        public void setFromRow(System.Data.DataRow row)
        {
            if (row["id"] != DBNull.Value) Id = int.Parse(row["id"].ToString());
            if (row["Nombre"] != DBNull.Value) Nombre = (string)row["Nombre"];
            if (row["Descripcion"] != DBNull.Value) { Descripcion = (string)row["Descripcion"]; } else { Descripcion = "Sin descripción"; }
            if (row["pass"] != DBNull.Value) Pass = (string)row["pass"];
            if (row["usuario"] != DBNull.Value) User = (string)row["usuario"];
            if (row["Permiso"] != DBNull.Value) Permiso = (PermisoEnum)row["Permiso"];
            if (row["Central"] != DBNull.Value) _centralId = (int)row["Central"];
            if (row["Responsable"] != DBNull.Value) _responsableId = (int)row["Responsable"];

        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public string User { get; set; }

        [DataMember]
        public string Pass { get; set; }

        public PermisoEnum Permiso { get; set; }
        public string descPermiso
        {
            get
        {
            switch (Permiso)
            {
                case PermisoEnum.Administrador:
                    return "Administrador del sistema";
                case PermisoEnum.Contador:
                    return "Gestión Contable";
                case PermisoEnum.Encargado:
                    return "Jefe de Ventas";
                case PermisoEnum.Vendedor:
                    return "Vendedor";
                case PermisoEnum.Visitante:
                    return "Visitante";
               default:
                    return "Administrador del sistema";

            }
            
        }
        }
        public gcDestinatario Central
        {
            get 
            {
                if (_central == null && _centralId > 0) _central = DestinatarioManager.Singlenton.getDestinatario(_centralId);
                return _central; 
            }
            set { _central = value;
                if (_central != null)
                {
                    _centralId = _central.Id;
                   // UpdateMe();
                }
                else
                {
                    _centralId = 0;
                }
               
            }
        }

        public int CentralId
        {
            get { return _centralId; }
           
        }

        public string CentralNombre
        {
            get { return Central != null ? Central.Nombre : "Sin Asignar"; }
        }

        public int ResponsableId
        {
            get { return _responsableId; }
           
        }

        public gcDestinatario Responsable
        {
            get { if (_responsable == null && _responsableId > 0)_responsable = DestinatarioManager.Singlenton.getDestinatario(_responsableId); return _responsable; }
            set { 
                _responsable = value;
                _responsableId = _responsable != null ? _responsable.Id : 0;
            }
        }
        public string ResponsableNombre
        {
            get
            {
                return Responsable != null ? Responsable.Nombre : "Sin Asignar";
            }
        }
        public void UpdateMe()
        {
           UsuarioManager.Singlenton.GuardarUsuarioEnDb(this);
        }
        public bool EsAdmin
        {
            get
            {
                return ((int)Permiso >= (int)PermisoEnum.Administrador);
            }
        }
        public bool EsContador
        {
            get { return ((int)Permiso >= (int)PermisoEnum.Contador);}
        }
        public bool EsEncargado
        {
            get { return ((int)Permiso >= (int)PermisoEnum.Encargado); }
        }
        public bool EsVendedor
        {
            get { return ((int)Permiso >= (int)PermisoEnum.Vendedor); }
        }
        public bool EsVisitante
        {
            get { return ((int)Permiso <= (int)PermisoEnum.Visitante); }
        }
        
        public object[] propiedadesarray { get { object[] o = { Id, Nombre, Descripcion, User, Pass, Permiso, Central,Responsable }; return o; } }
        public string toJSON()
        {
            MemoryStream stream1 = new MemoryStream();
           
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;
            string output = JsonConvert.SerializeObject(this);
            return output;
        }

    }
}
