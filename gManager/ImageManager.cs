﻿using gManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace gManager
{
    public class ImageManager
    {
        private static Hashtable _productos = new Hashtable();
        private static Hashtable _destinatarios = new Hashtable();
        private static string path = "Imagenes";
        //public static Image getImageProducto(gcObjeto o)
        //{
        //    string path = Path.Combine("Imagenes", "prod" + o.Id+".png");
        //    return openImage(path);
            
        //}
        //public static Image getImageDestinatario(gcDestinatario o)
        //{
        //    string path = Path.Combine("Imagenes", "dest" + o.Id + ".png");
        //    return openImage(path);

        //}
        public static string getPath(object o, string extencion = "jpg")
        {
            string path = Path.Combine(Utils.GetProgramDirectory(), "Imagenes");
            if (o is gcObjeto)
                return Path.Combine(path, "prod" + (o as gcObjeto).Id + "." + extencion);
            else if (o is gcDestinatario)
                return Path.Combine("Imagenes", "dest" + (o as gcDestinatario).Id + "." + extencion);
            return "";
        }
        public static string getPathIfExist(object o)
        {
            var path = getPath(o);
            if (File.Exists(path))
                return path;
            else
                return "";
        }
        public static Image getImageGeneric(object o, Image nopic = null, int width = 0, int height=0)
        {
            //if (o is gcObjeto)
            //    return getImageProducto(o as gcObjeto);
            //else if (o is gcDestinatario)
            //    return getImageDestinatario(o as gcDestinatario);
            //return Properties.Resources.NoImage;
            var gg=openImage(getPath(o));
            if (gg == null)
            {
                if(nopic != null)
                {
                    gg = nopic;
                }
                else
                {

                    gg = new Bitmap(100, 100);
                }
            }
            int w = gg.Width;
            int h = gg.Height;
            if (width > 0 && height > 0) // custon mode
            {
                w = width;
                h = w * gg.Height / gg.Width;
                if(h > height)
                {
                    h = height;
                    w = h * gg.Width / gg.Height;
                }
            }
            else if(width > 0) // width relation
            {
                w = width;
                h = w * gg.Height / gg.Width;
            }
            else if (height > 0) // width relation
            {
                h= height;
                w = h * gg.Width / gg.Height;
            }
            return gg.GetThumbnailImage(w,h,() => { return true; },IntPtr.Zero);
        }
        public static bool HasImage(object o)
        {
            return File.Exists(getPath(o));
        }
        public static string getStringImage(Image img)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            byte[] byteImage = ms.ToArray();
            return Convert.ToBase64String(byteImage);
        }
        public static string getStringImage(gcObjeto o)
        {
            return getStringImage(getImageGeneric(o));
        }
        public static void SaveImageProducto(Image img,gcObjeto o)
        {
            saveImage(img, "prod" + o.Id);
        }
        public static void SaveImageDestinatario(Image img, gcDestinatario o)
        {
            saveImage(img, "dest" + o.Id);
        }
        public static void saveImage(Image img,string key)
        {
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            try
            {
                Bitmap bmp=new Bitmap(img);
               bmp.Save(Path.Combine(path, key+".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (Exception e)
            {
                gLogger.logger.Singlenton.addErrorMsg(e.Message);
            }
            

        }
        public static Image openImage(string path)
        {
            Image ret = null;// Properties.Resources.NoImage;
            if (!File.Exists(path)) return ret;
            using (var fs = File.OpenRead(path))
            using (var img = Image.FromStream(fs))
            {
                ret = new Bitmap(img,200,250);
            }
            return ret;
        }
    }
}
