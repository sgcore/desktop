﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using gConnector;
using gLogger;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;

namespace gManager
{
    
    internal class ProductoManager
    {
        private static ProductoManager _instance;
        private System.Collections.Hashtable _productos = new System.Collections.Hashtable();
        private System.Collections.Hashtable _serializados = new Hashtable();
        private System.Collections.Hashtable _codificados = new Hashtable();
        private System.Collections.Hashtable _linkeados = new Hashtable();
        public delegate void ObjetoDelegate(gcObjeto o);
        private ProductoManager() { }
        public static ProductoManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new ProductoManager();
                return _instance;
            }
        }
       
      
        public void reload(System.ComponentModel.BackgroundWorker worker, DataTable dat,ref int cont, int totalcount)
        {
             _productos.Clear();
            _serializados.Clear();
            _codificados.Clear();
            foreach (var r in Enum.GetValues(typeof(gcObjeto.ObjetoTIpo)))
                _productos.Add((int)r, new Hashtable());
            foreach (DataRow row in dat.Rows)
            {
                gcObjeto.ObjetoTIpo tipo = gcObjeto.ObjetoTIpo.Producto;
                if (row["Tipo"] != DBNull.Value) tipo = (gcObjeto.ObjetoTIpo)row["Tipo"];
                gcObjeto u;
                switch (tipo)
                {
                    case gcObjeto.ObjetoTIpo.Categoria:
                        u=new gcCategoria(row);
                        break;
                    case gcObjeto.ObjetoTIpo.Servicio:
                        u=new gcServicio(row);
                        break;
                    case gcObjeto.ObjetoTIpo.Grupo:
                        u = new gcGrupo(row);
                        break;
                    default:
                        u = new gcProducto(row);
                       
                        break;
                }
                addToSerialAndCodigoList(u);
                ((Hashtable)_productos[(int)u.Tipo]).Add(u.Id, u);


                //prograss
                cont++;
                if (worker != null)
                {
                    worker.ReportProgress((cont * 100 / totalcount) - 1);
                }

            }
           
            
        }

       
        public void updateSincronizados()
        {
            var x = WebManager.Singlenton.getProductos((o, list) =>
            {
                foreach(gcObjeto po in list)
                {
                    var pe = getObjeto(po.Codigo);
                    if(pe != null)
                    {
                        pe.Sincronizado = true;
                    }
                }
            }, true);
        }
        public List<ResumenBase> ListaDePrecios(Filtros.FiltroProducto f)
        {
            var l = new List<ResumenBase>();
            Hashtable princ = new Hashtable();
            foreach (gcObjeto o in ProductosYServicios)
            {
                if (!o.Validate(f))
                    continue;

                gcCategoria cp = o.CategoriaPrincipal;
                if (cp != null)
                {
                    if (!princ.ContainsKey(cp.Id))
                    {
                        CategoriaResumen newcat = new CategoriaResumen();
                        newcat.Categoria = cp;
                        princ.Add(cp.Id, newcat);
                    }
                    ((CategoriaResumen)princ[cp.Id]).addSubObjeto(o);
                }
                else
                {
                    if (!princ.ContainsKey(o.Id))
                    {
                        ProductoResumen newcat = new ProductoResumen();
                        newcat.Producto = o;
                        princ.Add(o.Id, newcat);
                    }
                    decimal monto = o.Precio;
                    decimal cant = o.Stock;
                   
                    ((ProductoResumen)princ[o.Id]).Cantidad += cant;
                    ((ProductoResumen)princ[o.Id]).Total += monto;

                }


            }

            return princ.Values.OfType<ResumenBase>().ToList(); ;
        }

        private void addToSerialAndCodigoList(gcObjeto u)
        {
            if (u.Tipo == gcObjeto.ObjetoTIpo.Categoria) return;
            string serial = u.Serial.Trim().ToLower();
            string codigo = u.Codigo.Trim().ToLower();
            string link = u.Link.Trim().ToLower();


            if (serial != null && serial != "" && !_serializados.ContainsKey(serial)) _serializados.Add(serial, u);
            if (codigo != null && codigo != "" && !_codificados.ContainsKey(codigo)) _codificados.Add(codigo, u);
            if (!String.IsNullOrEmpty(link) && !_linkeados.ContainsKey(link)) _linkeados.Add(link, u);
        }
        
        public List<gcObjeto> ProductosEnCategoria(gcCategoria c, bool cats,bool servi,bool prod)
        {

            var f = new gManager.Filtros.FiltroProducto();
            
            f.Parent =c.Id;
            return recursiveSearch(f,cats,servi,prod);
           
        }

        private List<gcObjeto> recursiveSearch(Filtros.FiltroProducto f, bool cats, bool servi, bool prod)
        {
            var list = gManager.ProductoManager.Singlenton.getObjetos(f);
            List<gcObjeto> l = new List<gcObjeto>();
            foreach(var e in list){
                f.Parent=e.Id;
                if (e.Tipo == gcObjeto.ObjetoTIpo.Categoria) { 
                    l.AddRange(recursiveSearch(f, cats, servi, prod));
                    if (cats) l.Add(e);
                }
                else if (e.Tipo == gcObjeto.ObjetoTIpo.Servicio && servi) l.Add(e);
                else if (e.Tipo == gcObjeto.ObjetoTIpo.Producto && prod) l.Add(e);

            }
            
            return l ;
             
        }
        public int ObjetosCount
        {
            get
            {
                int ret = 0;
                foreach (var r in Enum.GetValues(typeof(gcObjeto.ObjetoTIpo)))
                    ret += ((Hashtable)_productos[(int)r]).Count;

                return ret;
            }
        }
        public List<gcProducto> Productos
        {
            get
            {
                if (_productos.Count > 0)
                    return ((Hashtable)_productos[(int)gcObjeto.ObjetoTIpo.Producto]).Values.OfType<gcProducto>().ToList<gcProducto>();
                return new List<gcProducto>();
            }
        }
        public List<gcServicio> Servicios
        {
            get
            {
                if (_productos.Count > 0)
                    return ((Hashtable)_productos[(int)gcObjeto.ObjetoTIpo.Servicio]).Values.OfType<gcServicio>().ToList<gcServicio>();
                return new List<gcServicio>();
            }
        }
        public List<gcGrupo> Grupos
        {
            get
            {
                if (_productos.Count > 0)
                    return ((Hashtable)_productos[(int)gcObjeto.ObjetoTIpo.Grupo]).Values.OfType<gcGrupo>().ToList<gcGrupo>();
                return new List<gcGrupo>();
            }
        }
        public List<gcCategoria>  Categorias
        {
            get
            {
                if (_productos.Count > 0)
                    return ((Hashtable)_productos[(int)gcObjeto.ObjetoTIpo.Categoria]).Values.OfType<gcCategoria>().ToList<gcCategoria>() ;
                return new List<gcCategoria>();
            }
        }
        public List<gcObjeto> Objetos
        {
            get
            {
                if (_productos.Count ==0) return new List<gcObjeto>();
                List<gcObjeto> l = new List<gcObjeto>();
                foreach (var r in Enum.GetValues(typeof(gcObjeto.ObjetoTIpo)))
                {
                    l.AddRange(((Hashtable)_productos[(int)r]).Values.OfType<gcObjeto>());
                    
                }

                return l;
            }
        }
        public List<gcObjeto> Sincronizados
        {
            get
            {
                return Objetos.Where(o => o.Sincronizado = true).ToList();
            }
        }
        public List<gcObjeto> ProductosYServicios
        {
            get
            {
                if (_productos.Count ==0) return new List<gcObjeto>();
                List<gcObjeto> l = new List<gcObjeto>();
                l.AddRange(((Hashtable)_productos[(int)gcObjeto.ObjetoTIpo.Producto]).Values.OfType<gcObjeto>());
                l.AddRange(((Hashtable)_productos[(int)gcObjeto.ObjetoTIpo.Servicio]).Values.OfType<gcObjeto>());

                return l;
            }
        }
        public bool SePuedeEliminar(gcObjeto o)
        {
            switch (o.Tipo)
            {
                case gcObjeto.ObjetoTIpo.Categoria:
                case gcObjeto.ObjetoTIpo.Grupo:
                    if (o.SubItems.Count == 0) return true;
                    break;
                case gcObjeto.ObjetoTIpo.Producto:
                case gcObjeto.ObjetoTIpo.Servicio:
                    if (!MovimientoManager.Singlenton.StockTable.ContainsKey(o.Id))
                        return true;
                    break;
                    
                    
            }

            return false;
        }
        
        public gcObjeto getObjeto(int id,int tipo)
        {
            return (gcObjeto)((Hashtable)_productos[tipo])[id];
        }
        public gcObjeto getObjeto(int id)
        {
            if (_productos.Count == 0) return null;
            foreach (var r in Enum.GetValues(typeof(gcObjeto.ObjetoTIpo)))
            {
                gcObjeto o=(gcObjeto)((Hashtable)_productos[(int)r])[id];
                if(o!=null)return o;
            }
            return null;
        }
        public List<gcObjeto> getObjetos(Filtros.FiltroProducto f = null)
        {
            List<gcObjeto> c=new List<gcObjeto>();
            if (_productos.Count == 0) return c;
            if (f == null)
            {
                foreach (var r in Enum.GetValues(typeof(gcObjeto.ObjetoTIpo)))
                {
                    c.AddRange(((Hashtable)_productos[(int)r]).Values.OfType<gcObjeto>());

                }
            }
            else
            {
                string[] tt=null;
                List<gcObjeto> R1 = new List<gcObjeto>(), R2 = new List<gcObjeto>(), R3 = new List<gcObjeto>();
                if (f.Texto == null) f.Texto = "";
                    tt = f.Texto.ToLower().Trim().Split(" ".ToCharArray());
                ICollection objetos;

                if (f.Tipos.Count>0)
                {
                    List<Hashtable> lt = new List<Hashtable>();
                    foreach (var t in f.Tipos)
                    {
                        lt.Add((Hashtable)_productos[(int)t]);
                    }
                    Dictionary<object, object> d = new Dictionary<object, object>();
                    for (int i = 0; i < lt.Count; i++)
                        d = d.Union(lt[i].Cast<DictionaryEntry>().ToDictionary(a => a.Key, a => a.Value)).ToDictionary(b => b.Key, b => b.Value);
                    
                    
                    objetos =d.Values;
                }
                else
                {
                    objetos = getObjetos();
                }
                foreach (gcObjeto ob in objetos)
                {
                    if (f.Parent > -1 && f.Parent != ob.Parent) continue;
                    if (f.Grupo > 0 && f.Grupo != ob.Grupo) continue;
                    if (ob.Nombre.ToLower().Contains(f.Texto.ToLower().Trim()))
                    {
                        R1.Add(ob);
                        continue;

                    }
                    if (ob.Descripcion.ToLower().Contains(f.Texto.ToLower().Trim()))
                    {
                        R3.Add(ob);
                        continue;

                    }
                    int pos = 0;
                    int ttcont = 0;
                    int rango = 4;
                    string nombre = ob.Nombre.ToLower();
                    // busca coincidencias con cada fraccion de texto y va bajando de rango a medidas que encuentra.
                    //corta si llega a rango 1(encontro suficientes coincidencias)
                    //corta si no encontro ninguna coincidencia desde la ultima encontrada
                    //corta si se acabaron las coincidencias
                    while (pos < ob.Nombre.Length && ttcont < tt.Length && rango > 1)
                    {
                        if (pos < 0) pos = 0;
                        pos = nombre.IndexOf(tt[ttcont], pos);
                        if (pos > -1) rango--;
                        ttcont++;

                    }
                    //agrega unicamente si esta en el rango.
                    switch (rango)
                    {
                        case 1:
                            R1.Add(ob);
                            break;
                        case 2:
                            R2.Add(ob);
                            break;
                        case 3:
                            R3.Add(ob);
                            break;


                    }





                }
                c.AddRange(R1);
                c.AddRange(R2);
                if((R1.Count+R2.Count) == 0) c.AddRange(R3);
               
            }

            return c; //c.OrderBy(o => o.Nombre).ToList();
        }
        public List<gcObjeto> OrdenarLista(List<gcObjeto> l)
        {
            return l.OrderBy(o => o.Nombre).ToList();
        }
        public gcObjeto getObjeto(string serial)
        {
            if (String.IsNullOrEmpty(serial)) return null;
            serial = serial.ToLower();
            gcObjeto o = (gcObjeto)_serializados[serial];
            if (o == null)
            {
                o = (gcObjeto)_codificados[serial];
            }
            if (o == null)
            {
                o = (gcObjeto)_linkeados[serial];
            }
            return o;
        }
        public gcObjeto guardarObjetoEnDb(gcObjeto o,bool actualizar, Conexion cnn=null)
        {
            Hashtable t = new Hashtable();
            t.Add("id", o.Id);
            t.Add("Nombre", o.Nombre);
            t.Add("Descripcion", o.Descripcion);
            t.Add("Codigo", o.Codigo);
            t.Add("Serial", o.Serial);
            t.Add("Link", o.Link);
            t.Add("Grupo", o.Grupo);
            t.Add("Costo", o.Costo);
            t.Add("iva", o.Iva);
            t.Add("Ganancia", o.Ganancia);
            t.Add("Cara0", o.Cara0);
            t.Add("Cara1", o.Cara1);
            t.Add("Cara2", o.Cara2);
            t.Add("Cara3", o.Cara3);
            t.Add("Cara4", o.Cara4);
            t.Add("Cara5", o.Cara5);
            t.Add("Cara6", o.Cara6);
            t.Add("Cara7", o.Cara7);
            t.Add("Cara8", o.Cara8);
            t.Add("Cara9", o.Cara9);
            t.Add("Parent", o.Parent);
            t.Add("Tipo", o.Tipo);
            t.Add("Metrica", o.Metrica);
            t.Add("Dolar", o.Dolar);
            t.Add("Ideal", o.Ideal);
            t.Add("State", o.State);
            t.Add("image", o.image);
            t.Add("updated_at", o.updated_at);
            //selecciona bd
            if (cnn == null)
            {
                o.Id = Connector.Singlenton.ConexionActiva.GuardarENtity(t, "gcobjetos", actualizar);
                if(o.Id > 0)addObjeto(o);
                //----------------
                /*
                if (addObjeto(o))
                {
                    logger.Singlenton.addMessage("Se agrego " + o.Nombre.ToString(), "PRODUCTOS", gcMessage.MessageType.SYSTEM);
                }
                else
                    logger.Singlenton.addMessage("Se actualizó " + o.Nombre.ToString(), "PRODUCTOS", gcMessage.MessageType.SYSTEM);
                    */
                // sincronize(o);
            }
            else
            {
                o.Id = cnn.GuardarENtity(t, "gcobjetos", actualizar);
               
            }
               
            return o;
        }
        public bool removeObjetoFromDB(gcObjeto o)
        {
            if (SePuedeEliminar(o))
            {
                ((Hashtable)_productos[(int)o.Tipo]).Remove(o.Id);
                _codificados.Remove(o.Id);
                _serializados.Remove(o.Id);
                gConnector.Connector.Singlenton.ConexionActiva.deleteEntity(o.Id, "gcObjetos");
                logger.Singlenton.addMessage("Se eliminó a " + o.Nombre);
                return true;
            }

            logger.Singlenton.addWarningMsg(o.Nombre + " no se puede eliminar por que ya se utilizó.");
            return false;
        }
       
        private bool addObjeto(gcObjeto o)
        {
            if (getObjeto(o.Id, (int)o.Tipo) != null)
            {
                ((Hashtable)_productos[(int)o.Tipo]).Remove(o.Id);
                ((Hashtable)_productos[(int)o.Tipo]).Add(o.Id, o);
                addToSerialAndCodigoList(o);
                return false;
            }
            else
            {
                ((Hashtable)_productos[(int)o.Tipo]).Add(o.Id, o);
                addToSerialAndCodigoList(o);
                CoreManager.Singlenton.agregarProducto(o);
                return true;
            }
        }
        

       

        
       
        
       
    }
    
}
 