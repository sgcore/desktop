﻿using gConnector;
using gLogger;
using gManager.Resumenes;
using SGServer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace gManager
{
    public class CoreManager
    {
        private static CoreManager _instance;
        private CoreManager()
        {
           
        }
        public static CoreManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new CoreManager();
                return _instance;
            }
        }
        public enum EstadoSistemaEnum
        {
            Desconectado,
            Conectado,
            Autenticado,
            Cancelado,
            Cargado
        }
        
        //delegados
        public delegate void DestinatarioDelegate(gcDestinatario d);
        public delegate void MovimientoDelegate(gcMovimiento m);
        public delegate void EstadoSistemaDelegate(EstadoSistemaEnum e);
        public delegate void PagoDelegate(gcPago p);
        public delegate void UsuarioDelegate(gcUsuario u);
        public delegate void ProductoDelegate(gcObjeto o);
        public delegate void CajaDelegate(gcCaja c);
        public delegate void TratamientoDelegate(gcTratamiento t);
        public delegate void InfoHtmlDelegate(string c);
        public delegate void ProgressDelegate(int percent, string msg);
        public event EstadoSistemaDelegate CambiodeEstado;
        public event UsuarioDelegate SesionIniciada;
        public event UsuarioDelegate SesionFinalizada;
        public event MovimientoDelegate MovimientoSeleccionado;
        public event MovimientoDelegate MovimientoEliminado;
        public event MovimientoDelegate MovimientoAgregado;
        public event ProductoDelegate ProductoSeleccionado;
        public event ProductoDelegate ProductoAgregado;
        public event DestinatarioDelegate DestinatarioSeleccionado;
        public event DestinatarioDelegate DestinatarioCreado;
        public event PagoDelegate PagoAgregado;
        public event PagoDelegate PagoEliminado;
        public event CajaDelegate CajaSeleccionada;
        public event InfoHtmlDelegate InfoHtml;
        public event TratamientoDelegate TratamientoSeleccionado;
        public event TratamientoDelegate TratamientoEliminado;
        public event TratamientoDelegate TratamientoCreado;
        public event ProgressDelegate ProgresoGlobal;
        bool _flagLoaded;
        bool _flagloguiando;
        public bool Loaded
        {
            get
            {
                return _flagLoaded;

            }
        }
        public bool iniciar()
        {
            if(DateTime.Now > new DateTime(2021,1,1))
            {
                logger.Singlenton.addWarningMsg("El tiempo de prueba ha terminado utilice otra instalacion.");
                return false;
            }
            if (_flagloguiando) return false;
            _flagloguiando = true;
            if (conectar())
            {
                if (login())
                {
                    _flagloguiando = false;

                    gLogger.logger.Singlenton.addMessage("Ha iniciado sesión como " + Usuario.Nombre +". Para cerrar sesión puede hacerlo desde el icono junto al reloj del sistema", "Bienvenido");
                    if (!CajaBloqueada(false))
                    {
                        return recargar(async (o, e) =>
                        {
                            // Al terminar loguea en internet
                            if (WebManager.Singlenton.testInternet())
                            {
                                await WebManager.Singlenton.LoginServerAsync(false, false);
                                WebManager.Singlenton.goHome();
                            }
                                
                        });
                    }


                }
                else
                {
                    logger.Singlenton.addWarningMsg("No se pudo iniciar sesión por que las credenciales no coinciden.");
                    Estado = EstadoSistemaEnum.Conectado;

                }
               
            }
            else
            {
                Estado = EstadoSistemaEnum.Desconectado;
            }
            _flagloguiando = false;

            return false;
       
        }
        public bool recargar(EventHandler finalizar)
        {
            if (_flagloguiando) return false;
            _flagloguiando = true;
            TaskManager.CrearTareaAislada(crearTree, (sndr, e) =>
            {
                if (e.Cancelled || e.Error != null)
                {
                    Estado = EstadoSistemaEnum.Cancelado;
                }
                else
                {
                    _flagLoaded = true;
                    CajaManager.Singlenton.IniciarCaja();
                    finalizar(null, null);
                    // comprueba los sincronizados asyncronicamente
                    
                    Estado = EstadoSistemaEnum.Cargado;

                }


            }, (percent, desc) => CoreManager.Singlenton.InformarProgreso(percent, desc));
            _flagloguiando = false;
            return true;
        }
        public bool CajaBloqueada(bool reset = true)
        {
            bool ret = !Connector.Singlenton.ConexionActiva.TengoPermisoBloqueo(CoreManager.Singlenton.Usuario.Id, CoreManager.Singlenton.Key.Token);
            if (ret)
            {
                logger.Singlenton.addErrorMsg("El sistema esta bloqueado por cierre.");
                if (reset)reiniciar();
            }
            return ret;
        }
        public bool BloquearCajaParaCierre()
        {
            bool ret = Connector.Singlenton.ConexionActiva.bloquearCierre(CoreManager.Singlenton.Usuario.Id, CoreManager.Singlenton.Key.Token);
            if (!ret)
            {
                logger.Singlenton.addErrorMsg("No puede bloquear por que el sistema ya esta bloqueado por cierre.");
                reiniciar();
            }
            return ret;
        }
        public bool DesbloquearCajaParaCierre()
        {
            bool ret = Connector.Singlenton.ConexionActiva.desbloquearCierre(CoreManager.Singlenton.Usuario.Id, CoreManager.Singlenton.Key.Token);
            if (!ret)
            {
                logger.Singlenton.addErrorMsg("Ni puede desbloquear el sistema por que no fue bloqueado desde aqui.");
            }
            return ret;
        }
        public bool conectar()
        {
            var t = gConnector.Connector.Singlenton.Iniciar();
            switch (t)
            {
                case Conexion.ConexionEstadoEnum.Desconectado:
                    Estado = EstadoSistemaEnum.Desconectado;
                    return false;
                case Conexion.ConexionEstadoEnum.Conectado:
                    Estado = EstadoSistemaEnum.Conectado;
                    return true;

            }
            return false;
        }
        public keyObject Key
        {
            get
            {
               return KeyManager.Singlenton.Key ;
               
            }
        }
        
        public void reiniciar()
        {
            terminar();
            iniciar();
        }
        
        public void terminar()
        {
            _flagLoaded = false;
            UsuarioManager.Singlenton.logout();
            gConnector.Connector.Singlenton.terminar();
            Estado = EstadoSistemaEnum.Desconectado;
        }
        public bool login()
        {
            if (UsuarioManager.Singlenton.login())
            {
                Estado = EstadoSistemaEnum.Autenticado;
                SesionIniciada?.Invoke(Usuario);
                return true;
            }
            return false;
        }
        public void logout()
        {
            var u=Usuario;
            UsuarioManager.Singlenton.logout();
            if (u != null)
            {
                //aqui solo cambia estado si estaba autenticado
                Estado = EstadoSistemaEnum.Cargado;
            }
            SesionFinalizada?.Invoke(u);
            Connector.Singlenton.resetUser();
            reiniciar();


        }
        public void cambiarConexion()
        {
            terminar();
            gConnector.Connector.Singlenton.SelecionarConexion() ;
        }
        

        private void loguear(System.ComponentModel.BackgroundWorker worker = null)
        { 
        
        }
        private EstadoSistemaEnum _estado = EstadoSistemaEnum.Desconectado;
        public EstadoSistemaEnum Estado
        {
            get
            {
                return _estado;
            }
            set
            {
                if (value != _estado)
                {
                    _estado = value;
                    if (CambiodeEstado != null) CambiodeEstado(_estado);
                }
            }
        }
        private void crearTree(System.ComponentModel.BackgroundWorker worker = null)
        {
           
            DataTable datProductos = Connector.Singlenton.ConexionActiva.ConsultarProductos();
            DataTable datMovimientos = Connector.Singlenton.ConexionActiva.ConsultarMovimientos();
            DataTable datObjetosMovidos = Connector.Singlenton.ConexionActiva.ConsultarObjetosMovidos();
            DataTable datPagos = Connector.Singlenton.ConexionActiva.ConsultarPagos();
            DataTable datDestinatarios = Connector.Singlenton.ConexionActiva.ConsultarDestinatarios();
            DataTable datUsuarios = Connector.Singlenton.ConexionActiva.ConsultarUsuarios();
           
                 
            DataTable datTratamientos = Connector.Singlenton.ConexionActiva.ConsultarTratamientos();
            int totalcount = datProductos.Rows.Count +
                datMovimientos.Rows.Count +
                datObjetosMovidos.Rows.Count +
                datPagos.Rows.Count +
                datDestinatarios.Rows.Count +
                datUsuarios.Rows.Count
               
                ;
            int cont=0;
            //limpiar las cajas
            CajaManager.Singlenton.resetCajas();
            ProductoManager.Singlenton.reload(worker, datProductos,ref cont, totalcount);
            MovimientoManager.Singlenton.reloadMovimientos(worker, datMovimientos, ref cont, totalcount);
            MovimientoManager.Singlenton.reloadObjetosMovidos(worker, datObjetosMovidos, ref cont, totalcount);
            DestinatarioManager.Singlenton.reload(worker, datDestinatarios, ref  cont, totalcount);
            DestinatarioManager.Singlenton.reloadTratamientos(worker, datTratamientos, ref  cont, totalcount);
           
            CajaManager.Singlenton.reloadPagos(worker, datPagos,ref cont, totalcount);
            UsuarioManager.Singlenton.reload(worker, datUsuarios, ref  cont, totalcount);
            //sincronizacion
           /* if(datSyncprod!=null) ProductoManager.Singlenton.
                    reloadSincronizados(worker, datSyncprod, ref cont, totalcount);*/
            //-----
            if (worker != null)
            {
                worker.ReportProgress(100);
            }
          
        }
        #region ACCIONES
        public void seleccionarGenerico(object o)
        {
            if (o is gcCaja)
                seleccionarCaja(o as gcCaja);
            else if (o is gcMovimiento)
                seleccionarMovimiento(o as gcMovimiento);
            else if (o is gcObjeto)
                seleccionarProducto(o as gcObjeto);
            else if (o is gcDestinatario)
                seleccionarDestinatario(o as gcDestinatario);
            else if (o is ProductoResumen)
                seleccionarProducto((o as ProductoResumen).Producto);
        }
        public void seleccionarCaja(gcCaja c)
        {
            if (c != null && CajaSeleccionada != null) CajaSeleccionada(c);
            
        }
        public void CambiarACaja(gcCaja c)
        {
            if (c == null) return;
            CajaManager.Singlenton.CambiarACaja(c.Caja);
        }
        public void seleccionarMovimiento(gcMovimiento m)
        {
            if (m != null && MovimientoSeleccionado != null)
                MovimientoSeleccionado(m);
        }
        internal void eliminarMovimiento(gcMovimiento m)
        {
            if (m != null && MovimientoEliminado != null)
                MovimientoEliminado(m);
        }
        internal void agregarMovimiento(gcMovimiento m)
        {
            if (m != null && MovimientoAgregado != null)
                MovimientoAgregado(m);
        }
        public void seleccionarProducto(gcObjeto o)
        {
            if (o != null && ProductoSeleccionado != null)
                ProductoSeleccionado(o);
        }
        public void agregarProducto(gcObjeto o)
        {
            if (o != null && ProductoAgregado!= null)
                ProductoAgregado(o);
        }
        public void seleccionarDestinatario(gcDestinatario d)
        {
            if (d != null && DestinatarioSeleccionado != null)
            {
                DestinatarioSeleccionado(d);
            }
        }
        public void CreadoDestinatario(gcDestinatario d)
        {
            if (d != null && DestinatarioCreado != null)
            {
                DestinatarioCreado(d);
            }
        }
        public void SeleccionarTratamiento(gcTratamiento t)
        {
            if (TratamientoSeleccionado != null) TratamientoSeleccionado(t);
        }
        internal void EliminarTratamiento(gcTratamiento t)
        {
            if (t != null && TratamientoEliminado != null) TratamientoEliminado(t);
        }
        internal void CrearTratamiento(gcTratamiento t)
        {
            if (t != null && TratamientoCreado != null) TratamientoCreado(t);
        }
        internal void AgregarPago(gcPago p)
        {
            if (p != null && PagoAgregado != null)
                PagoAgregado(p);
        }
        internal void EliminarPago(gcPago p)
        {
            if (p != null && PagoEliminado != null)
            {
                PagoEliminado(p);
            }
        }
        public void InformarProgreso(int percent, string msg)
        {
            if (ProgresoGlobal != null) ProgresoGlobal(percent, msg);
        }
        public gcMovimiento crearMovimiento(gcMovimiento.TipoMovEnum t, gcDestinatario d, gcUsuario user= null)
        {
            /*
            foreach (gcMovimiento ms in CajaManager.Singlenton.CajaActual.Movimientos)
            {
                if (ms.Tipo == t && ms.Estado == gcMovimiento.EstadoEnum.Creado)
                {
                    gLogger.logger.Singlenton.addWarningMsg("Tiene creado un movimiento de tipo " + t.ToString() + " sin terminar.");
                    break;
                }
            }*/
            return MovimientoManager.Singlenton.crearNuevoMovimiento(t, d, user);
        }
        public void GenerarInformeHtml(string t)
        {
            InfoHtml?.Invoke(t);
        }
        #endregion


        #region FUNCIONES
        //destinatarios
        public List<gcDestinatario> getDestinatarios(gcDestinatario.DestinatarioTipo tipo)
        {
            if (Loaded)
            {
                return DestinatarioManager.Singlenton.getDestinatarios(tipo);
            }
            return new List<gcDestinatario>();
        }
        public List<gcDestinatario> getDestinatarios()
        {
            if (Loaded)
            {
                return DestinatarioManager.Singlenton.getDestinatarios();
            }
            return new List<gcDestinatario>();
        }
        public List<tuplaPagos> getDestinatarioSeguimiento(gcDestinatario d,List<gcCaja> cajas)
        {
             return CajaManager.Singlenton.SeguimientoDestinatario(d,cajas);
        }
        public List<CuentaCorrienteResumen> MovimientosEnCuentasCorrientes(List<gcCaja> cajas,gcDestinatario d=null)
        {
            
            return CajaManager.Singlenton.MovimientosEnCuentasCorrientes(cajas,d);
           
        }
        public List<tuplaPagos> getDestinatarioSeguimiento(gcDestinatario d)
        {
            var c = CajaManager.Singlenton.getCajas(new Filtros.FiltroCaja(Filtros.FiltroCaja.TipoFiltroCaja.Todas));
            return CajaManager.Singlenton.SeguimientoDestinatario(d, c);
        }
        
        public List<gcDestinatario> getDestinatarios(Filtros.FiltroDestinatario f)
        {
            if (Loaded)
            {
                return DestinatarioManager.Singlenton.getDestinatarios(f);
            }
            return new List<gcDestinatario>();
        }
        public gcDestinatario getDestinatario(int id)
        {
            return DestinatarioManager.Singlenton.getDestinatario(id);
        }
        public gcDestinatario getDestinatario(int id,gcDestinatario.DestinatarioTipo tipo)
        {
            return DestinatarioManager.Singlenton.getDestinatario(id,(int)tipo);
        }
        //tratamientos
        public List<gcTratamiento> getTratamientos(Filtros.FiltroTratamientos f)
        {
            return DestinatarioManager.Singlenton.getTratamientos(f);
        }
        //Productos
        public List<ResumenBase> ListaDePrecios(Filtros.FiltroProducto f)
        {
            return ProductoManager.Singlenton.ListaDePrecios(f);
        }
        public List<gcProducto> getProductos()
        {
            return ProductoManager.Singlenton.Productos;
        }
        public List<gcObjeto> ProductosEnCategoria(gcCategoria c)
        {
            return ProductoManager.Singlenton.ProductosEnCategoria(c, false, true, true);
        }
        
        public async Task SincronizarProductos(List<gcObjeto> l,bool del=false)
        {
           
            var q = new Queue<gcObjeto>(l);
            await WebManager.Singlenton.SincronizarProductos(q,q.Count,del);
            if(l.Count > 1)
            {
                string sinc = del ? "desincronizaron" : "sincronizaron";
                logger.Singlenton.addDebugMsg("Sincronizacion", "Se " + sinc + " " + l.Count + " productos");

            }

        }
        public bool EliminarProductos(List<gcObjeto> l)
        {
            bool ret = true;
            foreach (gcObjeto o in l)
            {
                ret &= o.DeleteMe();
            }
            return ret;
        }
        public List<gcObjeto> getProductosSincronizados()
        {
            return ProductoManager.Singlenton.Sincronizados;
        }
        public List<gcObjeto> getProductos(Filtros.FiltroProducto f)
        {
            return ProductoManager.Singlenton.getObjetos(f);
        }
        public List<gcCategoria> getCategorias()
        {
            return ProductoManager.Singlenton.Categorias;
        }
        public List<gcServicio> getServicios()
        {
            return ProductoManager.Singlenton.Servicios;
        }
        public List<gcGrupo> getGrupos()
        {
            return ProductoManager.Singlenton.Grupos;
        }
        public List<gcObjeto> getProductosYServicios()
        {
            return ProductoManager.Singlenton.ProductosYServicios;
        }
        public List<gcObjeto> getProductosTodos()
        {
            return ProductoManager.Singlenton.Objetos;
        }
        public gcObjeto getProducto(int id)
        {
            return ProductoManager.Singlenton.getObjeto(id);
        }
        public gcObjeto getProducto(string serial)
        {
            return ProductoManager.Singlenton.getObjeto(serial);
        }
        public List<gcObjeto> getCategoriasPrincipales
        {
            get
            {
                var f = new Filtros.FiltroProducto();
                //f.Tipos.Add(gcObjeto.ObjetoTIpo.Categoria);
                f.Parent = 0;
                //f.Tipos.Add(gcObjeto.ObjetoTIpo.Categoria);
                //f.Tipos.Add(gcObjeto.ObjetoTIpo.Servicio);
                //f.Tipos.Add(gcObjeto.ObjetoTIpo.Producto);
                var l = ProductoManager.Singlenton.getObjetos(f);
                var c = new gcCategoria();
                c.Nombre = "DESCATEGORIZADOS";
                c.Descripcion = "Productos y servicios sin categorias";
               // l.Add(c);
                return l ;
            }
        }
        //usuario
        public gcUsuario Usuario
        {
            get
            {
                return UsuarioManager.Singlenton.UsuarioActual;
            }
        }
        public gcUsuario.PermisoEnum PermisoActual
        {
            get
            {
                if (Usuario == null)
                {
                    return gcUsuario.PermisoEnum.Visitante;
                }
                return Usuario.Permiso;
            }
        }
        public bool TienePermiso(gcUsuario.PermisoEnum p)
        {
            return (int)p >= (int)PermisoActual;
        }
        public bool ElUsuarioSuperaElPermiso(gcUsuario.PermisoEnum p)
        {
            return (int)p <= (int)PermisoActual;
        }
        public gcUsuario getUsuario(int id)
        {
            return UsuarioManager.Singlenton.getUsuario(id);
        }
        public List<gcUsuario> getUsuarios()
        {
            return UsuarioManager.Singlenton.getUsuarios();
        }
        //movimientos
        public List<gcMovimiento> getMovimientos()
        {
            return MovimientoManager.Singlenton.getMovimientos();
        }
        public List<gcMovimiento> getMovimientos(Filtros.FiltroMovimiento f)
        {
            return MovimientoManager.Singlenton.getMovimientos(f);
        }
        public List<gcMovimiento> getMovimientosCajaActual()
        {
            return new List<gcMovimiento>(CajaManager.Singlenton.CajaActual.Movimientos);
        }
        public gcMovimiento getMovimientoById(int id)
        {
            return MovimientoManager.Singlenton.getMovimiento(id);
        }
        public List<ProductoResumen> getMovimientosConsumo(Filtros.FiltroMovimiento f)
        {
            return MovimientoManager.Singlenton.ConsumoEnMovimientos(f);
        }
        public List<ProductoResumen> getDestinatarioConsumo(gcDestinatario d)
        {
            if (d == null) return new List<ProductoResumen>();
            Filtros.FiltroMovimiento f = new Filtros.FiltroMovimiento();
            f.Destinatario = d;
            f.setTodasLasCajas();
            return MovimientoManager.Singlenton.ConsumoEnMovimientos(f);
        }
        
        //caja
        public gcCaja CajaActual
        {
            get
            {
                return CajaManager.Singlenton.CajaActual;
            }
        }
        public List<gcCaja> getCajas()
        {
            return getCajas(new Filtros.FiltroCaja(Filtros.FiltroCaja.TipoFiltroCaja.Todas));
        }
        public List<gcCaja> getCajas(Filtros.FiltroCaja f)
        {
            return CajaManager.Singlenton.getCajas(f);
        }
        public List<ResumenBase> ResumenVentasPorCats(List<gcCaja> cajas)
        {

            return CajaManager.Singlenton.ResumenVentasPorCats(cajas);
        }
        //herramientas
        //private CachedCsvReader LeerExcel(string path)
        //{
        //    using (CachedCsvReader csv = new CachedCsvReader(new StreamReader(path), true))
        //    {
              
        //    }

        //}
        //public List<ProductoUpdate> ProductosEnCSV(string path)
        //{
        //    var l = new List<ProductoUpdate>();
        //     using (CachedCsvReader csv = new CachedCsvReader(new StreamReader(path,Encoding.Default), true,","[0]))
        //    {
        //        int fieldCount =  csv.FieldCount;
        //        int codindex = -1;
        //        int costoindex = -1;
        //        int nomindex = -1;
        //        int cotiindex = -1;
        //        int ganindex = -1;
                 

        //        string[] headers = csv.GetFieldHeaders();
        //        for (int i = 0; i < fieldCount; i++)
        //        {
        //            if (headers[i].ToLower() == "codigo")
        //            {
        //                codindex = i;
        //                continue;
        //            }
        //            if (headers[i].ToLower() == "costo")
        //            {
        //                costoindex = i;
        //                continue;
        //            }
        //            if (headers[i].ToLower() == "nombre")
        //            {
        //                nomindex = i;
        //                continue;
        //            }
        //            if (headers[i].ToLower() == "cotizacion")
        //            {
        //                cotiindex = i;
        //                continue;
        //            }
        //            if (headers[i].ToLower() == "ganancia")
        //            {
        //                ganindex = i;
        //                continue;
        //            }
        //        }
        //        if (codindex <0)
        //        {
        //            gLogger.logger.Singlenton.addErrorMsg("No existe una columna llamada 'codigo' (sin acento)");
        //            return l;
        //        }
          
        //        while (csv.ReadNextRecord())
        //        {
        //            string nombre="Desconocido";
        //            if (nomindex >= 0)
        //                nombre = csv[nomindex];
        //            string codigo = "";
        //            codigo = csv[codindex];

        //            decimal ganancia = 0;
        //            if (ganindex  >= 0)
        //                Decimal.TryParse(csv[ganindex], out ganancia);

        //            decimal costo = 0;
        //            if (costoindex >= 0)
        //                Decimal.TryParse(csv[costoindex], out costo);

        //            decimal cotiz = 1;
        //            if(cotiindex>=0)
        //                 Decimal.TryParse(csv[cotiindex], out cotiz);

        //            var np = new ProductoUpdate(nombre) { Codigo = codigo, Costo = costo, Cotizacion = cotiz, Ganancia=ganancia};
        //            //if (np.EsConocido)
        //            //    ac.SubItems.Add(np);
        //            //else
        //            //    des.SubItems.Add(np);
        //            l.Add(np);

        //        }
        //    }
            
        //     return l;
        //}
        public List<ProductoUpdate> ProductosEnXML(string path)
        {
            var l = new List<ProductoUpdate>();

            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlNode node = doc.DocumentElement.SelectSingleNode("/book/title");
            return l;
        }
        public List<ProductoUpdate> ProductosEnExcel(string path)
        {
            var l = new List<ProductoUpdate>();
            var excel = new LectorExcel();
            l = excel.ToEntidadHojaExcelList(path);


            
            return l;
        }
        //registro
        public async void RegistrarseAsync()
        {
            var k = Key;
           await WebManager.Singlenton.LoginServerAsync(String.IsNullOrEmpty(k.Usuario) || String.IsNullOrEmpty(k.Password), true);

        }
        public bool loginInternet(bool reset = false)
        {
            if (reset) KeyManager.Singlenton.resetKey();
            return KeyManager.Singlenton.Login();
        }

        #endregion
        //sincronizar
        ConexionSqlite _syncdb = new ConexionSqlite("sync.db");
        internal ConexionSqlite Syncdb
        {
            get
            {
                return _syncdb;
            }
        }
       
       
       

    }
}
