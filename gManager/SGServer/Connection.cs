﻿using System;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SGServer
{
    public class Connection
    {
        public event EventHandler closeMe;
        private Socket _socket;
        int _id = 0;
        
        public Connection(Socket socket, int id)
        {
            _id = id;
            StateObject state = new StateObject();
            state.workSocket = socket;
            socket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
            _socket = socket;
        }
        public int Id
        {
            get { return _id; }
        }

        public void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket.   
            try
            {
                int bytesRead = handler.EndReceive(ar);


                if (bytesRead > 0)
                {
                    // There  might be more data, so store the data received so far.  
                    state.sb.Append(Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead));

                    // Check for end-of-file tag. If it is not there, read   
                    // more data.  
                    content = state.sb.ToString();
                    int offset = content.IndexOf("<EOF>");
                    if (offset > -1)
                    {
                        check(content.Remove(offset));
                    }
                    else
                    {
                        // Not all data received. Get more.  
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReadCallback), state);
                    }
                }
            } catch (Exception e)
            {
                gLogger.logger.Singlenton.addErrorMsg(e.Message);
                Close();
            }
        }
        private void check(string data)
        {
            sgResponse resp = new sgResponse(data);
            switch (resp.Code)
            {
                case 101:
                    resp = checkRequest.user(resp);
                    break;
                case 102:
                    resp = checkRequest.productos(resp);
                    break;
                case 103:
                    resp = checkRequest.producto(resp);
                    break;
                case 104:
                    resp = checkRequest.movimientos(resp);
                    break;
                case 105:
                    resp = checkRequest.crearVenta(resp);
                    break;

            }
            Send(resp.ToString());

            
        }
        public void Send(String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            _socket.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), _socket);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(ar);
               // gLogger.logger.Singlenton.addInfoMsg("Sent " + bytesSent + " bytes to client.");

                

            }
            catch (Exception)
            {
               // gLogger.logger.Singlenton.addErrorMsg(e.Message);
                Close();
            }
        }
        public void Close()
        {
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
            if (closeMe != null)
            {
                closeMe(this,null);
            }
        }
    }
}
