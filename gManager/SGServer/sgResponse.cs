﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGServer
{
    public class sgResponse
    {
        public sgResponse(string data)
        {
            var codee = data.Substring(0, 3);
            string json = data.Substring(3);
            int code = 0;
            int.TryParse(codee, out code);
            if (code > 0)
            {
                Code = code;
                Data = json;
               
            } else
            {
                Code = 400;
                Data = "Bad request";
                    
            }
            
        }
        public sgResponse(int code, string data)
        {
            Code = code;
            Data = data;
        }
        public int Code { get; set; }
        public string Data { get; set; }
        public JObject DataToJobject()
        {

            return JObject.Parse(Data);
        }
        public override string ToString()
        {
            return Code + Data + "<EOF>";
        }
    }
}
