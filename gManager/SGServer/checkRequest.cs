﻿using gManager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGServer
{
    public static class checkRequest
    {
        public static sgResponse user(sgResponse resp)
        {
            JObject data = resp.DataToJobject();
            JToken user = data.GetValue("user");
            JToken pass = data.GetValue("pass");
            gManager.gcUsuario sguser = null;
            try
            {
                sguser = gManager.CoreManager.Singlenton.getUsuarios().First(u => u.User == user.Value<string>() && u.Pass == pass.Value<string>());
                resp.Data = JsonConvert.SerializeObject(sguser);
                resp.Code = 200;
            }
            catch(Exception)
            {
                resp.Data = "Unauthorized";
                resp.Code = 401;
            }

            return resp;
        }
        public static sgResponse productos(sgResponse resp)
        {
            try
            {
                resp.Data = JsonConvert.SerializeObject(gManager.CoreManager.Singlenton.getProductos());
                resp.Code = 200;
            }
            catch (Exception)
            {
                resp.Data = "Error obteniendo la lista de productos";
                resp.Code = 400;
            }

            return resp;
        }
        public static sgResponse producto(sgResponse resp)
        {
            JObject data = resp.DataToJobject();
            JToken id = data.GetValue("id");
            gManager.gcObjeto p = gManager.CoreManager.Singlenton.getProducto(id.Value<int>());
            if(p != null)
            {
                try
                {
                    resp.Data = JsonConvert.SerializeObject(p);
                    resp.Code = 200;
                }
                catch (Exception)
                {
                    resp.Data = "Error obteniendo el producto";
                    resp.Code = 400;
                }
            } else
            {
                resp.Data = "Error obteniendo el producto";
                resp.Code = 400;
            }

            return resp;
        }
        public static sgResponse movimientos(sgResponse resp)
        {
            JObject data = resp.DataToJobject();
            JToken id = data.GetValue("user");
            List<gcMovimiento> list = gManager.CoreManager.Singlenton.getMovimientosCajaActual();

            try
            {
                resp.Data = JsonConvert.SerializeObject(list);
                resp.Code = 200;
            }
            catch (Exception)
            {
                resp.Data = "Error obteniendo la lista de movimientos";
                resp.Code = 400;
            }

            return resp;
        }
        public static sgResponse crearVenta(sgResponse resp)
        {
            JObject data = resp.DataToJobject();
            JToken user = data.GetValue("user_id");
            JToken client = data.GetValue("client_id");
            gcDestinatario cliente = gManager.CoreManager.Singlenton.getDestinatario(client.Value<int>(),gcDestinatario.DestinatarioTipo.Cliente);
            gcUsuario usuario = gManager.CoreManager.Singlenton.getUsuario(user.Value<int>());
            
            gcMovimiento mov = gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Venta, cliente, usuario);

            try
            {
                if (mov == null) throw new Exception("No se creo el movimiento");
                mov.saveMe();
                resp.Data = JsonConvert.SerializeObject(mov);
                resp.Code = 200;
            }
            catch (Exception e)
            {
                resp.Data = e.Message;
                resp.Code = 400;
            }

            return resp;
        }
    }
}
