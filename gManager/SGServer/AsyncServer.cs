﻿using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SGServer
{
    public class AsyncServer
    {
        public ManualResetEvent allDone = new ManualResetEvent(false);
        private int _nextid = 0;
        Hashtable _clients = new Hashtable();
        private Socket _socket = null;

        private static AsyncServer _instance;
        private AsyncServer()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.  
            // The DNS name of the computer  
            // running the listener is "host.contoso.com".  
            IPHostEntry ipHostInfo = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = ipHostInfo.AddressList[1];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP socket.  
            _socket = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            
            try
            {
                _socket.Bind(localEndPoint);
                _socket.Listen(100);

            }
            catch (Exception e)
            {
                gLogger.logger.Singlenton.addErrorMsg(e.ToString());
            }
        }
        public static AsyncServer Singlenton
        {
            get
            {
                if (_instance == null) _instance = new AsyncServer();
                return _instance;
            }
        }
        private int nextId()
        {
            _nextid++;
            return _nextid;
        }

        public void StartListening()
        {
            

            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {
                listenAgain();
                gLogger.logger.Singlenton.addDebugMsg("Servidor","El sistema esta escuchando conexiones de la red...");
            }
            catch (Exception e)
            {
                gLogger.logger.Singlenton.addErrorMsg(e.ToString());
            }



        }
        public void listenAgain()
        {
            // Set the event to nonsignaled state.  
            allDone.Reset();
            
            _socket.BeginAccept(
                new AsyncCallback(AcceptCallback),
                _socket);

           
        }

        public void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.  
            allDone.Set();

            // Get the socket that handles the client request.  
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
            Connection cnn = new Connection(handler, nextId());
            _clients.Add(cnn.Id, cnn);
            cnn.closeMe += (c, b) =>
            {
                Connection con = c as Connection;
                if (_clients.ContainsKey(con.Id))
                {
                    _clients.Remove(con.Id);
                }
            };
            gLogger.logger.Singlenton.addDebugMsg("Servidor", "Se conectó "+cnn.Id);
            listenAgain();

            // Create the state object.  

        }
    }
}
