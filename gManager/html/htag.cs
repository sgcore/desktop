﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gManager.html
{
    public class htag
    {
        protected string _tname = "div";
        protected string _content = "";
        protected string _className = "";
        protected string _style = "";
        protected string _attribute = "";
        protected string _action = "";
        public htag()
        {

        }
        public htag tagName(string tagname)
        {
            _tname = tagname;
            return this;
        }
        public htag action(string action, string value)
        {
            
            _action += " " + action + "=\"" + value +"\"";
            return this;
        }
        public htag contentMetrica()
        {
            //editar cantidad
            string[] medida = _content.Split(" ".ToCharArray());
            if (medida.Length > 1)
            {
                _content =
                    new htag().content(medida[0]).className("big").toSpan()
                    + new htag().content(" " + medida[1]).className("micro").toSpan();
            }
            return this;
        }
        public htag content(string content)
        {
            _content = content;
            return this;
        }
        public htag content(htag content)
        {
            _content = content.ToString();
            return this;
        }
        public htag insertbefore(htag content)
        {
            _content = content.ToString() + _content;
            return this;
        }
        public htag insertbefore(string content)
        {
            _content = content + _content;
            return this;
        }
        public htag insertafter(htag content)
        {
            _content = _content + content.ToString() ;
            return this;
        }
        public htag insertafter(string content)
        {
            _content =   _content + content;
            return this;
        }
        public htag className(string className)
        {
            if(!_className.Contains(className))
                _className += " " +className;
            return this;
               
        }
        public htag style(string key, string value)
        {
            _style += key + ": " + value + ";";
            return this;
        }
        public htag attribute(string key, string value)
        {
            _attribute += " " +key + "=\"" + value + "\"";
            return this;
        }
        public htag width(uint width)
        {
            return attribute("width", width.ToString());
        }
        public virtual string toTd()
        {
            _tname = "td";
            return ToString();
        }
        public virtual string toH1()
        {
            _tname = "h1";
            return ToString();
        }
        public virtual string toH3()
        {
            _tname = "h3";
            return ToString();
        }
        public virtual string toH4()
        {
            _tname = "h4";
            return ToString();
        }
        public virtual string toH5()
        {
            _tname = "h5";
            return ToString();
        }
        public virtual string toSpan()
        {
            _tname = "span";
            return ToString();
        }
        public virtual string justOpen()
        {
            string cn = "";
            string st = "";
            if (!String.IsNullOrEmpty(_className))
            {
                cn = " class =\"" + _className + "\"";
            }
            if (!String.IsNullOrEmpty(_style))
            {
                st = " style =\"" + _style + "\"";
            }
            return "<" + _tname + " " + cn + " " + _attribute + " " + st + " >" + (!String.IsNullOrEmpty(_action) ? "<a  " + _action + ">" : "");
        }
        public virtual string justClose()
        {
            return (!String.IsNullOrEmpty(_action) ? "</a>" : "") + "</" + _tname + ">";
        }
        public override string ToString()
        {
            
            return justOpen() + _content + justClose();
        }
    }
}
