﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace gManager.html
{
    public abstract class htmlObject
    {
        protected StringBuilder  _body = new StringBuilder();
        
        protected void addRow(string p)
        {
            _body.Append("<tr>" + p + "</tr>.");
        }
        protected void addSeparator(int heigth = 16, int line = 0)
        {
            _body.Append(getSeparator(heigth, line));
        }
        protected htag getSeparator(int heigth = 16, int line = 0)
        {
            return new htag().attribute("height", heigth.ToString() + "px")
                .className("separator")
                .style("border-top-width",line.ToString() + "px");

        }
        protected void addTitulo(string p)
        {
            _body.Append(" <h1>" + p.ToUpper() + "</h1>");
        }
        protected void addSubTitulo(string p)
        {
            _body.Append(" <h3>" + p.ToUpper() + "</h3>");
        }
        protected void openTable(string className = "")
        {
            _body.Append("<table class =\"" + className +"\" >");
        }
        protected void closeTable()
        {
            _body.Append("</table>");
        }

        protected void addImage(System.Drawing.Bitmap bitmap, int width=16, int height=16)
        {
            _body.Append(Utils.ImageToHtml(bitmap,width, height));
        }
        protected string getImage(System.Drawing.Bitmap bitmap, int width = 16, int height = 16)
        {
            return(Utils.ImageToHtml(bitmap, width, height));
        }

        protected void addHeader(string[] cols, string title = "")
        {
            if (!String.IsNullOrEmpty(title))
            {
                addRow(new htag().content(title).className("micro centrado observacion").attribute("colspan", cols.Length.ToString()).toTd());
            }
            string t = "<tr>";
            string[] l = new List<string>(cols).FindAll(s => !String.IsNullOrEmpty(s)).ToArray();
            for(int i = 0; i < l.Length; i++)
            {
                string left = "0px";
                string rigth = "0px";
                if(i == 0)
                {
                    left = "10px";
                }
                if(i == l.Length - 1)
                {
                    rigth = "10px";
                }
                t += new htag().content(l[i])
                    .className("micro observacion presupuesto")
                    .style("corner-radius", rigth + " " + left + " 0px 0px")
                    .style("border", "1px solid #647fca")
                    .style("padding", "5px").toTd();



            }
            /*foreach (string s in l)
             {
                 t += "<td class =\"header\">" + s + " </td>";
             }
            */
            _body.Append(t + "</tr>");
        }
        protected void addHtmObject(htmlObject obj)
        {
            _body.Append(obj.ToString());
        }
        protected void addHtmObject(htag obj)
        {
            _body.Append(obj.ToString());
        }

        protected void addCita(string texto, System.Drawing.Bitmap icon)
        {
            addHtmObject(
                    new htag().content(
                         new htag().content(getImage(icon, 24, 24)).toSpan()

                    + new htag().tagName("span").content(texto).style("display", "inline-block")

                    .ToString()
                        ));
        }
        protected void addMessage(gLogger.gcMessage m)
        {
            Bitmap img = m.CustomImage != null ? (Bitmap)m.CustomImage : new Bitmap(16,16);
            string colorclass = "normal";
            switch (m.Tipo)
            {
                case gLogger.gcMessage.MessageType.ERROR:
                    colorclass = "critical";
                    break;
                case gLogger.gcMessage.MessageType.WARNING:
                    colorclass = "low";
                    break;
                case gLogger.gcMessage.MessageType.SYSTEM:
                case gLogger.gcMessage.MessageType.DEBUG:
                    colorclass = "high";
                    break;
                default:
                    colorclass = "normal";
                    break;


            }
            addRow(
                new htag().content(getImage(img, 20, 20)).attribute("width", "20px").attribute("rowspan", "2").toTd()
                + new htag().content(m.Titulo).className("centrado micro " + colorclass).attribute("width", "20%").toTd()
                + new htag().content(m.Mensaje).className("small").attribute("rowspan", "2").toTd()
                );
            addRow(
                new htag().content(m.FechaToString()).className("micro ").toTd()
                );
            addRow(getSeparator(4, 1).toTd() + getSeparator(4, 1).toTd() + getSeparator(4,1).toTd());

        }
        public void openPanel(string title, int cols = 1)
        {
            _body.Append(new htag().className("panel").justOpen());
            _body.Append(new htag().content(new htag().content(title)).toH3());
            addSeparator(4,2);
            openTable();
            


        }
        public void closePanel()
        {
            
            closeTable();
            _body.Append(new htag().justClose());
        }
        protected void addBreadcrum(string path)
        {
            string[] batchstring = path.Split("@".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            _body.Append("<div  class=\"breadcontainer\">");
            int i = batchstring.Length - 1;
            foreach(string item in batchstring)
            {
                if (i == 0)
                {
                    _body.Append("<span class=\"breadlast\">" + item + "</span>");
                } else
                {
                    _body.Append("<span class=\"breadcrumb\">" + item + "</span>");
                }
                i--;
            }
        }

        public bool partialRender { get; set; }

        public override string ToString()
        {
            if(partialRender)
            {
                return _body.ToString();
            }
            StringBuilder t = new StringBuilder("<body>");
            t.Append(_body);
            t.Append("</body>");
            return t.ToString();
        }
    }
}
