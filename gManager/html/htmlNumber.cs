﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gManager.html
{
    public class htmlNumber:htag
    {
        string _stateClass = "normal";

        public htmlNumber(decimal monto, decimal umbral=0, decimal critic = 0, bool filleable = false) : base()
        {
            if (filleable)
            {
                if (monto < umbral)
                    _stateClass = "critical";
                else if (monto > umbral)
                    _stateClass = "low";
                else if (Math.Round(monto - umbral, 2) == 0)
                    _stateClass = "high";
            } else
            {
                if (monto == 0)
                    _stateClass = "zero";
                else if (Math.Round(monto - umbral, 2) == 0)
                    _stateClass = "high";
                else if (monto < critic)
                    _stateClass = "critical";
                else if (monto < umbral)
                    _stateClass = "low";
                else
                    _stateClass = "normal";

                
            }
            _content = monto.ToString("0.00");

        }
        public htmlNumber normalClass(string normal)
        {
            if(_stateClass == "normal")
            {
                _stateClass = normal;
            }
            return this;
        }
        public htmlNumber lowClass(string normal)
        {
            if (_stateClass == "low")
            {
                _stateClass = normal;
            }
            return this;
        }
        public htmlNumber criticalClass(string normal)
        {
            if (_stateClass == "critical")
            {
                _stateClass = normal;
            }

            return this;
        }
        public override string ToString()
        {
            className(_stateClass);
            return base.ToString();
        }

    }
}
