﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using gLogger;
using gConnector;

namespace gManager
{
    internal class DestinatarioManager
    {
        private static DestinatarioManager _instance;
        private Hashtable _destinatarios = new Hashtable();
        private Hashtable _tratamientos = new Hashtable();
        private Dictionary<int, gcTratamiento> _avisos = new Dictionary<int, gcTratamiento>();
       
        private DestinatarioManager() { }
        public static DestinatarioManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new DestinatarioManager();
                return _instance;
            }
        }
        
        /// <summary>
        /// requiere cajas para las cuentas
        /// </summary>
        /// <returns></returns>
        public void reload(System.ComponentModel.BackgroundWorker worker, DataTable dat, ref int cont, int totalcount)
        {
           
            _destinatarios.Clear();
            foreach (var r in Enum.GetValues(typeof(gcDestinatario.DestinatarioTipo)))
                _destinatarios.Add((int)r, new SortedDictionary<int, gcDestinatario>());
            foreach (DataRow row in dat.Rows)
            {
                gcDestinatario u = new gcDestinatario(row);
                ((SortedDictionary<int, gcDestinatario>)_destinatarios[(int)u.Tipo]).Add(u.Id, u);
                //prograss
                cont++;
                if (worker != null)
                {
                    worker.ReportProgress((cont * 100 / totalcount) - 1);
                }
            }
          
          
           
        }
        
        public void reloadTratamientos(System.ComponentModel.BackgroundWorker worker, DataTable dat, ref int cont, int totalcount)
        {

            _tratamientos.Clear();
            _avisos.Clear();
            foreach (var r in Enum.GetValues(typeof(gcTratamiento.TipoTratamiento)))
                _tratamientos.Add((int)r, new SortedDictionary<int, gcTratamiento>());
            foreach (DataRow row in dat.Rows)
            {
               addTratamiento( new gcTratamiento(row));
                
                //prograss
                cont++;
                if (worker != null)
                {
                    worker.ReportProgress((cont * 100 / totalcount) - 1);
                }
            }



        }
        public void addTratamiento(gcTratamiento t)
        {
            var tr = (SortedDictionary<int, gcTratamiento>)_tratamientos[(int)t.Tipo];
            if (!tr.ContainsKey(t.Id))
                tr.Add(t.Id, t);
            else
                tr[t.Id] = t;

            getDestinatario(t.PacienteID).addTratamiento(t);
            if (t.Tipo != gcTratamiento.TipoTratamiento.Tratamiento)
            {
                var m = new gLogger.gcAviso(t.Titulo, t.Descripcion, t.Id);
                m.Start = t.Fecha;
                m.End = t.Proximo;
                gLogger.AvisoManager.Singlenton.addAviso(m);
            }
        }
        
        
        public List<gcTratamiento> getTratamientos(Filtros.FiltroTratamientos f)
        {
            if (_tratamientos.Count == 0) return new List<gcTratamiento>();
            if (f == null || !f.Filtrado)
            {
                List<gcTratamiento> rett = new List<gcTratamiento>();
                foreach (SortedDictionary<int, gcTratamiento> dic in _tratamientos.Values)
                {
                    
                    rett.AddRange(dic.Values);
                }
                return rett.OrderBy(o => o.Proximo).ToList();
            }

           //tipos
            var tipos = f.Tipos;
            var estados= f.Estados;
            var tdt=Enum.GetValues(typeof(gcTratamiento.TipoTratamiento));
            var std=Enum.GetValues(typeof(gcTratamiento.EstadoSeguimientoEnum));
            
            if (tipos.Count == 0) tipos.AddRange(tdt.OfType<gcTratamiento.TipoTratamiento>());
            if (estados.Count == 0) estados.AddRange(std.OfType<gcTratamiento.EstadoSeguimientoEnum>());

            //destinatarios
            var destinas = f.Destinatarios;
            List<gcTratamiento> objs = new List<gcTratamiento>();
            foreach (gcTratamiento.TipoTratamiento t in tipos)
            {
                var ls = ((SortedDictionary<int, gcTratamiento>)_tratamientos[(int)t]).Values.ToList<gcTratamiento>();
                objs.AddRange(ls);
            }
            if(!f.Filtrado)return objs;

            List<gcTratamiento> ret = new List<gcTratamiento>();
            foreach (gcTratamiento t in objs)
            {
                if (!t.Titulo.Contains(f.Texto) && !t.Descripcion.Contains(f.Texto)) continue;
                if (!f.Estados.Contains(t.Estado)) continue;
                
                if (destinas.Count > 0 && !destinas.Contains(t.Paciente))continue;

                ret.Add(t);
            }

            return ret.OrderBy(o => o.Proximo).ToList(); ;

        }
        public gcTratamiento getTratamiento(int id)
        {
            foreach (var r in Enum.GetValues(typeof(gcTratamiento.TipoTratamiento)))
            {
                var ls= ((SortedDictionary<int, gcTratamiento>)_tratamientos[(int)r]);
                if (ls.ContainsKey(id)) return ls[id] as gcTratamiento;
            }
            return null;
                
        }
        public void removeTratamientoFromDB(gcTratamiento t)
        {
           
            gConnector.Connector.Singlenton.ConexionActiva.deleteEntity(t.Id, "gcTratamientos");
            logger.Singlenton.addMessage("Se eliminó a " + t.Titulo);
            var dests = ((SortedDictionary<int, gcTratamiento>)_tratamientos[(int)t.Tipo]);
            if (dests.ContainsKey(t.Id))
            {
                dests.Remove(t.Id);
                AvisoManager.Singlenton.removeAviso(t.Id);
                
            }
        }
        public List<gcDestinatario> getDestinatarios(Filtros.FiltroDestinatario f)
        {
            if (_destinatarios.Count == 0) return new List<gcDestinatario>();
            
             List<gcDestinatario> c=new List<gcDestinatario>();
            
            if (f == null)
            {
                foreach (var r in Enum.GetValues(typeof(gcDestinatario.DestinatarioTipo)))
                {
                    c.AddRange(((SortedDictionary<int, gcDestinatario>)_destinatarios[(int)r]).Values.OfType<gcDestinatario>());

                }
            }
            else
            {
                string[] tt=null;
                List<gcDestinatario> R1 = new List<gcDestinatario>(), R2 = new List<gcDestinatario>(), R3 = new List<gcDestinatario>();
                if (f.Nombre == null) f.Nombre = "";
                if (f.Direccion == null) f.Direccion = "";
                if (f.Cuit == null) f.Cuit = "";
                tt = (f.Texto).ToLower().Trim().Split(" ".ToCharArray());
                List<gcDestinatario> objetos=new List<gcDestinatario>();
                ICollection tipos;
                if (f.Tipos.Count>0)
                {
                    tipos = f.Tipos;
                   
                }
                else
                {
                    tipos = Enum.GetValues(typeof(gcDestinatario.DestinatarioTipo));
                   
                }
                List<SortedDictionary<int, gcDestinatario>> lt = new List<SortedDictionary<int, gcDestinatario>>();
                foreach (var t in tipos)
                {
                    var ls = (SortedDictionary<int, gcDestinatario>)_destinatarios[(int)t];
                    objetos.AddRange(ls.Values);
                    
                }

                
                foreach (gcDestinatario ob in objetos)
                {
                    if (f.Parent > 0 && ob.Parent != f.Parent) continue;
                   if (ob.Nombre.ToLower().Contains(f.Texto.ToLower().Trim()))
                    {
                        R1.Add(ob);
                        continue;

                    }
                   if (ob.Cuit.ToLower().Contains(f.Texto.ToLower().Trim()))
                   {
                       R1.Add(ob);
                       continue;

                   }
                   if (ob.Direccion.ToLower().Contains(f.Texto.ToLower().Trim()))
                    {
                        R3.Add(ob);
                        continue;

                    }
                    int pos = 0;
                    int ttcont = 0;
                    int rango = 4;
                    string nombre = ob.Nombre.ToLower();
                    // busca coincidencias con cada fraccion de texto y va bajando de rango a medidas que encuentra.
                    //corta si llega a rango 1(encontro suficientes coincidencias)
                    //corta si no encontro ninguna coincidencia desde la ultima encontrada
                    //corta si se acabaron las coincidencias
                    while (pos < nombre.Length && ttcont < tt.Length && rango > 1)
                    {
                        if (pos < 0) pos = 0;
                        pos = nombre.IndexOf(tt[ttcont], pos);
                        if (pos > -1) rango--;
                        ttcont++;

                    }
                    //agrega unicamente si esta en el rango.
                    switch (rango)
                    {
                        case 1:
                            R1.Add(ob);
                            break;
                        case 2:
                            R2.Add(ob);
                            break;
                        case 3:
                            R3.Add(ob);
                            break;


                    }





                }
                c.AddRange(R1);
                c.AddRange(R2);
                if (c.Count == 0) c.AddRange(R3);
               
            }
            
             return c;
        }
        public List<gcDestinatario> getDestinatarios(gcDestinatario.DestinatarioTipo tipo)
        {
            if (_destinatarios.Count == 0) return new List<gcDestinatario>() ;
            return ((SortedDictionary<int, gcDestinatario>)_destinatarios[(int)tipo]).Values.ToList<gcDestinatario>();
        }
        public List<gcDestinatario> getDestinatarios()
        {
            var c=new List<gcDestinatario>();
            //si no se cargo devuelve vacio
            if (_destinatarios.Count == 0) return c;
            foreach (var r in Enum.GetValues(typeof(gcDestinatario.DestinatarioTipo)))
            {
                c.AddRange(((SortedDictionary<int, gcDestinatario>)_destinatarios[(int)r]).Values.OfType<gcDestinatario>());

            }
            return c;
        }
        public decimal CreditoEnCUentaCorriente(gcDestinatario d)
        {
            if (d == null) return 0;
            var cuenta = CajaManager.Singlenton.MovimientosEnCuentaCorrDestinatario(d,CoreManager.Singlenton.getCajas());
            if (cuenta != null)
            {
                d.SaldoEnCuenta = cuenta.Total;
                return d.SaldoEnCuenta;
            }
                
            return 0;
            
        }
        public gcDestinatario getDestinatario(int id, int tipo)
        {
            var dests = ((SortedDictionary<int, gcDestinatario>)_destinatarios[tipo]);
            if(dests!=null && dests.ContainsKey(id))
                return (gcDestinatario)dests[id];
            return null;
        }
        public gcDestinatario getDestinatario(int id)
        {
            gcDestinatario dest=null;
            if (_destinatarios.Count > 0)
            {
                foreach (var r in Enum.GetValues(typeof(gcDestinatario.DestinatarioTipo)))
                {
                    var dic=((SortedDictionary<int, gcDestinatario>)_destinatarios[(int)r]);
                    if (dic.ContainsKey(id)) 
                      return dic[id];
                   
                }
            }
           
            return dest;
        }
        
        public void destinatarioAddCredito(gcPago p, gcMovimiento mov)
        {
            gcDestinatario d = getDestinatario(p.Parent);
            if (p.Tipo != gcPago.TipoPago.Cuenta ||
                d==null ||
                mov==null) return;
            

           
            if (_destinatarios.ContainsKey((int)d.Tipo))
            {
                SortedDictionary<int, gcDestinatario> t = (SortedDictionary<int, gcDestinatario>)_destinatarios[(int)d.Tipo];
                if (t.ContainsKey(d.Id))
                {
                    decimal monto = p.Monto;
                    if (mov.EsSalidaDinero)
                    {

                        monto *= -1;
                    }
                    //si restamos la deuda se hace negativo
                    ((gcDestinatario)t[d.Id]).SaldoEnCuenta += monto;
                }
            }
        }
        public void destinatarioRemoveCredito(gcPago p, gcMovimiento mov)
        {
            gcDestinatario d = getDestinatario(p.Parent);
            if (p.Tipo != gcPago.TipoPago.Cuenta ||
                d == null ||
                mov == null) return;



            if (_destinatarios.ContainsKey((int)d.Tipo))
            {
                SortedDictionary<int, gcDestinatario> t = (SortedDictionary<int, gcDestinatario>)_destinatarios[(int)d.Tipo];
                if (t.ContainsKey(d.Id))
                {
                    decimal monto = p.Monto;
                    if (mov.EsSalidaDinero)
                    {

                        monto *= -1;
                    }
                    //si restamos la deuda se hace negativo
                    ((gcDestinatario)t[d.Id]).SaldoEnCuenta -= monto;
                }
            }
        }
        public void removeDestinatarioFromDb(gcDestinatario d)
        {
            if (SePuedeEliminar(d))
            {

                gConnector.Connector.Singlenton.ConexionActiva.deleteEntity(d.Id, "gcDestinatarios");
                logger.Singlenton.addMessage("Se eliminó a " + d.Nombre);
                var dests = ((SortedDictionary<int, gcDestinatario>)_destinatarios[d.Tipo]);
                if (dests.ContainsKey(d.Id))
                {
                    dests.Remove(d.Id);
                }
            }
        }
        public bool SePuedeEliminar(gcDestinatario d)
        {
            var f = new Filtros.FiltroMovimiento();
            f.Destinatario = d;
            var l = MovimientoManager.Singlenton.getMovimientos(f);
            return l.Count == 0;
        }
        public gcDestinatario GuardarDestinatarioEnDB(gcDestinatario d)
        {
            d.LastMod = DateTime.Now;
            Hashtable t = new Hashtable();
            t.Add("id", d.Id);
            t.Add("Nombre", d.Nombre);
            t.Add("Direccion", d.Direccion);
            t.Add("Telefono", d.Telefono);
            t.Add("Celular", d.Celular);
            t.Add("Mail", d.Mail);
            t.Add("Cuit", d.Cuit);
            t.Add("Observaciones", d.Observaciones);
            t.Add("Tipo", d.Tipo);
            t.Add("Parent", d.Parent);
            t.Add("Nacimiento", d.Nacimiento);
            t.Add("Estado", d.Estado);
            t.Add("LastMod", d.LastMod);
            //pacientes guarda otra
            t.Add("TipoIva", d.TipoGenerico);
            

            d.Id= Connector.Singlenton.ConexionActiva.GuardarENtity(t, "gcdestinatario",d.Id>0);
            if (addDestinatario(d))
            {
                logger.Singlenton.addMessage("Destinatarios: Se agrego un nuevo destinatario: " + d.Nombre.ToString());
            }
            else
                logger.Singlenton.addMessage("Destinatario: Se Actualizó un destinatario: " + d.Nombre.ToString());
            return d;
        }
        private bool addDestinatario(gcDestinatario d)
        {
            var l=(SortedDictionary<int, gcDestinatario>)_destinatarios[(int)d.Tipo];
            bool ret =true;
            if (l.ContainsKey(d.Id)){
                l.Remove(d.Id);
                ret =false;
            }
            l.Add(d.Id, d);
            return ret;
        }

        //avisos
       
        
    }
}
