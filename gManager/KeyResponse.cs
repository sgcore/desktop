﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace gManager
{
    public class KeyResponse
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public DateTime created { get; set; }
        [DataMember]
        public string token { get; set; }
        [DataMember]
        public string negocio { get; set; }
        [DataMember]
        public string terminal { get; set; }
        [DataMember]
        public DateTime expire { get; set; }

    }
}
