﻿using System;

namespace gManager
{
    public class keyObject
    {

        public string Usuario { get; set; }
        public string Password { get; set; }
        public string ServerToken { get; set; }
        public string Negocio { get; set; }
        public string Terminal { get; set; }
        public DateTime Tiempo { get; set; }
        public TimeSpan TiempoRestante
        {
            get
            {
                var res = Tiempo.Subtract(DateTime.Now);
                if (res.Days < 0) return new TimeSpan(0);
                return res;
            }
        }
        public bool Habilitado
        {

            get
            {
                return true; // TiempoRestante.TotalDays > 0;
            }
        }
        string t = null;
        public string Token
        {
            get
            {
                if (t == null) t = Utils.Key;
                return t;
            }
        }

    }
}
