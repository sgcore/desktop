﻿using SGServer;
using System;
using System.Windows.Forms;
namespace SGVeterinaria
{
    static class Program
    {
       
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var ini = new gLogger.gCoreContext(new fPrincipal());
            ini.Reiniciando += (o, e) =>
            {
                if (gManager.CoreManager.Singlenton.Estado == gManager.CoreManager.EstadoSistemaEnum.Autenticado)
                {
                    gLogger.logger.Singlenton.addWarningMsg("No se puede reiniciar en este momento. Por favor espere a que el sistema termine de iniciar.");
                    return;
                }
                gManager.CoreManager.Singlenton.reiniciar();
            };
            ini.CerrandoSesion += (o, e) =>
             {
                 if (gManager.CoreManager.Singlenton.Estado == gManager.CoreManager.EstadoSistemaEnum.Autenticado)
                 {
                     gLogger.logger.Singlenton.addWarningMsg("No se puede reiniciar en este momento. Por favor espere a que el sistema termine de iniciar.");
                     return;
                 }
                 gManager.CoreManager.Singlenton.logout();
             };
            ini.Reiniciar();
            //AsyncServer.Singlenton.StartListening();

            Application.Run(ini);
        }


       
    }
}
