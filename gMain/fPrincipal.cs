﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace SGVeterinaria
{
    public partial class fPrincipal : gControls.Formularios.BaseForm
    {
        Controles.BuscadorClientes _bcli = new Controles.BuscadorClientes();
        gControls.Buscadores.BuscadorProductosTree _bprod = new gControls.Buscadores.BuscadorProductosTree();
        private gControls.Visores.VisorCajas _vCaja = new gControls.Visores.VisorCajas();
        Controles.Visores.VisorPaciente _vPaciente = new Controles.Visores.VisorPaciente();
        public fPrincipal()
        {
            InitializeComponent();
            loadHotKeys(acordionContainer1.Controls);
            
                _bcli.Titulo = "Buscador de Clientes";
                _bcli.Texto = "Clientes";
                _bcli.iniciar();
                gControls.Acciones.BuscarClienteHandle = (o, e) =>
                {
                    acordionContainer1.Control = _bcli;
                };
                _bprod.Titulo = "Buscador de Productos";
                _bprod.Texto = "Productos";
               
                _bprod.iniciar();
                 _bprod.VerImagenCard = true;
               _bprod.VerBotonImagen = true;
                _bprod.SeleccionGlobal = true;
                gControls.Acciones.BuscarProductoHandle = (o, e) =>
                {
                    acordionContainer1.Control = _bprod;
                };
                //caja
                _vCaja.AutoActualizarse = true;
                _vCaja.iniciar();
                gControls.Acciones.VerCajaHandle = (o, e) =>
                {
                    acordionContainer1.Control = _vCaja;
                };
           
            
            gManager.CoreManager.Singlenton.CambiodeEstado += (u) =>
            {
                acordionContainer1.reset();
                MainMenuStrip.Items.Clear();
                if (u != gManager.CoreManager.EstadoSistemaEnum.Cargado) return;
                acordionContainer1.addToContainer(_bcli);
                acordionContainer1.addToContainer(_bprod);
                acordionContainer1.addToContainer(_vCaja);

                acordionContainer1.recargarBarra();
                
                MainMenuStrip.Items.Add(generarMenuCajas());
                MainMenuStrip.Items.Add(new ToolStripSeparator());
                MainMenuStrip.Items.Add(generarMenuProductos());
                MainMenuStrip.Items.Add(new ToolStripSeparator());
                MainMenuStrip.Items.Add(generarMenuClientes());
                MainMenuStrip.Items.Add(generarMenuProveedores());
                MainMenuStrip.Items.Add(generarMenuSucursales());
                MainMenuStrip.Items.Add(generarMenuResponsables());
                MainMenuStrip.Items.Add(generarMenuFinancieras());

                MainMenuStrip.Items.Add(new ToolStripSeparator());
                MainMenuStrip.Items.Add(generarMenuUsuarios());
                MainMenuStrip.Items.Add(new ToolStripSeparator());
                //MainMenuStrip.Items.Add(generarMenuPanel());
                MainMenuStrip.Items.Add(generarMenuHerramientas());
                acordionContainer1.IniciarControles();
                MainMenuStrip.Items.Add(generarMenuAyuda());
                MainMenuStrip.Items.Add(generarMenuPanel());
                if (gManager.CoreManager.Singlenton.Usuario != null)
                    Text = "SGCore - "
                    + gManager.CoreManager.Singlenton.Usuario.CentralNombre
                    + " - " + gManager.CoreManager.Singlenton.Usuario.Nombre
                    + " - version: " + Assembly.GetExecutingAssembly().GetName().Version.ToString();




            };
            //agregar Paciente
          
            AccionesGLobales();
        }
        public ToolStripMenuItem generarMenuPanel()
        {
            var mc = crearMenuItem(gControls.Properties.Resources.PanelLateral, "&Pa&nel", "Acciones con el panel lateral", null);
            
            mc.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.MostrarPanel,
               "&Mostrar/Ocultar panel",
               "Muestra u oculta el panel lateral.",
               (o, e) => { if (acordionContainer1.Expandido)acordionContainer1.Contraer(); else acordionContainer1.Expand(); },
                Keys.F12, gManager.gcUsuario.PermisoEnum.Vendedor
               ));
            mc.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.RellenarPanel,
               "&Maximizar/Minimizar panel",
               "Maximiza el panel para que ocupe toda la ventana.",
               (o, e) => { if (acordionContainer1.Rellenado)acordionContainer1.Contraer(); else acordionContainer1.Rellenar(); },
               Keys.Control | Keys.F12, gManager.gcUsuario.PermisoEnum.Vendedor
               ));


            return mc;
        }
        public void AccionesGLobales()
        {
            //preguntar cuando cree un paciente;
            gManager.CoreManager.Singlenton.DestinatarioCreado += (p) =>
                {
                    if (p.EsPaciente)
                    {
                        Util.AgregarVacunacion(p);
                    }
                };
            //agregar el visor paciente al swichpanel
            var vp = new Controles.Visores.VisorPaciente();
            gManager.CoreManager.Singlenton.DestinatarioSeleccionado += (p) =>
            {
                if (p.EsPaciente)
                {
                    vp.Paciente = p;
                    switchPanel1.switchto(vp);
                   // switchPanel1.ShowInfo(Util.parseHtmlPaciente(p));
                }

            };
            switchPanel1.addControl(vp);
        }
    }
}
