﻿namespace SGVeterinaria.Controles.Visores
{
    partial class VisorPacienteCard
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbImagen = new System.Windows.Forms.PictureBox();
            this.lblPropietario = new System.Windows.Forms.Label();
            this.lblCaracteristicas = new System.Windows.Forms.Label();
            this.lblRaza = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblObserv = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).BeginInit();
            this.SuspendLayout();
            // 
            // pbImagen
            // 
            this.pbImagen.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbImagen.Image = global::SGVeterinaria.Properties.Resources.Gatitos;
            this.pbImagen.Location = new System.Drawing.Point(0, 0);
            this.pbImagen.Name = "pbImagen";
            this.pbImagen.Size = new System.Drawing.Size(175, 178);
            this.pbImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbImagen.TabIndex = 1;
            this.pbImagen.TabStop = false;
            // 
            // lblPropietario
            // 
            this.lblPropietario.BackColor = System.Drawing.Color.LightGray;
            this.lblPropietario.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPropietario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPropietario.Location = new System.Drawing.Point(175, 105);
            this.lblPropietario.Name = "lblPropietario";
            this.lblPropietario.Size = new System.Drawing.Size(397, 20);
            this.lblPropietario.TabIndex = 10;
            this.lblPropietario.Text = "de Nombre del Propietario";
            this.lblPropietario.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblCaracteristicas
            // 
            this.lblCaracteristicas.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCaracteristicas.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCaracteristicas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaracteristicas.Location = new System.Drawing.Point(175, 72);
            this.lblCaracteristicas.Name = "lblCaracteristicas";
            this.lblCaracteristicas.Size = new System.Drawing.Size(397, 33);
            this.lblCaracteristicas.TabIndex = 9;
            this.lblCaracteristicas.Text = "Macho - Marron oscuro - Grande - Corto lazio";
            this.lblCaracteristicas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRaza
            // 
            this.lblRaza.BackColor = System.Drawing.Color.DarkGray;
            this.lblRaza.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblRaza.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRaza.Image = global::SGVeterinaria.Properties.Resources.AveGranja;
            this.lblRaza.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblRaza.Location = new System.Drawing.Point(175, 42);
            this.lblRaza.Name = "lblRaza";
            this.lblRaza.Size = new System.Drawing.Size(397, 30);
            this.lblRaza.TabIndex = 8;
            this.lblRaza.Text = "Golden Retriever";
            this.lblRaza.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNombre
            // 
            this.lblNombre.BackColor = System.Drawing.Color.LightGray;
            this.lblNombre.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNombre.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(175, 0);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(397, 42);
            this.lblNombre.TabIndex = 7;
            this.lblNombre.Text = "Nombre de la Mascota";
            this.lblNombre.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblEdad
            // 
            this.lblEdad.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblEdad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdad.Image = global::SGVeterinaria.Properties.Resources.Macho;
            this.lblEdad.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblEdad.Location = new System.Drawing.Point(487, 125);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(85, 53);
            this.lblEdad.TabIndex = 11;
            this.lblEdad.Text = "18 meses";
            this.lblEdad.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblObserv
            // 
            this.lblObserv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblObserv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObserv.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblObserv.Location = new System.Drawing.Point(175, 125);
            this.lblObserv.Name = "lblObserv";
            this.lblObserv.Size = new System.Drawing.Size(312, 53);
            this.lblObserv.TabIndex = 12;
            this.lblObserv.Text = "Observaciones";
            // 
            // VisorPacienteCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblObserv);
            this.Controls.Add(this.lblEdad);
            this.Controls.Add(this.lblPropietario);
            this.Controls.Add(this.lblCaracteristicas);
            this.Controls.Add(this.lblRaza);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.pbImagen);
            this.Name = "VisorPacienteCard";
            this.Size = new System.Drawing.Size(572, 178);
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbImagen;
        private System.Windows.Forms.Label lblPropietario;
        private System.Windows.Forms.Label lblCaracteristicas;
        private System.Windows.Forms.Label lblRaza;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblObserv;
    }
}
