﻿namespace SGVeterinaria.Controles.Visores
{
    partial class VisorPaciente
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorPaciente));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.buscadorCustom1 = new gControls.Buscadores.BuscadorCustom();
            this.editorAvisos1 = new SGVeterinaria.Controles.Editores.EditorAvisos();
            this.PanelInfoPaciente = new System.Windows.Forms.Panel();
            this.visorPacienteCard1 = new SGVeterinaria.Controles.Visores.VisorPacienteCard();
            this.PanelInfoPaciente.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Consulta");
            this.imageList1.Images.SetKeyName(1, "Peluqueria");
            this.imageList1.Images.SetKeyName(2, "Quimicos");
            this.imageList1.Images.SetKeyName(3, "Vacunacion");
            this.imageList1.Images.SetKeyName(4, "Terminado");
            this.imageList1.Images.SetKeyName(5, "Quimicos");
            this.imageList1.Images.SetKeyName(6, "Pendiente");
            this.imageList1.Images.SetKeyName(7, "Peluqueria");
            this.imageList1.Images.SetKeyName(8, "Futuro");
            this.imageList1.Images.SetKeyName(9, "Desparacitar");
            this.imageList1.Images.SetKeyName(10, "Cirujia");
            this.imageList1.Images.SetKeyName(11, "Castracion");
            this.imageList1.Images.SetKeyName(12, "Cancelado");
            this.imageList1.Images.SetKeyName(13, "Dieta");
            this.imageList1.Images.SetKeyName(14, "Otro");
            // 
            // buscadorCustom1
            // 
            this.buscadorCustom1.BloquearBarraHerramientas = false;
            this.buscadorCustom1.CustomActualizar = null;
            this.buscadorCustom1.CustomDatos = false;
            this.buscadorCustom1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscadorCustom1.Imagen = ((System.Drawing.Image)(resources.GetObject("buscadorCustom1.Imagen")));
            this.buscadorCustom1.ImagenTitulo = global::SGVeterinaria.Properties.Resources.ConsultaMedica;
            this.buscadorCustom1.Location = new System.Drawing.Point(0, 168);
            this.buscadorCustom1.Name = "buscadorCustom1";
            this.buscadorCustom1.OcultarSiEstaVacio = false;
            this.buscadorCustom1.RequiereKey = false;
            this.buscadorCustom1.SeleccionGlobal = false;
            this.buscadorCustom1.Size = new System.Drawing.Size(524, 570);
            this.buscadorCustom1.TabIndex = 2;
            this.buscadorCustom1.Texto = "Buscar";
            this.buscadorCustom1.Titulo = "Tratamientos y cirugias programadas";
            this.buscadorCustom1.VerBotonActualizar = false;
            this.buscadorCustom1.VerBotonAgrupar = false;
            this.buscadorCustom1.VerBotonFiltro = false;
            this.buscadorCustom1.VerBotonGuardar = false;
            this.buscadorCustom1.VerBotonImagen = false;
            this.buscadorCustom1.VerBotonImprimir = false;
            this.buscadorCustom1.VerImagenCard = false;
            // 
            // editorAvisos1
            // 
            this.editorAvisos1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editorAvisos1.Imagen = null;
            this.editorAvisos1.Location = new System.Drawing.Point(524, 0);
            this.editorAvisos1.Name = "editorAvisos1";
            this.editorAvisos1.Paciente = null;
            this.editorAvisos1.RequiereKey = false;
            this.editorAvisos1.Size = new System.Drawing.Size(388, 738);
            this.editorAvisos1.TabIndex = 3;
            this.editorAvisos1.Texto = null;
            // 
            // PanelInfoPaciente
            // 
            this.PanelInfoPaciente.Controls.Add(this.buscadorCustom1);
            this.PanelInfoPaciente.Controls.Add(this.visorPacienteCard1);
            this.PanelInfoPaciente.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelInfoPaciente.Location = new System.Drawing.Point(0, 0);
            this.PanelInfoPaciente.Name = "PanelInfoPaciente";
            this.PanelInfoPaciente.Size = new System.Drawing.Size(524, 738);
            this.PanelInfoPaciente.TabIndex = 4;
            // 
            // visorPacienteCard1
            // 
            this.visorPacienteCard1.Dock = System.Windows.Forms.DockStyle.Top;
            this.visorPacienteCard1.Location = new System.Drawing.Point(0, 0);
            this.visorPacienteCard1.Name = "visorPacienteCard1";
            this.visorPacienteCard1.Paciente = null;
            this.visorPacienteCard1.Size = new System.Drawing.Size(524, 168);
            this.visorPacienteCard1.TabIndex = 0;
            // 
            // VisorPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.editorAvisos1);
            this.Controls.Add(this.PanelInfoPaciente);
            this.Name = "VisorPaciente";
            this.Size = new System.Drawing.Size(912, 738);
            this.PanelInfoPaciente.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private Editores.EditorAvisos editorAvisos1;
        private gControls.Buscadores.BuscadorCustom buscadorCustom1;
        private System.Windows.Forms.Panel PanelInfoPaciente;
        private VisorPacienteCard visorPacienteCard1;
    }
}
