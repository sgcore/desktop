﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using System.Windows.Forms.Calendar;

namespace SGVeterinaria.Controles.Visores
{
    public partial class VisorPaciente : UserControl
    {
        gcDestinatario _paciente;
        
        public VisorPaciente()
        {
            InitializeComponent();
            configurarBuscador();
           
            

        }

        void Singlenton_TratamientoEliminado(gcTratamiento t)
        {
            
        }
        private void configurarBuscador()
        {
            //columnas
            var c = buscadorCustom1.addColumn("Tratamiento", "Titulo");
            c.FillsFreeSpace = true;
            c.Sortable = false;
            c.ImageGetter = delegate(object rowObject)
            {
                gcTratamiento s = (gcTratamiento)rowObject;
                return (Util.vTratamientoEnum)s.TipoGenerico;
            };
            c = buscadorCustom1.addColumn("Estado", "Estado");
            c.MinimumWidth = 80;
            c.Sortable = false;
            c = buscadorCustom1.addColumn("Fecha", "Fecha");
            c.MinimumWidth = 80;
            c.Sortable = false;
            c.IsVisible = false;

            buscadorCustom1.SetAllImageList(imageList1);
            buscadorCustom1.Lista.RebuildColumns();
            buscadorCustom1.CustomDatos = true;
            buscadorCustom1.CustomActualizar = (o, e) =>
            {
                if (_paciente != null)
                {
                    buscadorCustom1.setCustomDatos(_paciente.Tratamientos);
                   
                }
                    
            };
            CoreManager.Singlenton.TratamientoEliminado += (t) => { buscadorCustom1.ActualizarLista(); };
            CoreManager.Singlenton.TratamientoCreado += (t) => {
                buscadorCustom1.ActualizarLista(); };
           
            //delete
            buscadorCustom1.Eliminando += (ll,o) => 
            {
                var l = ll as ICollection<gcTratamiento>;
                if (l != null)
                {
                    
                    foreach (gcTratamiento t in l)
                    {
                        t.DeleteMe();
                    }
                    buscadorCustom1.ActualizarLista();
                }
            };
            buscadorCustom1.ItemSeleccionado += buscadorCustom1_ItemSeleccionado;
            
            
           
        }

        void buscadorCustom1_ItemSeleccionado(object sender, EventArgs e)
        {
            var d=sender as gcTratamiento;
            if (d != null)
                editorAvisos1.selectDay(d.Fecha);
        }
        public gcDestinatario Paciente
        {
            get { return _paciente; }
            set
            {
                _paciente = value;
                if (_paciente != null)
                {
                   
                    editorAvisos1.Paciente = _paciente;
                    buscadorCustom1.ActualizarLista();
                    visorPacienteCard1.Paciente = _paciente;
                  
                  
                   
                    

                }
            }
        }
       
       
        
    }
}
