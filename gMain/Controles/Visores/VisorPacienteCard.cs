﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace SGVeterinaria.Controles.Visores
{
    public partial class VisorPacienteCard : UserControl
    {
         gcDestinatario _paciente;
        public VisorPacienteCard()
        {
            InitializeComponent();
        }
        private void setPanelPaciente()
        {
            if (_paciente != null)
            {
                lblNombre.Text = _paciente.Nombre;
                lblCaracteristicas.Text =_paciente.Celular
                    + " - " + _paciente.Telefono + " - " + _paciente.Cuit;
                //edad
                var ed = DateTime.Now.Subtract(_paciente.Nacimiento);
                if (ed.TotalDays > 730)
                {

                    lblEdad.Text = ((int)(ed.TotalDays / 365d)) + " años";
                }
                else if (ed.TotalDays > 60)
                {
                    lblEdad.Text = ((int)(ed.TotalDays / 30)) + " meses";
                }
                else
                {
                    lblEdad.Text = ((int)(ed.TotalDays)) + " días";
                }
                //sexo
                if (_paciente.Mail.StartsWith("h"))
                    lblEdad.Image = Properties.Resources.Hembra;
                else
                    lblEdad.Image = Properties.Resources.Macho;
                //propietario
                lblPropietario.Text = "de " + _paciente.PropietarioNombre;
                //imagen
                if (gManager.ImageManager.HasImage(_paciente))
                {
                    pbImagen.Image = gManager.ImageManager.getImageGeneric(_paciente);
                }
                else
                {
                    pbImagen.Image = Properties.Resources.Gatitos;
                }
                //especie
                lblRaza.Image = Util.getImagen(_paciente.Especie);
                //observaciones
                lblObserv.Text  = _paciente.Observaciones;
                lblRaza.Text = _paciente.Direccion;


            }
        }
            
        public gcDestinatario Paciente
        {
            get { return _paciente; }
            set
            {
                _paciente = value;
                if (_paciente != null)
                {
                   
                    
                    setPanelPaciente();
                  
                   
                    

                }
            }
        }

       
    }
}
