﻿namespace SGVeterinaria.Controles
{
    partial class BuscadorClientes
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscadorClientes));
            this.tw1 = new BrightIdeasSoftware.DataTreeListView();
            this.colNombre = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Especies = new System.Windows.Forms.ImageList(this.components);
            this.visorDestinatario1 = new gControls.Visores.VisorDestinatario();
            this.colCuenta = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.tw1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.Location = new System.Drawing.Point(0, 31);
            // 
            // tw1
            // 
            this.tw1.AllColumns.Add(this.colNombre);
            this.tw1.AllColumns.Add(this.colCuenta);
            this.tw1.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNombre,
            this.colCuenta});
            this.tw1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tw1.DataSource = null;
            this.tw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tw1.EmptyListMsg = "Aún no creó ningun Cliente, Utilize el menu para hacerlo";
            this.tw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tw1.FullRowSelect = true;
            this.tw1.LargeImageList = this.Especies;
            this.tw1.Location = new System.Drawing.Point(0, 59);
            this.tw1.Name = "tw1";
            this.tw1.OwnerDraw = true;
            this.tw1.RootKeyValueString = "";
            this.tw1.ShowGroups = false;
            this.tw1.Size = new System.Drawing.Size(771, 423);
            this.tw1.SmallImageList = this.Especies;
            this.tw1.TabIndex = 19;
            this.tw1.UseCellFormatEvents = true;
            this.tw1.UseCompatibleStateImageBehavior = false;
            this.tw1.UseFilterIndicator = true;
            this.tw1.UseFiltering = true;
            this.tw1.UseHotItem = true;
            this.tw1.View = System.Windows.Forms.View.Details;
            this.tw1.VirtualMode = true;
            // 
            // colNombre
            // 
            this.colNombre.AspectName = "Nombre";
            this.colNombre.FillsFreeSpace = true;
            this.colNombre.Text = "Nombre";
            this.colNombre.Width = 279;
            // 
            // Especies
            // 
            this.Especies.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Especies.ImageStream")));
            this.Especies.TransparentColor = System.Drawing.Color.Transparent;
            this.Especies.Images.SetKeyName(0, "Felino");
            this.Especies.Images.SetKeyName(1, "Ave");
            this.Especies.Images.SetKeyName(2, "Granja");
            this.Especies.Images.SetKeyName(3, "Exotico");
            this.Especies.Images.SetKeyName(4, "Ganado");
            this.Especies.Images.SetKeyName(5, "Canino");
            this.Especies.Images.SetKeyName(6, "Equino");
            this.Especies.Images.SetKeyName(7, "Destinatario");
            // 
            // visorDestinatario1
            // 
            this.visorDestinatario1.BackColor = System.Drawing.Color.White;
            this.visorDestinatario1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.visorDestinatario1.Destinatario = null;
            this.visorDestinatario1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.visorDestinatario1.Location = new System.Drawing.Point(0, 482);
            this.visorDestinatario1.Name = "visorDestinatario1";
            this.visorDestinatario1.Size = new System.Drawing.Size(771, 102);
            this.visorDestinatario1.TabIndex = 20;
            this.visorDestinatario1.VerBotonSeguimiento = true;
            this.visorDestinatario1.Visible = false;
            // 
            // colCuenta
            // 
            this.colCuenta.AspectName = "SaldoEnCuenta";
            this.colCuenta.Text = "Deuda";
            // 
            // BuscadorClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tw1);
            this.Controls.Add(this.visorDestinatario1);
            this.Name = "BuscadorClientes";
            this.Controls.SetChildIndex(this.visorDestinatario1, 0);
            this.Controls.SetChildIndex(this.lblTitulo, 0);
            this.Controls.SetChildIndex(this.tw1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.tw1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.DataTreeListView tw1;
        private BrightIdeasSoftware.OLVColumn colNombre;
        private System.Windows.Forms.ImageList Especies;
        private gControls.Visores.VisorDestinatario visorDestinatario1;
        private BrightIdeasSoftware.OLVColumn colCuenta;
    }
}
