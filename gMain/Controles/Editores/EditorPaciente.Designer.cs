﻿namespace SGVeterinaria.Controles.Editores
{
    partial class EditorPaciente
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorPaciente));
            this.btMacho = new gControls.BotonAccion();
            this.btHembre = new gControls.BotonAccion();
            this.gbPropietario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // DestImageList
            // 
            this.DestImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("DestImageList.ImageStream")));
            this.DestImageList.Images.SetKeyName(0, "Cliente");
            this.DestImageList.Images.SetKeyName(1, "Proveedor");
            this.DestImageList.Images.SetKeyName(2, "Responsable");
            this.DestImageList.Images.SetKeyName(3, "Sucursal");
            // 
            // lblMail
            // 
            this.lblMail.Location = new System.Drawing.Point(393, 187);
            this.lblMail.Size = new System.Drawing.Size(31, 13);
            this.lblMail.Text = "Sexo";
            // 
            // txtMail
            // 
            this.txtMail.AutoCompleteCustomSource.AddRange(new string[] {
            "Macho",
            "Hembra"});
            this.txtMail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMail.Location = new System.Drawing.Point(651, 253);
            this.txtMail.Text = "macho";
            this.txtMail.Visible = false;
            // 
            // lblCuit
            // 
            this.lblCuit.Location = new System.Drawing.Point(267, 194);
            this.lblCuit.Size = new System.Drawing.Size(66, 13);
            this.lblCuit.Text = "Tipo de pelo";
            // 
            // txtCuit
            // 
            this.txtCuit.AutoCompleteCustomSource.AddRange(new string[] {
            "Largo lazio",
            "Corto lazio",
            "Largo crespo",
            "Corto crespo",
            "Largo ondulado",
            "corto ondulado",
            "Calvo"});
            this.txtCuit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCuit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCuit.Location = new System.Drawing.Point(270, 210);
            // 
            // lblDire
            // 
            this.lblDire.Location = new System.Drawing.Point(149, 94);
            this.lblDire.Size = new System.Drawing.Size(32, 13);
            this.lblDire.Text = "Raza";
            // 
            // txtDireccion
            // 
            this.txtDireccion.AutoCompleteCustomSource.AddRange(new string[] {
            "Akita ",
            "Akita Americano ",
            "Afgano",
            "Airedale Terrier",
            "Alaskan Malamute ",
            "American Pitt Bull Terrier ",
            "American Staffordshire Terrier",
            "American Water Spaniel",
            "Antiguo Pastor Inglés",
            "Australian Kelpie",
            "Australian Shepherd ",
            "Barzoi ",
            "Basset Azul de Gaseogne",
            "Basset Hound",
            "Basset leonado de Bretaña",
            "Beagle",
            "Bearded Collie",
            "Beauceron ",
            "Berger Blanc Suisse ",
            "Bichón Frisé",
            "Bichón Habanero ",
            "Bichón Maltés",
            "Bloodhound",
            "Bobtail",
            "Border Collie",
            "Border Collie ",
            "Borzoi ",
            "Boston Terrier",
            "Boxer",
            "Boyero Australiano",
            "Boyero de Flandes",
            "Boyero de Montaña Bernés ",
            "Braco Alemán",
            "Braco de Weimar ",
            "Braco Húngaro",
            "Briard ",
            "Bull Terrier",
            "Bulldog Americano",
            "Bulldog Francés",
            "Bulldog Inglés",
            "Bullmastiff",
            "Ca de Bou ",
            "Ca mè mallorquí ",
            "Cane Corso",
            "Caniche",
            "Carlino",
            "Chien de Saint Hubert ",
            "Chihuahua",
            "Chino Crestado",
            "Chow Chow",
            "Cirneco del Etna",
            "Cocker Spaniel Americano",
            "Cocker Spaniel Inglés",
            "Collie",
            "Coton de Tuléar ",
            "Dachshunds ",
            "Dálmata",
            "Deutsch Drahthaar",
            "Deutsch Kurzhaar ",
            "Dobermann",
            "Dogo Alemán",
            "Dogo Argentino",
            "Dogo Canario ",
            "Dogo de Burdeos",
            "Drahthaar",
            "English Springer Spaniel ",
            "Epagneul Bretón",
            "Eurasier",
            "Fila Brasileiro ",
            "Fox Terrier",
            "Foxhound Americano",
            "Foxhound Inglés",
            "Galgo Afgano",
            "Galgo Español",
            "Galgo Italiano",
            "Galgo Ruso",
            "Gigante de los Pirineos",
            "Golden Retriever",
            "Gos d\'Atura",
            "Gran Danés",
            "Gran Perro Japonés ",
            "Husky Siberiano",
            "Irish Wolfhound",
            "Jack Russel",
            "Japanes Chin",
            "Kelpie ",
            "Kerry Blue",
            "Komondor",
            "Kuvasz ",
            "Labrador Retriever",
            "Laika de Siberia Occidental",
            "Laika Ruso-europeo",
            "Lebrel ruso ",
            "Leonberger ",
            "Lhasa Apso",
            "Magyar Vizsla",
            "Maltés",
            "Mastín del Alentejo ",
            "Mastín del Pirineo",
            "Mastin del Tibet",
            "Mastín Español",
            "Mastín Napolitano",
            "Norfolk Terrier",
            "Ovtcharka ",
            "Pachón Navarro ",
            "Pastor Alemán",
            "Pastor Australiano",
            "Pastor Belga",
            "Pastor Blanco Suizo ",
            "Pastor de Beauce",
            "Pastor de Brie",
            "Pastor de los Pirineos de Cara Rosa",
            "Pastor de Shetland ",
            "Pastor del Cáucaso ",
            "Pekinés",
            "Perdiguero Burgos ",
            "Perdiguero Chesapeake Bay",
            "Perdiguero de Drentse",
            "Perdiguero de Pelo lido",
            "Perdiguero de pelo rizado",
            "Perdiguero Portugués",
            "Perro Crestado Chino",
            "Perro de Aguas",
            "Perro sin pelo de Mexico ",
            "Perro sin pelo del Perú",
            "Pinscher miniatura ",
            "Pitbull",
            "Podenco Andaluz",
            "Podenco Ibicenco",
            "Podenco Portugués",
            "presa canario",
            "Presa Mallorquin",
            "Rafeiro do Alentejo ",
            "Rottweiler",
            "Rough Collie",
            "Sabueso Español",
            "Sabueso Hélenico",
            "Sabueso Italiano",
            "Sabueso Suizo",
            "Saint Hubert ",
            "Saluki",
            "Samoyedo",
            "San Bernardo",
            "Schnaucer",
            "Scottish Terrier",
            "Sealyhalm Terrier",
            "Setter Gordon",
            "Setter Irlandés",
            "Shar Pei",
            "Shiba ",
            "Shiba Inu",
            "Shih Tzu ",
            "Siberian Husky",
            "Smooth Collie",
            "Spaniel Japonés ",
            "Spinone Italiano ",
            "Spitz Alemán ",
            "Springer Spaniel Inglés",
            "Staffordshire Bull Terrier",
            "Teckel",
            "Terranova",
            "Terrier Australiano",
            "Terrier Escocés",
            "Terrier Irlandés",
            "Terrier Japonés",
            "Terrier Negro Ruso",
            "Terrier Norfolk",
            "Terrier Ruso",
            "Tibetan Terrier",
            "Vizsla ",
            "Welhs Terrier",
            "West Highland T.",
            "Wolfspitz",
            "Xoloitzquintle ",
            "Yorkshire Terrier",
            "Mestizo"});
            this.txtDireccion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDireccion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDireccion.Location = new System.Drawing.Point(152, 110);
            this.txtDireccion.Size = new System.Drawing.Size(215, 20);
            // 
            // lblCel
            // 
            this.lblCel.Location = new System.Drawing.Point(147, 194);
            this.lblCel.Size = new System.Drawing.Size(31, 13);
            this.lblCel.Text = "Color";
            // 
            // txtCelular
            // 
            this.txtCelular.AutoCompleteCustomSource.AddRange(new string[] {
            "Blanco",
            "Blanco manchas negras",
            "Marron",
            "Marron Manto negro",
            "Gris",
            "Negro",
            "Negro pecho blanco"});
            this.txtCelular.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCelular.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCelular.Location = new System.Drawing.Point(150, 210);
            // 
            // lblTele
            // 
            this.lblTele.Location = new System.Drawing.Point(25, 194);
            this.lblTele.Size = new System.Drawing.Size(46, 13);
            this.lblTele.Text = "Tamaño";
            // 
            // txtTelefono
            // 
            this.txtTelefono.AutoCompleteCustomSource.AddRange(new string[] {
            "Muy Pequeño",
            "Pequeño",
            "Mediano",
            "Grande",
            "Enorme"});
            this.txtTelefono.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTelefono.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTelefono.Location = new System.Drawing.Point(28, 210);
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(149, 54);
            this.lblNombre.Size = new System.Drawing.Size(106, 13);
            this.lblNombre.Text = "Nombre del Paciente";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(147, 70);
            // 
            // lblObs
            // 
            this.lblObs.Location = new System.Drawing.Point(393, 15);
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Location = new System.Drawing.Point(396, 31);
            this.txtObservaciones.Size = new System.Drawing.Size(188, 153);
            // 
            // dtNacimiento
            // 
            this.dtNacimiento.Location = new System.Drawing.Point(279, 159);
            // 
            // lblFecha
            // 
            this.lblFecha.Location = new System.Drawing.Point(276, 143);
            this.lblFecha.Size = new System.Drawing.Size(114, 13);
            this.lblFecha.Text = "Fecha de Naciemiento";
            // 
            // gbPropietario
            // 
            this.gbPropietario.Location = new System.Drawing.Point(7, 6);
            this.gbPropietario.Size = new System.Drawing.Size(383, 46);
            this.gbPropietario.Visible = true;
            // 
            // lblLastMod
            // 
            this.lblLastMod.Location = new System.Drawing.Point(25, 242);
            // 
            // btGuardar
            // 
            this.btGuardar.Location = new System.Drawing.Point(663, 224);
            this.btGuardar.Visible = false;
            // 
            // btCancelar
            // 
            this.btCancelar.Location = new System.Drawing.Point(663, 184);
            this.btCancelar.Visible = false;
            // 
            // cmbIva
            // 
            this.cmbIva.Location = new System.Drawing.Point(152, 159);
            this.cmbIva.TabIndex = 2;
            this.cmbIva.SelectedIndexChanged += new System.EventHandler(this.cmbIva_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::SGVeterinaria.Properties.Resources.Perro;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(7, 58);
            this.pictureBox1.Size = new System.Drawing.Size(134, 110);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblTipoIva
            // 
            this.lblTipoIva.Location = new System.Drawing.Point(149, 143);
            this.lblTipoIva.Size = new System.Drawing.Size(45, 13);
            this.lblTipoIva.Text = "Especie";
            // 
            // btMacho
            // 
            this.btMacho.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btMacho.BackgroundImage")));
            this.btMacho.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btMacho.Checked = true;
            this.btMacho.FormContainer = null;
            this.btMacho.GlobalHotKey = false;
            this.btMacho.HacerInvisibleAlDeshabilitar = false;
            this.btMacho.HotKey = System.Windows.Forms.Shortcut.None;
            this.btMacho.Image = global::SGVeterinaria.Properties.Resources.Macho;
            this.btMacho.Location = new System.Drawing.Point(396, 203);
            this.btMacho.Name = "btMacho";
            this.btMacho.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btMacho.RegistradoEnForm = false;
            this.btMacho.RequiereKey = false;
            this.btMacho.Size = new System.Drawing.Size(55, 45);
            this.btMacho.TabIndex = 37;
            this.btMacho.Text = "Macho";
            this.btMacho.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btMacho.Titulo = "";
            this.btMacho.ToolTipDescripcion = "";
            this.btMacho.UseVisualStyleBackColor = true;
            this.btMacho.Click += new System.EventHandler(this.btMacho_Click);
            // 
            // btHembre
            // 
            this.btHembre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btHembre.Checked = false;
            this.btHembre.FormContainer = null;
            this.btHembre.GlobalHotKey = false;
            this.btHembre.HacerInvisibleAlDeshabilitar = false;
            this.btHembre.HotKey = System.Windows.Forms.Shortcut.None;
            this.btHembre.Image = global::SGVeterinaria.Properties.Resources.Hembra;
            this.btHembre.Location = new System.Drawing.Point(457, 203);
            this.btHembre.Name = "btHembre";
            this.btHembre.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btHembre.RegistradoEnForm = false;
            this.btHembre.RequiereKey = false;
            this.btHembre.Size = new System.Drawing.Size(55, 45);
            this.btHembre.TabIndex = 38;
            this.btHembre.Text = "Hembra";
            this.btHembre.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btHembre.Titulo = "";
            this.btHembre.ToolTipDescripcion = "";
            this.btHembre.UseVisualStyleBackColor = true;
            this.btHembre.Click += new System.EventHandler(this.btHembre_Click);
            // 
            // EditorPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = false;
            this.Controls.Add(this.btHembre);
            this.Controls.Add(this.btMacho);
            this.Name = "EditorPaciente";
            this.Size = new System.Drawing.Size(599, 268);
            this.Controls.SetChildIndex(this.txtNombre, 0);
            this.Controls.SetChildIndex(this.lblNombre, 0);
            this.Controls.SetChildIndex(this.txtTelefono, 0);
            this.Controls.SetChildIndex(this.lblTele, 0);
            this.Controls.SetChildIndex(this.txtCelular, 0);
            this.Controls.SetChildIndex(this.lblCel, 0);
            this.Controls.SetChildIndex(this.txtDireccion, 0);
            this.Controls.SetChildIndex(this.lblDire, 0);
            this.Controls.SetChildIndex(this.txtCuit, 0);
            this.Controls.SetChildIndex(this.lblCuit, 0);
            this.Controls.SetChildIndex(this.txtMail, 0);
            this.Controls.SetChildIndex(this.lblMail, 0);
            this.Controls.SetChildIndex(this.txtObservaciones, 0);
            this.Controls.SetChildIndex(this.lblObs, 0);
            this.Controls.SetChildIndex(this.dtNacimiento, 0);
            this.Controls.SetChildIndex(this.lblFecha, 0);
            this.Controls.SetChildIndex(this.gbPropietario, 0);
            this.Controls.SetChildIndex(this.lblLastMod, 0);
            this.Controls.SetChildIndex(this.btGuardar, 0);
            this.Controls.SetChildIndex(this.btCancelar, 0);
            this.Controls.SetChildIndex(this.cmbIva, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.lblTipoIva, 0);
            this.Controls.SetChildIndex(this.btMacho, 0);
            this.Controls.SetChildIndex(this.btHembre, 0);
            this.gbPropietario.ResumeLayout(false);
            this.gbPropietario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private gControls.BotonAccion btMacho;
        private gControls.BotonAccion btHembre;
    }
}
