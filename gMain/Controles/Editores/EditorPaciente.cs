﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace SGVeterinaria.Controles.Editores
{
    public partial class EditorPaciente : gControls.Editores.EditorDestinatario
    {
        public EditorPaciente():base()
        {
            InitializeComponent();
        }
        protected override gManager.gcDestinatario buildDestinatario()
        {
            if (_obj == null) return null;
            _obj.Nombre = txtNombre.Text;
            _obj.Direccion = txtDireccion.Text;
            _obj.Telefono = txtTelefono.Text;
            _obj.Celular = txtCelular.Text;
            _obj.Cuit = txtCuit.Text;
            _obj.Mail = txtMail.Text;
            _obj.Observaciones = txtObservaciones.Text;
            _obj.Nacimiento = dtNacimiento.Value;
            _obj.TipoGenerico = cmbIva.SelectedIndex;

            return _obj;
        }
        protected override void setDestinatario(gManager.gcDestinatario d)
        {
            _obj = d;
            if (_obj != null)
            {
                cmbIva.DataSource = Enum.GetValues(typeof(gcDestinatario.especieEnum));
                foreach (Control c in Controls) c.Enabled = true;
                txtNombre.Text = _obj.Nombre;
                txtDireccion.Text = _obj.Direccion;
                txtTelefono.Text = _obj.Telefono;
                txtCelular.Text = _obj.Celular;
                txtCuit.Text = _obj.Cuit;
                txtMail.Text = _obj.Mail;
                if (txtMail.Text.StartsWith("h"))
                {
                    btHembre_Click(null, null);
                }
                else btMacho_Click(null, null);
                txtObservaciones.Text = _obj.Observaciones;
                
               
                cmbIva.SelectedIndex = _obj.TipoGenerico;
               
                dtNacimiento.Value = _obj.Nacimiento;
                lblLastMod.Text = "Ultima modificación: " + gManager.Utils.MostrarHora(_obj.LastMod);
                gManager.gcDestinatario g = gManager.CoreManager.Singlenton.getDestinatario(_obj.Parent, gManager.gcDestinatario.DestinatarioTipo.Cliente);
                if (g != null)
                {

                    lblPropietario.Text = g.Nombre;
                }
                else
                {
                    lblPropietario.Text ="Seleccione el propietario";
                }
                    

                
                pictureBox1.Image = ImageManager.getImageGeneric(_obj);
            }
            else
            {
                txtNombre.Text = "";
                txtDireccion.Text = "";
                txtTelefono.Text = "";
                txtCelular.Text = "";
                txtCuit.Text = "";
                txtMail.Text = "";
                lblLastMod.Text = "No ha selseccionado un destinatario";
                txtObservaciones.Text = "";
                foreach (Control c in Controls) c.Enabled = false;
            }
                 
        }

        private void cmbIva_SelectedIndexChanged(object sender, EventArgs e)
        {
            gcDestinatario.especieEnum sel = (gcDestinatario.especieEnum)cmbIva.SelectedIndex;
            switch (sel)
            {
                case gcDestinatario.especieEnum.Ave:
                    pictureBox1.BackgroundImage = Properties.Resources.Ave;
                    break;
                case gcDestinatario.especieEnum.Canino:
                    pictureBox1.BackgroundImage = Properties.Resources.Perro;
                    break;
                case gcDestinatario.especieEnum.Equino:
                    base.pictureBox1.BackgroundImage = Properties.Resources.Equino;
                    break;
                case gcDestinatario.especieEnum.Exotico:
                    pictureBox1.BackgroundImage = Properties.Resources.Exotico;
                    break;
                case gcDestinatario.especieEnum.Felino:
                    pictureBox1.BackgroundImage = Properties.Resources.Gato;
                    break;
                case gcDestinatario.especieEnum.Ganado:
                    pictureBox1.BackgroundImage = Properties.Resources.Ganado;
                    break;
                case gcDestinatario.especieEnum.Granja:
                    pictureBox1.BackgroundImage = Properties.Resources.AveGranja;
                    break;


            }
        }

        private void btMacho_Click(object sender, EventArgs e)
        {
            btHembre.Checked = false;
            txtMail.Text = "m";
            btMacho.Checked = true;
        }

        private void btHembre_Click(object sender, EventArgs e)
        {
            btHembre.Checked = true;
            txtMail.Text = "h";
            btMacho.Checked = false;
        }
    }
}
