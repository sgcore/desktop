﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms.Calendar;
using gManager;

namespace SGVeterinaria.Controles.Editores
{
    public partial class EditorAvisos : gControls.BaseControl
    {
        Dictionary<int, CalendarItem> _items = new Dictionary<int, CalendarItem>();
        public EditorAvisos()
        {
            InitializeComponent();
           configurarBuscador();
            monthView1.SelectionChanged += monthView1_SelectionChanged;
            calendar1.LoadItems += calendar1_LoadItems;
            calendar1.ItemCreating += calendar1_ItemCreating;
            calendar1.ItemCreated += calendar1_ItemCreated;
            calendar1.ItemDatesChanged += calendar1_ItemDatesChanged;
            calendar1.ItemDeleting += calendar1_ItemDeleting;
            calendar1.ItemDeleted += calendar1_ItemDeleted;
             monthView1.SelectionOneDay = DateTime.Now; 
            
           
           
              
           
        }
        private void configurarBuscador()
        {
            //columnas
            //tratamiento
            var c = buscadorCustom1.addColumn("Tratamiento", "Titulo");
            c.FillsFreeSpace = true;
            c.Sortable = false;
            c.ImageGetter = delegate(object rowObject)
            {
                gcTratamiento s = (gcTratamiento)rowObject;
                return (Util.vTratamientoEnum)s.TipoGenerico;
            };
            //estado
            c = buscadorCustom1.addColumn("Estado", "Estado");
            c.MinimumWidth = 80;
            c.Sortable = false;
            c.ImageGetter = delegate(object rowObject)
            {
                gcTratamiento s = (gcTratamiento)rowObject;
                return s.Estado;
            };
            c = buscadorCustom1.addColumn("Fecha", "Fecha");
            c.MinimumWidth = 80;
            c.Sortable = false;
            c.IsVisible = false;

            buscadorCustom1.SetAllImageList(imageList1);
            buscadorCustom1.Lista.RebuildColumns();
            buscadorCustom1.CustomDatos = true;
            buscadorCustom1.CustomActualizar = (o, e) =>
            {
                if (_pac != null)
                {
                    buscadorCustom1.setCustomDatos(_pac.Tratamientos);
                    
                }

            };
            CoreManager.Singlenton.TratamientoEliminado += (t) => { buscadorCustom1.ActualizarLista(); };
            CoreManager.Singlenton.TratamientoCreado += (t) =>
            {
                buscadorCustom1.ActualizarLista();
            };

            //delete
            buscadorCustom1.Eliminando += (ll,o) =>
            {
                var l = ll as ICollection<gcTratamiento>;
                if (l != null)
                {
                    foreach (gcTratamiento t in l)
                    {
                        t.DeleteMe();
                    }
                    buscadorCustom1.ActualizarLista();
                    calendar1.ReloadItems();
                    calendar1.Invalidate();
                }
            };
            buscadorCustom1.ItemSeleccionado += buscadorCustom1_ItemSeleccionado;
            buscadorCustom1.iniciar();


        }
        void buscadorCustom1_ItemSeleccionado(object sender, EventArgs e)
        {
            var d = sender as gcTratamiento;
            if (d != null)
                selectDay(d.Fecha);
        }
        
        private bool montiviewExpanded { get; set; }

       
        void calendar1_ItemDeleted(object sender, CalendarItemEventArgs e)
        {
            if (e.Item.Tag != null && e.Item.Tag is gcTratamiento)
            {
                var t = e.Item.Tag as gcTratamiento;
                if (_items.ContainsKey(t.Id)) _items.Remove(t.Id);
                t.DeleteMe();
            }
        }

        void calendar1_ItemDeleting(object sender, CalendarItemCancelEventArgs e)
        {
            e.Cancel = !RequisitoPermiso(gManager.gcUsuario.PermisoEnum.Encargado);
        }

        void calendar1_ItemDatesChanged(object sender, CalendarItemEventArgs e)
        {
           
            if (e.Item.Tag != null && e.Item.Tag is gcTratamiento)
            {
                var t = e.Item.Tag as gcTratamiento;
                t.Fecha = e.Item.StartDate;
                t.Proximo = e.Item.EndDate;
                t.saveMe();
                addTratatoList(t);
            }
        }

        void calendar1_ItemCreated(object sender, CalendarItemCancelEventArgs e)
        {
            var t = e.Item.Tag as gcTratamiento;
            if (t != null && !String.IsNullOrEmpty(e.Item.Text))
            {

                t.Fecha = e.Item.StartDate;
                t.Proximo = e.Item.EndDate;
                t.Descripcion = e.Item.Text;
                t.Titulo = e.Item.Descripcion; ;

                if(t.Fecha <t.Proximo) t.saveMe();
                addTratatoList(t);
            }
            
        }

        void calendar1_ItemCreating(object sender, CalendarItemCancelEventArgs e)
        {
            
            int i=Util.ChoseTipoTratamiento();
            if (_pac != null &&  i >=0 && RequisitoPermiso(gManager.gcUsuario.PermisoEnum.Encargado)) 
            {
                var t = new gcTratamiento(_pac, gcTratamiento.TipoTratamiento.Tratamiento);
                t.TipoGenerico = i;
                e.Item.Image = Util.getImagen((Util.vTratamientoEnum)i);
                e.Item.ImageAlign = CalendarItemImageAlign.West;
                e.Item.Descripcion = ((Util.vTratamientoEnum)i).ToString();
                e.Item.Tag = t;
            }
            else
            {
                e.Cancel = true;
            }
               
        }

        void calendar1_LoadItems(object sender, System.Windows.Forms.Calendar.CalendarLoadEventArgs e)
        {
            var result = _items.Values.Where(item => calendar1.ViewIntersects(item));
            calendar1.Items.AddRange(result);
        }

        void monthView1_SelectionChanged(object sender, EventArgs e)
        {
            if(monthView1.SelectionStart <monthView1.SelectionEnd)
            {
               calendar1.SetViewRange (monthView1.SelectionStart,monthView1.SelectionEnd);
            }
                
        }
        gManager.gcDestinatario _pac;
        public gManager.gcDestinatario Paciente 
        {
            get { return _pac; }
            set
            {
                _pac = value;
                ReloadItems();
                buscadorCustom1.ActualizarLista();
                
               
               
            }
        
        }
        public void ReloadItems()
        {
            if (_pac != null)
            {
                setTrataList(_pac.Tratamientos);
            }
        }
        public void setTrataList(List<gcTratamiento> tl)
        {
            _items.Clear();
            foreach (var t in tl)
            {
                addTratatoList(t);
            }
            //resetea
            calendar1.SetViewRange(monthView1.SelectionStart, monthView1.SelectionEnd);
        }
        private void addTratatoList(gcTratamiento t)
        {
            CalendarItem i = new CalendarItem(calendar1, t.Fecha, t.Proximo, t.Descripcion);
            i.Tag = t;
            i.Image = Util.getImagen((Util.vTratamientoEnum)t.TipoGenerico);
            i.ImageAlign = CalendarItemImageAlign.West;
            i.Descripcion = t.Titulo;
            switch (t.Estado)
            {
                case gcTratamiento.EstadoSeguimientoEnum.Cancelado:
                    i.BackgroundColorLighter = Color.LightPink;
                    break;
                case gcTratamiento.EstadoSeguimientoEnum.Futuro:
                    i.BackgroundColorLighter = Color.LightSeaGreen;
                    break;
                case gcTratamiento.EstadoSeguimientoEnum.Pendiente:
                    i.BackgroundColorLighter = Color.LightYellow;
                    break;
                case gcTratamiento.EstadoSeguimientoEnum.Terminado:
                    i.BackgroundColorLighter = Color.LightSteelBlue;
                    break;
            }
            if (_items.ContainsKey(t.Id))
                _items[t.Id] = i;
            else
                _items.Add(t.Id, i);
        }

        private void btDia_Click(object sender, EventArgs e)
        {
            selectDay(DateTime.Now);
        }
        public void selectDay(DateTime d)
        {
            monthView1.SelectionOneDay = d;
           
            calendar1.setTimeOfDayVisible(d);
            
        }
        public void selectRangeDays(DateTime a, DateTime b)
        {
            monthView1.SelectRange(a, b);
        }

        private void btsemana_Click(object sender, EventArgs e)
        {
            var input=DateTime.Now;
            int delta = DayOfWeek.Monday - input.DayOfWeek;
            DateTime monday = input.AddDays(delta);
            DateTime sunday= monday.AddDays(6);

            monthView1.SelectRange(monday, sunday);
        }

        private void btMes_Click(object sender, EventArgs e)
        {
            btMes.Checked = !btMes.Checked;
            monthView1.Visible = btMes.Checked;
        }
    }
}
