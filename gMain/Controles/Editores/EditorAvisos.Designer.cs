﻿namespace SGVeterinaria.Controles.Editores
{
    partial class EditorAvisos
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange1 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange2 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange3 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange4 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange5 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorAvisos));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btMes = new gControls.BotonAccion();
            this.btDia = new gControls.BotonAccion();
            this.btsemana = new gControls.BotonAccion();
            this.calendar1 = new System.Windows.Forms.Calendar.Calendar();
            this.monthView1 = new System.Windows.Forms.Calendar.MonthView();
            this.buscadorCustom1 = new gControls.Buscadores.BuscadorCustom();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btMes);
            this.panel1.Controls.Add(this.btDia);
            this.panel1.Controls.Add(this.btsemana);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(315, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(597, 41);
            this.panel1.TabIndex = 2;
            // 
            // btMes
            // 
            this.btMes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btMes.Checked = false;
            this.btMes.Dock = System.Windows.Forms.DockStyle.Left;
            this.btMes.FormContainer = null;
            this.btMes.GlobalHotKey = false;
            this.btMes.HacerInvisibleAlDeshabilitar = false;
            this.btMes.HotKey = System.Windows.Forms.Shortcut.None;
            this.btMes.Image = global::SGVeterinaria.Properties.Resources.Mes;
            this.btMes.Location = new System.Drawing.Point(76, 0);
            this.btMes.Name = "btMes";
            this.btMes.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btMes.RegistradoEnForm = false;
            this.btMes.RequiereKey = false;
            this.btMes.Size = new System.Drawing.Size(38, 41);
            this.btMes.TabIndex = 2;
            this.btMes.Titulo = "Meses";
            this.btMes.ToolTipDescripcion = "Muestra los meses para seleccionar las fechas";
            this.btMes.UseVisualStyleBackColor = true;
            this.btMes.Click += new System.EventHandler(this.btMes_Click);
            // 
            // btDia
            // 
            this.btDia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDia.Checked = false;
            this.btDia.Dock = System.Windows.Forms.DockStyle.Left;
            this.btDia.FormContainer = null;
            this.btDia.GlobalHotKey = false;
            this.btDia.HacerInvisibleAlDeshabilitar = false;
            this.btDia.HotKey = System.Windows.Forms.Shortcut.None;
            this.btDia.Image = global::SGVeterinaria.Properties.Resources.Dia;
            this.btDia.Location = new System.Drawing.Point(38, 0);
            this.btDia.Name = "btDia";
            this.btDia.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btDia.RegistradoEnForm = false;
            this.btDia.RequiereKey = false;
            this.btDia.Size = new System.Drawing.Size(38, 41);
            this.btDia.TabIndex = 1;
            this.btDia.Titulo = "Seleccionar Hoy";
            this.btDia.ToolTipDescripcion = "Selecciona el día de hoy";
            this.btDia.UseVisualStyleBackColor = true;
            this.btDia.Click += new System.EventHandler(this.btDia_Click);
            // 
            // btsemana
            // 
            this.btsemana.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btsemana.Checked = false;
            this.btsemana.Dock = System.Windows.Forms.DockStyle.Left;
            this.btsemana.FormContainer = null;
            this.btsemana.GlobalHotKey = false;
            this.btsemana.HacerInvisibleAlDeshabilitar = false;
            this.btsemana.HotKey = System.Windows.Forms.Shortcut.None;
            this.btsemana.Image = global::SGVeterinaria.Properties.Resources.Semana;
            this.btsemana.Location = new System.Drawing.Point(0, 0);
            this.btsemana.Name = "btsemana";
            this.btsemana.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btsemana.RegistradoEnForm = false;
            this.btsemana.RequiereKey = false;
            this.btsemana.Size = new System.Drawing.Size(38, 41);
            this.btsemana.TabIndex = 0;
            this.btsemana.Titulo = "Seleccionar Semana";
            this.btsemana.ToolTipDescripcion = "Selecciona la semana actual ";
            this.btsemana.UseVisualStyleBackColor = true;
            this.btsemana.Click += new System.EventHandler(this.btsemana_Click);
            // 
            // calendar1
            // 
            this.calendar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendar1.Font = new System.Drawing.Font("Segoe UI", 9F);
            calendarHighlightRange1.DayOfWeek = System.DayOfWeek.Monday;
            calendarHighlightRange1.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange1.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange2.DayOfWeek = System.DayOfWeek.Tuesday;
            calendarHighlightRange2.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange2.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange3.DayOfWeek = System.DayOfWeek.Wednesday;
            calendarHighlightRange3.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange3.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange4.DayOfWeek = System.DayOfWeek.Thursday;
            calendarHighlightRange4.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange4.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange5.DayOfWeek = System.DayOfWeek.Friday;
            calendarHighlightRange5.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange5.StartTime = System.TimeSpan.Parse("08:00:00");
            this.calendar1.HighlightRanges = new System.Windows.Forms.Calendar.CalendarHighlightRange[] {
        calendarHighlightRange1,
        calendarHighlightRange2,
        calendarHighlightRange3,
        calendarHighlightRange4,
        calendarHighlightRange5};
            this.calendar1.Location = new System.Drawing.Point(315, 194);
            this.calendar1.MaximumFullDays = 5;
            this.calendar1.Name = "calendar1";
            this.calendar1.Size = new System.Drawing.Size(597, 405);
            this.calendar1.TabIndex = 1;
            this.calendar1.Text = "calendar1";
            // 
            // monthView1
            // 
            this.monthView1.ArrowsColor = System.Drawing.SystemColors.Window;
            this.monthView1.ArrowsSelectedColor = System.Drawing.Color.Gold;
            this.monthView1.DayBackgroundColor = System.Drawing.Color.Empty;
            this.monthView1.DayGrayedText = System.Drawing.SystemColors.GrayText;
            this.monthView1.DaySelectedBackgroundColor = System.Drawing.SystemColors.Highlight;
            this.monthView1.DaySelectedColor = System.Drawing.SystemColors.WindowText;
            this.monthView1.DaySelectedTextColor = System.Drawing.SystemColors.HighlightText;
            this.monthView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.monthView1.ItemPadding = new System.Windows.Forms.Padding(2);
            this.monthView1.Location = new System.Drawing.Point(315, 41);
            this.monthView1.MaxSelectionCount = 8;
            this.monthView1.MonthTitleColor = System.Drawing.SystemColors.ActiveCaption;
            this.monthView1.MonthTitleColorInactive = System.Drawing.SystemColors.InactiveCaption;
            this.monthView1.MonthTitleTextColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.monthView1.MonthTitleTextColorInactive = System.Drawing.SystemColors.InactiveCaptionText;
            this.monthView1.Name = "monthView1";
            this.monthView1.Size = new System.Drawing.Size(597, 153);
            this.monthView1.TabIndex = 0;
            this.monthView1.Text = "monthView1";
            this.monthView1.TodayBorderColor = System.Drawing.Color.Maroon;
            this.monthView1.Visible = false;
            // 
            // buscadorCustom1
            // 
            this.buscadorCustom1.BloquearBarraHerramientas = false;
            this.buscadorCustom1.CustomActualizar = null;
            this.buscadorCustom1.CustomDatos = false;
            this.buscadorCustom1.Dock = System.Windows.Forms.DockStyle.Left;
            this.buscadorCustom1.Imagen = ((System.Drawing.Image)(resources.GetObject("buscadorCustom1.Imagen")));
            this.buscadorCustom1.ImagenTitulo = global::SGVeterinaria.Properties.Resources.ConsultaMedica;
            this.buscadorCustom1.Location = new System.Drawing.Point(0, 0);
            this.buscadorCustom1.Name = "buscadorCustom1";
            this.buscadorCustom1.OcultarSiEstaVacio = false;
            this.buscadorCustom1.RequiereKey = false;
            this.buscadorCustom1.SeleccionGlobal = false;
            this.buscadorCustom1.Size = new System.Drawing.Size(315, 599);
            this.buscadorCustom1.TabIndex = 3;
            this.buscadorCustom1.Texto = "Buscar";
            this.buscadorCustom1.Titulo = "Tratamientos y cirugias programadas";
            this.buscadorCustom1.VerBotonActualizar = false;
            this.buscadorCustom1.VerBotonAgrupar = false;
            this.buscadorCustom1.VerBotonFiltro = false;
            this.buscadorCustom1.VerBotonGuardar = false;
            this.buscadorCustom1.VerBotonImagen = false;
            this.buscadorCustom1.VerBotonImprimir = false;
            this.buscadorCustom1.VerImagenCard = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Consulta");
            this.imageList1.Images.SetKeyName(1, "Quimicos");
            this.imageList1.Images.SetKeyName(2, "Vacunacion");
            this.imageList1.Images.SetKeyName(3, "Terminado");
            this.imageList1.Images.SetKeyName(4, "Quimicos");
            this.imageList1.Images.SetKeyName(5, "Pendiente");
            this.imageList1.Images.SetKeyName(6, "Peluqueria");
            this.imageList1.Images.SetKeyName(7, "Futuro");
            this.imageList1.Images.SetKeyName(8, "Desparacitar");
            this.imageList1.Images.SetKeyName(9, "Cirujia");
            this.imageList1.Images.SetKeyName(10, "Castracion");
            this.imageList1.Images.SetKeyName(11, "Cancelado");
            this.imageList1.Images.SetKeyName(12, "Dieta");
            this.imageList1.Images.SetKeyName(13, "Otro");
            // 
            // EditorAvisos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.calendar1);
            this.Controls.Add(this.monthView1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buscadorCustom1);
            this.Name = "EditorAvisos";
            this.Size = new System.Drawing.Size(912, 599);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Calendar.MonthView monthView1;
        private System.Windows.Forms.Calendar.Calendar calendar1;
        private System.Windows.Forms.Panel panel1;
        private gControls.BotonAccion btMes;
        private gControls.BotonAccion btDia;
        private gControls.BotonAccion btsemana;
        private gControls.Buscadores.BuscadorCustom buscadorCustom1;
        private System.Windows.Forms.ImageList imageList1;
    }
}
