﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gControls.Buscadores;
using gManager;
using gControls;
using BrightIdeasSoftware;

namespace SGVeterinaria.Controles
{
    public partial class BuscadorClientes : ListBase
    {
        System.Windows.Forms.ContextMenuStrip _menuclie = new System.Windows.Forms.ContextMenuStrip();
        System.Windows.Forms.ContextMenuStrip _menupaci = new System.Windows.Forms.ContextMenuStrip();
        public BuscadorClientes()
        {
            InitializeComponent();
            this.tw1.CanExpandGetter = delegate(object x)
            {
                return (x is gcDestinatario && (x as gcDestinatario).Pacientes.Count > 0);
            };
            this.tw1.ChildrenGetter = delegate(object x)
            {
                gcDestinatario dir = (gcDestinatario)x;
                return dir.Pacientes;
            };
            colNombre.ImageGetter = delegate(object rowObject)
            {
                gcDestinatario s = (gcDestinatario)rowObject;
                if(s.EsPaciente)
                    return s.Especie.ToString();
                return "Destinatario";
            };
            IniciarEventos();
            tw1.FormatCell += destinatario_FormatCell;
            EmpezarABuscar += (o, e) => { tw1.ExpandAll(); };
            gManager.CoreManager.Singlenton.DestinatarioCreado += (d) => { Actualizar(); };
            HandleMenues(ConfigurarMenues);
            SeleccionGlobal = true;
            //selecciona
            Lista.SelectionChanged += (o, e) =>
            {
                visorDestinatario1.Destinatario =Seleccionado ;
                visorDestinatario1.Visible = true;
            };
           
        }
        protected void ConfigurarMenues(object ob, EventArgs ev)
        {
            //clientes
            _menuclie.Items.Clear();
            if (RequisitoPermiso(gcUsuario.PermisoEnum.Vendedor))
                _menuclie.Items.Add(crearMenuItem(gControls.Properties.Resources.Vender, "Vender", "Crea una venta al cliente.",
                (o, e) => { Acciones.Vender(Seleccionado,null); }, Keys.None));
            if (RequisitoPermiso(gcUsuario.PermisoEnum.Contador))
               _menuclie.Items.Add(crearMenuItem(gControls.Properties.Resources.Editar, "Editar", "Edita el cliente seleccionado.",
               (o, e) =>
               {
                   if (Seleccionado != null) Editar.EditarDestinatario(Seleccionado);
                  
               }, Keys.None));
            if (RequisitoPermiso(gcUsuario.PermisoEnum.Encargado))
            {
                var np = crearMenuItem(gControls.Properties.Resources.Paciente, "Pacientes", "Agrega un nuevo paciente o selecciona uno existente", null, Keys.None);

                np.DropDownOpening += (ooo, eee) =>
                {
                    np.DropDownItems.Clear();
                    np.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.NuevoDestinatario, "Crear Paciente", "Agrega un nuevo paciente",
            (o, e) =>
            {
                if (Seleccionado != null)
                {
                    var p = new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Paciente);
                    p.Parent = Seleccionado.Id;
                    Editar.EditarDestinatario(p,new Editores.EditorPaciente());
                }
                else
                    Acciones.CrearPaciente(o, e);
            }
            , Keys.None));
                    if (Seleccionado != null && Seleccionado.Pacientes.Count > 0)
                    {
                        np.DropDownItems.Add(new ToolStripSeparator());
                        foreach (gcDestinatario p in Seleccionado.Pacientes)
                            np.DropDownItems.Add(
                                crearMenuItem(gControls.Properties.Resources.Paciente, p.Nombre, "",
                 (o, e) => { CoreManager.Singlenton.seleccionarDestinatario(p); }, Keys.None)
                                );

                    };
                };



                _menuclie.Items.Add(np);
                var na = crearMenuItem(gControls.Properties.Resources.Alarma, "Avisos", "Crear Avisos para este destinatario", null, Keys.None, true);
                na.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.Periodico, "Periódico", "Crea un aviso periódico", (op, ep) =>
                {
                    if (Seleccionado != null)
                    {
                        Editar.EditarTratamiento(new gcTratamiento(Seleccionado, gcTratamiento.TipoTratamiento.Periodico));
                        Actualizar();
                    }
                }));
                na.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.Alarma, "Recordatorio", "Crea un aviso recordatorio", (op, ep) =>
                {
                    if (Seleccionado != null)
                    {
                        Editar.EditarTratamiento(new gcTratamiento(Seleccionado, gcTratamiento.TipoTratamiento.Recordatorio));
                        Actualizar();
                    }
                }));
                _menuclie.Items.Add(na);
            }

            //pacientes
            _menupaci.Items.Clear();
            if (RequisitoPermiso(gcUsuario.PermisoEnum.Contador))
                _menupaci.Items.Add(crearMenuItem(gControls.Properties.Resources.Editar, "Editar", "Edita el paciente seleccionado.",
                (o, e) => { if (Seleccionado != null)Editar.EditarDestinatario(Seleccionado,new Editores.EditorPaciente()); }, Keys.None));
            if (RequisitoPermiso(gcUsuario.PermisoEnum.Encargado))
            {
                _menupaci.Items.Add(crearMenuItem(gControls.Properties.Resources.Editar, "Tratamientos", "Agrega un tratamiento a este paciente",
              (o, e) => { if (Seleccionado != null)Editar.EditarTratamiento(new gcTratamiento(Seleccionado, gcTratamiento.TipoTratamiento.Tratamiento)); }, Keys.None,true));
                var na = crearMenuItem(gControls.Properties.Resources.Mes, "Calendario", "Consultas y vacunación", null, Keys.None,false);
                na.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.Dia, "Ver Calendario", "Muestra calendario de vacunación y consultas medicas", (op, ep) =>
                {
                    if (Seleccionado != null)
                    {
                        Util.VerCalendarioVacunacion(Seleccionado);
                    }
                }));
                na.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.Periodico, "Periódico", "Crea un aviso periódico", (op, ep) =>
                {
                    if (Seleccionado != null)
                    {
                        var t = new gcTratamiento(Seleccionado, gcTratamiento.TipoTratamiento.Periodico);
                        t.Proximo = Editar.EditarFecha(DateTime.Now.AddDays(1));
                        Editar.EditarTratamiento(t);
                        Actualizar();
                    }
                },Keys.None,true));
                na.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.Alarma, "Recordatorio", "Crea un aviso recordatorio", (op, ep) =>
                {
                    if (Seleccionado != null)
                    {
                        Editar.EditarTratamiento(new gcTratamiento(Seleccionado, gcTratamiento.TipoTratamiento.Recordatorio));
                        Actualizar();
                    }
                }, Keys.None, true));
                _menupaci.Items.Add(na);
            }


            //menu
            tw1.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            tw1.ContextMenuStrip.Opening += (o, e) =>
            {
                e.Cancel = true;
                if (Seleccionado != null)
                {
                    if (Seleccionado.Tipo == gcDestinatario.DestinatarioTipo.Cliente)
                        _menuclie.Show(MousePosition);
                    else
                        _menupaci.Show(MousePosition);
                }
                

            };
                   
          
            
           

        }
        private void destinatario_FormatCell(object sender, FormatCellEventArgs e)
        {
            if (e.ColumnIndex == this.colCuenta.Index)
            {
                gcDestinatario customer = (gcDestinatario)e.Model;
                if (customer.Tipo == gcDestinatario.DestinatarioTipo.Paciente)
                {
                    e.SubItem.Text = "";
                }

                else if (customer.SaldoEnCuenta > 0)
                {
                    e.SubItem.ForeColor = Color.Red;
                    e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                    // e.SubItem.ImageSelector = Properties.Resources.Down;
                }
                else if (customer.SaldoEnCuenta < 0)
                {
                    e.SubItem.ForeColor = Color.DodgerBlue;
                    // e.SubItem.ImageSelector = Properties.Resources.Up;
                    e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                }
                else
                {
                    e.SubItem.ForeColor = Color.LightGray;
                }

            }
        }
        new public gcDestinatario Seleccionado
        {
            get { return base.Seleccionado as gcDestinatario; }
        }
        public override BrightIdeasSoftware.ObjectListView Lista
        {
            get { return tw1; }
        }
        protected override void Actualizar()
        {
            Datos = gManager.CoreManager.Singlenton.getDestinatarios(gcDestinatario.DestinatarioTipo.Cliente);
            base.Actualizar();
        }
       
    }
}
