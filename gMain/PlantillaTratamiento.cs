﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;


namespace SGVeterinaria
{
    [DataContract]
    public class PlantillaTratamiento
    {
       [DataMember]
        public int Periodo { get; set; }
       [DataMember]
        public string Titulo { get; set; }
       [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int Dias { get; set; }
    }
}
