﻿using gManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SGVeterinaria
{
    public class Paciente
    {
        private gcDestinatario _parent;
        private List<gcTratamiento> _vacunas = new List<gcTratamiento>();
        private List<gcTratamiento> _despara = new List<gcTratamiento>();
        private bool _castrado;
        public Paciente(gcDestinatario parent)
        {
            _parent = parent;
            foreach (gcTratamiento t in _parent.Tratamientos)
            {
                if (t.Estado == gcTratamiento.EstadoSeguimientoEnum.Terminado)
                {
                    if (t.TipoGenerico == (int)Util.vTratamientoEnum.Vacuna)
                    {
                        _vacunas.Add(t);
                    }
                    else if (t.TipoGenerico == (int)Util.vTratamientoEnum.Antiparacitario)
                    {
                        _despara.Add(t);
                    }
                    else if (t.TipoGenerico == (int)Util.vTratamientoEnum.Castracion)
                    {
                        _castrado = true;
                    }

                }
            
                
            }
        }
        public gcDestinatario Parent { get { return _parent; } }
        public string Edad
        {
            get
            {
                var ed = DateTime.Now.Subtract(_parent.Nacimiento);
                if (ed.TotalDays > 730)
                {

                    return ((int)(ed.TotalDays / 365d)) + " años";
                }
                else if (ed.TotalDays > 60)
                {
                    return ((int)(ed.TotalDays / 30)) + " meses";
                }
                else
                {
                    return ((int)(ed.TotalDays)) + " días";
                }
            }
        }
        public string Raza
        {
            get { return _parent.Direccion; }
        }
        public string Tamano
        {
            get { return _parent.Telefono; }
        }
        public string Color
        {
            get { return _parent.Celular; }
        }
        public string Pelo
        {
            get { return _parent.Cuit; }
        }
        public bool vacunado
        {
            get
            {
                return _vacunas.Count > 0;
            }
        }
        public bool Desparacitado
        {
            get
            {
                return _despara.Count > 0;
            }
        }
        public bool Castrado
        {
            get
            {
                return _castrado;
            }
        }
        public string Sexo
        {
            get { return EsMacho?"Hembra":"Macho";}
        }
        public bool EsMacho { get { return !Parent.Mail.StartsWith("h"); } }
    }
}
