﻿using gControls;
using gManager;
using Squirrel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SGVeterinaria
{
    class Util
    {
        public enum vTratamientoEnum
        {
            Vacuna,
            Antiparacitario,
            Consulta,
            Peluqueria,
            Cirujia,
            Inyeccion,
            Castracion
        }
        public static void AgregarVacunacion(gManager.gcDestinatario p)
        {
            if (MessageBox.Show( "Si agrega un calendario de vacunación a este paciente:"
                         + "\n* Se generan los avisos correspondientes en el calendario. Puede borrarlos o editarlos."
                         + "\n* Si ya los agregó antes se repiten."
                        + "\n* Si modifica o elimina el paciente debe eliminar manualmente los avisos.", "Crear calendario de vacunacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                            == System.Windows.Forms.DialogResult.Yes)
            {

                var l = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PlantillaTratamiento>>(JsonVacunas(p.Especie));
                foreach (PlantillaTratamiento t in l)
                {
                    gManager.gcTratamiento tra = new gManager.gcTratamiento(p, gManager.gcTratamiento.TipoTratamiento.Tratamiento);
                    tra.Fecha = p.Nacimiento;
                    tra.Proximo = p.Nacimiento.AddDays(t.Dias);
                    tra.Titulo = t.Titulo;
                    tra.Descripcion = t.Descripcion;
                    tra.saveMe();
                }

            }
           

        }
        private static string JsonVacunas(gManager.gcDestinatario.especieEnum espe)
        {
            
                string json = "";
                switch (espe)
                {
                    case gManager.gcDestinatario.especieEnum.Canino:
                        json = Properties.Settings.Default.VacunacionCanina;
                        break;
                    case gManager.gcDestinatario.especieEnum.Felino:
                        json = Properties.Settings.Default.VacunacionCanina;
                        break;
                    case gManager.gcDestinatario.especieEnum.Ave:
                        json = Properties.Settings.Default.VacunacionCanina;
                        break;
                    case gManager.gcDestinatario.especieEnum.Equino:
                        json = Properties.Settings.Default.VacunacionCanina;
                        break;
                    case gManager.gcDestinatario.especieEnum.Exotico:
                        json = Properties.Settings.Default.VacunacionCanina;
                        break;
                }
                return json;
            
        }

        public static void VerCalendarioVacunacion(gManager.gcDestinatario p)
        {
            Controles.Editores.EditorAvisos ea = new Controles.Editores.EditorAvisos();
            var f = ver.crearForm("EditorAvisosVete", "Ver Calendario Vacunación", ea, false);
            f.Text = "Calendario de vacunación de " + p.Nombre;
            f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Quimicos.GetHicon());
            ea.Paciente = p;
            f.ShowDialog();
           
        }
        public static int ChoseTipoTratamiento()
        {
            var f = new gControls.Formularios.fOpciones();
            f.Text = "Seleccionar el tipo de tratamiento";
            f.setDescripcion("El tipo de tratamiento le ayuda a diferenciar.");
            f.StartPosition = FormStartPosition.CenterScreen;
            foreach (vTratamientoEnum a in Enum.GetValues(typeof(vTratamientoEnum)))
            {
                f.addBoton(a.ToString(), (int)a, getImagen(a));
            }
            if (f.ShowDialog() == DialogResult.OK)
            {
                return f.Seleccion;
            }
            return -1;
        }

        
        public static System.Drawing.Image getImagen(vTratamientoEnum t)
        {
             switch(t)
                {
                 case vTratamientoEnum.Antiparacitario :
                        return Properties.Resources.Desparacitar;
                 case vTratamientoEnum.Castracion :
                        return Properties.Resources.Castracion;
                 case vTratamientoEnum.Cirujia:
                        return Properties.Resources.Cirujia;
                 case vTratamientoEnum.Consulta:
                        return Properties.Resources.ConsultaMedica;
                 case vTratamientoEnum.Inyeccion:
                        return Properties.Resources.Vacunacion;
                 case vTratamientoEnum.Peluqueria:
                        return Properties.Resources.Peluqueria;
                 case vTratamientoEnum.Vacuna:
                        return Properties.Resources.Vacunacion;
                 default:
                     return Properties.Resources.Quimicos;

                }

        }
        public static System.Drawing.Image getImagen(gManager.gcDestinatario.especieEnum t)
        {
            switch (t)
            {
                case gManager.gcDestinatario.especieEnum.Ave :
                    return Properties.Resources.Ave;
                case gManager.gcDestinatario.especieEnum.Canino:
                    return Properties.Resources.Perro;
                case gManager.gcDestinatario.especieEnum.Equino:
                    return Properties.Resources.Equino;
                case gManager.gcDestinatario.especieEnum.Exotico:
                    return Properties.Resources.Exotico;
                case gManager.gcDestinatario.especieEnum.Felino:
                    return Properties.Resources.Gato;
                case gManager.gcDestinatario.especieEnum.Ganado:
                    return Properties.Resources.Ganado;
                case gManager.gcDestinatario.especieEnum.Granja:
                    return Properties.Resources.AveGranja;
                default:
                    return Properties.Resources.Mascota;

            }

        }
        
        public static string parseHtmlPaciente(gManager.gcDestinatario p)
        {
            var pa = new Paciente(p);
            //imagen
            string img = gManager.ImageManager.HasImage(p) ? gManager.ImageManager.getPath(p) :
                Imagen64(Properties.Resources.Gatitos);
            //especie
            string especie =imagehtml(getImagen(p.Especie),p.Especie.ToString(),new System.Drawing.Point(32,32));
            //sexo
            string sex = imagehtml(!pa.EsMacho ?Properties.Resources.Hembra :Properties.Resources.Macho , pa.Sexo, new System.Drawing.Point(32, 32));
            string castrado = pa.Castrado ? imagehtml(Properties.Resources.Castracion, "Castrado", new System.Drawing.Point(32, 32)) : "";
                
            return Properties.Resources.PlantillaPaciente.Replace("{nombre}", p.Nombre)
                .Replace("{raza}", pa.Raza)
                .Replace("{img}", img)
                .Replace("{sexo}", sex)
                 .Replace("{edad}",pa.Edad )
                 .Replace("{dueño}", p.PropietarioNombre)
                 .Replace("{estado}", especie+sex+castrado)
            .Replace("{especie}", especie);
        }
        private static string imagehtml(System.Drawing.Image img, string title, System.Drawing.Point p)
        {
            return "<img  style=\"height: " + p.X + "px; width: " + p.Y + "px\" src=\"" +Imagen64(img) + "\" title=\"" + title + "\" />";
        }
        private static string Imagen64(System.Drawing.Image img)
        {
            return "data:image/png;base64," + gManager.ImageManager.getStringImage(img);
        }

       
    }
}
