﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGVeterinaria.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.1.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"[
  {
    ""Periodo"": ""0"",
    ""Dias"": ""45"",
    ""Descripcion"": ""Entre los 45 y 50 dias"",
    ""Titulo"": ""Vacunar contra el parvovirus""
  },
  {
    ""Periodo"": ""0"",
    ""Dias"": ""60"",
    ""Descripcion"": ""moquillo, parvovirus, hepatitis"",
    ""Titulo"": ""Aplicar la tripleviral ""
  },
  {
    ""Periodo"": ""365"",
    ""Dias"": ""80"",
    ""Descripcion"": ""moquillo, parvovirus, coronavirus, hepatitis, leptospirosis"",
    ""Titulo"": ""Aplicar la vacuna pentavalente o quíntuple""
  },
  {
    ""Periodo"": ""365"",
    ""Dias"": ""120"",
    ""Descripcion"": ""Vacunar individual"",
    ""Titulo"": ""Vacunar contra la rabia""
  }
]")]
        public string VacunacionCanina {
            get {
                return ((string)(this["VacunacionCanina"]));
            }
            set {
                this["VacunacionCanina"] = value;
            }
        }
    }
}
