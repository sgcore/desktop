﻿namespace SGVeterinaria
{
    partial class fPrincipal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fPrincipal));
            this.switchPanel1 = new gControls.Paneles.SwitchPanel();
            this.acordionContainer1 = new gControls.UI.AcordionContainer();
            this.visorMensajes1 = new gControls.Visores.VisorMensajes();
            this.SuspendLayout();
            // 
            // switchPanel1
            // 
            this.switchPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.switchPanel1.Imagen = null;
            this.switchPanel1.Location = new System.Drawing.Point(0, 24);
            this.switchPanel1.Name = "switchPanel1";
            this.switchPanel1.RequiereKey = false;
            this.switchPanel1.Size = new System.Drawing.Size(672, 637);
            this.switchPanel1.TabIndex = 7;
            this.switchPanel1.Texto = null;
            // 
            // acordionContainer1
            // 
            this.acordionContainer1.Alineacion = System.Windows.Forms.DockStyle.Right;
            this.acordionContainer1.AnchoFijo = 509;
            this.acordionContainer1.AutoScroll = true;
            this.acordionContainer1.BotoneraVisible = true;
            this.acordionContainer1.Control = null;
            this.acordionContainer1.Dock = System.Windows.Forms.DockStyle.Right;
            this.acordionContainer1.ExpandedWidth = 509;
            this.acordionContainer1.Frozen = false;
            this.acordionContainer1.Imagen = null;
            this.acordionContainer1.Location = new System.Drawing.Point(672, 24);
            this.acordionContainer1.Name = "acordionContainer1";
            this.acordionContainer1.RequiereKey = false;
            this.acordionContainer1.Size = new System.Drawing.Size(509, 637);
            this.acordionContainer1.TabIndex = 6;
            this.acordionContainer1.Texto = null;
            // 
            // visorMensajes1
            // 
            this.visorMensajes1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.visorMensajes1.Imagen = null;
            this.visorMensajes1.Location = new System.Drawing.Point(0, 661);
            this.visorMensajes1.Name = "visorMensajes1";
            this.visorMensajes1.RequiereKey = false;
            this.visorMensajes1.Size = new System.Drawing.Size(1181, 24);
            this.visorMensajes1.TabIndex = 8;
            this.visorMensajes1.Texto = null;
            // 
            // fPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 685);
            this.Controls.Add(this.switchPanel1);
            this.Controls.Add(this.acordionContainer1);
            this.Controls.Add(this.visorMensajes1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "fPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SGCore";
            this.Controls.SetChildIndex(this.visorMensajes1, 0);
            this.Controls.SetChildIndex(this.acordionContainer1, 0);
            this.Controls.SetChildIndex(this.switchPanel1, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private gControls.Paneles.SwitchPanel switchPanel1;
        private gControls.UI.AcordionContainer acordionContainer1;
        private gControls.Visores.VisorMensajes visorMensajes1;
    }
}

