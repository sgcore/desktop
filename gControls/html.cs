﻿using gControls.ControlesHtml.htmlObjects;
using gManager;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using TheArtOfDev.HtmlRenderer.Core.Entities;

namespace gControls
{
    public static class html
    {
        public static string GetStylesheet(string source)
        {
            if (source == "StyleSheet")
                return Properties.Resources.styles;
            return Properties.Resources.styles; 
        }
        public static void OnStylesheetLoad(object sender, HtmlStylesheetLoadEventArgs e)
        {
            var stylesheet = GetStylesheet(e.Src);
            if (stylesheet != null)
                e.SetStyleSheet = stylesheet;
        }
        public static void OnImageLoadPdfSharp(object sender, HtmlImageLoadEventArgs e)
        {
            //ImageLoad(e, true);
        }
        private static string addParrafo(string p)
        {
            return "<pre>" + p + "</pre>.";
        }
        public static string addTitulo(string p)
        {
            return " <h3>" + p + "</h3>";
        }
        public static string addSubTitulo(string p)
        {
            return " <h5>" + p + "</h5>";
        }
        public static string addLabelGreen(string titulo, string value)
        {
            return "<table cellspacing=\"5\"><tr><td width=\"350px\" class =\"c1\">" + titulo + "</td><td class =\"c5\">" + value + "</td></tr></table>";
        }
        public static string addLabelRed(string titulo, string value)
        {
            return "<table cellspacing=\"5\"><tr><td width=\"350px\" class =\"c1\">" + titulo + "</td><td class =\"c3\">" + value + "</td></tr></table>";
        }
        public static string addLabelBlue(string titulo, string value)
        {
            return "<table cellspacing=\"5\"><tr><td width=\"350px\" class =\"c1\">" + titulo + "</td><td class =\"c2\">" + value + "</td></tr></table>";
        }
        public static string addLabelYellow(string titulo, string value)
        {
            return "<table cellspacing=\"5\"><tr><td width=\"350px\" class =\"c1\">" + titulo + "</td><td class =\"c4\">" + value + "</td></tr></table>";
        }
        public static string addRow(string[] l)
        {
            string t = "<tr>";
            foreach (string s in l)
            {
                t += "<td>" + s + "</td>";
            }
            return  t+"</tr>" ;
        }
        public static string InformeCaja(gManager.ResumenCajas c,bool pedirPlantilla=false)
        {
            string t = getPlantillaCaja("Plantilla de informe de caja","Plantilla Caja|resumencaja.html",pedirPlantilla);
            
            if (!String.IsNullOrEmpty(t)) return  parseCajas(t,c);

            t = "<body>";
                
                t+=addTitulo(c.descripcion);
                t += addSubTitulo("DINERO EN EFECTIVO");
            var vf=new Visores.VisorBilletes();
            var vs = new Visores.VisorBilletes();
            vf.setPago(c.Efectivo, "DINERO EN EFECTIVO");
            vs.setPago(c.SiguienteDepositoInicial, "DEPOSITO INICIAL");
            t += addParrafo(
                addLabelGreen("Deposito inicial", c.EfectivoEnDepositoInicial.Monto.ToString("$0.00"))
                 + addLabelGreen("Depositos en general", c.EfectivoObtenidoEnDepositos.Monto.ToString("$0.00"))
                 + addLabelGreen("Pagos en efectivo por ventas", c.EfectivoObtenidoEnVentas.Monto.ToString("$0.00"))
                  + addLabelRed("Gastos varios y extracciones", c.EfectivoEnExtracciones.Monto.ToString("$0.00"))
                  + addLabelRed("Gastos en compras", c.EfectivoGastadoEnCompras.Monto.ToString("$0.00"))
                  +addParrafo(
                getImageStringHtm(UtilControls.ImageFromContro(vf))
                + addLabelGreen("Dinero en Efectivo", c.Efectivo.Monto.ToString("$0.00")))
                + addParrafo(
                getImageStringHtm(UtilControls.ImageFromContro(vs))
                + addLabelYellow("Deposito Caja Siguiente", c.SiguienteDepositoInicial.Monto.ToString("$0.00")))
                 + addLabelBlue("Recaudacion", c.Recaudacion.Monto.ToString("$0.00"))

                );
            t += addTitulo("Ventas");
            string tmp = "";
            decimal totalventas=0;
            foreach (var x in c.VentasPorCategorias)
            {
                totalventas += x.Total;
                tmp += addLabelBlue(x.Nombre, x.Total.ToString("$0.00"));
            }
            t += addParrafo(addSubTitulo("Resumen de ventas por categorias principales") + addParrafo(tmp)
                +addLabelGreen("Total vendido", totalventas.ToString("$0.00"))
                + addLabelGreen("Pagos en efectivo por ventas", c.EfectivoObtenidoEnVentas.Monto.ToString("$0.00"))
                + addLabelYellow("Pagos con cuenta corriente", c.PagosCuentasCLientes.Monto.ToString("$0.00"))
                + addLabelRed("Descuentos", c.EfectivoPerdidoEnDescuentos.Monto.ToString("$0.00"))
                );


            return t+"</body>";
        }
        public static string InformeVentas(List<gcCaja> cajas)
        {
            var cr =new htmlResumenCategorias(cajas);
            return cr.ToString();
        }
        public static string Preimpresa(gManager.gcMovimiento m, bool pedirPlantilla = false)
        {
            string t = getPlantillaPreimpresa("Factura Preimpresa", "Plantilla Preimpresa|*.html", pedirPlantilla, (int)m.Tipo);

            if (t != "") return parseMovis(t,m);

            t = "<h3>No selecciono una plantilla</h3>";
            return t;
        }
        public static string Ticket(gManager.gcMovimiento m, bool pedirPlantilla = false, gcMovimiento.TipoMovEnum tipo = gcMovimiento.TipoMovEnum.Venta)
        {

            string t = getPlantillaTicket("Ticket " + tipo.ToString(), "Plantilla Ticket|*.txt", pedirPlantilla, tipo);
            if (String.IsNullOrEmpty(t)) return "";
          
           
            return parseMovis(t, m);
        }
        private static string getImageStringHtm(System.Drawing.Bitmap bm)
        {
           
            return "<img style=\"width: 140;\" src=\"" + getImageString64(bm)+"\">"; //here you should get a base64 string
        }
        public static string getImageString64(System.Drawing.Bitmap bm)
        {
           
            return "data:image/png;base64," +ImageManager.getStringImage(bm) ; //here you should get a base64 string
        }
        public static string getProductoImageString(gcObjeto p)
        {
            return getImageString64 (new Bitmap(ImageManager.getImageGeneric(p)));
        }
        public static string getPagoImagenHtml(gcPago p, string title)
        {
            var vs = new Visores.VisorBilletes();
            vs.setPago(p, title);
            return getImageStringHtm(UtilControls.ImageFromContro(vs));
        }
        //plantilla
        public static string getPlantillaCaja(string titulo, string filtro, bool elegir)
        {
            string line = "";
            string path = Properties.Settings.Default.PlantillaInfoCaja;
            if (elegir || (!String.IsNullOrEmpty(path) && !System.IO.File.Exists(path)))
            {
                path = UtilControls.ObtenerDireccionDeArchivo(titulo, filtro);
                Properties.Settings.Default.PlantillaInfoCaja = path;
                Properties.Settings.Default.Save();

            }
            if (!String.IsNullOrEmpty(path))
            {

                using (StreamReader reader = new StreamReader(path, System.Text.Encoding.Default, false))
                {
                    line = reader.ReadToEnd();
                }

            }
            return line;
        }
        public static string getPlantillaTicket(string titulo, string filtro, bool elegir, gcMovimiento.TipoMovEnum tipo)
        {
            string line = "";
            string[] array = Properties.Settings.Default.PlantillaTicket[(int)tipo].Split(';');
            string print = array.Length > 0 ? array[0] : "";
            string path = array.Length > 1 ? array[1] : "";
           
            if (elegir || String.IsNullOrEmpty(path) || !System.IO.File.Exists(path))
            {
                path = UtilControls.ObtenerDireccionDeArchivo(titulo, filtro);
                Properties.Settings.Default.PlantillaTicket[(int)tipo] = print + ";" +path;
                Properties.Settings.Default.Save();

            }
            if (path != "")
            {

                using (StreamReader reader = new StreamReader(path, System.Text.Encoding.Default, false))
                {
                    line = reader.ReadToEnd();
                }

            }
            return line;
        }
        public static string getPlantillaPreimpresa(string titulo, string filtro, bool elegir, int tipo)
        {
            string line = "";
             string[] array = Properties.Settings.Default.PlantillaPreimpresa[tipo].Split(';');
            string print = array.Length > 0 ? array[0] : "";
            string path = array.Length > 1 ? array[1] : "";
            if (elegir || String.IsNullOrEmpty(path) || !System.IO.File.Exists(path))
            {
                path = UtilControls.ObtenerDireccionDeArchivo(titulo, filtro);
                Properties.Settings.Default.PlantillaPreimpresa[tipo] = print + ";" + path;
                Properties.Settings.Default.Save();

            }
            if (path != "")
            {

                using (StreamReader reader = new StreamReader(path, System.Text.Encoding.Default, false))
                {
                    line = reader.ReadToEnd();
                }

            }
            return line;
        }
        internal class Plantilla
        {
            public string Tag { get; set; }
            public int position { get; set; }
            public int endPosition {get;set;}
            public int Cont { get { return endPosition - position; } }
        

        }
        internal static List<Plantilla> sacarPlantillas(string t)
        {
            var l = new List<Plantilla>();
            int pos = 0;
           
            while (pos < t.Length && pos>-1)
            {
                pos = t.IndexOf("{", pos);
                if (pos < t.Length && pos > -1)
                {
                    int close=t.IndexOf("}",pos);
                    if(close < t.Length && close>-1){

                        var n = new Plantilla();
                        n.endPosition = close+1;
                        n.position = pos;
                        char[] text= new char[n.Cont];
                        t.CopyTo(pos, text, 0,n.Cont);
                        n.Tag = new string(text);
                        l.Add(n);
                        pos = close;
                        
                    }
                }
            }
            return l;
        }
        private static string parseCajas(string p, ResumenCajas c)
        {
            var plant = html.sacarPlantillas(p);
            int offset = 0;
            string ret = p;
            foreach (var pl in plant)
            {
                int temp = pl.Tag.Length;
                string data = "";

                if (pl.Tag == "{efeIni}") data = c.EfectivoEnDepositoInicial.Monto.ToString("0.00");
                else if (pl.Tag == "{efeDep}") data = c.EfectivoObtenidoEnDepositos.Monto.ToString("0.00");
                else if (pl.Tag == "{efeVent}") data = c.EfectivoObtenidoEnVentas.Monto.ToString("0.00");
                else if (pl.Tag == "{efeInTotal}") data = c.EfectivoTotalEntrante.Monto.ToString("0.00");
                else if (pl.Tag == "{efeExt}") data = c.EfectivoEnExtracciones.Monto.ToString("0.00");
                else if (pl.Tag == "{efeComp}") data = c.EfectivoGastadoEnCompras.Monto.ToString("0.00");
                else if (pl.Tag == "{efeOutTotal}") data = c.EfectivoTotalSaliente.Monto.ToString("0.00");
                else if (pl.Tag == "{efeTotal}") data = c.Efectivo.Monto.ToString("0.00");
                else if (pl.Tag == "{efeSig}") data = c.SiguienteDepositoInicial.Monto.ToString("0.00");
                else if (pl.Tag == "{efeReca}") data = c.Recaudacion.Monto.ToString("0.00");
                else if (pl.Tag == "{efeIniImg}") data = html.getPagoImagenHtml(c.EfectivoEnDepositoInicial, "DEPÓSITO INICIAL");
                else if (pl.Tag == "{efeDepImg}") data = html.getPagoImagenHtml(c.EfectivoObtenidoEnDepositos, "DEPÓSITOS");
                else if (pl.Tag == "{efeVentImg}") data = html.getPagoImagenHtml(c.EfectivoObtenidoEnVentas, "EFECTIVO POR VENTAS");
                else if (pl.Tag == "{efeInTotalImg}") data = html.getPagoImagenHtml(c.EfectivoTotalEntrante, "TOTAL OBTENIDO");
                else if (pl.Tag == "{efeExtImg}") data = html.getPagoImagenHtml(c.EfectivoEnExtracciones, "EXTRACCIONES");
                else if (pl.Tag == "{efeCompImg}") data = html.getPagoImagenHtml(c.EfectivoGastadoEnCompras, "GASTOS EN COMPRAS");
                else if (pl.Tag == "{efeOutTotalImg}") data = html.getPagoImagenHtml(c.EfectivoTotalSaliente, "TOTAL GASTOS");
                else if (pl.Tag == "{efeTotalImg}") data = html.getPagoImagenHtml(c.Efectivo, "EFECTIVO");
                else if (pl.Tag == "{efeSigImg}") data = html.getPagoImagenHtml(c.SiguienteDepositoInicial, "SIGUIENTE CAJA");
                else if (pl.Tag == "{efeRecaImg}") data = html.getPagoImagenHtml(c.Recaudacion, "RECAUDACIÓN");
                else if (pl.Tag == "{cajaDesc}") data = c.descripcion;
                else if (pl.Tag == "{cajaDescu}") data = c.EfectivoPerdidoEnDescuentos.Monto.ToString("0.00");
                else if (pl.Tag.StartsWith("{catList")) data = parsecats(pl.Tag, c);
                else if (pl.Tag.StartsWith("{movList")) data = parseMovList(pl.Tag, c);
                else if (pl.Tag.StartsWith("{ccList")) data = parseCuentas(pl.Tag, c);
                else
                    continue;


                ret = ret.Remove(pl.position + offset, pl.Cont);
                ret = ret.Insert(pl.position + offset, data);
                offset += data.Length - pl.Tag.Length;




            }
            string path = UtilControls.ObtenerDireccionDeArchivo("Guardar informe de caja", "Archivo html | *.html", true);
            if(!string.IsNullOrEmpty(path))
            {
                System.IO.File.WriteAllText(path, ret);
                System.Diagnostics.Process.Start(path);
            }
            return ret;
           
        }
        private static string parseCuentas(string p, ResumenCajas c)
        {
            string data = "";
            p = p.Replace("{ccList", "").Replace("}", "");
            var l = CoreManager.Singlenton.MovimientosEnCuentasCorrientes(c.Cajas);
            string cc = p[0].ToString();
            var todos = false;
            gcDestinatario.DestinatarioTipo tipo = gcDestinatario.DestinatarioTipo.Cliente;
            if (cc == "C") tipo = gcDestinatario.DestinatarioTipo.Cliente ;
            else if (cc == "P") tipo = gcDestinatario.DestinatarioTipo.Proveedor ;
            else if (cc == "R") tipo = gcDestinatario.DestinatarioTipo.Responsable;
            else if (cc == "F") tipo = gcDestinatario.DestinatarioTipo.Financiera;
            else if (cc == "B") tipo = gcDestinatario.DestinatarioTipo.Banco;
            else todos = true;
           
            p = p.Remove(0, 1);

            foreach (var x in l)
            {
                if (x.Destinatario == null) continue;
                if (!todos && x.Destinatario.Tipo != tipo) continue;
                data += p.Replace("[ccNombre]", x.DescripcionDestinatario).Replace("[ccPago]", x.Total.ToString("0.00")).Replace("[ccSaldo]", x.Destinatario.SaldoEnCuenta.ToString("0.00"));
            }
            return data;
        }
        private static string parsecats(string p, ResumenCajas c)
        {
            string data = "";
            p = p.Replace("{catList", "").Replace("}", "");

            foreach (var x in c.VentasPorCategorias)
            {
                   data += p.Replace("[vCatNom]", x.Nombre).Replace("[vCatCod]", x.Codigo).Replace("[vCatCan]", x.Cantidad.ToString("0.00")).Replace("[vCatLink]", x.Link).Replace("[vCatTotal]", x.Total.ToString("$0.00"));
            }
            return data;
        }
        private static string parseMovList(string p, ResumenCajas c)
        {
            string data = "";
            p = p.Replace("{movList", "").Replace("}", "");
            string cc = p[0].ToString();
            var todos = false;
            gcMovimiento.TipoMovEnum tipo = gcMovimiento.TipoMovEnum.Deposito;
            if (cc == "C") tipo = gcMovimiento.TipoMovEnum.Compra;
            else if (cc == "V") tipo = gcMovimiento.TipoMovEnum.Venta;
            else if (cc == "D") tipo = gcMovimiento.TipoMovEnum.Deposito;
            else if (cc == "E") tipo = gcMovimiento.TipoMovEnum.Extraccion;
            else if (cc == "O") tipo = gcMovimiento.TipoMovEnum.Devolucion ;
            else if (cc == "R") tipo = gcMovimiento.TipoMovEnum.Retorno ;
            else if (cc == "T") tipo = gcMovimiento.TipoMovEnum.Entrada;
            else if (cc == "S") tipo = gcMovimiento.TipoMovEnum.Salida;
            else if (cc == "P") tipo = gcMovimiento.TipoMovEnum.Presupuesto;
            else if (cc == "I") tipo = gcMovimiento.TipoMovEnum.Pedido;
            else todos = true;
            p = p.Remove(0, 1);

            foreach (var m in c.Movimientos)
            {
                if (!todos && m.Tipo != tipo) continue;
                data += p.Replace("[mFechaS]", m.Fecha.ToShortDateString())
                   .Replace("[mFechaL]", m.Fecha.ToLongDateString())
                   .Replace("[mDest]", m.DescripcionDestinatario)
                   .Replace("[mSubTotal]", m.TotalMovimiento.ToString("0.00"))
                   .Replace("[mObserv]", m.Observaciones)
                   .Replace("[mTotal]", (m.TotalMovimiento - m.TotalDescontado).ToString("0.00"))
                    .Replace("[mNum]", m.Numero.ToString("00000"))
                   .Replace("[mDescu]", m.TotalDescontado.ToString("0.00"));
            
            }
            return data;
        }
        private static string parseMovis(string p, gcMovimiento m)
        {
            var plant = html.sacarPlantillas(p);
            int offset = 0;
            string ret = p;
            if (String.IsNullOrEmpty(ret)) return "";
            foreach (var pl in plant)
            {
                int temp = pl.Tag.Length;
                string data = "";

                if (pl.Tag == "{mFechaL}") data = m.Fecha.ToLongDateString();
                else if (pl.Tag == "{mFechaS}") data = m.Fecha.ToShortDateString();
                else if (pl.Tag == "{mNumero}") data = m.Numero.ToString("00000");
                else if (pl.Tag == "{mDest}") data = m.DescripcionDestinatario;
                else if (pl.Tag == "{mDestSaldo}") data = m.Destinatario.SaldoEnCuenta.ToString("$0.00");
                else if (pl.Tag == "{mUsuario}") data = m.DescripcionUsuario;
                else if (pl.Tag == "{mSubTotal}") data = m.TotalMovimiento.ToString("0.00");
                else if (pl.Tag == "{mDescu}") data = m.TotalDescontado.ToString("0.00");
                else if (pl.Tag == "{mDescuPorc}") data = m.DescuentoPorcentual.ToString("0.00");
                else if (pl.Tag == "{mDestIva}") data = m.Destinatario.TipoIva.ToString();
                else if (pl.Tag == "{mDestCuit}") data = m.Destinatario.Cuit.ToString();
                else if (pl.Tag == "{mDestDir}") data = m.Destinatario.Direccion.ToString();
                else if (pl.Tag == "{mTotal}") data = (m.TotalMovimiento - m.TotalDescontado).ToString("$0.00");
                else if (pl.Tag.StartsWith("{OMList")) data = parseOM(pl.Tag, m);
                else if (pl.Tag == "{mPagoEfe}") data = m.PagosEnEfectivo.Monto.ToString("$0.00");
                else if (pl.Tag == "{mPagoVuelto}") data = m.PagosEnVueltos.Monto.ToString("$0.00");
                else if (pl.Tag == "{mPagoCuenta}") data = m.PagosEnCuentas.Monto.ToString("$0.00");
                else if (pl.Tag == "{mPagoTarjeta}") data = m.PagosEnTarjeta.Monto.ToString("$0.00");
                else if (pl.Tag == "{mPagoCheque}") data = m.PagosEnCheques.Monto.ToString("$0.00");
                else
                    continue;


                ret = ret.Remove(pl.position + offset, pl.Cont);
                ret = ret.Insert(pl.position + offset, data);
                offset += data.Length - pl.Tag.Length;




            }
            return ret;
        }
        private static string parseOM(string p, gcMovimiento m)
        {
            string data = "";
            p = p.Replace("{OMList", "").Replace("}", "");

            foreach (gcObjetoMovido x in m.ObjetosMovidos)
            {
                data += p.Replace("[vOMCan]", x.Cantidad.ToString("0.00")).Replace("[vOMCod]", x.CodigoProducto).Replace("[vOMNom]", x.NombreProducto).Replace("[vOMPrec]", x.Monto.ToString("0.00")).Replace("[vOMTotal]", x.Total.ToString("$0.00"));
            }
            return data;
        }
       
        
    }
}
