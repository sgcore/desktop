﻿using gManager;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    public static class UtilControls
    {
        /// <summary>
        /// Resalta un control asignandole al fondo el color especificado
        /// </summary>
        /// <param name="c">Control que se va a resaltar</param>
        /// <param name="color">0 gris, 1 Azul, 2 Verde, 3 Amarillo, 4 Rojo </param>
        public static void ResaltarControl(Control c, int color)
        {
            var i = Properties.Resources.FondoGris;
            switch (color)
            {
                case 1:
                    i = Properties.Resources.FondoAzul;
                    break;
                case 2:
                    i = Properties.Resources.FondoVerde;
                    break;
                case 3:
                    i = Properties.Resources.FondoAmarillo;
                    break;
                case 4:
                    i = Properties.Resources.FondoRojo;
                    break;
            }
            c.BackgroundImage = i;
            c.BackgroundImageLayout = ImageLayout.Stretch;

        }
        /// <summary>
        /// Resalta un control asignandole al fondo el color especificado
        /// </summary>
        /// <param name="c">Control que se va a resaltar</param>
        /// <param name="color">0 gris, 1 Azul, 2 Verde, 3 Amarillo, 4 Rojo </param>
        public static void ResaltarControl(ToolStripItem c, int color)
        {
            var i = Properties.Resources.FondoGris;
            switch (color)
            {
                case 1:
                    i = Properties.Resources.FondoAzul;
                    break;
                case 2:
                    i = Properties.Resources.FondoVerde;
                    break;
                case 3:
                    i = Properties.Resources.FondoAmarillo;
                    break;
                case 4:
                    i = Properties.Resources.FondoRojo;
                    break;
            }
            c.BackgroundImage = i;
            c.BackgroundImageLayout = ImageLayout.Stretch;

        }
        public static Bitmap imageFromMovTipo(gcMovimiento.TipoMovEnum tipo)
        {
            var ret = Properties.Resources.ImageOk;
            switch (tipo)
            {
                case gcMovimiento.TipoMovEnum.Compra:
                    ret = Properties.Resources.Compra;
                    break;
                case gcMovimiento.TipoMovEnum.Devolucion:
                    ret = Properties.Resources.Devolucion;
                    break;
                case gcMovimiento.TipoMovEnum.Entrada:
                    ret = Properties.Resources.Entrada;
                    break;
                case gcMovimiento.TipoMovEnum.Deposito:
                    ret = Properties.Resources.Deposito;
                    break;
                case gcMovimiento.TipoMovEnum.Pedido:
                    ret = Properties.Resources.Pedido;
                    break;
                case gcMovimiento.TipoMovEnum.Venta:
                    ret = Properties.Resources.Venta;
                    break;
                case gcMovimiento.TipoMovEnum.Salida:
                    ret = Properties.Resources.Salida;
                    break;
                case gcMovimiento.TipoMovEnum.Retorno:
                    ret = Properties.Resources.Retornar;
                    break;
                case gcMovimiento.TipoMovEnum.Extraccion:
                    ret = Properties.Resources.Extraccion;
                    break;
                case gcMovimiento.TipoMovEnum.Presupuesto:
                    ret = Properties.Resources.Presupuesto;
                    break;

            }

            return ret;

        }

        public static Bitmap imageFromMovEstado(gcMovimiento.EstadoEnum estado)
        {
            var ret = Properties.Resources.ImageOk;
            switch (estado)
            {
                case gcMovimiento.EstadoEnum.Creado:
                    ret = Properties.Resources.Tools;
                    break;
                case gcMovimiento.EstadoEnum.Modificado:
                    ret = Properties.Resources.Time;
                    break;
                case gcMovimiento.EstadoEnum.Terminado:
                    ret = Properties.Resources.Run;
                    break;
               

            }

            return ret;

        }
        public static Bitmap imageFromMovTipoFactura(gcMovimiento.TipoFacturaEnum tipoFac)
        {
            var ret = Properties.Resources.ImageOk;
            switch (tipoFac)
            {
                case gcMovimiento.TipoFacturaEnum.A:
                    ret = Properties.Resources.A;
                    break;
                case gcMovimiento.TipoFacturaEnum.C:
                    ret = Properties.Resources.C;
                    break;
                case gcMovimiento.TipoFacturaEnum.R:
                    ret = Properties.Resources.R;
                    break;
                case gcMovimiento.TipoFacturaEnum.X:
                    ret = Properties.Resources.X;
                    break;
            }

            return ret;

        }

        public static Bitmap imageFromDestTipo(gcDestinatario.DestinatarioTipo tipo)
        {
            var ret = Properties.Resources.ImageOk;
            switch (tipo)
            {
                case gcDestinatario.DestinatarioTipo.Banco:
                    ret = Properties.Resources.Banco;
                    break;
                case gcDestinatario.DestinatarioTipo.Cliente:
                    ret = Properties.Resources.Cliente;
                    break;
                case gcDestinatario.DestinatarioTipo.Contacto:
                    ret = Properties.Resources.Contacto;
                    break;
                case gcDestinatario.DestinatarioTipo.Financiera:
                    ret = Properties.Resources.Financiera;
                    break;
                case gcDestinatario.DestinatarioTipo.Paciente:
                    ret = Properties.Resources.Paciente;
                    break;
                case gcDestinatario.DestinatarioTipo.Proveedor:
                    ret = Properties.Resources.Proveedor;
                    break;
                case gcDestinatario.DestinatarioTipo.Responsable:
                    ret = Properties.Resources.Usuario;
                    break;
                case gcDestinatario.DestinatarioTipo.Sucursal:
                    ret = Properties.Resources.Sucursal;
                    break;

            }

            return ret;

        }
        public static Color ColorStok(gcObjeto producto)
        {
            if(producto.Tipo == gcObjeto.ObjetoTIpo.Servicio && !producto.HayStock)
            {
                return Color.SteelBlue;
            }
            if (producto.HayMuchoStock)
            {
                return Properties.Settings.Default.ColorSuccess;
            } else if (producto.HayPocoStock)
            {
                return Properties.Settings.Default.ColorWarning;
            }else
            {
                return Properties.Settings.Default.ColorError;
            }
        }
        public static string ObtenerDireccionDeArchivo(string titulo, string filtro = "Todos los archivos|*.*", bool guardar=false,string defa="",string dir="")
        {
            FileDialog fimg = null;
           if (guardar)
               fimg = new SaveFileDialog();
           else
               fimg = new OpenFileDialog();
           fimg.Title = titulo;
           fimg.FileName = defa;
           fimg.InitialDirectory = dir;
           fimg.Filter = filtro; //"Todas las Imagenes|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff";
           if (fimg.ShowDialog() == DialogResult.OK)
           {
               return fimg.FileName;
           }
           return "";
        }
       
        public static string ObtenerDireccionDeArchivo(string titulo, string filtro = "Todos los archivos|*.*")
        {
            var fimg = new OpenFileDialog();
            fimg.Title = titulo;
            fimg.Filter = filtro; //"Todas las Imagenes|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff";
            if (fimg.ShowDialog() == DialogResult.OK)
            {
                return fimg.FileName;
            }
            return "";
        }
        public static System.Drawing.Bitmap ImageFromContro(Control c)
        {
            Bitmap b = new Bitmap(c.Width, c.Height);
            c.DrawToBitmap(b, new Rectangle(0, 0, b.Width, b.Height));
            return b;
        }
        
    }
}
