﻿using gLogger;
using System;
using System.Collections.Concurrent;
using System.Text;
using System.Timers;

namespace gControls.Visores
{
    public partial class VisorMensajes : BaseControl
    {
        Timer _timer = new Timer();
        delegate void msgDelegate(gcMessage msg);
        
        public VisorMensajes()
        {
            InitializeComponent();
            logger.Singlenton.gcMessageAdded += Singlenton_gcMessageAdded;
            AvisoManager.Singlenton.SiguienteAviso += Singlenton_SiguienteAviso;
            gManager.CoreManager.Singlenton.ProgresoGlobal += Singlenton_ProgresoGlobal;
            _timer.Elapsed += (o, e) =>
            {
                _timer.Stop();
                gLogger.logger.Singlenton.addMessage(_nextAviso);
                cargarAvisos();
                gLogger.AvisoManager.Singlenton.nextAviso();
            };
        }

        void Singlenton_ProgresoGlobal(int percent, string msg)
        {
            progressBar1.Visible = true;
            if (percent < 100 && percent > 0)
            {
                progressBar1.Value = percent;
                if (!String.IsNullOrEmpty(msg))
                {
                    lblTitulo.Text = "Tarea";
                    lblMensaje.Text = msg;
                }
            }
            else
            {
                progressBar1.Visible = false;
                lblTitulo.Text = "Tarea";
                lblMensaje.Text = "se termino la tarea";
            }
        }
        private gcAviso _nextAviso = null;
        void Singlenton_SiguienteAviso(gcMessage msg)
        {
           _nextAviso = msg as gcAviso;
           _timer.Stop();
           if (_nextAviso != null && _nextAviso.Interval > 0)
            {
                _timer.Interval = _nextAviso.Interval;
                _timer.Start();
              
            }
        }
        
        void fu(gcMessage msg)
        {
            btAvisos.Text = logger.Singlenton.Mensajes.Count.ToString();
            btLimpiarMsgs.Show();
            if (msg.Tipo == gLogger.gcMessage.MessageType.CUSTOM && msg.CustomImage != null)
                pbImage.Image = msg.CustomImage;
            else
                pbImage.Image = imglist.Images[(int)msg.Tipo];
            lblMensaje.Text = msg.Mensaje;
            lblTitulo.Text = msg.Titulo;
            lblFecha.Text = msg.Fecha.ToLongTimeString();
            if (msg.Tipo == gLogger.gcMessage.MessageType.ERROR)
                pbImage.BackgroundImage = Properties.Resources.FondoRojo;
            else if (msg.Tipo == gLogger.gcMessage.MessageType.WARNING)
                pbImage.BackgroundImage = Properties.Resources.FondoAmarillo;
            else if (msg.Tipo == gLogger.gcMessage.MessageType.CUSTOM)
                pbImage.BackgroundImage = Properties.Resources.FondoVerde;
            else if (msg.Tipo == gLogger.gcMessage.MessageType.INFO)
                pbImage.BackgroundImage = Properties.Resources.FondoAzul;
            else
                pbImage.BackgroundImage = null;

            colorear();
        }
       
         void Singlenton_gcMessageAdded(gcMessage msg)
        {

            

            
            if (btAvisos.InvokeRequired)
            {
                msgDelegate t = x => fu(x);
                btAvisos.Invoke(t, msg);
            } else
            {

                btAvisos.Text = logger.Singlenton.Mensajes.Count.ToString();
            }
            /**/
            
            
        }
        public override void iniciar()
        {
            base.iniciar();
            cargarAvisos();
        }
        private void cargarAvisos()
        {
            int a = AvisoManager.Singlenton.Avisados.Count;
            if (a > 0)
            {
                btAvisos.Text = a.ToString();
                btAvisos.BackgroundImage = Properties.Resources.FondoAzul;
            }
        }

        private void btConfigMensages_Click(object sender, EventArgs e)
        {
            gLogger.logger.Singlenton.ShowConfig();
        }

        private void btAvisos_Click(object sender, EventArgs e)
        {
            /*
            StringBuilder t = new StringBuilder();
            if(dataItems.Count == 0)
            {
                gManager.CoreManager.Singlenton.GenerarInformeHtml("No hay mensajes");
                return;
            }
            t.Append("<table width=\"100 % \">");
            
            foreach (gcMessage m in dataItems)
            {
                string color = "blue";
                switch (m.Tipo)
                {
                    case gcMessage.MessageType.ERROR:
                        color = "#ec9c83";
                        break;
                    case gcMessage.MessageType.WARNING:
                        color = "#f4fda8";
                        break;
                    default:
                        color = "#a8aafd";
                        break;
                }
                t.Append("<tr >");
                t.Append("<td style=\"background-color: " + color + "; width=\"20 % \">");
                t.Append(m.Titulo);
                t.Append("</td>");
                t.Append("<td  style=\"background-color: " + color + ";\">");
                t.Append(m.Mensaje);
                t.Append("</td>");
                t.Append("<td style=\"background-color: " + color + "; width=\"20 % \">");
                t.Append(gManager.Utils.MostrarTiempo(DateTime.Now.Subtract(m.Fecha)));
                t.Append("</td>");
                t.Append("</tr>");
            }
            t.Append("</table>");
            */
            ControlesHtml.htmlObjects.htmlMessages t = new ControlesHtml.htmlObjects.htmlMessages(logger.Singlenton.Mensajes);
            gManager.CoreManager.Singlenton.GenerarInformeHtml(t.ToString());
            btAvisos.Text = logger.Singlenton.Mensajes.Count.ToString();
            ver.verAvisos();

        }

        private void colorear()
        {
            int count = logger.Singlenton.Mensajes.Count;
            if (count == 0)
            {
                btAvisos.BackgroundImage = Properties.Resources.FondoGris;

            } else if(count < 10)
            {
                btAvisos.BackgroundImage = Properties.Resources.FondoAzul;
            } else
            {
                btAvisos.BackgroundImage = Properties.Resources.FondoRojo;
            }
        }

        private void btLimpiarMsgs_Click(object sender, EventArgs e)
        {
            btAvisos.Text = "0";
            logger.Singlenton.clearMessages();
            btAvisos.Text = logger.Singlenton.Mensajes.Count.ToString();
            btLimpiarMsgs.Hide();
            colorear();
        }
    }
}
