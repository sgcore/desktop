﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorKey : UserControl
    {
        public VisorKey()
        {
            InitializeComponent();
        }
        public void UpdateTiempo()
        {
            if(gManager.CoreManager.Singlenton.Key.Habilitado)
            {
                var t=gManager.CoreManager.Singlenton.Key.TiempoRestante;
                lblDeshabilitado.Text =t.Days+" días, "+t.Hours+" horas, "+t.Minutes+" minutos";
                lblDeshabilitado.ForeColor = Color.Black;
            }
            else
            {
                lblDeshabilitado.Text = "Deshabilitado";
                lblDeshabilitado.ForeColor = Color.Maroon;
            }
            var k = gManager.CoreManager.Singlenton.Key;
            lblUsuario.Text = k.Usuario;
            lblNegocio.Text = k.Negocio;
            lblTerminal.Text = k.Terminal;
            
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            gManager.CoreManager.Singlenton.loginInternet(true);
            gManager.CoreManager.Singlenton.reiniciar();
            if (ParentForm != null)
            {
                ParentForm.DialogResult = DialogResult.Cancel;
            }

        }
    }
}
