﻿namespace gControls.Visores
{
    partial class VisorMonto
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Boton = new gControls.BotonAccion();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.RoyalBlue;
            this.lblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTitulo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitulo.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(73, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(147, 16);
            this.lblTitulo.TabIndex = 9;
            this.lblTitulo.Text = "TOTAL";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTotal.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTotal.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblTotal.Location = new System.Drawing.Point(73, 16);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(147, 20);
            this.lblTotal.TabIndex = 10;
            this.lblTotal.Text = "$0.000";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::gControls.Properties.Resources.Categoria;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(39, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // Boton
            // 
            this.Boton.BackColor = System.Drawing.Color.Transparent;
            this.Boton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Boton.Dock = System.Windows.Forms.DockStyle.Left;
            this.Boton.FormContainer = null;
            this.Boton.GlobalHotKey = false;
            this.Boton.HacerInvisibleAlDeshabilitar = false;
            this.Boton.HotKey = System.Windows.Forms.Shortcut.None;
            this.Boton.Location = new System.Drawing.Point(39, 0);
            this.Boton.Name = "Boton";
            this.Boton.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.Boton.RegistradoEnForm = false;
            this.Boton.Size = new System.Drawing.Size(34, 36);
            this.Boton.TabIndex = 11;
            this.Boton.Titulo = "";
            this.Boton.ToolTipDescripcion = "";
            this.Boton.UseVisualStyleBackColor = false;
            this.Boton.Visible = false;
            this.Boton.Click += new System.EventHandler(this.Boton_Click);
            // 
            // VisorMonto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.Boton);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.MinimumSize = new System.Drawing.Size(100, 32);
            this.Name = "VisorMonto";
            this.Size = new System.Drawing.Size(220, 36);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblTitulo;
        private BotonAccion Boton;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
