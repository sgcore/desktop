﻿namespace gControls.Visores
{
    partial class VisorMensajes
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            
            if (disposing && (components != null))
            {
               
                components.Dispose();
               
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorMensajes));
            this.pbImage = new System.Windows.Forms.PictureBox();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.imglist = new System.Windows.Forms.ImageList(this.components);
            this.lblFecha = new System.Windows.Forms.Label();
            this.btAvisos = new System.Windows.Forms.Button();
            this.btConfigMensages = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btLimpiarMsgs = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pbImage
            // 
            this.pbImage.BackColor = System.Drawing.SystemColors.Control;
            this.pbImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbImage.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbImage.Image = global::gControls.Properties.Resources.Mensaje;
            this.pbImage.Location = new System.Drawing.Point(0, 0);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(44, 32);
            this.pbImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbImage.TabIndex = 0;
            this.pbImage.TabStop = false;
            // 
            // lblMensaje
            // 
            this.lblMensaje.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMensaje.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMensaje.Location = new System.Drawing.Point(206, 0);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(359, 32);
            this.lblMensaje.TabIndex = 1;
            this.lblMensaje.Text = "Mensajes del sistema";
            this.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTitulo.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitulo.Location = new System.Drawing.Point(44, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(162, 32);
            this.lblTitulo.TabIndex = 2;
            this.lblTitulo.Text = "SYSTEM";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imglist
            // 
            this.imglist.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imglist.ImageStream")));
            this.imglist.TransparentColor = System.Drawing.Color.Transparent;
            this.imglist.Images.SetKeyName(0, "Error");
            this.imglist.Images.SetKeyName(1, "Warning");
            this.imglist.Images.SetKeyName(2, "Info");
            this.imglist.Images.SetKeyName(3, "Sistema");
            this.imglist.Images.SetKeyName(4, "Debug");
            this.imglist.Images.SetKeyName(5, "Seguridad");
            this.imglist.Images.SetKeyName(6, "Mensaje");
            // 
            // lblFecha
            // 
            this.lblFecha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFecha.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblFecha.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Location = new System.Drawing.Point(565, 0);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(121, 32);
            this.lblFecha.TabIndex = 3;
            this.lblFecha.Text = "Tiempo de emision";
            this.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btAvisos
            // 
            this.btAvisos.AutoSize = true;
            this.btAvisos.Dock = System.Windows.Forms.DockStyle.Right;
            this.btAvisos.Image = global::gControls.Properties.Resources.Aviso16;
            this.btAvisos.Location = new System.Drawing.Point(726, 0);
            this.btAvisos.Name = "btAvisos";
            this.btAvisos.Size = new System.Drawing.Size(45, 32);
            this.btAvisos.TabIndex = 5;
            this.btAvisos.Text = "0";
            this.btAvisos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAvisos.UseVisualStyleBackColor = true;
            this.btAvisos.Click += new System.EventHandler(this.btAvisos_Click);
            // 
            // btConfigMensages
            // 
            this.btConfigMensages.AutoSize = true;
            this.btConfigMensages.Dock = System.Windows.Forms.DockStyle.Right;
            this.btConfigMensages.Image = global::gControls.Properties.Resources.Mensaje;
            this.btConfigMensages.Location = new System.Drawing.Point(686, 0);
            this.btConfigMensages.Name = "btConfigMensages";
            this.btConfigMensages.Size = new System.Drawing.Size(40, 32);
            this.btConfigMensages.TabIndex = 4;
            this.btConfigMensages.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btConfigMensages.UseVisualStyleBackColor = true;
            this.btConfigMensages.Click += new System.EventHandler(this.btConfigMensages_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.progressBar1.Location = new System.Drawing.Point(465, 0);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 32);
            this.progressBar1.TabIndex = 6;
            this.progressBar1.Value = 3;
            this.progressBar1.Visible = false;
            // 
            // btLimpiarMsgs
            // 
            this.btLimpiarMsgs.AutoSize = true;
            this.btLimpiarMsgs.Dock = System.Windows.Forms.DockStyle.Right;
            this.btLimpiarMsgs.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btLimpiarMsgs.Location = new System.Drawing.Point(771, 0);
            this.btLimpiarMsgs.Name = "btLimpiarMsgs";
            this.btLimpiarMsgs.Size = new System.Drawing.Size(27, 32);
            this.btLimpiarMsgs.TabIndex = 7;
            this.btLimpiarMsgs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btLimpiarMsgs.UseVisualStyleBackColor = true;
            this.btLimpiarMsgs.Click += new System.EventHandler(this.btLimpiarMsgs_Click);
            // 
            // VisorMensajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.pbImage);
            this.Controls.Add(this.btConfigMensages);
            this.Controls.Add(this.btAvisos);
            this.Controls.Add(this.btLimpiarMsgs);
            this.Name = "VisorMensajes";
            this.Size = new System.Drawing.Size(798, 32);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbImage;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.ImageList imglist;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Button btAvisos;
        private System.Windows.Forms.Button btConfigMensages;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btLimpiarMsgs;
    }
}
