﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public class VisorDestinatario :UserControl
    {
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblDireccion;
        private System.Windows.Forms.Label lblContacto;
        private System.Windows.Forms.Label lblTipoIva;
        private System.Windows.Forms.Label lblDescripcion;
        private System.Windows.Forms.Label lblCuentaCorriente;
        private Panel PanelBotones;
        private BotonAccion btPeriodico;
        private BotonAccion btTratamientos;
        private BotonAccion btAvisos;
        private BotonAccion btTareas;
        private BotonAccion btVer;
        private BotonAccion btRecordatorios;
        private gManager.gcDestinatario _dest;
        public VisorDestinatario()
        {
            InitializeComponent();
        }
        public gManager.gcDestinatario Destinatario
        {
            get
            {
                return _dest;
            }
            set
            {
                _dest = value;
               
                if (_dest != null)
                {
                    tratamientos();
                    if(_dest.Tipo==gManager.gcDestinatario.DestinatarioTipo.Paciente)
                    {
                        lblNombre.Text = _dest.Nombre;
                        lblDireccion.Text ="de "+ _dest.PropietarioNombre;
                        lblContacto.Text = _dest.Telefono + " - " + _dest.Celular + " - " + _dest.Mail;
                        lblCuentaCorriente.Text = "";
                        lblDescripcion.Text = _dest.LastMod.ToString();
                        lblTipoIva.Text = _dest.Especie.ToString();
                        return;
                    }
                    lblNombre.Text = _dest.Nombre;
                    lblDireccion.Text = _dest.Direccion;
                    lblContacto.Text = _dest.Telefono + " - " + _dest.Celular + " - " + _dest.Mail;
                    setCuentaCorriente(_dest.SaldoEnCuenta);
                    lblDescripcion.Text = _dest.LastMod.ToString();
                    lblTipoIva.Text =  _dest.TipoIva.ToString().Replace("_", " ");
                    
                }
                else
                {
                    lblNombre.Text = "Desconocido";
                    lblDireccion.Text = "--";
                    lblContacto.Text ="--";
                    lblCuentaCorriente.Text = "Cuenta Corriente $0.00";
                    lblDescripcion.Text = "no hay ningun destinatario";
                }
            }
        }
        private void tratamientos()
        {
            btTratamientos.Visible = _dest.Tipo == gManager.gcDestinatario.DestinatarioTipo.Paciente;
            if (_dest.Avisos.Count > 0)
            {
                btAvisos.BackgroundImage = Properties.Resources.FondoRojo;
                btAvisos.Text = _dest.Avisos.Count.ToString();
                btAvisos.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                btAvisos.BackgroundImage = Properties.Resources.FondoGris;
                btAvisos.Text = "0";
                btAvisos.ForeColor = System.Drawing.Color.Silver;
            }
            if (_dest.Tareas.Count > 0)
            {
                btTareas.BackgroundImage = Properties.Resources.FondoAmarillo;
                btTareas.Text = _dest.Tareas.Count.ToString();
                btTareas.ForeColor = System.Drawing.Color.Yellow;
            }
            else
            {
                btTareas.BackgroundImage = Properties.Resources.FondoGris;
                btTareas.Text = "0";
                btTareas.ForeColor = System.Drawing.Color.Silver;
            }
            if (_dest.Tratamientos.Count > 0)
            {
                btTratamientos.BackgroundImage = Properties.Resources.FondoAzul;
                btTratamientos.Text = _dest.Tratamientos.Count.ToString();
                btTratamientos.ForeColor = System.Drawing.Color.Blue;
            }
            else
            {
                btTratamientos.BackgroundImage = Properties.Resources.FondoGris;
                btTratamientos.Text = "0";
                btTratamientos.ForeColor = System.Drawing.Color.Silver;
            }
            if (_dest.Periodicos.Count > 0)
            {
                btPeriodico.BackgroundImage = Properties.Resources.FondoAzul;
                btPeriodico.Text = _dest.Periodicos.Count.ToString();
                btPeriodico.ForeColor = System.Drawing.Color.Blue;
            }
            else
            {
                btPeriodico.BackgroundImage = Properties.Resources.FondoGris;
                btPeriodico.Text = "0";
                btPeriodico.ForeColor = System.Drawing.Color.Silver;
            }
            if (_dest.Recordatorios.Count > 0)
            {
                btRecordatorios.BackgroundImage = Properties.Resources.FondoAzul;
                btRecordatorios.Text = _dest.Recordatorios.Count.ToString();
                btRecordatorios.ForeColor = System.Drawing.Color.Blue;
            }
            else
            {
                btRecordatorios.BackgroundImage = Properties.Resources.FondoGris;
                btRecordatorios.Text = "0";
                btRecordatorios.ForeColor = System.Drawing.Color.Silver;
            }
        }
        private void setCuentaCorriente(decimal monto)
        {
            if (_dest != null)
            {
                if (_dest.Tipo == gManager.gcDestinatario.DestinatarioTipo.Proveedor)
                {
                    if (monto > 0)
                    {
                        lblCuentaCorriente.Text = "Credito en Proveedor: " + monto.ToString("$0.00");
                        lblCuentaCorriente.ForeColor = System.Drawing.Color.DarkSlateBlue;
                        lblCuentaCorriente.Visible = true;
                    }
                    else if (_dest.SaldoEnCuenta < 0)
                    {
                        lblCuentaCorriente.Text = "Deuda con Proveedor: " + (-monto).ToString("$0.00");
                        lblCuentaCorriente.ForeColor = System.Drawing.Color.LightCoral;
                        lblCuentaCorriente.Visible = true;
                    }
                    else
                    {
                        lblCuentaCorriente.Visible = false;
                    }
                }
                else if (_dest.Tipo == gManager.gcDestinatario.DestinatarioTipo.Cliente)
                {
                    if (monto > 0)
                    {
                        lblCuentaCorriente.Text = "Deuda de Cliente: " + monto.ToString("$0.00");
                        lblCuentaCorriente.ForeColor = System.Drawing.Color.LightCoral;
                        lblCuentaCorriente.Visible = true;
                    }
                    else if (_dest.SaldoEnCuenta < 0)
                    {
                        lblCuentaCorriente.Text = "Credito de Cliente: " + (-monto).ToString("$0.00");
                        lblCuentaCorriente.ForeColor = System.Drawing.Color.DarkSlateBlue;
                        lblCuentaCorriente.Visible = true;
                    }
                    else
                    {
                        lblCuentaCorriente.Visible = false;
                    }
                }
                else if (_dest.Tipo == gManager.gcDestinatario.DestinatarioTipo.Responsable)
                {
                    if (monto > 0)
                    {
                        lblCuentaCorriente.Text = "Recaudación " + monto.ToString("$0.00");
                        lblCuentaCorriente.ForeColor = System.Drawing.Color.DarkViolet;
                        lblCuentaCorriente.Visible = true;
                    }
                    else if (_dest.SaldoEnCuenta < 0)
                    {
                        lblCuentaCorriente.Text = "Aporte: " + (-monto).ToString("$0.00");
                        lblCuentaCorriente.ForeColor = System.Drawing.Color.DarkSeaGreen;
                        lblCuentaCorriente.Visible = true;
                    }
                    else
                    {
                        lblCuentaCorriente.Visible = false;
                    }
                }
                else
                {
                    lblCuentaCorriente.Visible = false;
                }
            }
            //negativo es credito
            
        }
        private void InitializeComponent()
        {
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.lblContacto = new System.Windows.Forms.Label();
            this.lblCuentaCorriente = new System.Windows.Forms.Label();
            this.lblTipoIva = new System.Windows.Forms.Label();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.PanelBotones = new System.Windows.Forms.Panel();
            this.btRecordatorios = new gControls.BotonAccion();
            this.btPeriodico = new gControls.BotonAccion();
            this.btTratamientos = new gControls.BotonAccion();
            this.btAvisos = new gControls.BotonAccion();
            this.btTareas = new gControls.BotonAccion();
            this.btVer = new gControls.BotonAccion();
            this.PanelBotones.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNombre.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(104, 0);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(311, 22);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Juan de los Palotes";
            this.lblNombre.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDireccion
            // 
            this.lblDireccion.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDireccion.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDireccion.Location = new System.Drawing.Point(104, 22);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(311, 21);
            this.lblDireccion.TabIndex = 1;
            this.lblDireccion.Text = "San Martín 253 - Santiago del Estero - 4200 - Argentina";
            this.lblDireccion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblContacto
            // 
            this.lblContacto.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblContacto.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContacto.Location = new System.Drawing.Point(104, 43);
            this.lblContacto.Name = "lblContacto";
            this.lblContacto.Size = new System.Drawing.Size(311, 21);
            this.lblContacto.TabIndex = 2;
            this.lblContacto.Text = "4219564 -156977856 -tuertilindo@hotmail.com";
            this.lblContacto.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblCuentaCorriente
            // 
            this.lblCuentaCorriente.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCuentaCorriente.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuentaCorriente.Location = new System.Drawing.Point(415, 0);
            this.lblCuentaCorriente.Name = "lblCuentaCorriente";
            this.lblCuentaCorriente.Size = new System.Drawing.Size(84, 102);
            this.lblCuentaCorriente.TabIndex = 4;
            this.lblCuentaCorriente.Text = "Cuenta Corriente $150560.22";
            this.lblCuentaCorriente.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTipoIva
            // 
            this.lblTipoIva.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTipoIva.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoIva.Location = new System.Drawing.Point(0, 0);
            this.lblTipoIva.Name = "lblTipoIva";
            this.lblTipoIva.Size = new System.Drawing.Size(104, 102);
            this.lblTipoIva.TabIndex = 5;
            this.lblTipoIva.Text = "Responsable Inscripto";
            this.lblTipoIva.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDescripcion.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion.Location = new System.Drawing.Point(104, 64);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(311, 18);
            this.lblDescripcion.TabIndex = 6;
            this.lblDescripcion.Text = "Descripción del destinatario";
            this.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PanelBotones
            // 
            this.PanelBotones.Controls.Add(this.btRecordatorios);
            this.PanelBotones.Controls.Add(this.btPeriodico);
            this.PanelBotones.Controls.Add(this.btTratamientos);
            this.PanelBotones.Controls.Add(this.btAvisos);
            this.PanelBotones.Controls.Add(this.btTareas);
            this.PanelBotones.Controls.Add(this.btVer);
            this.PanelBotones.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelBotones.Location = new System.Drawing.Point(104, 79);
            this.PanelBotones.Name = "PanelBotones";
            this.PanelBotones.Size = new System.Drawing.Size(311, 23);
            this.PanelBotones.TabIndex = 10;
            // 
            // btRecordatorios
            // 
            this.btRecordatorios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btRecordatorios.Checked = false;
            this.btRecordatorios.Dock = System.Windows.Forms.DockStyle.Right;
            this.btRecordatorios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRecordatorios.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRecordatorios.ForeColor = System.Drawing.Color.Silver;
            this.btRecordatorios.FormContainer = null;
            this.btRecordatorios.GlobalHotKey = false;
            this.btRecordatorios.HacerInvisibleAlDeshabilitar = false;
            this.btRecordatorios.HotKey = System.Windows.Forms.Shortcut.None;
            this.btRecordatorios.Image = global::gControls.Properties.Resources.Recordatorio16;
            this.btRecordatorios.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btRecordatorios.Location = new System.Drawing.Point(61, 0);
            this.btRecordatorios.Name = "btRecordatorios";
            this.btRecordatorios.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btRecordatorios.RegistradoEnForm = false;
            this.btRecordatorios.RequiereKey = true;
            this.btRecordatorios.Size = new System.Drawing.Size(50, 23);
            this.btRecordatorios.TabIndex = 12;
            this.btRecordatorios.Text = "0";
            this.btRecordatorios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btRecordatorios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btRecordatorios.Titulo = "Recordatorios";
            this.btRecordatorios.ToolTipDescripcion = "Los recordatorios envian un aviso al cliente a traves de un email.";
            this.btRecordatorios.UseVisualStyleBackColor = true;
            this.btRecordatorios.Click += new System.EventHandler(this.btRecordatorios_Click);
            // 
            // btPeriodico
            // 
            this.btPeriodico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPeriodico.Checked = false;
            this.btPeriodico.Dock = System.Windows.Forms.DockStyle.Right;
            this.btPeriodico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPeriodico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPeriodico.ForeColor = System.Drawing.Color.Silver;
            this.btPeriodico.FormContainer = null;
            this.btPeriodico.GlobalHotKey = false;
            this.btPeriodico.HacerInvisibleAlDeshabilitar = false;
            this.btPeriodico.HotKey = System.Windows.Forms.Shortcut.None;
            this.btPeriodico.Image = global::gControls.Properties.Resources.Periodico16;
            this.btPeriodico.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btPeriodico.Location = new System.Drawing.Point(111, 0);
            this.btPeriodico.Name = "btPeriodico";
            this.btPeriodico.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btPeriodico.RegistradoEnForm = false;
            this.btPeriodico.RequiereKey = true;
            this.btPeriodico.Size = new System.Drawing.Size(50, 23);
            this.btPeriodico.TabIndex = 11;
            this.btPeriodico.Text = "0";
            this.btPeriodico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPeriodico.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPeriodico.Titulo = "Eventos periodicos";
            this.btPeriodico.ToolTipDescripcion = "Genera aviso cada cierto intervalo.";
            this.btPeriodico.UseVisualStyleBackColor = true;
            this.btPeriodico.Click += new System.EventHandler(this.btPeriodico_Click);
            // 
            // btTratamientos
            // 
            this.btTratamientos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btTratamientos.Checked = false;
            this.btTratamientos.Dock = System.Windows.Forms.DockStyle.Right;
            this.btTratamientos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btTratamientos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTratamientos.ForeColor = System.Drawing.Color.Silver;
            this.btTratamientos.FormContainer = null;
            this.btTratamientos.GlobalHotKey = false;
            this.btTratamientos.HacerInvisibleAlDeshabilitar = false;
            this.btTratamientos.HotKey = System.Windows.Forms.Shortcut.None;
            this.btTratamientos.Image = global::gControls.Properties.Resources.Tratamiento16;
            this.btTratamientos.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btTratamientos.Location = new System.Drawing.Point(161, 0);
            this.btTratamientos.Name = "btTratamientos";
            this.btTratamientos.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btTratamientos.RegistradoEnForm = false;
            this.btTratamientos.RequiereKey = true;
            this.btTratamientos.Size = new System.Drawing.Size(50, 23);
            this.btTratamientos.TabIndex = 10;
            this.btTratamientos.Text = "0";
            this.btTratamientos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btTratamientos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btTratamientos.Titulo = "Tratamientos";
            this.btTratamientos.ToolTipDescripcion = "Tratamientos realizados a este paciente.";
            this.btTratamientos.UseVisualStyleBackColor = true;
            this.btTratamientos.Click += new System.EventHandler(this.btTratamientos_Click);
            // 
            // btAvisos
            // 
            this.btAvisos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btAvisos.Checked = false;
            this.btAvisos.Dock = System.Windows.Forms.DockStyle.Right;
            this.btAvisos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAvisos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAvisos.ForeColor = System.Drawing.Color.Silver;
            this.btAvisos.FormContainer = null;
            this.btAvisos.GlobalHotKey = false;
            this.btAvisos.HacerInvisibleAlDeshabilitar = false;
            this.btAvisos.HotKey = System.Windows.Forms.Shortcut.None;
            this.btAvisos.Image = global::gControls.Properties.Resources.Aviso16;
            this.btAvisos.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btAvisos.Location = new System.Drawing.Point(211, 0);
            this.btAvisos.Name = "btAvisos";
            this.btAvisos.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btAvisos.RegistradoEnForm = false;
            this.btAvisos.RequiereKey = true;
            this.btAvisos.Size = new System.Drawing.Size(50, 23);
            this.btAvisos.TabIndex = 9;
            this.btAvisos.Text = "0";
            this.btAvisos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAvisos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAvisos.Titulo = "Avisos";
            this.btAvisos.ToolTipDescripcion = "Avisos para los usuarios del sistema.";
            this.btAvisos.UseVisualStyleBackColor = true;
            this.btAvisos.Click += new System.EventHandler(this.btAvisos_Click);
            // 
            // btTareas
            // 
            this.btTareas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btTareas.Checked = false;
            this.btTareas.Dock = System.Windows.Forms.DockStyle.Right;
            this.btTareas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btTareas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTareas.ForeColor = System.Drawing.Color.Silver;
            this.btTareas.FormContainer = null;
            this.btTareas.GlobalHotKey = false;
            this.btTareas.HacerInvisibleAlDeshabilitar = false;
            this.btTareas.HotKey = System.Windows.Forms.Shortcut.None;
            this.btTareas.Image = global::gControls.Properties.Resources.Tarea16;
            this.btTareas.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btTareas.Location = new System.Drawing.Point(261, 0);
            this.btTareas.Name = "btTareas";
            this.btTareas.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btTareas.RegistradoEnForm = false;
            this.btTareas.RequiereKey = true;
            this.btTareas.Size = new System.Drawing.Size(50, 23);
            this.btTareas.TabIndex = 8;
            this.btTareas.Text = "0";
            this.btTareas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btTareas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btTareas.Titulo = "Tareas pendientes";
            this.btTareas.ToolTipDescripcion = "Realiza tareas en el tiempo indicado";
            this.btTareas.UseVisualStyleBackColor = true;
            this.btTareas.Click += new System.EventHandler(this.btTareas_Click);
            // 
            // btVer
            // 
            this.btVer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btVer.Checked = false;
            this.btVer.Dock = System.Windows.Forms.DockStyle.Left;
            this.btVer.FormContainer = null;
            this.btVer.GlobalHotKey = false;
            this.btVer.HacerInvisibleAlDeshabilitar = false;
            this.btVer.HotKey = System.Windows.Forms.Shortcut.None;
            this.btVer.Image = global::gControls.Properties.Resources.Ver;
            this.btVer.Location = new System.Drawing.Point(0, 0);
            this.btVer.Name = "btVer";
            this.btVer.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btVer.RegistradoEnForm = false;
            this.btVer.RequiereKey = true;
            this.btVer.Size = new System.Drawing.Size(39, 23);
            this.btVer.TabIndex = 7;
            this.btVer.Titulo = "Ver seguimiento";
            this.btVer.ToolTipDescripcion = "Muestra un informe de seguimiento para este destinatario.";
            this.btVer.UseVisualStyleBackColor = true;
            this.btVer.Click += new System.EventHandler(this.btVer_Click_1);
            // 
            // VisorDestinatario
            // 
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.PanelBotones);
            this.Controls.Add(this.lblDescripcion);
            this.Controls.Add(this.lblContacto);
            this.Controls.Add(this.lblDireccion);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblCuentaCorriente);
            this.Controls.Add(this.lblTipoIva);
            this.DoubleBuffered = true;
            this.Name = "VisorDestinatario";
            this.Size = new System.Drawing.Size(499, 102);
            this.PanelBotones.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        public bool VerBotonSeguimiento
        {
            get
            {
                return PanelBotones.Visible;
            }
            set
            {
                PanelBotones.Visible = value;
            }
        }

      

        private void btTareas_Click(object sender, EventArgs e)
        {
            buscar.BuscarTratamientos(Destinatario, gManager.gcTratamiento.TipoTratamiento.Tarea);
        }

        private void btAvisos_Click(object sender, EventArgs e)
        {
            buscar.BuscarTratamientos(Destinatario, gManager.gcTratamiento.TipoTratamiento.Aviso);
        }

        private void btTratamientos_Click(object sender, EventArgs e)
        {
            buscar.BuscarTratamientos(Destinatario, gManager.gcTratamiento.TipoTratamiento.Tratamiento);
        }

        private void btPeriodico_Click(object sender, EventArgs e)
        {
            buscar.BuscarTratamientos(Destinatario, gManager.gcTratamiento.TipoTratamiento.Periodico);
        }

        private void btVer_Click_1(object sender, EventArgs e)
        {
            ver.VerDestinatarios(Destinatario);
        }

        private void btRecordatorios_Click(object sender, EventArgs e)
        {
            buscar.BuscarTratamientos(Destinatario, gManager.gcTratamiento.TipoTratamiento.Recordatorio);
        }
    }
}
