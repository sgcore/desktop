﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorTotalesMovimiento : UserControl
    {
        public VisorTotalesMovimiento()
        {
          
            InitializeComponent();
            
        }
        public void setMovimiento(gManager.gcMovimiento m)
        {
            this.SuspendLayout();
            visorCotizacion1.setMovimiento(m);
            panelAccionesMovimiento1.setMovimiento(m);
           
            visorSaldoMovimiento1.setMovimiento(m);
            //setVerIva(false);
            //setVerPagos(false);
            //setVerSaldo(false);
            //setVerCotizacion(false);
            this.Visible = m != null;
            PanelDIscriminaciones.Controls.Clear();
            if (m != null)
            {
                setVerPagos(m.EsConDineroInvolucrado);
                setVerCotizacion(m.Cotizacion != 1);
                setVerSaldo(m.TotalPagado>0 || m.TotalMovimiento > 0);
                setVerIva(m.TipoFactura == gManager.gcMovimiento.TipoFacturaEnum.A);
                foreach (var d in m.Discriminaciones)
                {
                    var vi = new VisorIva();
                    vi.setMontos(d);
                    vi.Dock = DockStyle.Top;
                    PanelDIscriminaciones.Controls.Add(vi);
                }
            }
            this.ResumeLayout();
            
            
        }
        public void setVerIva(bool ver)
        {
            ver = ver && IvaVisible;
            PanelDIscriminaciones.Visible = ver;
          

        }
        bool _IvaVisible = true;
        bool _SaldoVisible = true;
        bool _PagosVisible = true;
        bool _CotizacionVisible = true;
        public bool IvaVisible { get { return _IvaVisible; } set { _IvaVisible = value; setVerIva(value); } }
        public bool SaldoVisible { get { return _SaldoVisible; } set { _SaldoVisible = value; setVerSaldo(value); } }
        public bool PagosVisible { get { return _PagosVisible; } set { _PagosVisible = value; setVerPagos(value); } }
        public bool CotizacionVisible { get { return _CotizacionVisible; } set { _CotizacionVisible = value; setVerCotizacion(value); } }
        public void setVerSaldo(bool ver)
        {
            visorSaldoMovimiento1.Visible = ver && SaldoVisible;
            
        }
        
        public void setVerPagos(bool ver)
        {
            ver = ver && PagosVisible;
           
            
        }
        public void setVerCotizacion(bool ver)
        {
            ver = ver && CotizacionVisible;
            visorCotizacion1.Visible = ver;
           
        }
       
    }
}
