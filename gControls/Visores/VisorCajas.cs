﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Visores
{
    public partial class VisorCajas : BaseControl
    {
        
        
        public VisorCajas()
        {
            InitializeComponent();
            gManager.CoreManager.Singlenton.CajaSeleccionada += Singlenton_CajaSeleccionada;
            gManager.CoreManager.Singlenton.PagoAgregado += Singlenton_PagoAgregado;
            gManager.CoreManager.Singlenton.PagoEliminado += Singlenton_PagoAgregado;
            buscadorMovimientos1.CambioFiltro += buscadorMovimientos1_CambioFiltro;
          
        
            //buscadorMovimientos1.generarBoton(Properties.Resources.Filtro,"Filtrar Cajas","Ingrese las cajas que desea mostrar.",(o,e)=>{setCajas(Editar.EditarFiltro(new ))})
            buscadorMovimientos1.iniciar();
            visorEfectivo1.Contraer();
        }

        void buscadorMovimientos1_CambioFiltro(object sender, EventArgs e)
        {
            if (sender is gManager.Filtros.FiltroCaja)
            {
                setCajas((sender as gManager.Filtros.FiltroCaja).ListaDeCajasCustom);
            }
        }
         void Singlenton_PagoAgregado(gManager.gcPago p)
        {
            if (AutoActualizarse)
            {
                setCaja(gManager.CoreManager.Singlenton.CajaActual);
            }
        }
         public bool verFiltro
         {
             get
             {
                 return buscadorMovimientos1.VerBotonFiltro;
             }
             set
             {
                 buscadorMovimientos1.VerBotonFiltro = value;
             }
         }

        void Singlenton_CajaSeleccionada(gManager.gcCaja c)
        {
            if (AutoActualizarse)
            {
                setCaja(gManager.CoreManager.Singlenton.CajaActual);
            }
        }
        public override void iniciar()
        {
            base.iniciar();
            if (AutoActualizarse)
            {
                setCaja(gManager.CoreManager.Singlenton.CajaActual);
            }
        }
        public void setCaja(gManager.gcCaja c)
        {
            setCajas(new List<gManager.gcCaja>{c});
        }
        public void setCajas(List<gManager.gcCaja> c)
        {
            var lista = new gManager.ResumenCajas(c);
            //visorEfectivo1.setPagos(c);
            visorEfectivo1.setCajas(lista);
            var f=new gManager.Filtros.FiltroMovimiento();
           // buscadorMovimientos1VerBotonFiltro = true;
            buscadorMovimientos1.setCajas(c);
            if (c.Count > 1)
            {
                buscadorMovimientos1.Titulo = "Entre Cajas " + c[0].Caja + " - " + c[c.Count - 1].Caja;
            }
            else if (c.Count == 1)
                buscadorMovimientos1.Titulo = "Caja " + c[0].Caja + " - " + c[0].DescripcionFechaInicio + " - " + c[0].DescripcionFechaCierre;
            else
                buscadorMovimientos1.Titulo = "Movimientos";
            
        }
        public bool AutoActualizarse { get; set; }


        
    }
}
