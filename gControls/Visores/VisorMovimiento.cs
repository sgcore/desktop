﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorMovimiento : UserControl
    {
        private gManager.gcMovimiento _mov;
        public VisorMovimiento()
        {
            InitializeComponent();
        }
        public void setMovimiento(gManager.gcMovimiento m)
        {
            _mov = m;
            this.Visible = m != null;
            if (m != null)
            {
                lblEstado.ImageKey = m.Estado.ToString();
                lblfecha.Text = m.Fecha.ToLongDateString();
                lblTipo.Text =m.Numero.ToString("00000")+" - "+ m.Tipo.ToString();
                lblTipo.ImageKey = m.Tipo.ToString();
                lblTipoFactura.Text = m.TipoFactura.ToString();
                lblObservaciones.Text = m.Observaciones;
                //switch (m.Tipo)
                //{
                //    case gManager.gcMovimiento.TipoMovEnum.Compra:
                //        lblEstado. BackgroundImage = Properties.Resources.FondoAmarillo;
                //        break;
                //    case gManager.gcMovimiento.TipoMovEnum.Venta:
                //        lblEstado.BackgroundImage = Properties.Resources.FondoVerde;
                //        break;
                //    case gManager.gcMovimiento.TipoMovEnum.Devolucion:
                //    case gManager.gcMovimiento.TipoMovEnum.Extraccion:
                //    case gManager.gcMovimiento.TipoMovEnum.Retorno:
                //        lblEstado.BackgroundImage = Properties.Resources.FondoRojo;
                //        break;
                //    default:
                //        lblEstado.BackgroundImage = Properties.Resources.FondoAzul;
                //        break;


                //}
                lblEstado.BackgroundImageLayout = ImageLayout.Stretch;
                switch (m.Estado)
                {
                    case gManager.gcMovimiento.EstadoEnum.Creado:
                        lblEstado.BackgroundImage = Properties.Resources.FondoRojo;
                        lblEstado.Text = "Creado por " + m.DescripcionUsuario;
                        break;
                    case gManager.gcMovimiento.EstadoEnum.Modificado:
                        lblEstado.BackgroundImage = Properties.Resources.FondoAmarillo;
                        lblEstado.Text = "Modificado por " + m.DescripcionUsuario;
                        break;
                    case gManager.gcMovimiento.EstadoEnum.Terminado:
                        lblEstado.BackgroundImage = Properties.Resources.FondoVerde;
                        lblEstado.Text = "terminado por " + m.DescripcionUsuario;
                        break;
                }
            }
            else
            {
                lblEstado.Text = "Estado";
                lblfecha.Text = "Fecha de emisión";
                lblTipo.Text = "Tipo";
                lblObservaciones.Text = "Observaciones";
               
            }
        }

        private void btObs_Click(object sender, EventArgs e)
        {
            if (_mov != null && !_mov.Terminado)
            {
                _mov.Observaciones = Editar.EditarTexto("Editar observaciones", _mov.Observaciones);
                _mov.saveMe();
                gManager.CoreManager.Singlenton.seleccionarMovimiento(_mov);
            }
        }

        private void btCerrar_Click(object sender, EventArgs e)
        {
             if (gManager.CoreManager.Singlenton.Usuario.EsAdmin)
            {
                var c=buscar.BuscarCaja();
                if (c == null) return;
                if (c.Caja != _mov.Caja)
                {
                    _mov.Caja = c.Caja;
                    _mov.Fecha = c.FechaDeInicio;
                    _mov.saveMe();
                    gLogger.logger.Singlenton.addWarningMsg("Se reubicó el movimiento " + _mov.DescripcionAuto + ". Recomendamos reiniciar el sistema.");
                }
            }
        }
    }
}
