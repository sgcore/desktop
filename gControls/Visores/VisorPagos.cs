﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorPagos : UserControl
    {
        public VisorPagos()
        {
            InitializeComponent();
        }
        public void setMovimiento(gManager.gcMovimiento m)
        {
            if (m != null)
            {
                //aqui incluye los vueltos
                decimal efe = m.EfectivoPagado.Monto;
                vEfectivo.setMonto(efe);
               
                decimal tar = m.PagosEnTarjeta.Monto;
                vTarjeta.setMonto(tar);
               

                decimal cuen = m.TotalAcreditado;
                vCuenta.setMonto(cuen);
                

                decimal che = m.PagosEnCheques.Monto;
                vCheque.setMonto(che);
               
            }
            else
            {
                vEfectivo.setMonto(0);
                vTarjeta.setMonto(0);
                vCuenta.setMonto(0);
                vCheque.setMonto(0);

            }
        }
        public void setCaja(gManager.gcCaja c)
        {
            if (c != null)
            {
                decimal efe = c.Efectivo.Monto;
                vEfectivo.setMonto(efe);
                vEfectivo.Enabled = efe > 0;

                decimal tar =c.Tarjeta.Monto;
                vTarjeta.setMonto(tar);
                vTarjeta.Enabled = tar > 0;

                decimal cuen = c.Cuenta.Monto;
                vCuenta.setMonto(cuen);
                vCuenta.Enabled = cuen != 0;

                decimal che = c.Cheque.Monto;
                vCheque.setMonto(che);
                vCheque.Enabled = che > 0;
            }
            else
            {
                vEfectivo.setMonto(0);
                vTarjeta.setMonto(0);
                vCuenta.setMonto(0);
                vCheque.setMonto(0);

            }
        }

        bool _alineacion=true;
        public bool Vertical
        {
            get
            {
                return _alineacion;
            }
            set
            {
                _alineacion = value;
                if (_alineacion)
                {
                    this.Size = new Size(104, 193);
                    vEfectivo.Location = new Point(1,0);
                    vCuenta.Location = new Point(1, 49);
                    vTarjeta.Location = new Point(1, 98);
                    vCheque.Location = new Point(1, 147);
                }
                else
                {
                    this.Size = new Size(427, 48);
                    vEfectivo.Location = new Point(6, 0);
                    vCuenta.Location = new Point(106, 0);
                    vTarjeta.Location = new Point(212,0);
                    vCheque.Location = new Point(318, 0);
                }
            }
        }
    }
}
