﻿using gManager;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorEfectivo : UserControl
    {
        gcCaja _c;
        List<gcCaja> _cajas;
        ResumenCajas _resumen;
        public VisorEfectivo()
        {
            InitializeComponent();
            vVentas.ClickDelegate = (o, e) =>
            {
                if ((VisorEfectivo.ModifierKeys & Keys.Shift) != 0)
                {
                    ver.VerResumenVentasHtml(_cajas);
                } else
                    ver.VerResumenVentas(_cajas); 
            };
           
        }
        public void setPago(gcCaja c)
        {
            _c = c;
            if (c != null)
            {
                 var p=c.Efectivo;
                vEfectivo.setMonto(p);
                vGastos.setMonto(c.EfectivoEnExtracciones);
                vCompras.setMonto(c.EfectivoGastadoEnCompras);
                vDepositos.setMonto(c.EfectivoObtenidoEnDepositos);
                vVentas.setMonto (c.EfectivoObtenidoEnVentas);
                vInicial.setMonto(c.DepositoInicial);
                vCierre.setMonto(c.ExtraccionFinal);
                vDescuentos.setMonto(c.EfectivoPerdidoEnDescuentos);
                _cajas = new List<gcCaja>() { c };
               
            }else{
                vEfectivo.setMonto(0);
                vGastos.setMonto(0);
                vCompras.setMonto(0);
                vDepositos.setMonto(0);
                vVentas.setMonto(0);
                vDescuentos.setMonto(0);
            }

           
           
        }
        public void setPagos(List<gcCaja> l)
        {
            vEfectivo.setMonto(0);
            vDepositos.setMonto(0);
            vVentas.setMonto(0);
            vInicial.setMonto(0);
            vCierre.setMonto(0);
            vGastos.setMonto(0);
            vCompras.setMonto(0);
            vDescuentos.setMonto(0);
            vTarjetas.setMonto(0);
            vCuentasCliente.setMonto(0);
            vCuentaProveedor.setMonto(0);
            vCuentaResponsables.setMonto(0);
            _cajas = l;
            foreach (var c in l)
            {
                vEfectivo.addPago( c.Efectivo);
                vGastos.addPago(c.EfectivoEnExtracciones);
                vCompras.addPago(c.EfectivoGastadoEnCompras);
                vDepositos.addPago(c.EfectivoObtenidoEnDepositos);
                vVentas.addPago(c.EfectivoObtenidoEnVentas);
                vInicial.addPago(c.EfectivoEnDepositoInicial);
                vCierre.addPago(c.EfectivoEnExtraccionFinal);
                vDescuentos.setMonto(c.EfectivoPerdidoEnDescuentos + vDescuentos.MontoDecimal);
                vCuentasCliente.addPago(c.PagosCuentasCLientes);
                vCuentaProveedor.addPago(c.DineroEnCuentasProveedor);
                vCuentaResponsables.addPago(c.DineroEnCuentasResponsables);
                vTarjetas.addPago(c.DineroEnTarjetas );
                
            }
            
        }
        public void setCajas(ResumenCajas c)
        {
            vEfectivo.setMonto(c.Efectivo);
            vGastos.setMonto(c.EfectivoEnExtracciones);
            vCompras.setMonto(c.EfectivoGastadoEnCompras);
            vDepositos.setMonto(c.EfectivoObtenidoEnDepositos);
            vVentas.setMonto(c.EfectivoObtenidoEnVentas);
            vInicial.setMonto(c.EfectivoEnDepositoInicial);
            vCierre.setMonto(c.EfectivoEnExtraccionFinal);
            vDescuentos.setMonto(c.EfectivoPerdidoEnDescuentos);
            vCuentasCliente.setMonto(c.PagosCuentasCLientes);
            vCuentaProveedor.setMonto(c.DineroEnCuentasProveedor);
            vCuentaResponsables.setMonto(c.DineroEnCuentasResponsables);
            vTarjetas.setMonto(c.DineroEnTarjetas);
            _cajas = c.Cajas;
            _resumen = c;
        }
        public bool contraido { get; set; }
        public int originalwidth { get; set; }
        private void botonAccion1_Click(object sender, EventArgs e)
        {
            if (contraido)
            {
                Width = originalwidth;
                contraido = false;
            }
            else
            {
                Contraer();
            }
        }
        public void Contraer()
        {
            originalwidth = Width;
            Width = 36;
            contraido = true;
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            bool control= ModifierKeys==Keys.Control;
            html.InformeCaja(_resumen, control); // mostrar en browser

           //ver.verHtml( html.InformeCaja(_resumen,control),_resumen.descripcion);
        }

        
    }
}
