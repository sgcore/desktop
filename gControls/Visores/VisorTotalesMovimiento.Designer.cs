﻿namespace gControls.Visores
{
    partial class VisorTotalesMovimiento
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelDIscriminaciones = new System.Windows.Forms.Panel();
            this.visorCotizacion1 = new gControls.Visores.VisorCotizacion();
            this.visorSaldoMovimiento1 = new gControls.Visores.VisorSaldoMovimiento();
            this.panelAccionesMovimiento1 = new gControls.Paneles.PanelAccionesMovimiento();
            this.SuspendLayout();
            // 
            // PanelDIscriminaciones
            // 
            this.PanelDIscriminaciones.AutoSize = true;
            this.PanelDIscriminaciones.BackColor = System.Drawing.Color.Transparent;
            this.PanelDIscriminaciones.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelDIscriminaciones.Location = new System.Drawing.Point(0, 0);
            this.PanelDIscriminaciones.Name = "PanelDIscriminaciones";
            this.PanelDIscriminaciones.Size = new System.Drawing.Size(677, 0);
            this.PanelDIscriminaciones.TabIndex = 7;
            // 
            // visorCotizacion1
            // 
            this.visorCotizacion1.BackColor = System.Drawing.Color.Transparent;
            this.visorCotizacion1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.visorCotizacion1.Location = new System.Drawing.Point(0, 52);
            this.visorCotizacion1.Name = "visorCotizacion1";
            this.visorCotizacion1.Size = new System.Drawing.Size(677, 43);
            this.visorCotizacion1.TabIndex = 9;
            this.visorCotizacion1.Vertical = false;
            // 
            // visorSaldoMovimiento1
            // 
            this.visorSaldoMovimiento1.AutoScroll = true;
            this.visorSaldoMovimiento1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.visorSaldoMovimiento1.Location = new System.Drawing.Point(0, 8);
            this.visorSaldoMovimiento1.Name = "visorSaldoMovimiento1";
            this.visorSaldoMovimiento1.Size = new System.Drawing.Size(677, 44);
            this.visorSaldoMovimiento1.TabIndex = 8;
            this.visorSaldoMovimiento1.Vertical = false;
            // 
            // panelAccionesMovimiento1
            // 
            this.panelAccionesMovimiento1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAccionesMovimiento1.Location = new System.Drawing.Point(0, 95);
            this.panelAccionesMovimiento1.Name = "panelAccionesMovimiento1";
            this.panelAccionesMovimiento1.Size = new System.Drawing.Size(677, 42);
            this.panelAccionesMovimiento1.TabIndex = 11;
            this.panelAccionesMovimiento1.Visible = false;
            // 
            // VisorTotalesMovimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.visorSaldoMovimiento1);
            this.Controls.Add(this.visorCotizacion1);
            this.Controls.Add(this.PanelDIscriminaciones);
            this.Controls.Add(this.panelAccionesMovimiento1);
            this.Name = "VisorTotalesMovimiento";
            this.Size = new System.Drawing.Size(677, 137);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PanelDIscriminaciones;
        private VisorSaldoMovimiento visorSaldoMovimiento1;
        private VisorCotizacion visorCotizacion1;
        private Paneles.PanelAccionesMovimiento panelAccionesMovimiento1;

    }
}
