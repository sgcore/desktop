﻿namespace gControls.Visores
{
    partial class VisorCotizacion
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorCotizacion));
            this.vCotizacion = new gControls.Visores.VisorMonto();
            this.vTotalCotizado = new gControls.Visores.VisorMonto();
            this.vMoneda = new gControls.Visores.VisorMonto();
            this.SuspendLayout();
            // 
            // vCotizacion
            // 
            this.vCotizacion.AutoSize = true;
            this.vCotizacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vCotizacion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vCotizacion.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vCotizacion.ColorFondoTitulo = System.Drawing.Color.RoyalBlue;
            this.vCotizacion.Dock = System.Windows.Forms.DockStyle.Left;
            this.vCotizacion.ImageBox = ((System.Drawing.Image)(resources.GetObject("vCotizacion.ImageBox")));
            this.vCotizacion.ImageButton = null;
            this.vCotizacion.ImagenFondoCero = null;
            this.vCotizacion.ImagenFondoNegativo = null;
            this.vCotizacion.ImagenFondoPositivo = null;
            this.vCotizacion.Location = new System.Drawing.Point(0, 0);
            this.vCotizacion.MinimumSize = new System.Drawing.Size(100, 40);
            this.vCotizacion.Monto = "0.000";
            this.vCotizacion.Name = "vCotizacion";
            this.vCotizacion.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vCotizacion.Size = new System.Drawing.Size(100, 45);
            this.vCotizacion.TabIndex = 0;
            this.vCotizacion.Titulo = "COTIZACION";
            this.vCotizacion.ToolTipDescripcion = "";
            this.vCotizacion.VerBoton = false;
            this.vCotizacion.VisibleImageBox = false;
            // 
            // vTotalCotizado
            // 
            this.vTotalCotizado.AutoSize = true;
            this.vTotalCotizado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vTotalCotizado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vTotalCotizado.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vTotalCotizado.ColorFondoTitulo = System.Drawing.Color.RoyalBlue;
            this.vTotalCotizado.Dock = System.Windows.Forms.DockStyle.Left;
            this.vTotalCotizado.ImageBox = ((System.Drawing.Image)(resources.GetObject("vTotalCotizado.ImageBox")));
            this.vTotalCotizado.ImageButton = null;
            this.vTotalCotizado.ImagenFondoCero = null;
            this.vTotalCotizado.ImagenFondoNegativo = null;
            this.vTotalCotizado.ImagenFondoPositivo = null;
            this.vTotalCotizado.Location = new System.Drawing.Point(100, 0);
            this.vTotalCotizado.MinimumSize = new System.Drawing.Size(100, 40);
            this.vTotalCotizado.Monto = "$0.000";
            this.vTotalCotizado.Name = "vTotalCotizado";
            this.vTotalCotizado.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vTotalCotizado.Size = new System.Drawing.Size(100, 45);
            this.vTotalCotizado.TabIndex = 1;
            this.vTotalCotizado.Titulo = "TOTALCOTIZADO";
            this.vTotalCotizado.ToolTipDescripcion = "";
            this.vTotalCotizado.VerBoton = false;
            this.vTotalCotizado.VisibleImageBox = false;
            // 
            // vMoneda
            // 
            this.vMoneda.AutoSize = true;
            this.vMoneda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vMoneda.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vMoneda.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vMoneda.ColorFondoTitulo = System.Drawing.Color.RoyalBlue;
            this.vMoneda.Dock = System.Windows.Forms.DockStyle.Left;
            this.vMoneda.ImageBox = ((System.Drawing.Image)(resources.GetObject("vMoneda.ImageBox")));
            this.vMoneda.ImageButton = null;
            this.vMoneda.ImagenFondoCero = null;
            this.vMoneda.ImagenFondoNegativo = null;
            this.vMoneda.ImagenFondoPositivo = null;
            this.vMoneda.Location = new System.Drawing.Point(200, 0);
            this.vMoneda.MinimumSize = new System.Drawing.Size(100, 40);
            this.vMoneda.Monto = "Peso";
            this.vMoneda.Name = "vMoneda";
            this.vMoneda.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vMoneda.Size = new System.Drawing.Size(100, 45);
            this.vMoneda.TabIndex = 2;
            this.vMoneda.Titulo = "MONEDA";
            this.vMoneda.ToolTipDescripcion = "";
            this.vMoneda.VerBoton = false;
            this.vMoneda.VisibleImageBox = false;
            // 
            // VisorCotizacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.vMoneda);
            this.Controls.Add(this.vTotalCotizado);
            this.Controls.Add(this.vCotizacion);
            this.Name = "VisorCotizacion";
            this.Size = new System.Drawing.Size(305, 45);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private VisorMonto vCotizacion;
        private VisorMonto vTotalCotizado;
        private VisorMonto vMoneda;

    }
}
