﻿namespace gControls.Visores
{
    partial class VisorBilletes
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl050 = new System.Windows.Forms.Label();
            this.lbl500 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl010 = new System.Windows.Forms.Label();
            this.lbl1000 = new System.Windows.Forms.Label();
            this.lbl001 = new System.Windows.Forms.Label();
            this.lbl005 = new System.Windows.Forms.Label();
            this.lbl200 = new System.Windows.Forms.Label();
            this.lbl025 = new System.Windows.Forms.Label();
            this.lbl50 = new System.Windows.Forms.Label();
            this.lbl100 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl20 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl050
            // 
            this.lbl050.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl050.Image = global::gControls.Properties.Resources.m050;
            this.lbl050.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl050.Location = new System.Drawing.Point(241, 78);
            this.lbl050.Name = "lbl050";
            this.lbl050.Size = new System.Drawing.Size(80, 37);
            this.lbl050.TabIndex = 40;
            this.lbl050.Text = "0";
            this.lbl050.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl500
            // 
            this.lbl500.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl500.Image = global::gControls.Properties.Resources.m500;
            this.lbl500.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl500.Location = new System.Drawing.Point(5, 41);
            this.lbl500.Name = "lbl500";
            this.lbl500.Size = new System.Drawing.Size(112, 37);
            this.lbl500.TabIndex = 52;
            this.lbl500.Text = "0";
            this.lbl500.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl1
            // 
            this.lbl1.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Image = global::gControls.Properties.Resources.m1;
            this.lbl1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl1.Location = new System.Drawing.Point(241, 41);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(80, 37);
            this.lbl1.TabIndex = 38;
            this.lbl1.Text = "0";
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl010
            // 
            this.lbl010.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl010.Image = global::gControls.Properties.Resources.m010;
            this.lbl010.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl010.Location = new System.Drawing.Point(241, 115);
            this.lbl010.Name = "lbl010";
            this.lbl010.Size = new System.Drawing.Size(80, 37);
            this.lbl010.TabIndex = 44;
            this.lbl010.Text = "0";
            this.lbl010.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl1000
            // 
            this.lbl1000.BackColor = System.Drawing.Color.Transparent;
            this.lbl1000.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1000.Image = global::gControls.Properties.Resources.m1000;
            this.lbl1000.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lbl1000.Location = new System.Drawing.Point(5, 4);
            this.lbl1000.Name = "lbl1000";
            this.lbl1000.Size = new System.Drawing.Size(112, 37);
            this.lbl1000.TabIndex = 50;
            this.lbl1000.Text = "0";
            this.lbl1000.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl001
            // 
            this.lbl001.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl001.Image = global::gControls.Properties.Resources.m001;
            this.lbl001.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl001.Location = new System.Drawing.Point(413, 4);
            this.lbl001.Name = "lbl001";
            this.lbl001.Size = new System.Drawing.Size(80, 37);
            this.lbl001.TabIndex = 48;
            this.lbl001.Text = "0";
            this.lbl001.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl005
            // 
            this.lbl005.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl005.Image = global::gControls.Properties.Resources.m005;
            this.lbl005.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl005.Location = new System.Drawing.Point(327, 41);
            this.lbl005.Name = "lbl005";
            this.lbl005.Size = new System.Drawing.Size(80, 37);
            this.lbl005.TabIndex = 46;
            this.lbl005.Text = "0";
            this.lbl005.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl200
            // 
            this.lbl200.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl200.Image = global::gControls.Properties.Resources.m200;
            this.lbl200.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl200.Location = new System.Drawing.Point(5, 78);
            this.lbl200.Name = "lbl200";
            this.lbl200.Size = new System.Drawing.Size(112, 37);
            this.lbl200.TabIndex = 54;
            this.lbl200.Text = "0";
            this.lbl200.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl025
            // 
            this.lbl025.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl025.Image = global::gControls.Properties.Resources.m025;
            this.lbl025.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl025.Location = new System.Drawing.Point(327, 4);
            this.lbl025.Name = "lbl025";
            this.lbl025.Size = new System.Drawing.Size(80, 37);
            this.lbl025.TabIndex = 42;
            this.lbl025.Text = "0";
            this.lbl025.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl50
            // 
            this.lbl50.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl50.Image = global::gControls.Properties.Resources.m50;
            this.lbl50.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl50.Location = new System.Drawing.Point(123, 4);
            this.lbl50.Name = "lbl50";
            this.lbl50.Size = new System.Drawing.Size(112, 37);
            this.lbl50.TabIndex = 28;
            this.lbl50.Text = "0";
            this.lbl50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl100
            // 
            this.lbl100.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl100.Image = global::gControls.Properties.Resources.m100;
            this.lbl100.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl100.Location = new System.Drawing.Point(5, 115);
            this.lbl100.Name = "lbl100";
            this.lbl100.Size = new System.Drawing.Size(112, 37);
            this.lbl100.TabIndex = 26;
            this.lbl100.Text = "0";
            this.lbl100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl2
            // 
            this.lbl2.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Image = global::gControls.Properties.Resources.m2;
            this.lbl2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl2.Location = new System.Drawing.Point(241, 4);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(80, 37);
            this.lbl2.TabIndex = 36;
            this.lbl2.Text = "0";
            this.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl20
            // 
            this.lbl20.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl20.Image = global::gControls.Properties.Resources.m20;
            this.lbl20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl20.Location = new System.Drawing.Point(123, 41);
            this.lbl20.Name = "lbl20";
            this.lbl20.Size = new System.Drawing.Size(112, 37);
            this.lbl20.TabIndex = 30;
            this.lbl20.Text = "0";
            this.lbl20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl10
            // 
            this.lbl10.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl10.Image = global::gControls.Properties.Resources.m10;
            this.lbl10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl10.Location = new System.Drawing.Point(123, 78);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(112, 37);
            this.lbl10.TabIndex = 32;
            this.lbl10.Text = "0";
            this.lbl10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl5
            // 
            this.lbl5.Font = new System.Drawing.Font("Caladea", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.Image = global::gControls.Properties.Resources.m5;
            this.lbl5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl5.Location = new System.Drawing.Point(123, 115);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(112, 37);
            this.lbl5.TabIndex = 34;
            this.lbl5.Text = "0";
            this.lbl5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotal
            // 
            this.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotal.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.lblTotal.Image = global::gControls.Properties.Resources.DepositoCliente;
            this.lblTotal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTotal.Location = new System.Drawing.Point(336, 115);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(157, 36);
            this.lblTotal.TabIndex = 55;
            this.lblTotal.Text = "0";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.Gainsboro;
            this.lblTitle.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitle.Location = new System.Drawing.Point(336, 77);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(157, 37);
            this.lblTitle.TabIndex = 56;
            this.lblTitle.Text = "Total";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VisorBilletes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lbl050);
            this.Controls.Add(this.lbl500);
            this.Controls.Add(this.lbl1000);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl010);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.lbl20);
            this.Controls.Add(this.lbl001);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl005);
            this.Controls.Add(this.lbl100);
            this.Controls.Add(this.lbl200);
            this.Controls.Add(this.lbl50);
            this.Controls.Add(this.lbl025);
            this.Name = "VisorBilletes";
            this.Size = new System.Drawing.Size(505, 160);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl100;
        private System.Windows.Forms.Label lbl001;
        private System.Windows.Forms.Label lbl50;
        private System.Windows.Forms.Label lbl005;
        private System.Windows.Forms.Label lbl20;
        private System.Windows.Forms.Label lbl010;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl025;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl050;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl500;
        private System.Windows.Forms.Label lbl1000;
        private System.Windows.Forms.Label lbl200;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblTitle;
    }
}
