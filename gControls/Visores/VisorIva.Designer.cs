﻿namespace gControls.Visores
{
    partial class VisorIva
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.vTotal = new gControls.Visores.VisorMonto();
            this.vImpuestos = new gControls.Visores.VisorMonto();
            this.vIVAPercent = new gControls.Visores.VisorMonto();
            this.vSubtotal = new gControls.Visores.VisorMonto();
            this.SuspendLayout();
            // 
            // vTotal
            // 
            this.vTotal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vTotal.AutoSize = true;
            this.vTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vTotal.ColorFondoTitulo = System.Drawing.Color.SteelBlue;
            this.vTotal.ImagenFondoCero = null;
            this.vTotal.ImagenFondoNegativo = null;
            this.vTotal.ImagenFondoPositivo = null;
            this.vTotal.Location = new System.Drawing.Point(326, 0);
            this.vTotal.Monto = "$0.000";
            this.vTotal.Name = "vTotal";
            this.vTotal.Size = new System.Drawing.Size(100, 43);
            this.vTotal.TabIndex = 6;
            this.vTotal.Titulo = "TOTAL";
            // 
            // vImpuestos
            // 
            this.vImpuestos.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vImpuestos.AutoSize = true;
            this.vImpuestos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vImpuestos.ColorFondoTitulo = System.Drawing.Color.SteelBlue;
            this.vImpuestos.ImagenFondoCero = null;
            this.vImpuestos.ImagenFondoNegativo = null;
            this.vImpuestos.ImagenFondoPositivo = null;
            this.vImpuestos.Location = new System.Drawing.Point(220, 0);
            this.vImpuestos.Monto = "$0.000";
            this.vImpuestos.Name = "vImpuestos";
            this.vImpuestos.Size = new System.Drawing.Size(100, 43);
            this.vImpuestos.TabIndex = 5;
            this.vImpuestos.Titulo = "IMPUESTO";
            // 
            // vIVAPercent
            // 
            this.vIVAPercent.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vIVAPercent.AutoSize = true;
            this.vIVAPercent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vIVAPercent.ColorFondoTitulo = System.Drawing.Color.SteelBlue;
            this.vIVAPercent.ImagenFondoCero = null;
            this.vIVAPercent.ImagenFondoNegativo = null;
            this.vIVAPercent.ImagenFondoPositivo = null;
            this.vIVAPercent.Location = new System.Drawing.Point(114, 0);
            this.vIVAPercent.Monto = "$0.000";
            this.vIVAPercent.Name = "vIVAPercent";
            this.vIVAPercent.Size = new System.Drawing.Size(100, 43);
            this.vIVAPercent.TabIndex = 4;
            this.vIVAPercent.Titulo = "I.V.A. %";
            // 
            // vSubtotal
            // 
            this.vSubtotal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vSubtotal.AutoSize = true;
            this.vSubtotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vSubtotal.ColorFondoTitulo = System.Drawing.Color.SteelBlue;
            this.vSubtotal.ImagenFondoCero = null;
            this.vSubtotal.ImagenFondoNegativo = null;
            this.vSubtotal.ImagenFondoPositivo = null;
            this.vSubtotal.Location = new System.Drawing.Point(8, 0);
            this.vSubtotal.Monto = "$0.000";
            this.vSubtotal.Name = "vSubtotal";
            this.vSubtotal.Size = new System.Drawing.Size(100, 43);
            this.vSubtotal.TabIndex = 3;
            this.vSubtotal.Titulo = "SUBTOTAL";
            // 
            // VisorIva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.vTotal);
            this.Controls.Add(this.vImpuestos);
            this.Controls.Add(this.vIVAPercent);
            this.Controls.Add(this.vSubtotal);
            this.Name = "VisorIva";
            this.Size = new System.Drawing.Size(438, 60);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private VisorMonto vImpuestos;
        private VisorMonto vIVAPercent;
        private VisorMonto vSubtotal;
        private VisorMonto vTotal;


    }
}
