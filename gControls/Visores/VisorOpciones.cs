﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gLogger;

namespace gControls.Visores
{
    public partial class VisorOpciones : UserControl
    {
        private List<gControls.BotonOpcion.OptionReturn> _ops = new List<BotonOpcion.OptionReturn>();
        List<gControls.BotonOpcion> _lops = new List<BotonOpcion>();
        
        public VisorOpciones()
        {
            InitializeComponent();
            OpcionSeleccionada = new gControls.BotonOpcion.OptionReturn();
            //por defecto es la primera aveces
            OpcionSeleccionada.option = 0;
            OpcionSeleccionada.message = "Cancelado";
         
            
        }

        public List<gControls.BotonOpcion> Listaopciones { get { return _lops; } }
        public gControls.BotonOpcion.OptionReturn OpcionSeleccionada{get;set;}
        public List<gControls.BotonOpcion.OptionReturn> OpcionesSeleccionadas { get { return _ops; } }

        void b_GotFocus(object sender, EventArgs e)
        {
            AcceptButton = ((gControls.BotonOpcion)sender);
        }
        private void optionclick(object sender, EventArgs e)
        {
            var b = ((gControls.BotonOpcion)sender);

            OpcionSeleccionada = b.OpcionR;

        }
        public gControls.BotonOpcion addOpcion(int idopt, string texto, Bitmap img)
        {
            var b = new gControls.BotonOpcion(idopt, texto,img);
            Listaopciones.Add(b);
            b.Top = (25) * Listaopciones.Count;
            if (!MultiSelect)
            {
                b.Click += new EventHandler(optionclick);
                b.GotFocus += new EventHandler(b_GotFocus);
                b.DialogResult = System.Windows.Forms.DialogResult.OK;
            }

            Controls.Add(b);
            return b;
        }
        public bool MultiSelect { get; set; }
        private Button AcceptButton
        {
            get
            {
                var f = FindForm();
                if (f != null)
                {
                    return (Button) f.AcceptButton;
                }
                logger.Singlenton.addErrorMsg("Se produjo un error interno al crear el control de opciones.");
                return null;
            }
            set
            {
                var f = FindForm();
                if (f != null)
                {
                    f.AcceptButton=value;
                }
            }
        }
    }
}
