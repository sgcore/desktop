﻿namespace gControls.Visores
{
    partial class VisorEfectivo
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorEfectivo));
            this.MovImageList = new System.Windows.Forms.ImageList(this.components);
            this.vCuentaResponsables = new gControls.Visores.VisorMonto();
            this.vCuentaProveedor = new gControls.Visores.VisorMonto();
            this.vCuentasCliente = new gControls.Visores.VisorMonto();
            this.vTarjetas = new gControls.Visores.VisorMonto();
            this.botonAccion1 = new gControls.BotonAccion();
            this.vEfectivo = new gControls.Visores.VisorMonto();
            this.vCompras = new gControls.Visores.VisorMonto();
            this.vGastos = new gControls.Visores.VisorMonto();
            this.vDescuentos = new gControls.Visores.VisorMonto();
            this.vCierre = new gControls.Visores.VisorMonto();
            this.vInicial = new gControls.Visores.VisorMonto();
            this.vVentas = new gControls.Visores.VisorMonto();
            this.vDepositos = new gControls.Visores.VisorMonto();
            this.btImprimir = new gControls.BotonAccion();
            this.SuspendLayout();
            // 
            // MovImageList
            // 
            this.MovImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MovImageList.ImageStream")));
            this.MovImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MovImageList.Images.SetKeyName(0, "Precio");
            this.MovImageList.Images.SetKeyName(1, "Dinero");
            this.MovImageList.Images.SetKeyName(2, "Compra");
            this.MovImageList.Images.SetKeyName(3, "Retorno");
            this.MovImageList.Images.SetKeyName(4, "Venta");
            this.MovImageList.Images.SetKeyName(5, "Devolucion");
            this.MovImageList.Images.SetKeyName(6, "Entrada");
            this.MovImageList.Images.SetKeyName(7, "Salida");
            this.MovImageList.Images.SetKeyName(8, "Deposito");
            this.MovImageList.Images.SetKeyName(9, "Extraccion");
            this.MovImageList.Images.SetKeyName(10, "Pedido");
            this.MovImageList.Images.SetKeyName(11, "Presupuesto");
            this.MovImageList.Images.SetKeyName(12, "Terminado");
            this.MovImageList.Images.SetKeyName(13, "Creado");
            this.MovImageList.Images.SetKeyName(14, "Modificado");
            // 
            // vCuentaResponsables
            // 
            this.vCuentaResponsables.AutoScroll = true;
            this.vCuentaResponsables.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vCuentaResponsables.BackgroundImage")));
            this.vCuentaResponsables.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vCuentaResponsables.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vCuentaResponsables.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vCuentaResponsables.ColorFondoTitulo = System.Drawing.Color.DarkSlateBlue;
            this.vCuentaResponsables.Dock = System.Windows.Forms.DockStyle.Top;
            this.vCuentaResponsables.ImageBox = global::gControls.Properties.Resources.Down;
            this.vCuentaResponsables.ImageButton = global::gControls.Properties.Resources.Banco;
            this.vCuentaResponsables.ImagenFondoCero = null;
            this.vCuentaResponsables.ImagenFondoNegativo = ((System.Drawing.Image)(resources.GetObject("vCuentaResponsables.ImagenFondoNegativo")));
            this.vCuentaResponsables.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vCuentaResponsables.Location = new System.Drawing.Point(0, 440);
            this.vCuentaResponsables.MinimumSize = new System.Drawing.Size(100, 40);
            this.vCuentaResponsables.Monto = "$0.000";
            this.vCuentaResponsables.Name = "vCuentaResponsables";
            this.vCuentaResponsables.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vCuentaResponsables.Size = new System.Drawing.Size(186, 40);
            this.vCuentaResponsables.TabIndex = 86;
            this.vCuentaResponsables.Titulo = "Cuentas Responsables";
            this.vCuentaResponsables.ToolTipDescripcion = "Principalmente determína el flujo de dinero del sistema.";
            this.vCuentaResponsables.UseColorMontos = true;
            this.vCuentaResponsables.VerBoton = true;
            this.vCuentaResponsables.VisibleImageBox = false;
            // 
            // vCuentaProveedor
            // 
            this.vCuentaProveedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vCuentaProveedor.BackgroundImage")));
            this.vCuentaProveedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vCuentaProveedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vCuentaProveedor.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vCuentaProveedor.ColorFondoTitulo = System.Drawing.Color.Olive;
            this.vCuentaProveedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.vCuentaProveedor.ImageBox = global::gControls.Properties.Resources.Down;
            this.vCuentaProveedor.ImageButton = global::gControls.Properties.Resources.Proveedor;
            this.vCuentaProveedor.ImagenFondoCero = null;
            this.vCuentaProveedor.ImagenFondoNegativo = ((System.Drawing.Image)(resources.GetObject("vCuentaProveedor.ImagenFondoNegativo")));
            this.vCuentaProveedor.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vCuentaProveedor.Location = new System.Drawing.Point(0, 400);
            this.vCuentaProveedor.MinimumSize = new System.Drawing.Size(100, 40);
            this.vCuentaProveedor.Monto = "$0.000";
            this.vCuentaProveedor.Name = "vCuentaProveedor";
            this.vCuentaProveedor.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vCuentaProveedor.Size = new System.Drawing.Size(186, 40);
            this.vCuentaProveedor.TabIndex = 85;
            this.vCuentaProveedor.Titulo = "Cuentas Proveedores";
            this.vCuentaProveedor.ToolTipDescripcion = "Total del credito con los proveedores";
            this.vCuentaProveedor.UseColorMontos = true;
            this.vCuentaProveedor.VerBoton = true;
            this.vCuentaProveedor.VisibleImageBox = false;
            // 
            // vCuentasCliente
            // 
            this.vCuentasCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vCuentasCliente.BackgroundImage")));
            this.vCuentasCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vCuentasCliente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vCuentasCliente.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vCuentasCliente.ColorFondoTitulo = System.Drawing.Color.DarkSlateGray;
            this.vCuentasCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.vCuentasCliente.ImageBox = global::gControls.Properties.Resources.Down;
            this.vCuentasCliente.ImageButton = global::gControls.Properties.Resources.Cuenta;
            this.vCuentasCliente.ImagenFondoCero = null;
            this.vCuentasCliente.ImagenFondoNegativo = ((System.Drawing.Image)(resources.GetObject("vCuentasCliente.ImagenFondoNegativo")));
            this.vCuentasCliente.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vCuentasCliente.Location = new System.Drawing.Point(0, 360);
            this.vCuentasCliente.MinimumSize = new System.Drawing.Size(100, 40);
            this.vCuentasCliente.Monto = "$0.000";
            this.vCuentasCliente.Name = "vCuentasCliente";
            this.vCuentasCliente.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vCuentasCliente.Size = new System.Drawing.Size(186, 40);
            this.vCuentasCliente.TabIndex = 84;
            this.vCuentasCliente.Titulo = "Cuentas Clientes";
            this.vCuentasCliente.ToolTipDescripcion = "Total de la deuda de los clientes.";
            this.vCuentasCliente.UseColorMontos = true;
            this.vCuentasCliente.VerBoton = true;
            this.vCuentasCliente.VisibleImageBox = false;
            // 
            // vTarjetas
            // 
            this.vTarjetas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vTarjetas.BackgroundImage")));
            this.vTarjetas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vTarjetas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vTarjetas.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vTarjetas.ColorFondoTitulo = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.vTarjetas.Dock = System.Windows.Forms.DockStyle.Top;
            this.vTarjetas.ImageBox = global::gControls.Properties.Resources.Down;
            this.vTarjetas.ImageButton = global::gControls.Properties.Resources.Tarjeta;
            this.vTarjetas.ImagenFondoCero = null;
            this.vTarjetas.ImagenFondoNegativo = null;
            this.vTarjetas.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vTarjetas.Location = new System.Drawing.Point(0, 320);
            this.vTarjetas.MinimumSize = new System.Drawing.Size(100, 40);
            this.vTarjetas.Monto = "$0.000";
            this.vTarjetas.Name = "vTarjetas";
            this.vTarjetas.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vTarjetas.Size = new System.Drawing.Size(186, 40);
            this.vTarjetas.TabIndex = 83;
            this.vTarjetas.Titulo = "Tarjetas";
            this.vTarjetas.ToolTipDescripcion = "Pagos realizados con tarjetas de crédito.";
            this.vTarjetas.UseColorMontos = true;
            this.vTarjetas.VerBoton = true;
            this.vTarjetas.VisibleImageBox = false;
            // 
            // botonAccion1
            // 
            this.botonAccion1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonAccion1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.botonAccion1.FormContainer = null;
            this.botonAccion1.GlobalHotKey = false;
            this.botonAccion1.HacerInvisibleAlDeshabilitar = false;
            this.botonAccion1.HotKey = System.Windows.Forms.Shortcut.None;
            this.botonAccion1.Image = global::gControls.Properties.Resources.MostrarPanel;
            this.botonAccion1.Location = new System.Drawing.Point(0, 538);
            this.botonAccion1.Name = "botonAccion1";
            this.botonAccion1.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.botonAccion1.RegistradoEnForm = false;
            this.botonAccion1.Size = new System.Drawing.Size(186, 16);
            this.botonAccion1.TabIndex = 82;
            this.botonAccion1.Titulo = "Expandir / Contraer";
            this.botonAccion1.ToolTipDescripcion = "Expande y contrae el panel de efectivo.";
            this.botonAccion1.UseVisualStyleBackColor = true;
            this.botonAccion1.Click += new System.EventHandler(this.botonAccion1_Click);
            // 
            // vEfectivo
            // 
            this.vEfectivo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vEfectivo.BackgroundImage")));
            this.vEfectivo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vEfectivo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vEfectivo.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vEfectivo.ColorFondoTitulo = System.Drawing.Color.ForestGreen;
            this.vEfectivo.Dock = System.Windows.Forms.DockStyle.Top;
            this.vEfectivo.ImageBox = global::gControls.Properties.Resources.Efectivo;
            this.vEfectivo.ImageButton = global::gControls.Properties.Resources.Efectivo;
            this.vEfectivo.ImagenFondoCero = null;
            this.vEfectivo.ImagenFondoNegativo = global::gControls.Properties.Resources.FondoRojo;
            this.vEfectivo.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vEfectivo.Location = new System.Drawing.Point(0, 280);
            this.vEfectivo.MinimumSize = new System.Drawing.Size(100, 40);
            this.vEfectivo.Monto = "$0.000";
            this.vEfectivo.Name = "vEfectivo";
            this.vEfectivo.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vEfectivo.Size = new System.Drawing.Size(186, 40);
            this.vEfectivo.TabIndex = 81;
            this.vEfectivo.Titulo = "TOTAL EFECTIVO";
            this.vEfectivo.ToolTipDescripcion = "Total de dinero en efectivo con el que cuenta la caja.";
            this.vEfectivo.UseColorMontos = true;
            this.vEfectivo.VerBoton = true;
            this.vEfectivo.VisibleImageBox = false;
            // 
            // vCompras
            // 
            this.vCompras.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vCompras.BackgroundImage")));
            this.vCompras.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vCompras.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vCompras.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vCompras.ColorFondoTitulo = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.vCompras.Dock = System.Windows.Forms.DockStyle.Top;
            this.vCompras.ImageBox = global::gControls.Properties.Resources.Comprar;
            this.vCompras.ImageButton = global::gControls.Properties.Resources.Comprar;
            this.vCompras.ImagenFondoCero = null;
            this.vCompras.ImagenFondoNegativo = null;
            this.vCompras.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoRojo;
            this.vCompras.Location = new System.Drawing.Point(0, 240);
            this.vCompras.MinimumSize = new System.Drawing.Size(100, 40);
            this.vCompras.Monto = "$0.000";
            this.vCompras.Name = "vCompras";
            this.vCompras.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vCompras.Size = new System.Drawing.Size(186, 40);
            this.vCompras.TabIndex = 77;
            this.vCompras.Titulo = "Compras";
            this.vCompras.ToolTipDescripcion = "Click para ver los billetes";
            this.vCompras.UseColorMontos = true;
            this.vCompras.VerBoton = true;
            this.vCompras.VisibleImageBox = false;
            // 
            // vGastos
            // 
            this.vGastos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vGastos.BackgroundImage")));
            this.vGastos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vGastos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vGastos.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vGastos.ColorFondoTitulo = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.vGastos.Dock = System.Windows.Forms.DockStyle.Top;
            this.vGastos.ImageBox = global::gControls.Properties.Resources.Extraccion;
            this.vGastos.ImageButton = global::gControls.Properties.Resources.Extraccion;
            this.vGastos.ImagenFondoCero = null;
            this.vGastos.ImagenFondoNegativo = null;
            this.vGastos.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoRojo;
            this.vGastos.Location = new System.Drawing.Point(0, 200);
            this.vGastos.MinimumSize = new System.Drawing.Size(100, 40);
            this.vGastos.Monto = "$0.000";
            this.vGastos.Name = "vGastos";
            this.vGastos.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vGastos.Size = new System.Drawing.Size(186, 40);
            this.vGastos.TabIndex = 79;
            this.vGastos.Titulo = "Gastos";
            this.vGastos.ToolTipDescripcion = "Extracciones de dinero por parte de los responsables.";
            this.vGastos.UseColorMontos = true;
            this.vGastos.VerBoton = true;
            this.vGastos.VisibleImageBox = false;
            // 
            // vDescuentos
            // 
            this.vDescuentos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vDescuentos.BackgroundImage")));
            this.vDescuentos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vDescuentos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vDescuentos.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vDescuentos.ColorFondoTitulo = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.vDescuentos.Dock = System.Windows.Forms.DockStyle.Top;
            this.vDescuentos.ImageBox = global::gControls.Properties.Resources.Porciento;
            this.vDescuentos.ImageButton = global::gControls.Properties.Resources.Porciento;
            this.vDescuentos.ImagenFondoCero = null;
            this.vDescuentos.ImagenFondoNegativo = null;
            this.vDescuentos.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoRojo;
            this.vDescuentos.Location = new System.Drawing.Point(0, 160);
            this.vDescuentos.MinimumSize = new System.Drawing.Size(100, 40);
            this.vDescuentos.Monto = "$0.000";
            this.vDescuentos.Name = "vDescuentos";
            this.vDescuentos.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vDescuentos.Size = new System.Drawing.Size(186, 40);
            this.vDescuentos.TabIndex = 81;
            this.vDescuentos.Titulo = "Descuentos";
            this.vDescuentos.ToolTipDescripcion = "Click para ver los billetes";
            this.vDescuentos.UseColorMontos = true;
            this.vDescuentos.VerBoton = true;
            this.vDescuentos.VisibleImageBox = false;
            // 
            // vCierre
            // 
            this.vCierre.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vCierre.BackgroundImage")));
            this.vCierre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vCierre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vCierre.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vCierre.ColorFondoTitulo = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.vCierre.Dock = System.Windows.Forms.DockStyle.Top;
            this.vCierre.ImageBox = global::gControls.Properties.Resources.Up;
            this.vCierre.ImageButton = global::gControls.Properties.Resources.Up;
            this.vCierre.ImagenFondoCero = null;
            this.vCierre.ImagenFondoNegativo = null;
            this.vCierre.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoRojo;
            this.vCierre.Location = new System.Drawing.Point(0, 120);
            this.vCierre.MinimumSize = new System.Drawing.Size(100, 40);
            this.vCierre.Monto = "$0.000";
            this.vCierre.Name = "vCierre";
            this.vCierre.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vCierre.Size = new System.Drawing.Size(186, 40);
            this.vCierre.TabIndex = 80;
            this.vCierre.Titulo = "Extracción Final";
            this.vCierre.ToolTipDescripcion = "Recaudación en efectivo al cerrar la caja.";
            this.vCierre.UseColorMontos = true;
            this.vCierre.VerBoton = true;
            this.vCierre.VisibleImageBox = false;
            // 
            // vInicial
            // 
            this.vInicial.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vInicial.BackgroundImage")));
            this.vInicial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vInicial.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vInicial.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vInicial.ColorFondoTitulo = System.Drawing.Color.Gray;
            this.vInicial.Dock = System.Windows.Forms.DockStyle.Top;
            this.vInicial.ImageBox = global::gControls.Properties.Resources.Down;
            this.vInicial.ImageButton = global::gControls.Properties.Resources.Down;
            this.vInicial.ImagenFondoCero = null;
            this.vInicial.ImagenFondoNegativo = null;
            this.vInicial.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vInicial.Location = new System.Drawing.Point(0, 80);
            this.vInicial.MinimumSize = new System.Drawing.Size(100, 40);
            this.vInicial.Monto = "$0.000";
            this.vInicial.Name = "vInicial";
            this.vInicial.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vInicial.Size = new System.Drawing.Size(186, 40);
            this.vInicial.TabIndex = 74;
            this.vInicial.Titulo = "Deposito Inicial";
            this.vInicial.ToolTipDescripcion = "Dinero inicial con el que se empezó la caja.";
            this.vInicial.UseColorMontos = true;
            this.vInicial.VerBoton = true;
            this.vInicial.VisibleImageBox = false;
            // 
            // vVentas
            // 
            this.vVentas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vVentas.BackgroundImage")));
            this.vVentas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vVentas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vVentas.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vVentas.ColorFondoTitulo = System.Drawing.Color.Gray;
            this.vVentas.Dock = System.Windows.Forms.DockStyle.Top;
            this.vVentas.ImageBox = global::gControls.Properties.Resources.Vender;
            this.vVentas.ImageButton = global::gControls.Properties.Resources.Vender;
            this.vVentas.ImagenFondoCero = null;
            this.vVentas.ImagenFondoNegativo = null;
            this.vVentas.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vVentas.Location = new System.Drawing.Point(0, 40);
            this.vVentas.MinimumSize = new System.Drawing.Size(100, 40);
            this.vVentas.Monto = "$0.000";
            this.vVentas.Name = "vVentas";
            this.vVentas.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vVentas.Size = new System.Drawing.Size(186, 40);
            this.vVentas.TabIndex = 72;
            this.vVentas.Titulo = "Ventas";
            this.vVentas.ToolTipDescripcion = "Pagos en efectivo en las ventas a clientes.";
            this.vVentas.UseColorMontos = true;
            this.vVentas.VerBoton = true;
            this.vVentas.VisibleImageBox = false;
            // 
            // vDepositos
            // 
            this.vDepositos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vDepositos.BackgroundImage")));
            this.vDepositos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vDepositos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vDepositos.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vDepositos.ColorFondoTitulo = System.Drawing.Color.Gray;
            this.vDepositos.Dock = System.Windows.Forms.DockStyle.Top;
            this.vDepositos.ImageBox = global::gControls.Properties.Resources.Deposito;
            this.vDepositos.ImageButton = global::gControls.Properties.Resources.Depositar;
            this.vDepositos.ImagenFondoCero = null;
            this.vDepositos.ImagenFondoNegativo = null;
            this.vDepositos.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vDepositos.Location = new System.Drawing.Point(0, 0);
            this.vDepositos.MinimumSize = new System.Drawing.Size(100, 40);
            this.vDepositos.Monto = "$0.000";
            this.vDepositos.Name = "vDepositos";
            this.vDepositos.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.vDepositos.Size = new System.Drawing.Size(186, 40);
            this.vDepositos.TabIndex = 73;
            this.vDepositos.Titulo = "Depósitos";
            this.vDepositos.ToolTipDescripcion = "Dinero en efectivo que ingreso a la caja por depositos de clientes o responsables" +
    "| Los reintegros de proveedor tambien estan incluidos.";
            this.vDepositos.UseColorMontos = true;
            this.vDepositos.VerBoton = true;
            this.vDepositos.VisibleImageBox = false;
            // 
            // btImprimir
            // 
            this.btImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btImprimir.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btImprimir.FormContainer = null;
            this.btImprimir.GlobalHotKey = false;
            this.btImprimir.HacerInvisibleAlDeshabilitar = false;
            this.btImprimir.HotKey = System.Windows.Forms.Shortcut.None;
            this.btImprimir.Image = global::gControls.Properties.Resources.Imprimir;
            this.btImprimir.Location = new System.Drawing.Point(0, 505);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btImprimir.RegistradoEnForm = false;
            this.btImprimir.Size = new System.Drawing.Size(186, 33);
            this.btImprimir.TabIndex = 87;
            this.btImprimir.Titulo = "Mostrar Resumen";
            this.btImprimir.ToolTipDescripcion = "Muestra un resumen de la caja en el visor lateral| Si tiene presionado Control le" +
    " pedira el archivo de plantilla.";
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // VisorEfectivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btImprimir);
            this.Controls.Add(this.vCuentaResponsables);
            this.Controls.Add(this.vCuentaProveedor);
            this.Controls.Add(this.vCuentasCliente);
            this.Controls.Add(this.vTarjetas);
            this.Controls.Add(this.botonAccion1);
            this.Controls.Add(this.vEfectivo);
            this.Controls.Add(this.vCompras);
            this.Controls.Add(this.vGastos);
            this.Controls.Add(this.vDescuentos);
            this.Controls.Add(this.vCierre);
            this.Controls.Add(this.vInicial);
            this.Controls.Add(this.vVentas);
            this.Controls.Add(this.vDepositos);
            this.Name = "VisorEfectivo";
            this.Size = new System.Drawing.Size(186, 554);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList MovImageList;
        private VisorMonto vInicial;
        private VisorMonto vVentas;
        private VisorMonto vDepositos;
        private VisorMonto vCierre;
        private VisorMonto vCompras;
        private VisorMonto vGastos;
        private VisorMonto vEfectivo;
        private VisorMonto vDescuentos;
        private BotonAccion botonAccion1;
        private VisorMonto vTarjetas;
        private VisorMonto vCuentasCliente;
        private VisorMonto vCuentaProveedor;
        private VisorMonto vCuentaResponsables;
        private BotonAccion btImprimir;
    }
}
