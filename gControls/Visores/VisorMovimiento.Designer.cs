﻿namespace gControls.Visores
{
    partial class VisorMovimiento
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorMovimiento));
            this.MovImageList = new System.Windows.Forms.ImageList(this.components);
            this.lblTipo = new System.Windows.Forms.Label();
            this.lblfecha = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblTipoFactura = new System.Windows.Forms.Label();
            this.lblObservaciones = new System.Windows.Forms.Label();
            this.panelIzq = new System.Windows.Forms.Panel();
            this.panelDer = new System.Windows.Forms.Panel();
            this.btCerrar = new gControls.BotonAccion();
            this.btObs = new gControls.BotonAccion();
            this.panelIzq.SuspendLayout();
            this.panelDer.SuspendLayout();
            this.SuspendLayout();
            // 
            // MovImageList
            // 
            this.MovImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MovImageList.ImageStream")));
            this.MovImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MovImageList.Images.SetKeyName(0, "Compra");
            this.MovImageList.Images.SetKeyName(1, "Retorno");
            this.MovImageList.Images.SetKeyName(2, "Venta");
            this.MovImageList.Images.SetKeyName(3, "Devolucion");
            this.MovImageList.Images.SetKeyName(4, "Entrada");
            this.MovImageList.Images.SetKeyName(5, "Salida");
            this.MovImageList.Images.SetKeyName(6, "Deposito");
            this.MovImageList.Images.SetKeyName(7, "Extraccion");
            this.MovImageList.Images.SetKeyName(8, "Pedido");
            this.MovImageList.Images.SetKeyName(9, "Presupuesto");
            this.MovImageList.Images.SetKeyName(10, "Terminado");
            this.MovImageList.Images.SetKeyName(11, "Creado");
            this.MovImageList.Images.SetKeyName(12, "Modificado");
            // 
            // lblTipo
            // 
            this.lblTipo.BackColor = System.Drawing.Color.Transparent;
            this.lblTipo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipo.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblTipo.ImageKey = "Devolucion";
            this.lblTipo.ImageList = this.MovImageList;
            this.lblTipo.Location = new System.Drawing.Point(0, 0);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(158, 34);
            this.lblTipo.TabIndex = 0;
            this.lblTipo.Text = "Devolución";
            this.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblfecha
            // 
            this.lblfecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblfecha.BackColor = System.Drawing.Color.Transparent;
            this.lblfecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfecha.Location = new System.Drawing.Point(649, 34);
            this.lblfecha.Name = "lblfecha";
            this.lblfecha.Size = new System.Drawing.Size(161, 16);
            this.lblfecha.TabIndex = 1;
            this.lblfecha.Text = "Domingo 14 de junio de 2014";
            this.lblfecha.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblEstado
            // 
            this.lblEstado.BackColor = System.Drawing.Color.Transparent;
            this.lblEstado.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblEstado.ImageKey = "Terminado";
            this.lblEstado.ImageList = this.MovImageList;
            this.lblEstado.Location = new System.Drawing.Point(0, 34);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(158, 18);
            this.lblEstado.TabIndex = 2;
            this.lblEstado.Text = "Modificado por Usuario";
            this.lblEstado.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTipoFactura
            // 
            this.lblTipoFactura.BackColor = System.Drawing.Color.Transparent;
            this.lblTipoFactura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTipoFactura.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTipoFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoFactura.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblTipoFactura.ImageKey = "Terminado";
            this.lblTipoFactura.Location = new System.Drawing.Point(158, 0);
            this.lblTipoFactura.Name = "lblTipoFactura";
            this.lblTipoFactura.Size = new System.Drawing.Size(48, 52);
            this.lblTipoFactura.TabIndex = 3;
            this.lblTipoFactura.Text = "A";
            this.lblTipoFactura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblObservaciones
            // 
            this.lblObservaciones.BackColor = System.Drawing.Color.Transparent;
            this.lblObservaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblObservaciones.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObservaciones.Location = new System.Drawing.Point(248, 0);
            this.lblObservaciones.Name = "lblObservaciones";
            this.lblObservaciones.Size = new System.Drawing.Size(562, 52);
            this.lblObservaciones.TabIndex = 17;
            this.lblObservaciones.Text = "Observaciones";
            // 
            // panelIzq
            // 
            this.panelIzq.BackColor = System.Drawing.Color.Transparent;
            this.panelIzq.Controls.Add(this.lblTipo);
            this.panelIzq.Controls.Add(this.lblEstado);
            this.panelIzq.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelIzq.Location = new System.Drawing.Point(0, 0);
            this.panelIzq.Name = "panelIzq";
            this.panelIzq.Size = new System.Drawing.Size(158, 52);
            this.panelIzq.TabIndex = 18;
            // 
            // panelDer
            // 
            this.panelDer.BackColor = System.Drawing.Color.Transparent;
            this.panelDer.Controls.Add(this.btCerrar);
            this.panelDer.Controls.Add(this.btObs);
            this.panelDer.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDer.Location = new System.Drawing.Point(206, 0);
            this.panelDer.Name = "panelDer";
            this.panelDer.Size = new System.Drawing.Size(42, 52);
            this.panelDer.TabIndex = 19;
            // 
            // btCerrar
            // 
            this.btCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCerrar.Checked = false;
            this.btCerrar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCerrar.FormContainer = null;
            this.btCerrar.GlobalHotKey = false;
            this.btCerrar.HacerInvisibleAlDeshabilitar = false;
            this.btCerrar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCerrar.Image = global::gControls.Properties.Resources.Reubicar16;
            this.btCerrar.Location = new System.Drawing.Point(0, 23);
            this.btCerrar.Name = "btCerrar";
            this.btCerrar.Permiso = gManager.gcUsuario.PermisoEnum.Administrador;
            this.btCerrar.RegistradoEnForm = false;
            this.btCerrar.RequiereKey = true;
            this.btCerrar.Size = new System.Drawing.Size(42, 23);
            this.btCerrar.TabIndex = 14;
            this.btCerrar.Titulo = "Reubicar Movimiento";
            this.btCerrar.ToolTipDescripcion = "Envia el movimiento a la caja seleccionada y le asigna  una fecha antes del cierr" +
    "e | MODIFICA LA CAJA DE DESTINO!!";
            this.btCerrar.UseVisualStyleBackColor = true;
            this.btCerrar.Click += new System.EventHandler(this.btCerrar_Click);
            // 
            // btObs
            // 
            this.btObs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btObs.Checked = false;
            this.btObs.Dock = System.Windows.Forms.DockStyle.Top;
            this.btObs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btObs.FormContainer = null;
            this.btObs.GlobalHotKey = false;
            this.btObs.HacerInvisibleAlDeshabilitar = false;
            this.btObs.HotKey = System.Windows.Forms.Shortcut.None;
            this.btObs.Image = global::gControls.Properties.Resources.Observaciones16;
            this.btObs.Location = new System.Drawing.Point(0, 0);
            this.btObs.Name = "btObs";
            this.btObs.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btObs.RegistradoEnForm = false;
            this.btObs.RequiereKey = false;
            this.btObs.Size = new System.Drawing.Size(42, 23);
            this.btObs.TabIndex = 15;
            this.btObs.Titulo = "Observaciones";
            this.btObs.ToolTipDescripcion = "Edita las observaciones del movimiento";
            this.btObs.UseVisualStyleBackColor = true;
            this.btObs.Click += new System.EventHandler(this.btObs_Click);
            // 
            // VisorMovimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::gControls.Properties.Resources.FondoGris;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.lblfecha);
            this.Controls.Add(this.lblObservaciones);
            this.Controls.Add(this.panelDer);
            this.Controls.Add(this.lblTipoFactura);
            this.Controls.Add(this.panelIzq);
            this.DoubleBuffered = true;
            this.Name = "VisorMovimiento";
            this.Size = new System.Drawing.Size(810, 52);
            this.panelIzq.ResumeLayout(false);
            this.panelDer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList MovImageList;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.Label lblfecha;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblTipoFactura;
        private System.Windows.Forms.Label lblObservaciones;
        private System.Windows.Forms.Panel panelIzq;
        private System.Windows.Forms.Panel panelDer;
        private BotonAccion btObs;
        private BotonAccion btCerrar;
    }
}
