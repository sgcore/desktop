﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorCaracteristica : UserControl
    {
        public event EventHandler Asignado;
        public VisorCaracteristica()
        {
            InitializeComponent();
            CaraIndex = 10;
        }
        public string Descripcion { get { return waterMarkTextBox1.WaterMarkText; } set { waterMarkTextBox1.WaterMarkText = value; } }
        public string Titulo { get { return label1.Text; } set { label1.Text = value; } }
        public string Texto { get { return waterMarkTextBox1.Text; } set { waterMarkTextBox1.Text = value; } }
        public bool Editable { get { return !waterMarkTextBox1.ReadOnly; } set { waterMarkTextBox1.ReadOnly = !value; } }
        public void setText(string titulo, string texto)
        {
            Titulo = titulo; Texto = texto;
        }
        public int CaraIndex { get; set; }
        private void waterMarkTextBox1_Leave(object sender, EventArgs e)
        {
            if (Asignado != null) Asignado(CaraIndex, null);
        }

        private void waterMarkTextBox1_TextChanged(object sender, EventArgs e)
        {
            int l = waterMarkTextBox1.Lines.Count();
            this.Height =label1.Height+16+( 12*l);
        }
    }
}
