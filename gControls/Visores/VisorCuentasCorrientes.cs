﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorCuentasCorrientes : UserControl
    {
        public VisorCuentasCorrientes()
        {
            InitializeComponent();
            
        }
        public void setDestinatario(gManager.gcDestinatario d)
        {
            colDescripcion.AspectName="DescripcionMovimiento";
            //gManager.tuplaPagos t = new gManager.tuplaPagos();
            //t.DescripcionMovimiento = "";
            colDescripcion.Text = "Movimiento";
           
            if(d!=null)
                fastObjectListView1.SetObjects(gManager.CoreManager.Singlenton.getDestinatarioSeguimiento(d));
              

            else
            {
                fastObjectListView1.SetObjects(null);
            }
        }
       
        
    }
}
