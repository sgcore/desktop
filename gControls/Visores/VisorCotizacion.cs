﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorCotizacion : UserControl
    {
        public VisorCotizacion()
        {
            InitializeComponent();
        }
        public void setMovimiento(gManager.gcMovimiento m)
        {
            if (m != null)
            {
                vTotalCotizado.setMonto(m.TotalCotizado);
                vCotizacion.setMonto(m.Cotizacion, "");
                vMoneda.Monto = m.Moneda.ToString();
            }
            else
            {
                vTotalCotizado.setMonto(0);
                vCotizacion.setMonto(0 ,"");
                vMoneda.Monto = "Desconocido";

            }
        }
        bool _vert = false;
        public bool Vertical
        {
            get { return _vert; }
            set
            {
                _vert = value;
                if (_vert)
                {
                    vTotalCotizado.Dock = DockStyle.Top;
                    vCotizacion.Dock = DockStyle.Top;
                    vMoneda.Dock = DockStyle.Top;

                }
                else
                {
                    vTotalCotizado.Dock = DockStyle.Left;
                    vCotizacion.Dock = DockStyle.Left;
                    vMoneda.Dock = DockStyle.Left;
                   
                }
            }
        }
    }
}
