﻿namespace gControls.Visores
{
    partial class VisorCuentasCorrientes
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.fastObjectListView1 = new BrightIdeasSoftware.FastObjectListView();
            this.colDescripcion = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colPago = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colSaldo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.fastObjectListView1)).BeginInit();
            this.SuspendLayout();
            // 
            // fastObjectListView1
            // 
            this.fastObjectListView1.AllColumns.Add(this.colDescripcion);
            this.fastObjectListView1.AllColumns.Add(this.colPago);
            this.fastObjectListView1.AllColumns.Add(this.colSaldo);
            this.fastObjectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDescripcion,
            this.colPago,
            this.colSaldo});
            this.fastObjectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fastObjectListView1.FullRowSelect = true;
            this.fastObjectListView1.GridLines = true;
            this.fastObjectListView1.Location = new System.Drawing.Point(0, 0);
            this.fastObjectListView1.MultiSelect = false;
            this.fastObjectListView1.Name = "fastObjectListView1";
            this.fastObjectListView1.ShowGroups = false;
            this.fastObjectListView1.Size = new System.Drawing.Size(305, 446);
            this.fastObjectListView1.TabIndex = 0;
            this.fastObjectListView1.UseCompatibleStateImageBehavior = false;
            this.fastObjectListView1.View = System.Windows.Forms.View.Details;
            this.fastObjectListView1.VirtualMode = true;
            // 
            // colDescripcion
            // 
            this.colDescripcion.AspectName = "Destinatario";
            this.colDescripcion.FillsFreeSpace = true;
            this.colDescripcion.Text = "Destinatario";
            // 
            // colPago
            // 
            this.colPago.AspectName = "MontoPago";
            this.colPago.MaximumWidth = 100;
            this.colPago.MinimumWidth = 60;
            this.colPago.Text = "Pago";
            this.colPago.Width = 70;
            // 
            // colSaldo
            // 
            this.colSaldo.AspectName = "MontoSaldo";
            this.colSaldo.MaximumWidth = 100;
            this.colSaldo.MinimumWidth = 70;
            this.colSaldo.Text = "Saldo";
            this.colSaldo.Width = 70;
            // 
            // VisorCuentasCorrientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fastObjectListView1);
            this.Name = "VisorCuentasCorrientes";
            this.Size = new System.Drawing.Size(305, 446);
            ((System.ComponentModel.ISupportInitialize)(this.fastObjectListView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.FastObjectListView fastObjectListView1;
        private BrightIdeasSoftware.OLVColumn colDescripcion;
        private BrightIdeasSoftware.OLVColumn colPago;
        private BrightIdeasSoftware.OLVColumn colSaldo;
    }
}
