﻿namespace gControls.Visores
{
    partial class VisorPagos
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorPagos));
            this.vCheque = new gControls.Visores.VisorMonto();
            this.vCuenta = new gControls.Visores.VisorMonto();
            this.vTarjeta = new gControls.Visores.VisorMonto();
            this.vEfectivo = new gControls.Visores.VisorMonto();
            this.SuspendLayout();
            // 
            // vCheque
            // 
            this.vCheque.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vCheque.AutoSize = true;
            this.vCheque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vCheque.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vCheque.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vCheque.ColorFondoTitulo = System.Drawing.Color.SeaGreen;
            this.vCheque.ImageBox = ((System.Drawing.Image)(resources.GetObject("vCheque.ImageBox")));
            this.vCheque.ImageButton = null;
            this.vCheque.ImagenFondoCero = null;
            this.vCheque.ImagenFondoNegativo = null;
            this.vCheque.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vCheque.Location = new System.Drawing.Point(324, 0);
            this.vCheque.MinimumSize = new System.Drawing.Size(100, 32);
            this.vCheque.Monto = "$0.000";
            this.vCheque.Name = "vCheque";
            this.vCheque.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vCheque.Size = new System.Drawing.Size(100, 43);
            this.vCheque.TabIndex = 10;
            this.vCheque.Titulo = "CHEQUE";
            this.vCheque.ToolTipDescripcion = "";
            this.vCheque.VerBoton = false;
            this.vCheque.VisibleImageBox = false;
            // 
            // vCuenta
            // 
            this.vCuenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vCuenta.AutoSize = true;
            this.vCuenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vCuenta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vCuenta.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vCuenta.ColorFondoTitulo = System.Drawing.Color.SeaGreen;
            this.vCuenta.ImageBox = ((System.Drawing.Image)(resources.GetObject("vCuenta.ImageBox")));
            this.vCuenta.ImageButton = null;
            this.vCuenta.ImagenFondoCero = null;
            this.vCuenta.ImagenFondoNegativo = null;
            this.vCuenta.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vCuenta.Location = new System.Drawing.Point(112, 0);
            this.vCuenta.MinimumSize = new System.Drawing.Size(100, 32);
            this.vCuenta.Monto = "$0.000";
            this.vCuenta.Name = "vCuenta";
            this.vCuenta.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vCuenta.Size = new System.Drawing.Size(100, 43);
            this.vCuenta.TabIndex = 9;
            this.vCuenta.Titulo = "CUENTA CORRIENTE";
            this.vCuenta.ToolTipDescripcion = "";
            this.vCuenta.VerBoton = false;
            this.vCuenta.VisibleImageBox = false;
            // 
            // vTarjeta
            // 
            this.vTarjeta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vTarjeta.AutoSize = true;
            this.vTarjeta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vTarjeta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vTarjeta.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vTarjeta.ColorFondoTitulo = System.Drawing.Color.SeaGreen;
            this.vTarjeta.ImageBox = ((System.Drawing.Image)(resources.GetObject("vTarjeta.ImageBox")));
            this.vTarjeta.ImageButton = null;
            this.vTarjeta.ImagenFondoCero = null;
            this.vTarjeta.ImagenFondoNegativo = null;
            this.vTarjeta.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vTarjeta.Location = new System.Drawing.Point(218, 0);
            this.vTarjeta.MinimumSize = new System.Drawing.Size(100, 32);
            this.vTarjeta.Monto = "$0.000";
            this.vTarjeta.Name = "vTarjeta";
            this.vTarjeta.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vTarjeta.Size = new System.Drawing.Size(100, 43);
            this.vTarjeta.TabIndex = 8;
            this.vTarjeta.Titulo = "TARJETA";
            this.vTarjeta.ToolTipDescripcion = "";
            this.vTarjeta.VerBoton = false;
            this.vTarjeta.VisibleImageBox = false;
            // 
            // vEfectivo
            // 
            this.vEfectivo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vEfectivo.AutoSize = true;
            this.vEfectivo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vEfectivo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vEfectivo.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vEfectivo.ColorFondoTitulo = System.Drawing.Color.SeaGreen;
            this.vEfectivo.ImageBox = ((System.Drawing.Image)(resources.GetObject("vEfectivo.ImageBox")));
            this.vEfectivo.ImageButton = null;
            this.vEfectivo.ImagenFondoCero = null;
            this.vEfectivo.ImagenFondoNegativo = null;
            this.vEfectivo.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vEfectivo.Location = new System.Drawing.Point(6, 0);
            this.vEfectivo.MinimumSize = new System.Drawing.Size(100, 32);
            this.vEfectivo.Monto = "$0.000";
            this.vEfectivo.Name = "vEfectivo";
            this.vEfectivo.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vEfectivo.Size = new System.Drawing.Size(100, 43);
            this.vEfectivo.TabIndex = 7;
            this.vEfectivo.Titulo = "EFECTIVO";
            this.vEfectivo.ToolTipDescripcion = "";
            this.vEfectivo.VerBoton = false;
            this.vEfectivo.VisibleImageBox = false;
            // 
            // VisorPagos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.vCheque);
            this.Controls.Add(this.vCuenta);
            this.Controls.Add(this.vTarjeta);
            this.Controls.Add(this.vEfectivo);
            this.Name = "VisorPagos";
            this.Size = new System.Drawing.Size(427, 48);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private VisorMonto vCheque;
        private VisorMonto vCuenta;
        private VisorMonto vTarjeta;
        private VisorMonto vEfectivo;
    }
}
