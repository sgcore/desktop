﻿namespace gControls.Visores
{
    partial class VisorCajas
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorCajas));
            this.buscadorMovimientos1 = new gControls.Buscadores.BuscadorMovimientos();
            this.visorEfectivo1 = new gControls.Visores.VisorEfectivo();
            this.SuspendLayout();
            // 
            // buscadorMovimientos1
            // 
            this.buscadorMovimientos1.CustomDatos = false;
            this.buscadorMovimientos1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscadorMovimientos1.Imagen = ((System.Drawing.Image)(resources.GetObject("buscadorMovimientos1.Imagen")));
            this.buscadorMovimientos1.ImagenTitulo = ((System.Drawing.Image)(resources.GetObject("buscadorMovimientos1.ImagenTitulo")));
            this.buscadorMovimientos1.Location = new System.Drawing.Point(0, 0);
             this.buscadorMovimientos1.MovimientosCajaACtual = false;
            this.buscadorMovimientos1.Name = "buscadorMovimientos1";
            this.buscadorMovimientos1.VerBotonFiltro = false;
            this.buscadorMovimientos1.OcultarSiEstaVacio = false;
            this.buscadorMovimientos1.RequiereKey = false;
            this.buscadorMovimientos1.SeleccionGlobal = true;
            this.buscadorMovimientos1.Size = new System.Drawing.Size(567, 601);
            this.buscadorMovimientos1.TabIndex = 1;
            this.buscadorMovimientos1.Texto = null;
            this.buscadorMovimientos1.Titulo = "Titulo";
            this.buscadorMovimientos1.VerImagenCard = false;
            // 
            // visorEfectivo1
            // 
            this.visorEfectivo1.BackColor = System.Drawing.Color.Transparent;
            this.visorEfectivo1.contraido = false;
            this.visorEfectivo1.Dock = System.Windows.Forms.DockStyle.Right;
            this.visorEfectivo1.Location = new System.Drawing.Point(567, 0);
            this.visorEfectivo1.Name = "visorEfectivo1";
            this.visorEfectivo1.originalwidth = 0;
            this.visorEfectivo1.Size = new System.Drawing.Size(179, 601);
            this.visorEfectivo1.TabIndex = 0;
            // 
            // VisorCajas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buscadorMovimientos1);
            this.Controls.Add(this.visorEfectivo1);
            this.Imagen = global::gControls.Properties.Resources.Caja;
            this.Name = "VisorCajas";
            this.Size = new System.Drawing.Size(746, 601);
            this.Texto = "Caja";
            this.ResumeLayout(false);

        }

        #endregion

        private VisorEfectivo visorEfectivo1;
        private Buscadores.BuscadorMovimientos buscadorMovimientos1;
    }
}
