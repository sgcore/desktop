﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Visores
{
    public partial class VisorMonto : UserControl
    {
        string _sign="$";
        public VisorMonto()
        {
            InitializeComponent();
            ClickDelegate = verbilletes;
        }
        public VisorMonto(string titulo, decimal monto)
        {
            Titulo = titulo;
            setMonto(monto);

        }
        public System.Drawing.Color ColorFondoTitulo
        {
            get { return lblTitulo.BackColor; }
            set { lblTitulo.BackColor = value; }
        }
        public string Titulo
        {
            get { return lblTitulo.Text; }
            set { lblTitulo.Text = value; Boton.Titulo = value; }

        }
        private string desc = "";
        public string ToolTipDescripcion
        {
            get
            {
                return  desc;
            }
            set
            {
                desc=value;
                Boton.ToolTipDescripcion = Monto+Environment.NewLine + desc;
            }
        }
        public gManager.gcUsuario.PermisoEnum PermisoBoton
        {
            get
            {
                return Boton.Permiso;
            }
            set
            {
                Boton.Permiso = value;
            }
        }
        public string Monto
        {
            get { return lblTotal.Text; }
            set { lblTotal.Text = value; }
        }
        new public bool Enabled
        {
            get
            {
                return Boton.Enabled;
            }
            set
            {
                Boton.Enabled = value;
            }
        }
        public Image ImagenFondoPositivo { get; set; }
        public Image ImagenFondoNegativo { get; set; }
        public Image ImagenFondoCero { get; set; }
        public void setMonto(decimal m, string sign = "$")
        {
            setMonto(new gManager.gcPago(m, gManager.gcPago.TipoPago.Efectivo), sign);

        }
        public void setMontoToZero()
        {

        }
        
        gManager.gcPago _pago=new gManager.gcPago(0,gManager.gcPago.TipoPago.Efectivo);
        public void addPago(gManager.gcPago p)
        {
            setMonto(Pago + p, _sign);
        }
        public bool UseColorMontos { get; set; }
        public void setMonto(gManager.gcPago p,string sign="$")
        {
            _sign = sign;
            _pago = p;
            var m = _pago.Monto;
           
            if (m > 0){
                //lblTotal.Image = ImagenFondoPositivo;
                BackgroundImage=ImagenFondoPositivo;
                Boton.BackgroundImage = ImagenFondoPositivo;
                if (UseColorMontos)
                    lblTitulo.BackgroundImage = ImagenFondoPositivo;
                lblTotal.ForeColor = Color.Black;
                Enabled = true;
            }

            else if (m < 0)
            {
                //lblTotal.Image = ImagenFondoNegativo;
                BackgroundImage = ImagenFondoNegativo;
                Boton.BackgroundImage = ImagenFondoNegativo;
                if (UseColorMontos)
                    lblTitulo.BackgroundImage = ImagenFondoNegativo;
                lblTotal.ForeColor = Color.Black;
                Enabled = true;
            }
            else
            {
               // lblTotal.Image = ImagenFondoCero;
                BackgroundImage = ImagenFondoCero;
                Boton.BackgroundImage = ImagenFondoCero;
                if (UseColorMontos)
                {
                    lblTitulo.BackgroundImage = ImagenFondoCero;
                    lblTitulo.ForeColor = Color.DarkGray;
                    lblTotal.ForeColor = Color.DarkGray;
                    Enabled = false;
                   
                }
                    
            }
            if (UseColorMontos)
            { lblTitulo.BackgroundImageLayout = ImageLayout.Stretch;
            lblTitulo.BackColor = Color.Transparent;
            }
            if (UseColorMontos)
            {
                lblTitulo.ForeColor = Color.Black;
                
            }
               

           


            lblTotal.Text = sign + m.ToString("0.000");
            ToolTipDescripcion = desc;
        }
        public gManager.gcPago Pago
        {
            get { return _pago; }
        }
        
        
        public gcPago MontoDecimal
        {
            get { return _pago; }
        }
        public Image ImageButton
        {
            get { return Boton.Image; }
            set { Boton.Image = value; }
        }
        public Image ImageBox
        {
            get
            {
                return pictureBox1.Image;
            }
            set
            {
                pictureBox1.Image = value;
            }
        }
        public bool VisibleImageBox
        {
            get
            {
                return pictureBox1.Visible;
            }
            set
            {
                pictureBox1.Visible = value;
            }
        }
        public DockStyle BotonAlineacion
        {
            get { return Boton.Dock; }
            set { Boton.Dock = value; pictureBox1.Dock = value; }
        }
        public bool VerBoton
        {
            get { return Boton.Visible; }
            set { Boton.Visible = value; }
        }
        //evento
        public event EventHandler BotonClicked;

        private void Boton_Click(object sender, EventArgs e)
        {
            if (BotonClicked != null) BotonClicked(sender, e);
            else ClickDelegate?.Invoke(sender, e);
        }
        //[field
        //: NonSerializedAttribute()]
        internal EventHandler ClickDelegate { get; set; }
        internal void verbilletes(object o, EventArgs e)
        {
            if (Pago != null)
                ver.VerBilletes(Pago,Titulo);
        }
    }
}
