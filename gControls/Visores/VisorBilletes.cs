﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorBilletes : UserControl
    {
        public VisorBilletes()
        {
            InitializeComponent();
        }
        public void setPago(gManager.gcPago p, string title)
        {
            lblTitle.Text = title;
            if (p != null)
            {
                // lbl001.Text = p.m001.ToString();
                //lbl005.Text = p.m005.ToString();
                //lbl010.Text = p.m010.ToString();
                //lbl025.Text = p.m025.ToString();
                //lbl050.Text = p.m050.ToString();
                //lbl1.Text = p.m1.ToString();
                //lbl2.Text = p.m2.ToString();
                //lbl5.Text = p.m5.ToString();
                //lbl10.Text = p.m10.ToString();
                //lbl20.Text = p.m20.ToString();
                //lbl50.Text = p.m50.ToString();
                //lbl100.Text = p.m100.ToString();
                setLabel(lbl001, p.m001);
                setLabel(lbl005, p.m005);
                setLabel(lbl010, p.m010);
                setLabel(lbl025, p.m025);
                setLabel(lbl050, p.m050);
                setLabel(lbl1, p.m1);
                setLabel(lbl2, p.m2);
                setLabel(lbl5, p.m5);
                setLabel(lbl10, p.m10);
                setLabel(lbl20, p.m20);
                setLabel(lbl50, p.m50);
                setLabel(lbl100, p.m100);
                setLabel(lbl200, p.m200);
                setLabel(lbl500, p.m500);
                setLabel(lbl1000, p.m1000);
                lblTotal.Text = p.SacarTotal().ToString();


            }
            else
            {
                //lbl001.Text = 0.ToString();
                //lbl005.Text = 0.ToString();
                //lbl010.Text = 0.ToString();
                //lbl025.Text = 0.ToString();
                //lbl050.Text = 0.ToString();
                //lbl1.Text = 0.ToString();
                //lbl2.Text = 0.ToString();
                //lbl5.Text = 0.ToString();
                //lbl10.Text = 0.ToString();
                //lbl20.Text = 0.ToString();
                //lbl50.Text = 0.ToString();
                //lbl100.Text = 0.ToString();
                setLabel(lbl001, 0);
                setLabel(lbl005, 0);
                setLabel(lbl010, 0);
                setLabel(lbl025, 0);
                setLabel(lbl050, 0);
                setLabel(lbl1, 0);
                setLabel(lbl2, 0);
                setLabel(lbl5, 0);
                setLabel(lbl10, 0);
                setLabel(lbl20, 0);
                setLabel(lbl50, 0);
                setLabel(lbl100, 0);
                setLabel(lbl200, 0);
                setLabel(lbl500, 0);
                setLabel(lbl1000, 0);
                setLabel(lblTotal, 0);

            }



        }
        private void setLabel(Label l, int cant)
        {
            l.Text = cant.ToString();
            if (cant == 0)
            {
                l.ForeColor = Color.Silver;
            }
            UtilControls.ResaltarControl(l, cant == 0 ? 0 : 2);

        }
    }
}
