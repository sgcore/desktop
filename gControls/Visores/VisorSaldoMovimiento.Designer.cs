﻿namespace gControls.Visores
{
    partial class VisorSaldoMovimiento
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisorSaldoMovimiento));
            this.vTotal = new gControls.Visores.VisorMonto();
            this.vTotalAcreditado = new gControls.Visores.VisorMonto();
            this.vsaldo = new gControls.Visores.VisorMonto();
            this.vtotalpagado = new gControls.Visores.VisorMonto();
            this.vdescuento = new gControls.Visores.VisorMonto();
            this.SuspendLayout();
            // 
            // vTotal
            // 
            this.vTotal.AutoSize = true;
            this.vTotal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vTotal.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vTotal.ColorFondoTitulo = System.Drawing.Color.SlateGray;
            this.vTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.vTotal.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.vTotal.ImageBox = ((System.Drawing.Image)(resources.GetObject("vTotal.ImageBox")));
            this.vTotal.ImageButton = null;
            this.vTotal.ImagenFondoCero = global::gControls.Properties.Resources.FondoAmarillo;
            this.vTotal.ImagenFondoNegativo = global::gControls.Properties.Resources.FondoRojo;
            this.vTotal.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vTotal.Location = new System.Drawing.Point(0, 0);
            this.vTotal.MinimumSize = new System.Drawing.Size(100, 40);
            this.vTotal.Monto = "$0.000";
            this.vTotal.Name = "vTotal";
            this.vTotal.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vTotal.Size = new System.Drawing.Size(119, 40);
            this.vTotal.TabIndex = 10;
            this.vTotal.Titulo = "TOTAL";
            this.vTotal.ToolTipDescripcion = "";
            this.vTotal.VerBoton = false;
            this.vTotal.VisibleImageBox = false;
            // 
            // vTotalAcreditado
            // 
            this.vTotalAcreditado.AutoSize = true;
            this.vTotalAcreditado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vTotalAcreditado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vTotalAcreditado.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vTotalAcreditado.ColorFondoTitulo = System.Drawing.Color.SlateGray;
            this.vTotalAcreditado.Dock = System.Windows.Forms.DockStyle.Top;
            this.vTotalAcreditado.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.vTotalAcreditado.ImageBox = ((System.Drawing.Image)(resources.GetObject("vTotalAcreditado.ImageBox")));
            this.vTotalAcreditado.ImageButton = null;
            this.vTotalAcreditado.ImagenFondoCero = null;
            this.vTotalAcreditado.ImagenFondoNegativo = global::gControls.Properties.Resources.FondoAmarillo;
            this.vTotalAcreditado.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vTotalAcreditado.Location = new System.Drawing.Point(0, 80);
            this.vTotalAcreditado.MinimumSize = new System.Drawing.Size(100, 40);
            this.vTotalAcreditado.Monto = "$0.000";
            this.vTotalAcreditado.Name = "vTotalAcreditado";
            this.vTotalAcreditado.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vTotalAcreditado.Size = new System.Drawing.Size(119, 40);
            this.vTotalAcreditado.TabIndex = 9;
            this.vTotalAcreditado.Titulo = "ACREDITADO";
            this.vTotalAcreditado.ToolTipDescripcion = "";
            this.vTotalAcreditado.VerBoton = false;
            this.vTotalAcreditado.VisibleImageBox = false;
            // 
            // vsaldo
            // 
            this.vsaldo.AutoSize = true;
            this.vsaldo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vsaldo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vsaldo.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vsaldo.ColorFondoTitulo = System.Drawing.Color.SlateGray;
            this.vsaldo.Dock = System.Windows.Forms.DockStyle.Top;
            this.vsaldo.ImageBox = ((System.Drawing.Image)(resources.GetObject("vsaldo.ImageBox")));
            this.vsaldo.ImageButton = null;
            this.vsaldo.ImagenFondoCero = global::gControls.Properties.Resources.FondoVerde;
            this.vsaldo.ImagenFondoNegativo = global::gControls.Properties.Resources.FondoAmarillo;
            this.vsaldo.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoRojo;
            this.vsaldo.Location = new System.Drawing.Point(0, 120);
            this.vsaldo.MinimumSize = new System.Drawing.Size(100, 40);
            this.vsaldo.Monto = "$0.000";
            this.vsaldo.Name = "vsaldo";
            this.vsaldo.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vsaldo.Size = new System.Drawing.Size(119, 40);
            this.vsaldo.TabIndex = 8;
            this.vsaldo.Titulo = "SALDO";
            this.vsaldo.ToolTipDescripcion = "";
            this.vsaldo.VerBoton = false;
            this.vsaldo.VisibleImageBox = false;
            // 
            // vtotalpagado
            // 
            this.vtotalpagado.AutoSize = true;
            this.vtotalpagado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vtotalpagado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vtotalpagado.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vtotalpagado.ColorFondoTitulo = System.Drawing.Color.SlateGray;
            this.vtotalpagado.Dock = System.Windows.Forms.DockStyle.Top;
            this.vtotalpagado.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.vtotalpagado.ImageBox = ((System.Drawing.Image)(resources.GetObject("vtotalpagado.ImageBox")));
            this.vtotalpagado.ImageButton = null;
            this.vtotalpagado.ImagenFondoCero = global::gControls.Properties.Resources.FondoAmarillo;
            this.vtotalpagado.ImagenFondoNegativo = global::gControls.Properties.Resources.FondoRojo;
            this.vtotalpagado.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.vtotalpagado.Location = new System.Drawing.Point(0, 40);
            this.vtotalpagado.MinimumSize = new System.Drawing.Size(100, 40);
            this.vtotalpagado.Monto = "$0.000";
            this.vtotalpagado.Name = "vtotalpagado";
            this.vtotalpagado.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vtotalpagado.Size = new System.Drawing.Size(119, 40);
            this.vtotalpagado.TabIndex = 7;
            this.vtotalpagado.Titulo = "TOTAL PAGADO";
            this.vtotalpagado.ToolTipDescripcion = "";
            this.vtotalpagado.VerBoton = false;
            this.vtotalpagado.VisibleImageBox = false;
            // 
            // vdescuento
            // 
            this.vdescuento.AutoSize = true;
            this.vdescuento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vdescuento.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vdescuento.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vdescuento.ColorFondoTitulo = System.Drawing.Color.SlateGray;
            this.vdescuento.Dock = System.Windows.Forms.DockStyle.Top;
            this.vdescuento.ImageBox = ((System.Drawing.Image)(resources.GetObject("vdescuento.ImageBox")));
            this.vdescuento.ImageButton = null;
            this.vdescuento.ImagenFondoCero = global::gControls.Properties.Resources.FondoVerde;
            this.vdescuento.ImagenFondoNegativo = global::gControls.Properties.Resources.FondoAmarillo;
            this.vdescuento.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoAmarillo;
            this.vdescuento.Location = new System.Drawing.Point(0, 160);
            this.vdescuento.MinimumSize = new System.Drawing.Size(100, 40);
            this.vdescuento.Monto = "$0.000";
            this.vdescuento.Name = "vdescuento";
            this.vdescuento.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vdescuento.Size = new System.Drawing.Size(119, 40);
            this.vdescuento.TabIndex = 11;
            this.vdescuento.Titulo = "Descuento";
            this.vdescuento.ToolTipDescripcion = "";
            this.vdescuento.VerBoton = false;
            this.vdescuento.VisibleImageBox = false;
            // 
            // VisorSaldoMovimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.vdescuento);
            this.Controls.Add(this.vsaldo);
            this.Controls.Add(this.vTotalAcreditado);
            this.Controls.Add(this.vtotalpagado);
            this.Controls.Add(this.vTotal);
            this.Name = "VisorSaldoMovimiento";
            this.Size = new System.Drawing.Size(119, 211);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private VisorMonto vTotalAcreditado;
        private VisorMonto vsaldo;
        private VisorMonto vtotalpagado;
        private VisorMonto vTotal;
        private VisorMonto vdescuento;
    }
}
