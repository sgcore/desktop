﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Visores
{
    public partial class VisorSaldoMovimiento : UserControl
    {
        public VisorSaldoMovimiento()
        {
            InitializeComponent();
        }
        public void setMovimiento(gManager.gcMovimiento m)
        {
            if (m != null)
            {
               vtotalpagado.setMonto(m.TotalPagado);
               vTotalAcreditado.setMonto(m.TotalAcreditado);
               vsaldo.setMonto(m.Diferencia);
               vTotal.setMonto(m.TotalCotizado);
               vdescuento.setMonto(m.DescuentoPorcentual, "%");
               vtotalpagado.Visible = m.TotalPagado> 0;
               vTotalAcreditado.Visible = m.TotalAcreditado != 0;
               vsaldo.Visible = !m.EsMovimientoDinero && m.EsConDineroInvolucrado;
               vdescuento.Visible = m.SePuedeDescontar;
               vTotal.Visible = !m.EsMovimientoDinero;

                
            }
            else
            {
                vtotalpagado.setMonto(0);
                vTotalAcreditado.setMonto(0);
                vsaldo.setMonto(0);
                vTotal.setMonto(0);
                vdescuento.setMonto(0, "%");
               
            }
        }
        bool _vert=true;
        public bool Vertical
        {
            get { return _vert; }
            set
            {
                _vert = value;
                if (_vert)
                {
                    vtotalpagado.Dock = DockStyle.Top;
                    vTotalAcreditado.Dock = DockStyle.Top;
                    vsaldo.Dock = DockStyle.Top;
                    vTotal.Dock = DockStyle.Top;
                    vdescuento.Dock = DockStyle.Top;
                  
                }
                else
                {
                    vtotalpagado.Dock = DockStyle.Right;
                    vTotalAcreditado.Dock = DockStyle.Right;
                    vsaldo.Dock = DockStyle.Right;
                    vTotal.Dock = DockStyle.Right;
                    vdescuento.Dock = DockStyle.Right;
                }
            }
        }
    }
}
