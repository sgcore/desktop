﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace gControls.Visores
{
    public partial class VisorIva : UserControl
    {
        public VisorIva()
        {
            InitializeComponent();
        }
        public void setMontos(gManager.gcMovimiento.Discriminacion disc)
        {
            vSubtotal.setMonto(disc.Subtotal);
            vIVAPercent.setMonto(disc.Iva, "%");
            vImpuestos.setMonto(disc.Impuesto);
            vTotal.setMonto(disc.Total);

        }
        
        
    }
}
