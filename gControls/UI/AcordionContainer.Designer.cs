﻿namespace gControls.UI
{
    partial class AcordionContainer
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuContz = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pContenedor = new System.Windows.Forms.Panel();
            this.Botonera = new System.Windows.Forms.ToolStrip();
            this.SuspendLayout();
            // 
            // mnuContz
            // 
            this.mnuContz.Name = "mnuContz";
            this.mnuContz.Size = new System.Drawing.Size(61, 4);
            // 
            // pContenedor
            // 
            this.pContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pContenedor.Location = new System.Drawing.Point(0, 31);
            this.pContenedor.Margin = new System.Windows.Forms.Padding(0);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(675, 659);
            this.pContenedor.TabIndex = 1;
            // 
            // Botonera
            // 
            this.Botonera.AutoSize = false;
            this.Botonera.Location = new System.Drawing.Point(0, 0);
            this.Botonera.Name = "Botonera";
            this.Botonera.Size = new System.Drawing.Size(675, 31);
            this.Botonera.TabIndex = 0;
            // 
            // AcordionContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.pContenedor);
            this.Controls.Add(this.Botonera);
            this.Name = "AcordionContainer";
            this.Size = new System.Drawing.Size(675, 690);
            this.SizeChanged += new System.EventHandler(this.AcordionContainer_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuContz;
        private System.Windows.Forms.Panel pContenedor;
        protected System.Windows.Forms.ToolStrip Botonera;

    }
}
