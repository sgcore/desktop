﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.UI
{
    public partial class AcordionContainer : BaseControl
    {
       
        List<AcordionElement> _l = new List<AcordionElement>();
        public AcordionContainer()
        {
            InitializeComponent();
            pContenedor.BringToFront();
           
           
        }
        public void IniciarControles()
        {
            mnuContz.Items.Clear();
            foreach (Control c in Controls)
            {
                if (c is AcordionElement)
                {
                    AcordionElement ac = (AcordionElement)c;
                    ac.Contraer();
                    ac.Colapsando += ac_Colapsar;
                    ac.Expandiendo += ac_Expandiendo;
                    _l.Add(ac);
                    var nm=new ToolStripMenuItem(ac.Titulo,ac.Imagen,ac.Clickear,(Keys)ac.HotKey);
                    nm.ToolTipText = ac.ToolTipDescripcion;

                    mnuContz.Items.Add(nm);
                }
            }
        ControlMoverOrResizer.Init(this, this);
        }
        public void reset()
        {
            mnuContz.Items.Clear();
            pContenedor.Controls.Clear();
            recargarBarra();
           
        }
        public void addToContainer(Control c)
        {
            c.Visible = false;
            pContenedor.Controls.Add(c);
            c.Dock = DockStyle.Fill;
            
        }
        
        void ac_Expandiendo(object sender, EventArgs e)
        {
            SuspendDrawing(Parent);
            AcordionElement ac = (AcordionElement)sender;
            if (ac.fullHeight)
            {
                foreach (AcordionElement c in _l)
                {
                    if (c != ac)
                    {
                        c.Collapsed = true;
                        c.Visible = false;

                    }
                    else
                    {
                        c.Visible = true;
                    }
                   
                }
            }
            Expand();
            ResumeDrawing(Parent);
            
        }

        void ac_Colapsar(object sender, EventArgs e)
        {
            SuspendLayout();
            AcordionElement ac = (AcordionElement)sender;
            if (ac.fullHeight)
            {
                foreach (AcordionElement c in _l)
                {
                    c.Visible = true;

                }
            }
            ResumeLayout();
        }
        public void switchElemento(UI.AcordionElement e)
        {
            e.Expandir();
        }
        private int _widh = 500;
        public int ExpandedWidth
        {
            get { return _widh; }
            set { _widh = value; }
        }
        private void btSwitch_Click(object sender, EventArgs e)
        {
            

            if (Expandido)
                Contraer();
            else
                Expand();
        }
        public void Expand()
        {

            if (Frozen) return;
            congelarBuscadores(true);
            if (Dock == DockStyle.Fill) Dock = Alineacion;
            _rellenado = false;
           // Width = _widh;
            Visible = true;
            _expanded = true;
            congelarBuscadores(false);
          
           
           
        }
        public void Rellenar()
        {
            if (Frozen) return;
            congelarBuscadores(true);
            Visible = true;
            _expanded = false;//esto va primero
            _rellenado = true;
            if (Parent != null)
            {
                foreach (Control c in Parent.Controls)
                {
                    if (c.Dock == DockStyle.Fill && c != this) c.BringToFront();
                }
                BringToFront();
                Dock = DockStyle.Fill;//Width = Parent.Width - 16;
            }
           
            congelarBuscadores(false);
           
        }
        bool _expanded=true;
        bool _rellenado = false;
        public bool Expandido
        {
            get
            {
                return _expanded;
            }
        }
        public bool Rellenado
        {
            get
            {
                return _rellenado;
            }
        }
        public void Contraer()
        {
            if (Frozen) return;
            congelarBuscadores(true);
            if (Dock == DockStyle.Fill) Dock = Alineacion;
            _expanded = false;//esto va primero
            _rellenado = false;
            //Width = 3;
            Visible = false;
           
            
        }
        DockStyle _align=DockStyle.Right;
        public DockStyle Alineacion
        {
            get { return _align; }
            set
            {
                _align = value;
                if (_align == DockStyle.Fill || _align == DockStyle.None) _align = DockStyle.Right;
                Dock = _align;
            }
        }
       
        private Control _control;
        public Control Control
        {
            get { return _control; }
            set
            {
                if (value == null) return;
                bool encontrado = false;
                foreach (Control c in pContenedor.Controls)
                {
                    int i = int.Parse(c.Tag.ToString());
                    if( value== c)
                    {
                        encontrado = true;
                        c.Show();
                        _control = c;
                        congelarBuscadores(false);
                        
                        if (Botonera.Items.Count > i)
                        {
                            
                            Botonera.Items[i].BackgroundImage = Properties.Resources.FondoAzul;
                        }
                        Botonera.SendToBack();
                        continue;
                    }
                    c.Hide();
                    if (Botonera.Items.Count > i)
                    {

                        Botonera.Items[i].BackgroundImage = null;
                    }
                    
                   
                }
                if (!encontrado)
                {
                    _control = value;
                    pContenedor.Controls.Add(_control);
                    _control.Dock = DockStyle.Fill;
                    recargarBarra();
                }

                if (!Expandido && !Rellenado) Expand();
            }
        }
        public bool Frozen { get; set; }

        private void btFullSize_Click(object sender, EventArgs e)
        {
            if (Rellenado)
            {
                Expand();
                return;
            }
            if (ParentForm != null)
                Width = ParentForm.Width;
        }

        //propiedades
        public int AnchoFijo { get { return _widh; } set { _widh = value; } }

        private void congelarBuscadores(bool b)
        {
            if (Control != null && Control is Buscadores.ListBase)
                (Control as Buscadores.ListBase).Congelar(b);
        }
       

        private void AcordionContainer_SizeChanged(object sender, EventArgs e)
        {

           
            if (Expandido)
            {
                _widh = Width;
            }
        }

        public void recargarBarra()
        {
            Botonera.Items.Clear();
            Botonera.BringToFront();

            for (int i = pContenedor.Controls.Count - 1; i > -1; i--)
            {
                var c = pContenedor.Controls[i];
                if (c is BaseControl)
                {
                    var bb = (c as BaseControl);
                    if (bb == null) bb.Texto = bb.Name;
                    var it = new ToolStripButton(bb.Texto, bb.Imagen);
                    it.BackgroundImageLayout = ImageLayout.Stretch;
                    it.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
                    it.ToolTipText = (bb is Buscadores.ListBase)?(bb as Buscadores.ListBase).Titulo:bb.Texto;
                    it.Click += (o, e) =>
                    {
                        Control = c;
                    };
                   it.Visible = !bb.Bloqueado;
                   if (bb.RequiereKey) gControls.Registracion.BloquearConKey(it);
                   c.Tag= Botonera.Items.Add(it);
                }
             

            }
            if (pContenedor.Controls.Count > 0) Control = pContenedor.Controls[0];
        }
        public bool BotoneraVisible
        {
            get { return Botonera.Visible; }
            set { Botonera.Visible = value; }
        }
      
        
    }
}
