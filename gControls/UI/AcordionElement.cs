﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace gControls.UI
{
    [Designer(typeof(gControls.UI.UserControlDesigner))]
    public partial class AcordionElement : UserControl
    {
        public event EventHandler Colapsando;
        public event EventHandler Expandiendo;
        // define a property called "DropZone"
        [Category("Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Panel DropZone
        {
            get { return panelContenedor; }
        }
        int _colh = 100;
        public AcordionElement()
        {
            InitializeComponent();
            System.Reflection.PropertyInfo propiedadListView = typeof(AcordionElement).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            propiedadListView.SetValue(this, true, null);
           
        }
        public string Titulo
        {
            get { return bt.Titulo; }
            set
            {
                bt.Text = value; 
                bt.Titulo = value;
              
           
            }
        }
        
        public string ToolTipDescripcion
        {
            get
            {
                return bt.ToolTipDescripcion;
            }
            set
            {
                bt.ToolTipDescripcion = value;
               

            }
        }
        public gManager.gcUsuario.PermisoEnum Permiso
        {
            get { return bt.Permiso; }
            set
            {
                bt.Permiso = value;
              
            }
        }
        public bool Permitido
        {
            get
            {
                return bt.Permitido;
            }
        }
        public Shortcut HotKey
        {
            get
            {
                return bt.HotKey;

            }
            set
            {
                bt.HotKey = value;
            }
        }
        public bool GlobalHotKey
        {
            get
            {
                return bt.GlobalHotKey;
            }
            set
            {
                bt.GlobalHotKey = value;
            }
        }
        public void Clickear(object sender, EventArgs e)
        {
            bt.clickear(sender, e);
        }
        public Image Imagen
        {
            get
            {
                return bt.Image;
            }
            set
            {
                bt.Image = value;
            }
        }
        bool _cola = false;
        
        public bool Collapsed
        {
            get
            {
                return _cola;
            }
            set
            {
                
                bt.BackgroundImage = null;
                if (_cola == value) return;
                _cola = value;
               
                BaseControl.SuspendDrawing(this);
                SuspendLayout();
                if (_cola)
                {
                    panelContenedor.Visible = false;
                    this.Height = bt.Height;
                    
                  
                }
                else
                {
                    if (fullHeight)
                    {
                        if (Parent != null)
                        {
                            Height = Parent.Height;
                            
                        }
                    }else
                        this.Height = _colh;
                    panelContenedor.Visible = true;
                    
                    
                }
                ResumeLayout();
                BaseControl.ResumeDrawing(this);
               
            }
        }
        private void freezBuscador(bool state,ControlCollection col)
        {
            foreach(Control c in col)
            {
                if (c is Buscadores.ListBase)
                {
                    (c as Buscadores.ListBase).Congelar(state);
                }else if (c.HasChildren)
                {
                    freezBuscador(state, c.Controls);
                }
                
            }
        }
        public void Expandir()
        {
            if (Collapsed && Permitido)
            {
               
                if (Expandiendo != null) Expandiendo(this, null);
                Collapsed = false;
                bt.BackgroundImage = Properties.Resources.FondoVerde;

                freezBuscador(false, panelContenedor.Controls);
            }
           

        }
        public void Contraer()
        {
            if (!Collapsed)
            {
                freezBuscador(true, panelContenedor.Controls);
                if (Colapsando != null) Colapsando(this, null);
                Collapsed = true;
                if(fullHeight)
                    bt.BackgroundImage = Properties.Resources.FondoAmarillo;
                
            }
        }
        public bool fullHeight { get; set; }
        new public Size Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                _colh = Height;
            }
        }
        private void bt_Click(object sender, EventArgs e)
        {
            if (Collapsed) Expandir();
            else Contraer();
        }
    }
    // my designer
    public class UserControlDesigner : ParentControlDesigner
    {
        public override void Initialize(System.ComponentModel.IComponent component)
            {
                base.Initialize(component);

                if (this.Control is AcordionElement)
                {
                    this.EnableDesignMode(((AcordionElement)this.Control).DropZone, "DropZone");
                }
            }
    }
    
}
