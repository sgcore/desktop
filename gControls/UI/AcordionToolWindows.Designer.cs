﻿namespace gControls.UI
{
    partial class AcordionToolWindows
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelCOntenedor = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // PanelCOntenedor
            // 
            this.PanelCOntenedor.AutoScroll = true;
            this.PanelCOntenedor.AutoSize = true;
            this.PanelCOntenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelCOntenedor.Location = new System.Drawing.Point(0, 0);
            this.PanelCOntenedor.Name = "PanelCOntenedor";
            this.PanelCOntenedor.Size = new System.Drawing.Size(41, 89);
            this.PanelCOntenedor.TabIndex = 0;
            // 
            // AcordionToolWindows
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(41, 89);
            this.Controls.Add(this.PanelCOntenedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AcordionToolWindows";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "AcordionToolWindows";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.SystemColors.Control;
            this.Deactivate += new System.EventHandler(this.AcordionToolWindows_Deactivate);
            this.MouseEnter += new System.EventHandler(this.AcordionToolWindows_MouseEnter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel PanelCOntenedor;
    }
}