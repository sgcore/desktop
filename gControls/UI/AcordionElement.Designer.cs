﻿namespace gControls.UI
{
    partial class AcordionElement
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContenedor = new System.Windows.Forms.Panel();
            this.bt = new gControls.BotonAccion();
            this.SuspendLayout();
            // 
            // panelContenedor
            // 
            this.panelContenedor.BackColor = System.Drawing.Color.Transparent;
            this.panelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedor.Location = new System.Drawing.Point(0, 32);
            this.panelContenedor.Name = "panelContenedor";
            this.panelContenedor.Size = new System.Drawing.Size(549, 361);
            this.panelContenedor.TabIndex = 1;
            // 
            // bt
            // 
            this.bt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt.Checked = false;
            this.bt.Dock = System.Windows.Forms.DockStyle.Top;
            this.bt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt.FormContainer = null;
            this.bt.GlobalHotKey = false;
            this.bt.HacerInvisibleAlDeshabilitar = false;
            this.bt.HotKey = System.Windows.Forms.Shortcut.None;
            this.bt.Image = global::gControls.Properties.Resources.Conectado;
            this.bt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt.Location = new System.Drawing.Point(0, 0);
            this.bt.Name = "bt";
            this.bt.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.bt.RegistradoEnForm = false;
            this.bt.RequiereKey = false;
            this.bt.Size = new System.Drawing.Size(549, 32);
            this.bt.TabIndex = 0;
            this.bt.Text = "Titulo";
            this.bt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bt.Titulo = "";
            this.bt.ToolTipDescripcion = "";
            this.bt.UseVisualStyleBackColor = true;
            this.bt.Click += new System.EventHandler(this.bt_Click);
            // 
            // AcordionElement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panelContenedor);
            this.Controls.Add(this.bt);
            this.Name = "AcordionElement";
            this.Size = new System.Drawing.Size(549, 393);
            this.ResumeLayout(false);

        }

        #endregion

        private BotonAccion bt;
        private System.Windows.Forms.Panel panelContenedor;
    }
}
