﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.UI
{
    public partial class AcordionToolWindows : Form
    {
        public AcordionToolWindows()
        {
            InitializeComponent();
        }
        public void addBOton(System.Drawing.Image img, string titulo, string descripcion, Delegate accion) { }
        public void addBOton(BotonAccion b)
        {
            PanelCOntenedor.Controls.Add(b);
        }
       new  public void Show()
        {
             SetDesktopLocation(Cursor.Position.X-Width, Cursor.Position.Y);
             base.Show();
        }

       private void AcordionToolWindows_Deactivate(object sender, EventArgs e)
       {
           hideMe();
       }
        private void hideMe()
       {
           PanelCOntenedor.Hide();
           Width = 100;
       }
        private void showMe()
        {
            PanelCOntenedor.Visible = true;
            Width = 30;
        }

        private void AcordionToolWindows_MouseEnter(object sender, EventArgs e)
        {
            showMe();
        }
    }
}
