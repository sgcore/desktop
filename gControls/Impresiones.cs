﻿using gManager;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using BrightIdeasSoftware;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.IO;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp;
using System.Diagnostics;

namespace gControls
{
    class Impresiones
    {
        public static void ImprimirTicket(gcMovimiento m,bool elegir)
        {
            if (m == null) return;
            if (PrinterSettings.InstalledPrinters.Count == 0)
            {
                gLogger.logger.Singlenton.addWarningMsg("No hay impresoras instaladas");
                return;
            }
            string[] array = Properties.Settings.Default.PlantillaTicket[(int)m.Tipo].Split(';');
            string print = array.Length > 0 ? array[0] : "";
            string plantilla = array.Length > 1 ? array[1] : "";
            bool find = false;
            if (!elegir)
            {
                for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
                {
                    if (PrinterSettings.InstalledPrinters[i] == print)
                    {
                        find = true;
                        break;
                    }
                }
            }
             
            if(!find)
            {
                
                System.Windows.Forms.PrintDialog pd = new System.Windows.Forms.PrintDialog();
                
                //pd.PrinterSettings = new System.Drawing.Printing.PrinterSettings();
                pd.UseEXDialog = true;
                if (pd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    print = pd.PrinterSettings.PrinterName;
                    find = true;
                    Properties.Settings.Default.PlantillaTicket[(int)m.Tipo] = print + ";" + plantilla;
                    Properties.Settings.Default.Save();
                    
                }
            }

            if (find)
            {
                rawprint.RawPrinterHelper.SendStringToPrinter(print, generarTicket(m,elegir));
            }
           
            
           
        }
        
        private static string generarTicket(gcMovimiento m, bool elegir)
        {

            return parseTicket(html.Ticket(m, elegir));
           

            //StringBuilder t = new StringBuilder();
            //t.Append(TicketHelper.Iniciar + TicketHelper.Centrar2 + "D.I.G INSUMOS GRAFICOS" + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + TicketHelper.Centrar2 + "Maipu 58 Barrio Centenario" + TicketHelper.Salto);
            //t.Append(TicketHelper.Centrar2 + "Tel: (0385) 4228124" + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + TicketHelper.Centrar2 + "www.diginsumosgraficos.com.ar" + TicketHelper.Salto);

            //t.Append(TicketHelper.Centrar2 + "-----------------------------------" + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + "Cliente: " + m.Destinatario.Nombre + TicketHelper.Salto);
            //t.Append(TicketHelper.Centrar2 + "-----------------------------------" + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + "Ticket Nro: " + m.Numero.ToString("000000") + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + "Fecha: " + m.Fecha.ToString("dd/MM/yyyy") + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + "NO VALIDO COMO FACTURA" + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + TicketHelper.Centrar2 + " " + TicketHelper.Salto);
            //foreach (gcObjetoMovido o in m.ObjetosMovidos)
            //{
            //    t.Append(TicketHelper.Iniciar + o.Objeto.Codigo + TicketHelper.Salto);
            //    t.Append(o.Objeto.Nombre + TicketHelper.Salto);
            //    t.Append(TicketHelper.Iniciar + o.Cantidad.ToString("0.00") + " x " + o.Monto.ToString("0.00") + "          " + o.Total.ToString("$0.00") + TicketHelper.Salto);
            //}
            //t.Append(TicketHelper.Centrar2 + "-----------------------------------" + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + TicketHelper.negraalta + "SUBTOTAL: " + "        " + m.TotalMovimiento.ToString("$0.00") + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + TicketHelper.negraalta + "DESCUENTO: " + "       " +m.TotalDescontado.ToString("$0.00") + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + TicketHelper.negraalta + "TOTAL: " + "          " + (m.TotalMovimiento - m.TotalDescontado).ToString("$0.00") + TicketHelper.Salto);
            //decimal efe = m.PagosEnEfectivo.Monto;
            //decimal vuel = m.PagosEnVueltos.Monto;
            //decimal tar = m.PagosEnTarjeta.Monto;
            //decimal cuen = m.PagosEnCuentas.Monto;
            //t.Append(TicketHelper.Iniciar + "Efectivo: " + efe.ToString("$0.00") + ", Vuelto: " + vuel.ToString("$0.00") + ", Tarjeta: " + tar.ToString("$0.00") + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + "Uso de Cuenta: " + cuen.ToString("$0.00") + ", Deuda: " + m.Destinatario.SaldoEnCuenta. ToString("$0.00") + TicketHelper.Salto);

            //t.Append(TicketHelper.Iniciar + TicketHelper.Centrar2 + "-----------------------------------" + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + TicketHelper.Centrar2 + "Atendido por: " + m.Usuario.Nombre + TicketHelper.Salto);
            //t.Append(TicketHelper.Iniciar + TicketHelper.CortarPapel2 + TicketHelper.Salto);

            //return t.ToString();
        }
        private static string parseTicket(string t)
        {
          
            if (String.IsNullOrEmpty(t)) return "";
            var tt = t.Replace("[centrar]", TicketHelper.Centrar2).Replace("[negrita]", TicketHelper.negraalta)
                .Replace("[derecha]", TicketHelper.Derecha).Replace("[izquierda]", TicketHelper.Izquierda);
            string[] lineas = tt.Split("\n".ToCharArray());
            var ret = new StringBuilder();
            foreach (string cur in lineas)
            {
                ret.Append(TicketHelper.Iniciar + cur + TicketHelper.Salto);
            }
            ret.Append(TicketHelper.Iniciar + TicketHelper.CortarPapel2 + TicketHelper.Salto);
            return ret.ToString();
           
        }
       
        public static class TicketHelper
        {
            public static string CortarPapel = "\x0A\x0D\x0A\x0D\x0A\x0D\x0A\x0D\x0A\x0D\x0A\x0D\x0A\x0D\x0A\x0D\x0A\x0D\x1B\x69";
            public static string CortarPapel2 = Convert.ToChar(29) + "V" + Convert.ToChar(65) + Convert.ToChar(0);
            public static string Salto = "\x0A\x0D";
            public static string Iniciar = "\x1B@";
            public static string Centrar = "\x1B\x40\x31";
            public static string Centrar2 = Convert.ToChar(27) + "a" + Convert.ToChar(1);
            public static string Derecha = Convert.ToChar(27) + "a" + Convert.ToChar(2);
            public static string Izquierda = Convert.ToChar(27) + "a" + Convert.ToChar(0);
            public static string negraalta = Convert.ToChar(27) + "!" + Convert.ToChar(24);
            public static string normal = Convert.ToChar(27) + "!" + Convert.ToChar(0);

        }
        //public static void ImprimirBuscador(Buscadores.BuscadorBase b)
        //{
        //    ImprimirListView(b.E, b.Titulo);
        //}
        
        public static void ImprimirListView(ObjectListView lw,string header)
        {
            var f = new System.Windows.Forms.PrintPreviewDialog();
            var lwp1 = new ListViewPrinter();
            lwp1.ListView = lw;
            f.Document = lwp1;
            lwp1.IsShrinkToFit = false;
            lwp1.IsShrinkToFit = true;
            //lwp1.IsTextOnly = true;
            lwp1.IsPrintSelectionOnly = false;
            lwp1.CellFormat = null;
            lwp1.ListFont = new Font("Ms Sans Serif", 9);
            lwp1.ListGridPen = new Pen(Color.DarkGray, 0.5f);

            lwp1.HeaderFormat = BlockFormat.Header(new Font("Verdana", 24, FontStyle.Bold));
            lwp1.HeaderFormat.BackgroundBrush = new LinearGradientBrush(new Point(0, 0), new Point(200, 0), Color.Silver, Color.White);

            lwp1.FooterFormat = BlockFormat.Footer();
            lwp1.FooterFormat.BackgroundBrush = new LinearGradientBrush(new Point(0, 0), new Point(200, 0), Color.White, Color.Blue);

            lwp1.GroupHeaderFormat = BlockFormat.GroupHeader();
            lwp1.ListHeaderFormat = BlockFormat.ListHeader(new Font("Verdana", 12));

            lwp1.WatermarkFont = null;
            lwp1.WatermarkColor = Color.Empty;
            lwp1.DocumentName = "Documento";
            lwp1.Header = header.Replace("\\t", "\t");
            lwp1.Footer = "{1:F}\t\tPagina: {0}".Replace("\\t", "\t");
            lwp1.Watermark = "SgCore";
            lwp1.DefaultPageSettings.Margins.Top = 5;
            lwp1.DefaultPageSettings.Margins.Left = 5;
            lwp1.DefaultPageSettings.Margins.Right = 5;


            f.ShowDialog();
        }
        public static void imprimirPreimpresa(gcMovimiento m,bool elegir)
        {
            ver.verHtml(html.Preimpresa(m, elegir), m.DescripcionAuto);
           
        }
        public static void imprimirPdf(string htm)
        {

            PdfGenerateConfig config = new PdfGenerateConfig();
            config.PageSize = PageSize.A4;
            config.SetMargins(20);

            var doc = PdfGenerator.GeneratePdf(htm, config, null, html.OnStylesheetLoad, html.OnImageLoadPdfSharp);
            var tmpFile = UtilControls.ObtenerDireccionDeArchivo("Guardar archivo PDF", "Archivo PDF|*.pdf", true);
            // tmpFile = Path.GetFileNameWithoutExtension(tmpFile) + ".pdf";
            if (tmpFile != "")
            {
                doc.Save(tmpFile);
                Process.Start(tmpFile);
            }
        }
        //public static void generarArchivoHtml(string htm, string titulo)
        //{
        //    if (String.IsNullOrEmpty(titulo))
        //    {

        //    }
        //     var tmpFile = UtilControls.ObtenerDireccionDeArchivo("Guardar archivo HTML", "Archivo HTML|*.html", true);
        //    // tmpFile = Path.GetFileNameWithoutExtension(tmpFile) + ".pdf";
        //    if (tmpFile != "")
        //    {
        //        File.WriteAllText(tmpFile, htm);
        //        Process.Start(tmpFile);
        //    }
        //}
        public static void generarArchivoHtml(string htm, string key)
        {
            string path = "informes";
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            
            var file = key + ".html";
            var tmpFile = Path.Combine(path, file);
            if (File.Exists(tmpFile))
            {
                tmpFile = UtilControls.ObtenerDireccionDeArchivo("Guardar archivo HTML", "Archivo HTML|*.html", true, file, path); 
               
            }
            if (tmpFile != "")
            {
                File.WriteAllText(tmpFile, htm);
                Process.Start(tmpFile);
            }
        }
        public static void FacturaElectronica(gcMovimiento m)
        {
            string tra;
            tra = WSAA_CreateTRA("wsfe", 3600);

        }
        [DllImport("C:\\Program Files (x86)\\PyAfipWs\\pyafipws.dll")]
        private static extern string WSAA_CreateTRA(
              string service,
              long ttl
        );
        
        
       

    }
}
