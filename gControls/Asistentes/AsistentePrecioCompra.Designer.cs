﻿namespace gControls.Asistentes
{
    partial class AsistentePrecioCompra
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btDejarComoEsta = new System.Windows.Forms.Button();
            this.lblProducto = new System.Windows.Forms.Label();
            this.btActualizarPrecio = new System.Windows.Forms.Button();
            this.cntrPrecioAnterior = new gManager.Editores.EditorPrecio();
            this.cntrlPrecioNuevo = new gManager.Editores.EditorPrecio();
            this.SuspendLayout();
            // 
            // btDejarComoEsta
            // 
            this.btDejarComoEsta.AutoSize = true;
            this.btDejarComoEsta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDejarComoEsta.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btDejarComoEsta.Location = new System.Drawing.Point(72, 193);
            this.btDejarComoEsta.Name = "btDejarComoEsta";
            this.btDejarComoEsta.Size = new System.Drawing.Size(117, 23);
            this.btDejarComoEsta.TabIndex = 17;
            this.btDejarComoEsta.Text = "Dejar como esta..";
            this.btDejarComoEsta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDejarComoEsta.UseVisualStyleBackColor = true;
            this.btDejarComoEsta.Click += new System.EventHandler(this.noActualizar_Click);
            // 
            // lblProducto
            // 
            this.lblProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProducto.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblProducto.Location = new System.Drawing.Point(0, 0);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(600, 22);
            this.lblProducto.TabIndex = 21;
            this.lblProducto.Text = "Producto";
            this.lblProducto.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btActualizarPrecio
            // 
            this.btActualizarPrecio.AutoSize = true;
            this.btActualizarPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btActualizarPrecio.ForeColor = System.Drawing.Color.ForestGreen;
            this.btActualizarPrecio.Location = new System.Drawing.Point(400, 193);
            this.btActualizarPrecio.Name = "btActualizarPrecio";
            this.btActualizarPrecio.Size = new System.Drawing.Size(121, 23);
            this.btActualizarPrecio.TabIndex = 23;
            this.btActualizarPrecio.Text = "¡Actualizar!";
            this.btActualizarPrecio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btActualizarPrecio.UseVisualStyleBackColor = true;
            this.btActualizarPrecio.Click += new System.EventHandler(this.btSiguiente_Click);
            // 
            // cntrPrecioAnterior
            // 
            this.cntrPrecioAnterior.AutoSize = true;
            this.cntrPrecioAnterior.BackColor = System.Drawing.Color.Transparent;
            this.cntrPrecioAnterior.Costo = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrPrecioAnterior.Dolar = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrPrecioAnterior.Enabled = false;
            this.cntrPrecioAnterior.Ganancia = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrPrecioAnterior.IVA = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrPrecioAnterior.Location = new System.Drawing.Point(17, 25);
            this.cntrPrecioAnterior.Name = "cntrPrecioAnterior";
            this.cntrPrecioAnterior.Size = new System.Drawing.Size(164, 162);
            this.cntrPrecioAnterior.TabIndex = 20;
            this.cntrPrecioAnterior.VerBotonDolar = true;
            this.cntrPrecioAnterior.VerBotonIva = true;
            // 
            // cntrlPrecioNuevo
            // 
            this.cntrlPrecioNuevo.AutoSize = true;
            this.cntrlPrecioNuevo.BackColor = System.Drawing.Color.Transparent;
            this.cntrlPrecioNuevo.Costo = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrlPrecioNuevo.Dolar = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrlPrecioNuevo.Ganancia = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrlPrecioNuevo.IVA = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrlPrecioNuevo.Location = new System.Drawing.Point(357, 25);
            this.cntrlPrecioNuevo.Name = "cntrlPrecioNuevo";
            this.cntrlPrecioNuevo.Size = new System.Drawing.Size(164, 162);
            this.cntrlPrecioNuevo.TabIndex = 25;
            this.cntrlPrecioNuevo.VerBotonDolar = true;
            this.cntrlPrecioNuevo.VerBotonIva = true;
            // 
            // AsistentePrecioCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btDejarComoEsta);
            this.Controls.Add(this.btActualizarPrecio);
            this.Controls.Add(this.lblProducto);
            this.Controls.Add(this.cntrPrecioAnterior);
            this.Controls.Add(this.cntrlPrecioNuevo);
            this.Name = "AsistentePrecioCompra";
            this.Size = new System.Drawing.Size(600, 234);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btDejarComoEsta;
        private gManager.Editores.EditorPrecio cntrPrecioAnterior;
        private System.Windows.Forms.Label lblProducto;
        private System.Windows.Forms.Button btActualizarPrecio;
        private gManager.Editores.EditorPrecio cntrlPrecioNuevo;
    }
}
