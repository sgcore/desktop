﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Asistentes
{
    public partial class AsistenteCerrarCaja : UserControl
    {
        private decimal _necesario = 0;
        decimal _real = 0;
        
        
        public AsistenteCerrarCaja()
        {
            InitializeComponent();
           eeCajaReal.CambioTotal += eeCajaReal_CambioTotal;
           editorEfectivo2.CambioTotal += editorEfectivo2_CambioTotal;
            
            
           
        }

        void editorEfectivo2_CambioTotal(object sender, EventArgs e)
        {
            setBotonAceptar(CierreValido);
        }
        public gcPago Extraccion
        {
            get
            {
                return eeCajaReal.ObtenerPago(gcPago.TipoPago.Efectivo);
            }
        }
        public gcPago Deposito
        {
            get
            {
                return editorEfectivo2.ObtenerPago(gcPago.TipoPago.Efectivo);
            }
        }
        public void iniciar()
        {
            _necesario = CoreManager.Singlenton.CajaActual.Efectivo.Monto;
            lblNesesario.Text = _necesario.ToString("$0.00");
            eeCajaReal.Enabled = sePuedeCerrar;
            editorEfectivo2.Enabled = Saldo == 0 && sePuedeCerrar;
            setBotonAceptar(CierreValido);
            eeCajaReal.EstablecerTotal(CoreManager.Singlenton.CajaActual.CajaReal);

        }

        void eeCajaReal_CambioTotal(object sender, EventArgs e)
        {
            CoreManager.Singlenton.CajaActual.CajaReal = eeCajaReal.ObtenerPago(gcPago.TipoPago.Efectivo);
            _real =  CoreManager.Singlenton.CajaActual.CajaReal.Monto;
            lblSaldo.Text = Saldo.ToString("$0.00");
            lblReal.Text = _real.ToString("$0.00");
            editorEfectivo2.Enabled = Saldo == 0 && sePuedeCerrar ;
            setBotonAceptar(CierreValido);
        }
        private decimal Saldo
        {
            get
            {
                return _necesario - _real;
            }
        }
        private void setBotonAceptar(bool b)
        {
            btTerminar.Enabled = b;
            if (b)
            {
                btTerminar.BackgroundImage = Properties.Resources.Activo;
            }
            else
            {
                btTerminar.BackgroundImage = null;
            }
        }
        public bool CierreValido
        {
            get
            {
                return sePuedeCerrar && pagosCorrectos;
            }
        }
        private bool sePuedeCerrar
        {
            get
            {
                bool valido = true;
                gcCaja micaja =  CoreManager.Singlenton.CajaActual;
                //es la ultima caja.
                if (micaja.EsUltimaCaja)
                {
                    valido &= true;
                    lblEsUltimaCaja.ForeColor = Color.SteelBlue;
                    pbEsUltimaCaja.Image = gControls.Properties.Resources.Cancelar16;

                }
                else
                {
                    valido &= false;
                    lblEsUltimaCaja.ForeColor = Color.Red;
                    pbEsUltimaCaja.Image = gControls.Properties.Resources.Cancelar16;
                }
                //tiene compras o ventas
                if ((micaja.Compras.Count+micaja.Ventas.Count)>0)
                {
                    valido &= true;
                    lblCompraOventas.ForeColor = Color.SteelBlue;
                    pbCompraOventas.Image = gControls.Properties.Resources.Aceptar16;

                }
                else
                {
                    valido &= false;
                    lblCompraOventas.ForeColor = Color.Red;
                    pbCompraOventas.Image = gControls.Properties.Resources.Cancelar16;
                }
                //hay dinero en caja
                if ((micaja.Efectivo.Monto + micaja.Tarjeta.Monto+micaja.Cheque.Monto) > 0)
                {
                    valido &= true;
                    lblHayDInero.ForeColor = Color.SteelBlue;
                    pbHayDinero.Image = gControls.Properties.Resources.Aceptar16;

                }
                else
                {
                    valido &= false;
                    lblHayDInero.ForeColor = Color.Red;
                    pbHayDinero.Image = gControls.Properties.Resources.Cancelar16;
                }
                //todos cerrados
                if (micaja.Incompletos.Count == 0)
                {
                    valido &= true;
                    lblIncompletos.ForeColor = Color.SteelBlue;
                    pbIncompletos.Image = gControls.Properties.Resources.Aceptar16;

                }
                else
                {
                    valido &= false;
                    lblIncompletos.ForeColor = Color.Red;
                    pbIncompletos.Image = gControls.Properties.Resources.Cancelar16;
                }
                //tiene permiso
                if (CoreManager.Singlenton.ElUsuarioSuperaElPermiso(gcUsuario.PermisoEnum.Encargado))
                {
                    valido &= true;
                    lblTienePermiso.ForeColor = Color.SteelBlue;
                    pbTienePermiso.Image = gControls.Properties.Resources.Aceptar16;

                }
                else
                {
                    valido &= false;
                    lblTienePermiso.ForeColor = Color.Red;
                    pbTienePermiso.Image = gControls.Properties.Resources.Cancelar16;
                }

                return valido;
            }
        }
        private bool pagosCorrectos
        {
            get
            {
                bool valido = true;
                gcCaja micaja =  CoreManager.Singlenton.CajaActual;
                //coincide
                if (Saldo==0)
                {
                    valido &= true;
                    lblCoincide.ForeColor = Color.SteelBlue;
                    pbCoincide.Image = gControls.Properties.Resources.Aceptar16;

                }
                else
                {
                    valido &= false;
                    lblCoincide.ForeColor = Color.Red;
                    pbCoincide.Image = gControls.Properties.Resources.Cancelar16;
                }
                //depositado
                if (Deposito.Monto > 0)
                {
                    valido &= true;
                    lblTieneDeposito.ForeColor = Color.SteelBlue;
                    pbTieneDeposito.Image = gControls.Properties.Resources.Aceptar16;

                }
                else
                {
                    valido &= false;
                    lblTieneDeposito.ForeColor = Color.Red;
                    pbTieneDeposito.Image = gControls.Properties.Resources.Cancelar16;
                }
               

                return valido;
            }
        }

        private void botonAccion1_Click(object sender, EventArgs e)
        {
            if (sePuedeCerrar && CoreManager.Singlenton.CajaActual.cerrarCaja(Extraccion, Deposito))
            {
                if (ParentForm != null) ParentForm.DialogResult = DialogResult.OK;
            }
           
                
                
        }
    }
}
