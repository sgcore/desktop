﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Asistentes
{
    public partial class AsistentePrecioCompra : UserControl
    {
        public AsistentePrecioCompra()
        {
            InitializeComponent();
        }
        gcObjetoMovido _curr;
        gcObjeto _prod;
        System.Collections.IEnumerator _l;
        public void setObjetos(System.Collections.ICollection l)
        {
            _l = l.GetEnumerator();
            selectnext();
        }
        private void selectnext()
        {
            if (_l.MoveNext())
            {
                _curr = (gcObjetoMovido)_l.Current;
                Producto = _curr.Objeto;
            }
            else
            {
                Producto = null;
            }
            
        }
        public gcObjeto Producto
        {
            get
            {
                return _prod;
            }
            set
            {
                _prod = value;
                if(_prod!=null)
                {
                    
                    lblProducto.Text = _prod.Nombre;
                    btDejarComoEsta.Enabled = true;
                    cntrPrecioAnterior.setProducto(_prod);
                    cntrlPrecioNuevo.setProducto(_prod);
                    cntrlPrecioNuevo.Costo = _curr.MontoCotizado;
                    if(cntrlPrecioNuevo.Ganancia < 30)
                    {
                        cntrlPrecioNuevo.Ganancia = 50;
                    }
                   
                    

                }
                else
                {
                   btDejarComoEsta.Enabled = false;
                    btActualizarPrecio.Enabled = false;
                    cntrPrecioAnterior.Enabled = false;
                    cntrlPrecioNuevo.Enabled = false;
                }
            }
        }

        private void noActualizar_Click(object sender, EventArgs e)
        {
            selectnext();

        }
        

        private void btSiguiente_Click(object sender, EventArgs e)
        {
            Producto.Costo = cntrlPrecioNuevo.Costo;
            Producto.Ganancia = cntrlPrecioNuevo.Ganancia;
            Producto.Iva = cntrlPrecioNuevo.IVA;
            Producto.Dolar = cntrlPrecioNuevo.Dolar;
            Producto.SaveMe();
            selectnext();
        }

        
    }
}
