﻿using gControls.Editores;
namespace gControls.Asistentes
{
    partial class AsistenteCerrarCaja
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblReal = new System.Windows.Forms.Label();
            this.lblNesesario = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSaldo = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbIncompletos = new System.Windows.Forms.PictureBox();
            this.lblIncompletos = new System.Windows.Forms.Label();
            this.pbTieneDeposito = new System.Windows.Forms.PictureBox();
            this.lblTieneDeposito = new System.Windows.Forms.Label();
            this.pbTienePermiso = new System.Windows.Forms.PictureBox();
            this.lblTienePermiso = new System.Windows.Forms.Label();
            this.pbCoincide = new System.Windows.Forms.PictureBox();
            this.lblCoincide = new System.Windows.Forms.Label();
            this.pbHayDinero = new System.Windows.Forms.PictureBox();
            this.lblHayDInero = new System.Windows.Forms.Label();
            this.pbCompraOventas = new System.Windows.Forms.PictureBox();
            this.lblCompraOventas = new System.Windows.Forms.Label();
            this.pbEsUltimaCaja = new System.Windows.Forms.PictureBox();
            this.lblEsUltimaCaja = new System.Windows.Forms.Label();
            this.btTerminar = new gControls.BotonAccion();
            this.editorEfectivo2 = new gControls.Editores.EditorEfectivo();
            this.eeCajaReal = new gControls.Editores.EditorEfectivo();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIncompletos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTieneDeposito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTienePermiso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoincide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHayDinero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCompraOventas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEsUltimaCaja)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(507, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Cuente e ingrese los billetes y monedas que usted tenga en la caja real.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(188, 275);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(521, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ingrese los billetes y monedas que quedarán para empezar la nueva caja.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(563, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Dinero Real:";
            // 
            // lblReal
            // 
            this.lblReal.AutoSize = true;
            this.lblReal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReal.Location = new System.Drawing.Point(635, 51);
            this.lblReal.Name = "lblReal";
            this.lblReal.Size = new System.Drawing.Size(54, 20);
            this.lblReal.TabIndex = 5;
            this.lblReal.Text = "$0.00";
            // 
            // lblNesesario
            // 
            this.lblNesesario.AutoSize = true;
            this.lblNesesario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNesesario.Location = new System.Drawing.Point(635, 74);
            this.lblNesesario.Name = "lblNesesario";
            this.lblNesesario.Size = new System.Drawing.Size(54, 20);
            this.lblNesesario.TabIndex = 7;
            this.lblNesesario.Text = "$0.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(572, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Nesesario:";
            // 
            // lblSaldo
            // 
            this.lblSaldo.AutoSize = true;
            this.lblSaldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaldo.Location = new System.Drawing.Point(635, 104);
            this.lblSaldo.Name = "lblSaldo";
            this.lblSaldo.Size = new System.Drawing.Size(54, 20);
            this.lblSaldo.TabIndex = 9;
            this.lblSaldo.Text = "$0.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(592, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Saldo:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btTerminar);
            this.groupBox1.Controls.Add(this.pbIncompletos);
            this.groupBox1.Controls.Add(this.lblIncompletos);
            this.groupBox1.Controls.Add(this.pbTieneDeposito);
            this.groupBox1.Controls.Add(this.lblTieneDeposito);
            this.groupBox1.Controls.Add(this.pbTienePermiso);
            this.groupBox1.Controls.Add(this.lblTienePermiso);
            this.groupBox1.Controls.Add(this.pbCoincide);
            this.groupBox1.Controls.Add(this.lblCoincide);
            this.groupBox1.Controls.Add(this.pbHayDinero);
            this.groupBox1.Controls.Add(this.lblHayDInero);
            this.groupBox1.Controls.Add(this.pbCompraOventas);
            this.groupBox1.Controls.Add(this.lblCompraOventas);
            this.groupBox1.Controls.Add(this.pbEsUltimaCaja);
            this.groupBox1.Controls.Add(this.lblEsUltimaCaja);
            this.groupBox1.Location = new System.Drawing.Point(13, 287);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(148, 238);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Requisitos";
            // 
            // pbIncompletos
            // 
            this.pbIncompletos.Image = global::gControls.Properties.Resources.Cancelar16;
            this.pbIncompletos.Location = new System.Drawing.Point(6, 108);
            this.pbIncompletos.Name = "pbIncompletos";
            this.pbIncompletos.Size = new System.Drawing.Size(16, 16);
            this.pbIncompletos.TabIndex = 13;
            this.pbIncompletos.TabStop = false;
            // 
            // lblIncompletos
            // 
            this.lblIncompletos.AutoSize = true;
            this.lblIncompletos.ForeColor = System.Drawing.Color.Red;
            this.lblIncompletos.Location = new System.Drawing.Point(28, 108);
            this.lblIncompletos.Name = "lblIncompletos";
            this.lblIncompletos.Size = new System.Drawing.Size(95, 13);
            this.lblIncompletos.TabIndex = 12;
            this.lblIncompletos.Text = "Todos Terminados";
            // 
            // pbTieneDeposito
            // 
            this.pbTieneDeposito.Image = global::gControls.Properties.Resources.Cancelar16;
            this.pbTieneDeposito.Location = new System.Drawing.Point(6, 153);
            this.pbTieneDeposito.Name = "pbTieneDeposito";
            this.pbTieneDeposito.Size = new System.Drawing.Size(16, 16);
            this.pbTieneDeposito.TabIndex = 11;
            this.pbTieneDeposito.TabStop = false;
            // 
            // lblTieneDeposito
            // 
            this.lblTieneDeposito.AutoSize = true;
            this.lblTieneDeposito.ForeColor = System.Drawing.Color.Red;
            this.lblTieneDeposito.Location = new System.Drawing.Point(28, 153);
            this.lblTieneDeposito.Name = "lblTieneDeposito";
            this.lblTieneDeposito.Size = new System.Drawing.Size(77, 13);
            this.lblTieneDeposito.TabIndex = 10;
            this.lblTieneDeposito.Text = "Tiene deposito";
            // 
            // pbTienePermiso
            // 
            this.pbTienePermiso.Image = global::gControls.Properties.Resources.Cancelar16;
            this.pbTienePermiso.Location = new System.Drawing.Point(6, 85);
            this.pbTienePermiso.Name = "pbTienePermiso";
            this.pbTienePermiso.Size = new System.Drawing.Size(16, 16);
            this.pbTienePermiso.TabIndex = 9;
            this.pbTienePermiso.TabStop = false;
            // 
            // lblTienePermiso
            // 
            this.lblTienePermiso.AutoSize = true;
            this.lblTienePermiso.ForeColor = System.Drawing.Color.Red;
            this.lblTienePermiso.Location = new System.Drawing.Point(28, 85);
            this.lblTienePermiso.Name = "lblTienePermiso";
            this.lblTienePermiso.Size = new System.Drawing.Size(74, 13);
            this.lblTienePermiso.TabIndex = 8;
            this.lblTienePermiso.Text = "Tiene Permiso";
            // 
            // pbCoincide
            // 
            this.pbCoincide.Image = global::gControls.Properties.Resources.Cancelar16;
            this.pbCoincide.Location = new System.Drawing.Point(6, 131);
            this.pbCoincide.Name = "pbCoincide";
            this.pbCoincide.Size = new System.Drawing.Size(16, 16);
            this.pbCoincide.TabIndex = 7;
            this.pbCoincide.TabStop = false;
            // 
            // lblCoincide
            // 
            this.lblCoincide.AutoSize = true;
            this.lblCoincide.ForeColor = System.Drawing.Color.Red;
            this.lblCoincide.Location = new System.Drawing.Point(28, 131);
            this.lblCoincide.Name = "lblCoincide";
            this.lblCoincide.Size = new System.Drawing.Size(110, 13);
            this.lblCoincide.TabIndex = 6;
            this.lblCoincide.Text = "Coinciden los montos.";
            // 
            // pbHayDinero
            // 
            this.pbHayDinero.Image = global::gControls.Properties.Resources.Cancelar16;
            this.pbHayDinero.Location = new System.Drawing.Point(6, 62);
            this.pbHayDinero.Name = "pbHayDinero";
            this.pbHayDinero.Size = new System.Drawing.Size(16, 16);
            this.pbHayDinero.TabIndex = 5;
            this.pbHayDinero.TabStop = false;
            // 
            // lblHayDInero
            // 
            this.lblHayDInero.AutoSize = true;
            this.lblHayDInero.ForeColor = System.Drawing.Color.Red;
            this.lblHayDInero.Location = new System.Drawing.Point(28, 62);
            this.lblHayDInero.Name = "lblHayDInero";
            this.lblHayDInero.Size = new System.Drawing.Size(98, 13);
            this.lblHayDInero.TabIndex = 4;
            this.lblHayDInero.Text = "Hay Dinero en caja";
            // 
            // pbCompraOventas
            // 
            this.pbCompraOventas.Image = global::gControls.Properties.Resources.Cancelar16;
            this.pbCompraOventas.Location = new System.Drawing.Point(6, 39);
            this.pbCompraOventas.Name = "pbCompraOventas";
            this.pbCompraOventas.Size = new System.Drawing.Size(16, 16);
            this.pbCompraOventas.TabIndex = 3;
            this.pbCompraOventas.TabStop = false;
            // 
            // lblCompraOventas
            // 
            this.lblCompraOventas.AutoSize = true;
            this.lblCompraOventas.ForeColor = System.Drawing.Color.Red;
            this.lblCompraOventas.Location = new System.Drawing.Point(28, 39);
            this.lblCompraOventas.Name = "lblCompraOventas";
            this.lblCompraOventas.Size = new System.Drawing.Size(113, 13);
            this.lblCompraOventas.TabIndex = 2;
            this.lblCompraOventas.Text = "Hay compras o ventas";
            // 
            // pbEsUltimaCaja
            // 
            this.pbEsUltimaCaja.Image = global::gControls.Properties.Resources.Cancelar16;
            this.pbEsUltimaCaja.Location = new System.Drawing.Point(6, 16);
            this.pbEsUltimaCaja.Name = "pbEsUltimaCaja";
            this.pbEsUltimaCaja.Size = new System.Drawing.Size(16, 16);
            this.pbEsUltimaCaja.TabIndex = 1;
            this.pbEsUltimaCaja.TabStop = false;
            // 
            // lblEsUltimaCaja
            // 
            this.lblEsUltimaCaja.AutoSize = true;
            this.lblEsUltimaCaja.ForeColor = System.Drawing.Color.Red;
            this.lblEsUltimaCaja.Location = new System.Drawing.Point(28, 16);
            this.lblEsUltimaCaja.Name = "lblEsUltimaCaja";
            this.lblEsUltimaCaja.Size = new System.Drawing.Size(86, 13);
            this.lblEsUltimaCaja.TabIndex = 0;
            this.lblEsUltimaCaja.Text = "Es la ultima caja.";
            // 
            // btTerminar
            // 
            this.btTerminar.BackgroundImage = global::gControls.Properties.Resources.Activo;
            this.btTerminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btTerminar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btTerminar.GlobalHotKey = false;
            this.btTerminar.HacerInvisibleAlDeshabilitar = false;
            this.btTerminar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btTerminar.Location = new System.Drawing.Point(3, 208);
            this.btTerminar.Name = "btTerminar";
            this.btTerminar.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btTerminar.Size = new System.Drawing.Size(142, 27);
            this.btTerminar.TabIndex = 14;
            this.btTerminar.Text = "TERMINAR";
            this.btTerminar.Titulo = "Cerrar Caja";
            this.btTerminar.ToolTipDescripcion = "Realiza la extracción de la caja correspondiente y el deposito inicial para la si" +
    "guiente.";
            this.btTerminar.UseVisualStyleBackColor = true;
            this.btTerminar.Click += new System.EventHandler(this.botonAccion1_Click);
            // 
            // editorEfectivo2
            // 
            this.editorEfectivo2.AutoSize = true;
            this.editorEfectivo2.Enabled = false;
            this.editorEfectivo2.Location = new System.Drawing.Point(171, 294);
            this.editorEfectivo2.Name = "editorEfectivo2";
            this.editorEfectivo2.Size = new System.Drawing.Size(550, 232);
            this.editorEfectivo2.TabIndex = 1;
            // 
            // eeCajaReal
            // 
            this.eeCajaReal.AutoSize = true;
            this.eeCajaReal.Enabled = false;
            this.eeCajaReal.Location = new System.Drawing.Point(3, 25);
            this.eeCajaReal.Name = "eeCajaReal";
            this.eeCajaReal.Size = new System.Drawing.Size(550, 232);
            this.eeCajaReal.TabIndex = 0;
            // 
            // AsistenteCerrarCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblSaldo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblNesesario);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblReal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.editorEfectivo2);
            this.Controls.Add(this.eeCajaReal);
            this.Name = "AsistenteCerrarCaja";
            this.Size = new System.Drawing.Size(736, 543);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIncompletos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTieneDeposito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTienePermiso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoincide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbHayDinero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCompraOventas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEsUltimaCaja)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private EditorEfectivo eeCajaReal;
        private EditorEfectivo editorEfectivo2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblReal;
        private System.Windows.Forms.Label lblNesesario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblSaldo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbEsUltimaCaja;
        private System.Windows.Forms.Label lblEsUltimaCaja;
        private System.Windows.Forms.PictureBox pbCompraOventas;
        private System.Windows.Forms.Label lblCompraOventas;
        private System.Windows.Forms.PictureBox pbHayDinero;
        private System.Windows.Forms.Label lblHayDInero;
        private System.Windows.Forms.PictureBox pbCoincide;
        private System.Windows.Forms.Label lblCoincide;
        private System.Windows.Forms.PictureBox pbTienePermiso;
        private System.Windows.Forms.Label lblTienePermiso;
        private System.Windows.Forms.PictureBox pbTieneDeposito;
        private System.Windows.Forms.Label lblTieneDeposito;
        private System.Windows.Forms.PictureBox pbIncompletos;
        private System.Windows.Forms.Label lblIncompletos;
        private BotonAccion btTerminar;
    }
}
