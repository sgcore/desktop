﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gControls
{
    public enum EnumEntityTipo
    {
        Compra,
        Retorno,
        Venta,
        Devolucion,
        Entrada,
        Salida,
        Deposito,
        Extraccion,
        Presupuesto,
        Pedido,
        Proveedor,
        Cliente,
        Responsable,
        Sucursal,
        Financiera,
        Paciente,
        Contacto,
        Banco,
        Producto,
        ProductoYServicio,
        Categoria,
        Servicio,
        Grupo,
        TodosDestinatarios,
        TodosProductos,
        TodosMovimientos,
        MovimientosCajaActual,
        FiltroMovimientos,
        FiltroDestinatarios,
        FiltroProductos,
        FiltroCajas,
        Cajas,
        SeguimientoDestinatario,
        SeguimientoProducto,
        Consumo,
        Tratamientos




    }
}
