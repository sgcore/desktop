﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;

namespace gControls
{
    public class BotonAccion: Button
    {
       
        public BotonAccion()
        {
            InitializeComponent();
            gManager.CoreManager.Singlenton.CambiodeEstado += Singlenton_CambiodeEstado;
           
           
        }

        void Singlenton_CambiodeEstado(gManager.CoreManager.EstadoSistemaEnum e)
        {
           
            switch (e)
            {
                case gManager.CoreManager.EstadoSistemaEnum.Desconectado:
                case gManager.CoreManager.EstadoSistemaEnum.Conectado:
                case gManager.CoreManager.EstadoSistemaEnum.Cancelado:
                    cerrar();
                    break;
                case gManager.CoreManager.EstadoSistemaEnum.Autenticado:
                case gManager.CoreManager.EstadoSistemaEnum.Cargado:
                   iniciar();
                    break;
            }
        }

        private void iniciar()
        {
           
           

                Enabled = gManager.CoreManager.Singlenton.ElUsuarioSuperaElPermiso(Permiso);
                if (RequiereKey && !gManager.CoreManager.Singlenton.Key.Habilitado)
                {
                    Enabled = false;
                    BackgroundImage = Properties.Resources.FondoRojo;
                }
                if (HacerInvisibleAlDeshabilitar)
                {
                    Visible = Enabled;
                }
        }
        public bool RequiereKey { get; set; }
        public bool HacerInvisibleAlDeshabilitar { get; set; }
        public void iniciarTOolTips()
        {
            tt.RemoveAll();
            if (Properties.Settings.Default.cfgShowToolTips && ToolTipDescripcion != "")
            {
                tt.SetToolTip(this, _desc.Replace("|", Environment.NewLine) + Environment.NewLine + PermisoDescripcion + Environment.NewLine + HotKeyDescripcion);
            }
        }
        private void cerrar()
        {
            Enabled = Permiso == gManager.gcUsuario.PermisoEnum.Visitante;
            if (HacerInvisibleAlDeshabilitar)
            {
                Visible = Enabled;
            }
        }
        private bool _checked;
        public bool Checked
        {
            get { return _checked;}
            set
            {
                _checked = value;
                if (_checked)
                {
                    BackgroundImage = Properties.Resources.FondoAzul;

                }
                else
                    BackgroundImage = null;
            }
        }

       
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tt = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // tt
            // 
            this.tt.AutoPopDelay = 15000;
            this.tt.BackColor = System.Drawing.Color.Silver;
            this.tt.InitialDelay = 500;
            this.tt.IsBalloon = true;
            this.tt.ReshowDelay = 100;
            this.tt.ShowAlways = true;
            this.tt.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // BotonAccion
            // 
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UseVisualStyleBackColor = true;
            this.ResumeLayout(false);

        }

        private ToolTip tt;
        private IContainer components;
        string _desc = "";
        public string ToolTipDescripcion
        {
            get
            {
                
                return _desc ;
            }
            set
            {
                _desc = value;
                iniciarTOolTips();
               
                
            }
        }
        public string PermisoDescripcion
        {
            get
            {
                 return "(Requiere Permiso: " + Permiso.ToString()+")";
            }
        }
        public string HotKeyDescripcion
        {
            get
            {
                string sk = "";
                if (HotKey != Shortcut.None)
                {

                    sk = "(Acceso directo: " + HotKey.ToString() + (GlobalHotKey ? " Acción Global" : " Acción Local") + ")";
                   
                }
                return sk;
            }
        }
        public string Titulo
        {
            get { return tt.ToolTipTitle; }
            set
            {
               
                tt.ToolTipTitle = value;
                iniciarTOolTips();
            }
        }

        Shortcut _hkey = Shortcut.None;
        public Shortcut HotKey { get { return _hkey; }
            set
            {
                _hkey = value;
                RegistreMe();
            }   
        }
        private void RegistreMe()
        {
            if (!RegistradoEnForm && FormContainer != null)
            {
                FormContainer.registrarBoton(this);
            }
        }
        public bool RegistradoEnForm { get; set; }
        public Formularios.BaseForm FormContainer { get; set; }
        public bool GlobalHotKey { get;set; }
        gManager.gcUsuario.PermisoEnum _per;
        public gManager.gcUsuario.PermisoEnum Permiso { get { return _per; } set { _per = value; iniciarTOolTips(); } }
        public void clickear(object sender, EventArgs e)
        {
            if (!Permitido) return;
            if(!GlobalHotKey)
                PerformClick();
            else
            {
                FieldInfo eventClick = typeof(Control).GetField("EventClick", BindingFlags.NonPublic | BindingFlags.Static);
                object secret = eventClick.GetValue(null);
                // Retrieve the click event
                PropertyInfo eventsProp = typeof(Component).GetProperty("Events", BindingFlags.NonPublic | BindingFlags.Instance);
                EventHandlerList events = (EventHandlerList)eventsProp.GetValue(this, null);
                Delegate click = events[secret];
                Invoke(click);
             

            }
        }
        public bool Permitido
        {
            get
            {
                return gManager.CoreManager.Singlenton.ElUsuarioSuperaElPermiso(Permiso);
            }
        }
        
        

        
    }
}
