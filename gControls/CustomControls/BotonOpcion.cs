﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    [Serializable]
    public class BotonOpcion : Button
    {
       
        private OptionReturn _opcion;
        private bool _checked;
        public BotonOpcion()
            : base()
        {
            TextAlign = ContentAlignment.MiddleLeft;
            TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            chkImagen = Properties.Resources.Aceptar16;
            Checked = false;
            Width = 250;
            AutoSize = true;

            _opcion = new OptionReturn();
        }
        public BotonOpcion(int op, string msg, System.Drawing.Image img)
        {
            _opcion = new OptionReturn();
            _opcion.option = op;
            _opcion.message = msg;

            TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            TextAlign = ContentAlignment.MiddleLeft;
            tmpImagen = img;// gEngine.Utils.ObtenerImagenTombnail(img, 32);

            chkImagen = Properties.Resources.Aceptar16; ;
            Checked = false;
            Width = 250;
            AutoSize = true;
            Text = msg;
            Click += new EventHandler(BotonOpcion_Click);
        }

        void BotonOpcion_Click(object sender, EventArgs e)
        {
            Checked = !Checked;
        }
        public int Opcion { get { return _opcion.option; } set { _opcion.option = value; } }
        public string Mensaje { get { return _opcion.message; } set { _opcion.message = value; Text = value; } }

        public Image tmpImagen { get; set; }
        private Image chkImagen { get; set; }
        public bool Checked
        {
            get { return _checked; }
            set
            {
                _checked = value;
                if (_checked) Image = chkImagen.GetThumbnailImage(16, 16, callback, System.IntPtr.Zero);
                else
                {
                    if (tmpImagen != null)
                        Image =tmpImagen.GetThumbnailImage(16,16,callback,System.IntPtr.Zero) ;//tmpImagen.GetThumbnailImage(16, 16, callback, System.IntPtr.Zero);
                    else Image = null;
                }
            }
        }
        private bool callback()
        {
            return true;
        }
        public OptionReturn OpcionR
        {
            get { return _opcion; }
        }


        public class OptionReturn
        {
            public OptionReturn()
            {
                option = 0;
                message = "Sin asignar";
            }
            public int option { get; set; }
            public string message { get; set; }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BotonOpcion
            // 
            this.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ResumeLayout(false);

        }
    }
}
