﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    public class ToolStripBotonAccion : ToolStripControlHost
    {
        public ToolStripBotonAccion()
            : base(new BotonAccion())
        {
            
        }
        public BotonAccion Boton
        {
            get
            {
                return Control as BotonAccion;
            }
        }
    }
}
