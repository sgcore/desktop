﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace gControls.CustomControls
{
    public partial class YearSelector : UserControl
    {
     
        public YearSelector()
        {
            InitializeComponent();
            Minima = new DateTime(1955, 1, 1);
            Maxima = new DateTime(2090, 1, 1);
            analogClock1.Start();
        }
        public DateTime Minima { get; set; }
        public DateTime Maxima { get; set; }
        private DateTime _year;
        public DateTime Year
        {
            get { return _year ; }
            set 
            { 
                _year = value;
                int c=_year.Year;
                if (_year < Minima)
                {
                    _year = Minima;
                }
                if(_year>Maxima)
                {
                    _year = Maxima;
                }
                trackBar1.Value = (int)_year.TimeOfDay.TotalMinutes;
                button1.Text = _year.AddYears(-4).ToString("yyyy");
                button2.Text = _year.AddYears(-3).ToString("yyyy");
                button3.Text = _year.AddYears(-2).ToString("yyyy");
                button4.Text = _year.AddYears(-1).ToString("yyyy");
                button5.Text = _year.ToString("yyyy");
                button6.Text = _year.AddYears(1).ToString("yyyy");
                button7.Text = _year.AddYears(2).ToString("yyyy");
                button8.Text = _year.AddYears(3).ToString("yyyy");
                button9.Text = _year.AddYears(4).ToString("yyyy");

                button10.Text = _year.AddMonths(4).ToString("MMMM");
                button11.Text = _year.AddMonths(3).ToString("MMMM");
                button12.Text = _year.AddMonths(2).ToString("MMMM");
                button13.Text = _year.AddMonths(1).ToString("MMMM");
                button14.Text = _year.ToString("MMMM");
                button15.Text = _year.AddMonths(-1).ToString("MMMM");
                button16.Text = _year.AddMonths(-2).ToString("MMMM");
                button17.Text = _year.AddMonths(-3).ToString("MMMM");
                button18.Text = _year.AddMonths(-4).ToString("MMMM");

                button19.Text = _year.AddDays(4).ToString("dd");
                button20.Text = _year.AddDays(3).ToString("dd");
                button21.Text = _year.AddDays(2).ToString("dd");
                button22.Text = _year.AddDays(1).ToString("dd");
                button23.Text = _year.ToString("dd");
                button24.Text = _year.AddDays(-1).ToString("dd");
                button25.Text = _year.AddDays(-2).ToString("dd");
                button26.Text = _year.AddDays(-3).ToString("dd");
                button27.Text = _year.AddDays(-4).ToString("dd");

                lblFecha.Text = Year.ToLongDateString() + " a las " + Year.ToLongTimeString();
                
               
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Year = _year.AddYears(-4);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Year = _year.AddYears(-3);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Year = _year.AddYears(-2);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Year = _year.AddYears(-1);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Year = _year.AddYears(1);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Year = _year.AddYears(2);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Year = _year.AddYears(3);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Year = _year.AddYears(4);
        }

       

        private void button18_Click(object sender, EventArgs e)
        {
            Year = _year.AddMonths(-4);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Year = _year.AddMonths(-3);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Year = _year.AddMonths(-2);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Year = _year.AddMonths(-1);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Year = _year.AddMonths(1);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Year = _year.AddMonths(2);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Year = _year.AddMonths(3);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Year = _year.AddMonths(4);
        }

        private void button27_Click(object sender, EventArgs e)
        {
            Year = _year.AddDays(-4);
        }

        private void button26_Click(object sender, EventArgs e)
        {
            Year = _year.AddDays(-3);
        }

        private void button25_Click(object sender, EventArgs e)
        {
            Year = _year.AddDays(-2);
        }

        private void button24_Click(object sender, EventArgs e)
        {
            Year = _year.AddDays(-1);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            Year = _year.AddDays(1);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            Year = _year.AddDays(2);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Year = _year.AddDays(3);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Year = _year.AddDays(4);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

            _year = _year.AddMinutes(trackBar1.Value - (int)_year.TimeOfDay.TotalMinutes).AddSeconds(-(int)_year.TimeOfDay.Seconds);
            lblFecha.Text = Year.ToLongDateString() + " a las " + Year.ToLongTimeString();
        }
    }
}
