﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace gControls
{
    public class BaseControl : UserControl
    {
        private gManager.gcUsuario.PermisoEnum _permiso = gManager.gcUsuario.PermisoEnum.Visitante;
        public BaseControl()
        {
            this.Resize += new System.EventHandler(this.BaseControl_Resize);
           
           gManager.CoreManager.Singlenton.CambiodeEstado += Singlenton_CambiodeEstado;
          
           
        }
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        private const int WM_SETREDRAW = 11;

        public static void SuspendDrawing(Control parent)
        {if(parent!=null)
            SendMessage(parent.Handle, WM_SETREDRAW, false, 0);
        }

        public static void ResumeDrawing(Control parent)
        {
            if (parent != null)
            {
                 SendMessage(parent.Handle, WM_SETREDRAW, true, 0);
                 parent.Refresh();
            }
             
        }
        
        protected  gManager.gcUsuario.PermisoEnum Permiso
        {
            get
            {
                return _permiso;
            }
            set
            {
                _permiso = value;
            }
            
        }
        protected bool RequisitoPermiso(gManager.gcUsuario.PermisoEnum p)
        {
            //el permiso requerido es menor= que el actual(p)
            return ((int) p <=(int)gManager.CoreManager.Singlenton.PermisoActual);
            
        }
        protected void Enableall(bool b)
        {
            foreach (Control c in Controls)
            {
                c.Enabled = b;
            }
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (Height == 0 || Width == 0) return;
            using (LinearGradientBrush brush = new LinearGradientBrush(this.ClientRectangle,
                                                                       Color.GhostWhite,
                                                                       Color.DarkGray,
                                                                       90F))
            {
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
            }
        }
        public Image Imagen { get; set; }
        public string Texto { get; set; }

        void Singlenton_CambiodeEstado(gManager.CoreManager.EstadoSistemaEnum e)
        {
            CambiarEstado(e);
            switch (e)
            {
                case gManager.CoreManager.EstadoSistemaEnum.Desconectado:
                case gManager.CoreManager.EstadoSistemaEnum.Conectado:
                case gManager.CoreManager.EstadoSistemaEnum.Cancelado:
                    cerrar();
                    break;
                case gManager.CoreManager.EstadoSistemaEnum.Autenticado:
                case gManager.CoreManager.EstadoSistemaEnum.Cargado:
                    iniciar();
                    break;
            }
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            // Do not call base.OnPaint if you want to completely
            // control the appearance of the control.
            base.OnPaint(pe);
            // Insert code to do custom painting.
        }
        public virtual void cerrar() { Enableall(false); }
        public virtual void iniciar() { Enableall(RequisitoPermiso(Permiso)); }
        public virtual void reiniciar() { cerrar(); iniciar(); }
        protected virtual void CambiarEstado(gManager.CoreManager.EstadoSistemaEnum e){}

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BaseControl
            // 
            this.Name = "BaseControl";
            this.ResumeLayout(false);

        }
        public virtual bool Bloqueado { get { return false; } }
        public bool RequiereKey { get; set; }
        private void BaseControl_Resize(object sender, EventArgs e)
        {
            this.Invalidate();
            
            
        }


    }
}
