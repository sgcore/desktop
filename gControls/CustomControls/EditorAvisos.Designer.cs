﻿namespace gControls.CustomControls
{
    partial class EditorAvisos
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange1 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange2 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange3 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange4 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            System.Windows.Forms.Calendar.CalendarHighlightRange calendarHighlightRange5 = new System.Windows.Forms.Calendar.CalendarHighlightRange();
            this.botonera = new System.Windows.Forms.ToolStrip();
            this.btVerSemana = new System.Windows.Forms.ToolStripButton();
            this.btVerDia = new System.Windows.Forms.ToolStripButton();
            this.calendar1 = new System.Windows.Forms.Calendar.Calendar();
            this.monthView1 = new System.Windows.Forms.Calendar.MonthView();
            this.botonera.SuspendLayout();
            this.SuspendLayout();
            // 
            // botonera
            // 
            this.botonera.AutoSize = false;
            this.botonera.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btVerSemana,
            this.btVerDia});
            this.botonera.Location = new System.Drawing.Point(0, 0);
            this.botonera.Name = "botonera";
            this.botonera.Size = new System.Drawing.Size(859, 32);
            this.botonera.TabIndex = 0;
            this.botonera.Text = "toolStrip1";
            // 
            // btVerSemana
            // 
            this.btVerSemana.AutoSize = false;
            this.btVerSemana.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btVerSemana.Image = global::gControls.Properties.Resources.Semana;
            this.btVerSemana.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btVerSemana.Name = "btVerSemana";
            this.btVerSemana.Size = new System.Drawing.Size(32, 32);
            this.btVerSemana.ToolTipText = "Muestra la semana en curso en el calendario";
            // 
            // btVerDia
            // 
            this.btVerDia.AutoSize = false;
            this.btVerDia.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btVerDia.Image = global::gControls.Properties.Resources.Dia;
            this.btVerDia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btVerDia.Name = "btVerDia";
            this.btVerDia.Size = new System.Drawing.Size(32, 32);
            this.btVerDia.ToolTipText = "Muestra el momento actual en el calendario";
            // 
            // calendar1
            // 
            this.calendar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendar1.Font = new System.Drawing.Font("Segoe UI", 9F);
            calendarHighlightRange1.DayOfWeek = System.DayOfWeek.Monday;
            calendarHighlightRange1.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange1.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange2.DayOfWeek = System.DayOfWeek.Tuesday;
            calendarHighlightRange2.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange2.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange3.DayOfWeek = System.DayOfWeek.Wednesday;
            calendarHighlightRange3.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange3.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange4.DayOfWeek = System.DayOfWeek.Thursday;
            calendarHighlightRange4.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange4.StartTime = System.TimeSpan.Parse("08:00:00");
            calendarHighlightRange5.DayOfWeek = System.DayOfWeek.Friday;
            calendarHighlightRange5.EndTime = System.TimeSpan.Parse("17:00:00");
            calendarHighlightRange5.StartTime = System.TimeSpan.Parse("08:00:00");
            this.calendar1.HighlightRanges = new System.Windows.Forms.Calendar.CalendarHighlightRange[] {
        calendarHighlightRange1,
        calendarHighlightRange2,
        calendarHighlightRange3,
        calendarHighlightRange4,
        calendarHighlightRange5};
            this.calendar1.Location = new System.Drawing.Point(0, 32);
            this.calendar1.Name = "calendar1";
            this.calendar1.Size = new System.Drawing.Size(859, 570);
            this.calendar1.TabIndex = 1;
            this.calendar1.Text = "calendar1";
            // 
            // monthView1
            // 
            this.monthView1.ArrowsColor = System.Drawing.SystemColors.Window;
            this.monthView1.ArrowsSelectedColor = System.Drawing.Color.Gold;
            this.monthView1.DayBackgroundColor = System.Drawing.Color.Empty;
            this.monthView1.DayGrayedText = System.Drawing.SystemColors.GrayText;
            this.monthView1.DaySelectedBackgroundColor = System.Drawing.SystemColors.Highlight;
            this.monthView1.DaySelectedColor = System.Drawing.SystemColors.WindowText;
            this.monthView1.DaySelectedTextColor = System.Drawing.SystemColors.HighlightText;
            this.monthView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.monthView1.ItemPadding = new System.Windows.Forms.Padding(2);
            this.monthView1.Location = new System.Drawing.Point(0, 32);
            this.monthView1.MonthTitleColor = System.Drawing.SystemColors.ActiveCaption;
            this.monthView1.MonthTitleColorInactive = System.Drawing.SystemColors.InactiveCaption;
            this.monthView1.MonthTitleTextColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.monthView1.MonthTitleTextColorInactive = System.Drawing.SystemColors.InactiveCaptionText;
            this.monthView1.Name = "monthView1";
            this.monthView1.Size = new System.Drawing.Size(213, 570);
            this.monthView1.TabIndex = 2;
            this.monthView1.Text = "monthView1";
            this.monthView1.TodayBorderColor = System.Drawing.Color.Maroon;
            // 
            // EditorAvisos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.monthView1);
            this.Controls.Add(this.calendar1);
            this.Controls.Add(this.botonera);
            this.Name = "EditorAvisos";
            this.Size = new System.Drawing.Size(859, 602);
            this.botonera.ResumeLayout(false);
            this.botonera.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip botonera;
        private System.Windows.Forms.ToolStripButton btVerSemana;
        private System.Windows.Forms.ToolStripButton btVerDia;
        private System.Windows.Forms.Calendar.Calendar calendar1;
        private System.Windows.Forms.Calendar.MonthView monthView1;
    }
}
