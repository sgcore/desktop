﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    public class toolStripWaterMarkTextBox :ToolStripControlHost
    {
        // Call the base constructor passing in a MonthCalendar instance. 
        public toolStripWaterMarkTextBox() : base(new WaterMarkTextBox()) { }

        public WaterMarkTextBox WaterMarkTextBoxControl
		{
			get
			{
                return Control as WaterMarkTextBox;
			}
		}

		// Expose the MonthCalendar.FirstDayOfWeek as a property. 
		
		// Subscribe and unsubscribe the control events you wish to expose. 
		protected override void OnSubscribeControlEvents(Control c)
		{
			// Call the base so the base events are connected. 
			base.OnSubscribeControlEvents(c);

			// Cast the control to a MonthCalendar control.
            WaterMarkTextBox WaterMarkTextBoxControl = (WaterMarkTextBox)c;

			// Add the event.
            WaterMarkTextBoxControl.TextChanged +=
				new EventHandler(OnDateChanged);
		}

		protected override void OnUnsubscribeControlEvents(Control c)
		{
			// Call the base method so the basic events are unsubscribed. 
			base.OnUnsubscribeControlEvents(c);

			// Cast the control to a MonthCalendar control.
            WaterMarkTextBox WaterMarkTextBoxControl = (WaterMarkTextBox)c;

			// Remove the event.
            WaterMarkTextBoxControl.TextChanged  -=
				new EventHandler (OnDateChanged);
		}
        public string WaterMarkText
        {
            get
            {
                return WaterMarkTextBoxControl.WaterMarkText;
            }
            set { WaterMarkTextBoxControl.WaterMarkText = value; }
        }
        public Color WaterMarkColor
        {
            get { return WaterMarkTextBoxControl.WaterMarkColor ; }
            set
            {
                WaterMarkTextBoxControl.WaterMarkColor = value;
            }
        }

		// Declare the DateChanged event. 
		public event EventHandler  WTextChanged;

		// Raise the DateChanged event. 
        private void OnDateChanged(object sender, EventArgs e)
		{
            if (WTextChanged != null)
			{
                WTextChanged(this, e);
			}
		}
    }
}
