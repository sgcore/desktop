﻿using Microsoft.Reporting.WinForms;
using System.Windows.Forms;

namespace gControls.Informes
{
    public partial class Preimpresa : UserControl
    {
        public Preimpresa()
        {
            InitializeComponent();

        }
        public void SetMovimiento(gManager.gcMovimiento m)
        {
            gcObjetoMovidoBindingSource.DataSource = m.ObjetosMovidos;
            LocalReport localReport = reportViewer1.LocalReport;
            ////
            //Limpiemos el DataSource del informe
            //reportViewer1.LocalReport.DataSources.Clear();
            //
            //Establezcamos los parámetros que enviaremos al reporte
            //recuerde que son dos para el titulo del reporte y para el nombre de la empresa
            //
            ReportParameter[] parameters = new ReportParameter[3];
            parameters[0] = new ReportParameter("paraDestinatario", m.DescripcionDestinatario);
            parameters[1] = new ReportParameter("paraFecha", m.Fecha.ToShortDateString());
            parameters[2] = new ReportParameter("paraTotal", m.TotalCotizado.ToString("$0.00"));

            //
            //Establezcamos la lista como Datasource del informe
            //
            //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Encabezado", Invoice));
            //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Detalle", Detail));
            //
            //Enviemos la lista de parametros
            //

            localReport.SetParameters(parameters);
            //
            //Hagamos un refresh al reportViewer
            //
            reportViewer1.RefreshReport();
            

        }
    }
}
