﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    public class HotKeys
    {
        List<keyreg> _list = new List<keyreg>();
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        
        public enum KeyModifier
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            WinKey = 8
        }
        IntPtr _p;
        public HotKeys(Form form)
        {
            _p = form.Handle;
            form.Deactivate += delegate(object o, EventArgs e)
            {
                for(int i=0;i<_list.Count;i++)
                    UnregisterHotKey(_p, i); 
            };
            form.Activated += delegate(object o, EventArgs e)
            {
                foreach(keyreg kr in _list)
                    RegisterHotKey(_p, kr.Id, (int)kr.KeyMod, kr.Key.GetHashCode());
            };

        }
        public void perform(int id)
        {
            _list[id].Procedimiento(null, null);
        }
        public bool add(KeyModifier km, Keys k, EventHandler proc)
        {
            foreach (keyreg ck in _list)
            {
                if (ck.Key == k && ck.KeyMod == km) return false;
            }
            keyreg kr = new keyreg();
            kr.Id= _list.Count;
            kr.Key = k;
            kr.KeyMod = km;
            kr.Procedimiento = proc;
            _list.Add(kr);
            return true;
            //RegisterHotKey(_p, id, (int)km, k.GetHashCode());
        }
        public bool add(Shortcut sc, EventHandler proc)
        {
            return add(getMod(sc), (Keys)((byte)sc),proc);
        }
        private KeyModifier getMod(Shortcut s)
        {
            if (s.ToString().StartsWith("Shi")) return KeyModifier.Shift;
            if (s.ToString().StartsWith("Ctr")) return KeyModifier.Control;
            if (s.ToString().StartsWith("Alt")) return KeyModifier.Alt;
             return KeyModifier.None;
            
        }
        public bool remove(KeyModifier km, Keys k)
        {
            for(int i=0;i<_list.Count;i++)
            {
                var ck = _list[i];
                if (ck.Key == k && ck.KeyMod == km)
                {
                    _list.RemoveAt(i);
                    UnregisterHotKey(_p, i);
                    return true;
                }
            }
            return false;
        }
        private class keyreg
        {
            public KeyModifier KeyMod { get; set; }
            public Keys Key { get; set; }
            public EventHandler Procedimiento { get; set; }
            public int Id { get; set; }
        }
    }
}
