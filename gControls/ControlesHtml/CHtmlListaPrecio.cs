﻿using gManager.Filtros;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Windows.Forms;


namespace gControls.ControlesHtml
{
    public partial class CHtmlListaPrecio : htmlBase
    {
        ToolStripMenuItem _menu = new ToolStripMenuItem("Configuracion",Properties.Resources.Configuracion);
        ToolStripMenuItem _mnuDolar = new ToolStripMenuItem("Precio en dolares");
        ToolStripMenuItem _mnuStockNumerico = new ToolStripMenuItem("Mostrar cantidades");
        ToolStripMenuItem _mnuStockColores = new ToolStripMenuItem("Mostrar estimados");
        ToolStripMenuItem _mnuStockOculto = new ToolStripMenuItem("Ocultar Stock");
        FiltroProducto _filtro;
        bool _dolar = false;
        int _stock = -1;
        MenuItem _mdolar = new MenuItem("Precio en dolares");
        public CHtmlListaPrecio(FiltroProducto filtro)
        {
            InitializeComponent();
            setStyle(Properties.Resources.styles);
            _filtro = filtro;
            string cfg = Properties.Settings.Default.cfgListaPrecio;
            if (String.IsNullOrEmpty(cfg)) cfg = "{\"stock\":1,\"dolar\":false}";

            JObject json = null;
            try
            {
                json= JObject.Parse(cfg);
            }
            catch
            {

            }
            if(json != null)
            {
                bool valid = false;
                JToken t;
                valid = json.TryGetValue("stock", out t);
                if (valid)
                {
                    _stock = t.ToObject<int>();
                }

                valid = json.TryGetValue("dolar", out t);
                if (valid)
                {
                    _dolar = t.ToObject<bool>();
                }
            }
           


            _menu.DropDownItems.Add(_mnuDolar);
            var mnu = _menu.DropDownItems.Add("Mostrar Stock") as ToolStripMenuItem;
            mnu.DropDownItems.Add(_mnuStockNumerico);
            mnu.DropDownItems.Add(_mnuStockColores);
            mnu.DropDownItems.Add(_mnuStockOculto);
            addMenu(_menu);

            _mnuStockColores.Click += (o, e) =>
            {
                if (_stock != 1)
                {
                    _stock = 1;
                    saveConfig();
                }
                updateMenu();
            };
            _mnuStockNumerico.Click += (o, e) =>
            {
                if (_stock != 0)
                {
                    _stock = 0;
                    saveConfig();
                }
                updateMenu();
            };
            _mnuStockOculto.Click += (o, e) =>
            {
                if (_stock != -1)
                {
                    _stock = -1;
                    saveConfig();
                }
                updateMenu();
            };
            _mnuDolar.Click += (o, e) =>
            {
                _dolar = !_dolar;
                saveConfig();
                updateMenu();
            };
           
            updateMenu();

        }
        private void updateMenu()
        {
            _mnuDolar.Checked = _dolar;
            _mnuStockColores.Checked = _stock == 1;
            _mnuStockNumerico.Checked = _stock == 0;
            _mnuStockOculto.Checked = _stock == -1;
            var lista = new ControlesHtml.htmlObjects.htmlListaPrecios(_filtro, _dolar, _stock);
            setContent(lista.ToString());
        }
        private void saveConfig()
        {
            Properties.Settings.Default.cfgListaPrecio = "{\"stock\":" + _stock.ToString() + ",\"dolar\":" + _dolar.ToString().ToLower() +"}";
            Properties.Settings.Default.Save();
        }
    }
}
