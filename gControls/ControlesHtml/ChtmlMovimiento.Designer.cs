﻿namespace gControls.ControlesHtml
{
    partial class ChtmlMovimiento
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.botonAccion1 = new gControls.BotonAccion();
            this.pInfo = new gControls.ControlesHtml.htmlBase();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btResumen = new gControls.BotonAccion();
            this.btGuardar = new gControls.BotonAccion();
            this.btTicket = new gControls.BotonAccion();
            this.btTerminar = new gControls.BotonAccion();
            this.btEditar = new gControls.BotonAccion();
            this.btLimpiarPagos = new gControls.BotonAccion();
            this.btEliminar = new gControls.BotonAccion();
            this.botonAccion3 = new gControls.BotonAccion();
            this.panelAgregarOM1 = new gControls.Paneles.PanelAgregarOM();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // botonAccion1
            // 
            this.botonAccion1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonAccion1.Checked = false;
            this.botonAccion1.Dock = System.Windows.Forms.DockStyle.Left;
            this.botonAccion1.FormContainer = null;
            this.botonAccion1.GlobalHotKey = false;
            this.botonAccion1.HacerInvisibleAlDeshabilitar = false;
            this.botonAccion1.HotKey = System.Windows.Forms.Shortcut.None;
            this.botonAccion1.Image = global::gControls.Properties.Resources.Caja;
            this.botonAccion1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.botonAccion1.Location = new System.Drawing.Point(68, 0);
            this.botonAccion1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.botonAccion1.Name = "botonAccion1";
            this.botonAccion1.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.botonAccion1.RegistradoEnForm = false;
            this.botonAccion1.RequiereKey = true;
            this.botonAccion1.Size = new System.Drawing.Size(68, 74);
            this.botonAccion1.TabIndex = 15;
            this.botonAccion1.Text = "Actual";
            this.botonAccion1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.botonAccion1.Titulo = "Caja Actual";
            this.botonAccion1.ToolTipDescripcion = "Muestra todos los movimientos realizados en la caja actual. (Shift +click para el" +
    "egir)";
            this.botonAccion1.UseVisualStyleBackColor = true;
            this.botonAccion1.Click += new System.EventHandler(this.btCajaActual);
            // 
            // pInfo
            // 
            this.pInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pInfo.Location = new System.Drawing.Point(0, 0);
            this.pInfo.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.pInfo.Name = "pInfo";
            this.pInfo.Size = new System.Drawing.Size(1244, 799);
            this.pInfo.TabIndex = 17;
            this.pInfo.Load += new System.EventHandler(this.pInfo_Load);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btResumen);
            this.panel1.Controls.Add(this.btGuardar);
            this.panel1.Controls.Add(this.btTicket);
            this.panel1.Controls.Add(this.btTerminar);
            this.panel1.Controls.Add(this.btEditar);
            this.panel1.Controls.Add(this.btLimpiarPagos);
            this.panel1.Controls.Add(this.btEliminar);
            this.panel1.Controls.Add(this.botonAccion1);
            this.panel1.Controls.Add(this.botonAccion3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 834);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1244, 74);
            this.panel1.TabIndex = 18;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btResumen
            // 
            this.btResumen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btResumen.Checked = false;
            this.btResumen.Dock = System.Windows.Forms.DockStyle.Left;
            this.btResumen.FormContainer = null;
            this.btResumen.GlobalHotKey = false;
            this.btResumen.HacerInvisibleAlDeshabilitar = false;
            this.btResumen.HotKey = System.Windows.Forms.Shortcut.None;
            this.btResumen.Image = global::gControls.Properties.Resources.Venta;
            this.btResumen.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btResumen.Location = new System.Drawing.Point(136, 0);
            this.btResumen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btResumen.Name = "btResumen";
            this.btResumen.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btResumen.RegistradoEnForm = false;
            this.btResumen.RequiereKey = true;
            this.btResumen.Size = new System.Drawing.Size(81, 74);
            this.btResumen.TabIndex = 24;
            this.btResumen.Text = "ventas";
            this.btResumen.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btResumen.Titulo = "Resumen de ventas";
            this.btResumen.ToolTipDescripcion = "Muestra un resumen de ventas por categorias de la caja actual. (shift + click par" +
    "a elegir)";
            this.btResumen.UseVisualStyleBackColor = true;
            this.btResumen.Click += new System.EventHandler(this.btResumen_Click);
            // 
            // btGuardar
            // 
            this.btGuardar.AutoSize = true;
            this.btGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btGuardar.Checked = false;
            this.btGuardar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGuardar.FormContainer = null;
            this.btGuardar.GlobalHotKey = false;
            this.btGuardar.HacerInvisibleAlDeshabilitar = false;
            this.btGuardar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btGuardar.Image = global::gControls.Properties.Resources.Guardar;
            this.btGuardar.Location = new System.Drawing.Point(509, 0);
            this.btGuardar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btGuardar.Name = "btGuardar";
            this.btGuardar.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btGuardar.RegistradoEnForm = false;
            this.btGuardar.RequiereKey = false;
            this.btGuardar.Size = new System.Drawing.Size(130, 74);
            this.btGuardar.TabIndex = 22;
            this.btGuardar.Text = "Guardar";
            this.btGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btGuardar.Titulo = "Guardar Formulario";
            this.btGuardar.ToolTipDescripcion = "Si el movimiento aun no esta guardado no puede agregar productos o pagos.";
            this.btGuardar.UseVisualStyleBackColor = true;
            this.btGuardar.Click += new System.EventHandler(this.btGuardar_Click);
            // 
            // btTicket
            // 
            this.btTicket.AutoSize = true;
            this.btTicket.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btTicket.Checked = false;
            this.btTicket.Dock = System.Windows.Forms.DockStyle.Right;
            this.btTicket.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTicket.FormContainer = null;
            this.btTicket.GlobalHotKey = false;
            this.btTicket.HacerInvisibleAlDeshabilitar = false;
            this.btTicket.HotKey = System.Windows.Forms.Shortcut.None;
            this.btTicket.Image = global::gControls.Properties.Resources.Ticket;
            this.btTicket.Location = new System.Drawing.Point(639, 0);
            this.btTicket.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btTicket.Name = "btTicket";
            this.btTicket.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btTicket.RegistradoEnForm = false;
            this.btTicket.RequiereKey = false;
            this.btTicket.Size = new System.Drawing.Size(105, 74);
            this.btTicket.TabIndex = 21;
            this.btTicket.Text = "Ticket";
            this.btTicket.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btTicket.Titulo = "Imprimir ticket";
            this.btTicket.ToolTipDescripcion = "Imprime el movimiento en un ticket. Debe configurar en cual impresora.";
            this.btTicket.UseVisualStyleBackColor = true;
            this.btTicket.Click += new System.EventHandler(this.btTicket_Click);
            // 
            // btTerminar
            // 
            this.btTerminar.AutoSize = true;
            this.btTerminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btTerminar.Checked = false;
            this.btTerminar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btTerminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTerminar.FormContainer = null;
            this.btTerminar.GlobalHotKey = false;
            this.btTerminar.HacerInvisibleAlDeshabilitar = false;
            this.btTerminar.HotKey = System.Windows.Forms.Shortcut.CtrlF10;
            this.btTerminar.Image = global::gControls.Properties.Resources.Terminar;
            this.btTerminar.Location = new System.Drawing.Point(744, 0);
            this.btTerminar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btTerminar.Name = "btTerminar";
            this.btTerminar.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btTerminar.RegistradoEnForm = false;
            this.btTerminar.RequiereKey = false;
            this.btTerminar.Size = new System.Drawing.Size(141, 74);
            this.btTerminar.TabIndex = 18;
            this.btTerminar.Text = "Terminar";
            this.btTerminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btTerminar.Titulo = "Terminar";
            this.btTerminar.ToolTipDescripcion = "Finaliza la operación actual.";
            this.btTerminar.UseVisualStyleBackColor = true;
            this.btTerminar.Click += new System.EventHandler(this.btTerminar_Click);
            // 
            // btEditar
            // 
            this.btEditar.AutoSize = true;
            this.btEditar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEditar.Checked = false;
            this.btEditar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditar.FormContainer = null;
            this.btEditar.GlobalHotKey = false;
            this.btEditar.HacerInvisibleAlDeshabilitar = false;
            this.btEditar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btEditar.Image = global::gControls.Properties.Resources.Editar;
            this.btEditar.Location = new System.Drawing.Point(885, 0);
            this.btEditar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btEditar.Name = "btEditar";
            this.btEditar.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btEditar.RegistradoEnForm = false;
            this.btEditar.RequiereKey = false;
            this.btEditar.Size = new System.Drawing.Size(104, 74);
            this.btEditar.TabIndex = 19;
            this.btEditar.Text = "Editar";
            this.btEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btEditar.Titulo = "Editar Movimiento";
            this.btEditar.ToolTipDescripcion = "Habilita la edición para modificar el movimiento.";
            this.btEditar.UseVisualStyleBackColor = true;
            this.btEditar.Click += new System.EventHandler(this.btEditar_Click);
            // 
            // btLimpiarPagos
            // 
            this.btLimpiarPagos.AutoSize = true;
            this.btLimpiarPagos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btLimpiarPagos.Checked = false;
            this.btLimpiarPagos.Dock = System.Windows.Forms.DockStyle.Right;
            this.btLimpiarPagos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLimpiarPagos.FormContainer = null;
            this.btLimpiarPagos.GlobalHotKey = false;
            this.btLimpiarPagos.HacerInvisibleAlDeshabilitar = false;
            this.btLimpiarPagos.HotKey = System.Windows.Forms.Shortcut.None;
            this.btLimpiarPagos.Image = global::gControls.Properties.Resources.Reiniciar24;
            this.btLimpiarPagos.Location = new System.Drawing.Point(989, 0);
            this.btLimpiarPagos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btLimpiarPagos.Name = "btLimpiarPagos";
            this.btLimpiarPagos.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btLimpiarPagos.RegistradoEnForm = false;
            this.btLimpiarPagos.RequiereKey = false;
            this.btLimpiarPagos.Size = new System.Drawing.Size(123, 74);
            this.btLimpiarPagos.TabIndex = 23;
            this.btLimpiarPagos.Text = "Limpiar";
            this.btLimpiarPagos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btLimpiarPagos.Titulo = "Limpiar Pagos";
            this.btLimpiarPagos.ToolTipDescripcion = "Elimina los pagos realizados.";
            this.btLimpiarPagos.UseVisualStyleBackColor = true;
            this.btLimpiarPagos.Click += new System.EventHandler(this.btLimpiarPagos_Click);
            // 
            // btEliminar
            // 
            this.btEliminar.AutoSize = true;
            this.btEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEliminar.Checked = false;
            this.btEliminar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEliminar.FormContainer = null;
            this.btEliminar.GlobalHotKey = false;
            this.btEliminar.HacerInvisibleAlDeshabilitar = false;
            this.btEliminar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btEliminar.Image = global::gControls.Properties.Resources.Eliminar;
            this.btEliminar.Location = new System.Drawing.Point(1112, 0);
            this.btEliminar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btEliminar.Name = "btEliminar";
            this.btEliminar.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btEliminar.RegistradoEnForm = false;
            this.btEliminar.RequiereKey = false;
            this.btEliminar.Size = new System.Drawing.Size(132, 74);
            this.btEliminar.TabIndex = 20;
            this.btEliminar.Text = "Eliminar";
            this.btEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btEliminar.Titulo = "Eliminar";
            this.btEliminar.ToolTipDescripcion = "Borra completamente el Movimiento.";
            this.btEliminar.UseVisualStyleBackColor = true;
            this.btEliminar.Click += new System.EventHandler(this.btEliminar_Click);
            // 
            // botonAccion3
            // 
            this.botonAccion3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonAccion3.Checked = false;
            this.botonAccion3.Dock = System.Windows.Forms.DockStyle.Left;
            this.botonAccion3.FormContainer = null;
            this.botonAccion3.GlobalHotKey = false;
            this.botonAccion3.HacerInvisibleAlDeshabilitar = false;
            this.botonAccion3.HotKey = System.Windows.Forms.Shortcut.None;
            this.botonAccion3.Image = global::gControls.Properties.Resources.Cerrar24;
            this.botonAccion3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.botonAccion3.Location = new System.Drawing.Point(0, 0);
            this.botonAccion3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.botonAccion3.Name = "botonAccion3";
            this.botonAccion3.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.botonAccion3.RegistradoEnForm = false;
            this.botonAccion3.RequiereKey = false;
            this.botonAccion3.Size = new System.Drawing.Size(68, 74);
            this.botonAccion3.TabIndex = 17;
            this.botonAccion3.Text = "Inicio";
            this.botonAccion3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.botonAccion3.Titulo = "Pagina principal";
            this.botonAccion3.ToolTipDescripcion = "Muestra la pagina principal";
            this.botonAccion3.UseVisualStyleBackColor = true;
            this.botonAccion3.Click += new System.EventHandler(this.botonAccion3_Click);
            // 
            // panelAgregarOM1
            // 
            this.panelAgregarOM1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAgregarOM1.Location = new System.Drawing.Point(0, 799);
            this.panelAgregarOM1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.panelAgregarOM1.Name = "panelAgregarOM1";
            this.panelAgregarOM1.Size = new System.Drawing.Size(1244, 35);
            this.panelAgregarOM1.TabIndex = 19;
            // 
            // ChtmlMovimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pInfo);
            this.Controls.Add(this.panelAgregarOM1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ChtmlMovimiento";
            this.Size = new System.Drawing.Size(1244, 908);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private BotonAccion botonAccion1;
        private htmlBase pInfo;
        private System.Windows.Forms.Panel panel1;
        private BotonAccion botonAccion3;
        private BotonAccion btGuardar;
        private BotonAccion btTicket;
        private BotonAccion btTerminar;
        private BotonAccion btEditar;
        private BotonAccion btLimpiarPagos;
        private BotonAccion btEliminar;
        private Paneles.PanelAgregarOM panelAgregarOM1;
        private BotonAccion btResumen;
    }
}
