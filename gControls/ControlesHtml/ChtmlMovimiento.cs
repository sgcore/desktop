﻿using gControls.ControlesHtml.htmlObjects;
using gLogger;
using gManager;
using PdfSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace gControls.ControlesHtml
{
    public partial class ChtmlMovimiento : UserControl
    {
        gcMovimiento _mov;
        List<gcMovimiento> _movs = new List<gcMovimiento>();
        List<gcCaja> _cajas = new List<gcCaja>();
        public ChtmlMovimiento()
        {
            InitializeComponent();
            pInfo.setStyle(html.GetStylesheet("StyleSheet"));
            CoreManager.Singlenton.MovimientoSeleccionado += Singlenton_MovimientoSeleccionado;
            pInfo.addAction("remom", (v) =>
            {
                _mov.removeObjetoMovido(int.Parse(v));
                CoreManager.Singlenton.seleccionarMovimiento(_mov);
            });
            pInfo.addAction("editom", (v) =>
            {
                gcObjetoMovido om = _mov.getObjetoMovidoById(int.Parse(v));
                om = Editar.EditarFraccion(om);
                _mov.reloadMontos();
                CoreManager.Singlenton.seleccionarMovimiento(_mov);
            });
            pInfo.addAction("selmov", (v) =>
            {
                _mov = CoreManager.Singlenton.getMovimientoById(int.Parse(v));
                CoreManager.Singlenton.seleccionarMovimiento(_mov);
            });
            pInfo.addAction("moveditobs", (v) =>
            {
                string last = _mov.Observaciones;
                string t = Editar.EditarTexto("Observaciones del movimiento", last);
                if (last != t)
                {
                    _mov.Observaciones = t;
                    _mov.saveMe();
                    CoreManager.Singlenton.seleccionarMovimiento(_mov);
                }
            });
            pInfo.addAction("moveditfactnum", (v) =>
            {
                decimal last = (decimal)_mov.FacturaNumero;
                decimal t = Editar.EditarDecimal(last ,"Numero de factura",0);
                if (last != t)
                {
                    _mov.FacturaNumero = (int)t;
                    _mov.saveMe();
                    CoreManager.Singlenton.seleccionarMovimiento(_mov);
                }
            });
            pInfo.addAction("moveditcoti", (v) =>
            {
                decimal last = (decimal)_mov.Cotizacion;
                decimal t = Editar.EditarDecimal(last, "Cotizacion", 2, 1);
                if (last != t)
                {
                    _mov.setMonedaYCotizacion(gcObjetoMovido.MonedaEnum.Dolar, t);
                    _mov.saveMe();
                    CoreManager.Singlenton.seleccionarMovimiento(_mov);
                }
            });
            // Pagos
            pInfo.addAction("movpagodescuento", (v) =>
            {
                decimal monto = Editar.EditarDecimal(0, "Agregar descuento", 2, 0, _mov.MaximoDescuentoPermitido);

                if (monto > 0)
                {
                    gcPago p = new gcPago(monto, gcPago.TipoPago.Descuento);
                    crearPago(p);

                }
            });
            pInfo.addAction("movpagovuelto", (v) =>
            {
                gcPago p = null;
                if (_mov.Diferencia < 0) p = new gcPago(-_mov.Diferencia, gcPago.TipoPago.Vuelto);
                gcPago np = Editar.EditarEfectivo(_mov.PagosEnVueltos, "Vuelto", p, null);
                if (np != null)
                {

                    crearPago(np);

                }
            });
            pInfo.addAction("movpagoefectivo", (v) =>
            {
                gcPago p = new gcPago(_mov.TotalMovimiento, gcPago.TipoPago.Efectivo);
                gcPago np = Editar.EditarEfectivo(_mov.PagosEnEfectivo, "Pago en efectivo", p, null);
                if (np != null)
                {

                    crearPago(np);

                }
            });
            pInfo.addAction("movpagotargeta", (v) =>
            {
               
                CoreManager.Singlenton.seleccionarMovimiento(_mov);
            });
            pInfo.addAction("movpagocuenta", (v) =>
            {
                addPagoCuenta();
            });

        }
        public void crearPago(gcPago p)
        {
            switch (p.Tipo)
            {
                case gcPago.TipoPago.Efectivo:
                case gcPago.TipoPago.Vuelto:
                    _mov.addPagoUnique(p);
                    break;
                case gcPago.TipoPago.Tarjeta:
                case gcPago.TipoPago.Cheque:
                case gcPago.TipoPago.Descuento:
                    _mov.addPago(p);
                    break;
                case gcPago.TipoPago.Cuenta:
                    _mov.addPagoCuenta(p.Monto);
                    break;
            }
            CoreManager.Singlenton.seleccionarMovimiento(_mov);
        }
        private void addPagoCuenta(bool preguntar = true)
        {
            if (_mov.Destinatario == null)
            {
                logger.Singlenton.addErrorMsg("No puede agregar un pago si no eligió el destinatario.");
                return;
            }
            foreach (gcPago p in _mov.Pagos)
            {
                if (p.Tipo == gcPago.TipoPago.Cuenta)
                {
                    logger.Singlenton.addWarningMsg("En este movimiento ya existe un pago de cuenta corriente de " + p.Monto.ToString("$0.00") + ".\n Por favor eliminelo y vuelva a crearlo con el monto correcto.");
                    return;
                }
            }
            DialogResult dr = DialogResult.Cancel;
            if (_mov.Diferencia > 0)
            {
                dr = MessageBox.Show("¿Desea agregar " + _mov.Diferencia.ToString("$0.00") + " a la deuda de " + _mov.Destinatario.Nombre + "?", "Cuenta Corriente", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else if (_mov.Diferencia < 0)
            {
                dr = MessageBox.Show("¿Desea retirar " + (-_mov.Diferencia).ToString("$0.00") + " de la cuenta de " + _mov.Destinatario.Nombre + "?", "Cuenta Corriente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }
            if (preguntar && dr == DialogResult.Yes)
            {

                _mov.addPagoCuenta(_mov.Diferencia);

            }
            else if (!preguntar)
            {
                _mov.addPagoCuenta(_mov.Diferencia);
            }
            CoreManager.Singlenton.seleccionarMovimiento(_mov);
        }

        private void Singlenton_MovimientoSeleccionado(gcMovimiento m)
        {
            _mov = m;
            panelAgregarOM1.Visible = _mov != null && _mov.SePuedeAgregarOEliminarOM;
            panelAgregarOM1.setMovimiento(_mov);
            // show products
            
            if (_mov != null)
            {
                if (!m.Terminado && m.EsMovimientoMercaderia)
                {
                    Acciones.BuscarProductos(null, null);
                }
                pInfo.Text = new HtmlMovimiento(m).ToString();

                if (_mov.Id < 1 && Properties.Settings.Default.cfgAutoguardarAlCrearMov)
                { _mov.saveMe(); reload(); return; }


                btEliminar.Visible = _mov.SePuedeEliminar;
                btEditar.Visible = _mov.SePuedeEditar;

               btTerminar.Visible = _mov.SePuedeTerminar;
                if (btTerminar.Visible) btTerminar.BackgroundImage = Properties.Resources.Activo;
                btTicket.Visible = _mov.Terminado;
                btGuardar.Visible = _mov.Id < 1;
                btLimpiarPagos.Visible = !_mov.Terminado && _mov.TotalPagado > 0;

            }
            else
            {

                btEliminar.Visible = false;
                btTerminar.Visible = false;
                btTicket.Visible = false;
                btEditar.Visible = false;
                btGuardar.Visible = false;
                btLimpiarPagos.Visible = false;



            }
        }
      

        private void btCajaActual(object sender, EventArgs e)
        {
            if ((ModifierKeys & Keys.Shift) != 0)
            {
                _cajas = buscar.BuscarCajas();
            }
            if (_cajas == null || _cajas.Count == 0)
            {
                _cajas = new List<gcCaja>(new gcCaja[] { CoreManager.Singlenton.CajaActual });
            }
            showCajas();
        }
        private void showCajas()
        {
            Singlenton_MovimientoSeleccionado(null);
            if (_cajas != null && _cajas.Count > 0)
            {
                _movs.Clear();
                foreach (gcCaja c in _cajas)
                {
                    _movs.AddRange(c.Movimientos);
                }
                string t = new htmlMovimientos(_movs).ToString();
                pInfo.Text = t;
            } else
            {
                pInfo.Text = "No selecciono ninguna caja";
            }
          
        }

        private void botonAccion3_Click(object sender, EventArgs e)
        {
            CoreManager.Singlenton.GenerarInformeHtml(null);
        }
        private void reload()
        {
            CoreManager.Singlenton.seleccionarMovimiento(_mov);
        }
        private void btEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Deséa eliminar este Movimiento?", "Eliminar definitivamente", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes
               && _mov.deleteMe())
            {

                _mov = null;
                pInfo.Text = "";
                CoreManager.Singlenton.GenerarInformeHtml(null);
                Singlenton_MovimientoSeleccionado(_mov);
                
            }
        }

        private void btTicket_Click(object sender, EventArgs e)
        {
            Impresiones.ImprimirTicket(_mov, ModifierKeys == Keys.Control);
        }

        private void btTerminar_Click(object sender, EventArgs e)
        {
            if (_mov != null)
            {
                ver.VerAsistenteCompra(_mov);
                _mov.TerminarOperacion();
            }
            reload();
        }

        private void btEditar_Click(object sender, EventArgs e)
        {
            if (_mov != null) _mov.EditarMovimiento();
            reload();
        }

        private void btLimpiarPagos_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Deséa eliminar todos los pagos?", "Limpiar los pagos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _mov.clearPagos();
                CoreManager.Singlenton.seleccionarMovimiento(_mov);
            }
        }

        private void btGuardar_Click(object sender, EventArgs e)
        {
            if (_mov != null) _mov.saveMe();
            reload();
        }

        private void btResumen_Click(object sender, EventArgs e)
        {
            Singlenton_MovimientoSeleccionado(null);
            if ((ModifierKeys & Keys.Shift) != 0)
            {
                _cajas = buscar.BuscarCajas();
            }
            if (_cajas == null || _cajas.Count == 0)
            {
                _cajas = new List<gcCaja>(new gcCaja[] { CoreManager.Singlenton.CajaActual });
            }
            var cr = new htmlResumenCategorias(_cajas);
            pInfo.Text = cr.ToString();

        }

        private void pInfo_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
