﻿using gManager;
using gManager.html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gControls.ControlesHtml.htmlObjects
{
    public class htmlListaPrecios : htmlObject
    {
        List<ResumenBase> _resumen;
        string titulo = "Lista de precios";
        int _stock = -1;
        bool _dolar = false;
        public htmlListaPrecios(gManager.Filtros.FiltroProducto f, bool dolar, int stock)
        {
            _resumen = CoreManager.Singlenton.ListaDePrecios(f);
            _stock = stock;
            _dolar = dolar;
            init();
        }
        protected void addProductoResumen(ProductoResumen o)
        {
            string stadd = "";
            htag tstk = new htag().className("normal");
            switch (_stock)
            {
                case 0:
                    stadd = tstk.content(Utils.UnidadMedida(o.Producto.Metrica, o.Cantidad)).contentMetrica().toTd();
                    break;
                case 1:
                    
                    if(!o.Producto.HayStock)
                    {
                        stadd = tstk.content("Sin Stock").className("critical").toTd();
                    }else if (o.Producto.HayPocoStock)
                    {
                        stadd = tstk.content("Poco Stock").className("low").toTd();
                    }
                    else if (o.Producto.HayMuchoStock)
                    {
                        stadd = tstk.content("En Stock").className("high").toTd();
                    }
                    break;
                        
            }
            addRow(
                new htag().content((String.IsNullOrEmpty(o.Codigo) ? "------" : o.Codigo)).className("normal small").toTd()
                + new htag().content(o.Nombre).width(300).className("normal small").toTd()
                + stadd
                + new htag().content((_dolar ? o.Producto.Dolar.ToString("U$S 0.00") : o.Total.ToString("$0.00"))).className("normal").toTd()
                );
        }
        protected void addHeaderResument()
        {
            addHeader(new string[] {"Código"
                , "Producto"
                , "Stock"
                , "Precio"});
        }

        private void subtree(string name, CategoriaResumen cat)
        {
            if (cat.SubObjetos.Count > 0)
            {
                addBreadcrum(name + "@" + cat.Nombre);
                openTable();
                addHeaderResument();
                foreach (ProductoResumen o in cat.SubObjetos)
                {
                    addProductoResumen(o);
                }
                
                closeTable();
            }
            foreach (CategoriaResumen sc in cat.SubCats)
            {
                subtree(name + "@" + cat.Nombre, sc);
            }
        }

        private void init()
        {

            addTitulo(titulo);
            foreach (ResumenBase c in _resumen)
            {
                if (c is ProductoResumen)
                {
                    ProductoResumen o = c as ProductoResumen;
                    openTable();
                    addProductoResumen(o);
                    closeTable();
                }
                else
                {
                    subtree("", (CategoriaResumen)c);
                }


            }
        }
    }
}
