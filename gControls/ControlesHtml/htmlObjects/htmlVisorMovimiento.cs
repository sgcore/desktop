﻿using gManager;
using gManager.html;

namespace gControls.ControlesHtml.htmlObjects
{
    public class htmlVisorMovimiento : htmlObject
    {
        public htmlVisorMovimiento(gcMovimiento m)
        {
            partialRender = true;
            gcDestinatario dest = m.Destinatario;
            decimal monto = dest.SaldoEnCuenta * -1;
            string cclass = "micro centrado";
            string saldoclass = monto > 0 ? cclass + " c2" : (monto < 0 ? cclass + " c3" : cclass + " c0");
            openTable(m.Tipo.ToString().ToLower());
            addRow(
                  //destinatario
                  new htag().content(getImage(UtilControls.imageFromDestTipo(dest.Tipo))).width(20).toTd()
                + new htag().content(dest.Nombre).className("centrado observacion big").width(300).attribute("colspan","2").toTd()
                // movimiento
                + new htag().content(getImage(UtilControls.imageFromMovTipoFactura(m.TipoFactura), 70, 50)).width(50).attribute("rowspan","2").toTd()
                + new htag().content(getImage(UtilControls.imageFromMovTipo(m.Tipo), 30, 30)).width(50).attribute("rowspan", "2").toTd()
                + new htag().content(m.Fecha.ToLongDateString()).className("tag observacion small").toTd()
            );
            addRow(
               //destinatario
               new htag().content(dest.Direccion + " - " + dest.Cuit + " - tel: " + dest.Telefono).className("centrado observacion small").attribute("colspan", "3").toTd()
               + new htag().content(m.Numero.ToString("0000000")).className("observacion giant centrado").toTd()

            );
            addRow(
                  //destinatario
                  new htag().content(getImage(Properties.Resources.Banco)).width(20).toTd()
                + new htmlNumber(monto).className(saldoclass).toTd()
                + new htag().content(dest.TipoIva.ToString().Replace("_", " ")).className("observacion micro centrado").toTd()
                + new htag().content(m.Tipo.ToString()).className("tag micro centrado").attribute("colspan", "1").toTd()
                + new htag().content("").toTd()
                + new htag().content(m.Estado.ToString()).className("micro centrado "+m.Estado.ToString().ToLower()).width(250).toTd()
            );
            closeTable();
        }
    }
}
