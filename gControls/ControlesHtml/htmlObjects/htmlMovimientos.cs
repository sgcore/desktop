﻿using gManager;
using gManager.Containers;
using gManager.html;
using System;
using System.Collections.Generic;

namespace gControls.ControlesHtml.htmlObjects
{
    class htmlMovimientos: htmlObject
    {
        public htmlMovimientos(List<gcMovimiento> movs, string title = "Movimientos")
        {
            var list = new ContainerMovimiento(movs);
             foreach (var par in list.Coleccion)
            {
                addBreadcrum(getImage(UtilControls.imageFromMovTipo((gcMovimiento.TipoMovEnum)Enum.Parse(typeof(gcMovimiento.TipoMovEnum), par.Key)))
                    +"@"+par.Key);
                foreach(var m in par.Value.Todos)
                {
                    addHtmObject(
                        new htag().tagName("table").content(
                            new htag().tagName("tr").content(
                              new htag().content(new htag().action("selmov", m.Id.ToString()).className("header description").content(m.Numero.ToString("00000"))).toTd()
                             + new htag().className("header").attribute("width","350px").content(m.DescripcionDestinatario).toTd()
                             + new htag().className("header " + m.Tipo.ToString().ToLower()).attribute("width", "90px").content(m.TotalMovimiento.ToString("$0.00")).toTd()
                             + new htag().className("tag "+ m.Estado.ToString().ToLower()).content(m.Estado.ToString().ToLower()).toTd()

                        )));
                    
                   
                }
              
            }
            
        }
    }
}
