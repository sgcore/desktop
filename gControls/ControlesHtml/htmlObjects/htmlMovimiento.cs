﻿using gManager;
using gManager.html;
using System;
using System.Collections.Generic;

namespace gControls.ControlesHtml.htmlObjects
{
    public class HtmlMovimiento: htmlObject
    {
        public HtmlMovimiento(gcMovimiento m, bool row = false)
        {
             addHtmObject(new htmlVisorMovimiento(m));
            if (m.EsMovimientoMercaderia || m.EsPresupuestooPedido)
            {
                addObjetoMovidos(m);
                if (!m.EsMovimientoDinero && m.EsResponsableInscripto)
                {

                    openTable();
                    addHeader(new string[] { "SUBTOTAL", "IVA", "IMPUESTO", "TOTAL" }, "IMPUESTOS");
                    foreach (gcMovimiento.Discriminacion d in m.Discriminaciones)
                    {
                        _body.Append("<tr>"
                       + new htmlNumber(d.Subtotal).insertbefore("$").className("c2").toTd()
                       + new htmlNumber(d.Iva).insertafter("%").className("c2").toTd()
                       + new htmlNumber(d.Impuesto).insertbefore("$").className("c2").toTd()
                       + new htmlNumber(d.Total).insertbefore("$").className("c2").toTd()
                       + "</tr>");
                    }


                    closeTable();
                }

            } else
            {
                addTitulo(m.DescripcionPago);
            }
            

            if (m.EsConDineroInvolucrado)
            {
                

                htmlNumber descuento = new htmlNumber(m.TotalDescontado, m.TotalCotizado);
                htmlNumber efectivo = new htmlNumber(m.PagosEnEfectivo.Monto);
                htmlNumber vueltos = new htmlNumber(m.PagosEnVueltos.Monto);
                htmlNumber tarjetas = new htmlNumber(m.PagosEnTarjetas.Monto);
                htmlNumber cuentas = new htmlNumber(m.PagosEnCuentas.Monto);
                if (!m.Terminado )
                {
                    if (m.SePuedeDescontar)
                    {
                        descuento.action("movpagodescuento", "ok");
                    }
                    if (m.Diferencia < 0)
                    {
                        vueltos.action("movpagovuelto", "ok");
                    } else if (m.Diferencia > 0 || m.EsDepositoOExtraccion)
                    {
                        efectivo.action("movpagoefectivo", "ok");
                        if(!m.EsExtraccion)tarjetas.action("movpagotargeta", "ok");
                    }
                   
                    if (m.Diferencia != 0)
                    {
                        cuentas.action("movpagocuenta", "ok");
                    }
                    
                }
                // Pagos
                openTable();
                addHeader(new string[] { "Subtotal", "Descuento", "A pagar", "Pagado", "efectivo", "Vuelto", "Tarjeta", "Cuenta" }, "PAGOS REALIZADOS");
                addRow(
                     new htmlNumber(m.TotalCotizado).toTd()
                   + descuento.toTd()
                   + new htmlNumber(m.Diferencia, m.TotalAPagar + 1).toTd()
                   + new htmlNumber(m.TotalPagodoSinDescuentos, m.TotalAPagar, m.TotalAPagar, true).toTd()
                   + efectivo.toTd()
                   + vueltos.toTd()
                   + tarjetas.toTd()
                   + cuentas.toTd()
                    );
                closeTable();
                
                if (!m.EsMovimientoDinero)
                {
                    

                    //descuentos
                    if (m.Descuentos.Count > 0)
                    {
                        openTable();
                        addHeader(new string[] { "Razon", "Monto" }, "DESCUENTOS");
                        foreach (gcPago d in m.Descuentos)
                        {
                            _body.Append("<tr>"
                            + new htag().content(d.Observaciones).className("c2").toTd()
                            + new htmlNumber(d.Monto).className("c2").toTd()
                            + "</tr>");
                        }

                        closeTable();
                    }
                }

            }
            addSeparator();
            //Cotizacion
            htag coti = new htag();
            if (!m.Terminado)
            {
                coti = coti.action("moveditcoti", "ok");
            }
            if (m.Cotizacion > 1)
            {
                coti.className("venta");
                coti.content(m.Moneda.ToString()+ " " + m.Cotizacion.ToString("$0.00"));
                _body.Append(coti.toH4());

            }
            else
            {
                if (!m.Terminado)
                {
                    coti.content("AGREGAR COTIZACION...");
                    _body.Append(coti.toH5());
                }

            }


            //numero de factura
            htag fact = new htag();
            if (!m.Terminado)
            {
                fact = fact.action("moveditfactnum", "ok");
            }
            if (m.FacturaNumero > 0)
            {
                fact.content("FACTURA Nº:" + m.FacturaNumero.ToString());
                _body.Append(fact.toH5());

            }
            else
            {
                if (!m.Terminado)
                {
                    fact.content("AGREGAR NUMERO DE FACTURA...");
                    _body.Append(fact.toH5());
                }

            }
             // descripcion pago
             if(m.TotalPagado > 0)
                _body.Append(new htag().content(m.DescripcionTotal).className("presupuesto ").toH4());
            addSeparator();

            //observaciones
            htag obs = new htag();
            if (!m.Terminado)
            {
                obs = obs.action("moveditobs", "ok");
            }
            if (!String.IsNullOrEmpty(m.Observaciones))
            {
                obs.content(m.Observaciones);
                _body.Append(obs.toH3());

            }
            else
            {
                if (!m.Terminado)
                {
                    obs.content("AGREGAR OBSERVACIONES...");
                    _body.Append(obs.toH3());
                }

            }




        }

        protected void addObjetoMovidos(gcMovimiento m)
        {
            openTable("");
            bool meronly = m.EsMovimientoMercaSolo;
            List<string> columnas = new List<string>();
            if(!m.Terminado)
            {
                columnas.Add("X");
            }
            columnas.Add("Cantidad");
            columnas.Add("Producto");

            if (!meronly)
            {
                columnas.Add("Precio");
                columnas.Add("Total");

            }
            
            // header
            addHeader(columnas.ToArray());

            //responsable
            

            //productos
            foreach (gcObjetoMovido o in m.ObjetosMovidos)
            {
                //eliminar om
                string cerrar = m.Terminado ? "" : new htag().action("remOM", o.id.ToString()).content("X").className("button micro").toTd();

                //editar cantidad
                htag cant = new htag().content(Utils.UnidadMedida(o.Objeto.Metrica, o.Cantidad)).contentMetrica();
                
                
                if(!m.Terminado)
                {
                    cant = cant.action("editom",o.id.ToString());
                }


                _body.Append("<tr>"
                 + cerrar
                 + cant.className("centrado observacion big").width(100).toTd()
                 + new htag().content(o.NombreProducto + " - " + new htag().content(o.CodigoProducto).className("tag micro venta").toSpan()).className("observacion").width(330).toTd()
                 + (meronly ? "" : new htmlNumber(m.EsResponsableInscripto ? o.MontoSIVA : o.Monto).className("observacion small").width(90).toTd())
                 + (meronly ? "" : new htmlNumber(m.EsResponsableInscripto ? o.TotalSIVA : o.Total).className("observacion big").toTd())
                 + "</tr>");
            }
           
            if (!meronly)
            {
                
                _body.Append("<tr>" + (m.Terminado ? "" : "<td></td>")
               + new htag().content("").attribute("colspan", "2").toTd()
               + new htag().content("TOTAL").className("observacion small").toTd()
               + new htmlNumber(m.EsResponsableInscripto ? m.TotalMovimientoSinIva : m.TotalMovimiento).insertbefore("$").className("giant").toTd()
               + "</tr>");
            }
            
            closeTable();
            
        }
        
    }
}
