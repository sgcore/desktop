﻿using gLogger;
using gManager.html;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gControls.ControlesHtml.htmlObjects
{
    public class htmlMessages : htmlObject
    {
        public htmlMessages(List<gcMessage> messages)
        {
            openPanel("Mensajes del sistema", 3);
            foreach (gcMessage m in messages)
            {
               if (m.CustomImage == null) m.CustomImage = Properties.Resources.Mensaje;
                addMessage(m);
                

            }
            closePanel();
        }
    }
}
