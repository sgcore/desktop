﻿using gManager;
using gManager.html;
using System;
using System.Collections.Generic;

namespace gControls.ControlesHtml.htmlObjects
{
    public class htmlResumenCategorias: htmlObject
    {
        List<ResumenBase> _resumen;
        string titulo = "Resumen de Ventas";
        public htmlResumenCategorias(List<gcCaja> cajas)
        {
            if (cajas == null || cajas.Count == 0)
            {
                _resumen = new List<ResumenBase>();
            } else
            {
                _resumen = CoreManager.Singlenton.ResumenVentasPorCats(cajas);
                if (cajas.Count == 1)
                    titulo += " Caja " + cajas[0].Caja;
                else
                    titulo += " Cajas " + cajas[cajas.Count - 1].Caja + " -> " + cajas[0].Caja;
            }
            init();
           
        }
        protected void addProductoResumen(ProductoResumen o)
        {
            addRow(
                new htag().content((String.IsNullOrEmpty(o.Codigo) ? "------" : o.Codigo)).className("normal").toTd()
                + new htag().content(o.Nombre).width(300).className("normal small").toTd()
                + new htag().content(Utils.UnidadMedida(o.Producto.Metrica, o.Cantidad)).contentMetrica().className("normal").toTd()
                + new htag().content(o.Total.ToString("$0.00")).className("normal").toTd()
                + new htag().content(o.UnitarioEstimado.ToString("$0.00")).className("normal").toTd()
                + new htag().content((String.IsNullOrEmpty(o.Link) ? "------" : o.Link)).className("normal").toTd()
                );
        }
        protected void addHeaderResument()
        {
            addHeader(new string[] {"Código"
                , "Producto"
                , "Cantidad"
                , "Total"
                , "Precio"
                , "Extra"});
        }

        private void subtree(string name, CategoriaResumen cat)
        {
            if (cat.SubObjetos.Count > 0)
            {
                addBreadcrum(name + "@" + cat.Nombre);
                openTable();
                addHeaderResument();
                foreach (ProductoResumen o in cat.SubObjetos)
                {
                    addProductoResumen(o);
                }
                //total
                addRow(
                new htag().attribute("colspan","2").toTd()
                + new htag().content(cat.Cantidad.ToString()).className("normal").toTd()
                + new htag().content(cat.Total.ToString("$0.00")).className("normal").toTd()
                + new htag().content(cat.UnitarioEstimado.ToString("$0.00")).className("normal").toTd()
                + new htag().toTd()
                );
                closeTable();
            }
            foreach(CategoriaResumen sc in cat.SubCats)
            {
                subtree(name + "@" + cat.Nombre, sc);
            }
        }

        private void init()
        {
            
            addTitulo(titulo);
            foreach(ResumenBase c in _resumen)
            {
                if(c is ProductoResumen)
                {
                    ProductoResumen o = c as ProductoResumen;
                    openTable();
                    addProductoResumen(o);
                    closeTable();
                } else
                {
                    subtree("", (CategoriaResumen)c);
                }
                
               
            }
        }
    }
}
