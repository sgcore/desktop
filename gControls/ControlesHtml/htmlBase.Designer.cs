﻿namespace gControls.ControlesHtml
{
    partial class htmlBase
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(htmlBase));
            this.pInfo = new TheArtOfDev.HtmlRenderer.WinForms.HtmlPanel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.imprimirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.paginaPrincipalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pInfo
            // 
            this.pInfo.AutoScroll = true;
            this.pInfo.AutoScrollMinSize = new System.Drawing.Size(711, 8);
            this.pInfo.BackColor = System.Drawing.SystemColors.Window;
            this.pInfo.BaseStylesheet = resources.GetString("pInfo.BaseStylesheet");
            this.pInfo.ContextMenuStrip = this.contextMenuStrip1;
            this.pInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pInfo.IsSelectionEnabled = false;
            this.pInfo.Location = new System.Drawing.Point(0, 0);
            this.pInfo.Name = "pInfo";
            this.pInfo.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.pInfo.Size = new System.Drawing.Size(711, 429);
            this.pInfo.TabIndex = 12;
            this.pInfo.Text = "<body></body>";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimirToolStripMenuItem,
            this.toolStripMenuItem1,
            this.paginaPrincipalToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(157, 54);
            // 
            // imprimirToolStripMenuItem
            // 
            this.imprimirToolStripMenuItem.Image = global::gControls.Properties.Resources.Pdf;
            this.imprimirToolStripMenuItem.Name = "imprimirToolStripMenuItem";
            this.imprimirToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.imprimirToolStripMenuItem.Text = "Guardar en PDF";
            this.imprimirToolStripMenuItem.Click += new System.EventHandler(this.imprimirToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(153, 6);
            // 
            // paginaPrincipalToolStripMenuItem
            // 
            this.paginaPrincipalToolStripMenuItem.Image = global::gControls.Properties.Resources.Iniciado;
            this.paginaPrincipalToolStripMenuItem.Name = "paginaPrincipalToolStripMenuItem";
            this.paginaPrincipalToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.paginaPrincipalToolStripMenuItem.Text = "Inicio";
            this.paginaPrincipalToolStripMenuItem.Click += new System.EventHandler(this.paginaPrincipalToolStripMenuItem_Click);
            // 
            // htmlBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pInfo);
            this.Name = "htmlBase";
            this.Size = new System.Drawing.Size(711, 429);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TheArtOfDev.HtmlRenderer.WinForms.HtmlPanel pInfo;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem imprimirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem paginaPrincipalToolStripMenuItem;
    }
}
