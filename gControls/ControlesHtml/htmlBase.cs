﻿using PdfSharp;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace gControls.ControlesHtml
{
    public partial class htmlBase : UserControl
    {
        public delegate void linkClick(string value);
        private Hashtable _actions = new Hashtable();

        public htmlBase()
        {
            InitializeComponent();
            //pInfo.BaseStylesheet = Properties.Resources.styles;
            pInfo.LinkClicked += PInfo_LinkClicked;
            pInfo.Click += PInfo_Click;
        }

        private void PInfo_Click(object sender, System.EventArgs e)
        {
           
        }

        private void PInfo_LinkClicked(object sender, TheArtOfDev.HtmlRenderer.Core.Entities.HtmlLinkClickedEventArgs e)
        {
            if (e.Attributes == null) return;
            foreach(var a in e.Attributes.ToList())
            {
                if (_actions.ContainsKey(a.Key))
                {
                    linkClick call = _actions[a.Key] as linkClick;
                    call(a.Value);
                }
            }
        }

        public void setStyle(string style)
        {
            pInfo.BaseStylesheet = style;
        }
        public void setContent(string t)
        {
            if (pInfo.InvokeRequired)
            {
                gManager.CoreManager.InfoHtmlDelegate del = new gManager.CoreManager.InfoHtmlDelegate((tt) => { pInfo.Text = tt; });
                pInfo.Invoke(del, t);
            }
            else
            {
                pInfo.Text = t;
            }

            
        }
        public void setContent(gManager.html.htmlObject obj)
        {
            setContent(obj.ToString());
        }
        new public string Text
        {
            get { return pInfo.Text; }
            set { pInfo.Text = value; }
        }
        public void setSyle(string style)
        {
            pInfo.BaseStylesheet = style;
        }
        public void addAction(string action, linkClick callback)
        {

            if (!_actions.ContainsKey(action))
            {
                _actions.Add(action, callback);
            }
            else
            {

                _actions[action] = callback;
            }
        }
        public void addMenu(ToolStripItem m)
        {

            contextMenuStrip1.Items.Add(m);
        }
        private void imprimirToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            PdfGenerateConfig config = new PdfGenerateConfig();
            config.PageSize = PageSize.A4;
            config.SetMargins(0);
            config.MarginLeft = 20;
            config.MarginRight = 20;
            var doc = PdfGenerator.GeneratePdf(pInfo.GetHtml(), config, null, html.OnStylesheetLoad, html.OnImageLoadPdfSharp);
            var tmpFile = UtilControls.ObtenerDireccionDeArchivo("Guardar archivo PDF", "Archivo PDF|*.pdf", true);
            // tmpFile = Path.GetFileNameWithoutExtension(tmpFile) + ".pdf";
            if (tmpFile != "")
            {
                doc.Save(tmpFile);
                Process.Start(tmpFile);
            }
        }

        private void paginaPrincipalToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            gManager.CoreManager.Singlenton.GenerarInformeHtml(null);
        }
    }
}
