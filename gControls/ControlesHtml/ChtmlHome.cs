﻿
using gControls.ControlesHtml.htmlObjects;

namespace gControls.ControlesHtml
{
    public partial class ChtmlHome : htmlBase
    {
        public ChtmlHome()
        {
            InitializeComponent();
            setSyle(Properties.Resources.styles);
            //gLogger.logger.Singlenton.gcMessageAdded += m => goHome(); //actualiza si hay mensajes
            gManager.WebManager.Singlenton.atHome += (x, s) => setContent(s);
            goHome();
        }
        public void goHome()
        {
            //setContent(new htmlPantallaPrincipal().ToString());
           
            gManager.WebManager.Singlenton.goHome();

        }
    }
}
