﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Buscadores
{
    public partial class BuscadorUsuarios : ListBase
    {
        public BuscadorUsuarios()
        {
            InitializeComponent();
            Lista = lw1;
            IniciarEventos();
            ConfigurarColumnas();
        }
        void ConfigurarColumnas()
        {

            

            //nombre
            Col0.Text = "Nombre";
            Col0.AspectName = "Nombre";
            Col0.MinimumWidth = 20;
            Col0.ImageGetter = delegate(object rowObject)
            {
                gManager.gcUsuario s = (gManager.gcUsuario)rowObject;
                return s.Permiso.ToString();
            };
            //Col1
            Col1.Text = "Permiso";
            Col1.MinimumWidth = 50;
            Col1.AspectName = "descPermiso";
            
            //Col2
            Col2.Text = "Usuario";
            Col2.AspectName = "User";
            Col1.MinimumWidth = 20;
            //Col3
            Col3.Text = "Central Asignada";
            Col3.AspectName = "CentralNombre";
            Col3.FillsFreeSpace = true;
            //Col4
            Col4.Text = "Responsable Asignado";
            Col4.AspectName = "ResponsableNombre";
            Col4.FillsFreeSpace = true;



        }
        protected override void Actualizar()
        {
            Datos = CoreManager.Singlenton.getUsuarios();
            base.Actualizar();
        }
        new public gcUsuario Seleccionado
        {
            get
            {
                return base.Seleccionado as gcUsuario;
            }
        }
    }
}
