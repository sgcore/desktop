﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using gManager.Filtros;

namespace gControls.Buscadores
{
    public partial class BuscadorConsumo : ListBase
    {
        public BuscadorConsumo()
        {
            InitializeComponent();
            Lista = lw1;
            IniciarEventos();
            ConfigurarColumnas();
            
        }
         void ConfigurarColumnas()
        {
          

            //nombre
            Col0.Text = "Producto";
            Col0.AspectName = "ProductoDescripcion";
            Col0.MinimumWidth = 50;
            Col0.MaximumWidth = -1;
            Col0.FillsFreeSpace = true;
            Col0.ImageGetter = delegate(object rowObject)
            {
                ProductoResumen s = (ProductoResumen)rowObject;

                return s.Producto.Tipo.ToString();
            };
            //Col1
            Col1.Text = "Cantidad";
            Col1.AspectName = "Cantidad";
            Col1.AspectToStringFormat = "{0:0.00}";
            Col1.MinimumWidth = 70;
            Col1.MaximumWidth = 100;
            Col1.Width = 70;
            Col1.FillsFreeSpace = false;


            ////Col2
            Col2.Text = "Total";
            Col2.AspectName = "Total";
            Col2.AspectToStringFormat = "{0:$0.00}";
            Col2.MinimumWidth = 70;
            Col2.MaximumWidth = 100;
            Col2.Width = 70;
            Col2.FillsFreeSpace = false;

            ////col3
            Col3.Text = "Código";
            Col3.AspectName = "ProductoCodigo";
            Col3.AspectToStringFormat = "{0:$0.00}";
            Col3.MinimumWidth = 70;
            Col3.MaximumWidth = 100;
            Col3.Width = 70;
            Col3.FillsFreeSpace = false;
            Col3.Searchable = true;

            //////col4
            //Col4.IsVisible = false;
        }
        new public gcObjeto Seleccionado
        {
            get
            {
                var b = base.Seleccionado as ProductoResumen;
                return b.Producto;

            }
        }
        protected override void seleccionarGlobal()
        {
            if (!SeleccionGlobal) return;
            CoreManager.Singlenton.seleccionarGenerico(Seleccionado);
        }
        public gcDestinatario Destinatario { get; set; }
        protected override void Actualizar()
        {
            if (Destinatario != null)
                Datos = gManager.CoreManager.Singlenton.getDestinatarioConsumo(Destinatario);
            else
                Datos = CoreManager.Singlenton.getMovimientosConsumo(Filtro as FiltroMovimiento);
                    
            base.Actualizar();
        }
       
    }
}
