﻿using BrightIdeasSoftware;
using gManager;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace gControls.Buscadores
{
    internal class CardRenderer : AbstractRenderer
    {
        

        internal Pen BorderPen = new Pen(Color.FromArgb(0x33, 0x33, 0x33));
        internal Brush TextBrush = new SolidBrush(Color.FromArgb(0x22, 0x22, 0x22));
        internal Brush HeaderTextBrush = Brushes.AliceBlue;
        internal Brush PriceBrush = Brushes.SteelBlue;
        internal Brush HeaderBackBrush = new SolidBrush(Color.FromArgb(0x33, 0x33, 0x33));
        internal Brush BackBrush = Brushes.LemonChiffon;
        protected const int spacing = 2;
        private Graphics g;
        private Rectangle itemBounds;
        private StringFormat fmt;
        public CardRenderer(Graphics gr, Rectangle ib)
        {
            g = gr;
            itemBounds = ib;
            itemBounds.Inflate(-spacing, -spacing);
            const int rounding = 16;
            var path = this.GetRoundedRect(itemBounds,rounding);
            g.FillPath(this.BackBrush, path);
            g.DrawPath(this.BorderPen, path);
            g.Clip = new Region(itemBounds);

            //format
            fmt = new StringFormat(StringFormatFlags.NoWrap);
            fmt.Trimming = StringTrimming.EllipsisCharacter;
            fmt.Alignment = StringAlignment.Center;
            fmt.LineAlignment = StringAlignment.Near;
        }
        
        private Rectangle WriteLineText(string txt, Rectangle textBoxRect, Font font, Brush textBrush, Brush backBrush = null)
        {
            textBoxRect.Inflate(-spacing, -spacing);
            // Measure the height of the title
            SizeF size = g.MeasureString(txt, font, (int)textBoxRect.Width, fmt);
            // Draw the title
            RectangleF r3 = textBoxRect;
            r3.Height = size.Height;
            if(backBrush != null)
            {
                g.FillPath(backBrush, GetRoundedRect(r3, 15));
            }
            
            g.DrawString(txt, font, textBrush, textBoxRect, fmt);

            textBoxRect.Y += (int)size.Height + spacing;

            return textBoxRect;
        }
        private Rectangle DrawImage(Image photo, Rectangle photoRect)
        {
            //imagen
            photoRect.Inflate(-spacing, -spacing);
            photoRect.Width = photo.Width;
            photoRect.Height = photo.Height;
            g.DrawImage(photo, photoRect);
            Rectangle textBoxRect = photoRect;
            textBoxRect.X += (photoRect.Width + spacing);
            textBoxRect.Width = itemBounds.Right - textBoxRect.X - spacing;
            return textBoxRect;
        }
        
        public virtual void DrawBusinessCard(Graphics g, Rectangle itemBounds, object rowObject, ObjectListView olv, OLVListItem item)
        {


            // Allow a border around the card
            itemBounds.Inflate(-2, -2);

            // Draw card background
            const int rounding = 50;
            GraphicsPath path = this.GetRoundedRect(itemBounds, rounding);
            g.FillPath(this.BackBrush, path);
            g.DrawPath(this.BorderPen, path);

            g.Clip = new Region(itemBounds);

            // Draw the photo
            Rectangle photoRect = itemBounds;
            photoRect.Inflate(-spacing, -spacing);

            if (rowObject != null)
            {
                photoRect.Width = 80;


                Image photo = ImageManager.getImageGeneric(rowObject, Properties.Resources.nopicbox);
                if (photo.Width > photoRect.Width)
                    photoRect.Height = (int)(photo.Height * ((float)photoRect.Width / photo.Width));
                else
                    photoRect.Height = photo.Height;
                g.DrawImage(photo, photoRect);

                //else
                //{
                //    g.DrawRectangle(Pens.DarkGray, photoRect);
                //}
            }

            // Now draw the text portion
            RectangleF textBoxRect = photoRect;
            textBoxRect.X += (photoRect.Width + spacing);
            textBoxRect.Width = itemBounds.Right - textBoxRect.X - spacing;

            StringFormat fmt = new StringFormat(StringFormatFlags.NoWrap);
            fmt.Trimming = StringTrimming.EllipsisCharacter;
            fmt.Alignment = StringAlignment.Center;
            fmt.LineAlignment = StringAlignment.Near;
            String txt = item.Text;

            using (Font font = new Font("Tahoma", 11))
            {
                // Measure the height of the title
                SizeF size = g.MeasureString(txt, font, (int)textBoxRect.Width, fmt);
                // Draw the title
                RectangleF r3 = textBoxRect;
                r3.Height = size.Height;
                path = this.GetRoundedRect(r3, 15);
                g.FillPath(this.HeaderBackBrush, path);
                g.DrawString(txt, font, this.HeaderTextBrush, textBoxRect, fmt);
                textBoxRect.Y += size.Height + spacing;
            }

            // Draw the other bits of information
            using (Font font = new Font("Tahoma", 8))
            {
                SizeF size = g.MeasureString("Wj", font, itemBounds.Width, fmt);
                textBoxRect.Height = size.Height;
                fmt.Alignment = StringAlignment.Near;
                for (int i = 0; i < olv.Columns.Count; i++)
                {
                    OLVColumn column = olv.GetColumn(i);
                    if (column.Index > 0)
                    {
                        txt = column.GetStringValue(rowObject);
                        g.DrawString(txt, font, this.TextBrush, textBoxRect, fmt);
                        textBoxRect.Y += size.Height;
                    }
                }
            }
        }
        public void DrawProductoCard(gcObjeto producto)
        {
            bool showprecio = false;
            Image defaultimg = Properties.Resources.nopicbox;
            switch (producto.Tipo)
            {
                case gcObjeto.ObjetoTIpo.Producto:
                    HeaderBackBrush = Brushes.BlueViolet;
                    showprecio = true;
                    break;
                case gcObjeto.ObjetoTIpo.Servicio:
                    HeaderBackBrush = Brushes.SteelBlue;
                    showprecio = true;
                    break;
                case gcObjeto.ObjetoTIpo.Grupo:
                    HeaderBackBrush = Brushes.RosyBrown;
                    defaultimg = Properties.Resources.Grupo;
                    break;
                case gcObjeto.ObjetoTIpo.Categoria:
                    HeaderBackBrush = Brushes.Sienna;
                    defaultimg = Properties.Resources.Categoria;
                    break;

            }
            // Now draw the text portion
            Rectangle textBoxRect = itemBounds;
           
            // titulo
            using (Font font = new Font("Tahoma", 11))
            {
                textBoxRect = WriteLineText(producto.Nombre, itemBounds, font, HeaderTextBrush, HeaderBackBrush);
            }

            //image
            Image photo = ImageManager.getImageGeneric(producto, defaultimg, 40);
            textBoxRect = DrawImage(photo, textBoxRect);

            //descripcion
            using (Font font = new Font("Tahoma", 8,FontStyle.Italic))
            {
                fmt.Alignment = StringAlignment.Near;
                textBoxRect = WriteLineText(producto.Descripcion, textBoxRect, font, Brushes.Black);
            }

            //Codigo
            using (Font font = new Font("Arial", 9, FontStyle.Bold))
            {
                fmt.Alignment = StringAlignment.Near;
                textBoxRect = WriteLineText(producto.Codigo, textBoxRect, font, Brushes.Black);
            }

            //Precio
            if (showprecio)
            {
                int altura = textBoxRect.Y;
                using (Font font = new Font("Arial", 14, FontStyle.Bold))
                {
                    fmt.Alignment = StringAlignment.Far;
                    textBoxRect.Y -= 16;
                    textBoxRect = WriteLineText(producto.Precio.ToString("$0.00"), textBoxRect, font, PriceBrush);
                }
                //stock
                using (Font font = new Font("Arial", 8, FontStyle.Bold))
                {
                    fmt.Alignment = StringAlignment.Center;
                    textBoxRect.Y = altura - 3;
                    textBoxRect = WriteLineText(producto.StockDescripcion, textBoxRect, font, new SolidBrush(UtilControls.ColorStok(producto)));
                }
            }
        }

        protected GraphicsPath GetRoundedRect(RectangleF rect, float diameter)
        {
            GraphicsPath path = new GraphicsPath();

            RectangleF arc = new RectangleF(rect.X, rect.Y, diameter, diameter);
            path.AddArc(arc, 180, 90);
            arc.X = rect.Right - diameter;
            path.AddArc(arc, 270, 90);
            arc.Y = rect.Bottom - diameter;
            path.AddArc(arc, 0, 90);
            arc.X = rect.Left;
            path.AddArc(arc, 90, 90);
            path.CloseFigure();

            return path;
        }

    }
}
