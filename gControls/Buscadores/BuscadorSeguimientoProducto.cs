﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BrightIdeasSoftware;
using gManager;

namespace gControls.Buscadores
{
    public partial class BuscadorSeguimientoProducto : ListBase
    {
        public BuscadorSeguimientoProducto()
        {
            InitializeComponent();
            Lista = lw1;
            IniciarEventos();
            lw1.FormatCell += resumen_FormatCell;
            Col0.ImageGetter = delegate(object rowObject)
            {
                ProductoResumen s = (ProductoResumen)rowObject;
                return s.Movimiento.Tipo.ToString();
            };
           
        }
        private void resumen_FormatCell(object sender, FormatCellEventArgs e)
        {
            if (e.ColumnIndex == this.Col3.Index)
            {
                ProductoResumen customer = (ProductoResumen)e.Model;

                if (customer.Saldo < 0)
                {
                    e.SubItem.ForeColor = Color.Red;
                    e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                    // e.SubItem.ImageSelector = Properties.Resources.Down;
                }
                else if (customer.Saldo > 0)
                {
                    e.SubItem.ForeColor = Color.DodgerBlue;
                    // e.SubItem.ImageSelector = Properties.Resources.Up;
                    e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                }
                else
                {
                    e.SubItem.ForeColor = Color.LightGray;
                }

            }
        }
        gManager.gcObjeto _obj;
        public void setProductoSeguimiento(gManager.Resumenes.SeguimientoProducto sp)
        {
            _obj=sp.Producto ;
            if (_obj == null)
            {
                Titulo = "Desconocido";
            }
            else
            {
                Titulo = _obj.Nombre;
               
                setCustomDatos(sp.Resumenes);
            }

        }
        protected override void seleccionarGlobal()
        {
            if (!SeleccionGlobal) return;
            CoreManager.Singlenton.seleccionarMovimiento(Seleccionado);
        }
        new public gcMovimiento Seleccionado
        {
            get
            {
                if (base.Seleccionado == null) return null;
                return (base.Seleccionado as ProductoResumen).Movimiento;
            }
        }
    }
}
