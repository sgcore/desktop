﻿namespace gControls.Buscadores
{
    partial class BuscadorMovimientos
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscadorMovimientos));
            this.lw1 = new BrightIdeasSoftware.FastObjectListView();
            this.Col0 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colCaja = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MovImageList = new System.Windows.Forms.ImageList(this.components);
            this.colId = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.Location = new System.Drawing.Point(0, 31);
            // 
            // lw1
            // 
            this.lw1.AllColumns.Add(this.Col0);
            this.lw1.AllColumns.Add(this.Col1);
            this.lw1.AllColumns.Add(this.Col2);
            this.lw1.AllColumns.Add(this.Col4);
            this.lw1.AllColumns.Add(this.Col3);
            this.lw1.AllColumns.Add(this.colCaja);
            this.lw1.AllColumns.Add(this.colId);
            this.lw1.AlternateRowBackColor = System.Drawing.Color.Silver;
            this.lw1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lw1.CellEditUseWholeCell = false;
            this.lw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Col0,
            this.Col1,
            this.Col2});
            this.lw1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lw1.ForeColor = System.Drawing.Color.Black;
            this.lw1.FullRowSelect = true;
            this.lw1.GroupImageList = this.MovImageList;
            this.lw1.HideSelection = false;
            this.lw1.LargeImageList = this.MovImageList;
            this.lw1.Location = new System.Drawing.Point(0, 59);
            this.lw1.Name = "lw1";
            this.lw1.OverlayImage.Transparency = 60;
            this.lw1.OverlayText.Text = "";
            this.lw1.ShowGroups = false;
            this.lw1.ShowImagesOnSubItems = true;
            this.lw1.Size = new System.Drawing.Size(771, 525);
            this.lw1.SmallImageList = this.MovImageList;
            this.lw1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lw1.TabIndex = 18;
            this.lw1.UseCellFormatEvents = true;
            this.lw1.UseCompatibleStateImageBehavior = false;
            this.lw1.UseExplorerTheme = true;
            this.lw1.UseFilterIndicator = true;
            this.lw1.UseFiltering = true;
            this.lw1.UseHotItem = true;
            this.lw1.View = System.Windows.Forms.View.Details;
            this.lw1.VirtualMode = true;
            // 
            // Col0
            // 
            this.Col0.AspectName = "DescripcionDestinatario";
            this.Col0.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col0.HeaderForeColor = System.Drawing.Color.Black;
            this.Col0.MinimumWidth = 100;
            this.Col0.Text = "Destinatario";
            this.Col0.Width = 200;
            // 
            // Col1
            // 
            this.Col1.AspectName = "Numero";
            this.Col1.AspectToStringFormat = "{0:0000}";
            this.Col1.IsVisible = global::gControls.Properties.Settings.Default.BuscMovcolNumero;
            this.Col1.MinimumWidth = 50;
            this.Col1.Text = "Número";
            this.Col1.Width = 66;
            // 
            // Col2
            // 
            this.Col2.AspectName = "Monto";
            this.Col2.AspectToStringFormat = "{0:$0.00}";
            this.Col2.Groupable = false;
            this.Col2.IsVisible = global::gControls.Properties.Settings.Default.BuscMovcolMonto;
            this.Col2.MinimumWidth = 60;
            this.Col2.Text = "Monto";
            this.Col2.Width = 100;
            // 
            // Col4
            // 
            this.Col4.AspectName = "Fecha";
            this.Col4.AspectToStringFormat = "{0:d}";
            this.Col4.DisplayIndex = 4;
            this.Col4.Groupable = false;
            this.Col4.IsVisible = global::gControls.Properties.Settings.Default.BuscMovcolFecha;
            this.Col4.MinimumWidth = 100;
            this.Col4.Text = "Fecha";
            this.Col4.Width = 129;
            // 
            // Col3
            // 
            this.Col3.AspectName = "Observaciones";
            this.Col3.DisplayIndex = 3;
            this.Col3.IsVisible = global::gControls.Properties.Settings.Default.BuscMovcolObserv;
            this.Col3.MinimumWidth = 50;
            this.Col3.Text = "Observaciones";
            this.Col3.UseInitialLetterForGroup = true;
            this.Col3.Width = 114;
            // 
            // colCaja
            // 
            this.colCaja.AspectName = "Caja";
            this.colCaja.DisplayIndex = 5;
            this.colCaja.IsVisible = global::gControls.Properties.Settings.Default.BuscMovcolCaja;
            this.colCaja.MinimumWidth = 50;
            this.colCaja.Text = "Caja";
            // 
            // MovImageList
            // 
            this.MovImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MovImageList.ImageStream")));
            this.MovImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MovImageList.Images.SetKeyName(0, "Compra");
            this.MovImageList.Images.SetKeyName(1, "Retorno");
            this.MovImageList.Images.SetKeyName(2, "Venta");
            this.MovImageList.Images.SetKeyName(3, "Devolucion");
            this.MovImageList.Images.SetKeyName(4, "Entrada");
            this.MovImageList.Images.SetKeyName(5, "Salida");
            this.MovImageList.Images.SetKeyName(6, "Deposito");
            this.MovImageList.Images.SetKeyName(7, "Extraccion");
            this.MovImageList.Images.SetKeyName(8, "Pedido");
            this.MovImageList.Images.SetKeyName(9, "Presupuesto");
            this.MovImageList.Images.SetKeyName(10, "Terminado");
            this.MovImageList.Images.SetKeyName(11, "Creado");
            this.MovImageList.Images.SetKeyName(12, "Modificado");
            this.MovImageList.Images.SetKeyName(13, "Abajo");
            this.MovImageList.Images.SetKeyName(14, "Arriba");
            this.MovImageList.Images.SetKeyName(15, "Cheque");
            this.MovImageList.Images.SetKeyName(16, "Cuenta");
            this.MovImageList.Images.SetKeyName(17, "Efectivo");
            this.MovImageList.Images.SetKeyName(18, "Tarjeta");
            this.MovImageList.Images.SetKeyName(19, "Cotizacion");
            // 
            // colId
            // 
            this.colId.AspectName = "id";
            this.colId.IsVisible = false;
            this.colId.Text = "ID";
            // 
            // BuscadorMovimientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lw1);
            this.Name = "BuscadorMovimientos";
            this.Controls.SetChildIndex(this.lblTitulo, 0);
            this.Controls.SetChildIndex(this.lw1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.FastObjectListView lw1;
        private BrightIdeasSoftware.OLVColumn Col0;
        private BrightIdeasSoftware.OLVColumn Col1;
        private BrightIdeasSoftware.OLVColumn Col2;
        private BrightIdeasSoftware.OLVColumn Col3;
        private BrightIdeasSoftware.OLVColumn Col4;
        protected System.Windows.Forms.ImageList MovImageList;
        private BrightIdeasSoftware.OLVColumn colCaja;
        private BrightIdeasSoftware.OLVColumn colId;
    }
}
