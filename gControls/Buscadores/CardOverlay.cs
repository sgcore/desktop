﻿using BrightIdeasSoftware;
using gManager;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Buscadores
{
    internal class CardOverlay : AbstractOverlay
    {
        public CardOverlay()
        {
            this.Transparency = 255;
        }
        #region IOverlay Members

        public override void Draw(ObjectListView olv, Graphics g, Rectangle r)
        {
            if (olv.HotRowIndex < 0)
                return;

            if (olv.View == View.Tile)
                return;

            OLVListItem item = olv.GetItem(olv.HotRowIndex);
            if (item == null)
                return;

            if (item.RowObject is gcObjeto)
            {
                var cardBounds = createCardBounds(300, 80, r);

                new CardRenderer(g, cardBounds).DrawProductoCard(item.RowObject as gcObjeto);

            }
            else
            {
                var cardBounds = createCardBounds(250, 120, r);
                new CardRenderer(g, cardBounds).DrawBusinessCard(g, cardBounds, item.RowObject, olv, item);

            }
        }
        private Rectangle createCardBounds(int width, int height, Rectangle container)
        {
            Size cardSize = new Size(width, height);
            return new Rectangle(
                container.Right - cardSize.Width - 8, container.Bottom - cardSize.Height - 8, cardSize.Width, cardSize.Height);
        }

        #endregion
        
    }
    
}
