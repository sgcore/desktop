﻿namespace gControls.Buscadores
{
    partial class BuscadorProductoUpdate
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lw1 = new BrightIdeasSoftware.FastObjectListView();
            this.ColNombre = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColCodigo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColCosto = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColCostoCotizado = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColCotizacion = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColProducto = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.Location = new System.Drawing.Point(0, 31);
            // 
            // lw1
            // 
            this.lw1.AllColumns.Add(this.ColNombre);
            this.lw1.AllColumns.Add(this.ColCodigo);
            this.lw1.AllColumns.Add(this.ColCosto);
            this.lw1.AllColumns.Add(this.ColCostoCotizado);
            this.lw1.AllColumns.Add(this.ColCotizacion);
            this.lw1.AllColumns.Add(this.ColProducto);
            this.lw1.AlternateRowBackColor = System.Drawing.Color.Silver;
            this.lw1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColNombre,
            this.ColCodigo,
            this.ColCostoCotizado,
            this.ColProducto});
            this.lw1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lw1.ForeColor = System.Drawing.Color.Black;
            this.lw1.FullRowSelect = true;
            this.lw1.HideSelection = false;
            this.lw1.Location = new System.Drawing.Point(0, 59);
            this.lw1.Name = "lw1";
            this.lw1.OverlayImage.Transparency = 60;
            this.lw1.OverlayText.Text = "";
            this.lw1.OwnerDraw = true;
            this.lw1.ShowGroups = false;
            this.lw1.ShowImagesOnSubItems = true;
            this.lw1.Size = new System.Drawing.Size(771, 525);
            this.lw1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lw1.TabIndex = 21;
            this.lw1.UseCellFormatEvents = true;
            this.lw1.UseCompatibleStateImageBehavior = false;
            this.lw1.UseExplorerTheme = true;
            this.lw1.UseFilterIndicator = true;
            this.lw1.UseFiltering = true;
            this.lw1.UseHotItem = true;
            this.lw1.View = System.Windows.Forms.View.Details;
            this.lw1.VirtualMode = true;
            // 
            // ColNombre
            // 
            this.ColNombre.AspectName = "Nombre";
            this.ColNombre.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColNombre.HeaderForeColor = System.Drawing.Color.Black;
            this.ColNombre.Text = "Nombre";
            this.ColNombre.Width = 202;
            // 
            // ColCodigo
            // 
            this.ColCodigo.AspectName = "Codigo";
            this.ColCodigo.Groupable = false;
            this.ColCodigo.Text = "Codigo";
            this.ColCodigo.Width = 97;
            // 
            // ColCosto
            // 
            this.ColCosto.AspectName = "Costo";
            this.ColCosto.DisplayIndex = 2;
            this.ColCosto.Groupable = false;
            this.ColCosto.IsVisible = false;
            this.ColCosto.MinimumWidth = 50;
            this.ColCosto.Text = "Costo";
            this.ColCosto.Width = 111;
            // 
            // ColCostoCotizado
            // 
            this.ColCostoCotizado.AspectName = "CostoCotizado";
            this.ColCostoCotizado.Groupable = false;
            this.ColCostoCotizado.Text = "Costo Cotizado";
            this.ColCostoCotizado.Width = 106;
            // 
            // ColCotizacion
            // 
            this.ColCotizacion.AspectName = "Cotizacion";
            this.ColCotizacion.DisplayIndex = 4;
            this.ColCotizacion.FillsFreeSpace = true;
            this.ColCotizacion.Groupable = false;
            this.ColCotizacion.IsVisible = false;
            this.ColCotizacion.Text = "Cotizacion";
            this.ColCotizacion.Width = 136;
            // 
            // ColProducto
            // 
            this.ColProducto.AspectName = "ProductoDescripcion";
            this.ColProducto.FillsFreeSpace = true;
            this.ColProducto.MinimumWidth = 100;
            this.ColProducto.Text = "Producto Asociado";
            this.ColProducto.Width = 100;
            // 
            // BuscadorProductoUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lw1);
            this.Name = "BuscadorProductoUpdate";
            this.Controls.SetChildIndex(this.lblTitulo, 0);
            this.Controls.SetChildIndex(this.lw1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.FastObjectListView lw1;
        private BrightIdeasSoftware.OLVColumn ColNombre;
        private BrightIdeasSoftware.OLVColumn ColCodigo;
        private BrightIdeasSoftware.OLVColumn ColCosto;
        private BrightIdeasSoftware.OLVColumn ColCostoCotizado;
        private BrightIdeasSoftware.OLVColumn ColCotizacion;
        private BrightIdeasSoftware.OLVColumn ColProducto;
    }
}
