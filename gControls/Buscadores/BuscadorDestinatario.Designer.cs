﻿namespace gControls.Buscadores
{
    partial class BuscadorDestinatario
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscadorDestinatario));
            this.lw1 = new BrightIdeasSoftware.FastObjectListView();
            this.Col0 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.DestImageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.Location = new System.Drawing.Point(0, 31);
            // 
            // lw1
            // 
            this.lw1.AllColumns.Add(this.Col0);
            this.lw1.AllColumns.Add(this.Col1);
            this.lw1.AllColumns.Add(this.Col2);
            this.lw1.AllColumns.Add(this.Col3);
            this.lw1.AllColumns.Add(this.Col4);
            this.lw1.AlternateRowBackColor = System.Drawing.Color.Silver;
            this.lw1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Col0,
            this.Col1,
            this.Col2,
            this.Col3,
            this.Col4});
            this.lw1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lw1.ForeColor = System.Drawing.Color.Black;
            this.lw1.FullRowSelect = true;
            this.lw1.HideSelection = false;
            this.lw1.LargeImageList = this.DestImageList;
            this.lw1.Location = new System.Drawing.Point(0, 59);
            this.lw1.Name = "lw1";
            this.lw1.OverlayImage.Transparency = 60;
            this.lw1.OverlayText.Text = "";
            this.lw1.OwnerDraw = true;
            this.lw1.ShowGroups = false;
            this.lw1.ShowImagesOnSubItems = true;
            this.lw1.Size = new System.Drawing.Size(771, 525);
            this.lw1.SmallImageList = this.DestImageList;
            this.lw1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lw1.TabIndex = 18;
            this.lw1.UseCellFormatEvents = true;
            this.lw1.UseCompatibleStateImageBehavior = false;
            this.lw1.UseExplorerTheme = true;
            this.lw1.UseFilterIndicator = true;
            this.lw1.UseFiltering = true;
            this.lw1.UseHotItem = true;
            this.lw1.View = System.Windows.Forms.View.Details;
            this.lw1.VirtualMode = true;
            // 
            // Col0
            // 
            this.Col0.AspectName = "Nombre";
            this.Col0.FillsFreeSpace = true;
            this.Col0.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col0.HeaderForeColor = System.Drawing.Color.Black;
            this.Col0.MinimumWidth = 100;
            this.Col0.Text = "Nombre";
            this.Col0.Width = 202;
            // 
            // Col1
            // 
            this.Col1.AspectName = "SaldoEnCuenta";
            this.Col1.AspectToStringFormat = "{0:$0.00}";
            this.Col1.Groupable = false;
            this.Col1.MaximumWidth = 80;
            this.Col1.MinimumWidth = 60;
            this.Col1.Text = "Deuda";
            this.Col1.Width = 80;
            // 
            // Col2
            // 
            this.Col2.AspectName = "Direccion";
            this.Col2.Groupable = false;
            this.Col2.MinimumWidth = 50;
            this.Col2.Text = "Direccion";
            this.Col2.Width = 111;
            // 
            // Col3
            // 
            this.Col3.AspectName = "Mail";
            this.Col3.Groupable = false;
            this.Col3.Text = "Correo";
            this.Col3.Width = 101;
            // 
            // Col4
            // 
            this.Col4.AspectName = "Cuit";
            this.Col4.FillsFreeSpace = true;
            this.Col4.Groupable = false;
            this.Col4.Text = "CUIT";
            this.Col4.Width = 136;
            // 
            // DestImageList
            // 
            this.DestImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("DestImageList.ImageStream")));
            this.DestImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.DestImageList.Images.SetKeyName(0, "Cliente");
            this.DestImageList.Images.SetKeyName(1, "Proveedor");
            this.DestImageList.Images.SetKeyName(2, "Contacto");
            this.DestImageList.Images.SetKeyName(3, "Banco");
            this.DestImageList.Images.SetKeyName(4, "Financiera");
            this.DestImageList.Images.SetKeyName(5, "Paciente");
            this.DestImageList.Images.SetKeyName(6, "Responsable");
            this.DestImageList.Images.SetKeyName(7, "Sucursal");
            this.DestImageList.Images.SetKeyName(8, "Usuario");
            this.DestImageList.Images.SetKeyName(9, "Like");
            this.DestImageList.Images.SetKeyName(10, "Unlike");
            // 
            // BuscadorDestinatario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lw1);
            this.Name = "BuscadorDestinatario";
            this.Controls.SetChildIndex(this.lblTitulo, 0);
            this.Controls.SetChildIndex(this.lw1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.FastObjectListView lw1;
        private BrightIdeasSoftware.OLVColumn Col0;
        private BrightIdeasSoftware.OLVColumn Col1;
        private BrightIdeasSoftware.OLVColumn Col2;
        private BrightIdeasSoftware.OLVColumn Col3;
        private BrightIdeasSoftware.OLVColumn Col4;
        private System.Windows.Forms.ImageList DestImageList;

    }
}
