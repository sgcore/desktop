﻿using BrightIdeasSoftware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace gControls.Buscadores
{
    public class textFilter: AbstractModelFilter
    {
        string[] texts;
        ObjectListView ListView = null;
        public textFilter(ObjectListView olv, string[] txts ) :base()
        {
            texts = txts;
            ListView = olv;
        }
        /// <summary>
        /// Gets whether or not this filter has any search criteria
        /// </summary>
        public bool HasComponents
        {
            get
            {
                return texts.Length > 0;
            }
        }
       
        public override bool Filter(object modelObject)
        {
            if (ListView == null || !this.HasComponents)
                return true;
            bool retu = false;
            
            foreach (OLVColumn column in this.ListView.Columns)
            {
                if (column.IsVisible && column.Searchable)
                {
                    string cellText = column.GetStringValue(modelObject);
                    string reg = "";
                    foreach (string filter in texts)
                    {
                        if (!String.IsNullOrEmpty(filter))
                        {
                            reg += "(?=.*" + filter.ToLower() + ")";
                        }
                        
                       
                    }
                    Regex rgx = new Regex(reg, RegexOptions.IgnoreCase);
                    retu |= rgx.IsMatch(cellText);
                }
            }

            return retu;
        }
    }
}
