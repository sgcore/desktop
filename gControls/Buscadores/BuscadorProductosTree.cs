﻿using BrightIdeasSoftware;
using gControls.menues;
using gManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace gControls.Buscadores
{
    public partial class BuscadorProductosTree : ListBase
    {
        public BuscadorProductosTree():base()
        {
            
            InitializeComponent();
            ColSync.ImageKey = "Sincronizacion";
            this.tw1.CanExpandGetter = delegate(object x)
            {
                return (x is gManager.gcCategoria && (x as gManager.gcCategoria).SubItems.Count > 0);
            };
            this.tw1.ChildrenGetter = delegate(object x)
            {
                gcObjeto dir = (gcObjeto)x;
                return dir.SubItems;
            };
            Lista.FormatCell += Productos_FormatCell;

            colNombre.ImageGetter = delegate(object rowObject)
            {
                gcObjeto s = (gcObjeto)rowObject;
                return s.Tipo.ToString();
            };
            ColSync.ImageGetter = (obj) =>
            {
                gcObjeto s = (gcObjeto)obj;
                
                if (s.Sincronizado)
                {
                    return "Sincronizado";
                } else
                {
                    if(s.State == gcObjeto.ObjetoState.Publico)
                    {
                        if (String.IsNullOrEmpty(s.Codigo))
                        {
                            return "error";
                        }
                        return "Desincronizado";
                    }
                }

                return null;
            };
            
            
            HandleMenues( (o, e) =>
            {
                tw1.ContextMenuStrip = new ContextMenuStrip();
                if(RequisitoPermiso(gcUsuario.PermisoEnum.Contador))
                tw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Ver, "Ver", "Muestra un seguimiento del item seleccionado."
                   , (oo, ee) => { Acciones.setProducto(Seleccionado); Acciones.verProducto(null, null); }, Keys.None,true));
                if (RequisitoPermiso(gcUsuario.PermisoEnum.Contador))
                {
                    tw1.ContextMenuStrip.Items.Add(new MenuEditarCategoria((oo, ee) => {
                        Acciones.setProducto(Seleccionado);
                    }));
                }
                    
                if (RequisitoPermiso(gcUsuario.PermisoEnum.Administrador))
                    tw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Conectar , "Sincronizar", "Guarda el producto en internet."
                       , (oo, ee) => {
                           var sel = Seleccionados;
                           if (sel == null || sel.Count == 0) return;
                           CoreManager.Singlenton.SincronizarProductos(sel);
                       }, Keys.None,true));
                if (RequisitoPermiso(gcUsuario.PermisoEnum.Administrador))
                    tw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Desconectar, "Desincronizar", "Elimina un producto del servidor de internet."
                       , (oo, ee) => { CoreManager.Singlenton.SincronizarProductos(Seleccionados,true); }, Keys.None,true));

                if (RequisitoPermiso(gcUsuario.PermisoEnum.Contador))
                    tw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Eliminar, "Eliminar", "Elimina un producto de ser posible."
                       , (oo, ee) => {if (CoreManager.Singlenton.EliminarProductos(Seleccionados))
                       {
                           
                       }
                       Actualizar();
                       }, Keys.None, true));
               
                tw1.ContextMenuStrip.Items.Add(new ToolStripSeparator());

                tw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Expandir, "Expandir todos", "Expande todos los nodos del arbol"
                    , (oo, ee) => { tw1.ExpandAll(); }, Keys.Control | Keys.Right));
                tw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Contraer, "Contraer todos", "Contrae a las categorias principales"
                    , (oo, ee) => { tw1.CollapseAll(); }, Keys.Control | Keys.Left));
            });
            IniciarEventos();
            EmpezarABuscar += BuscadorProductosTree_EmpezarABuscar;
            gManager.CoreManager.Singlenton.ProductoAgregado += (p) => { Actualizar(); };

            //privado grupos
            tw1.SelectedIndexChanged += tw1_ItemActivate;
            
           
        }

        void tw1_ItemActivate(object sender, EventArgs e)
        {
            bool t=Seleccionado != null &&( Seleccionado.Tipo==gcObjeto.ObjetoTIpo.Grupo ||Seleccionado.Grupo>0);
            buscadorProductos1.Visible = t;
            if (t)
            {
                var g = Seleccionado.Tipo==gcObjeto.ObjetoTIpo.Grupo?Seleccionado: Seleccionado.GrupoContenedor;
                if (g != null)
                {
                    buscadorProductos1.Titulo = g.Nombre;
                    buscadorProductos1.setCustomDatos(g.SubItems);
                }
               
            }
        }

        void BuscadorProductosTree_EmpezarABuscar(object sender, ListBase.ItemsFoundedArgs e)
        {
            
            tw1.ExpandAll();
        }

        public override BrightIdeasSoftware.ObjectListView Lista
        {
            get {return tw1; }
        }
        protected override void Actualizar()
        {
            Datos = gManager.CoreManager.Singlenton.getCategoriasPrincipales;
            base.Actualizar();
        }
        private void Productos_FormatCell(object sender, FormatCellEventArgs e)
        {
            gcObjeto customer = (gcObjeto)e.Model;
            
            if (e.ColumnIndex == this.colStock.Index)
            {


                switch (customer.Tipo)
                {
                    case gcObjeto.ObjetoTIpo.Categoria:
                    case gcObjeto.ObjetoTIpo.Grupo:
                        e.SubItem.Text = "";
                        break;
                    case gcObjeto.ObjetoTIpo.Producto:
                        e.SubItem.Text = gManager.Utils.UnidadMedida(customer.Metrica, customer.Stock);
                        if (customer.Stock < 0)
                        {
                            e.SubItem.ForeColor = Color.Red;
                            e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                            // e.SubItem.ImageSelector = Properties.Resources.Down;
                        }
                        else if (customer.Stock > 0)
                        {
                            e.SubItem.ForeColor = Color.Black;
                            // e.SubItem.ImageSelector = Properties.Resources.Up;
                            e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                        }
                        else
                        {
                            e.SubItem.ForeColor = Color.LightGray;
                        }
                        break;
                    case gcObjeto.ObjetoTIpo.Servicio:

                        e.SubItem.Text = Math.Abs(customer.Stock).ToString("0.00");
                        e.SubItem.ForeColor = Color.DarkBlue;
                        //e.SubItem.ImageSelector = Properties.Resources.Like;
                        e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);
                        break;
                }


            }
            else if (e.ColumnIndex == this.colPrecio.Index)
            {
                switch (customer.Tipo)
                {
                    case gcObjeto.ObjetoTIpo.Categoria:
                    case gcObjeto.ObjetoTIpo.Grupo:
                        e.SubItem.Text = String.Empty;
                        break;
                    case gcObjeto.ObjetoTIpo.Producto:
                    case gcObjeto.ObjetoTIpo.Servicio:

                        e.SubItem.ForeColor = Color.Black;
                        // e.SubItem.ImageSelector = Properties.Resources.Up;
                        e.SubItem.Font = new Font("Arial", 11, FontStyle.Regular);
                        break;
                }
                
            }
            else if (e.ColumnIndex == this.colNombre.Index)
            {
                if (customer.Tipo == gcObjeto.ObjetoTIpo.Producto || customer.Tipo == gcObjeto.ObjetoTIpo.Servicio)
                {

                    e.SubItem.Font = new Font("Arial", 9, FontStyle.Bold);
                    switch (customer.State)
                    {
                        case gcObjeto.ObjetoState.Publico:
                            e.SubItem.ForeColor = Color.SteelBlue;
                            break;
                        case gcObjeto.ObjetoState.Discontinuo:
                            e.SubItem.ForeColor = Color.Salmon;
                            break;
                        default:
                            e.SubItem.ForeColor = Color.Black;
                            break;

                    }
                }
            }
           
        }
        new public gcObjeto Seleccionado
        {
            get
            {
                return base.Seleccionado as gcObjeto;
            }
        }
        new public List<gcObjeto> Seleccionados
        {
            get
            {
                return base.Seleccionados.Cast<gcObjeto>().ToList<gcObjeto>();
            }
        }
      

       

       
    }
}
