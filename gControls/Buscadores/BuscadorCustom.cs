﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Buscadores
{
    public partial class BuscadorCustom : ListBase
    {
        public BuscadorCustom()
        {
            InitializeComponent();
            Lista = lw1;
            IniciarEventos();
           
        }
        public BrightIdeasSoftware.OLVColumn addColumn(string titulo,string aspect)
        {
            BrightIdeasSoftware.OLVColumn col = new BrightIdeasSoftware.OLVColumn(titulo, aspect);
            Lista.AllColumns.Add(col);
            Lista.Columns.Add(col);
            return col;
        }
        public void ActualizarLista()
        {
            Actualizar();
        }
       
    }
}
