﻿using BrightIdeasSoftware;
using gManager;
using gManager.Filtros;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Buscadores
{
    public class ListBase:BaseControl
    {
        protected ToolStrip Botonera;
        public event EventHandler Actualizando;
        public event EventHandler Eliminando;
        private ToolStripLabel lblBuscar;
        private ToolStripTextBox txtBuscar;
        private ToolStripButton btActualizar;
        private ToolStripButton btFiltro;
        private ToolStripButton btImprimir;
        private ToolStripSeparator separadorImprimir;
        private ToolStripButton btVerImagenCard;
        protected BotonAccion lblTitulo;
        private ToolStripButton btGuardar;
        private ToolStripButton btAgrupar;
        private Timer _timer = new Timer();
        public ListBase()
            : base()
        {
            InitializeComponent();
            _timer.Interval = 500;
            _timer.Tick += (o, e) =>
            {
                _timer.Stop();
                BuscarTexto(txtBuscar.Text);
            };
            

            
        }
        protected void IniciarEventos()
        {
            lblTitulo.Click += lblTitulo_Click;
            Lista.KeyDown += lista_KeyDown;
            Lista.MouseDoubleClick += lista_MouseDoubleClick;
            lblTitulo.SendToBack();
            Lista.Font = new Font("Tahoma", 10);
           
        }
        #region DELEGADOS
        public class ItemsFoundedArgs : EventArgs
        {
            public int Contados { get; set; }
            public int Encontrados { get; set; }
            public string TextoBuscado { get; set; }
            public long Tiempo { get; set; }
            public bool Vacio { get { return Contados == 0 && Encontrados == 0; } }
        }
        public event EventHandler<ItemsFoundedArgs> ItemsEcontrados;
        public event EventHandler<ItemsFoundedArgs> EmpezarABuscar;
        public event EventHandler ItemSeleccionado;
        public EventHandler MostrarBotonesHandle { get; private set; }
        public EventHandler MostrarMenuesHandle { get; private set; }
        public void HandleBotones(EventHandler h)
        {
            MostrarBotonesHandle = h;
        }
        public void HandleMenues(EventHandler h)
        {
            MostrarMenuesHandle = h;
        }
        #endregion 


        #region PROPIEDADES
        public virtual ObjectListView Lista { get; protected set; }
        protected object Seleccionado { get { return Lista.SelectedObject; } }
        protected ICollection Seleccionados { get { return Lista.SelectedObjects; } }
        public bool SeleccionGlobal { get; set; }
        protected ICollection Datos { get; set; }
        public Filtro Filtro { get; protected set; }
        public bool VerImagenCard
        {
            get { return this.Lista!=null && this.Lista.HotItemStyle != null && this.Lista.HotItemStyle.Overlay!=null ; }
            set
            {
               
                if (value)
                {
                    this.Lista.HotItemStyle = new HotItemStyle();
                    this.Lista.HotItemStyle.Overlay = new CardOverlay();
                    this.Lista.HotItemStyle = this.Lista.HotItemStyle;
                }
                else
                {
                    if (Lista != null)
                    {
                        if(Lista.HotItemStyle!=null) this.Lista.HotItemStyle.Overlay = null;
                        this.Lista.HotItemStyle = this.Lista.HotItemStyle;
                    }
                   
                }

            }
        }
               
        public bool OcultarSiEstaVacio { get; set; }
        public bool VerBotonAgrupar { get; set; }
        public bool VerBotonGuardar { get; set; }
        public bool VerBotonImprimir { get; set; }
        public bool VerBotonActualizar { get; set; }
        public bool VerBotonImagen { get; set; }
        public bool VerBotonFiltro { get; set; }

        
        
        
        public bool BloquearBarraHerramientas { get; set; }
        
        public Image ImagenTitulo
        {
            get
            {
                return lblTitulo.Image;
            }
            set
            {
                if (this.Imagen == null) this.Imagen = value;
                lblTitulo.Image = value;
               
            }
        }
        public string Titulo
        {
            get
            {
                return lblTitulo.Text;
            }
            set
            {
                lblTitulo.Text = value;
            }
        }
        #endregion
        /// <summary>
        /// Si esta activo solo se puede asignar datos con setCustomDatos
        /// </summary>
        public bool CustomDatos { get; set; }
       
        

        #region METODOS
        public void setCustomDatos(ICollection l)
        {
            Datos = l;
            CustomDatos = true;
            Lista.SetObjects(null);
            if (Lista is DataTreeListView)
            {
                (Lista as DataTreeListView).Roots = Datos;
            }
            else
                Lista.SetObjects(Datos);
        }
        public void setCustomColums(ICollection<OLVColumn> l)
        {
            Lista.AllColumns.Clear();
            Lista.Columns.Clear();
            foreach (var h in l)
            {
                Lista.AllColumns.Add(h);
            }
           
            Lista.Columns.AddRange(l.ToArray<OLVColumn>());
            Actualizar();
        }
        public void SetAllImageList(ImageList il)
        {
            Lista.LargeImageList = il;
            Lista.SmallImageList = il;
            Lista.BaseSmallImageList = il;
        }
        public EventHandler CustomActualizar { get; set; }
        public virtual void Reset()
        {
           // Lista.ResetCellFormatSubscription();
            foreach (OLVColumn c in Lista.Columns)
            {
                c.AspectToStringFormat = "";
                c.ImageGetter = null;
            }
        }
        void seleccionar()
        {

            if (Seleccionado != null && ItemSeleccionado != null)
                ItemSeleccionado(Seleccionado, null);
            seleccionarGlobal();
        }
        protected virtual void seleccionarGlobal()
        {
            if (!SeleccionGlobal) return;
            CoreManager.Singlenton.seleccionarGenerico(Seleccionado);
        }
        public void BuscarTexto(string texto)
        {
            EmpezarABuscar?.Invoke(null, new ItemsFoundedArgs() { TextoBuscado = texto });
            TimedFilter(Lista, texto, 0);
        }
        void TimedFilter(ObjectListView olv, string txt, int matchKind)
        {
            TextMatchFilter filter = null;
            textFilter filter2 = null;
            filter = TextMatchFilter.Regex(olv, txt);
            
            if (!String.IsNullOrEmpty(txt))
            {
                string[] strs = txt.Trim().Split(" ".ToCharArray());
                filter2 =new textFilter(olv, strs);
                if (strs.Length > 1)
                {
                    filter = TextMatchFilter.Regex(olv, strs);
                   
                }
                else
                {
                    switch (matchKind)
                    {
                        case 0:
                        default:
                            filter = TextMatchFilter.Contains(olv, txt);
                            break;
                        case 1:
                            filter = TextMatchFilter.Prefix(olv, txt);
                            break;
                        case 2:
                            filter = TextMatchFilter.Regex(olv, txt);
                            break;
                    }
                }

            }


            // Setup a default renderer to draw the filter matches
            if (filter == null)
                olv.DefaultRenderer = null;
            else
            {
                olv.DefaultRenderer = new HighlightTextRenderer(filter);

                // Uncomment this line to see how the GDI+ rendering looks
                // olv.DefaultRenderer = new HighlightTextRenderer { Filter = filter, UseGdiTextRendering = false };
            }

            // Some lists have renderers already installed
            HighlightTextRenderer highlightingRenderer = olv.GetColumn(0).Renderer as HighlightTextRenderer;
            if (highlightingRenderer != null)
                highlightingRenderer.Filter = filter;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            olv.AdditionalFilter = filter2;
            olv.Invalidate();
            stopWatch.Stop();
            
            IList objects = olv.Objects as IList;
            ItemsEcontrados?.Invoke(this, new ItemsFoundedArgs()
            {
                Contados = objects != null ? objects.Count : 0,
                Encontrados = olv.Items.Count,
                TextoBuscado = txt,
                Tiempo = stopWatch.ElapsedMilliseconds
            });
            if (objects == null)
                gLogger.logger.Singlenton.addCustomMessage(Properties.Resources.Buscar, "Busqueda", "No se encontraron resultados.");

            else
                gLogger.logger.Singlenton.addCustomMessage(Properties.Resources.Buscar, "Busqueda", String.Format("Se filtraron {0} objetos y se encontraron {1} en {2}ms",
                                    objects.Count,
                                    olv.Items.Count,
                                    stopWatch.ElapsedMilliseconds));
            ;
            if (olv.Items.Count > 0)
            {
                txtBuscar.BackColor = Color.LawnGreen;

            }
            else
            {
                txtBuscar.BackColor = Color.LightCoral;
            }
            if (txtBuscar.Text == "")
            {
                txtBuscar.BackColor = Color.LemonChiffon;
            }
            

        }
        public void Congelar(bool state)
        {
            Lista.Frozen = state;
        }
        protected void lista_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                seleccionar();
            }
            else if (e.KeyCode == Keys.Delete)
            {
                var l = Seleccionados;
                if (l == null && Seleccionado!=null) l = new List<object> { Seleccionado };
                if (Eliminando != null) Eliminando(l, null);
            }
        }
        protected void lista_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            seleccionar();

        }

        protected virtual void EditarFiltro() { Filtro = Editar.EditarFiltro(Filtro); Actualizar(); }
        protected virtual void Imprimir() 
        {
            Impresiones.ImprimirListView(Lista, Titulo);
        }
        protected virtual void Actualizar()
        {
            if (CustomDatos)
            {
                CustomActualizar?.Invoke(null, null);

            }
            else
            {
                Lista.SetObjects(null);
                if (Lista is DataTreeListView)
                {
                    (Lista as DataTreeListView).Roots = Datos;
                }
                else
                    Lista.SetObjects(Datos);
               
            }
            Actualizando?.Invoke(Datos, null);

        }
        public virtual void setFiltro(Filtro f)
        {
            Filtro = f;
            txtBuscar.Text = f.Texto;
            f.Texto = "";
           
            Actualizar();
        }
        public override void cerrar()
        {
            Lista.SetObjects(null);
            if (Lista is DataTreeListView)
            {
                (Lista as DataTreeListView).Roots = null;
            } 
            Datos = null;
            
        }
        public override bool Bloqueado
        {
            get
            {
                return Lista.Items.Count==0 && OcultarSiEstaVacio;
            }
           
        }

        public override void iniciar()
        {
            base.iniciar();

            //eventos
            btAgrupar.Visible = VerBotonAgrupar;
            btFiltro.Visible = VerBotonFiltro;
            btImprimir.Visible = VerBotonImprimir;
            btVerImagenCard.Visible = VerBotonImagen;
            btActualizar.Visible = VerBotonActualizar;
            btGuardar.Visible = VerBotonGuardar;

            btAgrupar.Checked = Lista.ShowGroups;
            ResetBotonera();
            MostrarBotonesHandle?.Invoke(null, null);
            MostrarMenuesHandle?.Invoke(null, null);
            Actualizar();

        }

        void lblTitulo_Click(object sender, EventArgs e)
        {

        }
        
       
        
        private void InitializeComponent()
        {
            this.Botonera = new System.Windows.Forms.ToolStrip();
            this.lblBuscar = new System.Windows.Forms.ToolStripLabel();
            this.txtBuscar = new System.Windows.Forms.ToolStripTextBox();
            this.btActualizar = new System.Windows.Forms.ToolStripButton();
            this.btFiltro = new System.Windows.Forms.ToolStripButton();
            this.btAgrupar = new System.Windows.Forms.ToolStripButton();
            this.btImprimir = new System.Windows.Forms.ToolStripButton();
            this.separadorImprimir = new System.Windows.Forms.ToolStripSeparator();
            this.btGuardar = new System.Windows.Forms.ToolStripButton();
            this.btVerImagenCard = new System.Windows.Forms.ToolStripButton();
            this.lblTitulo = new gControls.BotonAccion();
            this.Botonera.SuspendLayout();
            this.SuspendLayout();
            // 
            // Botonera
            // 
            this.Botonera.AutoSize = false;
            this.Botonera.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblBuscar,
            this.txtBuscar,
            this.btActualizar,
            this.btFiltro,
            this.btAgrupar,
            this.btImprimir,
            this.separadorImprimir,
            this.btGuardar,
            this.btVerImagenCard});
            this.Botonera.Location = new System.Drawing.Point(0, 28);
            this.Botonera.Name = "Botonera";
            this.Botonera.Size = new System.Drawing.Size(771, 31);
            this.Botonera.TabIndex = 16;
            // 
            // lblBuscar
            // 
            this.lblBuscar.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(45, 28);
            this.lblBuscar.Text = "Buscar:";
            // 
            // txtBuscar
            // 
            this.txtBuscar.BackColor = System.Drawing.Color.LemonChiffon;
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(100, 31);
            this.txtBuscar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBuscar_KeyDown);
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // btActualizar
            // 
            this.btActualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btActualizar.Image = global::gControls.Properties.Resources.Actualizar;
            this.btActualizar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btActualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btActualizar.Name = "btActualizar";
            this.btActualizar.Size = new System.Drawing.Size(28, 28);
            this.btActualizar.Text = "Actualizar";
            this.btActualizar.ToolTipText = "Actualizar la lista";
            this.btActualizar.Click += new System.EventHandler(this.btActualizar_Click);
            // 
            // btFiltro
            // 
            this.btFiltro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btFiltro.Image = global::gControls.Properties.Resources.Filtro;
            this.btFiltro.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btFiltro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btFiltro.Name = "btFiltro";
            this.btFiltro.Size = new System.Drawing.Size(28, 28);
            this.btFiltro.Text = "Filtrar";
            this.btFiltro.ToolTipText = "Genera un filtro mas especifico";
            this.btFiltro.Click += new System.EventHandler(this.btFiltro_Click);
            // 
            // btAgrupar
            // 
            this.btAgrupar.CheckOnClick = true;
            this.btAgrupar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btAgrupar.Image = global::gControls.Properties.Resources.Grupo;
            this.btAgrupar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btAgrupar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAgrupar.Name = "btAgrupar";
            this.btAgrupar.Size = new System.Drawing.Size(28, 28);
            this.btAgrupar.Text = "Ver Imagen";
            this.btAgrupar.ToolTipText = "Agrupar los elementos";
            this.btAgrupar.CheckedChanged += new System.EventHandler(this.btAgrupar_Click);
            // 
            // btImprimir
            // 
            this.btImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btImprimir.Image = global::gControls.Properties.Resources.Imprimir;
            this.btImprimir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(28, 28);
            this.btImprimir.Text = "Imprimir";
            this.btImprimir.ToolTipText = "Imprime la lista";
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // separadorImprimir
            // 
            this.separadorImprimir.Name = "separadorImprimir";
            this.separadorImprimir.Size = new System.Drawing.Size(6, 31);
            // 
            // btGuardar
            // 
            this.btGuardar.CheckOnClick = true;
            this.btGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btGuardar.Image = global::gControls.Properties.Resources.Guardar;
            this.btGuardar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGuardar.Name = "btGuardar";
            this.btGuardar.Size = new System.Drawing.Size(28, 28);
            this.btGuardar.Text = "Guardar CSV";
            this.btGuardar.ToolTipText = "Guarda la lista seleccionada en un archivo";
            this.btGuardar.Click += new System.EventHandler(this.btGuardar_Click);
            // 
            // btVerImagenCard
            // 
            this.btVerImagenCard.CheckOnClick = true;
            this.btVerImagenCard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btVerImagenCard.Image = global::gControls.Properties.Resources.ImageOk;
            this.btVerImagenCard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btVerImagenCard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btVerImagenCard.Name = "btVerImagenCard";
            this.btVerImagenCard.Size = new System.Drawing.Size(28, 28);
            this.btVerImagenCard.Text = "Ver Imagen";
            this.btVerImagenCard.ToolTipText = "Mostrar el Cuadro de Imagen al pasar por un item";
            this.btVerImagenCard.CheckedChanged += new System.EventHandler(this.btVerImagenCard_CheckedChanged);
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lblTitulo.Checked = false;
            this.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.FormContainer = null;
            this.lblTitulo.GlobalHotKey = false;
            this.lblTitulo.HacerInvisibleAlDeshabilitar = false;
            this.lblTitulo.HotKey = System.Windows.Forms.Shortcut.None;
            this.lblTitulo.Image = global::gControls.Properties.Resources.Buscar;
            this.lblTitulo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTitulo.Location = new System.Drawing.Point(0, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.lblTitulo.RegistradoEnForm = false;
            this.lblTitulo.RequiereKey = false;
            this.lblTitulo.Size = new System.Drawing.Size(771, 28);
            this.lblTitulo.TabIndex = 17;
            this.lblTitulo.Text = "Titulo";
            this.lblTitulo.Titulo = "Mostrar / Ocultar Botonera";
            this.lblTitulo.ToolTipDescripcion = "Muestra u oculta la botonera de busqueda y acciones.";
            this.lblTitulo.UseVisualStyleBackColor = true;
            // 
            // ListBase
            // 
            this.Controls.Add(this.Botonera);
            this.Controls.Add(this.lblTitulo);
            this.Imagen = global::gControls.Properties.Resources.Buscar;
            this.Name = "ListBase";
            this.Size = new System.Drawing.Size(771, 584);
            this.Texto = "Buscar";
            this.Botonera.ResumeLayout(false);
            this.Botonera.PerformLayout();
            this.ResumeLayout(false);

        }

        private void btActualizar_Click(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btFiltro_Click(object sender, EventArgs e)
        {
            EditarFiltro();
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            Imprimir();
        }

        private void btVerImagenCard_CheckedChanged(object sender, EventArgs e)
        {
            VerImagenCard = btVerImagenCard.Checked;
        }

        //buscar
        
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (_timer.Enabled) _timer.Stop();
            _timer.Start();
        }
        #endregion

        #region BOTONES
        private List<string> _agregados = new List<string>();
        public ToolStripButton generarBoton(Image img, string texto, string desc, EventHandler del, gcUsuario.PermisoEnum permiso)
        {
            var btseg = new ToolStripButton(img) { Name = "bt" + _agregados.Count };
            btseg.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btseg.ToolTipText = desc;
            btseg.Text = texto;
            btseg.ImageScaling = ToolStripItemImageScaling.None;
            btseg.Click += del;
            if (RequisitoPermiso(permiso))
            {
                Botonera.Items.Add(btseg);
                _agregados.Add(btseg.Name);
            }

            return btseg;
        }
        public ToolStripBotonAccion generarBotonAccion(Image img, string texto, string desc, EventHandler del, gcUsuario.PermisoEnum permiso, Shortcut sc = Shortcut.None, bool global = false)
        {
            var btseg = new ToolStripBotonAccion() { Name = "bt" + _agregados.Count };
            btseg.Boton.Image = img;
            btseg.Boton.GlobalHotKey = global;
            btseg.Boton.HotKey = sc;
            btseg.Boton.TextImageRelation = TextImageRelation.ImageAboveText;
            btseg.Boton.Permiso = permiso;
            btseg.Boton.Click += del;
            btseg.Boton.Titulo = texto;
            btseg.Boton.ToolTipDescripcion = desc;
            btseg.Boton.Width = 29;
            btseg.Boton.FlatStyle = FlatStyle.Flat;


            Botonera.Items.Add(btseg);
            _agregados.Add(btseg.Name);


            return btseg;
        }
        public void generarSeparador()
        {
            var sep = new ToolStripSeparator() { Name = "bt" + _agregados.Count };
            Botonera.Items.Add(sep);
            _agregados.Add(sep.Name);
        }
        public ToolStripMenuItem crearMenuItem(Image img, string texto, string desc, EventHandler del, Keys shortcut = Keys.None,bool requierekey=false)
        {
            var btseg = new ToolStripMenuItem(img);
            btseg.ToolTipText = desc;
            btseg.Text = texto;
            btseg.ShortcutKeys = shortcut;
            btseg.Click += del;
            if (requierekey)
            {
                Registracion.BloquearConKey(btseg);
            }
            return btseg;
        }
        public ToolStripMenuItem addContextMenuItem(Image img, string texto, string desc, EventHandler del, Keys shortcut = Keys.None, bool requierekey = false)
        {
            var btseg = crearMenuItem(img,texto,desc,del,shortcut,requierekey);
            if(Lista.ContextMenuStrip == null)
            {
                Lista.ContextMenuStrip = new ContextMenuStrip();
            }
            Lista.ContextMenuStrip.Items.Add(btseg);

            return btseg;
        }
        public void ResetBotonera()
        {
            foreach (string s in _agregados)
                Botonera.Items.RemoveByKey(s);
            _agregados.Clear();
            
        }
       
        #endregion

        private void txtBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Lista.Items.Count > 0)
                {
                    Lista.SelectedIndex = 0;
                    Lista.Select();
                }
            }
        }
        

        public void saveToCSV()
        {
            string path = UtilControls.ObtenerDireccionDeArchivo("Guardar lista", "Archivo CSV |*.csv", true);
            if (String.IsNullOrEmpty(path) || Seleccionados == null) return;
            using (var file = File.CreateText(path))
            {
                foreach (var arr in Seleccionados)
                {

                    foreach (var col in Lista.Columns)
                    {

                    }
                }
            }
        }

        private void btGuardar_Click(object sender, EventArgs e)
        {
            saveToCSV();
        }

        private void btAgrupar_Click(object sender, EventArgs e)
        {
            Lista.ShowGroups = btAgrupar.Checked;
            if(btAgrupar.Checked)Actualizar();
        }

     


    }
}
