﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BrightIdeasSoftware;
using gManager;
using gManager.Filtros;

namespace gControls.Buscadores
{
    public partial class BuscadorProductos : ListBase
    {
        public BuscadorProductos()
        {
            InitializeComponent();
            Lista = lw1;
            IniciarEventos();
            ConfigurarColumnas();
        }
        private void Productos_FormatCell(object sender, FormatCellEventArgs e)
        {
            if (e.ColumnIndex == this.Col2.Index)
            {

                gcObjeto customer = (gcObjeto)e.Model;

                switch (customer.Tipo)
                {
                    case gcObjeto.ObjetoTIpo.Categoria:
                    case gcObjeto.ObjetoTIpo.Grupo:
                        e.SubItem.Text = "";
                        break;
                    case gcObjeto.ObjetoTIpo.Producto:
                        if (customer.Stock < 0)
                        {
                            e.SubItem.ForeColor = Color.Red;
                            e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                            // e.SubItem.ImageSelector = Properties.Resources.Down;
                        }
                        else if (customer.Stock > 0)
                        {
                            e.SubItem.ForeColor = Color.Black;
                            // e.SubItem.ImageSelector = Properties.Resources.Up;
                            e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                        }
                        else
                        {
                            e.SubItem.ForeColor = Color.LightGray;
                        }
                        break;
                    case gcObjeto.ObjetoTIpo.Servicio:

                        e.SubItem.ForeColor = Color.DarkBlue;
                        //e.SubItem.ImageSelector = Properties.Resources.Like;
                        e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);
                        break;
                }


            }
        }
        void ConfigurarColumnas()
        {
            lw1.FormatCell += Productos_FormatCell;
             Col0.ImageGetter = delegate(object rowObject)
            {
                gcObjeto s = (gcObjeto)rowObject;
                return s.Tipo.ToString();
            };
            Col1.ImageGetter = delegate(object rowObject)
            {
                gcObjeto s = (gcObjeto)rowObject;
                return "Precio";
            };
            //nombre
            Col0.Text = "Nombre";
            Col0.AspectName = "Nombre";
            Col1.MinimumWidth = 150;
            //Col1
            Col1.Text = "Precio";
            Col1.MinimumWidth = 60;
            Col1.MaximumWidth = 70;
            Col1.AspectToStringFormat = "{0:$0.00}";
            Col1.AspectName = "Precio";
            Col1.FillsFreeSpace = true;
            //Col2
            Col2.Text = "Stock";
            Col2.AspectName = "Stock";
            Col2.MinimumWidth = 60;
            Col2.MaximumWidth = 70;
            Col2.FillsFreeSpace = true;
            Col2.AspectToStringFormat = "{0:0.00}";
            //Col3
            Col3.Text = "Código";
            Col3.AspectName = "Codigo";
            Col3.FillsFreeSpace = true;
            Col3.MinimumWidth = 60;
            Col3.MaximumWidth = 70;
            //Col4
            Col4.Text = "Descripción";
            Col4.AspectName = "Descripcion";
            Col4.FillsFreeSpace = true;

        }
        protected override void Actualizar()
        {
            if(!CustomDatos) Datos = CoreManager.Singlenton.getProductos(Filtro as FiltroProducto);
            base.Actualizar();
        }
        new public gcObjeto Seleccionado
        {
            get
            {
                return base.Seleccionado as gcObjeto;
            }
        }
        new public List<gcObjeto> Seleccionados
        {
            get
            {
                return base.Seleccionados.Cast<gcObjeto>().ToList<gcObjeto>();
            }
        }
    }
}
