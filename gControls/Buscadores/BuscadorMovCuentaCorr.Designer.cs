﻿namespace gControls.Buscadores
{
    partial class BuscadorMovCuentaCorr
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscadorMovCuentaCorr));
            this.lw1 = new BrightIdeasSoftware.FastObjectListView();
            this.Col0 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colObservaciones = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colFecha = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MovImageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.Location = new System.Drawing.Point(0, 31);
            // 
            // lw1
            // 
            this.lw1.AllColumns.Add(this.Col0);
            this.lw1.AllColumns.Add(this.Col1);
            this.lw1.AllColumns.Add(this.Col2);
            this.lw1.AllColumns.Add(this.colObservaciones);
            this.lw1.AllColumns.Add(this.colFecha);
            this.lw1.AlternateRowBackColor = System.Drawing.Color.Silver;
            this.lw1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Col0,
            this.Col1,
            this.Col2,
            this.colFecha});
            this.lw1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lw1.ForeColor = System.Drawing.Color.Black;
            this.lw1.FullRowSelect = true;
            this.lw1.HideSelection = false;
            this.lw1.LargeImageList = this.MovImageList;
            this.lw1.Location = new System.Drawing.Point(0, 59);
            this.lw1.Name = "lw1";
            this.lw1.OverlayImage.Transparency = 60;
            this.lw1.OverlayText.Text = "";
            this.lw1.OwnerDraw = true;
            this.lw1.ShowGroups = false;
            this.lw1.ShowImagesOnSubItems = true;
            this.lw1.Size = new System.Drawing.Size(771, 525);
            this.lw1.SmallImageList = this.MovImageList;
            this.lw1.TabIndex = 20;
            this.lw1.UseCellFormatEvents = true;
            this.lw1.UseCompatibleStateImageBehavior = false;
            this.lw1.UseExplorerTheme = true;
            this.lw1.UseFilterIndicator = true;
            this.lw1.UseFiltering = true;
            this.lw1.UseHotItem = true;
            this.lw1.View = System.Windows.Forms.View.Details;
            this.lw1.VirtualMode = true;
            // 
            // Col0
            // 
            this.Col0.AspectName = "Nombre";
            this.Col0.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col0.HeaderForeColor = System.Drawing.Color.Black;
            this.Col0.Sortable = false;
            this.Col0.Text = "Nombre";
            this.Col0.Width = 202;
            // 
            // Col1
            // 
            this.Col1.AspectName = "";
            this.Col1.Groupable = false;
            this.Col1.Sortable = false;
            this.Col1.Text = "Columna 1";
            this.Col1.Width = 97;
            // 
            // Col2
            // 
            this.Col2.AspectName = "";
            this.Col2.Groupable = false;
            this.Col2.MinimumWidth = 50;
            this.Col2.Sortable = false;
            this.Col2.Text = "Columna 2";
            this.Col2.Width = 111;
            // 
            // colObservaciones
            // 
            this.colObservaciones.AspectName = "ObservacionesMovimiento";
            this.colObservaciones.IsVisible = false;
            this.colObservaciones.MinimumWidth = 150;
            this.colObservaciones.Sortable = false;
            this.colObservaciones.Text = "Observaciones";
            this.colObservaciones.Width = 150;
            // 
            // colFecha
            // 
            this.colFecha.Sortable = false;
            this.colFecha.Text = "Fecha";
            // 
            // MovImageList
            // 
            this.MovImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MovImageList.ImageStream")));
            this.MovImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MovImageList.Images.SetKeyName(0, "Compra");
            this.MovImageList.Images.SetKeyName(1, "Retorno");
            this.MovImageList.Images.SetKeyName(2, "Venta");
            this.MovImageList.Images.SetKeyName(3, "Devolucion");
            this.MovImageList.Images.SetKeyName(4, "Entrada");
            this.MovImageList.Images.SetKeyName(5, "Salida");
            this.MovImageList.Images.SetKeyName(6, "Deposito");
            this.MovImageList.Images.SetKeyName(7, "Extraccion");
            this.MovImageList.Images.SetKeyName(8, "Pedido");
            this.MovImageList.Images.SetKeyName(9, "Presupuesto");
            this.MovImageList.Images.SetKeyName(10, "Terminado");
            this.MovImageList.Images.SetKeyName(11, "Creado");
            this.MovImageList.Images.SetKeyName(12, "Modificado");
            this.MovImageList.Images.SetKeyName(13, "Abajo");
            this.MovImageList.Images.SetKeyName(14, "Arriba");
            this.MovImageList.Images.SetKeyName(15, "Cheque");
            this.MovImageList.Images.SetKeyName(16, "Cuenta");
            this.MovImageList.Images.SetKeyName(17, "Efectivo");
            this.MovImageList.Images.SetKeyName(18, "Tarjeta");
            // 
            // BuscadorMovCuentaCorr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lw1);
            this.Name = "BuscadorMovCuentaCorr";
            this.Controls.SetChildIndex(this.lblTitulo, 0);
            this.Controls.SetChildIndex(this.lw1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.FastObjectListView lw1;
        private BrightIdeasSoftware.OLVColumn Col0;
        private BrightIdeasSoftware.OLVColumn Col1;
        private BrightIdeasSoftware.OLVColumn Col2;
        protected System.Windows.Forms.ImageList MovImageList;
        private BrightIdeasSoftware.OLVColumn colFecha;
        private BrightIdeasSoftware.OLVColumn colObservaciones;

       
    }
}
