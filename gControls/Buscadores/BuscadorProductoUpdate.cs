﻿using gManager;
using System;
using System.Windows.Forms;

namespace gControls.Buscadores
{
    public partial class BuscadorProductoUpdate : ListBase
    {
        public enum UpdateProductOrigen
        {
            Excel,
            Web
        }
        public BuscadorProductoUpdate()
        {
            InitializeComponent();
            Lista = lw1;
            HandleMenues( (o, e) =>
            {
                lw1.ContextMenuStrip = new ContextMenuStrip();


                lw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Guardar, "Guardar/Actualizar Seleccionados", "Actualiza el sistema con los productos seleccionados"
                    , (oo, ee) => {
                        foreach (ProductoUpdate a in Seleccionados)
                            a.SaveMe();
                    }));
                
            });
            IniciarEventos();
            
        }
        protected override void Actualizar()
        {
            switch (Origen)
            {
                case UpdateProductOrigen.Excel:
                    string path = UtilControls.ObtenerDireccionDeArchivo("Obtener Excel", "Archivo Excel|*.xls");
                    if (String.IsNullOrEmpty(path)) return;
                    Datos = CoreManager.Singlenton.ProductosEnExcel(path);
                    base.Actualizar();
                    break;
                case UpdateProductOrigen.Web:
                    break;
            }
           
        }
        public UpdateProductOrigen Origen { get; set; }
    }
}
