﻿namespace gControls.Buscadores
{
    partial class BuscadorProductosTree
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscadorProductosTree));
            this.tw1 = new BrightIdeasSoftware.DataTreeListView();
            this.colNombre = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colPrecio = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColSync = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colStock = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colCodigo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colDescripcion = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colSerial = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colLink = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ProdImageList = new System.Windows.Forms.ImageList(this.components);
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.buscadorProductos1 = new gControls.Buscadores.BuscadorProductos();
            ((System.ComponentModel.ISupportInitialize)(this.tw1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.Image = global::gControls.Properties.Resources.Buscar;
            this.lblTitulo.Location = new System.Drawing.Point(0, 31);
            this.lblTitulo.Size = new System.Drawing.Size(843, 28);
            this.lblTitulo.Text = "Buscar Productos y servicios";
            // 
            // tw1
            // 
            this.tw1.AllColumns.Add(this.colNombre);
            this.tw1.AllColumns.Add(this.colPrecio);
            this.tw1.AllColumns.Add(this.ColSync);
            this.tw1.AllColumns.Add(this.colStock);
            this.tw1.AllColumns.Add(this.colCodigo);
            this.tw1.AllColumns.Add(this.colDescripcion);
            this.tw1.AllColumns.Add(this.colSerial);
            this.tw1.AllColumns.Add(this.colLink);
            this.tw1.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tw1.CellEditUseWholeCell = false;
            this.tw1.CheckedAspectName = "";
            this.tw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNombre,
            this.colPrecio,
            this.ColSync,
            this.colStock,
            this.colCodigo,
            this.colDescripcion});
            this.tw1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tw1.DataSource = null;
            this.tw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tw1.EmptyListMsg = "No tiene ningun producto ingresado";
            this.tw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tw1.FullRowSelect = true;
            this.tw1.LargeImageList = this.ProdImageList;
            this.tw1.Location = new System.Drawing.Point(0, 59);
            this.tw1.Name = "tw1";
            this.tw1.RootKeyValueString = "";
            this.tw1.ShowGroups = false;
            this.tw1.ShowImagesOnSubItems = true;
            this.tw1.Size = new System.Drawing.Size(843, 521);
            this.tw1.SmallImageList = this.ProdImageList;
            this.tw1.TabIndex = 0;
            this.tw1.UseCellFormatEvents = true;
            this.tw1.UseCompatibleStateImageBehavior = false;
            this.tw1.UseFilterIndicator = true;
            this.tw1.UseFiltering = true;
            this.tw1.UseHotItem = true;
            this.tw1.View = System.Windows.Forms.View.Details;
            this.tw1.VirtualMode = true;
            // 
            // colNombre
            // 
            this.colNombre.AspectName = "Nombre";
            this.colNombre.MinimumWidth = 200;
            this.colNombre.Text = "Nombre";
            this.colNombre.Width = 258;
            // 
            // colPrecio
            // 
            this.colPrecio.AspectName = "Precio";
            this.colPrecio.AspectToStringFormat = "{0:$0.00}";
            this.colPrecio.IsVisible = global::gControls.Properties.Settings.Default.BuscProdTcolPrecio;
            this.colPrecio.MaximumWidth = 150;
            this.colPrecio.MinimumWidth = 70;
            this.colPrecio.Text = "Precio";
            this.colPrecio.Width = 101;
            // 
            // ColSync
            // 
            this.ColSync.AspectName = "";
            this.ColSync.IsEditable = false;
            this.ColSync.MaximumWidth = 30;
            this.ColSync.MinimumWidth = 26;
            this.ColSync.Searchable = false;
            this.ColSync.Sortable = false;
            this.ColSync.Text = "";
            this.ColSync.ToolTipText = "Estado de la sincronización";
            this.ColSync.Width = 30;
            // 
            // colStock
            // 
            this.colStock.AspectName = "Stock";
            this.colStock.AspectToStringFormat = "{0:0.00}";
            this.colStock.IsVisible = global::gControls.Properties.Settings.Default.BuscProdTcolStock;
            this.colStock.MaximumWidth = 150;
            this.colStock.MinimumWidth = 70;
            this.colStock.Text = "Stock";
            this.colStock.Width = 81;
            // 
            // colCodigo
            // 
            this.colCodigo.AspectName = "Codigo";
            this.colCodigo.IsVisible = global::gControls.Properties.Settings.Default.BuscProdTcolCodigo;
            this.colCodigo.MinimumWidth = 50;
            this.colCodigo.Text = "Código";
            this.colCodigo.Width = 96;
            // 
            // colDescripcion
            // 
            this.colDescripcion.AspectName = "Descripcion";
            this.colDescripcion.FillsFreeSpace = true;
            this.colDescripcion.IsVisible = global::gControls.Properties.Settings.Default.BuscProdTcolDescr;
            this.colDescripcion.MinimumWidth = 50;
            this.colDescripcion.Text = "Descripcion";
            this.colDescripcion.Width = 100;
            // 
            // colSerial
            // 
            this.colSerial.AspectName = "Serial";
            this.colSerial.IsVisible = global::gControls.Properties.Settings.Default.buscProdTcolSerial;
            this.colSerial.MinimumWidth = 50;
            this.colSerial.Text = "Serial";
            // 
            // colLink
            // 
            this.colLink.AspectName = "Link";
            this.colLink.IsVisible = global::gControls.Properties.Settings.Default.buscProdTColLinkVisible;
            this.colLink.MinimumWidth = 50;
            this.colLink.Text = "Link";
            // 
            // ProdImageList
            // 
            this.ProdImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ProdImageList.ImageStream")));
            this.ProdImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ProdImageList.Images.SetKeyName(0, "Producto");
            this.ProdImageList.Images.SetKeyName(1, "Categoria");
            this.ProdImageList.Images.SetKeyName(2, "Grupo");
            this.ProdImageList.Images.SetKeyName(3, "Servicio");
            this.ProdImageList.Images.SetKeyName(4, "Precio");
            this.ProdImageList.Images.SetKeyName(5, "Desincronizado");
            this.ProdImageList.Images.SetKeyName(6, "error");
            this.ProdImageList.Images.SetKeyName(7, "Sincronizacion");
            this.ProdImageList.Images.SetKeyName(8, "Sincronizado");
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Nombre";
            // 
            // olvColumn2
            // 
            this.olvColumn2.Text = "Precio";
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "Stock";
            // 
            // olvColumn4
            // 
            this.olvColumn4.Text = "Código";
            // 
            // buscadorProductos1
            // 
            this.buscadorProductos1.BloquearBarraHerramientas = false;
            this.buscadorProductos1.CustomActualizar = null;
            this.buscadorProductos1.CustomDatos = true;
            this.buscadorProductos1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buscadorProductos1.Imagen = global::gControls.Properties.Resources.Grupo;
            this.buscadorProductos1.ImagenTitulo = global::gControls.Properties.Resources.Grupo;
            this.buscadorProductos1.Location = new System.Drawing.Point(0, 334);
            this.buscadorProductos1.Name = "buscadorProductos1";
            this.buscadorProductos1.OcultarSiEstaVacio = false;
            this.buscadorProductos1.RequiereKey = false;
            this.buscadorProductos1.SeleccionGlobal = true;
            this.buscadorProductos1.Size = new System.Drawing.Size(843, 246);
            this.buscadorProductos1.TabIndex = 18;
            this.buscadorProductos1.Texto = "Buscar";
            this.buscadorProductos1.Titulo = "Grupos";
            this.buscadorProductos1.VerBotonActualizar = false;
            this.buscadorProductos1.VerBotonAgrupar = false;
            this.buscadorProductos1.VerBotonFiltro = false;
            this.buscadorProductos1.VerBotonGuardar = false;
            this.buscadorProductos1.VerBotonImagen = false;
            this.buscadorProductos1.VerBotonImprimir = false;
            this.buscadorProductos1.VerImagenCard = false;
            // 
            // BuscadorProductosTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buscadorProductos1);
            this.Controls.Add(this.tw1);
            this.Imagen = global::gControls.Properties.Resources.Categoria;
            this.ImagenTitulo = global::gControls.Properties.Resources.Buscar;
            this.Name = "BuscadorProductosTree";
            this.Size = new System.Drawing.Size(843, 580);
            this.Texto = "Buscar Productos y servicios";
            this.Titulo = "Buscar Productos y servicios";
            this.Controls.SetChildIndex(this.lblTitulo, 0);
            this.Controls.SetChildIndex(this.tw1, 0);
            this.Controls.SetChildIndex(this.buscadorProductos1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.tw1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.DataTreeListView tw1;
        private BrightIdeasSoftware.OLVColumn colNombre;
        private BrightIdeasSoftware.OLVColumn colPrecio;
        private BrightIdeasSoftware.OLVColumn colStock;
        private BrightIdeasSoftware.OLVColumn colCodigo;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private System.Windows.Forms.ImageList ProdImageList;
        private BuscadorProductos buscadorProductos1;
        private BrightIdeasSoftware.OLVColumn colDescripcion;
        private BrightIdeasSoftware.OLVColumn colSerial;
        private BrightIdeasSoftware.OLVColumn colLink;
        private BrightIdeasSoftware.OLVColumn ColSync;
    }
}
