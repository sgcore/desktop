﻿namespace gControls.Buscadores
{
    partial class BuscadorResumenTree
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscadorResumenTree));
            this.tw1 = new BrightIdeasSoftware.DataTreeListView();
            this.colNombre = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colCantidad = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colTotal = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colPrecioUnitario = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colCodigo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colSerial = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colLink = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ProdImageList = new System.Windows.Forms.ImageList(this.components);
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.tw1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.Location = new System.Drawing.Point(0, 31);
            this.lblTitulo.Size = new System.Drawing.Size(843, 28);
            // 
            // tw1
            // 
            this.tw1.AllColumns.Add(this.colNombre);
            this.tw1.AllColumns.Add(this.colCantidad);
            this.tw1.AllColumns.Add(this.colTotal);
            this.tw1.AllColumns.Add(this.colPrecioUnitario);
            this.tw1.AllColumns.Add(this.colCodigo);
            this.tw1.AllColumns.Add(this.colSerial);
            this.tw1.AllColumns.Add(this.colLink);
            this.tw1.AllowColumnReorder = true;
            this.tw1.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNombre,
            this.colTotal});
            this.tw1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tw1.DataSource = null;
            this.tw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tw1.FullRowSelect = true;
            this.tw1.LargeImageList = this.ProdImageList;
            this.tw1.Location = new System.Drawing.Point(0, 59);
            this.tw1.Name = "tw1";
            this.tw1.OwnerDraw = true;
            this.tw1.RootKeyValueString = "";
            this.tw1.ShowGroups = false;
            this.tw1.Size = new System.Drawing.Size(843, 521);
            this.tw1.SmallImageList = this.ProdImageList;
            this.tw1.TabIndex = 0;
            this.tw1.UseCellFormatEvents = true;
            this.tw1.UseCompatibleStateImageBehavior = false;
            this.tw1.UseFilterIndicator = true;
            this.tw1.UseFiltering = true;
            this.tw1.UseHotItem = true;
            this.tw1.View = System.Windows.Forms.View.Details;
            this.tw1.VirtualMode = true;
            // 
            // colNombre
            // 
            this.colNombre.AspectName = "Nombre";
            this.colNombre.MinimumWidth = 200;
            this.colNombre.Text = "Nombre";
            this.colNombre.Width = 400;
            // 
            // colCantidad
            // 
            this.colCantidad.AspectName = "Cantidad";
            this.colCantidad.AspectToStringFormat = "{0:0.00}";
            this.colCantidad.DisplayIndex = 1;
            this.colCantidad.IsVisible = false;
            this.colCantidad.MinimumWidth = 70;
            this.colCantidad.Text = "Cantidad";
            this.colCantidad.Width = 70;
            // 
            // colTotal
            // 
            this.colTotal.AspectName = "Total";
            this.colTotal.AspectToStringFormat = "{0:$0.00}";
            this.colTotal.MinimumWidth = 120;
            this.colTotal.Text = "Total";
            this.colTotal.Width = 150;
            // 
            // colPrecioUnitario
            // 
            this.colPrecioUnitario.AspectName = "UnitarioEstimado";
            this.colPrecioUnitario.AspectToStringFormat = "{0:$0.00}";
            this.colPrecioUnitario.DisplayIndex = 4;
            this.colPrecioUnitario.IsVisible = false;
            this.colPrecioUnitario.MinimumWidth = 80;
            this.colPrecioUnitario.Text = "Estimado";
            this.colPrecioUnitario.Width = 80;
            // 
            // colCodigo
            // 
            this.colCodigo.AspectName = "Codigo";
            this.colCodigo.DisplayIndex = 3;
            this.colCodigo.IsVisible = false;
            this.colCodigo.MinimumWidth = 80;
            this.colCodigo.Text = "Código";
            this.colCodigo.Width = 96;
            // 
            // colSerial
            // 
            this.colSerial.AspectName = "Serial";
            this.colSerial.DisplayIndex = 5;
            this.colSerial.IsVisible = false;
            this.colSerial.Text = "Serial";
            // 
            // colLink
            // 
            this.colLink.AspectName = "Link";
            this.colLink.DisplayIndex = 6;
            this.colLink.IsVisible = false;
            this.colLink.Text = "Link";
            // 
            // ProdImageList
            // 
            this.ProdImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ProdImageList.ImageStream")));
            this.ProdImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ProdImageList.Images.SetKeyName(0, "Producto");
            this.ProdImageList.Images.SetKeyName(1, "Categoria");
            this.ProdImageList.Images.SetKeyName(2, "Grupo");
            this.ProdImageList.Images.SetKeyName(3, "Servicio");
            this.ProdImageList.Images.SetKeyName(4, "Precio");
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Nombre";
            // 
            // olvColumn2
            // 
            this.olvColumn2.Text = "Precio";
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "Stock";
            // 
            // olvColumn4
            // 
            this.olvColumn4.Text = "Código";
            // 
            // BuscadorResumenTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tw1);
            this.Name = "BuscadorResumenTree";
            this.Size = new System.Drawing.Size(843, 580);
            this.Controls.SetChildIndex(this.lblTitulo, 0);
            this.Controls.SetChildIndex(this.tw1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.tw1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.DataTreeListView tw1;
        private BrightIdeasSoftware.OLVColumn colNombre;
        private BrightIdeasSoftware.OLVColumn colCantidad;
        private BrightIdeasSoftware.OLVColumn colTotal;
        private BrightIdeasSoftware.OLVColumn colCodigo;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private System.Windows.Forms.ImageList ProdImageList;
        private BrightIdeasSoftware.OLVColumn colPrecioUnitario;
        private BrightIdeasSoftware.OLVColumn colSerial;
        private BrightIdeasSoftware.OLVColumn colLink;
    }
}
