﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using BrightIdeasSoftware;
using gManager.Resumenes;

namespace gControls.Buscadores
{
    public partial class BuscadorMovCuentaCorr : ListBase
    {
        public BuscadorMovCuentaCorr()
        {
            InitializeComponent();
            Lista = lw1;
            VerBotonImprimir = true;
            IniciarEventos();
            ConfigurarColumnas();
            
        }
        private void CuentasCorr_FormatCell(object sender, FormatCellEventArgs e)
        {
            tuplaPagos customer = (tuplaPagos)e.Model;
            if (e.ColumnIndex == Col1.Index)
            {
                if (customer.Movimiento.EsIncrementoDeDeuda)
                {
                    e.SubItem.ForeColor = Color.PaleVioletRed;
                }
                else
                {
                    e.SubItem.ForeColor = Color.SteelBlue;
                }
                e.SubItem.Text = (Math.Abs(customer.Pago.Monto)).ToString("$0.00");


            }
            else if (e.ColumnIndex == Col2.Index)
            {
                if (customer.Saldo > 0)
                {
                    e.SubItem.ForeColor = Color.PaleVioletRed;
                }
                else
                {
                    e.SubItem.ForeColor = Color.SteelBlue;
                }
                e.SubItem.Text = (Math.Abs(customer.Saldo)).ToString("$0.00");
            }
        }
       void ConfigurarColumnas()
        {
           lw1.FormatCell += CuentasCorr_FormatCell;
           

            //nombre
            Col0.Text = "Movimiento";
            Col0.AspectName = "DescripcionMovimiento";
            Col0.MinimumWidth = 150;
            Col0.MaximumWidth = -1;
            Col0.ImageGetter = delegate(object rowObject)
            {
                tuplaPagos s = (tuplaPagos)rowObject;

                return s.Movimiento.Tipo.ToString();
            };
            //Col1
            Col1.Text = "Monto";
            Col1.AspectName = "MontoPago";
            Col1.AspectToStringFormat = "{0:$0.00}";
            Col1.MinimumWidth = 80;
            Col1.MaximumWidth = 250;
            Col1.Width = 80;
            Col1.FillsFreeSpace = false;
            Col1.ImageGetter = delegate(object rowObject)
            {
                tuplaPagos s = (tuplaPagos)rowObject;
                if (s.Movimiento.EsIncrementoDeDeuda) return "Abajo";
                return "Arriba";
            };

            ////Col2
            Col2.Text = "Saldo";
            Col2.AspectName = "MontoSaldo";
            Col2.AspectToStringFormat = "{0:$0.00}";
            Col1.MinimumWidth = 80;
            Col1.MaximumWidth = 250;
            Col1.Width = 80;
            Col2.FillsFreeSpace = false;
            Col2.ImageGetter = delegate(object rowObject)
            {
                tuplaPagos s = (tuplaPagos)rowObject;
                if (s.Saldo > 0) return "Abajo";
                return "Arriba";
            };
            //fecha
            colFecha.Text = "Fecha";
            colFecha.AspectName = "DescripcionFecha";
            colFecha.MinimumWidth = 150;
            colFecha.MaximumWidth = -1;
           
           
           

        }
        protected override void Actualizar()
        {
           
            Datos = gManager.CoreManager.Singlenton.getDestinatarioSeguimiento(Destinatario);
            base.Actualizar();
        }
        gcDestinatario _dest;
        public gcDestinatario Destinatario 
        {
            get
            {
                return _dest;
            }
            set
            {
                _dest = value;
                Actualizar();

            }
        }
        new public gcMovimiento Seleccionado
        {
            get {
                var m = base.Seleccionado as tuplaPagos;
                return m.Movimiento; }
        }
        protected override void seleccionarGlobal()
        {
            if (!SeleccionGlobal) return;
            CoreManager.Singlenton.seleccionarGenerico(Seleccionado);
        }
        
        
    }
}
