﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using BrightIdeasSoftware;

namespace gControls.Buscadores
{
    public partial class BuscadorResumenTree : ListBase
    {
        public BuscadorResumenTree():base()
        {
            
            InitializeComponent();
            base.FontHeight = 20;
            this.tw1.CanExpandGetter = delegate(object x)
            {
                return (x is gManager.CategoriaResumen && ((x as gManager.CategoriaResumen).SubCats.Count + (x as gManager.CategoriaResumen).SubObjetos.Count) > 0);
            };
            this.tw1.ChildrenGetter = delegate(object x)
            {
                gManager.CategoriaResumen dir = (gManager.CategoriaResumen)x;
                return dir.Childrens;
                
            };
           Lista.FormatCell += Productos_FormatCell;

            colNombre.ImageGetter = delegate(object rowObject)
            {
                if (rowObject is gManager.CategoriaResumen) return "Categoria";
                var o = rowObject as gManager.ProductoResumen;

                return o.Producto.Tipo.ToString();
            };
            HandleMenues( (o, e) =>
            {
                tw1.ContextMenuStrip = new ContextMenuStrip();
              

                tw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Expandir, "Expandir todos", "Expande todos los nodos del arbol"
                    , (oo, ee) => { tw1.ExpandAll(); }, Keys.Control | Keys.Right));
                tw1.ContextMenuStrip.Items.Add(crearMenuItem(Properties.Resources.Contraer, "Contraer todos", "Contrae a las categorias principales"
                    , (oo, ee) => { tw1.CollapseAll(); }, Keys.Control | Keys.Left));
            });
            VerBotonImprimir = true;
            IniciarEventos();
            EmpezarABuscar += BuscadorResumenTree_EmpezarABuscar;
           

            //privado grupos
           
        }

       

        void BuscadorResumenTree_EmpezarABuscar(object sender, ListBase.ItemsFoundedArgs e)
        {
            
            tw1.ExpandAll();
        }

        public override BrightIdeasSoftware.ObjectListView Lista
        {
            get {return tw1; }
        }
        private List<gcCaja> _cajas;
        protected override void Actualizar()
        {
            
            Datos = gManager.CoreManager.Singlenton.ResumenVentasPorCats(_cajas);
            base.Actualizar();
        }
        public void setCajas(List<gcCaja> c,bool actualizar=true)
        {
            _cajas = c;
            if(actualizar)Actualizar();
        }
        public override void setFiltro(gManager.Filtros.Filtro f)
        {
            _cajas = gManager.CoreManager.Singlenton.getCajas(f as gManager.Filtros.FiltroCaja);
        }
        
        
        private void Productos_FormatCell(object sender, FormatCellEventArgs e)
        {
            if (e.Model is gManager.CategoriaResumen)
            {
                e.SubItem.BackColor = Color.LightGray;

            }
            else
            {
                e.SubItem.Font = new Font("Arial", 10, FontStyle.Bold);

            }
           
        }
        new public gcObjeto Seleccionado
        {
            get
            {
                return null;//base.Seleccionado as gcObjeto;
            }
        }
      

       

       
    }
}
