﻿namespace gControls.Buscadores
{
    partial class BuscadorTratamientos
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscadorTratamientos));
            this.imageList1 = new System.Windows.Forms.ImageList();
            this.lw1 = new BrightIdeasSoftware.FastObjectListView();
            this.Col0 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.Col3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.FlatAppearance.BorderSize = 0;
            this.lblTitulo.Location = new System.Drawing.Point(0, 31);
            this.lblTitulo.Size = new System.Drawing.Size(692, 28);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Tratamiento");
            this.imageList1.Images.SetKeyName(1, "Recordatorio");
            this.imageList1.Images.SetKeyName(2, "Periodico");
            this.imageList1.Images.SetKeyName(3, "Tarea");
            this.imageList1.Images.SetKeyName(4, "Aviso");
            this.imageList1.Images.SetKeyName(5, "Terminado");
            this.imageList1.Images.SetKeyName(6, "Cancelado");
            this.imageList1.Images.SetKeyName(7, "Pendiente");
            // 
            // lw1
            // 
            this.lw1.AllColumns.Add(this.Col0);
            this.lw1.AllColumns.Add(this.Col1);
            this.lw1.AllColumns.Add(this.Col4);
            this.lw1.AllColumns.Add(this.Col2);
            this.lw1.AllColumns.Add(this.Col3);
            this.lw1.AlternateRowBackColor = System.Drawing.Color.Silver;
            this.lw1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Col0,
            this.Col1,
            this.Col2});
            this.lw1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lw1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lw1.ForeColor = System.Drawing.Color.Black;
            this.lw1.FullRowSelect = true;
            this.lw1.HideSelection = false;
            this.lw1.LargeImageList = this.imageList1;
            this.lw1.Location = new System.Drawing.Point(0, 59);
            this.lw1.Name = "lw1";
            this.lw1.OverlayImage.Transparency = 60;
            this.lw1.OverlayText.Text = "";
            this.lw1.OwnerDraw = true;
            this.lw1.ShowGroups = false;
            this.lw1.ShowImagesOnSubItems = true;
            this.lw1.Size = new System.Drawing.Size(692, 434);
            this.lw1.SmallImageList = this.imageList1;
            this.lw1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lw1.TabIndex = 20;
            this.lw1.UseCellFormatEvents = true;
            this.lw1.UseCompatibleStateImageBehavior = false;
            this.lw1.UseExplorerTheme = true;
            this.lw1.UseFilterIndicator = true;
            this.lw1.UseFiltering = true;
            this.lw1.UseHotItem = true;
            this.lw1.View = System.Windows.Forms.View.Details;
            this.lw1.VirtualMode = true;
            this.lw1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lw1_KeyDown);
            // 
            // Col0
            // 
            this.Col0.AspectName = "Titulo";
            this.Col0.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col0.HeaderForeColor = System.Drawing.Color.Black;
            this.Col0.MinimumWidth = 150;
            this.Col0.Sortable = false;
            this.Col0.Text = "Título";
            this.Col0.Width = 166;
            // 
            // Col1
            // 
            this.Col1.AspectName = "Descripcion";
            this.Col1.FillsFreeSpace = true;
            this.Col1.MinimumWidth = 150;
            this.Col1.Sortable = false;
            this.Col1.Text = "Descripcion";
            this.Col1.Width = 150;
            // 
            // Col4
            // 
            this.Col4.AspectName = "Estado";
            this.Col4.DisplayIndex = 1;
            this.Col4.IsVisible = false;
            this.Col4.MinimumWidth = 80;
            this.Col4.Sortable = false;
            this.Col4.Text = "Estado";
            this.Col4.Width = 111;
            // 
            // Col2
            // 
            this.Col2.AspectName = "FechaDesc";
            this.Col2.MinimumWidth = 50;
            this.Col2.Sortable = false;
            this.Col2.Text = "Fecha";
            this.Col2.Width = 132;
            // 
            // Col3
            // 
            this.Col3.AspectName = "UsuarioNombre";
            this.Col3.DisplayIndex = 4;
            this.Col3.IsVisible = false;
            this.Col3.MinimumWidth = 80;
            this.Col3.Sortable = false;
            this.Col3.Text = "Creado por";
            this.Col3.Width = 120;
            // 
            // BuscadorTratamientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lw1);
            this.Name = "BuscadorTratamientos";
            this.Size = new System.Drawing.Size(692, 493);
            this.Controls.SetChildIndex(this.lblTitulo, 0);
            this.Controls.SetChildIndex(this.lw1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lw1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private BrightIdeasSoftware.FastObjectListView lw1;
        private BrightIdeasSoftware.OLVColumn Col0;
        private BrightIdeasSoftware.OLVColumn Col1;
        private BrightIdeasSoftware.OLVColumn Col2;
        private BrightIdeasSoftware.OLVColumn Col3;
        private BrightIdeasSoftware.OLVColumn Col4;
    }
}
