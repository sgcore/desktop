﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using gManager.Filtros;
using BrightIdeasSoftware;

namespace gControls.Buscadores
{
    public partial class BuscadorTratamientos : ListBase
    {
        public BuscadorTratamientos():base()
        {
            InitializeComponent();
            Lista = lw1;
        

            IniciarEventos();
            ConfigurarColumnas();
            HandleBotones(ConfigurarBotones);
            CoreManager.Singlenton.TratamientoSeleccionado += o => { Actualizar(); };
        }
        void ConfigurarColumnas()
        {
            
            // 
            // Col0
            // 
            Col0.ImageGetter = delegate(object rowObject)
            {
                gManager.gcTratamiento s = (gManager.gcTratamiento)rowObject;
                return s.Tipo.ToString();
            };
            // 
            // Col1
            // 
           
            // 
            // Col2
            // 
            
            // 
            // Col3
            // 
            
            //
            //Col4
            //
           Col4.ImageGetter = delegate(object rowObject)
            {
                gManager.gcTratamiento s = (gManager.gcTratamiento)rowObject;
                return s.Estado.ToString();
            };
           Col0.GroupKeyGetter = delegate(object rowObject)
           {
               gcTratamiento s = (gcTratamiento)rowObject;
               return s.Tipo;
           };
           Col0.GroupKeyToTitleConverter = delegate(object groupKey)
           {
               return groupKey.ToString();
           };
           Col4.GroupKeyGetter = delegate(object rowObject)
           {
               gcTratamiento s = (gcTratamiento)rowObject;
               return new DateTime(s.Proximo.Year,s.Proximo.Month,s.Proximo.Day);
           };
           Col4.GroupKeyToTitleConverter = delegate(object groupKey)
           {
               return ((DateTime)groupKey) .ToString("MMMM dd");
           };
        }
       
        protected override void Actualizar()
        {
            if (CustomDatos && Destinatario!=null)
            {
                switch (tipot)
                {

                    case gcTratamiento.TipoTratamiento.Tratamiento:
                        setCustomDatos(Destinatario.Tratamientos);
                        
                        break;
                    case gcTratamiento.TipoTratamiento.Aviso:
                        setCustomDatos(Destinatario.Avisos);
                        
                        break;
                    case gcTratamiento.TipoTratamiento.Periodico:
                        setCustomDatos(Destinatario.Periodicos);
                        break;
                    case gcTratamiento.TipoTratamiento.Recordatorio:
                        setCustomDatos(Destinatario.Recordatorios);
                        break;
                    case gcTratamiento.TipoTratamiento.Tarea:
                        setCustomDatos(Destinatario.Tareas);
                        break;


                }
                return;
            }else{
                 Datos = CoreManager.Singlenton.getTratamientos(Filtro as FiltroTratamientos);
            }
           
            base.Actualizar();
        }
        protected void ConfigurarBotones(object ob, EventArgs ev)
        {

            if(VerBotonAddTratamiento)
                generarBoton(Properties.Resources.AgregarTratamiento, "Tratamiento", "Crear un nuevo tratamiento",
                    (o, e) =>{
                        if (Destinatario != null)
                        {
                            var t = new gManager.gcTratamiento(Destinatario, gManager.gcTratamiento.TipoTratamiento.Tratamiento);
                            Editar.EditarTratamiento(t);
                            Actualizar();

                        }
                    
                    }, gManager.gcUsuario.PermisoEnum.Encargado);
            if (VerBotonAddAlerta)
                generarBoton(Properties.Resources.Alarma, "Aviso", "Crear un alerta",
                    (o, e) =>
                    {
                        if (Destinatario != null)
                        {
                            var t = new gManager.gcTratamiento(Destinatario, gManager.gcTratamiento.TipoTratamiento.Aviso);
                            Editar.EditarTratamiento(t);
                            Actualizar();
                        }

                    }, gManager.gcUsuario.PermisoEnum.Encargado);
            if (VerBotonAddPeriodico)
                generarBoton(Properties.Resources.Alarma, "Periodico", "Crear un aviso periódico",
                    (o, e) =>
                    {
                        if (Destinatario != null)
                        {
                            var t = new gManager.gcTratamiento(Destinatario, gManager.gcTratamiento.TipoTratamiento.Periodico);
                            t.Fecha = DateTime.Now;
                           
                            Editar.EditarTratamiento(t);
                            Actualizar();
                        }

                    }, gManager.gcUsuario.PermisoEnum.Encargado);
            if (VerBotonAddTarea)
                generarBoton(Properties.Resources.Alarma, "Tarea", "Crear una tarea",
                    (o, e) =>
                    {
                        if (Destinatario != null)
                        {
                            var t = new gManager.gcTratamiento(Destinatario, gManager.gcTratamiento.TipoTratamiento.Tarea);
                            Editar.EditarTratamiento(t);
                            Actualizar();
                        }

                    }, gManager.gcUsuario.PermisoEnum.Encargado);
            if (VerBotonAddRecordatorio)
                generarBoton(Properties.Resources.Recordatorio, "Recordatorio", "Crear un recordatorio",
                    (o, e) =>
                    {
                        if (Destinatario != null)
                        {
                            var t = new gManager.gcTratamiento(Destinatario, gManager.gcTratamiento.TipoTratamiento.Recordatorio);
                            Editar.EditarTratamiento(t);
                            Actualizar();
                        }

                    }, gManager.gcUsuario.PermisoEnum.Encargado);
        }
        public void setBotonAgregar(gcTratamiento.TipoTratamiento t)
        {
            VerBotonAddAlerta = t == gcTratamiento.TipoTratamiento.Aviso;
            VerBotonAddPeriodico = t == gcTratamiento.TipoTratamiento.Periodico;
            VerBotonAddTarea = t == gcTratamiento.TipoTratamiento.Tarea;
            VerBotonAddTratamiento = t == gcTratamiento.TipoTratamiento.Tratamiento;
            VerBotonAddRecordatorio = t == gcTratamiento.TipoTratamiento.Recordatorio;
            iniciar();
        }
        public bool VerBotonAddTratamiento { get; set; }
        public bool VerBotonAddAlerta { get; set; }
        public bool VerBotonAddPeriodico { get; set; }
        public bool VerBotonAddTarea { get; set; }
        public bool VerBotonAddRecordatorio { get; set; }
        gcTratamiento.TipoTratamiento tipot;
        public void setDestinatario(gcDestinatario d, gcTratamiento.TipoTratamiento t)
        {
            Destinatario = d;
            CustomDatos = true;
            tipot = t;
            setBotonAgregar(t);
           
        }

        public gcDestinatario Destinatario { get; set; }
        private void ConfigMenuPacientes()
        {
            var contmnu = new System.Windows.Forms.ContextMenuStrip();
            //menu

            if (RequisitoPermiso(gcUsuario.PermisoEnum.Contador))
                contmnu.Items.Add(crearMenuItem(Properties.Resources.Editar, "Editar", "Edita la tarea seleccionada",
               (o, e) => { if (Seleccionado != null)Editar.EditarTratamiento(Seleccionado); }, Keys.Alt | Keys.E));
            lw1.ContextMenuStrip=contmnu;
            //if (RequisitoPermiso(gcUsuario.PermisoEnum.Encargado))
            //    contmnu.Items.Add(crearMenuItem(Properties.Resources.Ver, "Ver Detalles", "Muestra un seguimiento detallado del paciente.",
            //   (o, e) => { if (Seleccionado != null)ver.VerDestinatarios(Seleccionado); }, Keys.Control | Keys.E));
            //SetContextMenu(contmnu);

        }
        new public gcTratamiento Seleccionado
        {
            get
            {
                return base.Seleccionado as gcTratamiento;
            }
        }
        public override void iniciar()
        {
            base.iniciar();
            ConfigMenuPacientes();
            //MostrarBotonFiltro(RequisitoPermiso(gcUsuario.PermisoEnum.Encargado));
            //MostrarBotonFiltro(RequisitoPermiso(gcUsuario.PermisoEnum.Encargado));
            //MostrarBotonFiltro(RequisitoPermiso(gcUsuario.PermisoEnum.Encargado));
            
        }

        private void lw1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Seleccionado != null && (RequisitoPermiso(gcUsuario.PermisoEnum.Administrador) || Seleccionado.UsuarioID == CoreManager.Singlenton.Usuario.Id))
                {
                    Seleccionado.DeleteMe();
                    Actualizar();
                }else if(Seleccionados.Count>0 && (RequisitoPermiso(gcUsuario.PermisoEnum.Administrador) || (Seleccionado.UsuarioID == CoreManager.Singlenton.Usuario.Id && (DateTime.Now.Subtract(Seleccionado.Fecha).Days<1)))){
                    foreach(gcTratamiento ss in Seleccionados)
                        ss.DeleteMe();
                    Actualizar();
                        
                }
            }
            
        }
        
        
        
        
      
        
        


    }
}
