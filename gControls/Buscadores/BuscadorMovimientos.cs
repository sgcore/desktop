﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BrightIdeasSoftware;
using gManager;
using gManager.Filtros;

namespace gControls.Buscadores
{
    public partial class BuscadorMovimientos : ListBase
    {
        public event EventHandler CambioFiltro;
        public BuscadorMovimientos()
        {
            InitializeComponent();
            Lista = lw1;
            VerBotonAgrupar = true;
            IniciarEventos();
           // lw1.FormatCell += Movimientos_FormatCell;
            
             Col0.ImageGetter = delegate(object rowObject)
            {
                gcMovimiento s = (gcMovimiento)rowObject;
                return (int)s.Estado + 9;
            };
             Col1.GroupKeyGetter = delegate(object rowObject)
             {
                 gcMovimiento s = (gcMovimiento)rowObject;
                 return s.Tipo;
             };
             Col1.GroupKeyToTitleConverter = delegate(object groupKey)
             {
                 return groupKey.ToString();
             };
            Col1.ImageGetter = delegate(object rowObject)
            {
                gcMovimiento s = (gcMovimiento)rowObject;
                return s.Tipo;
            };

             Col2.ImageGetter = delegate(object rowObject)
            {
                gcMovimiento s = (gcMovimiento)rowObject;
                if (s.Cotizacion != 1)
                    return "Cotizacion";
                if (s.PagosEnCuentas.Monto > 0)
                    return "Cuenta";
                if (s.PagosEnTarjeta.Monto > 0)
                    return "Tarjeta";
                if (s.PagosEnCheques.Monto > 0)
                    return "Cheque";

                return "Efectivo";
            };
            gManager.CoreManager.Singlenton.MovimientoEliminado += Singlenton_MovimientoAgregado;
            gManager.CoreManager.Singlenton.MovimientoAgregado += Singlenton_MovimientoAgregado;
           // HandleBotones(iniciarBotones);
        }
        private void iniciarBotones(object ob, EventArgs ev)
        {
            var b = generarBoton(Properties.Resources.Grupo, "", "Agrupar Movimientos"
    , null, gcUsuario.PermisoEnum.Visitante);
            b.Checked = lw1.ShowGroups;
            b.CheckedChanged += (o, e) => { lw1.ShowGroups = b.Checked; if (b.Checked)Actualizar(); };
            b.CheckOnClick = true;
            b.Visible = true;
         

        }
        public override void iniciar()
        {
            base.iniciar();
        }
        void Singlenton_MovimientoAgregado(gManager.gcMovimiento m)
        {
            
                Actualizar();
        }
        protected override void Actualizar()
        {
            if (MovimientosCajaACtual)
            {
                Datos = CoreManager.Singlenton.getMovimientosCajaActual();

                FiltroMovimiento f=Filtro as FiltroMovimiento;
                if (f == null) f = new FiltroMovimiento();
                f.porCaja = true;
                f.CajaInicial = CoreManager.Singlenton.CajaActual.Caja;
                f.CajaFinal = CoreManager.Singlenton.CajaActual.Caja;
                Filtro = f;

            }
            else if (Filtro is FiltroCaja)
            {
                var l = new List<gcMovimiento>();
                var _lcajas = CoreManager.Singlenton.getCajas(Filtro as FiltroCaja);
                   
                foreach (var c in _lcajas)
                {
                    l.AddRange(c.Movimientos);
                }
                //se le asigna sin cambiar el tipo de filtro
                (Filtro as FiltroCaja).ListaDeCajasCustom = _lcajas;
                 
               
                Datos = l;
                base.Actualizar();
                return;
            }
            Datos = CoreManager.Singlenton.getMovimientos(Filtro as FiltroMovimiento);
            base.Actualizar();
        }
       
        public void setCajas(List<gcCaja> l)
        {
            MovimientosCajaACtual = false;
            Filtro = new FiltroCaja(l);
            Actualizar();
        }
       

        protected override void EditarFiltro()
        {
            //if (_lcajas != null)
            //{
            //    if (Filtro == null) Filtro = new FiltroCaja(_lcajas);
            //}
            if (Filtro == null) Filtro = new FiltroMovimiento();
           
            base.EditarFiltro();
            if (CambioFiltro != null) CambioFiltro(Filtro, null);
            
        }
        new public gcMovimiento Seleccionado
        {
            get
            {
                return base.Seleccionado as gcMovimiento;
            }
        }
        private void Movimientos_FormatCell(object sender, FormatCellEventArgs e)
        {
            gcMovimiento customer = (gcMovimiento)e.Model;
            if (e.ColumnIndex == this.Col2.Index)
            {
                if (customer.EsMovimientoDinero)
                    e.SubItem.Text = customer.TotalPagado.ToString("0.00");
                else if (customer.EsMovimientoMercaSolo)
                {
                    e.SubItem.Text = "no hay";
                }
                else
                {
                    e.SubItem.Text = customer.TotalCotizado.ToString("0.00");
                }


            }

        }
        public void HideFirsColumn()
        {
            Col0.MinimumWidth = -1;
            Col0.MaximumWidth = 0;
            Col0.Width = 0;
            Col0.FillsFreeSpace = false;
            lw1.RebuildColumns();

        }
        public bool MovimientosCajaACtual { get; set; }

    }
}
