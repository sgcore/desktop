﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using ZedGraph;
using BrightIdeasSoftware;

namespace gControls.Buscadores
{
    public partial class BuscadorCajas : ListBase
    {
        public BuscadorCajas()
        {
            InitializeComponent();
            Lista = lw1;
            IniciarEventos();
            ConfigurarColumnas();
            var b = generarBoton(Properties.Resources.Estadisticas, "", "Mostrar/ocultar las estadisticas",null, gcUsuario.PermisoEnum.Contador);
            b.CheckOnClick = true;
            b.CheckedChanged += (o, e) =>
            {
                z1.Visible = b.Checked;
            };
          
            
        }
        protected override void Actualizar()
        {
            Datos = gManager.CoreManager.Singlenton.getCajas(Filtro as gManager.Filtros.FiltroCaja);
            base.Actualizar();
            CreateGraph(z1, Datos as List<gcCaja>);
        }
        //protected override void Imprimir()
        //{
        //    if(Seleccionados.Count>0)
        //    CoreManager.Singlenton.GenerarInformeHtml(html.InformeCaja(new gManager.ResumenCajas(Seleccionados)));
        //}
         void ConfigurarColumnas()
        {
            lw1.ShowGroups = true;

            //ExposedLW.FormatCell += Movimientos_FormatCell;

            Col0.GroupKeyGetter = delegate(object rowObject)
            {
                gcCaja caja = (gcCaja)rowObject;
                return new DateTime(caja.FechaDeFinalizacion.Year, caja.FechaDeFinalizacion.Month, 1);
            };
            Col0.GroupKeyToTitleConverter = delegate(object groupKey)
            {
                return ((DateTime)groupKey).ToString("MMMM yyyy");
            };

            //nombre
            Col0.Text = "Fecha";
            Col0.AspectName = "DescripcionFechaCierre";
            Col0.MinimumWidth = 100;
            Col0.MaximumWidth = -1;
            Col0.FillsFreeSpace = false;
            //Col1
            Col1.Text = "Numero";
            Col1.AspectName = "Caja";
            Col1.AspectToStringFormat = "{0:0000}";
            Col1.MinimumWidth = 50;
            Col1.MaximumWidth = -1;
            Col1.FillsFreeSpace = false;
            

            ////Col2
            Col2.Text = "Recaudacion";
            Col2.AspectName = "DescripcionRecaudacion";
            
            Col2.MinimumWidth = 60;
            Col2.MaximumWidth = -1;
            Col2.FillsFreeSpace = false;
            //Col3
            Col3.Text = "Deposito Inicial";
            Col3.AspectName = "DescripcionInicial";
            Col3.FillsFreeSpace = false;
            Col3.MinimumWidth = 100;
            Col3.MaximumWidth = -1;
            ////Col4
            Col4.Text = "Usuario";
            Col4.AspectName = "CerradaPor";
            Col4.MinimumWidth = -1;
            Col4.MaximumWidth = -1;
            Col4.FillsFreeSpace = true;


        }
        new public gcCaja Seleccionado
        {
            get
            {
                return base.Seleccionado as gcCaja;
            }
        }
        new public List<gcCaja> Seleccionados
        {
            get
            {
                var l=(lw1 as FastObjectListView).SelectedObjects;
                return l.OfType<gcCaja>().ToList<gcCaja>();
            }
        }
        protected override void EditarFiltro()
        {
            if (Filtro == null) Filtro = new gManager.Filtros.FiltroCaja(gManager.Filtros.FiltroCaja.TipoFiltroCaja.CajaActual);
            base.EditarFiltro();
        }
        private void CreateGraph(ZedGraphControl zg1, List<gManager.gcCaja> l)
        {
            // Get a reference to the GraphPane


            zg1.GraphPane.CurveList.Clear();
            zg1.GraphPane.GraphObjList.Clear();
            zg1.Invalidate();


            GraphPane myPane = zg1.GraphPane;
            
            // Set the titles
            myPane.Title.Text = "Recaudación";
            myPane.XAxis.Title.Text = "Fecha";


            // Make up some random data points
            // double x , y;
            PointPairList rec = new PointPairList();
           
            //ordenar
            l.Sort(delegate(gcCaja a, gcCaja b)
            {
                return a.Caja.CompareTo(b.Caja);
               
            });
            foreach (gcCaja c in l)
            {
                rec.Add((double)new XDate(c.FechaDeInicio), (double)c.Produccion);
            }



            // Generate a red curve with diamond
            // symbols, and "My Curve" in the legend
            myPane.CurveList.Clear();
            //CurveItem myCurve = myPane.AddCurve("Cantidad ",
            //      list, Color.Red, SymbolType.Diamond);
            myPane.AddCurve("Recaudación", rec, Color.Violet);
           

            // Set the XAxis to date type
            myPane.XAxis.Type = AxisType.Date;

            // Tell ZedGraph to refigure the axes since the data 
            // have changed
            zg1.AxisChange();
        }
    }
}
