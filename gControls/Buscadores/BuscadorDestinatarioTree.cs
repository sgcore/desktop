﻿using gManager;

namespace gControls.Buscadores
{
    public partial class BuscadorDestinatarioTree : ListBase
    {
        public BuscadorDestinatarioTree()
        {
            InitializeComponent();
            this.tw1.CanExpandGetter = delegate(object x)
            {
                return (x is gcDestinatario && (x as gcDestinatario).Pacientes.Count > 0);
            };
            this.tw1.ChildrenGetter = delegate(object x)
            {
                gcDestinatario dir = (gcDestinatario)x;
                return dir.Pacientes;
            };
            colNombre.ImageGetter = delegate(object rowObject)
            {
                gcObjeto s = (gcObjeto)rowObject;
                return s.Tipo.ToString();
            };
        }
    }
}
