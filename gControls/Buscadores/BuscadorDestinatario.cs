﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using BrightIdeasSoftware;
using gManager.Filtros;

namespace gControls.Buscadores
{
    public partial class BuscadorDestinatario : ListBase
    {
        public BuscadorDestinatario()
        {
            InitializeComponent();
            Lista = lw1;
          
            IniciarEventos();
            lw1.FormatCell += destinatario_FormatCell;
            Col0.ImageGetter = delegate(object rowObject)
            {
                gManager.gcDestinatario s = (gManager.gcDestinatario)rowObject;
                return s.Tipo.ToString();
            };
            gManager.CoreManager.Singlenton.DestinatarioCreado += Singlenton_DestinatarioCreado;
            
        }
        protected void ConfigurarMenues(object ob, EventArgs ev)
        {
            var contmnu = new System.Windows.Forms.ContextMenuStrip();
            //menu

            if (RequisitoPermiso(gcUsuario.PermisoEnum.Contador))
                contmnu.Items.Add(crearMenuItem(Properties.Resources.Editar, "Editar", "Edita Destinatario",
               (o, e) => { if (Seleccionado != null)Editar.EditarDestinatario(Seleccionado); }, Keys.None));
            if (RequisitoPermiso(gcUsuario.PermisoEnum.Encargado))
            {
                var np = crearMenuItem(Properties.Resources.Paciente, "Pacientes", "Agrega un nuevo paciente o selecciona uno existente", null, Keys.None);
               
                np.DropDownOpening += (oo, ee) =>
                {
                    np.DropDownItems.Clear();
                    np.DropDownItems.Add(crearMenuItem(Properties.Resources.NuevoDestinatario, "Crear Paciente", "Agrega un nuevo paciente",
            Acciones.CrearPaciente, Keys.None));
                    if (Seleccionado != null && Seleccionado.Pacientes.Count > 0)
                    {
                        np.DropDownItems.Add(new ToolStripSeparator());
                        foreach (gcDestinatario p in Seleccionado.Pacientes)
                            np.DropDownItems.Add(
                                crearMenuItem(Properties.Resources.Paciente,p.Nombre, "",
                 (o, e) => { CoreManager.Singlenton.seleccionarDestinatario(p); }, Keys.None)
                                );

                    };
                };
              
                
                
                contmnu.Items.Add(np);
                var na = crearMenuItem(Properties.Resources.Alarma, "Avisos", "Crear Avisos para este destinatario",null, Keys.None, true);
                na.DropDownItems.Add(crearMenuItem(Properties.Resources.Periodico, "Periódico", "Crea un aviso periódico", (op, ep) =>
                {
                    if (Seleccionado != null)
                    {
                        Editar.EditarTratamiento(new gcTratamiento(Seleccionado, gcTratamiento.TipoTratamiento.Periodico));
                        Actualizar();
                    }
                }));
                na.DropDownItems.Add(crearMenuItem(Properties.Resources.Alarma, "Recordatorio", "Crea un aviso recordatorio", (op, ep) =>
                {
                    if (Seleccionado != null)
                    {
                        Editar.EditarTratamiento(new gcTratamiento(Seleccionado, gcTratamiento.TipoTratamiento.Recordatorio));
                        Actualizar();
                    }
                }));
                contmnu.Items.Add(na);
            }
              
            lw1.ContextMenuStrip = contmnu;
            
        }
        new public gcDestinatario Seleccionado
        {
            get { return base.Seleccionado as gcDestinatario; }
        }
        void Singlenton_DestinatarioCreado(gcDestinatario d)
        {
            Actualizar();
        }
        public BuscadorDestinatario(gManager.gcDestinatario.DestinatarioTipo t)
        {
            InitializeComponent();
            Lista = lw1;
            IniciarEventos();
            VerBotonFiltro = false;
            lw1.FormatCell += destinatario_FormatCell;
            Col0.ImageGetter = delegate(object rowObject)
            {
                gManager.gcDestinatario s = (gManager.gcDestinatario)rowObject;
                return s.Tipo.ToString();
            };
            var f = new FiltroDestinatario();
            f.Tipos.Add(t);
            Filtro = f;
            gManager.CoreManager.Singlenton.DestinatarioCreado += Singlenton_DestinatarioCreado;
            if(t==gcDestinatario.DestinatarioTipo.Cliente)
                HandleMenues(ConfigurarMenues);
        }
        private void destinatario_FormatCell(object sender, FormatCellEventArgs e)
        {
            if (e.ColumnIndex == this.Col1.Index)
            {
                gcDestinatario customer = (gcDestinatario)e.Model;

                if (customer.SaldoEnCuenta > 0)
                {
                    e.SubItem.ForeColor = Color.Red;
                    e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                    // e.SubItem.ImageSelector = Properties.Resources.Down;
                }
                else if (customer.SaldoEnCuenta < 0)
                {
                    e.SubItem.ForeColor = Color.DodgerBlue;
                    // e.SubItem.ImageSelector = Properties.Resources.Up;
                    e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                }
                else
                {
                    e.SubItem.ForeColor = Color.LightGray;
                }

            }
        }
        protected override void EditarFiltro()
        {
            if (Filtro == null) Filtro = new FiltroDestinatario();
            base.EditarFiltro();
        }
        protected override void Actualizar()
        {
            
            Datos = CoreManager.Singlenton.getDestinatarios(Filtro as gManager.Filtros.FiltroDestinatario);
            base.Actualizar();
        }
        
    }
}
