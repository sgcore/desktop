﻿namespace gControls.Editores
{
    partial class EditorUsuarios
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.cmbPermiso = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pbp = new System.Windows.Forms.PictureBox();
            this.btCentral = new gControls.BotonAccion();
            this.btResponsable = new gControls.BotonAccion();
            this.lblcentral = new System.Windows.Forms.Label();
            this.lblRespo = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescrip = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbp)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(74, 24);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(231, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(24, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(24, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Usuario";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(74, 60);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(117, 20);
            this.txtUser.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(7, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Contraseña";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(74, 99);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(117, 20);
            this.txtPass.TabIndex = 2;
            // 
            // cmbPermiso
            // 
            this.cmbPermiso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermiso.FormattingEnabled = true;
            this.cmbPermiso.Location = new System.Drawing.Point(392, 27);
            this.cmbPermiso.Name = "cmbPermiso";
            this.cmbPermiso.Size = new System.Drawing.Size(106, 21);
            this.cmbPermiso.TabIndex = 5;
            this.cmbPermiso.SelectedIndexChanged += new System.EventHandler(this.cmbPermiso_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(392, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Permiso";
            // 
            // pbp
            // 
            this.pbp.BackColor = System.Drawing.Color.Transparent;
            this.pbp.Image = global::gControls.Properties.Resources.Ver;
            this.pbp.Location = new System.Drawing.Point(344, 15);
            this.pbp.Name = "pbp";
            this.pbp.Size = new System.Drawing.Size(42, 42);
            this.pbp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbp.TabIndex = 8;
            this.pbp.TabStop = false;
            // 
            // btCentral
            // 
            this.btCentral.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCentral.FormContainer = null;
            this.btCentral.GlobalHotKey = false;
            this.btCentral.HacerInvisibleAlDeshabilitar = false;
            this.btCentral.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCentral.Image = global::gControls.Properties.Resources.Sucursal;
            this.btCentral.Location = new System.Drawing.Point(10, 140);
            this.btCentral.Name = "btCentral";
            this.btCentral.Permiso = gManager.gcUsuario.PermisoEnum.Administrador;
            this.btCentral.RegistradoEnForm = false;
            this.btCentral.Size = new System.Drawing.Size(84, 47);
            this.btCentral.TabIndex = 3;
            this.btCentral.Text = "Casa Central";
            this.btCentral.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btCentral.Titulo = "Sucursal asignada al usuario";
            this.btCentral.ToolTipDescripcion = "Las  operaciones realizadas por este usuario tendran como casa central la sucursa" +
    "l seleccionada";
            this.btCentral.UseVisualStyleBackColor = true;
            this.btCentral.Click += new System.EventHandler(this.btCentral_Click);
            // 
            // btResponsable
            // 
            this.btResponsable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btResponsable.FormContainer = null;
            this.btResponsable.GlobalHotKey = false;
            this.btResponsable.HacerInvisibleAlDeshabilitar = false;
            this.btResponsable.HotKey = System.Windows.Forms.Shortcut.None;
            this.btResponsable.Image = global::gControls.Properties.Resources.Deposito;
            this.btResponsable.Location = new System.Drawing.Point(10, 193);
            this.btResponsable.Name = "btResponsable";
            this.btResponsable.Permiso = gManager.gcUsuario.PermisoEnum.Administrador;
            this.btResponsable.RegistradoEnForm = false;
            this.btResponsable.Size = new System.Drawing.Size(84, 47);
            this.btResponsable.TabIndex = 4;
            this.btResponsable.Text = "Responsable";
            this.btResponsable.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btResponsable.Titulo = "Responsable del dinero";
            this.btResponsable.ToolTipDescripcion = "Los ingresos y extracciones del usuario se realizarán con el responsable seleccio" +
    "nado";
            this.btResponsable.UseVisualStyleBackColor = true;
            this.btResponsable.Click += new System.EventHandler(this.btResponsable_Click);
            // 
            // lblcentral
            // 
            this.lblcentral.AutoSize = true;
            this.lblcentral.BackColor = System.Drawing.Color.Transparent;
            this.lblcentral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcentral.Location = new System.Drawing.Point(100, 157);
            this.lblcentral.Name = "lblcentral";
            this.lblcentral.Size = new System.Drawing.Size(166, 16);
            this.lblcentral.TabIndex = 11;
            this.lblcentral.Text = "Seleccione una central";
            // 
            // lblRespo
            // 
            this.lblRespo.AutoSize = true;
            this.lblRespo.BackColor = System.Drawing.Color.Transparent;
            this.lblRespo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRespo.Location = new System.Drawing.Point(100, 208);
            this.lblRespo.Name = "lblRespo";
            this.lblRespo.Size = new System.Drawing.Size(197, 16);
            this.lblRespo.TabIndex = 12;
            this.lblRespo.Text = "Seleccione un responsable";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(235, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Descripción";
            // 
            // txtDescrip
            // 
            this.txtDescrip.Location = new System.Drawing.Point(304, 84);
            this.txtDescrip.Multiline = true;
            this.txtDescrip.Name = "txtDescrip";
            this.txtDescrip.Size = new System.Drawing.Size(231, 57);
            this.txtDescrip.TabIndex = 13;
            // 
            // EditorUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDescrip);
            this.Controls.Add(this.lblRespo);
            this.Controls.Add(this.lblcentral);
            this.Controls.Add(this.btResponsable);
            this.Controls.Add(this.btCentral);
            this.Controls.Add(this.pbp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbPermiso);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNombre);
            this.Name = "EditorUsuarios";
            this.Size = new System.Drawing.Size(557, 259);
            ((System.ComponentModel.ISupportInitialize)(this.pbp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.ComboBox cmbPermiso;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pbp;
        private BotonAccion btCentral;
        private BotonAccion btResponsable;
        private System.Windows.Forms.Label lblcentral;
        private System.Windows.Forms.Label lblRespo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDescrip;
    }
}
