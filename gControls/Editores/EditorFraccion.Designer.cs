﻿namespace gControls.Editores
{
    partial class EditorFraccion
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.nFracFrac = new System.Windows.Forms.NumericUpDown();
            this.nPrecioFrac = new System.Windows.Forms.NumericUpDown();
            this.nCantFrac = new System.Windows.Forms.NumericUpDown();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbFraccion = new System.Windows.Forms.RadioButton();
            this.panelFraccion = new System.Windows.Forms.Panel();
            this.lblivafraccion = new System.Windows.Forms.Label();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.lblTotalIva = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblPrecioUnitario = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nCantTotal = new System.Windows.Forms.NumericUpDown();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.nTotalTotal = new System.Windows.Forms.NumericUpDown();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.rbTotal = new System.Windows.Forms.RadioButton();
            this.panelUnitario = new System.Windows.Forms.Panel();
            this.lblivaUnitario = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPrecioTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nPrecioUnitario = new System.Windows.Forms.NumericUpDown();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.nCantUnitario = new System.Windows.Forms.NumericUpDown();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.rbPrecioyCantidad = new System.Windows.Forms.RadioButton();
            this.chkConIva = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.nFracFrac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nPrecioFrac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCantFrac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelFraccion.SuspendLayout();
            this.panelTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nCantTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panelUnitario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nPrecioUnitario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCantUnitario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "---------------------";
            // 
            // nFracFrac
            // 
            this.nFracFrac.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nFracFrac.Location = new System.Drawing.Point(48, 54);
            this.nFracFrac.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nFracFrac.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nFracFrac.Name = "nFracFrac";
            this.nFracFrac.Size = new System.Drawing.Size(63, 22);
            this.nFracFrac.TabIndex = 1;
            this.nFracFrac.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nFracFrac.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nFracFrac.Enter += new System.EventHandler(this.nPre_Enter);
            // 
            // nPrecioFrac
            // 
            this.nPrecioFrac.DecimalPlaces = 3;
            this.nPrecioFrac.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nPrecioFrac.Location = new System.Drawing.Point(170, 31);
            this.nPrecioFrac.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nPrecioFrac.Name = "nPrecioFrac";
            this.nPrecioFrac.Size = new System.Drawing.Size(95, 26);
            this.nPrecioFrac.TabIndex = 2;
            this.nPrecioFrac.Enter += new System.EventHandler(this.nPre_Enter);
            this.nPrecioFrac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDown1_KeyPress);
            // 
            // nCantFrac
            // 
            this.nCantFrac.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nCantFrac.Location = new System.Drawing.Point(48, 13);
            this.nCantFrac.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nCantFrac.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nCantFrac.Name = "nCantFrac";
            this.nCantFrac.Size = new System.Drawing.Size(63, 22);
            this.nCantFrac.TabIndex = 0;
            this.nCantFrac.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nCantFrac.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nCantFrac.Enter += new System.EventHandler(this.nPre_Enter);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::gControls.Properties.Resources.Precio;
            this.pictureBox2.Location = new System.Drawing.Point(140, 33);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::gControls.Properties.Resources.Fraccion;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // rbFraccion
            // 
            this.rbFraccion.AutoSize = true;
            this.rbFraccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbFraccion.Location = new System.Drawing.Point(13, 235);
            this.rbFraccion.Name = "rbFraccion";
            this.rbFraccion.Size = new System.Drawing.Size(312, 20);
            this.rbFraccion.TabIndex = 5;
            this.rbFraccion.Text = "Ingrese la fracción y el precio de fracción";
            this.rbFraccion.UseVisualStyleBackColor = true;
            this.rbFraccion.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // panelFraccion
            // 
            this.panelFraccion.Controls.Add(this.lblivafraccion);
            this.panelFraccion.Controls.Add(this.nCantFrac);
            this.panelFraccion.Controls.Add(this.nFracFrac);
            this.panelFraccion.Controls.Add(this.nPrecioFrac);
            this.panelFraccion.Controls.Add(this.pictureBox2);
            this.panelFraccion.Controls.Add(this.label3);
            this.panelFraccion.Controls.Add(this.pictureBox1);
            this.panelFraccion.Enabled = false;
            this.panelFraccion.Location = new System.Drawing.Point(8, 258);
            this.panelFraccion.Name = "panelFraccion";
            this.panelFraccion.Size = new System.Drawing.Size(317, 87);
            this.panelFraccion.TabIndex = 2;
            // 
            // lblivafraccion
            // 
            this.lblivafraccion.AutoSize = true;
            this.lblivafraccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblivafraccion.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblivafraccion.Location = new System.Drawing.Point(159, 60);
            this.lblivafraccion.Name = "lblivafraccion";
            this.lblivafraccion.Size = new System.Drawing.Size(136, 13);
            this.lblivafraccion.TabIndex = 16;
            this.lblivafraccion.Text = "precio fraccion sin IVA";
            // 
            // panelTotal
            // 
            this.panelTotal.Controls.Add(this.lblTotalIva);
            this.panelTotal.Controls.Add(this.label4);
            this.panelTotal.Controls.Add(this.lblPrecioUnitario);
            this.panelTotal.Controls.Add(this.label1);
            this.panelTotal.Controls.Add(this.nCantTotal);
            this.panelTotal.Controls.Add(this.pictureBox5);
            this.panelTotal.Controls.Add(this.nTotalTotal);
            this.panelTotal.Controls.Add(this.pictureBox3);
            this.panelTotal.Controls.Add(this.pictureBox4);
            this.panelTotal.Enabled = false;
            this.panelTotal.Location = new System.Drawing.Point(8, 142);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(317, 87);
            this.panelTotal.TabIndex = 1;
            // 
            // lblTotalIva
            // 
            this.lblTotalIva.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalIva.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblTotalIva.Location = new System.Drawing.Point(3, 34);
            this.lblTotalIva.Name = "lblTotalIva";
            this.lblTotalIva.Size = new System.Drawing.Size(89, 40);
            this.lblTotalIva.TabIndex = 17;
            this.lblTotalIva.Text = "Precio total sin IVA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(98, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Precio Unitario";
            // 
            // lblPrecioUnitario
            // 
            this.lblPrecioUnitario.AutoSize = true;
            this.lblPrecioUnitario.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioUnitario.Location = new System.Drawing.Point(114, 50);
            this.lblPrecioUnitario.Name = "lblPrecioUnitario";
            this.lblPrecioUnitario.Size = new System.Drawing.Size(71, 24);
            this.lblPrecioUnitario.TabIndex = 14;
            this.lblPrecioUnitario.Text = "$0.000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 13;
            // 
            // nCantTotal
            // 
            this.nCantTotal.DecimalPlaces = 3;
            this.nCantTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nCantTotal.Location = new System.Drawing.Point(200, 3);
            this.nCantTotal.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nCantTotal.Name = "nCantTotal";
            this.nCantTotal.Size = new System.Drawing.Size(95, 26);
            this.nCantTotal.TabIndex = 1;
            this.nCantTotal.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nCantTotal.ValueChanged += new System.EventHandler(this.nTotal_ValueChanged);
            this.nCantTotal.Enter += new System.EventHandler(this.nPre_Enter);
            this.nCantTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDown1_KeyPress);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::gControls.Properties.Resources.Cantidad;
            this.pictureBox5.Location = new System.Drawing.Point(170, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(24, 24);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            // 
            // nTotalTotal
            // 
            this.nTotalTotal.DecimalPlaces = 3;
            this.nTotalTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nTotalTotal.Location = new System.Drawing.Point(64, 3);
            this.nTotalTotal.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nTotalTotal.Name = "nTotalTotal";
            this.nTotalTotal.Size = new System.Drawing.Size(95, 26);
            this.nTotalTotal.TabIndex = 0;
            this.nTotalTotal.ValueChanged += new System.EventHandler(this.nTotal_ValueChanged);
            this.nTotalTotal.Enter += new System.EventHandler(this.nPre_Enter);
            this.nTotalTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDown1_KeyPress);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::gControls.Properties.Resources.Precio;
            this.pictureBox3.Location = new System.Drawing.Point(34, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(24, 24);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::gControls.Properties.Resources.Cuenta;
            this.pictureBox4.Location = new System.Drawing.Point(3, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(24, 24);
            this.pictureBox4.TabIndex = 9;
            this.pictureBox4.TabStop = false;
            // 
            // rbTotal
            // 
            this.rbTotal.AutoSize = true;
            this.rbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTotal.Location = new System.Drawing.Point(13, 119);
            this.rbTotal.Name = "rbTotal";
            this.rbTotal.Size = new System.Drawing.Size(222, 20);
            this.rbTotal.TabIndex = 4;
            this.rbTotal.Text = "Ingrese el total y la cantidad";
            this.rbTotal.UseVisualStyleBackColor = true;
            this.rbTotal.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // panelUnitario
            // 
            this.panelUnitario.Controls.Add(this.chkConIva);
            this.panelUnitario.Controls.Add(this.lblivaUnitario);
            this.panelUnitario.Controls.Add(this.label2);
            this.panelUnitario.Controls.Add(this.lblPrecioTotal);
            this.panelUnitario.Controls.Add(this.label6);
            this.panelUnitario.Controls.Add(this.nPrecioUnitario);
            this.panelUnitario.Controls.Add(this.pictureBox6);
            this.panelUnitario.Controls.Add(this.nCantUnitario);
            this.panelUnitario.Controls.Add(this.pictureBox7);
            this.panelUnitario.Controls.Add(this.pictureBox8);
            this.panelUnitario.Location = new System.Drawing.Point(8, 26);
            this.panelUnitario.Name = "panelUnitario";
            this.panelUnitario.Size = new System.Drawing.Size(317, 87);
            this.panelUnitario.TabIndex = 0;
            // 
            // lblivaUnitario
            // 
            this.lblivaUnitario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblivaUnitario.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblivaUnitario.Location = new System.Drawing.Point(206, 32);
            this.lblivaUnitario.Name = "lblivaUnitario";
            this.lblivaUnitario.Size = new System.Drawing.Size(89, 40);
            this.lblivaUnitario.TabIndex = 18;
            this.lblivaUnitario.Text = "Precio unitario sin IVA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(125, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 16);
            this.label2.TabIndex = 15;
            this.label2.Text = "Total";
            // 
            // lblPrecioTotal
            // 
            this.lblPrecioTotal.AutoSize = true;
            this.lblPrecioTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioTotal.Location = new System.Drawing.Point(114, 50);
            this.lblPrecioTotal.Name = "lblPrecioTotal";
            this.lblPrecioTotal.Size = new System.Drawing.Size(71, 24);
            this.lblPrecioTotal.TabIndex = 0;
            this.lblPrecioTotal.Text = "$0.000";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 4;
            // 
            // nPrecioUnitario
            // 
            this.nPrecioUnitario.DecimalPlaces = 3;
            this.nPrecioUnitario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nPrecioUnitario.Location = new System.Drawing.Point(200, 3);
            this.nPrecioUnitario.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nPrecioUnitario.Name = "nPrecioUnitario";
            this.nPrecioUnitario.Size = new System.Drawing.Size(95, 26);
            this.nPrecioUnitario.TabIndex = 1;
            this.nPrecioUnitario.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nPrecioUnitario.ValueChanged += new System.EventHandler(this.nCantUnitario_ValueChanged);
            this.nPrecioUnitario.Enter += new System.EventHandler(this.nPre_Enter);
            this.nPrecioUnitario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDown1_KeyPress);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::gControls.Properties.Resources.Cantidad;
            this.pictureBox6.Location = new System.Drawing.Point(35, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(24, 24);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 12;
            this.pictureBox6.TabStop = false;
            // 
            // nCantUnitario
            // 
            this.nCantUnitario.DecimalPlaces = 3;
            this.nCantUnitario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nCantUnitario.Location = new System.Drawing.Point(64, 3);
            this.nCantUnitario.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.nCantUnitario.Name = "nCantUnitario";
            this.nCantUnitario.Size = new System.Drawing.Size(95, 26);
            this.nCantUnitario.TabIndex = 0;
            this.nCantUnitario.ValueChanged += new System.EventHandler(this.nCantUnitario_ValueChanged);
            this.nCantUnitario.Enter += new System.EventHandler(this.nPre_Enter);
            this.nCantUnitario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDown1_KeyPress);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::gControls.Properties.Resources.Precio;
            this.pictureBox7.Location = new System.Drawing.Point(170, 5);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(24, 24);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 10;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::gControls.Properties.Resources.Cuenta;
            this.pictureBox8.Location = new System.Drawing.Point(3, 3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(24, 24);
            this.pictureBox8.TabIndex = 9;
            this.pictureBox8.TabStop = false;
            // 
            // rbPrecioyCantidad
            // 
            this.rbPrecioyCantidad.AutoSize = true;
            this.rbPrecioyCantidad.Checked = true;
            this.rbPrecioyCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbPrecioyCantidad.Location = new System.Drawing.Point(13, 3);
            this.rbPrecioyCantidad.Name = "rbPrecioyCantidad";
            this.rbPrecioyCantidad.Size = new System.Drawing.Size(257, 20);
            this.rbPrecioyCantidad.TabIndex = 3;
            this.rbPrecioyCantidad.TabStop = true;
            this.rbPrecioyCantidad.Text = "Ingrese cantidad y precio unitario";
            this.rbPrecioyCantidad.UseVisualStyleBackColor = true;
            this.rbPrecioyCantidad.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // chkConIva
            // 
            this.chkConIva.AutoSize = true;
            this.chkConIva.Location = new System.Drawing.Point(253, 67);
            this.chkConIva.Name = "chkConIva";
            this.chkConIva.Size = new System.Drawing.Size(61, 17);
            this.chkConIva.TabIndex = 19;
            this.chkConIva.Text = "con iva";
            this.chkConIva.UseVisualStyleBackColor = true;
            this.chkConIva.CheckedChanged += new System.EventHandler(this.chkConIva_CheckedChanged);
            // 
            // EditorFraccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelUnitario);
            this.Controls.Add(this.rbPrecioyCantidad);
            this.Controls.Add(this.panelTotal);
            this.Controls.Add(this.rbTotal);
            this.Controls.Add(this.panelFraccion);
            this.Controls.Add(this.rbFraccion);
            this.Name = "EditorFraccion";
            this.Size = new System.Drawing.Size(355, 365);
            ((System.ComponentModel.ISupportInitialize)(this.nFracFrac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nPrecioFrac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCantFrac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelFraccion.ResumeLayout(false);
            this.panelFraccion.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nCantTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panelUnitario.ResumeLayout(false);
            this.panelUnitario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nPrecioUnitario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCantUnitario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nFracFrac;
        private System.Windows.Forms.NumericUpDown nPrecioFrac;
        private System.Windows.Forms.NumericUpDown nCantFrac;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.RadioButton rbFraccion;
        private System.Windows.Forms.Panel panelFraccion;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPrecioUnitario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nCantTotal;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.NumericUpDown nTotalTotal;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.RadioButton rbTotal;
        private System.Windows.Forms.Panel panelUnitario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPrecioTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nPrecioUnitario;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.NumericUpDown nCantUnitario;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.RadioButton rbPrecioyCantidad;
        private System.Windows.Forms.Label lblivafraccion;
        private System.Windows.Forms.Label lblTotalIva;
        private System.Windows.Forms.Label lblivaUnitario;
        private System.Windows.Forms.CheckBox chkConIva;
    }
}
