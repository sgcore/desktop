﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using gLogger;

namespace gControls.Editores
{
    public partial class EditorPagos : UserControl
    {
        gcMovimiento _mov;
        public EditorPagos()
        {
            InitializeComponent();
            enableBotones();
        }
        public void setMovimiento(gcMovimiento m)
        {
            _mov = m;
            enableBotones();
            this.Width = 0;
            this.Visible = !(_mov == null || !_mov.EsConDineroInvolucrado);
            if (_mov != null)
            {
                btCheque.setMonto(m.PagosEnCheques);
                btTarjeta.setMonto(m.PagosEnTarjeta);
                btEfectivo.setMonto(m.PagosEnEfectivo);
                btVuelto.setMonto(m.PagosEnVueltos);
                btCuenta.setMonto(m.PagosEnCuentas);
                btDescuentos.setMonto(m.PagosEnDescuentos);

                btEfectivo.Visible = _mov.EsMovimientoDinero || _mov.Diferencia > 0 || _mov.PagosEnEfectivo.Monto>0;
                btVuelto.Visible = _mov.Diferencia < 0 || _mov.PagosEnVueltos.Monto > 0;
                btCuenta.Visible = _mov.Diferencia != 0 || _mov.PagosEnCuentas.Monto > 0;
                btTarjeta.Visible = _mov.Diferencia > 0 || _mov.PagosEnTarjetas.Monto > 0;
                btDescuentos.Visible = _mov.Diferencia > 0 || _mov.PagosEnDescuentos.Monto > 0;
                btCheque.Visible = false;// _mov.Diferencia > 0 || _mov.PagosEnCheques.Monto>0;
                enableBotones();

            }
            //this.Visible = btEfectivo.Visible ||
            //    btVuelto.Visible ||
            //    btCuenta.Visible ||
            //    btTarjeta.Visible ||
            //    btCheque.Visible;

        }
        private void enableBotones()
        {
            if (_mov != null && !_mov.Terminado)
            {
                bool enb = ( _mov.Diferencia > 0 || _mov.EsMovimientoDinero);
                btEfectivo.Enabled = enb;
                btCuenta.Enabled = _mov.Diferencia != 0;
                btCheque.Enabled = enb;
                btTarjeta.Enabled = enb;
                btVuelto.Enabled = !enb && _mov.Diferencia != 0;
                btDescuentos.Enabled = enb && _mov.SePuedeDescontar;
            }
            else
            {
                btEfectivo.Enabled = false;
                btCuenta.Enabled = false;
                btCheque.Enabled = false;
                btTarjeta.Enabled = false;
                btVuelto.Enabled = false;
                btDescuentos.Enabled = false;


            }
            
        }


        public void crearPago(gcPago p)
        {
            switch (p.Tipo)
            {
                case gcPago.TipoPago.Efectivo:
                case gcPago.TipoPago.Vuelto:
                    _mov.addPagoUnique(p);
                    break;
                case gcPago.TipoPago.Tarjeta:
                case gcPago.TipoPago.Cheque:
                case gcPago.TipoPago.Descuento:
                    _mov.addPago(p);
                    break;
                case gcPago.TipoPago.Cuenta:
                    _mov.addPagoCuenta(p.Monto);
                    break;
            }
            CoreManager.Singlenton.seleccionarMovimiento(_mov);
        }
        private void btEfectivo_Click(object sender, EventArgs e)
        {
            gcPago p = new gcPago(_mov.TotalMovimiento,gcPago.TipoPago.Efectivo);
            gcPago np = Editar.EditarEfectivo(_mov.PagosEnEfectivo, "Pago en efectivo", p, null);
            if (np!=null)
            {
               
                crearPago(np);

            }

        }
        bool _vert=true;
        public bool Vertical { get { return _vert; }
            set
            {
                _vert = value;
                if (_vert)
                {
                    btCheque.Dock = DockStyle.Top;
                    btEfectivo.Dock = DockStyle.Top;
                    btTarjeta.Dock = DockStyle.Top;
                    btVuelto.Dock = DockStyle.Top;
                    btCuenta.Dock = DockStyle.Top;
                    btDescuentos.Dock = DockStyle.Top;
                    
                }
                else
                {
                    btCheque.Dock = DockStyle.Left;
                    btEfectivo.Dock = DockStyle.Left;
                    btTarjeta.Dock = DockStyle.Left;
                    btVuelto.Dock = DockStyle.Left;
                    btCuenta.Dock = DockStyle.Left;
                    btDescuentos.Dock = DockStyle.Left;
                     
                }
            }
        }
        private void btVuelto_Click(object sender, EventArgs e)
        {

            gcPago p = null;
            if(_mov.Diferencia<0)p=new gcPago(-_mov.Diferencia,gcPago.TipoPago.Vuelto);
            gcPago np = Editar.EditarEfectivo(_mov.PagosEnVueltos, "Vuelto", p, null);
            if (np!=null)
            {
               
                crearPago(np);

            }
        }

        private void btTarjeta_Click(object sender, EventArgs e)
        {
           
        }

        private void btCheque_Click(object sender, EventArgs e)
        {
           
        }

        private void btCuenta_Click(object sender, EventArgs e)
        {
            addPagoCuenta();
            
        }
        private void btDescuentos_BotonClicked(object sender, EventArgs e)
        {
            decimal monto = Editar.EditarDecimal(0, "Agregar descuento", 2, 0, _mov.MaximoDescuentoPermitido);
            
            if (monto > 0)
            {
                gcPago p = new gcPago(monto, gcPago.TipoPago.Descuento);
                crearPago(p);

            }

        }
        private void addPagoCuenta(bool preguntar = true)
        {
            if (_mov.Destinatario == null)
            {
                logger.Singlenton.addErrorMsg("No puede agregar un pago si no eligió el destinatario.");
                return;
            }
            foreach (gcPago p in _mov.Pagos)
            {
                if (p.Tipo == gcPago.TipoPago.Cuenta)
                {
                    logger.Singlenton.addWarningMsg("En este movimiento ya existe un pago de cuenta corriente de " + p.Monto.ToString("$0.00") + ".\n Por favor eliminelo y vuelva a crearlo con el monto correcto.");
                    return;
                }
            }
            DialogResult dr = DialogResult.Cancel;
            if (_mov.Diferencia > 0)
            {
                dr = MessageBox.Show("¿Desea agregar " + _mov.Diferencia.ToString("$0.00") + " a la deuda de " + _mov.Destinatario.Nombre + "?", "Cuenta Corriente", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else if (_mov.Diferencia < 0)
            {
                dr = MessageBox.Show("¿Desea retirar " + (-_mov.Diferencia).ToString("$0.00") + " de la cuenta de " + _mov.Destinatario.Nombre + "?", "Cuenta Corriente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }
            if (preguntar && dr == DialogResult.Yes)
            {

                _mov.addPagoCuenta(_mov.Diferencia);

            }
            else if (!preguntar)
            {
                _mov.addPagoCuenta(_mov.Diferencia);
            }
            CoreManager.Singlenton.seleccionarMovimiento(_mov);
        }

        
    }
}
