﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Editores
{
    public partial class EditorFiltroTratamientos : UserControl
    {
        public EditorFiltroTratamientos()
        {
            InitializeComponent();
        }
        public gManager.Filtros.FiltroTratamientos Filtro { get; set; }
    }
}
