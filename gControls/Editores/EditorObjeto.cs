﻿using gManager;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace gControls.Editores
{
    public partial class EditorObjeto : BaseControl
    {
        
        
        private gManager.gcObjeto _obj;
        private gManager.gcObjeto  _cateParent;
        private gManager.gcObjeto _grupo;
        private decimal _ideal = 0;
        private Image _image = null;
        public gManager.gcObjeto Objeto 
        {
            get 
            {
                if (_obj == null) return null;
                 _obj.Nombre=txtNombre.Text ;
                 _obj.Descripcion=txtDescripcion.Text ;
                 _obj.Link=txtLink.Text ;
                 _obj.Costo = cntrlEditorPrecio1.Costo;
                 _obj.Ganancia = cntrlEditorPrecio1.Ganancia;
                 _obj.Iva = cntrlEditorPrecio1.IVA;
                _obj.Dolar = cntrlEditorPrecio1.Dolar;
                _obj.Codigo = txtCodigo.Text;
                 _obj.Serial = txtSerial.Text;
                _obj.Ideal = _ideal;
                _obj.Metrica = (gcObjeto.ObjetoMedida)cmbMetrica.SelectedIndex;
                _obj.State = (gcObjeto.ObjetoState)cmbEstado.SelectedIndex;
               
                if (_cateParent != null) _obj.Parent = _cateParent.Id; ;
                 if (_grupo != null) _obj.Grupo = _grupo.Id;
                
                
                return _obj; 
            } 
            set 
            {
               
               _obj=value ;
               if (_obj != null)
               {
                   foreach (Control c in Controls) c.Enabled = RequisitoPermiso(gManager.gcUsuario.PermisoEnum.Administrador);
                   txtNombre.Text = _obj.Nombre;
                   txtDescripcion.Text = _obj.Descripcion;
                   txtLink.Text = _obj.Link;
                   cmbMetrica.SelectedIndex = (int)_obj.Metrica;
                   cmbEstado.SelectedIndex = (int)_obj.State;
                   lblEstado.Text = _obj.State.ToString().ToUpper();
                    switch (_obj.State)
                    {
                        case gcObjeto.ObjetoState.Publico:
                            lblEstado.ForeColor = Color.ForestGreen;
                            break;
                        case gcObjeto.ObjetoState.Discontinuo:
                            lblEstado.ForeColor = Color.Red;
                            break;
                        default:
                            lblEstado.ForeColor = Color.SteelBlue;
                            break;
                    }
                   txtCodigo.Text = _obj.Codigo;
                   txtSerial.Text = _obj.Serial;
                    _ideal = _obj.Ideal;
                    btIdeal.Text = Utils.UnidadMedida((gcObjeto.ObjetoMedida)cmbMetrica.SelectedIndex, _ideal);
                    _cateParent = gManager.CoreManager.Singlenton.getProducto(_obj.Parent);
                   if (_cateParent != null)
                   {
                       btCategoria.Text = _cateParent.Nombre; ;
                       btCategoria.ForeColor = Color.OliveDrab;
                       btCancelarCat.Visible = true;
                   }
                   else
                   {
                       btCategoria.Text = "Categoria";
                       btCategoria.ForeColor = Color.Red;
                       btCancelarCat.Visible = false;
                   }
                   _grupo = _obj.GrupoContenedor;
                   if (_grupo != null)
                   {
                       btGrupo.Text = _grupo.Nombre; ;
                       btGrupo.ForeColor = Color.OliveDrab;
                       btEliminarGrupo.Visible = true;
                   }
                   else
                   {
                       btGrupo.Text = "Grupo";
                       btGrupo.ForeColor = Color.Red;
                       btEliminarGrupo.Visible = false;
                   }
                   cntrlEditorPrecio1.Ganancia = _obj.Ganancia;
                   cntrlEditorPrecio1.Costo = _obj.Costo;
                   cntrlEditorPrecio1.IVA = _obj.Iva;
                    cntrlEditorPrecio1.Dolar = _obj.Dolar;
                    cntrlEditorPrecio1.Visible = _obj.Tipo != gManager.gcObjeto.ObjetoTIpo.Categoria && _obj.Tipo!=gManager.gcObjeto.ObjetoTIpo.Grupo;
                    
                    //el grupo no usa
                    cmbMetrica.Visible = _obj.Tipo == gcObjeto.ObjetoTIpo.Producto || _obj.Tipo == gcObjeto.ObjetoTIpo.Servicio;
                    cmbEstado.Visible = _obj.Tipo != gcObjeto.ObjetoTIpo.Grupo;
                    lblEstado.Visible = cmbEstado.Visible;
                    btIdeal.Visible = _obj.Tipo == gcObjeto.ObjetoTIpo.Producto;
                    lblmetrica.Visible = cmbMetrica.Visible;
                    lblideal.Visible = btIdeal.Visible;
                    gbParent.Visible = _obj.Tipo != gManager.gcObjeto.ObjetoTIpo.Grupo;
                    txtCodigo.Visible = gbParent.Visible;
                   txtSerial.Visible = gbParent.Visible;
                   btGrupo.Visible = _obj.Tipo != gManager.gcObjeto.ObjetoTIpo.Categoria;
                   pictureBox1.Image = ImageManager.getImageGeneric(_obj, Properties.Resources.nopic);
                   btClonar.Enabled = CoreManager.Singlenton.Key.Habilitado;
                   btCaracteristicas.Visible = _obj.Tipo != gManager.gcObjeto.ObjetoTIpo.Grupo;

                    //no se puede cambiar el codigo ni el estado a un producto sincronizado
                    txtCodigo.Enabled = !_obj.Sincronizado;
                    cmbEstado.Enabled = !_obj.Sincronizado;
               }
               else
               {
                   foreach (Control c in Controls) c.Enabled = false;
                   txtNombre.Text = "";
                   txtDescripcion.Text = "";
                   txtLink.Text = "";
                   //txtManual.Text = _obj.Manual;
                   txtCodigo.Text = "";
                   txtSerial.Text = "";
                   _cateParent = null;
                   btCategoria.Text = "Categoria";
                   btCategoria.ForeColor = Color.Red;
                   btCancelarCat.Visible = false;
                   cntrlEditorPrecio1.Ganancia = 0;
                   cntrlEditorPrecio1.Costo = 0;
                   cntrlEditorPrecio1.IVA = 0;
                   cntrlEditorPrecio1.Visible = false;
                   btClonar.Visible = false;
                   btCaracteristicas.Visible = false;
               }
               
               
            } 
        }
        public bool ShowButtons
        {
            get { return btCancelar.Visible; }
            set
            {
                btCancelar.Visible = value;
                btGuardar.Visible = value;
            }
        }
        
        public EditorObjeto()
        {
            InitializeComponent();
            cmbMetrica.DataSource = Enum.GetValues(typeof(gcObjeto.ObjetoMedida));
            cmbEstado.DataSource = Enum.GetValues(typeof(gcObjeto.ObjetoState));
            //foreach (System.Data.DataRow row in gManager.Singlenton.CORE.OBJETOS.obtenerTodosLosGrupos().Rows)
            //    txtManual.AutoCompleteCustomSource.Add((string)row["grupo"]);
        }
        public override void iniciar()
        {
            Objeto = null;
        }
        public override void cerrar()
        {
            Objeto = null;

        }
        private void btCategoria_Click(object sender, EventArgs e)
        {
            var f =new gManager.Filtros.FiltroProducto();
            f.Tipos.Add(gManager.gcObjeto.ObjetoTIpo.Categoria);
            var c = buscar.BuscarProducto("Buscar Categoría",f);
            if (c != null)
            {
                if (_obj == null || c.Id == _obj.Id)
                {
                    gLogger.logger.Singlenton.addWarningMsg("No puede ser padre de si misma.");
                        return;
                }
                btCategoria.Text = c.Nombre;
                _cateParent = c;
                _obj.Parent = c.Id;
                btCategoria.ForeColor = Color.OliveDrab;
                btCancelarCat.Visible = true;
            }
        }

        private void btCancelarCat_Click(object sender, EventArgs e)
        {
            _cateParent = null;
            _obj.Parent = 0;
            btCategoria.Text = "Categoria";
            btCategoria.ForeColor = Color.Red;
            btCancelarCat.Visible = false;

        }

        private void btClonar_Click(object sender, EventArgs e)
        {
            Objeto.SaveMe();
            Objeto = Objeto.Clone();
            txtNombre.Focus();
           

        }

        private void btGuardar_Click(object sender, EventArgs e)
        {
            if (Objeto != null)
                Objeto.SaveMe();
            cerrar();
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            cerrar
                ();
        }

        private void btGrupo_Click(object sender, EventArgs e)
        {
            var f = new gManager.Filtros.FiltroProducto();
            f.Tipos.Add(gManager.gcObjeto.ObjetoTIpo.Grupo);
            var c = buscar.BuscarProducto("Buscar Grupo", f);
            if (c != null)
            {
                btGrupo.Text = c.Nombre;
                _grupo = c;
                btGrupo.ForeColor = Color.OliveDrab;
                btEliminarGrupo.Visible = true;
            }
        }

        private void btEliminarGrupo_Click(object sender, EventArgs e)
        {
            _grupo = null;
            _obj.Grupo = 0;
            btGrupo.Text = "Grupo";
            btGrupo.ForeColor = Color.Red;
            btEliminarGrupo.Visible = false;
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {

            if (Objeto == null || Objeto.Id<1)
            {
                gLogger.logger.Singlenton.addWarningMsg("Debe guardar antes de agregar una imagen");
                    return;
            }
            var im=buscar.BuscarImage();
            if(im!=null)
            {
               pictureBox1.Image = im;
                _image = im;
            }
               

        }
        public Image ImageToSave
        {
            get
            {
                return _image;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ver.verCaracteristicas(_obj);
        }

        private void btIdeal_Click(object sender, EventArgs e)
        {
            _ideal = Editar.EditarDecimal(_ideal, "Editar Stock Ideal");
            btIdeal.Text = Utils.UnidadMedida((gcObjeto.ObjetoMedida)cmbMetrica.SelectedIndex, _ideal);
        }
    }
}
