﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Editores
{
    public partial class EditorDestinatario : BaseControl
    {
         protected gManager.gcDestinatario  _obj;
         public gManager.gcDestinatario Destinatario
         {
             get
             {
                 return buildDestinatario();
             }
             set
             {
                 setDestinatario(value);
                
             }
         }
         protected virtual gcDestinatario buildDestinatario()
         {
             if (_obj == null) return null;
             _obj.Nombre = txtNombre.Text;
             _obj.Direccion = txtDireccion.Text;
             _obj.Telefono = txtTelefono.Text;
             _obj.Celular = txtCelular.Text;
             _obj.Cuit = txtCuit.Text;
             _obj.Mail = txtMail.Text;
             _obj.Observaciones = txtObservaciones.Text;
             _obj.Nacimiento = dtNacimiento.Value;
             _obj.TipoGenerico = cmbIva.SelectedIndex;

             return _obj;
         }
         protected virtual void setDestinatario(gcDestinatario d)
         {
             _obj = d;
             if (_obj != null)
             {
                 if (_obj.EsPaciente)
                     cmbIva.DataSource = Enum.GetValues(typeof(gcDestinatario.especieEnum));
                 else
                     cmbIva.DataSource = Enum.GetValues(typeof(gcDestinatario.DestinatarioIVA));
                 foreach (Control c in Controls) c.Enabled = true;
                 txtNombre.Text = _obj.Nombre;
                 txtDireccion.Text = _obj.Direccion;
                 txtTelefono.Text = _obj.Telefono;
                 txtCelular.Text = _obj.Celular;
                 txtCuit.Text = _obj.Cuit;
                 txtMail.Text = _obj.Mail;
                 txtObservaciones.Text = _obj.Observaciones;
                 cmbIva.Visible = _obj.Tipo == gcDestinatario.DestinatarioTipo.Cliente
                     || _obj.Tipo == gcDestinatario.DestinatarioTipo.Proveedor
                     || _obj.Tipo == gcDestinatario.DestinatarioTipo.Paciente;
                 cmbIva.SelectedIndex = _obj.TipoGenerico;
                 //pbImage.Image = gControls.ImagenLists.DestImagenList.Images[_obj.Tipo.ToString()];
                 dtNacimiento.Value = _obj.Nacimiento;
                 lblLastMod.Text = "Ultima modificación: " + gManager.Utils.MostrarHora(_obj.LastMod);
                 if (_obj.Tipo == gManager.gcDestinatario.DestinatarioTipo.Paciente)
                 {
                     //lblDire.Text = "Raza";
                     //lblTele.Text = "Tamaño";
                     //lblCel.Text = "Pelo";
                     //lblMail.Text = "Color";
                     //lblCuit.Text ="Caracter";
                     //lblFecha.Text = "Nacimiento";
                     gbPropietario.Visible = true;
                     gManager.gcDestinatario g = gManager.CoreManager.Singlenton.getDestinatario(_obj.Parent, gManager.gcDestinatario.DestinatarioTipo.Cliente);
                     if (g != null)
                     {

                         lblPropietario.Text = g.Nombre;
                     }
                     setPacienteUI();

                 }
                 pictureBox1.Image = ImageManager.getImageGeneric(_obj);
             }
             else
             {
                 txtNombre.Text = "";
                 txtDireccion.Text = "";
                 txtTelefono.Text = "";
                 txtCelular.Text = "";
                 txtCuit.Text = "";
                 txtMail.Text = "";
                 lblLastMod.Text = "No ha selseccionado un destinatario";
                 txtObservaciones.Text = "";
                 foreach (Control c in Controls) c.Enabled = false;
             }
                 

                 
         }
         private void setPacienteUI()
         {
             lblDire.Text = "Raza";
             lblFecha.Text = "Nacimiento";
             lblCel.Text = "Especie";
             lblTele.Text = "Sexo";
             lblMail.Text = "Color";
             lblCuit.Text = "Tamaño";

         }

         public EditorDestinatario()
             : base()
        {
            InitializeComponent();
           
            cerrar();
        }
         public override void cerrar()
         {
             Destinatario = null;

         }
         public override void iniciar()
         {
            
             
             Destinatario = null;
         }

        private void btPropietario_Click(object sender, EventArgs e)
        {
           var g= buscar.BuscarDestinatario("Buscar Propietario",gcDestinatario.DestinatarioTipo.Cliente);
           if (g != null)
           {
               Destinatario.Parent = g.Id;
               lblPropietario.Text = g.Nombre;
           }
        }
        public bool ShowButtons
        {
            get { return btCancelar.Visible; }
            set
            {
                btCancelar.Visible = value;
                btGuardar.Visible = value;
            }
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            Destinatario = null;
        }

        private void btGuardar_Click(object sender, EventArgs e)
        {
            if (Destinatario != null)
                Destinatario.SaveMe();
            Destinatario = null;
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            if (Destinatario == null || Destinatario.Id < 1)
            {
                gLogger.logger.Singlenton.addWarningMsg("Debe guardar antes de agregar una imagen");
                return;
            }
            var im = buscar.BuscarImage();
            if (im != null)
            {
                ImageManager.SaveImageDestinatario(im, Destinatario);
                pictureBox1.Image = im;
            }
        }

       

        
    }
}
