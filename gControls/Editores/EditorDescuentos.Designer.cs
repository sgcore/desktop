﻿namespace gControls.Editores
{
    partial class EditorDescuentos
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorDescuentos));
            this.buscadorCustom1 = new gControls.Buscadores.BuscadorCustom();
            this.SuspendLayout();
            // 
            // buscadorCustom1
            // 
            this.buscadorCustom1.BloquearBarraHerramientas = false;
            this.buscadorCustom1.CustomActualizar = null;
            this.buscadorCustom1.CustomDatos = false;
            this.buscadorCustom1.Imagen = ((System.Drawing.Image)(resources.GetObject("buscadorCustom1.Imagen")));
            this.buscadorCustom1.ImagenTitulo = global::gControls.Properties.Resources.Porciento;
            this.buscadorCustom1.Location = new System.Drawing.Point(65, 19);
            this.buscadorCustom1.Name = "buscadorCustom1";
            this.buscadorCustom1.OcultarSiEstaVacio = false;
            this.buscadorCustom1.RequiereKey = false;
            this.buscadorCustom1.SeleccionGlobal = false;
            this.buscadorCustom1.Size = new System.Drawing.Size(625, 349);
            this.buscadorCustom1.TabIndex = 0;
            this.buscadorCustom1.Texto = "Descuentos";
            this.buscadorCustom1.Titulo = "Realizar descuentos";
            this.buscadorCustom1.VerBotonActualizar = false;
            this.buscadorCustom1.VerBotonAgrupar = false;
            this.buscadorCustom1.VerBotonFiltro = false;
            this.buscadorCustom1.VerBotonGuardar = false;
            this.buscadorCustom1.VerBotonImagen = false;
            this.buscadorCustom1.VerBotonImprimir = false;
            this.buscadorCustom1.VerImagenCard = false;
            // 
            // EditorDescuentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buscadorCustom1);
            this.Name = "EditorDescuentos";
            this.Size = new System.Drawing.Size(799, 420);
            this.ResumeLayout(false);

        }

        #endregion

        private Buscadores.BuscadorCustom buscadorCustom1;
    }
}
