﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gLogger;


namespace gManager.Editores
{
    public partial class EditorDecimal : UserControl
    {
        decimal _ini = 0;
        string _ceros = "00";
         public EditorDecimal()
        {
            InitializeComponent();
            numericUpDown1.GotFocus += new EventHandler(numericUpDown1_GotFocus);
        }
        public int LugaresDecimales
        {
            get
            {
                return numericUpDown1.DecimalPlaces;
            }
            set { numericUpDown1.DecimalPlaces = value;
            _ceros = "0.";
            for (int i = 0; i < value; i++)
                _ceros += "0";
            }
        }
        public decimal Minimo { get { return numericUpDown1.Minimum; }
            set { numericUpDown1.Minimum = value; }
        }
        public decimal Maximo
        {
            get { return numericUpDown1.Maximum; }
            set { numericUpDown1.Maximum = value; }
        }
        void numericUpDown1_GotFocus(object sender, EventArgs e)
        {
            numericUpDown1.Select(0, 10);
        }
        public decimal ValorDecimal
        {
            get { return numericUpDown1.Value; }
            set { _ini = value;
                if(value<=numericUpDown1.Maximum) numericUpDown1.Value = value;
                else
                {
                    logger.Singlenton.addWarningMsg("Se intentó bloquear el editor de decimales con un valor incorrecto.\n" +
                    "Esto ocurre en los casos en los que desea modificar un stock inexistente");
                    numericUpDown1.Value = numericUpDown1.Maximum;
                }
            }
        }
        private void numericUpDown1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((System.Globalization.CultureInfo)System.Globalization.CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }
       

        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Decimal)
            {
                e.SuppressKeyPress = true;
                SendKeys.Send(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

            lbldesc.Text = "Inicial: " + _ini.ToString(_ceros);
            lblRango.Text = "Rango: de " + Minimo.ToString(_ceros) + " a " + Maximo.ToString(_ceros);
            decimal dif=(numericUpDown1.Value - _ini);
             lbldif.Text = dif.ToString(_ceros);

             //lbldif.ForeColor = Utils.ColorDecimal(dif,1);
        }

        

        
    }
}
