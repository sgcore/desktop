﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Editores
{
    public partial class EditorFiltroCaja : UserControl
    {
       
        public EditorFiltroCaja()
        {
            InitializeComponent();
            gcCaja[] cajas = CoreManager.Singlenton.getCajas().ToArray();
            //cmbCajaIni.DataSource = cajas;
            //cmbCajaIni.DisplayMember = "Caja";
            //cmbCajaFin.DataSource = cajas.Clone();
            //cmbCajaFin.DisplayMember = "Caja";
            nCajafinal.Maximum = CoreManager.Singlenton.CajaActual.Caja;
            nCajafinal.Value = CoreManager.Singlenton.CajaActual.Caja;
            nCajaInicial.Maximum = CoreManager.Singlenton.CajaActual.Caja;
            cmbCajaSola.DataSource = cajas.Clone();
            cmbCajaSola.DisplayMember = "Caja";

        }
        private List<gcCaja> _customlist = new List<gcCaja>();
        public gManager.Filtros.FiltroCaja Filtro
        {
            get
            {
                
                switch (tabControl1.SelectedIndex)
                {
                    case 3:
                        //return new gManager.Filtros.FiltroCaja(((gcCaja)cmbCajaIni.SelectedItem).Caja, ((gcCaja)cmbCajaFin.SelectedItem).Caja);
                    return new gManager.Filtros.FiltroCaja((int)nCajaInicial.Value,(int) nCajafinal.Value);
                    case 4:
                        return new gManager.Filtros.FiltroCaja(dpFechaIni.Value,dpFechaFin.Value);
                    case 2:
                        return new gManager.Filtros.FiltroCaja(((gcCaja)cmbCajaSola.SelectedItem).Caja);
                    case 0:
                        return new gManager.Filtros.FiltroCaja(gManager.Filtros.FiltroCaja.TipoFiltroCaja.CajaActual);
                    case 1:
                        return new gManager.Filtros.FiltroCaja(_customlist);
                    case 5:
                        return new gManager.Filtros.FiltroCaja(gManager.Filtros.FiltroCaja.TipoFiltroCaja.Todas);
                    default:
                        return new gManager.Filtros.FiltroCaja(gManager.Filtros.FiltroCaja.TipoFiltroCaja.SinFiltrar);
                }
            }
            set
            {
                //tiene que coincidir con el enum.
                tabControl1.TabIndex = (int)value.Tipo;
                switch(value.Tipo)
                { 
                    case gManager.Filtros.FiltroCaja.TipoFiltroCaja.EntreFechas:
                        dpFechaFin.Value=value.FechaFinal;
                        dpFechaIni.Value=value.FechaInicial;
                        tabControl1.SelectedIndex = 4;
                        
                        break;
                    case gManager.Filtros.FiltroCaja.TipoFiltroCaja.EntreCajas:
                        nCajaInicial.Value = value.CajaInicial;
                       nCajafinal.Value = value.CajaFinal;
                        tabControl1.SelectedIndex = 3;
                        break;
                    case gManager.Filtros.FiltroCaja.TipoFiltroCaja.UnaCaja:
                        cmbCajaSola.SelectedItem=value.CajaSola;
                        tabControl1.SelectedIndex = 2;
                        break;
                    case gManager.Filtros.FiltroCaja.TipoFiltroCaja.UltimaCaja:
                        tabControl1.SelectedIndex = 1;
                        break;
                    case gManager.Filtros.FiltroCaja.TipoFiltroCaja.CajaActual:
                        tabControl1.SelectedIndex = 0;
                        break;
                    case gManager.Filtros.FiltroCaja.TipoFiltroCaja.Todas:
                        tabControl1.SelectedIndex = 5;
                        break;
                    default:
                        break;
                }
            }
        }

        private void nCajaInicial_Enter(object sender, EventArgs e)
        {
            (sender as NumericUpDown).Select(0, 999);
        }

        private void btListaCustom_Click(object sender, EventArgs e)
        {
            _customlist = buscar.BuscarCajas();
        }

       
    }
}
