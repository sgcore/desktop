﻿namespace gControls.Editores
{
    partial class EditorPagos
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorPagos));
            this.btCheque = new gControls.Visores.VisorMonto();
            this.btTarjeta = new gControls.Visores.VisorMonto();
            this.btCuenta = new gControls.Visores.VisorMonto();
            this.btVuelto = new gControls.Visores.VisorMonto();
            this.btEfectivo = new gControls.Visores.VisorMonto();
            this.btDescuentos = new gControls.Visores.VisorMonto();
            this.SuspendLayout();
            // 
            // btCheque
            // 
            this.btCheque.AutoSize = true;
            this.btCheque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCheque.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btCheque.BotonAlineacion = System.Windows.Forms.DockStyle.Right;
            this.btCheque.ColorFondoTitulo = System.Drawing.Color.DimGray;
            this.btCheque.Dock = System.Windows.Forms.DockStyle.Top;
            this.btCheque.ImageBox = ((System.Drawing.Image)(resources.GetObject("btCheque.ImageBox")));
            this.btCheque.ImageButton = global::gControls.Properties.Resources.Cheque;
            this.btCheque.ImagenFondoCero = null;
            this.btCheque.ImagenFondoNegativo = ((System.Drawing.Image)(resources.GetObject("btCheque.ImagenFondoNegativo")));
            this.btCheque.ImagenFondoPositivo = ((System.Drawing.Image)(resources.GetObject("btCheque.ImagenFondoPositivo")));
            this.btCheque.Location = new System.Drawing.Point(0, 160);
            this.btCheque.MinimumSize = new System.Drawing.Size(150, 40);
            this.btCheque.Monto = "$0.000";
            this.btCheque.Name = "btCheque";
            this.btCheque.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btCheque.Size = new System.Drawing.Size(154, 40);
            this.btCheque.TabIndex = 11;
            this.btCheque.Titulo = "Cheque";
            this.btCheque.ToolTipDescripcion = "Realiza un pago con cheques.";
            this.btCheque.UseColorMontos = false;
            this.btCheque.VerBoton = true;
            this.btCheque.VisibleImageBox = false;
            this.btCheque.BotonClicked += new System.EventHandler(this.btCheque_Click);
            // 
            // btTarjeta
            // 
            this.btTarjeta.AutoSize = true;
            this.btTarjeta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btTarjeta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btTarjeta.BotonAlineacion = System.Windows.Forms.DockStyle.Right;
            this.btTarjeta.ColorFondoTitulo = System.Drawing.Color.DimGray;
            this.btTarjeta.Dock = System.Windows.Forms.DockStyle.Top;
            this.btTarjeta.ImageBox = ((System.Drawing.Image)(resources.GetObject("btTarjeta.ImageBox")));
            this.btTarjeta.ImageButton = global::gControls.Properties.Resources.Tarjeta;
            this.btTarjeta.ImagenFondoCero = null;
            this.btTarjeta.ImagenFondoNegativo = ((System.Drawing.Image)(resources.GetObject("btTarjeta.ImagenFondoNegativo")));
            this.btTarjeta.ImagenFondoPositivo = ((System.Drawing.Image)(resources.GetObject("btTarjeta.ImagenFondoPositivo")));
            this.btTarjeta.Location = new System.Drawing.Point(0, 120);
            this.btTarjeta.MinimumSize = new System.Drawing.Size(150, 40);
            this.btTarjeta.Monto = "$0.000";
            this.btTarjeta.Name = "btTarjeta";
            this.btTarjeta.PermisoBoton = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btTarjeta.Size = new System.Drawing.Size(154, 40);
            this.btTarjeta.TabIndex = 10;
            this.btTarjeta.Titulo = "Tarjeta";
            this.btTarjeta.ToolTipDescripcion = "Realiza un pago con tarjeta de credito.";
            this.btTarjeta.UseColorMontos = false;
            this.btTarjeta.VerBoton = true;
            this.btTarjeta.VisibleImageBox = false;
            this.btTarjeta.BotonClicked += new System.EventHandler(this.btTarjeta_Click);
            // 
            // btCuenta
            // 
            this.btCuenta.AutoSize = true;
            this.btCuenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCuenta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btCuenta.BotonAlineacion = System.Windows.Forms.DockStyle.Right;
            this.btCuenta.ColorFondoTitulo = System.Drawing.Color.DimGray;
            this.btCuenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.btCuenta.ImageBox = ((System.Drawing.Image)(resources.GetObject("btCuenta.ImageBox")));
            this.btCuenta.ImageButton = global::gControls.Properties.Resources.Cuenta;
            this.btCuenta.ImagenFondoCero = null;
            this.btCuenta.ImagenFondoNegativo = global::gControls.Properties.Resources.FondoAmarillo;
            this.btCuenta.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.btCuenta.Location = new System.Drawing.Point(0, 80);
            this.btCuenta.MinimumSize = new System.Drawing.Size(150, 40);
            this.btCuenta.Monto = "$0.000";
            this.btCuenta.Name = "btCuenta";
            this.btCuenta.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btCuenta.Size = new System.Drawing.Size(154, 40);
            this.btCuenta.TabIndex = 9;
            this.btCuenta.Titulo = "Cuenta Corriente";
            this.btCuenta.ToolTipDescripcion = "Agrega la diferencia a la cuenta corriente del destinatario.";
            this.btCuenta.UseColorMontos = false;
            this.btCuenta.VerBoton = true;
            this.btCuenta.VisibleImageBox = false;
            this.btCuenta.BotonClicked += new System.EventHandler(this.btCuenta_Click);
            // 
            // btVuelto
            // 
            this.btVuelto.AutoSize = true;
            this.btVuelto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btVuelto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btVuelto.BotonAlineacion = System.Windows.Forms.DockStyle.Right;
            this.btVuelto.ColorFondoTitulo = System.Drawing.Color.DimGray;
            this.btVuelto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btVuelto.ImageBox = ((System.Drawing.Image)(resources.GetObject("btVuelto.ImageBox")));
            this.btVuelto.ImageButton = global::gControls.Properties.Resources.Vuelto;
            this.btVuelto.ImagenFondoCero = null;
            this.btVuelto.ImagenFondoNegativo = ((System.Drawing.Image)(resources.GetObject("btVuelto.ImagenFondoNegativo")));
            this.btVuelto.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoAmarillo;
            this.btVuelto.Location = new System.Drawing.Point(0, 40);
            this.btVuelto.MinimumSize = new System.Drawing.Size(150, 40);
            this.btVuelto.Monto = "$0.000";
            this.btVuelto.Name = "btVuelto";
            this.btVuelto.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btVuelto.Size = new System.Drawing.Size(154, 40);
            this.btVuelto.TabIndex = 8;
            this.btVuelto.Titulo = "Vuelto";
            this.btVuelto.ToolTipDescripcion = "Crea un vuelto en efectivo";
            this.btVuelto.UseColorMontos = false;
            this.btVuelto.VerBoton = true;
            this.btVuelto.Visible = false;
            this.btVuelto.VisibleImageBox = false;
            this.btVuelto.BotonClicked += new System.EventHandler(this.btVuelto_Click);
            // 
            // btEfectivo
            // 
            this.btEfectivo.AutoSize = true;
            this.btEfectivo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEfectivo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btEfectivo.BotonAlineacion = System.Windows.Forms.DockStyle.Right;
            this.btEfectivo.ColorFondoTitulo = System.Drawing.Color.DimGray;
            this.btEfectivo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btEfectivo.ImageBox = ((System.Drawing.Image)(resources.GetObject("btEfectivo.ImageBox")));
            this.btEfectivo.ImageButton = global::gControls.Properties.Resources.Efectivo;
            this.btEfectivo.ImagenFondoCero = null;
            this.btEfectivo.ImagenFondoNegativo = global::gControls.Properties.Resources.FondoRojo;
            this.btEfectivo.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoVerde;
            this.btEfectivo.Location = new System.Drawing.Point(0, 0);
            this.btEfectivo.MinimumSize = new System.Drawing.Size(150, 40);
            this.btEfectivo.Monto = "$0.000";
            this.btEfectivo.Name = "btEfectivo";
            this.btEfectivo.PermisoBoton = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btEfectivo.Size = new System.Drawing.Size(154, 40);
            this.btEfectivo.TabIndex = 7;
            this.btEfectivo.Titulo = "Efectivo";
            this.btEfectivo.ToolTipDescripcion = "Agrega un pago en efectivo";
            this.btEfectivo.UseColorMontos = false;
            this.btEfectivo.VerBoton = true;
            this.btEfectivo.VisibleImageBox = false;
            this.btEfectivo.BotonClicked += new System.EventHandler(this.btEfectivo_Click);
            // 
            // btDescuentos
            // 
            this.btDescuentos.AutoSize = true;
            this.btDescuentos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDescuentos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.btDescuentos.BotonAlineacion = System.Windows.Forms.DockStyle.Right;
            this.btDescuentos.ColorFondoTitulo = System.Drawing.Color.DimGray;
            this.btDescuentos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btDescuentos.ImageBox = ((System.Drawing.Image)(resources.GetObject("btDescuentos.ImageBox")));
            this.btDescuentos.ImageButton = global::gControls.Properties.Resources.Porciento;
            this.btDescuentos.ImagenFondoCero = null;
            this.btDescuentos.ImagenFondoNegativo = ((System.Drawing.Image)(resources.GetObject("btDescuentos.ImagenFondoNegativo")));
            this.btDescuentos.ImagenFondoPositivo = global::gControls.Properties.Resources.FondoAmarillo;
            this.btDescuentos.Location = new System.Drawing.Point(0, 200);
            this.btDescuentos.MinimumSize = new System.Drawing.Size(150, 40);
            this.btDescuentos.Monto = "$0.000";
            this.btDescuentos.Name = "btDescuentos";
            this.btDescuentos.PermisoBoton = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btDescuentos.Size = new System.Drawing.Size(154, 40);
            this.btDescuentos.TabIndex = 12;
            this.btDescuentos.Titulo = "Descuento";
            this.btDescuentos.ToolTipDescripcion = "Realiza un descuento";
            this.btDescuentos.UseColorMontos = false;
            this.btDescuentos.VerBoton = true;
            this.btDescuentos.VisibleImageBox = false;
            this.btDescuentos.BotonClicked += new System.EventHandler(this.btDescuentos_BotonClicked);
            // 
            // EditorPagos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.btDescuentos);
            this.Controls.Add(this.btCheque);
            this.Controls.Add(this.btTarjeta);
            this.Controls.Add(this.btCuenta);
            this.Controls.Add(this.btVuelto);
            this.Controls.Add(this.btEfectivo);
            this.Name = "EditorPagos";
            this.Size = new System.Drawing.Size(154, 240);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Visores.VisorMonto btCheque;
        private Visores.VisorMonto btTarjeta;
        private Visores.VisorMonto btCuenta;
        private Visores.VisorMonto btVuelto;
        private Visores.VisorMonto btEfectivo;
        private Visores.VisorMonto btDescuentos;
    }
}
