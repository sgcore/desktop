﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Editores
{
    public partial class EditorTratamiento : UserControl
    {
        gManager.gcTratamiento _trat;
        public EditorTratamiento()
        {
            InitializeComponent();
        }
        public gManager.gcTratamiento Tratamiento
        {
            get
            {
                if (_trat != null)
                {
                    _trat.Descripcion =txtDescripcion.Text ;
                    _trat.Titulo= txtTitulo.Text ;
                   _trat.Proximo = dtProx.Value;
                   switch (_trat.Tipo)
                   {
                       case gManager.gcTratamiento.TipoTratamiento.Aviso:
                          break;
                       case gManager.gcTratamiento.TipoTratamiento.Periodico:
                           _trat.Proximo = _trat.Fecha.AddDays((double)nDias.Value).AddHours((double)nHoras.Value).AddMinutes((double)nMinutos.Value);
                           break;
                       case gManager.gcTratamiento.TipoTratamiento.Recordatorio:
                           break;
                       case gManager.gcTratamiento.TipoTratamiento.Tarea:
                           break;
                       case gManager.gcTratamiento.TipoTratamiento.Tratamiento:
                           break;
                   }
                   


                }
                
                return _trat;
            }
            set
            {
                _trat = value;
                if (_trat != null)
                {
                    txtDescripcion.Text = _trat.Descripcion;
                    txtTitulo.Text = _trat.Titulo;
                    lblTipo.Text = _trat.Tipo.ToString();
                    pnFuturo.Visible=(_trat.Tipo==gManager.gcTratamiento.TipoTratamiento.Aviso
                        || _trat.Tipo == gManager.gcTratamiento.TipoTratamiento.Tarea
                        || _trat.Tipo == gManager.gcTratamiento.TipoTratamiento.Recordatorio);
                    pnPeriodico.Visible = (_trat.Tipo == gManager.gcTratamiento.TipoTratamiento.Periodico);
                    nHoras.Value = _trat.Periodo.Hours<0 || _trat.Periodo.Hours>23?0:_trat.Periodo.Hours;
                    nMinutos.Value = _trat.Periodo.Minutes < 0 || _trat.Periodo.Minutes > 59 ? 0 : _trat.Periodo.Minutes;
                    nDias.Value = _trat.Periodo.Days>0?_trat.Periodo.Days:0;

                    switch (_trat.Tipo)
                    {
                        case gManager.gcTratamiento.TipoTratamiento.Aviso:
                            lblTipo.Image = Properties.Resources.Alarma;
                            
                            break;
                        case gManager.gcTratamiento.TipoTratamiento.Periodico:
                            lblTipo.Image = Properties.Resources.Periodico;
                            break;
                        case gManager.gcTratamiento.TipoTratamiento.Recordatorio:
                            lblTipo.Image = Properties.Resources.Recordatorio;
                            break;
                        case gManager.gcTratamiento.TipoTratamiento.Tarea:
                            lblTipo.Image = Properties.Resources.Tarea;
                            break;
                        case gManager.gcTratamiento.TipoTratamiento.Tratamiento:
                            lblTipo.Image = Properties.Resources.Tratamiento;
                            break;
                    }
                }
                
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            string t = "El evento se ejecutará en ";
            if (nDias.Value > 0)
                t += nDias.Value + " días ";
            if (nHoras.Value > 0)
                t += nHoras.Value + " horas ";
            if (nMinutos.Value > 0)
                t += nMinutos.Value + " minutos ";
            lblPeriodicodesc.Text = t;
        }
    }
}
