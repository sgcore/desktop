﻿using BrightIdeasSoftware;
using gManager;
using System;
using System.Windows.Forms;

namespace gControls.Editores
{
    public partial class EditorMovimiento : BaseControl
    {
        public EditorMovimiento()
            : base()
        {
            InitializeComponent();
            Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            lwOM.FormatCell +=mov_FormatCell;
            colMonto.HeaderImageKey = "Precio";
            colTotal.HeaderImageKey = "Precio";
            colMontoCotizado.HeaderImageKey = "Cotizacion";
            colTotalCotizado.HeaderImageKey = "Cotizacion";
            colMontoSIVA.HeaderImageKey = "Iva";
            colTotalSIVA.HeaderImageKey = "Iva";
            colMontoSIVACot.HeaderImageKey = "Total";
            colTotalSIVACot.HeaderImageKey = "Total";
            colIVA.HeaderImageKey = "Iva";
            colCotizacion.HeaderImageKey = "Cotizacion";
            colNombre.HeaderImageKey = "Producto";
            setEstadoInicial();
            gManager.CoreManager.Singlenton.MovimientoSeleccionado += Singlenton_MovimientoSeleccionado;
            gManager.CoreManager.Singlenton.MovimientoEliminado += Singlenton_MovimientoEliminado;
            gManager.CoreManager.Singlenton.ProductoSeleccionado += Singlenton_ProductoSeleccionado;

           
        }

        void Singlenton_ProductoSeleccionado(gcObjeto o)
        {
            if (RecibirGlobal) addProducto(o);
        }

        void Singlenton_MovimientoEliminado(gcMovimiento m)
        {
            if (RecibirGlobal && Movimiento != null && (Movimiento.Id == m.Id))
            {
                Movimiento = null;
            }
        }

        void Singlenton_MovimientoSeleccionado(gcMovimiento m)
        {
            if (RecibirGlobal && m != null) Movimiento = m;
        }
          private void mov_FormatCell(object sender, FormatCellEventArgs e)
        {
            gcObjetoMovido  om = (gcObjetoMovido) e.Model;
            if (e.ColumnIndex == this.colCantidad.Index)
            {
                if (om.Fraccionado)
                    e.SubItem.Text = om.CantidadFracciones + "/" + om.Fracciones;
                e.SubItem.Text = om.Cantidad.ToString("0.00");
               
            }
            else if (e.ColumnIndex == this.colCodigo.Index)
            {
                e.SubItem.Text = om.Objeto.Codigo;
            }
            else if (e.ColumnIndex == this.colNombre.Index)
            {
                e.SubItem.Text = om.Objeto.Nombre;
            }
        }
        gcMovimiento _movimiento = null;
       public gcMovimiento Movimiento
        {
            get
            {
                return _movimiento;
            }
            set
            {
                SuspendLayout();
                _movimiento = value;
                visorTotalesMovimiento1.setMovimiento(_movimiento);
                visorMovimiento1.setMovimiento(_movimiento);
                lwOM.Visible = _movimiento != null;
                lblDescPago.Visible = _movimiento != null;
                panelAgregarOM1.setMovimiento(_movimiento);
                if (_movimiento != null)
                {
                    visorDestinatario1.Destinatario = _movimiento.Destinatario;
                    lwOM.SetObjects(_movimiento.ObjetosMovidos);
                    lwOM.Visible = _movimiento.EsMovimientoMercaderia || _movimiento.EsPresupuestooPedido;
                    lblDescPago.Text = _movimiento.DescripcionPago;
                    lblDescPago.Visible = _movimiento.EsMovimientoDinero;
                   
                    

                   
                }
                else
                {
                    setEstadoInicial();
                }
                ResumeLayout();
            }
        }
       public EventHandler CerrandoMovimiento;
        private enum tipoVistaColumnas { Normal,Discriminado,Cotizado,Discriminado_Cotizado}
        private tipoVistaColumnas _tipovista;
        private void setTipoVista(tipoVistaColumnas t)
        {
            _tipovista = t;
            setVistaColumnas();
        }
        
      
        private void setVistaColumnas()
        {
            colMonto.IsVisible = _tipovista==tipoVistaColumnas.Normal;
            colMontoCotizado.IsVisible = _tipovista == tipoVistaColumnas.Cotizado; ;
            colMontoSIVA.IsVisible = _tipovista == tipoVistaColumnas.Discriminado; ;
            colMontoSIVACot.IsVisible = _tipovista == tipoVistaColumnas.Discriminado_Cotizado; ;
            
            colTotal.IsVisible = _tipovista == tipoVistaColumnas.Normal; ;
            colTotalCotizado.IsVisible = _tipovista == tipoVistaColumnas.Cotizado; ;
            colTotalSIVA.IsVisible = _tipovista == tipoVistaColumnas.Discriminado; ;
            colTotalSIVACot.IsVisible = _tipovista == tipoVistaColumnas.Discriminado_Cotizado; ;
            colCotizacion.IsVisible = _tipovista == tipoVistaColumnas.Cotizado || _tipovista == tipoVistaColumnas.Discriminado_Cotizado;
            colIVA.IsVisible = _tipovista == tipoVistaColumnas.Discriminado || _tipovista == tipoVistaColumnas.Discriminado_Cotizado;
            lwOM.RebuildColumns();
           
            
            
        }
        public void addProducto(gcObjeto o)
        {
           panelAgregarOM1.addProducto(o);
        }
        public bool RecibirGlobal { get; set; }
        

        public void setEstadoInicial()
        {
            setVistaColumnas();
            visorDestinatario1.Destinatario = null;
            lwOM.SetObjects(null);
            //ocultamos cotizaciones
            visorTotalesMovimiento1.setMovimiento(null);
             
            panelAgregarOM1.setMovimiento(null);
            CerrandoMovimiento?.Invoke(null, null);

        }

        private void lwOM_KeyDown(object sender, KeyEventArgs e)
        {
            //e.SuppressKeyPress = true;
            if (_movimiento == null || _movimiento.Terminado) return;
            
            var om = (gcObjetoMovido)lwOM.SelectedObject;

            if (om == null) return;
            if (e.KeyCode == Keys.Delete)
            {
                _movimiento.removeObjetoMovido(om.Id);
                reload();

            }
            else if (e.KeyCode == Keys.Enter)
            {
                om = Editar.EditarFraccion(om);
                reload();
               
            }
           
            else if (e.KeyCode == Keys.OemMinus || e.KeyCode==Keys.Subtract)
            {
                decimal c = 1;
                if (e.Control)
                    c = c / 10;
                if(e.Alt)
                    c = c / 10;
                if ((om.Cantidad-c)  > 0)
                {
                    om.incrementar(-c);
                    reload();
                }
                    

            }
            else if (e.KeyCode == Keys.Oemplus || e.KeyCode==Keys.Add)
            {
                decimal c = 1;
                if (e.Control)
                    c = c / 10;
                if (e.Alt)
                    c = c / 10;
                    om.incrementar(c);
                    reload();
               

            }
          
        }
        private void reload()
        {
            if (_movimiento != null)
                _movimiento.reloadMontos();
            int selindx = lwOM.SelectedIndex;
            CoreManager.Singlenton.seleccionarMovimiento(_movimiento);
            if (lwOM.Items.Count >= selindx)
            {
                lwOM.SelectedIndex = selindx;
                lwOM.Select();
            }
        }

        private void lwOM_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (_movimiento == null || _movimiento.Terminado) return;

            var om = (gcObjetoMovido)lwOM.SelectedObject;

            if (om == null) return;
            om = Editar.EditarFraccion(om);
            reload();

        }

        private void btCerrar_Click(object sender, EventArgs e)
        {
            Movimiento = null;
        }
        

        
       
    }
}
