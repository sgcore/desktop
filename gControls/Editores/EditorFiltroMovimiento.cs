﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using gManager.Filtros;

namespace gControls.Editores
{
    public partial class EditorFiltroMovimiento : UserControl
    {
        private gManager.Filtros.FiltroMovimiento _fil;
        public EditorFiltroMovimiento()
        {
            InitializeComponent();
            _fil = new gManager.Filtros.FiltroMovimiento();
            CrearBotonesTipo();
         
        }
        private void CrearBotonesTipo()
        {
            PanelTipos.Controls.Clear();
            foreach (var r in Enum.GetValues(typeof(gcMovimiento.TipoMovEnum)))
            {
                var nb = new BotonOpcion((int)r, Enum.GetName(typeof(gcMovimiento.TipoMovEnum), r),(Bitmap)MovImageList.Images[(int)r]);
                if (_fil.TiposAFiltrar.Exists(x => x == (int)r))
                    nb.Checked = true;
                PanelTipos.Controls.Add(nb);
            }
             
        }
        public void seleccionarTodosbt(object sender, EventArgs e)
        {
            foreach (BotonOpcion bo in PanelTipos.Controls)
            {
                bo.Checked = true;
            }
        }
        public void deseleccionarTodosbt(object sender, EventArgs e)
        {
            foreach (BotonOpcion bo in PanelTipos.Controls)
            {
                bo.Checked = false;
            }
        }

        public gManager.Filtros.FiltroMovimiento Filtro
        {
            get
            {
               _fil.CajaFinal = FiltroCajas.CajaFinal;
               _fil.CajaInicial = FiltroCajas.CajaInicial;
               _fil.porCaja = FiltroCajas.EstaFiltradoPorNumero;
                _fil.porFecha = FiltroCajas.esEntreFechas  ;
                _fil.FechaFinal = FiltroCajas.FechaInicial;
                _fil.FechaInicial = FiltroCajas.FechaFinal;
                _fil.FiltroSQL = FiltroWhere;
                _fil.Incompletos = Incompletos;
                _fil.NumeroEspecifico = (int)nEspecifico.Value;
                
                //tipos
                _fil.TiposAFiltrar.Clear();
                foreach (Control c in PanelTipos.Controls)
                {
                    var b = (BotonOpcion)c;
                    if (b.Checked) _fil.TiposAFiltrar.Add(b.Opcion);
                }
                return _fil;
            }
            set
            {
                if (value.porCaja)
                {
                    FiltroCajas = new gManager.Filtros.FiltroCaja(value.CajaInicial, value.CajaFinal);
                }
                else if (value.porFecha)
                {
                    FiltroCajas = new gManager.Filtros.FiltroCaja(value.FechaInicial, value.FechaFinal);
                }
                
                 
                Destinatario = value.Destinatario;
                Producto = value.Producto;
                Incompletos = value.Incompletos;
                 
            //tipos
                nEspecifico.Value = value.NumeroEspecifico;
                PanelTipos.Controls.Clear();
                foreach (var r in Enum.GetValues(typeof(gcMovimiento.TipoMovEnum)))
                {
                    var nb = new BotonOpcion((int)r, Enum.GetName(typeof(gcMovimiento.TipoMovEnum), r), Properties.Resources.Aceptar16);
                    if (value.TiposAFiltrar.Exists(x => x == (int)r))
                        nb.Checked = true;
                    PanelTipos.Controls.Add(nb);
                }
            }
        }
        public gManager.Filtros.FiltroCaja FiltroCajas
        {
            get { return EditorFiltroCajas.Filtro; }
            set { EditorFiltroCajas.Filtro = value; }
        }
        public bool Incompletos
        {
            get { return chkIncompletos.Checked; }
            set { chkIncompletos.Checked = value; }
        }
        
        public gcDestinatario Destinatario
        {
            get
            {
                return _fil.Destinatario;
            }
            set
            {
                _fil.Destinatario = value;
                cheking = true;
                if (Destinatario == null)
                {
                    chkFiltrarDest.Checked = false; chkFiltrarDest.Text = "Elija un destinatario"; chkFiltrarDest.ForeColor = System.Drawing.Color.Red;
                }
                else { chkFiltrarDest.Text = "Filtrado por: " + Destinatario.Nombre; chkFiltrarDest.ForeColor = System.Drawing.Color.Green; chkFiltrarDest.Checked = true; }
                cheking = false;
            }
        }
        public gcObjeto Producto
        {
            get
            {
                return _fil.Producto;
            }
            set
            {
                _fil.Producto = value;
                cheking = true;
                if (Producto == null)
                {
                    chkFiltrarProd.Checked = false; chkFiltrarProd.Text = "Elija un Producto"; chkFiltrarProd.ForeColor = System.Drawing.Color.Red;
                }
                else { chkFiltrarProd.Text = "Filtrado por: " + Producto.Nombre; chkFiltrarProd.ForeColor = System.Drawing.Color.Green; chkFiltrarProd.Checked = true; }
                cheking = false;
            }
        }

        public string FiltroWhere
        {
            get
            {
                var f = FiltroCajas;
                
                if (f.esEntreCajas)
                {
                    
                    return "(caja>=" + f.CajaInicial + " and caja<=" + f.CajaFinal + ")";

                }
                else if (f.esCajaActual || f.esUnaCaja)
                {
                    return "(caja=" + f.CajaSola + ")";
                }
                else if (f.esEntreFechas)
                {
                   
                    return "(fecha BETWEEN '" + f.FechaInicial.ToString("yyy-M-d") + "' and '" + f.FechaFinal.ToString("yyy-M-d") + "')"; ;
                }
                else
                    return "";
            }
        }
        bool cheking;
        private void chkFiltrarDest_CheckedChanged(object sender, EventArgs e)
        {
            if(cheking)return ;
            cheking=true;
            if (chkFiltrarDest.Checked)
                Destinatario = buscar.BuscarDestinatario("Filtrar por Destino", gcDestinatario.DestinatarioTipo.Cliente,true);
            else Destinatario = null;
            
            cheking = false;
        }

        private void chkFiltrarProd_CheckedChanged(object sender, EventArgs e)
        {
            if (cheking) return;
            cheking = true;
            if (chkFiltrarProd.Checked)
            {
                var f = new FiltroProducto();
                f.Tipos.Add(gcObjeto.ObjetoTIpo.Producto);
                f.Tipos.Add(gcObjeto.ObjetoTIpo.Servicio);
                Producto = buscar.BuscarProducto("Buscar el producto o servicio", f);
            }
            else Producto = null;

            cheking = false;
        }
        
        
    }
    
}
