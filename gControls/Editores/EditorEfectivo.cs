﻿using System;
using System.Drawing;
using System.Windows.Forms;
using gManager;
using gLogger;

namespace gControls.Editores
{
    
    public partial class EditorEfectivo : UserControl
    {
        private gcPago _sugerido = null;
        private gcPago _blockeo = null;
        public event EventHandler CambioTotal;
        public EditorEfectivo()
        {
            InitializeComponent();
             foreach ( Control c in Controls){
                 if(c.GetType ()== typeof ( NumericUpDown)){
                 NumericUpDown nn = (NumericUpDown)c;
                nn.ValueChanged +=new EventHandler(nn_ValueChanged);
                nn.GotFocus+=new EventHandler(nn_GotFocus); 
                nn.LostFocus+=new EventHandler(nn_LostFocus);
                nn.KeyDown+=new KeyEventHandler(nn_KeyDown);

                 }
             }
        }

        void  nn_KeyDown(object sender, KeyEventArgs e)
        {
 	         if(e.KeyCode == Keys.Enter ){
                       Control c = (Control)sender;

                        e.SuppressKeyPress = true;
                        SendKeys.Send("{TAB}");
             }
        }

        void  nn_LostFocus(object sender, EventArgs e)
        {
 	          NumericUpDown nn=(NumericUpDown) sender;
              colorear(nn);
        }

        void  nn_GotFocus(object sender, EventArgs e)
        {
 	          NumericUpDown nn=(NumericUpDown) sender;
                    nn.BackColor = Color.CadetBlue;
                    nn.ForeColor = Color.Black;
                    nn.Select(0, 10);
        }

        void  nn_ValueChanged(object sender, EventArgs e)
        {
 	            SacarTotal ();
        }
        public decimal SacarTotal()
        {
            decimal total = 0;
            total += n1000.Value * 1000;
            total += n500.Value * 500;
            total += n200.Value * 200;
            total += n100.Value * 100;
        total += n50.Value * 50;
        total += n20.Value * 20;
        total += n10.Value * 10;
        total += n5.Value * 5;
        total += n2.Value * 2;
        total += n1.Value * 1;
        total += n050.Value *(decimal) 0.5;
        total += n025.Value * (decimal)0.25;
        total += n010.Value * (decimal)0.1;
        total += n005.Value * (decimal)0.05;
        total += n001.Value * (decimal)0.01;

        lblTotal.Text =total.ToString("$0.00");
        if (CambioTotal != null) CambioTotal(null, null);
       return total;

        }
       
        public void restringir(gcPago sugg, gcPago block)
        {
           if(block!=null) Bloquear(block);
            if(sugg!=null)sugerir(sugg.Monto);

        }
        public void restringir(decimal monto, gcPago block)
        {
            if (block != null) Bloquear(block);
            sugerir(monto);

        }
        private void sugerir(decimal monto)
        {
            if (_blockeo != null)
                _sugerido=crearPagoConBloqueo(monto, _blockeo);
            else
            {
                _sugerido=new gcPago(monto,gcPago.TipoPago.Efectivo);
                lblSugerencia.Text = "Sugerido: " + _sugerido.Monto.ToString("$0.00");
                lblSugerencia.ForeColor = Color.DarkSeaGreen;
            }
               
        }
        private gcPago crearPagoConBloqueo(decimal monto, gcPago block)
        {
            monto = Math.Abs(monto);
            decimal cont = 0;
            int[] bi=block.Billetes;
            decimal[] nom=block.Nominaciones;
            int[] np = new int[block.Billetes.Length];
            for(int n=0;n<bi.Length;n++)
            {
                int cant=bi[n];
                while(cant>0 && cont+nom[n]<=monto){
                    np[n]++;
                    int misbilletes = np[n];
                    decimal nominacion = nom[n];
                    cont += nominacion;
                   // cont += np[n] * nom[n];
                    cant--;
                }
                
            }
            gcPago ret=new gcPago(np,gcPago.TipoPago.Efectivo);
            if(ret.SacarTotal()<monto || ret.Monto<=0){
                lblSugerencia.Text ="No hay billetes suficientes.";
                lblSugerencia.ForeColor = Color.Coral;
            }
            else
            {
                lblSugerencia.Text = "Sugerido: " + ret.Monto.ToString("$0.00");
                lblSugerencia.ForeColor = Color.DarkSeaGreen;
            }
            return ret;
        }
        public void EstablecerTotal(decimal monto)
        {
            monto = Math.Abs(monto);
            
            monto = Math.Round(monto, 2);

            n1000.Value = Math.Truncate(monto / 1000);
            monto = monto - (1000 * n1000.Value);

            n500.Value = Math.Truncate(monto / 500);
            monto = monto - (500 * n500.Value);

            n200.Value = Math.Truncate(monto / 200);
            monto = monto - (200 * n200.Value);




            n100.Value = Math.Truncate(monto / 100);
        monto = monto - (100 * n100.Value);

        n50.Value = Math.Truncate(monto / 50);
        monto = monto - (50 * n50.Value);

        n20.Value = Math.Truncate(monto / 20);
        monto = monto - (20 * n20.Value);

        n10.Value = Math.Truncate(monto / 10);
        monto = monto - (10 * n10.Value);

        n5.Value = Math.Truncate(monto / 5);
        monto = monto - (5 * n5.Value);

        n2.Value = Math.Truncate(monto / 2);
        monto = monto - (2 * n2.Value);

        n1.Value = Math.Truncate(monto / 1);
        monto = monto - (1 * n1.Value);

        n050.Value = Math.Truncate(monto /(decimal) 0.5);
        monto = monto - ((decimal)0.5 * n050.Value);

        
        n025.Value = Math.Truncate(monto /(decimal) 0.25);
        monto = monto - ((decimal)0.25 * n025.Value);

        n010.Value = Math.Truncate(monto /(decimal) 0.1);
        monto = monto - ((decimal)0.1 * n010.Value);


        n005.Value = Math.Truncate(monto /(decimal) 0.05);
        monto = monto - ((decimal)0.05 * n005.Value);

        n001.Value = Math.Truncate(Math.Round(monto, 2) /(decimal) 0.01);
        monto = monto - ((decimal)0.01 * n001.Value);
       foreach (Control c in Controls)
            if  (c.GetType()== typeof (NumericUpDown)){
                NumericUpDown nn =(NumericUpDown) c;
                colorear(nn);
            }
       if (CambioTotal != null) CambioTotal(null, null);
        }
        public void EstablecerTotal(gcPago p)
        {
            if (p == null)
            {
                EstablecerTotal(0);
                return;
            }
            try
            {
                n1000.Value = p.m1000;
                n500.Value = p.m500;
                n200.Value = p.m200;
                n100.Value = p.m100;

                n50.Value = p.m50; ;

                n20.Value = p.m20; ;

                n10.Value = p.m10;

                n5.Value = p.m5;

                n2.Value = p.m2;

                n1.Value = p.m1;

                n050.Value = p.m050;

                n010.Value = p.m010;

                n025.Value = p.m025;

                n005.Value = p.m005;

                n001.Value = p.m001;
            }
            catch { }
            foreach (Control c in Controls)
                if (c.GetType() == typeof(NumericUpDown))
                {
                    NumericUpDown nn = (NumericUpDown)c;
                    colorear(nn);
                }
        }
        public int[] Monedas()
        {
            int[] t = { (int)n1000.Value, (int)n500.Value, (int)n200.Value, (int)n100.Value, (int)n50.Value, (int)n20.Value, (int)n10.Value, (int)n5.Value, (int)n2.Value, (int)n1.Value, (int)n050.Value, (int)n025.Value, (int)n010.Value, (int)n005.Value, (int)n001.Value };
            return t;
        }
        public gcPago ObtenerPago(gcPago.TipoPago t)
        {
            return new gcPago(Monedas(),t);
        }
        private void Bloquear(gcPago pb)
        {
            if (pb == null || pb.Monto <= 0)
            {
                logger.Singlenton.addWarningMsg("Se intento bloquear el editor de Efectivo con parametros incorrectos.\nEsto suele suceder, por ejemplo, cuando no hay suficientes billetes para dar un vuelto.");
                return;
            }
            _blockeo = pb;
            if (pb.m1000 > 0) n1000.Maximum = pb.m1000; else n1000.Maximum = 0;
            if (pb.m500 > 0) n500.Maximum = pb.m500; else n500.Maximum = 0;
            if (pb.m200 > 0) n200.Maximum = pb.m200; else n200.Maximum = 0;
            if (pb.m100 > 0) n100.Maximum = pb.m100; else n100.Maximum = 0;
            if (pb.m50 > 0) n50.Maximum = pb.m50; else n50.Maximum = 0;
            if (pb.m20 > 0) n20.Maximum = pb.m20; else n20.Maximum = 0;
            if (pb.m10 > 0) n10.Maximum = pb.m10; else n10.Maximum = 0;
            if (pb.m5 > 0) n5.Maximum = pb.m5; else n5.Maximum = 0;
            if (pb.m2 > 0) n2.Maximum = pb.m2; else n2.Maximum = 0;
            if (pb.m1 > 0) n1.Maximum = pb.m1; else n1.Maximum = 0;
            if (pb.m050 > 0) n050.Maximum = pb.m050; else n050.Maximum = 0;
            if (pb.m025 > 0) n025.Maximum = pb.m025; else n025.Maximum = 0;
            if (pb.m010 > 0) n010.Maximum = pb.m010; else n010.Maximum = 0;
            if (pb.m005 > 0) n005.Maximum = pb.m005; else n005.Maximum = 0;
            if (pb.m001 > 0) n001.Maximum = pb.m001; else n001.Maximum = 0;
            btBlockear.Enabled = true;
            lblBloqueado.ForeColor = Color.Coral;
            
            lblBloqueado.Text = "¡Bloqueado!";
        }
        private void colorear(NumericUpDown nn)
        {
            if(nn.Value > 0 ){
                nn.BackColor = Color.Orange;
            }else{
    
                nn.BackColor = Color.White;
            }
            nn.ForeColor = Color.Black;
        }
        private void clickearBillete(NumericUpDown n)
        {
            int i = 1;
            if (Control.ModifierKeys == Keys.Control)
            {
                i = -1;
                if (n.Value == 0) i = 0;
            }
           if((n.Value+i)<=n.Maximum) n.Value += i;
            colorear(n);
        }
        private void bt100_Click(object sender, EventArgs e)
        {
            clickearBillete (n100);
        }

        private void bt10_Click(object sender, EventArgs e)
        {
            clickearBillete(n10);
        }

        private void bt50_Click(object sender, EventArgs e)
        {
            clickearBillete(n50);
        }

        private void bt5_Click(object sender, EventArgs e)
        {
            clickearBillete(n5);
        }

        private void bt20_Click(object sender, EventArgs e)
        {
            clickearBillete(n20);
        }

        private void bt2_Click(object sender, EventArgs e)
        {
            clickearBillete(n2);
        }

        private void bt1_Click(object sender, EventArgs e)
        {
            clickearBillete(n1);
        }

        private void bt050_Click(object sender, EventArgs e)
        {
            clickearBillete(n050);
        }

        private void bt025_Click(object sender, EventArgs e)
        {
            clickearBillete(n025);
        }

        private void bt010_Click(object sender, EventArgs e)
        {
            clickearBillete(n010);
        }

        private void bt005_Click(object sender, EventArgs e)
        {
            clickearBillete(n005);
        }

        private void bt001_Click(object sender, EventArgs e)
        {
            clickearBillete(n001);
        }

        private void btCero_Click(object sender, EventArgs e)
        {
            EstablecerTotal(0);
        }

        private void btBlockear_Click(object sender, EventArgs e)
        {
            n1000.Maximum = 999999;
            n500.Maximum = 999999;
            n200.Maximum = 999999;
            n100.Maximum = 999999;
            n50.Maximum = 999999;
            n20.Maximum = 999999;
            n10.Maximum = 999999;
            n5.Maximum = 999999;
            n2.Maximum = 999999;
            n1.Maximum = 999999;
            n050.Maximum = 999999;
            n025.Maximum = 999999;
            n10.Maximum = 999999;
            n005.Maximum = 999999;
            n001.Maximum = 999999;
            btBlockear.Enabled = false;
            btBlockear.ForeColor = Color.LightGray;

            lblBloqueado.Text = "Desbloqueado";
            lblBloqueado.ForeColor = Color.LightGray;
        }

        private void btSugerir_Click(object sender, EventArgs e)
        {
            if (_sugerido != null)
            {
                EstablecerTotal(_sugerido);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            clickearBillete(n1000);
        }

        private void bt500_Click(object sender, EventArgs e)
        {
            clickearBillete(n500);
        }

        private void bt200_Click(object sender, EventArgs e)
        {
            clickearBillete(n200);
        }
    } 
 }
    

