﻿namespace gControls.Editores
{
    partial class EditorConfigEditorMov
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.nCantidad = new System.Windows.Forms.NumericUpDown();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nCantidad)).BeginInit();
            this.SuspendLayout();
            // 
            // nCantidad
            // 
            this.nCantidad.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::gControls.Properties.Settings.Default, "cfgResetCantValue", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nCantidad.DecimalPlaces = 3;
            this.nCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nCantidad.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nCantidad.Location = new System.Drawing.Point(20, 63);
            this.nCantidad.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.nCantidad.Name = "nCantidad";
            this.nCantidad.Size = new System.Drawing.Size(70, 22);
            this.nCantidad.TabIndex = 2;
            this.nCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nCantidad.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.nCantidad.Value = global::gControls.Properties.Settings.Default.cfgResetCantValue;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = global::gControls.Properties.Settings.Default.cfgSelCantidadAlAgregar;
            this.checkBox2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::gControls.Properties.Settings.Default, "cfgSelCantidadAlAgregar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox2.Location = new System.Drawing.Point(20, 40);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(294, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Seleccionar la casilla de cantidad al agregar un producto";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = global::gControls.Properties.Settings.Default.cfgAutoguardarAlCrearMov;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::gControls.Properties.Settings.Default, "cfgAutoguardarAlCrearMov", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox1.Location = new System.Drawing.Point(20, 17);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(192, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Autoguardar al crear el movimiento.";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(0, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(319, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "NOTA:  Cancelar no guarda la configuración pero no la desactiva.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(96, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(281, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Establecer la cantidad al agregar un producto (0 no altera)";
            // 
            // EditorConfigEditorMov
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nCantidad);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Name = "EditorConfigEditorMov";
            this.Size = new System.Drawing.Size(414, 115);
            ((System.ComponentModel.ISupportInitialize)(this.nCantidad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.NumericUpDown nCantidad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
