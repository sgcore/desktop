﻿namespace gControls.Editores
{
    partial class EditorDestinatario
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorDestinatario));
            this.lblMail = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.lblCuit = new System.Windows.Forms.Label();
            this.txtCuit = new System.Windows.Forms.TextBox();
            this.lblDire = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.lblCel = new System.Windows.Forms.Label();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.lblTele = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblObs = new System.Windows.Forms.Label();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.dtNacimiento = new System.Windows.Forms.DateTimePicker();
            this.lblFecha = new System.Windows.Forms.Label();
            this.btPropietario = new System.Windows.Forms.Button();
            this.gbPropietario = new System.Windows.Forms.GroupBox();
            this.lblPropietario = new System.Windows.Forms.Label();
            this.lblLastMod = new System.Windows.Forms.Label();
            this.DestImageList = new System.Windows.Forms.ImageList(this.components);
            this.btGuardar = new System.Windows.Forms.Button();
            this.btCancelar = new System.Windows.Forms.Button();
            this.cmbIva = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblTipoIva = new System.Windows.Forms.Label();
            this.gbPropietario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMail
            // 
            this.lblMail.AutoSize = true;
            this.lblMail.Location = new System.Drawing.Point(263, 76);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(29, 13);
            this.lblMail.TabIndex = 23;
            this.lblMail.Text = "Mail:";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(266, 92);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(185, 20);
            this.txtMail.TabIndex = 8;
            // 
            // lblCuit
            // 
            this.lblCuit.AutoSize = true;
            this.lblCuit.Location = new System.Drawing.Point(263, 38);
            this.lblCuit.Name = "lblCuit";
            this.lblCuit.Size = new System.Drawing.Size(28, 13);
            this.lblCuit.TabIndex = 21;
            this.lblCuit.Text = "Cuit:";
            // 
            // txtCuit
            // 
            this.txtCuit.Location = new System.Drawing.Point(266, 54);
            this.txtCuit.Name = "txtCuit";
            this.txtCuit.Size = new System.Drawing.Size(120, 20);
            this.txtCuit.TabIndex = 7;
            // 
            // lblDire
            // 
            this.lblDire.AutoSize = true;
            this.lblDire.Location = new System.Drawing.Point(11, 54);
            this.lblDire.Name = "lblDire";
            this.lblDire.Size = new System.Drawing.Size(55, 13);
            this.lblDire.TabIndex = 19;
            this.lblDire.Text = "Dirección:";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(14, 70);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(243, 20);
            this.txtDireccion.TabIndex = 1;
            // 
            // lblCel
            // 
            this.lblCel.AutoSize = true;
            this.lblCel.Location = new System.Drawing.Point(140, 95);
            this.lblCel.Name = "lblCel";
            this.lblCel.Size = new System.Drawing.Size(42, 13);
            this.lblCel.TabIndex = 17;
            this.lblCel.Text = "Celular:";
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(143, 111);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(114, 20);
            this.txtCelular.TabIndex = 3;
            // 
            // lblTele
            // 
            this.lblTele.AutoSize = true;
            this.lblTele.Location = new System.Drawing.Point(11, 95);
            this.lblTele.Name = "lblTele";
            this.lblTele.Size = new System.Drawing.Size(52, 13);
            this.lblTele.TabIndex = 15;
            this.lblTele.Text = "Telefono:";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(14, 111);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(114, 20);
            this.txtTelefono.TabIndex = 2;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(11, 15);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 13;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(14, 31);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(243, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // lblObs
            // 
            this.lblObs.AutoSize = true;
            this.lblObs.Location = new System.Drawing.Point(260, 118);
            this.lblObs.Name = "lblObs";
            this.lblObs.Size = new System.Drawing.Size(81, 13);
            this.lblObs.TabIndex = 25;
            this.lblObs.Text = "Observaciones:";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Location = new System.Drawing.Point(263, 134);
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(188, 52);
            this.txtObservaciones.TabIndex = 9;
            // 
            // dtNacimiento
            // 
            this.dtNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtNacimiento.Location = new System.Drawing.Point(266, 15);
            this.dtNacimiento.Name = "dtNacimiento";
            this.dtNacimiento.Size = new System.Drawing.Size(100, 20);
            this.dtNacimiento.TabIndex = 6;
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(263, -1);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(42, 13);
            this.lblFecha.TabIndex = 28;
            this.lblFecha.Text = "Ingreso";
            // 
            // btPropietario
            // 
            this.btPropietario.Location = new System.Drawing.Point(6, 15);
            this.btPropietario.Name = "btPropietario";
            this.btPropietario.Size = new System.Drawing.Size(53, 23);
            this.btPropietario.TabIndex = 1;
            this.btPropietario.Text = "Dueño";
            this.btPropietario.UseVisualStyleBackColor = true;
            this.btPropietario.Click += new System.EventHandler(this.btPropietario_Click);
            // 
            // gbPropietario
            // 
            this.gbPropietario.Controls.Add(this.lblPropietario);
            this.gbPropietario.Controls.Add(this.btPropietario);
            this.gbPropietario.Location = new System.Drawing.Point(14, 161);
            this.gbPropietario.Name = "gbPropietario";
            this.gbPropietario.Size = new System.Drawing.Size(197, 46);
            this.gbPropietario.TabIndex = 5;
            this.gbPropietario.TabStop = false;
            this.gbPropietario.Text = "Propietario";
            this.gbPropietario.Visible = false;
            // 
            // lblPropietario
            // 
            this.lblPropietario.AutoSize = true;
            this.lblPropietario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPropietario.Location = new System.Drawing.Point(65, 18);
            this.lblPropietario.Name = "lblPropietario";
            this.lblPropietario.Size = new System.Drawing.Size(53, 16);
            this.lblPropietario.TabIndex = 31;
            this.lblPropietario.Text = "Dueño";
            // 
            // lblLastMod
            // 
            this.lblLastMod.AutoSize = true;
            this.lblLastMod.Location = new System.Drawing.Point(6, 210);
            this.lblLastMod.Name = "lblLastMod";
            this.lblLastMod.Size = new System.Drawing.Size(67, 13);
            this.lblLastMod.TabIndex = 34;
            this.lblLastMod.Text = "Modificación";
            // 
            // DestImageList
            // 
            this.DestImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("DestImageList.ImageStream")));
            this.DestImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.DestImageList.Images.SetKeyName(0, "Cliente");
            this.DestImageList.Images.SetKeyName(1, "Proveedor");
            this.DestImageList.Images.SetKeyName(2, "Responsable");
            this.DestImageList.Images.SetKeyName(3, "Sucursal");
            // 
            // btGuardar
            // 
            this.btGuardar.AutoSize = true;
            this.btGuardar.Image = global::gControls.Properties.Resources.Like;
            this.btGuardar.Location = new System.Drawing.Point(271, 199);
            this.btGuardar.Name = "btGuardar";
            this.btGuardar.Size = new System.Drawing.Size(78, 23);
            this.btGuardar.TabIndex = 10;
            this.btGuardar.Text = "Guardar";
            this.btGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btGuardar.UseVisualStyleBackColor = true;
            this.btGuardar.Click += new System.EventHandler(this.btGuardar_Click);
            // 
            // btCancelar
            // 
            this.btCancelar.AutoSize = true;
            this.btCancelar.Image = global::gControls.Properties.Resources.Unlike;
            this.btCancelar.Location = new System.Drawing.Point(355, 200);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(75, 23);
            this.btCancelar.TabIndex = 11;
            this.btCancelar.Text = "Cancelar";
            this.btCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCancelar.UseVisualStyleBackColor = true;
            this.btCancelar.Click += new System.EventHandler(this.btCancelar_Click);
            // 
            // cmbIva
            // 
            this.cmbIva.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIva.FormattingEnabled = true;
            this.cmbIva.Location = new System.Drawing.Point(71, 134);
            this.cmbIva.Name = "cmbIva";
            this.cmbIva.Size = new System.Drawing.Size(121, 21);
            this.cmbIva.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(457, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // lblTipoIva
            // 
            this.lblTipoIva.AutoSize = true;
            this.lblTipoIva.Location = new System.Drawing.Point(17, 137);
            this.lblTipoIva.Name = "lblTipoIva";
            this.lblTipoIva.Size = new System.Drawing.Size(48, 13);
            this.lblTipoIva.TabIndex = 36;
            this.lblTipoIva.Text = "Tipo IVA";
            // 
            // EditorDestinatario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lblTipoIva);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cmbIva);
            this.Controls.Add(this.btCancelar);
            this.Controls.Add(this.btGuardar);
            this.Controls.Add(this.lblLastMod);
            this.Controls.Add(this.gbPropietario);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.dtNacimiento);
            this.Controls.Add(this.lblObs);
            this.Controls.Add(this.txtObservaciones);
            this.Controls.Add(this.lblMail);
            this.Controls.Add(this.txtMail);
            this.Controls.Add(this.lblCuit);
            this.Controls.Add(this.txtCuit);
            this.Controls.Add(this.lblDire);
            this.Controls.Add(this.txtDireccion);
            this.Controls.Add(this.lblCel);
            this.Controls.Add(this.txtCelular);
            this.Controls.Add(this.lblTele);
            this.Controls.Add(this.txtTelefono);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.txtNombre);
            this.Name = "EditorDestinatario";
            this.Size = new System.Drawing.Size(678, 239);
            this.gbPropietario.ResumeLayout(false);
            this.gbPropietario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ImageList DestImageList;
        protected System.Windows.Forms.Label lblMail;
        protected System.Windows.Forms.TextBox txtMail;
        protected System.Windows.Forms.Label lblCuit;
        protected System.Windows.Forms.TextBox txtCuit;
        protected System.Windows.Forms.Label lblDire;
        protected System.Windows.Forms.TextBox txtDireccion;
        protected System.Windows.Forms.Label lblCel;
        protected System.Windows.Forms.TextBox txtCelular;
        protected System.Windows.Forms.Label lblTele;
        protected System.Windows.Forms.TextBox txtTelefono;
        protected System.Windows.Forms.Label lblNombre;
        protected System.Windows.Forms.TextBox txtNombre;
        protected System.Windows.Forms.Label lblObs;
        protected System.Windows.Forms.TextBox txtObservaciones;
        protected System.Windows.Forms.DateTimePicker dtNacimiento;
        protected System.Windows.Forms.Label lblFecha;
        protected System.Windows.Forms.GroupBox gbPropietario;
        protected System.Windows.Forms.Label lblLastMod;
        protected System.Windows.Forms.Button btGuardar;
        protected System.Windows.Forms.Button btCancelar;
        protected System.Windows.Forms.ComboBox cmbIva;
        protected System.Windows.Forms.PictureBox pictureBox1;
        protected System.Windows.Forms.Button btPropietario;
        protected System.Windows.Forms.Label lblPropietario;
        protected System.Windows.Forms.Label lblTipoIva;
    }
}
