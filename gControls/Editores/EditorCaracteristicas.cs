﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Editores
{
    public partial class EditorCaracteristicas : UserControl
    {
        public EditorCaracteristicas()
        {
            InitializeComponent();
        }
        public void SetObjeto(gManager.gcObjeto o)
        {
            panel.Controls.Clear();
            if (o == null) return;
            label1.Text = o.Nombre;
            if (o.Tipo == gManager.gcObjeto.ObjetoTIpo.Categoria)
            {
                for (int i = 0; i < 10; i++)
                {
                    //if(String.IsNullOrEmpty(o.getCaracteristica(i)))continue;

                    var j = new Visores.VisorCaracteristica();
                    j.CaraIndex = i;
                    j.Descripcion = "Debe asignar una caracteristica";
                    j.setText("Caracteristica " + i, o.getCaracteristica(i));
                    j.Asignado += (sender, e) => { 
                        o.setCaracteristica((int)sender, j.Texto); };
                    panel.Controls.Add(j);

                }
            }
            else
            {
                if (o.CategoriaContenedora == null)
                {
                    gLogger.logger.Singlenton.addWarningMsg(o.Nombre + " no está asignado a ninguna categoría. No puede tener características.");
                    return;
                }
                for (int i = 0; i < 10; i++)
                {
                    string cara=o.CategoriaContenedora.getCaracteristica(i);
                    if(String.IsNullOrEmpty(cara))continue;

                    var j = new Visores.VisorCaracteristica();
                    j.Descripcion = "Debe asignar una caracteristica";
                    j.setText(cara, o.getCaracteristica(i));

                    panel.Controls.Add(j);

                }
            }
        }
    }
}
