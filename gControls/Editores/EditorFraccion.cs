﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Editores
{
    public partial class EditorFraccion : UserControl
    {
        private gcObjetoMovido _om;
        private bool _sinIva = false;
        private decimal _iva = 1.21m;
        public EditorFraccion()
        {
            InitializeComponent();
           

        }

        public gcObjetoMovido ObjetoMovido
        {
            get {
                decimal coti = _om.Cotizacion > 1 ? _om.Cotizacion : 1;
                if (EsFraccion)
                {
                    decimal precioFrac = SinIVA ? nPrecioFrac.Value * _iva / coti : nPrecioFrac.Value / coti;
                    _om.Fraccionar((int)nCantFrac.Value,(int) nFracFrac.Value, precioFrac);

                }
                else if(EsTotal)
                {
                    decimal preciototal = SinIVA ? nTotalTotal.Value * _iva / coti : nTotalTotal.Value / coti;
                    _om.incrementar( nCantTotal.Value -_om.Cantidad);
                    _om.Monto = preciototal / nCantTotal.Value;
                }
                else if (EsUnitario)
                {
                    decimal preciounitario = SinIVA ? nPrecioUnitario.Value * _iva / coti : nPrecioUnitario.Value / coti;
                    _om.incrementar(nCantUnitario.Value-_om.Cantidad );
                    _om.Monto = preciounitario;
                }
                return _om; }
            set
            {
                _om = value;
                if (_om != null)
                {
                    chkConIva.Visible = _om.MovimientoObj.EsResponsableInscripto;
                    SinIVA = _om.MovimientoObj != null && (_om.MovimientoObj.EsResponsableInscripto && !chkConIva.Checked);
                    _iva = 1 + (_om.Objeto.Iva / 100);
                    _om.Fraccionado = true;
                    nCantFrac.Value = _om.CantidadFracciones;
                    nFracFrac.Value = _om.Fracciones;
                    nPrecioFrac.Value = _om.PrecioFraccion;
                    nCantTotal.Value = _om.Cantidad;
                    nTotalTotal.Value = SinIVA ? _om.TotalSIVA : _om.Total;
                    nCantUnitario.Value = _om.Cantidad;
                    nPrecioUnitario.Value = SinIVA ? _om.MontoSIVA : _om.Monto;
                }
            }
        }
        public bool SinIVA { get { return _sinIva; }
            set
            {
                lblivafraccion.Visible = value;
                lblivaUnitario.Visible = value;
                lblTotalIva.Visible = value;
                _sinIva = value;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

            panelFraccion.Enabled = rbFraccion.Checked;
            panelTotal.Enabled = rbTotal.Checked;
            panelUnitario.Enabled = rbPrecioyCantidad.Checked;
        }
        public bool EsFraccion
        {
            get { return rbFraccion.Checked; }
        }
        public bool EsUnitario
        {
            get { return rbPrecioyCantidad.Checked; }
        }
        public bool EsTotal
        {
            get { return rbTotal.Checked; }
        }
        

        private void nTotal_ValueChanged(object sender, EventArgs e)
        {
            lblPrecioUnitario.Text = (nTotalTotal.Value / nCantTotal.Value).ToString("$0.000");
        }

        private void nPre_Enter(object sender, EventArgs e)
        {
            if (sender is NumericUpDown)
                ((NumericUpDown)sender).Select(0, 99);
        }

        private void nCantUnitario_ValueChanged(object sender, EventArgs e)
        {
            lblPrecioTotal.Text = (nCantUnitario.Value * nPrecioUnitario.Value).ToString("$0.000");
        }
        private void numericUpDown1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        private void chkConIva_CheckedChanged(object sender, EventArgs e)
        {
            SinIVA = !chkConIva.Checked;
        }
    }
}
