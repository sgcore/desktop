﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gLogger;
using gControls;

namespace gManager.Editores
{
    public partial class EditorPrecio : UserControl
    {
       
        private decimal _costo;
        private decimal _ganancia;
        private decimal _iva;
        private decimal _dolar;
        public EditorPrecio()
        {
            InitializeComponent();
        }

        public void setProducto(gcObjeto p)
        {
            _costo = p.Costo;
            _ganancia = p.Ganancia;
            _iva = p.Iva;
            _dolar = p.Dolar;
            updatePrecio();
        }
        public decimal Costo
        {
            get
            {
                return _costo;
            }
            set
            {
                _costo = value;
                updatePrecio();
            }
        }

        public decimal Ganancia
        {
            get
            {
                return _ganancia;
               
            }
            set
            {
                _ganancia = value;
                updatePrecio();
            }
        }
        public decimal IVA
        {
            get
            {
                return _iva;
            }
            set
            {
                _iva= value;
                updatePrecio();
            }
        }
        public decimal Dolar
        {
            get
            {
                return _dolar;

            }
            set
            {
                if (value != _dolar && value > 0)
                {
                    _dolar = value;
                    btDolar.Text = Dolar.ToString("U$S 0.00");
                }
            }
        }
        public decimal Precio
        {
            get { return (decimal)(Costo * (1+((decimal)Ganancia / 100)));}
        }
        public bool VerBotonIva
        {
            get
            {
                return btIva.Visible;
            }
            set
            {
                btIva.Visible = value;
                lbliva.Visible = value;
            }
        }
        public bool VerBotonDolar
        {
            get
            {
                return btDolar.Visible;
            }
            set
            {
                btDolar.Visible = value;
                lbldolar.Visible = value;
            }
        }
        private void updatePrecio()
        {
           
            btPrecio.Text = Precio.ToString("$0.00");

            btCosto.Text = Costo.ToString("$0.00");
            btGanacia.Text = "%" +Ganancia.ToString("0.000");
            btIva.Text = "%" + IVA.ToString("0.000");
            btDolar.Text = "U$S " + Dolar.ToString("0.00");
        }


        private void btCosto_Click(object sender, EventArgs e)
        {
            decimal a = Editar.EditarDecimal(Costo, "Editar Costo");
            if (Costo!=a && a>0)
            {
                Costo = a;
                updatePrecio();
            }

            
        }

        private void btGanacia_Click(object sender, EventArgs e)
        {

            decimal a = Editar.EditarDecimal(Ganancia, "Editar Costo");
            if (Ganancia != a && a > 0)
            {
                Ganancia = a;
                updatePrecio();
            }
        }

        private void btPrecio_Click(object sender, EventArgs e)
        {
            if (Costo == 0)
            {
                logger.Singlenton.addWarningMsg("No se puede modificar el precio si el costo es 0.");
                return;
            }
            decimal nprecio = Editar.EditarDecimal(Precio,"Editar Precio",3);

            if (Precio!=nprecio)
            {

                updatePrecio();
            }
            if (Costo > nprecio)
            {
                logger.Singlenton.addWarningMsg("No se puede modificar el precio si el costo es mayor.");
                return;
            }

            Ganancia =( nprecio / Costo *100)-100;
            updatePrecio();
        }

        private void btIva_Click(object sender, EventArgs e)
        {
            decimal a = Editar.EditarDecimal(IVA, "Editar IVA");
            if (IVA != a )
            {
                IVA = a;
                updatePrecio();
            }
        }

        private void btDolar_Click(object sender, EventArgs e)
        {
            decimal a = Editar.EditarDecimal(Dolar, "Editar Dolar");
            if (Dolar != a && a > 0)
            {
                Dolar = a;
                btDolar.Text = Dolar.ToString("U$S 0.00");
            }
        }

      
    }
}
