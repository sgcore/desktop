﻿namespace gControls.Editores
{
    partial class EditorMovimiento
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorMovimiento));
            this.colCodigo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colMontoCotizado = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colMontoSIVA = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colMontoSIVACot = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colTotalCotizado = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colTotalSIVA = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colTotalSIVACot = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colCotizacion = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colIVA = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ColumImageList = new System.Windows.Forms.ImageList(this.components);
            this.PagosImageList = new System.Windows.Forms.ImageList(this.components);
            this.btCerrar = new gControls.BotonAccion();
            this.lwOM = new BrightIdeasSoftware.ObjectListView();
            this.colCantidad = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colNombre = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colMonto = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colTotal = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.panelAgregarOM1 = new gControls.Paneles.PanelAgregarOM();
            this.lblDescPago = new System.Windows.Forms.Label();
            this.visorTotalesMovimiento1 = new gControls.Visores.VisorTotalesMovimiento();
            this.visorDestinatario1 = new gControls.Visores.VisorDestinatario();
            this.visorMovimiento1 = new gControls.Visores.VisorMovimiento();
            ((System.ComponentModel.ISupportInitialize)(this.lwOM)).BeginInit();
            this.SuspendLayout();
            // 
            // colCodigo
            // 
            this.colCodigo.AspectName = "CodigoProducto";
            this.colCodigo.DisplayIndex = 1;
            this.colCodigo.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colCodigo.ImageAspectName = "Codigo";
            this.colCodigo.IsVisible = false;
            this.colCodigo.MaximumWidth = 70;
            this.colCodigo.MinimumWidth = 120;
            this.colCodigo.Text = "Código";
            this.colCodigo.ToolTipText = "Codigo del Producto.";
            this.colCodigo.Width = 70;
            // 
            // colMontoCotizado
            // 
            this.colMontoCotizado.AspectName = "MontoCotizado";
            this.colMontoCotizado.AspectToStringFormat = "{0:$0.000}";
            this.colMontoCotizado.DisplayIndex = 3;
            this.colMontoCotizado.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colMontoCotizado.IsVisible = false;
            this.colMontoCotizado.MaximumWidth = 100;
            this.colMontoCotizado.MinimumWidth = 70;
            this.colMontoCotizado.Text = "Precio U.";
            this.colMontoCotizado.ToolTipText = "Precio con la cotización de la moneda extranjera aplicada.";
            this.colMontoCotizado.Width = 90;
            // 
            // colMontoSIVA
            // 
            this.colMontoSIVA.AspectName = "MontoSIVA";
            this.colMontoSIVA.AspectToStringFormat = "{0:$0.000}";
            this.colMontoSIVA.DisplayIndex = 4;
            this.colMontoSIVA.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colMontoSIVA.IsVisible = false;
            this.colMontoSIVA.MaximumWidth = 100;
            this.colMontoSIVA.MinimumWidth = 70;
            this.colMontoSIVA.Text = "Precio U.";
            this.colMontoSIVA.ToolTipText = "Precio del producto sin aplicar el IVA.";
            this.colMontoSIVA.Width = 90;
            // 
            // colMontoSIVACot
            // 
            this.colMontoSIVACot.AspectName = "MontoSIVACot";
            this.colMontoSIVACot.AspectToStringFormat = "{0:$0.000}";
            this.colMontoSIVACot.DisplayIndex = 5;
            this.colMontoSIVACot.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colMontoSIVACot.IsVisible = false;
            this.colMontoSIVACot.MaximumWidth = 100;
            this.colMontoSIVACot.MinimumWidth = 70;
            this.colMontoSIVACot.Text = "Precio U.";
            this.colMontoSIVACot.ToolTipText = "Precio del Producto cotizado a la moneda extranjera sin aplicar el IVA.";
            this.colMontoSIVACot.Width = 90;
            // 
            // colTotalCotizado
            // 
            this.colTotalCotizado.AspectName = "TotalCotizado";
            this.colTotalCotizado.AspectToStringFormat = "{0:$0.000}";
            this.colTotalCotizado.DisplayIndex = 6;
            this.colTotalCotizado.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colTotalCotizado.IsVisible = false;
            this.colTotalCotizado.MaximumWidth = 100;
            this.colTotalCotizado.MinimumWidth = 70;
            this.colTotalCotizado.Text = "Total";
            this.colTotalCotizado.ToolTipText = "Precio total con la cotización de moneda extranjera aplicado.";
            this.colTotalCotizado.Width = 90;
            // 
            // colTotalSIVA
            // 
            this.colTotalSIVA.AspectName = "TotalSIVA";
            this.colTotalSIVA.AspectToStringFormat = "{0:$0.000}";
            this.colTotalSIVA.DisplayIndex = 7;
            this.colTotalSIVA.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colTotalSIVA.IsVisible = false;
            this.colTotalSIVA.MaximumWidth = 100;
            this.colTotalSIVA.MinimumWidth = 70;
            this.colTotalSIVA.Text = "Total";
            this.colTotalSIVA.ToolTipText = "Precio Total sin la aplicación del IVA.";
            this.colTotalSIVA.Width = 90;
            // 
            // colTotalSIVACot
            // 
            this.colTotalSIVACot.AspectName = "TotalSIVACot";
            this.colTotalSIVACot.AspectToStringFormat = "{0:$0.000}";
            this.colTotalSIVACot.DisplayIndex = 10;
            this.colTotalSIVACot.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colTotalSIVACot.IsVisible = false;
            this.colTotalSIVACot.MaximumWidth = 100;
            this.colTotalSIVACot.MinimumWidth = 70;
            this.colTotalSIVACot.Text = "Total";
            this.colTotalSIVACot.ToolTipText = "Precio Total sin iva cotizado a la moneda extranjera";
            this.colTotalSIVACot.Width = 90;
            // 
            // colCotizacion
            // 
            this.colCotizacion.AspectName = "Cotizacion";
            this.colCotizacion.AspectToStringFormat = "{0:0.000}";
            this.colCotizacion.DisplayIndex = 9;
            this.colCotizacion.FillsFreeSpace = true;
            this.colCotizacion.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colCotizacion.IsVisible = false;
            this.colCotizacion.MaximumWidth = 100;
            this.colCotizacion.MinimumWidth = 70;
            this.colCotizacion.Text = "Cotización";
            this.colCotizacion.ToolTipText = "Cotización de la moneda extranjera.";
            this.colCotizacion.Width = 90;
            // 
            // colIVA
            // 
            this.colIVA.AspectName = "IVAProducto";
            this.colIVA.DisplayIndex = 11;
            this.colIVA.IsVisible = false;
            this.colIVA.MaximumWidth = 100;
            this.colIVA.MinimumWidth = 70;
            this.colIVA.Text = "%IVA";
            this.colIVA.Width = 70;
            // 
            // ColumImageList
            // 
            this.ColumImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ColumImageList.ImageStream")));
            this.ColumImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ColumImageList.Images.SetKeyName(0, "Cantidad");
            this.ColumImageList.Images.SetKeyName(1, "Iva");
            this.ColumImageList.Images.SetKeyName(2, "Cotizacion");
            this.ColumImageList.Images.SetKeyName(3, "Precio");
            this.ColumImageList.Images.SetKeyName(4, "Producto");
            this.ColumImageList.Images.SetKeyName(5, "Total");
            // 
            // PagosImageList
            // 
            this.PagosImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("PagosImageList.ImageStream")));
            this.PagosImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.PagosImageList.Images.SetKeyName(0, "Efectivo");
            this.PagosImageList.Images.SetKeyName(1, "Vuelto");
            this.PagosImageList.Images.SetKeyName(2, "Tarjeta");
            this.PagosImageList.Images.SetKeyName(3, "Cheque");
            this.PagosImageList.Images.SetKeyName(4, "Cuenta");
            // 
            // btCerrar
            // 
            this.btCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCerrar.FormContainer = null;
            this.btCerrar.GlobalHotKey = false;
            this.btCerrar.HacerInvisibleAlDeshabilitar = false;
            this.btCerrar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCerrar.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btCerrar.Location = new System.Drawing.Point(746, 0);
            this.btCerrar.Name = "btCerrar";
            this.btCerrar.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btCerrar.RegistradoEnForm = false;
            this.btCerrar.RequiereKey = false;
            this.btCerrar.Size = new System.Drawing.Size(21, 20);
            this.btCerrar.TabIndex = 13;
            this.btCerrar.Titulo = "Cerrar Formulario";
            this.btCerrar.ToolTipDescripcion = "Cierra el formulario del movimiento";
            this.btCerrar.UseVisualStyleBackColor = true;
            this.btCerrar.Click += new System.EventHandler(this.btCerrar_Click);
            // 
            // lwOM
            // 
            this.lwOM.AllColumns.Add(this.colCantidad);
            this.lwOM.AllColumns.Add(this.colCodigo);
            this.lwOM.AllColumns.Add(this.colNombre);
            this.lwOM.AllColumns.Add(this.colMonto);
            this.lwOM.AllColumns.Add(this.colMontoCotizado);
            this.lwOM.AllColumns.Add(this.colMontoSIVA);
            this.lwOM.AllColumns.Add(this.colMontoSIVACot);
            this.lwOM.AllColumns.Add(this.colTotalCotizado);
            this.lwOM.AllColumns.Add(this.colTotalSIVA);
            this.lwOM.AllColumns.Add(this.colTotal);
            this.lwOM.AllColumns.Add(this.colTotalSIVACot);
            this.lwOM.AllColumns.Add(this.colCotizacion);
            this.lwOM.AllColumns.Add(this.colIVA);
            this.lwOM.AutoArrange = false;
            this.lwOM.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colCantidad,
            this.colNombre,
            this.colMonto,
            this.colTotal});
            this.lwOM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lwOM.FullRowSelect = true;
            this.lwOM.GridLines = true;
            this.lwOM.HasCollapsibleGroups = false;
            this.lwOM.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lwOM.HeaderUsesThemes = true;
            this.lwOM.HeaderWordWrap = true;
            this.lwOM.LargeImageList = this.ColumImageList;
            this.lwOM.Location = new System.Drawing.Point(0, 157);
            this.lwOM.MultiSelect = false;
            this.lwOM.Name = "lwOM";
            this.lwOM.ShowGroups = false;
            this.lwOM.Size = new System.Drawing.Size(767, 272);
            this.lwOM.SmallImageList = this.ColumImageList;
            this.lwOM.TabIndex = 0;
            this.lwOM.UseCompatibleStateImageBehavior = false;
            this.lwOM.View = System.Windows.Forms.View.Details;
            this.lwOM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lwOM_KeyDown);
            this.lwOM.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lwOM_MouseDoubleClick);
            // 
            // colCantidad
            // 
            this.colCantidad.AspectName = "Cantidad";
            this.colCantidad.AspectToStringFormat = "{0:0.000}";
            this.colCantidad.HeaderImageKey = "Cantidad";
            this.colCantidad.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colCantidad.IsEditable = false;
            this.colCantidad.MaximumWidth = 100;
            this.colCantidad.MinimumWidth = 90;
            this.colCantidad.Text = "Cantidad";
            this.colCantidad.ToolTipText = "Cantidad de unidades o fracciones del producto.";
            this.colCantidad.Width = 100;
            this.colCantidad.WordWrap = true;
            // 
            // colNombre
            // 
            this.colNombre.AspectName = "NombreProducto";
            this.colNombre.FillsFreeSpace = true;
            this.colNombre.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colNombre.IsEditable = false;
            this.colNombre.MinimumWidth = 150;
            this.colNombre.Text = "Nombre del Producto";
            this.colNombre.ToolTipText = "Nombre del producto";
            this.colNombre.UseFiltering = false;
            this.colNombre.Width = 150;
            // 
            // colMonto
            // 
            this.colMonto.AspectName = "Monto";
            this.colMonto.AspectToStringFormat = "{0:$0.000}";
            this.colMonto.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colMonto.MaximumWidth = 100;
            this.colMonto.MinimumWidth = 70;
            this.colMonto.Text = "Precio U.";
            this.colMonto.ToolTipText = "Precio (IVA incluido) del Producto";
            this.colMonto.Width = 90;
            // 
            // colTotal
            // 
            this.colTotal.AspectName = "Total";
            this.colTotal.AspectToStringFormat = "{0:$0.000}";
            this.colTotal.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colTotal.MaximumWidth = 100;
            this.colTotal.MinimumWidth = 70;
            this.colTotal.Text = "Total";
            this.colTotal.ToolTipText = "Precio total iva incluido sin la cotización de la moneda extranjera.";
            this.colTotal.Width = 90;
            // 
            // panelAgregarOM1
            // 
            this.panelAgregarOM1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAgregarOM1.Location = new System.Drawing.Point(0, 134);
            this.panelAgregarOM1.Name = "panelAgregarOM1";
            this.panelAgregarOM1.Size = new System.Drawing.Size(767, 23);
            this.panelAgregarOM1.TabIndex = 12;
            // 
            // lblDescPago
            // 
            this.lblDescPago.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescPago.Font = new System.Drawing.Font("Segoe Marker", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescPago.Location = new System.Drawing.Point(0, 157);
            this.lblDescPago.Name = "lblDescPago";
            this.lblDescPago.Size = new System.Drawing.Size(767, 272);
            this.lblDescPago.TabIndex = 11;
            this.lblDescPago.Text = "Descripcion del pago";
            this.lblDescPago.Visible = false;
            // 
            // visorTotalesMovimiento1
            // 
            this.visorTotalesMovimiento1.AutoSize = true;
            this.visorTotalesMovimiento1.BackColor = System.Drawing.Color.LightGray;
            this.visorTotalesMovimiento1.CotizacionVisible = true;
            this.visorTotalesMovimiento1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.visorTotalesMovimiento1.IvaVisible = true;
            this.visorTotalesMovimiento1.Location = new System.Drawing.Point(0, 429);
            this.visorTotalesMovimiento1.Name = "visorTotalesMovimiento1";
            this.visorTotalesMovimiento1.PagosVisible = true;
            this.visorTotalesMovimiento1.SaldoVisible = true;
            this.visorTotalesMovimiento1.Size = new System.Drawing.Size(767, 87);
            this.visorTotalesMovimiento1.TabIndex = 6;
            // 
            // visorDestinatario1
            // 
            this.visorDestinatario1.BackColor = System.Drawing.Color.Transparent;
            this.visorDestinatario1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.visorDestinatario1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.visorDestinatario1.Destinatario = null;
            this.visorDestinatario1.Dock = System.Windows.Forms.DockStyle.Top;
            this.visorDestinatario1.Location = new System.Drawing.Point(0, 48);
            this.visorDestinatario1.Name = "visorDestinatario1";
            this.visorDestinatario1.Size = new System.Drawing.Size(767, 86);
            this.visorDestinatario1.TabIndex = 1;
            this.visorDestinatario1.VerBotonSeguimiento = false;
            // 
            // visorMovimiento1
            // 
            this.visorMovimiento1.BackColor = System.Drawing.Color.Transparent;
            this.visorMovimiento1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("visorMovimiento1.BackgroundImage")));
            this.visorMovimiento1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.visorMovimiento1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.visorMovimiento1.Dock = System.Windows.Forms.DockStyle.Top;
            this.visorMovimiento1.Location = new System.Drawing.Point(0, 0);
            this.visorMovimiento1.Name = "visorMovimiento1";
            this.visorMovimiento1.Size = new System.Drawing.Size(767, 48);
            this.visorMovimiento1.TabIndex = 7;
            // 
            // EditorMovimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btCerrar);
            this.Controls.Add(this.lwOM);
            this.Controls.Add(this.lblDescPago);
            this.Controls.Add(this.visorTotalesMovimiento1);
            this.Controls.Add(this.panelAgregarOM1);
            this.Controls.Add(this.visorDestinatario1);
            this.Controls.Add(this.visorMovimiento1);
            this.Name = "EditorMovimiento";
            this.Size = new System.Drawing.Size(767, 516);
            ((System.ComponentModel.ISupportInitialize)(this.lwOM)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView lwOM;
        private BrightIdeasSoftware.OLVColumn colCantidad;
        private BrightIdeasSoftware.OLVColumn colCodigo;
        private BrightIdeasSoftware.OLVColumn colNombre;
        private BrightIdeasSoftware.OLVColumn colMonto;
        private BrightIdeasSoftware.OLVColumn colTotal;
        private Visores.VisorDestinatario visorDestinatario1;
        private System.Windows.Forms.ImageList PagosImageList;
        private BrightIdeasSoftware.OLVColumn colMontoCotizado;
        private BrightIdeasSoftware.OLVColumn colTotalCotizado;
        private BrightIdeasSoftware.OLVColumn colCotizacion;
        private BrightIdeasSoftware.OLVColumn colMontoSIVA;
        private BrightIdeasSoftware.OLVColumn colTotalSIVA;
        private BrightIdeasSoftware.OLVColumn colMontoSIVACot;
        private BrightIdeasSoftware.OLVColumn colTotalSIVACot;
        private System.Windows.Forms.ImageList ColumImageList;
        private BrightIdeasSoftware.OLVColumn colIVA;
        private Visores.VisorTotalesMovimiento visorTotalesMovimiento1;
        private Visores.VisorMovimiento visorMovimiento1;
        private System.Windows.Forms.Label lblDescPago;
        private Paneles.PanelAgregarOM panelAgregarOM1;
        private BotonAccion btCerrar;
    }
}
