﻿using gManager.Editores;
namespace gControls.Editores
{
    partial class EditorObjeto
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLink = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtSerial = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.gbParent = new System.Windows.Forms.GroupBox();
            this.btEliminarGrupo = new System.Windows.Forms.Button();
            this.btGrupo = new System.Windows.Forms.Button();
            this.btCancelarCat = new System.Windows.Forms.Button();
            this.btCategoria = new System.Windows.Forms.Button();
            this.btClonar = new System.Windows.Forms.Button();
            this.cntrlEditorPrecio1 = new gManager.Editores.EditorPrecio();
            this.btCancelar = new System.Windows.Forms.Button();
            this.btGuardar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btCaracteristicas = new System.Windows.Forms.Button();
            this.cmbMetrica = new System.Windows.Forms.ComboBox();
            this.lblmetrica = new System.Windows.Forms.Label();
            this.btIdeal = new System.Windows.Forms.Button();
            this.lblideal = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.cmbEstado = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gbParent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.ForeColor = System.Drawing.Color.SteelBlue;
            this.txtNombre.Location = new System.Drawing.Point(136, 17);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(284, 22);
            this.txtNombre.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre:";
            // 
            // txtLink
            // 
            this.txtLink.Location = new System.Drawing.Point(110, 59);
            this.txtLink.Name = "txtLink";
            this.txtLink.Size = new System.Drawing.Size(195, 20);
            this.txtLink.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(133, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Descripción:";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(136, 58);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(284, 20);
            this.txtDescripcion.TabIndex = 1;
            this.txtDescripcion.Text = "descripcion del producto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(137, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Codigo:";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(140, 94);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(135, 20);
            this.txtCodigo.TabIndex = 2;
            // 
            // txtSerial
            // 
            this.txtSerial.Location = new System.Drawing.Point(288, 94);
            this.txtSerial.Name = "txtSerial";
            this.txtSerial.Size = new System.Drawing.Size(135, 20);
            this.txtSerial.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(285, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Serial:";
            // 
            // gbParent
            // 
            this.gbParent.BackColor = System.Drawing.Color.Transparent;
            this.gbParent.Controls.Add(this.label2);
            this.gbParent.Controls.Add(this.txtLink);
            this.gbParent.Controls.Add(this.btIdeal);
            this.gbParent.Controls.Add(this.lblideal);
            this.gbParent.Controls.Add(this.lblmetrica);
            this.gbParent.Controls.Add(this.cmbEstado);
            this.gbParent.Controls.Add(this.cmbMetrica);
            this.gbParent.Controls.Add(this.lblEstado);
            this.gbParent.Location = new System.Drawing.Point(3, 188);
            this.gbParent.Name = "gbParent";
            this.gbParent.Size = new System.Drawing.Size(389, 87);
            this.gbParent.TabIndex = 3;
            this.gbParent.TabStop = false;
            this.gbParent.Text = "Datos Adicionales";
            // 
            // btEliminarGrupo
            // 
            this.btEliminarGrupo.AutoSize = true;
            this.btEliminarGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEliminarGrupo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btEliminarGrupo.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btEliminarGrupo.Location = new System.Drawing.Point(398, 154);
            this.btEliminarGrupo.Name = "btEliminarGrupo";
            this.btEliminarGrupo.Size = new System.Drawing.Size(25, 28);
            this.btEliminarGrupo.TabIndex = 6;
            this.btEliminarGrupo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btEliminarGrupo.UseVisualStyleBackColor = true;
            this.btEliminarGrupo.Click += new System.EventHandler(this.btEliminarGrupo_Click);
            // 
            // btGrupo
            // 
            this.btGrupo.AutoSize = true;
            this.btGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGrupo.ForeColor = System.Drawing.Color.Teal;
            this.btGrupo.Location = new System.Drawing.Point(3, 154);
            this.btGrupo.Name = "btGrupo";
            this.btGrupo.Size = new System.Drawing.Size(389, 28);
            this.btGrupo.TabIndex = 5;
            this.btGrupo.Text = "Grupo";
            this.btGrupo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btGrupo.UseVisualStyleBackColor = true;
            this.btGrupo.Click += new System.EventHandler(this.btGrupo_Click);
            // 
            // btCancelarCat
            // 
            this.btCancelarCat.AutoSize = true;
            this.btCancelarCat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCancelarCat.ForeColor = System.Drawing.Color.DarkRed;
            this.btCancelarCat.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btCancelarCat.Location = new System.Drawing.Point(398, 120);
            this.btCancelarCat.Name = "btCancelarCat";
            this.btCancelarCat.Size = new System.Drawing.Size(25, 28);
            this.btCancelarCat.TabIndex = 4;
            this.btCancelarCat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCancelarCat.UseVisualStyleBackColor = true;
            this.btCancelarCat.Click += new System.EventHandler(this.btCancelarCat_Click);
            // 
            // btCategoria
            // 
            this.btCategoria.AutoSize = true;
            this.btCategoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCategoria.ForeColor = System.Drawing.Color.Red;
            this.btCategoria.Location = new System.Drawing.Point(3, 120);
            this.btCategoria.Name = "btCategoria";
            this.btCategoria.Size = new System.Drawing.Size(389, 28);
            this.btCategoria.TabIndex = 3;
            this.btCategoria.Text = "Categoria";
            this.btCategoria.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCategoria.UseVisualStyleBackColor = true;
            this.btCategoria.Click += new System.EventHandler(this.btCategoria_Click);
            // 
            // btClonar
            // 
            this.btClonar.AutoSize = true;
            this.btClonar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClonar.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btClonar.Image = global::gControls.Properties.Resources.Clonar16;
            this.btClonar.Location = new System.Drawing.Point(430, 216);
            this.btClonar.Name = "btClonar";
            this.btClonar.Size = new System.Drawing.Size(171, 30);
            this.btClonar.TabIndex = 4;
            this.btClonar.Text = "Guardar y Clonar";
            this.btClonar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btClonar.UseVisualStyleBackColor = true;
            this.btClonar.Click += new System.EventHandler(this.btClonar_Click);
            // 
            // cntrlEditorPrecio1
            // 
            this.cntrlEditorPrecio1.AutoSize = true;
            this.cntrlEditorPrecio1.BackColor = System.Drawing.Color.Transparent;
            this.cntrlEditorPrecio1.Costo = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrlEditorPrecio1.Dolar = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrlEditorPrecio1.Ganancia = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrlEditorPrecio1.IVA = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.cntrlEditorPrecio1.Location = new System.Drawing.Point(430, 5);
            this.cntrlEditorPrecio1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cntrlEditorPrecio1.Name = "cntrlEditorPrecio1";
            this.cntrlEditorPrecio1.Size = new System.Drawing.Size(171, 167);
            this.cntrlEditorPrecio1.TabIndex = 5;
            this.cntrlEditorPrecio1.VerBotonDolar = true;
            this.cntrlEditorPrecio1.VerBotonIva = true;
            // 
            // btCancelar
            // 
            this.btCancelar.AutoSize = true;
            this.btCancelar.Image = global::gControls.Properties.Resources.Unlike;
            this.btCancelar.Location = new System.Drawing.Point(526, 252);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(75, 23);
            this.btCancelar.TabIndex = 38;
            this.btCancelar.Text = "Cancelar";
            this.btCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCancelar.UseVisualStyleBackColor = true;
            this.btCancelar.Click += new System.EventHandler(this.btCancelar_Click);
            // 
            // btGuardar
            // 
            this.btGuardar.AutoSize = true;
            this.btGuardar.Image = global::gControls.Properties.Resources.Like;
            this.btGuardar.Location = new System.Drawing.Point(430, 252);
            this.btGuardar.Name = "btGuardar";
            this.btGuardar.Size = new System.Drawing.Size(78, 23);
            this.btGuardar.TabIndex = 37;
            this.btGuardar.Text = "Guardar";
            this.btGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btGuardar.UseVisualStyleBackColor = true;
            this.btGuardar.Click += new System.EventHandler(this.btGuardar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(127, 111);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // btCaracteristicas
            // 
            this.btCaracteristicas.AutoSize = true;
            this.btCaracteristicas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCaracteristicas.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btCaracteristicas.Image = global::gControls.Properties.Resources.Ver;
            this.btCaracteristicas.Location = new System.Drawing.Point(445, 180);
            this.btCaracteristicas.Name = "btCaracteristicas";
            this.btCaracteristicas.Size = new System.Drawing.Size(156, 30);
            this.btCaracteristicas.TabIndex = 40;
            this.btCaracteristicas.Text = "Características";
            this.btCaracteristicas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCaracteristicas.UseVisualStyleBackColor = true;
            this.btCaracteristicas.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbMetrica
            // 
            this.cmbMetrica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMetrica.FormattingEnabled = true;
            this.cmbMetrica.Location = new System.Drawing.Point(137, 32);
            this.cmbMetrica.Name = "cmbMetrica";
            this.cmbMetrica.Size = new System.Drawing.Size(82, 21);
            this.cmbMetrica.TabIndex = 41;
            // 
            // lblmetrica
            // 
            this.lblmetrica.AutoSize = true;
            this.lblmetrica.Location = new System.Drawing.Point(134, 16);
            this.lblmetrica.Name = "lblmetrica";
            this.lblmetrica.Size = new System.Drawing.Size(96, 13);
            this.lblmetrica.TabIndex = 14;
            this.lblmetrica.Text = "Unidad de medida:";
            // 
            // btIdeal
            // 
            this.btIdeal.AutoSize = true;
            this.btIdeal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btIdeal.ForeColor = System.Drawing.Color.DarkCyan;
            this.btIdeal.Location = new System.Drawing.Point(27, 33);
            this.btIdeal.Name = "btIdeal";
            this.btIdeal.Size = new System.Drawing.Size(89, 23);
            this.btIdeal.TabIndex = 43;
            this.btIdeal.Text = "0,00";
            this.btIdeal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btIdeal.UseVisualStyleBackColor = true;
            this.btIdeal.Click += new System.EventHandler(this.btIdeal_Click);
            // 
            // lblideal
            // 
            this.lblideal.AutoSize = true;
            this.lblideal.Location = new System.Drawing.Point(24, 16);
            this.lblideal.Name = "lblideal";
            this.lblideal.Size = new System.Drawing.Size(63, 13);
            this.lblideal.TabIndex = 42;
            this.lblideal.Text = "Stock ideal:";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.lblEstado.Location = new System.Drawing.Point(239, 9);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(66, 20);
            this.lblEstado.TabIndex = 44;
            this.lblEstado.Text = "Estado";
            // 
            // cmbEstado
            // 
            this.cmbEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEstado.FormattingEnabled = true;
            this.cmbEstado.Location = new System.Drawing.Point(243, 32);
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.Size = new System.Drawing.Size(75, 21);
            this.cmbEstado.TabIndex = 45;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "Extra (Link)";
            // 
            // EditorObjeto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.btGrupo);
            this.Controls.Add(this.btEliminarGrupo);
            this.Controls.Add(this.txtSerial);
            this.Controls.Add(this.btCancelarCat);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btCategoria);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btCaracteristicas);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btCancelar);
            this.Controls.Add(this.btGuardar);
            this.Controls.Add(this.btClonar);
            this.Controls.Add(this.cntrlEditorPrecio1);
            this.Controls.Add(this.gbParent);
            this.Name = "EditorObjeto";
            this.Size = new System.Drawing.Size(605, 278);
            this.gbParent.ResumeLayout(false);
            this.gbParent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLink;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.GroupBox gbParent;
        private System.Windows.Forms.Button btCategoria;
        private System.Windows.Forms.Button btCancelarCat;
        private System.Windows.Forms.Button btClonar;
        private System.Windows.Forms.TextBox txtSerial;
        private System.Windows.Forms.Label label6;
        private EditorPrecio cntrlEditorPrecio1;
        private System.Windows.Forms.Button btCancelar;
        private System.Windows.Forms.Button btGuardar;
        private System.Windows.Forms.Button btEliminarGrupo;
        private System.Windows.Forms.Button btGrupo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btCaracteristicas;
        private System.Windows.Forms.ComboBox cmbMetrica;
        private System.Windows.Forms.Label lblmetrica;
        private System.Windows.Forms.Button btIdeal;
        private System.Windows.Forms.Label lblideal;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.ComboBox cmbEstado;
        private System.Windows.Forms.Label label2;
    }
}
