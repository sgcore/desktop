﻿namespace gManager.Editores
{
    partial class EditorPrecio
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btPrecio = new System.Windows.Forms.Button();
            this.btGanacia = new System.Windows.Forms.Button();
            this.lbliva = new System.Windows.Forms.Label();
            this.btCosto = new System.Windows.Forms.Button();
            this.btIva = new System.Windows.Forms.Button();
            this.btDolar = new System.Windows.Forms.Button();
            this.lbldolar = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Precio:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Ganancia:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Costo:";
            // 
            // btPrecio
            // 
            this.btPrecio.AutoSize = true;
            this.btPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPrecio.ForeColor = System.Drawing.Color.DarkCyan;
            this.btPrecio.Location = new System.Drawing.Point(55, 73);
            this.btPrecio.Name = "btPrecio";
            this.btPrecio.Size = new System.Drawing.Size(106, 28);
            this.btPrecio.TabIndex = 11;
            this.btPrecio.Text = "0,00";
            this.btPrecio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPrecio.UseVisualStyleBackColor = true;
            this.btPrecio.Click += new System.EventHandler(this.btPrecio_Click);
            // 
            // btGanacia
            // 
            this.btGanacia.AutoSize = true;
            this.btGanacia.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGanacia.ForeColor = System.Drawing.Color.DarkCyan;
            this.btGanacia.Location = new System.Drawing.Point(55, 39);
            this.btGanacia.Name = "btGanacia";
            this.btGanacia.Size = new System.Drawing.Size(106, 28);
            this.btGanacia.TabIndex = 9;
            this.btGanacia.Text = "0,00";
            this.btGanacia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btGanacia.UseVisualStyleBackColor = true;
            this.btGanacia.Click += new System.EventHandler(this.btGanacia_Click);
            // 
            // lbliva
            // 
            this.lbliva.AutoSize = true;
            this.lbliva.Location = new System.Drawing.Point(22, 112);
            this.lbliva.Name = "lbliva";
            this.lbliva.Size = new System.Drawing.Size(27, 13);
            this.lbliva.TabIndex = 15;
            this.lbliva.Text = "IVA:";
            // 
            // btCosto
            // 
            this.btCosto.AutoSize = true;
            this.btCosto.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCosto.ForeColor = System.Drawing.Color.DarkCyan;
            this.btCosto.Location = new System.Drawing.Point(55, 3);
            this.btCosto.Name = "btCosto";
            this.btCosto.Size = new System.Drawing.Size(106, 28);
            this.btCosto.TabIndex = 8;
            this.btCosto.Text = "0,00";
            this.btCosto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCosto.UseVisualStyleBackColor = true;
            this.btCosto.Click += new System.EventHandler(this.btCosto_Click);
            // 
            // btIva
            // 
            this.btIva.AutoSize = true;
            this.btIva.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btIva.ForeColor = System.Drawing.Color.DarkCyan;
            this.btIva.Location = new System.Drawing.Point(55, 107);
            this.btIva.Name = "btIva";
            this.btIva.Size = new System.Drawing.Size(106, 23);
            this.btIva.TabIndex = 16;
            this.btIva.Text = "0,00";
            this.btIva.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btIva.UseVisualStyleBackColor = true;
            this.btIva.Click += new System.EventHandler(this.btIva_Click);
            // 
            // btDolar
            // 
            this.btDolar.AutoSize = true;
            this.btDolar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDolar.ForeColor = System.Drawing.Color.DarkCyan;
            this.btDolar.Location = new System.Drawing.Point(55, 136);
            this.btDolar.Name = "btDolar";
            this.btDolar.Size = new System.Drawing.Size(106, 23);
            this.btDolar.TabIndex = 18;
            this.btDolar.Text = "0,00";
            this.btDolar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDolar.UseVisualStyleBackColor = true;
            this.btDolar.Click += new System.EventHandler(this.btDolar_Click);
            // 
            // lbldolar
            // 
            this.lbldolar.AutoSize = true;
            this.lbldolar.Location = new System.Drawing.Point(22, 141);
            this.lbldolar.Name = "lbldolar";
            this.lbldolar.Size = new System.Drawing.Size(35, 13);
            this.lbldolar.TabIndex = 17;
            this.lbldolar.Text = "Dolar:";
            // 
            // EditorPrecio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btDolar);
            this.Controls.Add(this.lbldolar);
            this.Controls.Add(this.btIva);
            this.Controls.Add(this.lbliva);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btCosto);
            this.Controls.Add(this.btPrecio);
            this.Controls.Add(this.btGanacia);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Name = "EditorPrecio";
            this.Size = new System.Drawing.Size(174, 164);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btPrecio;
        private System.Windows.Forms.Button btGanacia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbliva;
        private System.Windows.Forms.Button btCosto;
        private System.Windows.Forms.Button btIva;
        private System.Windows.Forms.Button btDolar;
        private System.Windows.Forms.Label lbldolar;
    }
}
