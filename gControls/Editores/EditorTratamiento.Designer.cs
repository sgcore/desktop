﻿namespace gControls.Editores
{
    partial class EditorTratamiento
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTitulo = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.dtProx = new System.Windows.Forms.DateTimePicker();
            this.lblTipo = new System.Windows.Forms.Label();
            this.pnFuturo = new System.Windows.Forms.GroupBox();
            this.lblFuturoDesc = new System.Windows.Forms.Label();
            this.pnPeriodico = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nMinutos = new System.Windows.Forms.NumericUpDown();
            this.nHoras = new System.Windows.Forms.NumericUpDown();
            this.nDias = new System.Windows.Forms.NumericUpDown();
            this.lblPeriodicodesc = new System.Windows.Forms.Label();
            this.pnFuturo.SuspendLayout();
            this.pnPeriodico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMinutos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nHoras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nDias)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTitulo
            // 
            this.txtTitulo.Location = new System.Drawing.Point(16, 30);
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.Size = new System.Drawing.Size(381, 20);
            this.txtTitulo.TabIndex = 0;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(16, 56);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(381, 89);
            this.txtDescripcion.TabIndex = 1;
            // 
            // dtProx
            // 
            this.dtProx.CustomFormat = "dddd, MMMM d, yyy  HH:mm";
            this.dtProx.Dock = System.Windows.Forms.DockStyle.Top;
            this.dtProx.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtProx.Location = new System.Drawing.Point(3, 43);
            this.dtProx.Name = "dtProx";
            this.dtProx.Size = new System.Drawing.Size(416, 20);
            this.dtProx.TabIndex = 2;
            // 
            // lblTipo
            // 
            this.lblTipo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipo.Image = global::gControls.Properties.Resources.Tarea;
            this.lblTipo.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblTipo.Location = new System.Drawing.Point(0, 0);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(422, 27);
            this.lblTipo.TabIndex = 3;
            this.lblTipo.Text = "Aviso";
            this.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnFuturo
            // 
            this.pnFuturo.Controls.Add(this.dtProx);
            this.pnFuturo.Controls.Add(this.lblFuturoDesc);
            this.pnFuturo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnFuturo.Location = new System.Drawing.Point(0, 238);
            this.pnFuturo.Name = "pnFuturo";
            this.pnFuturo.Size = new System.Drawing.Size(422, 83);
            this.pnFuturo.TabIndex = 4;
            this.pnFuturo.TabStop = false;
            this.pnFuturo.Text = "Evento Futuro";
            // 
            // lblFuturoDesc
            // 
            this.lblFuturoDesc.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblFuturoDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuturoDesc.Image = global::gControls.Properties.Resources.Tarea;
            this.lblFuturoDesc.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblFuturoDesc.Location = new System.Drawing.Point(3, 16);
            this.lblFuturoDesc.Name = "lblFuturoDesc";
            this.lblFuturoDesc.Size = new System.Drawing.Size(416, 27);
            this.lblFuturoDesc.TabIndex = 4;
            this.lblFuturoDesc.Text = "El evento se ejecutara dentro de 45 horas, 24 minutos";
            this.lblFuturoDesc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnPeriodico
            // 
            this.pnPeriodico.Controls.Add(this.label3);
            this.pnPeriodico.Controls.Add(this.label2);
            this.pnPeriodico.Controls.Add(this.label1);
            this.pnPeriodico.Controls.Add(this.nMinutos);
            this.pnPeriodico.Controls.Add(this.nHoras);
            this.pnPeriodico.Controls.Add(this.nDias);
            this.pnPeriodico.Controls.Add(this.lblPeriodicodesc);
            this.pnPeriodico.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnPeriodico.Location = new System.Drawing.Point(0, 155);
            this.pnPeriodico.Name = "pnPeriodico";
            this.pnPeriodico.Size = new System.Drawing.Size(422, 83);
            this.pnPeriodico.TabIndex = 5;
            this.pnPeriodico.TabStop = false;
            this.pnPeriodico.Text = "Evento Periódico";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(236, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Minutos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(144, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Horas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Dias";
            // 
            // nMinutos
            // 
            this.nMinutos.Location = new System.Drawing.Point(286, 58);
            this.nMinutos.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nMinutos.Name = "nMinutos";
            this.nMinutos.Size = new System.Drawing.Size(40, 20);
            this.nMinutos.TabIndex = 7;
            this.nMinutos.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // nHoras
            // 
            this.nHoras.Location = new System.Drawing.Point(185, 58);
            this.nHoras.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.nHoras.Name = "nHoras";
            this.nHoras.Size = new System.Drawing.Size(45, 20);
            this.nHoras.TabIndex = 6;
            this.nHoras.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // nDias
            // 
            this.nDias.Location = new System.Drawing.Point(89, 57);
            this.nDias.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nDias.Name = "nDias";
            this.nDias.Size = new System.Drawing.Size(49, 20);
            this.nDias.TabIndex = 5;
            this.nDias.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // lblPeriodicodesc
            // 
            this.lblPeriodicodesc.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPeriodicodesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodicodesc.Image = global::gControls.Properties.Resources.Periodico;
            this.lblPeriodicodesc.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblPeriodicodesc.Location = new System.Drawing.Point(3, 16);
            this.lblPeriodicodesc.Name = "lblPeriodicodesc";
            this.lblPeriodicodesc.Size = new System.Drawing.Size(416, 27);
            this.lblPeriodicodesc.TabIndex = 4;
            this.lblPeriodicodesc.Text = "Tiempo del evento";
            this.lblPeriodicodesc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // EditorTratamiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnPeriodico);
            this.Controls.Add(this.pnFuturo);
            this.Controls.Add(this.lblTipo);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.txtTitulo);
            this.Name = "EditorTratamiento";
            this.Size = new System.Drawing.Size(422, 321);
            this.pnFuturo.ResumeLayout(false);
            this.pnPeriodico.ResumeLayout(false);
            this.pnPeriodico.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMinutos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nHoras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nDias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtTitulo;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.DateTimePicker dtProx;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.GroupBox pnFuturo;
        private System.Windows.Forms.Label lblFuturoDesc;
        private System.Windows.Forms.GroupBox pnPeriodico;
        private System.Windows.Forms.Label lblPeriodicodesc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nMinutos;
        private System.Windows.Forms.NumericUpDown nHoras;
        private System.Windows.Forms.NumericUpDown nDias;
    }
}
