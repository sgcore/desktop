﻿using gManager.Filtros;
using System.Windows.Forms;

namespace gControls.Editores
{
    public partial class EditorFiltroProducto : UserControl
    {
        FiltroProducto _filtro = new FiltroProducto();
        public EditorFiltroProducto()
        {
            InitializeComponent();
        }

        public FiltroProducto Filtro {
            get
            {
                _filtro.EstadoDiscontinuo = chkDiscontinuo.Checked;
                _filtro.EstadoNormal = chkNormal.Checked;
                _filtro.EstadoPublico = chkPublico.Checked;
                _filtro.SinStock = chkSinStock.Checked;
                _filtro.StockAlto = chkStockIdeal.Checked;
                _filtro.StockBajo = chkStockBajo.Checked;
                _filtro.StockNegativo = chkStockNegativo.Checked;
                return _filtro;
            }
            set
            {
                if(value != null)
                {
                    _filtro = value;
                    chkDiscontinuo.Checked = _filtro.EstadoDiscontinuo;
                    chkNormal.Checked = _filtro.EstadoNormal;
                    chkPublico.Checked = _filtro.EstadoPublico;
                    chkSinStock.Checked = _filtro.SinStock;
                    chkStockIdeal.Checked = _filtro.StockAlto;
                    chkStockBajo.Checked = _filtro.StockBajo;
                    chkStockNegativo.Checked = _filtro.StockNegativo;
                }
               
            }
        }
    }
}
