﻿namespace gControls.Editores
{
    partial class EditorFiltroMovimiento
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorFiltroMovimiento));
            this.PanelTipos = new System.Windows.Forms.FlowLayoutPanel();
            this.chkFiltrarDest = new System.Windows.Forms.CheckBox();
            this.chkIncompletos = new System.Windows.Forms.CheckBox();
            this.chkFiltrarProd = new System.Windows.Forms.CheckBox();
            this.btTodos = new System.Windows.Forms.Button();
            this.btNinguno = new System.Windows.Forms.Button();
            this.nEspecifico = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.EditorFiltroCajas = new gControls.Editores.EditorFiltroCaja();
            this.MovImageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.nEspecifico)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelTipos
            // 
            this.PanelTipos.Location = new System.Drawing.Point(318, 3);
            this.PanelTipos.Name = "PanelTipos";
            this.PanelTipos.Size = new System.Drawing.Size(151, 302);
            this.PanelTipos.TabIndex = 8;
            // 
            // chkFiltrarDest
            // 
            this.chkFiltrarDest.AutoSize = true;
            this.chkFiltrarDest.Location = new System.Drawing.Point(14, 242);
            this.chkFiltrarDest.Name = "chkFiltrarDest";
            this.chkFiltrarDest.Size = new System.Drawing.Size(126, 17);
            this.chkFiltrarDest.TabIndex = 10;
            this.chkFiltrarDest.Text = "Filtrar por destinatario";
            this.chkFiltrarDest.UseVisualStyleBackColor = true;
            this.chkFiltrarDest.CheckedChanged += new System.EventHandler(this.chkFiltrarDest_CheckedChanged);
            // 
            // chkIncompletos
            // 
            this.chkIncompletos.AutoSize = true;
            this.chkIncompletos.Location = new System.Drawing.Point(14, 288);
            this.chkIncompletos.Name = "chkIncompletos";
            this.chkIncompletos.Size = new System.Drawing.Size(144, 17);
            this.chkIncompletos.TabIndex = 11;
            this.chkIncompletos.Text = "Movimientos incompletos";
            this.chkIncompletos.UseVisualStyleBackColor = true;
            // 
            // chkFiltrarProd
            // 
            this.chkFiltrarProd.AutoSize = true;
            this.chkFiltrarProd.Location = new System.Drawing.Point(14, 265);
            this.chkFiltrarProd.Name = "chkFiltrarProd";
            this.chkFiltrarProd.Size = new System.Drawing.Size(114, 17);
            this.chkFiltrarProd.TabIndex = 12;
            this.chkFiltrarProd.Text = "Filtrar por producto";
            this.chkFiltrarProd.UseVisualStyleBackColor = true;
            this.chkFiltrarProd.CheckedChanged += new System.EventHandler(this.chkFiltrarProd_CheckedChanged);
            // 
            // btTodos
            // 
            this.btTodos.Image = global::gControls.Properties.Resources.Sucursal;
            this.btTodos.Location = new System.Drawing.Point(237, 128);
            this.btTodos.Name = "btTodos";
            this.btTodos.Size = new System.Drawing.Size(75, 23);
            this.btTodos.TabIndex = 14;
            this.btTodos.Text = "Todos";
            this.btTodos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btTodos.UseVisualStyleBackColor = true;
            this.btTodos.Click += new System.EventHandler(this.seleccionarTodosbt);
            // 
            // btNinguno
            // 
            this.btNinguno.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btNinguno.Location = new System.Drawing.Point(237, 157);
            this.btNinguno.Name = "btNinguno";
            this.btNinguno.Size = new System.Drawing.Size(75, 26);
            this.btNinguno.TabIndex = 15;
            this.btNinguno.Text = "Ninguno";
            this.btNinguno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btNinguno.UseVisualStyleBackColor = true;
            this.btNinguno.Click += new System.EventHandler(this.deseleccionarTodosbt);
            // 
            // nEspecifico
            // 
            this.nEspecifico.Location = new System.Drawing.Point(30, 147);
            this.nEspecifico.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nEspecifico.Name = "nEspecifico";
            this.nEspecifico.Size = new System.Drawing.Size(87, 20);
            this.nEspecifico.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Número ";
            // 
            // EditorFiltroCajas
            // 
            this.EditorFiltroCajas.Location = new System.Drawing.Point(3, 3);
            this.EditorFiltroCajas.Name = "EditorFiltroCajas";
            this.EditorFiltroCajas.Size = new System.Drawing.Size(309, 119);
            this.EditorFiltroCajas.TabIndex = 13;
            // 
            // MovImageList
            // 
            this.MovImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MovImageList.ImageStream")));
            this.MovImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MovImageList.Images.SetKeyName(0, "Compra");
            this.MovImageList.Images.SetKeyName(1, "Retorno");
            this.MovImageList.Images.SetKeyName(2, "Venta");
            this.MovImageList.Images.SetKeyName(3, "Devolucion");
            this.MovImageList.Images.SetKeyName(4, "Entrada");
            this.MovImageList.Images.SetKeyName(5, "Salida");
            this.MovImageList.Images.SetKeyName(6, "Deposito");
            this.MovImageList.Images.SetKeyName(7, "Extraccion");
            this.MovImageList.Images.SetKeyName(8, "Pedido");
            this.MovImageList.Images.SetKeyName(9, "Presupuesto");
            this.MovImageList.Images.SetKeyName(10, "Terminado");
            this.MovImageList.Images.SetKeyName(11, "Creado");
            this.MovImageList.Images.SetKeyName(12, "Modificado");
            this.MovImageList.Images.SetKeyName(13, "Abajo");
            this.MovImageList.Images.SetKeyName(14, "Arriba");
            this.MovImageList.Images.SetKeyName(15, "Cheque");
            this.MovImageList.Images.SetKeyName(16, "Cuenta");
            this.MovImageList.Images.SetKeyName(17, "Efectivo");
            this.MovImageList.Images.SetKeyName(18, "Tarjeta");
            // 
            // EditorFiltroMovimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nEspecifico);
            this.Controls.Add(this.btNinguno);
            this.Controls.Add(this.btTodos);
            this.Controls.Add(this.EditorFiltroCajas);
            this.Controls.Add(this.chkFiltrarProd);
            this.Controls.Add(this.chkIncompletos);
            this.Controls.Add(this.chkFiltrarDest);
            this.Controls.Add(this.PanelTipos);
            this.Name = "EditorFiltroMovimiento";
            this.Size = new System.Drawing.Size(477, 322);
            ((System.ComponentModel.ISupportInitialize)(this.nEspecifico)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel PanelTipos;
        private System.Windows.Forms.CheckBox chkFiltrarDest;
        private System.Windows.Forms.CheckBox chkIncompletos;
        private System.Windows.Forms.CheckBox chkFiltrarProd;
        private EditorFiltroCaja EditorFiltroCajas;
        private System.Windows.Forms.Button btTodos;
        private System.Windows.Forms.Button btNinguno;
        private System.Windows.Forms.NumericUpDown nEspecifico;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ImageList MovImageList;
        
    }
}
