﻿namespace gControls.Editores
{
    partial class EditorFiltroProducto
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkDiscontinuo = new System.Windows.Forms.CheckBox();
            this.chkNormal = new System.Windows.Forms.CheckBox();
            this.chkPublico = new System.Windows.Forms.CheckBox();
            this.chkStockIdeal = new System.Windows.Forms.CheckBox();
            this.chkStockBajo = new System.Windows.Forms.CheckBox();
            this.chkSinStock = new System.Windows.Forms.CheckBox();
            this.chkStockNegativo = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chkDiscontinuo
            // 
            this.chkDiscontinuo.AutoSize = true;
            this.chkDiscontinuo.Location = new System.Drawing.Point(55, 41);
            this.chkDiscontinuo.Name = "chkDiscontinuo";
            this.chkDiscontinuo.Size = new System.Drawing.Size(193, 24);
            this.chkDiscontinuo.TabIndex = 0;
            this.chkDiscontinuo.Text = "En estado discontinuo";
            this.chkDiscontinuo.UseVisualStyleBackColor = true;
            // 
            // chkNormal
            // 
            this.chkNormal.AutoSize = true;
            this.chkNormal.Location = new System.Drawing.Point(55, 71);
            this.chkNormal.Name = "chkNormal";
            this.chkNormal.Size = new System.Drawing.Size(160, 24);
            this.chkNormal.TabIndex = 1;
            this.chkNormal.Text = "En estado normal";
            this.chkNormal.UseVisualStyleBackColor = true;
            // 
            // chkPublico
            // 
            this.chkPublico.AutoSize = true;
            this.chkPublico.Location = new System.Drawing.Point(55, 101);
            this.chkPublico.Name = "chkPublico";
            this.chkPublico.Size = new System.Drawing.Size(162, 24);
            this.chkPublico.TabIndex = 2;
            this.chkPublico.Text = "En estado publico";
            this.chkPublico.UseVisualStyleBackColor = true;
            // 
            // chkStockIdeal
            // 
            this.chkStockIdeal.AutoSize = true;
            this.chkStockIdeal.Location = new System.Drawing.Point(55, 237);
            this.chkStockIdeal.Name = "chkStockIdeal";
            this.chkStockIdeal.Size = new System.Drawing.Size(273, 24);
            this.chkStockIdeal.TabIndex = 5;
            this.chkStockIdeal.Text = "Stock alto (por ensima de lo ideal)";
            this.chkStockIdeal.UseVisualStyleBackColor = true;
            // 
            // chkStockBajo
            // 
            this.chkStockBajo.AutoSize = true;
            this.chkStockBajo.Location = new System.Drawing.Point(55, 207);
            this.chkStockBajo.Name = "chkStockBajo";
            this.chkStockBajo.Size = new System.Drawing.Size(261, 24);
            this.chkStockBajo.TabIndex = 4;
            this.chkStockBajo.Text = "Stock bajo (por debajo del ideal)";
            this.chkStockBajo.UseVisualStyleBackColor = true;
            // 
            // chkSinStock
            // 
            this.chkSinStock.AutoSize = true;
            this.chkSinStock.Location = new System.Drawing.Point(55, 147);
            this.chkSinStock.Name = "chkSinStock";
            this.chkSinStock.Size = new System.Drawing.Size(491, 24);
            this.chkSinStock.TabIndex = 3;
            this.chkSinStock.Text = "Stock en 0 (activar stock negativo tambien para filtrar \"sin stock\")";
            this.chkSinStock.UseVisualStyleBackColor = true;
            // 
            // chkStockNegativo
            // 
            this.chkStockNegativo.AutoSize = true;
            this.chkStockNegativo.Location = new System.Drawing.Point(55, 177);
            this.chkStockNegativo.Name = "chkStockNegativo";
            this.chkStockNegativo.Size = new System.Drawing.Size(491, 24);
            this.chkStockNegativo.TabIndex = 6;
            this.chkStockNegativo.Text = "Stock negativo (activar stock en 0 tambien para filtrar \"sin stock\")";
            this.chkStockNegativo.UseVisualStyleBackColor = true;
            // 
            // EditorFiltroProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkStockNegativo);
            this.Controls.Add(this.chkStockIdeal);
            this.Controls.Add(this.chkStockBajo);
            this.Controls.Add(this.chkSinStock);
            this.Controls.Add(this.chkPublico);
            this.Controls.Add(this.chkNormal);
            this.Controls.Add(this.chkDiscontinuo);
            this.Name = "EditorFiltroProducto";
            this.Size = new System.Drawing.Size(579, 305);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkDiscontinuo;
        private System.Windows.Forms.CheckBox chkNormal;
        private System.Windows.Forms.CheckBox chkPublico;
        private System.Windows.Forms.CheckBox chkStockIdeal;
        private System.Windows.Forms.CheckBox chkStockBajo;
        private System.Windows.Forms.CheckBox chkSinStock;
        private System.Windows.Forms.CheckBox chkStockNegativo;
    }
}
