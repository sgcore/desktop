﻿namespace gControls.Editores
{
    partial class EditorEfectivo
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorEfectivo));
            this.lblTotal = new System.Windows.Forms.Label();
            this.n001 = new System.Windows.Forms.NumericUpDown();
            this.n005 = new System.Windows.Forms.NumericUpDown();
            this.n010 = new System.Windows.Forms.NumericUpDown();
            this.n025 = new System.Windows.Forms.NumericUpDown();
            this.n050 = new System.Windows.Forms.NumericUpDown();
            this.n1 = new System.Windows.Forms.NumericUpDown();
            this.n2 = new System.Windows.Forms.NumericUpDown();
            this.n5 = new System.Windows.Forms.NumericUpDown();
            this.n10 = new System.Windows.Forms.NumericUpDown();
            this.n20 = new System.Windows.Forms.NumericUpDown();
            this.n50 = new System.Windows.Forms.NumericUpDown();
            this.n100 = new System.Windows.Forms.NumericUpDown();
            this.lblBloqueado = new System.Windows.Forms.Label();
            this.lblSugerencia = new System.Windows.Forms.Label();
            this.btSugerir = new System.Windows.Forms.Button();
            this.btBlockear = new System.Windows.Forms.Button();
            this.bt001 = new System.Windows.Forms.Button();
            this.bt005 = new System.Windows.Forms.Button();
            this.bt010 = new System.Windows.Forms.Button();
            this.bt025 = new System.Windows.Forms.Button();
            this.bt050 = new System.Windows.Forms.Button();
            this.bt1 = new System.Windows.Forms.Button();
            this.bt2 = new System.Windows.Forms.Button();
            this.bt5 = new System.Windows.Forms.Button();
            this.bt10 = new System.Windows.Forms.Button();
            this.bt20 = new System.Windows.Forms.Button();
            this.bt50 = new System.Windows.Forms.Button();
            this.bt100 = new System.Windows.Forms.Button();
            this.btCero = new System.Windows.Forms.Button();
            this.btRedondear = new System.Windows.Forms.Button();
            this.bt200 = new System.Windows.Forms.Button();
            this.bt500 = new System.Windows.Forms.Button();
            this.bt1000 = new System.Windows.Forms.Button();
            this.n200 = new System.Windows.Forms.NumericUpDown();
            this.n500 = new System.Windows.Forms.NumericUpDown();
            this.n1000 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.n001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n010)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n025)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n050)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n500)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.n1000)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTotal
            // 
            this.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTotal.Location = new System.Drawing.Point(240, 187);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(227, 41);
            this.lblTotal.TabIndex = 67;
            this.lblTotal.Text = "$0.00";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // n001
            // 
            this.n001.Location = new System.Drawing.Point(537, 116);
            this.n001.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n001.Name = "n001";
            this.n001.Size = new System.Drawing.Size(47, 20);
            this.n001.TabIndex = 65;
            this.n001.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n005
            // 
            this.n005.Location = new System.Drawing.Point(537, 64);
            this.n005.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n005.Name = "n005";
            this.n005.Size = new System.Drawing.Size(47, 20);
            this.n005.TabIndex = 63;
            this.n005.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n010
            // 
            this.n010.Location = new System.Drawing.Point(539, 11);
            this.n010.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n010.Name = "n010";
            this.n010.Size = new System.Drawing.Size(47, 20);
            this.n010.TabIndex = 61;
            this.n010.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n025
            // 
            this.n025.Location = new System.Drawing.Point(440, 115);
            this.n025.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n025.Name = "n025";
            this.n025.Size = new System.Drawing.Size(47, 20);
            this.n025.TabIndex = 59;
            this.n025.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n050
            // 
            this.n050.Location = new System.Drawing.Point(440, 67);
            this.n050.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n050.Name = "n050";
            this.n050.Size = new System.Drawing.Size(47, 20);
            this.n050.TabIndex = 57;
            this.n050.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n1
            // 
            this.n1.Location = new System.Drawing.Point(440, 16);
            this.n1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n1.Name = "n1";
            this.n1.Size = new System.Drawing.Size(47, 20);
            this.n1.TabIndex = 55;
            this.n1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n2
            // 
            this.n2.Location = new System.Drawing.Point(347, 118);
            this.n2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n2.Name = "n2";
            this.n2.Size = new System.Drawing.Size(47, 20);
            this.n2.TabIndex = 53;
            this.n2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n5
            // 
            this.n5.Location = new System.Drawing.Point(347, 64);
            this.n5.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n5.Name = "n5";
            this.n5.Size = new System.Drawing.Size(47, 20);
            this.n5.TabIndex = 51;
            this.n5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n10
            // 
            this.n10.Location = new System.Drawing.Point(347, 14);
            this.n10.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n10.Name = "n10";
            this.n10.Size = new System.Drawing.Size(47, 20);
            this.n10.TabIndex = 49;
            this.n10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n20
            // 
            this.n20.Location = new System.Drawing.Point(219, 118);
            this.n20.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n20.Name = "n20";
            this.n20.Size = new System.Drawing.Size(47, 20);
            this.n20.TabIndex = 47;
            this.n20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n50
            // 
            this.n50.Location = new System.Drawing.Point(220, 66);
            this.n50.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n50.Name = "n50";
            this.n50.Size = new System.Drawing.Size(47, 20);
            this.n50.TabIndex = 45;
            this.n50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n100
            // 
            this.n100.Location = new System.Drawing.Point(219, 13);
            this.n100.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n100.Name = "n100";
            this.n100.Size = new System.Drawing.Size(47, 20);
            this.n100.TabIndex = 43;
            this.n100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblBloqueado
            // 
            this.lblBloqueado.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBloqueado.ForeColor = System.Drawing.Color.Silver;
            this.lblBloqueado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBloqueado.Location = new System.Drawing.Point(51, 165);
            this.lblBloqueado.Name = "lblBloqueado";
            this.lblBloqueado.Size = new System.Drawing.Size(103, 17);
            this.lblBloqueado.TabIndex = 84;
            this.lblBloqueado.Text = "Desbloqueado";
            this.lblBloqueado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSugerencia
            // 
            this.lblSugerencia.AutoSize = true;
            this.lblSugerencia.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSugerencia.ForeColor = System.Drawing.Color.Silver;
            this.lblSugerencia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblSugerencia.Location = new System.Drawing.Point(52, 205);
            this.lblSugerencia.Name = "lblSugerencia";
            this.lblSugerencia.Size = new System.Drawing.Size(127, 16);
            this.lblSugerencia.TabIndex = 86;
            this.lblSugerencia.Text = "No hay sugerencia";
            this.lblSugerencia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btSugerir
            // 
            this.btSugerir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btSugerir.BackgroundImage")));
            this.btSugerir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btSugerir.Location = new System.Drawing.Point(13, 197);
            this.btSugerir.Name = "btSugerir";
            this.btSugerir.Size = new System.Drawing.Size(32, 32);
            this.btSugerir.TabIndex = 85;
            this.btSugerir.UseVisualStyleBackColor = true;
            this.btSugerir.Click += new System.EventHandler(this.btSugerir_Click);
            // 
            // btBlockear
            // 
            this.btBlockear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btBlockear.BackgroundImage")));
            this.btBlockear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btBlockear.Location = new System.Drawing.Point(13, 157);
            this.btBlockear.Name = "btBlockear";
            this.btBlockear.Size = new System.Drawing.Size(32, 32);
            this.btBlockear.TabIndex = 83;
            this.btBlockear.UseVisualStyleBackColor = true;
            this.btBlockear.Click += new System.EventHandler(this.btBlockear_Click);
            // 
            // bt001
            // 
            this.bt001.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt001.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt001.Image = global::gControls.Properties.Resources.m001;
            this.bt001.Location = new System.Drawing.Point(493, 104);
            this.bt001.Name = "bt001";
            this.bt001.Size = new System.Drawing.Size(46, 40);
            this.bt001.TabIndex = 82;
            this.bt001.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt001.UseVisualStyleBackColor = true;
            this.bt001.Click += new System.EventHandler(this.bt001_Click);
            // 
            // bt005
            // 
            this.bt005.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt005.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt005.Image = global::gControls.Properties.Resources.m005;
            this.bt005.Location = new System.Drawing.Point(493, 52);
            this.bt005.Name = "bt005";
            this.bt005.Size = new System.Drawing.Size(46, 40);
            this.bt005.TabIndex = 81;
            this.bt005.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt005.UseVisualStyleBackColor = true;
            this.bt005.Click += new System.EventHandler(this.bt005_Click);
            // 
            // bt010
            // 
            this.bt010.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt010.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt010.Image = global::gControls.Properties.Resources.m010;
            this.bt010.Location = new System.Drawing.Point(493, 3);
            this.bt010.Name = "bt010";
            this.bt010.Size = new System.Drawing.Size(46, 40);
            this.bt010.TabIndex = 80;
            this.bt010.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt010.UseVisualStyleBackColor = true;
            this.bt010.Click += new System.EventHandler(this.bt010_Click);
            // 
            // bt025
            // 
            this.bt025.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt025.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt025.Image = global::gControls.Properties.Resources.m025;
            this.bt025.Location = new System.Drawing.Point(400, 108);
            this.bt025.Name = "bt025";
            this.bt025.Size = new System.Drawing.Size(44, 40);
            this.bt025.TabIndex = 79;
            this.bt025.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt025.UseVisualStyleBackColor = true;
            this.bt025.Click += new System.EventHandler(this.bt025_Click);
            // 
            // bt050
            // 
            this.bt050.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt050.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt050.Image = global::gControls.Properties.Resources.m050;
            this.bt050.Location = new System.Drawing.Point(400, 55);
            this.bt050.Name = "bt050";
            this.bt050.Size = new System.Drawing.Size(44, 40);
            this.bt050.TabIndex = 78;
            this.bt050.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt050.UseVisualStyleBackColor = true;
            this.bt050.Click += new System.EventHandler(this.bt050_Click);
            // 
            // bt1
            // 
            this.bt1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt1.Image = global::gControls.Properties.Resources.m1;
            this.bt1.Location = new System.Drawing.Point(400, 6);
            this.bt1.Name = "bt1";
            this.bt1.Size = new System.Drawing.Size(44, 40);
            this.bt1.TabIndex = 77;
            this.bt1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt1.UseVisualStyleBackColor = true;
            this.bt1.Click += new System.EventHandler(this.bt1_Click);
            // 
            // bt2
            // 
            this.bt2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt2.Image = global::gControls.Properties.Resources.m2;
            this.bt2.Location = new System.Drawing.Point(272, 106);
            this.bt2.Name = "bt2";
            this.bt2.Size = new System.Drawing.Size(80, 40);
            this.bt2.TabIndex = 76;
            this.bt2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt2.UseVisualStyleBackColor = true;
            this.bt2.Click += new System.EventHandler(this.bt2_Click);
            // 
            // bt5
            // 
            this.bt5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt5.Image = global::gControls.Properties.Resources.m5;
            this.bt5.Location = new System.Drawing.Point(272, 57);
            this.bt5.Name = "bt5";
            this.bt5.Size = new System.Drawing.Size(80, 40);
            this.bt5.TabIndex = 75;
            this.bt5.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt5.UseVisualStyleBackColor = true;
            this.bt5.Click += new System.EventHandler(this.bt5_Click);
            // 
            // bt10
            // 
            this.bt10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt10.Image = global::gControls.Properties.Resources.m10;
            this.bt10.Location = new System.Drawing.Point(272, 5);
            this.bt10.Name = "bt10";
            this.bt10.Size = new System.Drawing.Size(80, 40);
            this.bt10.TabIndex = 74;
            this.bt10.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt10.UseVisualStyleBackColor = true;
            this.bt10.Click += new System.EventHandler(this.bt10_Click);
            // 
            // bt20
            // 
            this.bt20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt20.Image = global::gControls.Properties.Resources.m20;
            this.bt20.Location = new System.Drawing.Point(146, 106);
            this.bt20.Name = "bt20";
            this.bt20.Size = new System.Drawing.Size(80, 40);
            this.bt20.TabIndex = 73;
            this.bt20.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt20.UseVisualStyleBackColor = true;
            this.bt20.Click += new System.EventHandler(this.bt20_Click);
            // 
            // bt50
            // 
            this.bt50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt50.Image = global::gControls.Properties.Resources.m50;
            this.bt50.Location = new System.Drawing.Point(146, 54);
            this.bt50.Name = "bt50";
            this.bt50.Size = new System.Drawing.Size(80, 40);
            this.bt50.TabIndex = 72;
            this.bt50.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt50.UseVisualStyleBackColor = true;
            this.bt50.Click += new System.EventHandler(this.bt50_Click);
            // 
            // bt100
            // 
            this.bt100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt100.Image = global::gControls.Properties.Resources.m100;
            this.bt100.Location = new System.Drawing.Point(146, 3);
            this.bt100.Name = "bt100";
            this.bt100.Size = new System.Drawing.Size(80, 40);
            this.bt100.TabIndex = 71;
            this.bt100.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt100.UseVisualStyleBackColor = true;
            this.bt100.Click += new System.EventHandler(this.bt100_Click);
            // 
            // btCero
            // 
            this.btCero.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btCero.BackgroundImage")));
            this.btCero.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCero.Location = new System.Drawing.Point(473, 187);
            this.btCero.Name = "btCero";
            this.btCero.Size = new System.Drawing.Size(32, 32);
            this.btCero.TabIndex = 70;
            this.btCero.UseVisualStyleBackColor = true;
            this.btCero.Click += new System.EventHandler(this.btCero_Click);
            // 
            // btRedondear
            // 
            this.btRedondear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btRedondear.BackgroundImage")));
            this.btRedondear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btRedondear.Location = new System.Drawing.Point(515, 187);
            this.btRedondear.Name = "btRedondear";
            this.btRedondear.Size = new System.Drawing.Size(32, 32);
            this.btRedondear.TabIndex = 69;
            this.btRedondear.UseVisualStyleBackColor = true;
            // 
            // bt200
            // 
            this.bt200.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt200.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt200.Image = global::gControls.Properties.Resources.m200;
            this.bt200.Location = new System.Drawing.Point(13, 109);
            this.bt200.Name = "bt200";
            this.bt200.Size = new System.Drawing.Size(80, 40);
            this.bt200.TabIndex = 92;
            this.bt200.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt200.UseVisualStyleBackColor = true;
            this.bt200.Click += new System.EventHandler(this.bt200_Click);
            // 
            // bt500
            // 
            this.bt500.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt500.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt500.Image = global::gControls.Properties.Resources.m500;
            this.bt500.Location = new System.Drawing.Point(13, 57);
            this.bt500.Name = "bt500";
            this.bt500.Size = new System.Drawing.Size(80, 40);
            this.bt500.TabIndex = 91;
            this.bt500.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt500.UseVisualStyleBackColor = true;
            this.bt500.Click += new System.EventHandler(this.bt500_Click);
            // 
            // bt1000
            // 
            this.bt1000.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt1000.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt1000.Image = global::gControls.Properties.Resources.m1000;
            this.bt1000.Location = new System.Drawing.Point(13, 6);
            this.bt1000.Name = "bt1000";
            this.bt1000.Size = new System.Drawing.Size(80, 40);
            this.bt1000.TabIndex = 90;
            this.bt1000.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.bt1000.UseVisualStyleBackColor = true;
            this.bt1000.Click += new System.EventHandler(this.button3_Click);
            // 
            // n200
            // 
            this.n200.Location = new System.Drawing.Point(86, 121);
            this.n200.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n200.Name = "n200";
            this.n200.Size = new System.Drawing.Size(47, 20);
            this.n200.TabIndex = 89;
            this.n200.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n500
            // 
            this.n500.Location = new System.Drawing.Point(87, 69);
            this.n500.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n500.Name = "n500";
            this.n500.Size = new System.Drawing.Size(47, 20);
            this.n500.TabIndex = 88;
            this.n500.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // n1000
            // 
            this.n1000.Location = new System.Drawing.Point(86, 16);
            this.n1000.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.n1000.Name = "n1000";
            this.n1000.Size = new System.Drawing.Size(47, 20);
            this.n1000.TabIndex = 87;
            this.n1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // EditorEfectivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.bt200);
            this.Controls.Add(this.bt500);
            this.Controls.Add(this.bt1000);
            this.Controls.Add(this.n200);
            this.Controls.Add(this.n500);
            this.Controls.Add(this.n1000);
            this.Controls.Add(this.lblSugerencia);
            this.Controls.Add(this.btSugerir);
            this.Controls.Add(this.lblBloqueado);
            this.Controls.Add(this.btBlockear);
            this.Controls.Add(this.bt001);
            this.Controls.Add(this.bt005);
            this.Controls.Add(this.bt010);
            this.Controls.Add(this.bt025);
            this.Controls.Add(this.bt050);
            this.Controls.Add(this.bt1);
            this.Controls.Add(this.bt2);
            this.Controls.Add(this.bt5);
            this.Controls.Add(this.bt10);
            this.Controls.Add(this.bt20);
            this.Controls.Add(this.bt50);
            this.Controls.Add(this.bt100);
            this.Controls.Add(this.btCero);
            this.Controls.Add(this.btRedondear);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.n001);
            this.Controls.Add(this.n005);
            this.Controls.Add(this.n010);
            this.Controls.Add(this.n025);
            this.Controls.Add(this.n050);
            this.Controls.Add(this.n1);
            this.Controls.Add(this.n2);
            this.Controls.Add(this.n5);
            this.Controls.Add(this.n10);
            this.Controls.Add(this.n20);
            this.Controls.Add(this.n50);
            this.Controls.Add(this.n100);
            this.Name = "EditorEfectivo";
            this.Size = new System.Drawing.Size(608, 257);
            ((System.ComponentModel.ISupportInitialize)(this.n001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n010)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n025)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n050)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n500)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.n1000)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btCero;
        internal System.Windows.Forms.Button btRedondear;
        internal System.Windows.Forms.Label lblTotal;
        internal System.Windows.Forms.NumericUpDown n001;
        internal System.Windows.Forms.NumericUpDown n005;
        internal System.Windows.Forms.NumericUpDown n010;
        internal System.Windows.Forms.NumericUpDown n025;
        internal System.Windows.Forms.NumericUpDown n050;
        internal System.Windows.Forms.NumericUpDown n1;
        internal System.Windows.Forms.NumericUpDown n2;
        internal System.Windows.Forms.NumericUpDown n5;
        internal System.Windows.Forms.NumericUpDown n10;
        internal System.Windows.Forms.NumericUpDown n20;
        internal System.Windows.Forms.NumericUpDown n50;
        internal System.Windows.Forms.NumericUpDown n100;
        private System.Windows.Forms.Button bt100;
        private System.Windows.Forms.Button bt50;
        private System.Windows.Forms.Button bt20;
        private System.Windows.Forms.Button bt10;
        private System.Windows.Forms.Button bt2;
        private System.Windows.Forms.Button bt5;
        private System.Windows.Forms.Button bt1;
        private System.Windows.Forms.Button bt050;
        private System.Windows.Forms.Button bt025;
        private System.Windows.Forms.Button bt010;
        private System.Windows.Forms.Button bt005;
        private System.Windows.Forms.Button bt001;
        internal System.Windows.Forms.Button btBlockear;
        private System.Windows.Forms.Label lblBloqueado;
        internal System.Windows.Forms.Button btSugerir;
        private System.Windows.Forms.Label lblSugerencia;
        private System.Windows.Forms.Button bt200;
        private System.Windows.Forms.Button bt500;
        private System.Windows.Forms.Button bt1000;
        internal System.Windows.Forms.NumericUpDown n200;
        internal System.Windows.Forms.NumericUpDown n500;
        internal System.Windows.Forms.NumericUpDown n1000;
    }
}
