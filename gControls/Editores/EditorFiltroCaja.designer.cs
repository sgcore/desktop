﻿namespace gControls.Editores
{
    partial class EditorFiltroCaja
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tcajaactual = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.tLista = new System.Windows.Forms.TabPage();
            this.btListaCustom = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tunacaja = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbCajaSola = new System.Windows.Forms.ComboBox();
            this.tentrecajas = new System.Windows.Forms.TabPage();
            this.nCajafinal = new System.Windows.Forms.NumericUpDown();
            this.nCajaInicial = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.tentrefechas = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.dpFechaFin = new System.Windows.Forms.DateTimePicker();
            this.dpFechaIni = new System.Windows.Forms.DateTimePicker();
            this.ttodas = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tcajaactual.SuspendLayout();
            this.tLista.SuspendLayout();
            this.tunacaja.SuspendLayout();
            this.tentrecajas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nCajafinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCajaInicial)).BeginInit();
            this.tentrefechas.SuspendLayout();
            this.ttodas.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tcajaactual);
            this.tabControl1.Controls.Add(this.tLista);
            this.tabControl1.Controls.Add(this.tunacaja);
            this.tabControl1.Controls.Add(this.tentrecajas);
            this.tabControl1.Controls.Add(this.tentrefechas);
            this.tabControl1.Controls.Add(this.ttodas);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(303, 113);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 3;
            // 
            // tcajaactual
            // 
            this.tcajaactual.Controls.Add(this.label1);
            this.tcajaactual.Location = new System.Drawing.Point(4, 49);
            this.tcajaactual.Name = "tcajaactual";
            this.tcajaactual.Size = new System.Drawing.Size(295, 60);
            this.tcajaactual.TabIndex = 2;
            this.tcajaactual.Text = "Caja Actual";
            this.tcajaactual.ToolTipText = "Obtiene la caja actual seleccionada en el sistema.";
            this.tcajaactual.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(289, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Muestra la caja en curso";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tLista
            // 
            this.tLista.Controls.Add(this.btListaCustom);
            this.tLista.Controls.Add(this.label2);
            this.tLista.Location = new System.Drawing.Point(4, 49);
            this.tLista.Name = "tLista";
            this.tLista.Size = new System.Drawing.Size(295, 60);
            this.tLista.TabIndex = 3;
            this.tLista.Text = "Lista";
            this.tLista.ToolTipText = "Obtiene la última caja creada en el sistema";
            this.tLista.UseVisualStyleBackColor = true;
            // 
            // btListaCustom
            // 
            this.btListaCustom.Location = new System.Drawing.Point(82, 29);
            this.btListaCustom.Name = "btListaCustom";
            this.btListaCustom.Size = new System.Drawing.Size(113, 23);
            this.btListaCustom.TabIndex = 2;
            this.btListaCustom.Text = "Seleccionar Cajas";
            this.btListaCustom.UseVisualStyleBackColor = true;
            this.btListaCustom.Click += new System.EventHandler(this.btListaCustom_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(289, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Muestra una lista especifica de cajas";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tunacaja
            // 
            this.tunacaja.Controls.Add(this.label3);
            this.tunacaja.Controls.Add(this.cmbCajaSola);
            this.tunacaja.Location = new System.Drawing.Point(4, 49);
            this.tunacaja.Name = "tunacaja";
            this.tunacaja.Size = new System.Drawing.Size(295, 60);
            this.tunacaja.TabIndex = 4;
            this.tunacaja.Text = "Una Caja";
            this.tunacaja.ToolTipText = "Obtiene una caja en particular.";
            this.tunacaja.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 40);
            this.label3.TabIndex = 2;
            this.label3.Text = "Seleccione la caja que desea";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbCajaSola
            // 
            this.cmbCajaSola.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCajaSola.FormattingEnabled = true;
            this.cmbCajaSola.Location = new System.Drawing.Point(153, 21);
            this.cmbCajaSola.Name = "cmbCajaSola";
            this.cmbCajaSola.Size = new System.Drawing.Size(84, 21);
            this.cmbCajaSola.TabIndex = 1;
            // 
            // tentrecajas
            // 
            this.tentrecajas.Controls.Add(this.nCajafinal);
            this.tentrecajas.Controls.Add(this.nCajaInicial);
            this.tentrecajas.Controls.Add(this.label4);
            this.tentrecajas.Location = new System.Drawing.Point(4, 49);
            this.tentrecajas.Name = "tentrecajas";
            this.tentrecajas.Padding = new System.Windows.Forms.Padding(3);
            this.tentrecajas.Size = new System.Drawing.Size(295, 60);
            this.tentrecajas.TabIndex = 0;
            this.tentrecajas.Text = "Entre cajas";
            this.tentrecajas.ToolTipText = "Obtiene las cajas intermedias entre dos cajas.";
            this.tentrecajas.UseVisualStyleBackColor = true;
            // 
            // nCajafinal
            // 
            this.nCajafinal.Location = new System.Drawing.Point(171, 27);
            this.nCajafinal.Name = "nCajafinal";
            this.nCajafinal.Size = new System.Drawing.Size(87, 20);
            this.nCajafinal.TabIndex = 4;
            this.nCajafinal.Enter += new System.EventHandler(this.nCajaInicial_Enter);
            // 
            // nCajaInicial
            // 
            this.nCajaInicial.Location = new System.Drawing.Point(42, 27);
            this.nCajaInicial.Name = "nCajaInicial";
            this.nCajaInicial.Size = new System.Drawing.Size(86, 20);
            this.nCajaInicial.TabIndex = 3;
            this.nCajaInicial.Enter += new System.EventHandler(this.nCajaInicial_Enter);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(289, 24);
            this.label4.TabIndex = 2;
            this.label4.Text = "Seleccione el rango de cajas";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tentrefechas
            // 
            this.tentrefechas.Controls.Add(this.label5);
            this.tentrefechas.Controls.Add(this.dpFechaFin);
            this.tentrefechas.Controls.Add(this.dpFechaIni);
            this.tentrefechas.Location = new System.Drawing.Point(4, 49);
            this.tentrefechas.Name = "tentrefechas";
            this.tentrefechas.Padding = new System.Windows.Forms.Padding(3);
            this.tentrefechas.Size = new System.Drawing.Size(295, 60);
            this.tentrefechas.TabIndex = 1;
            this.tentrefechas.Text = "Entre fechas";
            this.tentrefechas.ToolTipText = "Obtiene las cajas entre creadas entre dos fechas.";
            this.tentrefechas.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 40);
            this.label5.TabIndex = 9;
            this.label5.Text = "Seleccione el rango de fechas";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dpFechaFin
            // 
            this.dpFechaFin.CustomFormat = "dd/MM/yyy - HH:mm";
            this.dpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpFechaFin.Location = new System.Drawing.Point(137, 32);
            this.dpFechaFin.Name = "dpFechaFin";
            this.dpFechaFin.Size = new System.Drawing.Size(143, 20);
            this.dpFechaFin.TabIndex = 8;
            // 
            // dpFechaIni
            // 
            this.dpFechaIni.CustomFormat = "dd/MM/yyy - HH:mm";
            this.dpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpFechaIni.Location = new System.Drawing.Point(137, 6);
            this.dpFechaIni.Name = "dpFechaIni";
            this.dpFechaIni.Size = new System.Drawing.Size(143, 20);
            this.dpFechaIni.TabIndex = 7;
            // 
            // ttodas
            // 
            this.ttodas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ttodas.Controls.Add(this.label6);
            this.ttodas.Location = new System.Drawing.Point(4, 49);
            this.ttodas.Name = "ttodas";
            this.ttodas.Size = new System.Drawing.Size(295, 60);
            this.ttodas.TabIndex = 5;
            this.ttodas.Text = "Todas";
            this.ttodas.ToolTipText = "Obtiene todas las cajas creadas en el sistema.";
            this.ttodas.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(1, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(289, 40);
            this.label6.TabIndex = 1;
            this.label6.Text = "Muestra todas las cajas creadas en el sistema";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditorFiltroCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "EditorFiltroCaja";
            this.Size = new System.Drawing.Size(309, 119);
            this.tabControl1.ResumeLayout(false);
            this.tcajaactual.ResumeLayout(false);
            this.tLista.ResumeLayout(false);
            this.tunacaja.ResumeLayout(false);
            this.tentrecajas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nCajafinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nCajaInicial)).EndInit();
            this.tentrefechas.ResumeLayout(false);
            this.ttodas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tentrecajas;
        private System.Windows.Forms.TabPage tentrefechas;
        private System.Windows.Forms.TabPage tunacaja;
        private System.Windows.Forms.TabPage tcajaactual;
        private System.Windows.Forms.TabPage tLista;
        private System.Windows.Forms.TabPage ttodas;
        private System.Windows.Forms.ComboBox cmbCajaSola;
        private System.Windows.Forms.DateTimePicker dpFechaFin;
        private System.Windows.Forms.DateTimePicker dpFechaIni;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nCajafinal;
        private System.Windows.Forms.NumericUpDown nCajaInicial;
        private System.Windows.Forms.Button btListaCustom;
    }
}
