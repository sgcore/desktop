﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Editores
{
    public partial class EditorUsuarios : BaseControl
    {
        gcUsuario _user;
        public EditorUsuarios()
        {
            InitializeComponent();
            cmbPermiso.DataSource = Enum.GetValues(typeof(gcUsuario.PermisoEnum));
        }
        public gManager.gcUsuario Usuario
        {
            get
            {
                if (_user != null)
                {
                    _user.Nombre = txtNombre.Text;
                    _user.User = txtUser.Text;
                    _user.Pass = txtPass.Text;
                    _user.Permiso = cmbPermiso.SelectedValue!=null?(gcUsuario.PermisoEnum)cmbPermiso.SelectedValue:gcUsuario.PermisoEnum.Visitante;
                    _user.Central = _central;
                    _user.Responsable = _responsable;
                    _user.Descripcion = txtDescrip.Text;

                    
                }
                return _user;
            }
            set
            {
                
                _user = value;
                txtNombre.Text = _user.Nombre;
                txtUser.Text = _user.User;
                txtPass.Text = _user.Pass;
                cmbPermiso.SelectedIndex  = (int)_user.Permiso <5?(int)_user.Permiso:4;
                _responsable = _user.Responsable;
                _central = _user.Central;
                lblcentral.Text = _central != null ? _central.Nombre : "Seleccione una central para el usuario.";
                lblRespo.Text = _responsable != null ? _responsable.Nombre : "Seleccione un responsable para el usuario.";
                bool esadmin = RequisitoPermiso(gcUsuario.PermisoEnum.Administrador);
                txtNombre.Enabled = esadmin;
                txtUser.Enabled = esadmin;
                cmbPermiso.Enabled = esadmin && gManager.CoreManager.Singlenton.Usuario.Id != _user.Id ;
                txtDescrip.Text = _user.Descripcion;
                btCentral.Enabled = esadmin;
                btResponsable.Enabled = esadmin;
                
            }
        }
       
        private void cmbPermiso_SelectedIndexChanged(object sender, EventArgs e)
        {
            var p = cmbPermiso.SelectedValue!=null?(gcUsuario.PermisoEnum)cmbPermiso.SelectedValue:gcUsuario.PermisoEnum.Visitante;
            switch (p)
            {
                case gcUsuario.PermisoEnum.Visitante:
                    pbp.Image = Properties.Resources.Ver;
                    break;
                case gcUsuario.PermisoEnum.Vendedor:
                     pbp.Image = Properties.Resources.Venta;
                    break;
                case gcUsuario.PermisoEnum.Encargado:
                     pbp.Image = Properties.Resources.Caja;
                    break;
                case gcUsuario.PermisoEnum.Contador:
                     pbp.Image = Properties.Resources.Cantidad;
                    break;
                case gcUsuario.PermisoEnum.Administrador:
                     pbp.Image = Properties.Resources.Configuracion;
                    break;
            }
        }
        private gcDestinatario _central;
        private void btCentral_Click(object sender, EventArgs e)
        {
            _central = buscar.BuscarDestinatario("Buscar Central de Usuario", gcDestinatario.DestinatarioTipo.Sucursal);
            lblcentral.Text = _central != null ? _central.Nombre : "Seleccione una central para el usuario.";
        }
        private gcDestinatario _responsable;
        private void btResponsable_Click(object sender, EventArgs e)
        {
            _responsable = buscar.BuscarDestinatario("Buscar Responsable de Usuario", gcDestinatario.DestinatarioTipo.Responsable);
            lblRespo.Text = _responsable != null ? _responsable.Nombre : "Seleccione un responsable para el usuario.";
        }
    }
}
