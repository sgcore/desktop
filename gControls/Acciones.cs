﻿using gManager;
using gManager.Filtros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gControls
{
    public static class Acciones
    {
        public static event EventHandler CambioDestinatario;
        public static event EventHandler CambioProducto;
        //crear
        public static void CrearCliente(object o, EventArgs e)
        {
           var d= Editar.EditarDestinatario(new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Cliente));
           if (d != null) setDestinatario(d);
        }
        public static void CrearProveedor(object o, EventArgs e)
        {
            var d=Editar.EditarDestinatario(new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Proveedor));
            if (d != null) setDestinatario(d);
        }
        public static void CrearSucursal(object o, EventArgs e)
        {
            var d=Editar.EditarDestinatario(new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Sucursal));
            if (d != null) setDestinatario(d);
        }
        public static void CrearResponsable(object o, EventArgs e)
        {
            var d=Editar.EditarDestinatario(new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Responsable));
            if (d != null) setDestinatario(d);
        }
        public static void CrearPaciente(object o, EventArgs e)
        {
            var p = new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Paciente);
            if (getDestinatario("Buscar el Cliente correspondiente", gManager.gcDestinatario.DestinatarioTipo.Cliente) != null) p.Parent = Destinatario.Id;
            
            Editar.EditarDestinatario(p);
        }
        public static void CrearContacto(object o, EventArgs e)
        {
            var p = new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Contacto);
            if (getDestinatario("Buscar el Cliente correspondiente",gManager.gcDestinatario.DestinatarioTipo.Cliente) != null) p.Parent = Destinatario.Id;
            Editar.EditarDestinatario(p);
        }
        public static void CrearFinanciera(object o, EventArgs e)
        {
            var d=Editar.EditarDestinatario(new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Financiera));
            if (d != null) setDestinatario(d);
        }
        public static void CrearBanco(object o, EventArgs e)
        {
            Editar.EditarDestinatario(new gManager.gcDestinatario(gManager.gcDestinatario.DestinatarioTipo.Banco));
        }

        //buscar
        public static EventHandler BuscarClienteHandle { get; set; }
        public static EventHandler BuscarProveedorHandle { get; set; }
        public static EventHandler BuscarSucursalHandle { get; set; }
        public static EventHandler BuscarResponsableHandle { get; set; }
        public static EventHandler BuscarPacienteHandle { get; set; }
        public static EventHandler BuscarContactoHandle { get; set; }
        public static EventHandler BuscarFinancieraHandle { get; set; }
        public static EventHandler BuscarBancoHandle { get; set; }
        

        public static void BuscarCliente(object o, EventArgs e)
        {
            if (BuscarClienteHandle != null)
            {
                BuscarClienteHandle(o, e);
                return;

            }
            Destinatario = null;
            getDestinatario("Buscar Cliente", gManager.gcDestinatario.DestinatarioTipo.Cliente);

            
        }
        public static void BuscarProveedor(object o, EventArgs e)
        {
            if (BuscarProveedorHandle != null)
            {
                BuscarProveedorHandle(o, e);
                return;

            }
            Destinatario = null;
            getDestinatario("Buscar Proveedor", gManager.gcDestinatario.DestinatarioTipo.Proveedor);

             
        }
        public static void BuscarSucursal(object o, EventArgs e)
        {
            if (BuscarSucursalHandle != null)
            {
                BuscarSucursalHandle(o, e);
                return;

            }
            Destinatario = null;
            getDestinatario("Buscar Sucursal", gManager.gcDestinatario.DestinatarioTipo.Sucursal);

            
        }
        public static void BuscarResponsable(object o, EventArgs e)
        {
            if (BuscarResponsableHandle != null)
            {
                BuscarResponsableHandle(o, e);
                return;

            }
            Destinatario = null;
            getDestinatario("Buscar Responsable", gManager.gcDestinatario.DestinatarioTipo.Responsable);

             
        }
        public static void BuscarPaciente(object o, EventArgs e)
        {
            if (BuscarPacienteHandle != null)
            {
                BuscarPacienteHandle(o, e);
                return;

            }
            Destinatario = null;
            getDestinatario("Buscar Paciente", gManager.gcDestinatario.DestinatarioTipo.Paciente);

            
        }
        public static void BuscarContacto(object o, EventArgs e)
        {
            if (BuscarContactoHandle != null)
            {
                BuscarContactoHandle(o, e);
                return;

            }
            Destinatario = null;
            getDestinatario("Buscar Contacto", gManager.gcDestinatario.DestinatarioTipo.Contacto);

             
        }
        public static void BuscarFinanciera(object o, EventArgs e)
        {
            if (BuscarFinancieraHandle != null)
            {
                BuscarFinancieraHandle(o, e);
                return;

            }
            getDestinatario("Buscar Financiera", gManager.gcDestinatario.DestinatarioTipo.Financiera);

            
        }
        public static void BuscarBanco(object o, EventArgs e)
        {
            if (BuscarBancoHandle != null)
            {
                BuscarBancoHandle(o, e);
                return;

            }
            Destinatario = null;
            getDestinatario("Buscar Banco", gManager.gcDestinatario.DestinatarioTipo.Banco);

             
        }
        

        //destinatario
        public static gManager.gcDestinatario Destinatario { get; internal set; }
        private static gManager.gcDestinatario getDestinatario(string titulo,gManager.gcDestinatario.DestinatarioTipo t)
        {
            if (Destinatario == null || Destinatario.Tipo != t) { Destinatario = buscar.BuscarDestinatario(titulo, t);
            if (CambioDestinatario != null) CambioDestinatario(Destinatario, null);
            }
            return Destinatario;
        }
        public static void setDestinatario(gcDestinatario d)
        {
            Destinatario = d;
            if (CambioDestinatario != null) CambioDestinatario(Destinatario, null);
        }
        public static void EditarDestinatario(object o, EventArgs e)
        {
            Editar.EditarDestinatario(Destinatario);
        }

        public static void VerDestinatario(object o, EventArgs e)
        {
            ver.VerDestinatarios(Destinatario);
        }

        //editar
        public static void EditarCliente(object o, EventArgs e)
        {
            getDestinatario("Seleccione el cliente que desea editar:", gManager.gcDestinatario.DestinatarioTipo.Cliente);
            
            Editar.EditarDestinatario(Destinatario);
        }
        public static void EditarProveedor(object o, EventArgs e)
        {
            getDestinatario("Seleccione el proveedor que desea editar:", gManager.gcDestinatario.DestinatarioTipo.Proveedor);

            Editar.EditarDestinatario(Destinatario);
        }
        public static void EditarSucursal(object o, EventArgs e)
        {
            getDestinatario("Seleccione la sucursal que desea editar:", gManager.gcDestinatario.DestinatarioTipo.Sucursal);

            Editar.EditarDestinatario(Destinatario);
        }
        public static void EditarResponsable(object o, EventArgs e)
        {
            getDestinatario("Seleccione el responsable que desea editar:", gManager.gcDestinatario.DestinatarioTipo.Responsable);

            Editar.EditarDestinatario(Destinatario);
        }
        public static void EditarPaciente(object o, EventArgs e)
        {
            getDestinatario("Seleccione el paciente que desea editar:", gManager.gcDestinatario.DestinatarioTipo.Paciente);

            Editar.EditarDestinatario(Destinatario);
        }
        public static void EditarContacto(object o, EventArgs e)
        {
            getDestinatario("Seleccione el contacto que desea editar:", gManager.gcDestinatario.DestinatarioTipo.Contacto);

            Editar.EditarDestinatario(Destinatario);
        }
        public static void EditarFinanciera(object o, EventArgs e)
        {
            getDestinatario("Seleccione la financiera que desea editar:", gManager.gcDestinatario.DestinatarioTipo.Financiera);

            Editar.EditarDestinatario(Destinatario);
        }
        public static void EditarBanco(object o, EventArgs e)
        {
            getDestinatario("Seleccione el banco que desea editar:", gManager.gcDestinatario.DestinatarioTipo.Banco);

            Editar.EditarDestinatario(Destinatario);
        }

        //Ver
        public static void VerCliente(object o, EventArgs e)
        {
            getDestinatario("Seleccione el cliente que desea ver:", gManager.gcDestinatario.DestinatarioTipo.Cliente);

            ver.VerDestinatarios(Destinatario);
        }
        public static void VerProveedor(object o, EventArgs e)
        {
            getDestinatario("Seleccione el Proveedor que desea ver:", gManager.gcDestinatario.DestinatarioTipo.Proveedor);

            ver.VerDestinatarios(Destinatario);
        }
        public static void VerSucursal(object o, EventArgs e)
        {
            getDestinatario("Seleccione la sucursal que desea ver:", gManager.gcDestinatario.DestinatarioTipo.Sucursal);

            ver.VerDestinatarios(Destinatario);
        }
        public static void VerResponsable(object o, EventArgs e)
        {
            getDestinatario("Seleccione el responsable que desea ver:", gManager.gcDestinatario.DestinatarioTipo.Responsable);

            ver.VerDestinatarios(Destinatario);
        }
        public static void VerPaciente(object o, EventArgs e)
        {
            getDestinatario("Seleccione el paciente que desea ver:", gManager.gcDestinatario.DestinatarioTipo.Paciente);

            ver.VerDestinatarios(Destinatario);
        }
        public static void VerContacto(object o, EventArgs e)
        {
            getDestinatario("Seleccione el contácto que desea ver:", gManager.gcDestinatario.DestinatarioTipo.Contacto);

            ver.VerDestinatarios(Destinatario);
        }
        public static void VerFinanciera(object o, EventArgs e)
        {
            getDestinatario("Seleccione la financiera que desea ver:", gManager.gcDestinatario.DestinatarioTipo.Financiera);

            ver.VerDestinatarios(Destinatario);
        }
        public static void VerBanco(object o, EventArgs e)
        {
            getDestinatario("Seleccione el banco que desea ver:", gManager.gcDestinatario.DestinatarioTipo.Banco);

            ver.VerDestinatarios(Destinatario);
        }

        //acciones clientes
        public static void Vender(object o, EventArgs e)
        {
            //vende si pasan el parametro
            if (o != null && o is gcDestinatario)
            {
                var de = o as gcDestinatario;
                if(de.Tipo==gcDestinatario.DestinatarioTipo.Cliente)
                    CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Venta, de));
                return;
            }
            getDestinatario("Buscar Cliente para vender", gManager.gcDestinatario.DestinatarioTipo.Cliente);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Cliente)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Venta, Destinatario));
            }
        }
        public static void VenderA(object o, EventArgs e)
        {
            setDestinatario(null);
            Vender(o, e);
        }
        public static void Devolver(object o, EventArgs e)
        {
            getDestinatario("Buscar Cliente para devolver", gManager.gcDestinatario.DestinatarioTipo.Cliente);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Cliente)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Devolucion, Destinatario));
            }
        }
        public static void Presupuestar(object o, EventArgs e)
        {
            getDestinatario("Buscar Cliente para presupuestar", gManager.gcDestinatario.DestinatarioTipo.Cliente);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Cliente)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Presupuesto, Destinatario));
            }
        }
        public static void DepositoCliente(object o, EventArgs e)
        {
            getDestinatario("Buscar Cliente para depósito", gManager.gcDestinatario.DestinatarioTipo.Cliente);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Cliente)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Deposito, Destinatario));
            }
        }
        public static void ExtraccionCliente(object o, EventArgs e)
        {
            getDestinatario("Buscar Cliente para extracción", gManager.gcDestinatario.DestinatarioTipo.Cliente);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Cliente)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Extraccion, Destinatario));
            }
        }
        //acciones proveedores
        public static void Comprar(object o, EventArgs e)
        {
            getDestinatario("Buscar Proveedor para comprar", gManager.gcDestinatario.DestinatarioTipo.Proveedor);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Proveedor)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Compra, Destinatario));
            }
        }
        public static void Retornar(object o, EventArgs e)
        {
            getDestinatario("Buscar Proveedor para retornar", gManager.gcDestinatario.DestinatarioTipo.Proveedor);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Proveedor)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Retorno, Destinatario));
            }
        }
        public static void Pedir(object o, EventArgs e)
        {
            getDestinatario("Buscar Proveedor para hacer pedido", gManager.gcDestinatario.DestinatarioTipo.Proveedor);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Proveedor)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Pedido, Destinatario));
            }
        }
        public static void DepositoProveedor(object o, EventArgs e)
        {
            getDestinatario("Buscar Proveedor para depósitar", gManager.gcDestinatario.DestinatarioTipo.Proveedor);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Proveedor)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Extraccion, Destinatario));
            }
        }
        public static void ExtraccionProveedor(object o, EventArgs e)
        {
            getDestinatario("Buscar Proveedor para recibir reintegro", gManager.gcDestinatario.DestinatarioTipo.Proveedor);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Proveedor)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Deposito, Destinatario));
            }
        }
        //acciones con sucursales
        public static void Entrada(object o, EventArgs e)
        {
            getDestinatario("Buscar Sucursal para Ingresar", gManager.gcDestinatario.DestinatarioTipo.Sucursal);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Sucursal)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Entrada, Destinatario));
            }
        }
        public static void Salida(object o, EventArgs e)
        {
            getDestinatario("Buscar Sucursal para enviar", gManager.gcDestinatario.DestinatarioTipo.Sucursal);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Sucursal)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Salida, Destinatario));
            }
        }
        //acciones con responsables
        public static void DepositoResponsable(object o, EventArgs e)
        {
            getDestinatario("Buscar Responsable para depósitar", gManager.gcDestinatario.DestinatarioTipo.Responsable);
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Responsable)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Deposito, Destinatario));
            }
        }
        public static void ExtraccionResponsable(object o, EventArgs e)
        {
            getDestinatario("Buscar Responsable para Extraer", gManager.gcDestinatario.DestinatarioTipo.Responsable );
            if (Destinatario != null && Destinatario.Tipo == gManager.gcDestinatario.DestinatarioTipo.Responsable)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(gcMovimiento.TipoMovEnum.Extraccion, Destinatario));
            }
        }
        //cajas
        public static EventHandler VerCajaHandle { get; set; }
        public static void BuscarCaja(object o, EventArgs e)
        {
            
            CoreManager.Singlenton.CambiarACaja(buscar.BuscarCaja());
        }

        public static void TerminarCaja(object o, EventArgs e)
        {
            if (CoreManager.Singlenton.BloquearCajaParaCierre())
            {
                CoreManager.Singlenton.recargar((p, q) =>
                {
                    ver.VerAsistenteCierreCaja();
                    CoreManager.Singlenton.DesbloquearCajaParaCierre();
                });
            }
            

        }
        public static void VerCaja(object o, EventArgs e)
        {
            if (VerCajaHandle != null)
            {
                VerCajaHandle(o, e);
                return;

            }
            ver.VerCaja(CoreManager.Singlenton.CajaActual);
        }
        public static void VerCajas(object o, EventArgs e)
        {
            var l = buscar.BuscarCajas();
            if(l.Count>0)
                ver.VerCajas(l);
        }
        public static void BuscarMovimientos(object o, EventArgs e)
        {
            buscar.BuscarMovimiento("Buscar con Filtro", true);
        }
        //usuarios
        public static void BuscarUsuarios(object o, EventArgs e)
        {
            Editar.EditarUsuario(buscar.BuscarUsuario());
        }
        public static void EditarMiUsuario(object o, EventArgs e)
        {
            Editar.EditarUsuario(CoreManager.Singlenton.Usuario);
        }
        public static void NuevoUsuario(object o, EventArgs e)
        {
            Editar.EditarUsuario(new gcUsuario());
        }
        //productos
        static gcObjeto _producto = null;
        public static gcObjeto Producto
        {
            get
            {
               
                return _producto;
            }
        }
        public static EventHandler BuscarProductoHandle { get; set; }
        public static void BuscarProductos(object o, EventArgs e)
        {
            if (BuscarProductoHandle != null)
            {
                BuscarProductoHandle(o, e);
                return;

            }
            var f = new FiltroProducto();
            f.Tipos.Add(gcObjeto.ObjetoTIpo.Producto);
            f.Tipos.Add(gcObjeto.ObjetoTIpo.Servicio);
            buscar.BuscarProducto("Buscar productos y servicios",f);
        }
        public static void setProducto(gcObjeto o)
        {
            _producto = o;
            CambioProducto(o, null);
        }
        private static void crearProducto(gcObjeto.ObjetoTIpo t)
        {
            gcObjeto c = null;
            switch (t)
            {
                case gcObjeto.ObjetoTIpo.Categoria :
                    c = new gcCategoria(){Parent= obtenerParent()} ;
                    break;
                case gcObjeto.ObjetoTIpo.Producto:
                    c = new gcProducto() { Parent = obtenerParent() };
                    break;
                case gcObjeto.ObjetoTIpo.Servicio:
                    c = new gcServicio() { Parent = obtenerParent() };
                    break;
                case gcObjeto.ObjetoTIpo.Grupo:

                    c = new gcGrupo();
                    
                    break;
            }
            Editar.EditarProducto(c);
            if(c.Id>0)setProducto(c);
        }
        private static int obtenerParent()
        {
            if (Producto == null || Producto.Tipo != gcObjeto.ObjetoTIpo.Categoria)
            {

                var f = new FiltroProducto();
                f.Tipos.Add(gcObjeto.ObjetoTIpo.Categoria);
                getProducto("Buscar categoria contenedora", f);

            }
            if (Producto != null && Producto.Tipo == gcObjeto.ObjetoTIpo.Categoria)
                return Producto.Id;
                   

            return 0;
        }
        private static gcObjeto  getProducto(string titulo, FiltroProducto f)
        {
            
                   
                setProducto(buscar.BuscarProducto(titulo, f));
                  
            
             return Producto;

        }
        public static void verProducto(object o, EventArgs e)
        {
            
            if(Producto==null){
                var f=new FiltroProducto();

                getProducto("Buscar un producto, servicio o categoria para ver", f);
            }
            ver.VerProducto(Producto);
        }
        public static void EditarProducto(object o, EventArgs e)
        {

            if (Producto == null)
            {
                var f = new FiltroProducto();

                getProducto("Buscar un producto, servicio o categoria para editar", f);
            }
            Editar.EditarProducto(Producto);
        }
        private static bool _cascadiando = false;
        private static void cascadiar(CoreManager.ProductoDelegate accion)
        {
            if (_cascadiando)
            {
                gLogger.logger.Singlenton.addWarningMsg("Ya esta realizando una accion. espere mientras termina.");
                return;
            }
            if (Producto != null && Producto.Tipo == gcObjeto.ObjetoTIpo.Categoria)
            {
                gcCategoria cat = Producto as gcCategoria;
                
                    _cascadiando = true;
                    gLogger.TaskManager.CrearTareaAislada((worker) =>
                    {
                        var list = cat.subProductosYServicios();
                        int cont = 0;
                        int totalcount = list.Count;
                        foreach (gcObjeto pro in list)
                        {
                            accion(pro);
                            cont++;
                            if (worker != null)
                            {
                                worker.ReportProgress((cont * 100 / totalcount) - 1);
                            }


                        }
                        if (worker != null)
                        {
                            worker.ReportProgress(100);
                        }
                    }, (aa, bb) => { _cascadiando = false; }, (percent, desc) => CoreManager.Singlenton.InformarProgreso(percent, desc), "Actualizando " + Producto.Nombre + "...");

                
            }
        }
        public static void ActualizarCascadaCosto(object o, EventArgs e)
        {
            decimal costo = Editar.EditarDecimal(0, "Editar el costo en cascada");
            if (costo > 0)
            {
                cascadiar(p =>
                {
                    p.Costo = costo;
                    p.SaveMe();
                });
            }

        }
        public static void ActualizarCascadaGanancia(object o, EventArgs e)
        {
            decimal costo = Editar.EditarDecimal(0, "Editar la ganancia en cascada");
            if (costo > 0)
            {
                cascadiar(p =>
                {
                    p.Ganancia = costo;
                    p.SaveMe();
                });
            }

        }
        public static void ActualizarCascadaIva(object o, EventArgs e)
        {
            decimal costo = Editar.EditarDecimal(0, "Editar el IVA en cascada");
            if (costo > 0)
            {
                cascadiar(p =>
                {
                    p.Iva = costo;
                    p.SaveMe();
                });
            }

        }
        public static void ActualizarCascadaIdeal(object o, EventArgs e)
        {
            decimal costo = Editar.EditarDecimal(0, "Editar el Stock Ideal en cascada");
            if (costo > 0)
            {
                cascadiar(p =>
                {
                    p.Ideal = costo;
                    p.SaveMe();
                });
            }

        }
        public static void ActualizarCascadaEstadoNormal(object o, EventArgs e)
        {
            cascadiar(p =>
            {
                p.State  = gcObjeto.ObjetoState.Normal;
                p.SaveMe();
            });

        }
        public static void ActualizarCascadaEstadoPublico(object o, EventArgs e)
        {
            cascadiar(p =>
            {
                p.State = gcObjeto.ObjetoState.Publico;
                p.SaveMe();
            });

        }
        public static void ActualizarCascadaEstadoDiscontinuo(object o, EventArgs e)
        {
            cascadiar(p =>
            {
                p.State = gcObjeto.ObjetoState.Discontinuo;
                p.SaveMe();
            });

        }
        public static void ActualizarCascadaDolar(object o, EventArgs e)
        {
            decimal costo = Editar.EditarDecimal(0, "Editar el percio dolar en cascada");
            if (costo > 0)
            {
                cascadiar(p =>
                {
                    p.Dolar = costo;
                    p.SaveMe();
                });
            }

        }
        public static void ActualizarCascadaMetrica(object o, EventArgs e)
        {
            int costo = Editar.SeleccionarMetrica();
            if (costo > -1)
            {
                cascadiar(p =>
                {
                    p.Metrica = (gcObjeto.ObjetoMedida)costo;
                    p.SaveMe();
                });
            }

        }

        public static void NuevoProducto(object o, EventArgs e)
        {
            crearProducto(gcObjeto.ObjetoTIpo.Producto);
        }
        public static void NuevoGrupo(object o, EventArgs e)
        {
            crearProducto(gcObjeto.ObjetoTIpo.Grupo);
        }
        public static void NuevaCategoria(object o, EventArgs e)
        {
            crearProducto(gcObjeto.ObjetoTIpo.Categoria);
        }
        public static void NuevoServicio(object o, EventArgs e)
        {
            crearProducto(gcObjeto.ObjetoTIpo.Servicio);
        }

        private static FiltroProducto _filtroListaPrecio = new FiltroProducto();
        public static void ListaPrecio(object o, EventArgs e)
        {
            _filtroListaPrecio = Editar.EditarFiltro(_filtroListaPrecio) as FiltroProducto;
            ver.VerListaPrecio(_filtroListaPrecio);
            //var lista = new ControlesHtml.htmlObjects.htmlListaPrecios(f as FiltroProducto);
            //CoreManager.Singlenton.GenerarInformeHtml(lista.ToString());
        }



        //herramientas
        public static void LeerExcel(object o, EventArgs e)
        {
            buscar.BuscarProductoEnCSV("Actualizar desde Archivo");
              
        }
        public static void VerRegistroKey(object o, EventArgs e)
        {
            ver.VerRegistro();

        }
        public static void LoguinInternet(object o, EventArgs e)
        {
            CoreManager.Singlenton.loginInternet();
        }
        public static void Registrarse(object o, EventArgs e)
        {
            CoreManager.Singlenton.RegistrarseAsync();

        }

        public static void ObtenerProductos(object o, EventArgs e)
        {
            var b = new Buscadores.BuscadorProductos();
            b.Titulo = "Obteniendo productos del servidor...";
            b.addContextMenuItem(Properties.Resources.Guardar, "Guardar/Actualizar Seleccionados", "Actualiza el sistema con los productos seleccionados"
                   , (ooo, eee) => { // funcion para guardar si existe o crear si no existe
                       foreach (gcObjeto a in b.Seleccionados)
                       {
                           gcObjeto x = a.getMyMergedLocalCopy();
                           x.Sincronizado = true;
                           x.SaveMe();
                       }


                   });
            b.CustomDatos = true;
            
            b.CustomActualizar = (oo, ee) =>
            {
                var x = WebManager.Singlenton.getProductos((nada, l) =>
                {

                    b.setCustomDatos(l as List<gcObjetoGenerico>);
                    b.Titulo = "Productos actualmente sincronizados";

                });
                //CoreManager.Singlenton.getWebProductos();
            };
            b.iniciar();
            var f = new Formularios.fEditorContenedor();
            f.Text = "Mi lista de internet";
            f.Control = b;
            f.ShowDialog();
           

        }
    }
}
