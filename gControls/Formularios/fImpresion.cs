﻿using BrightIdeasSoftware;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace gControls.Formularios
{
    public partial class fImpresion : Form
    {
        public fImpresion()
        {
            InitializeComponent();
        }
        public void SetLista(ObjectListView l)
        {
            lwp1.ListView = l;

        }
        //public void setBuscador(Buscadores.Buscador b)
        //{
        //    SetLista(b.ExposeLW);
        //    this.printPreviewControl1.Zoom = 1;
        //    this.printPreviewControl1.AutoZoom = true;

        //    ApplyModernFormatting();
        //    UpdatePrintPreview();
        //}
        /// <summary>
        /// Give the report a minimal set of default formatting values.
        /// </summary>
        public void ApplyMinimalFormatting()
        {
            this.lwp1.CellFormat = null;
            this.lwp1.ListFont = new Font("Tahoma", 9);

            this.lwp1.HeaderFormat = BlockFormat.Header();
            this.lwp1.HeaderFormat.TextBrush = Brushes.Black;
            this.lwp1.HeaderFormat.BackgroundBrush = null;
            this.lwp1.HeaderFormat.SetBorderPen(Sides.Bottom, new Pen(Color.Black, 0.5f));

            this.lwp1.FooterFormat = BlockFormat.Footer();
            this.lwp1.GroupHeaderFormat = BlockFormat.GroupHeader();
            Brush brush = new LinearGradientBrush(new Point(0, 0), new Point(200, 0), Color.Gray, Color.White);
            this.lwp1.GroupHeaderFormat.SetBorder(Sides.Bottom, 2, brush);

            this.lwp1.ListHeaderFormat = BlockFormat.ListHeader();
            this.lwp1.ListHeaderFormat.BackgroundBrush = null;

            this.lwp1.WatermarkFont = null;
            this.lwp1.WatermarkColor = Color.Empty;
        }
        /// <summary>
        /// Give the report a minimal set of default formatting values.
        /// </summary>
        public void ApplyModernFormatting()
        {
            this.lwp1.CellFormat = null;
            this.lwp1.ListFont = new Font("Ms Sans Serif", 9);
            this.lwp1.ListGridPen = new Pen(Color.DarkGray, 0.5f);

            this.lwp1.HeaderFormat = BlockFormat.Header(new Font("Verdana", 24, FontStyle.Bold));
            this.lwp1.HeaderFormat.BackgroundBrush = new LinearGradientBrush(new Point(0, 0), new Point(200, 0), Color.DarkBlue, Color.White);

            this.lwp1.FooterFormat = BlockFormat.Footer();
            this.lwp1.FooterFormat.BackgroundBrush = new LinearGradientBrush(new Point(0, 0), new Point(200, 0), Color.White, Color.Blue);

            this.lwp1.GroupHeaderFormat = BlockFormat.GroupHeader();
            this.lwp1.ListHeaderFormat = BlockFormat.ListHeader(new Font("Verdana", 12));

            this.lwp1.WatermarkFont = null;
            this.lwp1.WatermarkColor = Color.Empty;
        }

        /// <summary>
        /// Give the report a minimal set of default formatting values.
        /// </summary>
        public void ApplyOverTheTopFormatting()
        {
            this.lwp1.CellFormat = null;
            this.lwp1.ListFont = new Font("Ms Sans Serif", 9);
            this.lwp1.ListGridPen = new Pen(Color.Blue, 0.5f);

            this.lwp1.HeaderFormat = BlockFormat.Header(new Font("Comic Sans MS", 36));
            this.lwp1.HeaderFormat.TextBrush = new LinearGradientBrush(new Point(0, 0), new Point(900, 0), Color.Black, Color.Blue);
            this.lwp1.HeaderFormat.BackgroundBrush = new TextureBrush(Properties.Resources.Aceptar16, WrapMode.Tile);
            this.lwp1.HeaderFormat.SetBorder(Sides.All, 10, new LinearGradientBrush(new Point(0, 0), new Point(300, 0), Color.Purple, Color.Pink));

            this.lwp1.FooterFormat = BlockFormat.Footer(new Font("Comic Sans MS", 12));
            this.lwp1.FooterFormat.TextBrush = Brushes.Blue;
            this.lwp1.FooterFormat.BackgroundBrush = new LinearGradientBrush(new Point(0, 0), new Point(200, 0), Color.Gold, Color.Green);
            this.lwp1.FooterFormat.SetBorderPen(Sides.All, new Pen(Color.FromArgb(128, Color.Green), 5));

            this.lwp1.GroupHeaderFormat = BlockFormat.GroupHeader();
            Brush brush = new HatchBrush(HatchStyle.LargeConfetti, Color.Blue, Color.Empty);
            this.lwp1.GroupHeaderFormat.SetBorder(Sides.Bottom, 5, brush);

            this.lwp1.ListHeaderFormat = BlockFormat.ListHeader(new Font("Comic Sans MS", 12));
            this.lwp1.ListHeaderFormat.BackgroundBrush = Brushes.PowderBlue;
            this.lwp1.ListHeaderFormat.TextBrush = Brushes.Black;

            this.lwp1.WatermarkFont = new Font("Comic Sans MS", 72);
            this.lwp1.WatermarkColor = Color.Red;
        }
        void UpdatePrintPreview()
        {
           

            this.lwp1.DocumentName = "Documento";
            this.lwp1.Header = "Header".Replace("\\t", "\t");
            this.lwp1.Footer = "footer".Replace("\\t", "\t");
            this.lwp1.Watermark = "Watermark";

            this.lwp1.IsShrinkToFit = true;
            this.lwp1.IsTextOnly = true;
            this.lwp1.IsPrintSelectionOnly = false;

            
                this.ApplyMinimalFormatting();
            

            //elimina las lineas
                //this.lwp1.ListGridPen = null;

            this.lwp1.FirstPage = 0;
            this.lwp1.LastPage = 10;

            this.printPreviewControl1.InvalidatePreview();
        }

    }
}
