﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Formularios
{
    public partial class fOpciones : Form
    {
        public fOpciones()
        {
            InitializeComponent();
            Icon = Icon.FromHandle(Properties.Resources.Conectado24.GetHicon());
            Seleccion = -1;
        }
        public int Seleccion { get;set; }
        public void  addBoton(string titulo,int opt,Image img){
            var b = new BotonOpcion(opt, titulo, img);
            b.Click += (o, e) => { Seleccion = b.Opcion; DialogResult = System.Windows.Forms.DialogResult.OK; };
            Controls.Add(b);
            b.Dock = DockStyle.Top;
            
        }
        public void setDescripcion(string title)
        {
            label1.Text = title;
            label1.Visible = !String.IsNullOrEmpty(title);
        }
        
    }
}
