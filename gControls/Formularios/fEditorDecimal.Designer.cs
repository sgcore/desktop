﻿namespace gControls.Formularios
{
    partial class fEditorDecimal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btCancelar = new System.Windows.Forms.Button();
            this.btGuardar = new System.Windows.Forms.Button();
            this.editorDecimal1 = new gManager.Editores.EditorDecimal();
            this.SuspendLayout();
            // 
            // btCancelar
            // 
            this.btCancelar.AutoSize = true;
            this.btCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancelar.Image = global::gControls.Properties.Resources.Unlike;
            this.btCancelar.Location = new System.Drawing.Point(36, 93);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(74, 23);
            this.btCancelar.TabIndex = 40;
            this.btCancelar.Text = "Cancelar";
            this.btCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCancelar.UseVisualStyleBackColor = true;
            // 
            // btGuardar
            // 
            this.btGuardar.AutoSize = true;
            this.btGuardar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btGuardar.Image = global::gControls.Properties.Resources.Like;
            this.btGuardar.Location = new System.Drawing.Point(141, 95);
            this.btGuardar.Name = "btGuardar";
            this.btGuardar.Size = new System.Drawing.Size(78, 23);
            this.btGuardar.TabIndex = 39;
            this.btGuardar.Text = "Guardar";
            this.btGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btGuardar.UseVisualStyleBackColor = true;
            // 
            // editorDecimal1
            // 
            this.editorDecimal1.Dock = System.Windows.Forms.DockStyle.Top;
            this.editorDecimal1.Location = new System.Drawing.Point(0, 0);
            this.editorDecimal1.LugaresDecimales = 2;
            this.editorDecimal1.Maximo = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.editorDecimal1.Minimo = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.editorDecimal1.Name = "editorDecimal1";
            this.editorDecimal1.Size = new System.Drawing.Size(235, 89);
            this.editorDecimal1.TabIndex = 0;
            this.editorDecimal1.ValorDecimal = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // fEditorDecimal
            // 
            this.AcceptButton = this.btGuardar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancelar;
            this.ClientSize = new System.Drawing.Size(235, 125);
            this.Controls.Add(this.btCancelar);
            this.Controls.Add(this.btGuardar);
            this.Controls.Add(this.editorDecimal1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fEditorDecimal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private gManager.Editores.EditorDecimal editorDecimal1;
        private System.Windows.Forms.Button btCancelar;
        private System.Windows.Forms.Button btGuardar;
    }
}