﻿namespace gControls.Formularios
{
    partial class fImpresion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lwp1 = new BrightIdeasSoftware.ListViewPrinter();
            this.printPreviewControl1 = new System.Windows.Forms.PrintPreviewControl();
            this.SuspendLayout();
            // 
            // lwp1
            // 
            // 
            // 
            // 
            this.lwp1.CellFormat.CanWrap = true;
            this.lwp1.CellFormat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lwp1.Footer = "This is the footers";
            // 
            // 
            // 
            this.lwp1.FooterFormat.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Italic);
            // 
            // 
            // 
            this.lwp1.GroupHeaderFormat.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.lwp1.Header = "This is the header\t\tRight";
            // 
            // 
            // 
            this.lwp1.HeaderFormat.Font = new System.Drawing.Font("Verdana", 24F);
            this.lwp1.IsListHeaderOnEachPage = false;
            // 
            // 
            // 
            this.lwp1.ListHeaderFormat.CanWrap = true;
            this.lwp1.ListHeaderFormat.Font = new System.Drawing.Font("Verdana", 12F);
            this.lwp1.Watermark = "SGCORE";
            this.lwp1.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // printPreviewControl1
            // 
            this.printPreviewControl1.AutoZoom = false;
            this.printPreviewControl1.Columns = 2;
            this.printPreviewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printPreviewControl1.Document = this.lwp1;
            this.printPreviewControl1.Location = new System.Drawing.Point(0, 0);
            this.printPreviewControl1.Name = "printPreviewControl1";
            this.printPreviewControl1.Size = new System.Drawing.Size(915, 547);
            this.printPreviewControl1.TabIndex = 7;
            this.printPreviewControl1.UseAntiAlias = true;
            this.printPreviewControl1.Zoom = 0.25834046193327631D;
            // 
            // fImpresion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 547);
            this.Controls.Add(this.printPreviewControl1);
            this.Name = "fImpresion";
            this.Text = "Impresión";
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.ListViewPrinter lwp1;
        private System.Windows.Forms.PrintPreviewControl printPreviewControl1;
    }
}