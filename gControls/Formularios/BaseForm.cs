﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;

namespace gControls.Formularios
{
    
    public class BaseForm:Form
    {
        public HotKeys _hk;
        protected MenuStrip MenuPrincipal;
        private ToolStripLabel mnuClientesLabelAcciones;
        private List<BotonAccion> _list = new List<BotonAccion>();
        public BaseForm()
        {
            InitializeComponent();
            //captura las selecciones
            gManager.CoreManager.Singlenton.DestinatarioSeleccionado += Acciones.setDestinatario;
            gManager.CoreManager.Singlenton.ProductoSeleccionado += Acciones.setProducto;
        }
        public HotKeys HotKeysEngine
        {
            get
            {
                if (_hk == null) _hk = new HotKeys(this);
                return _hk;
            }
        }
        protected void loadHotKeys(Control.ControlCollection col)
        {
            //_list.Clear();
            foreach (Control cc in col)
            {
                if (cc is ToolStrip)
                {
                    foreach (ToolStripItem i in (cc as ToolStrip).Items)
                    {
                        if (i is ToolStripBotonAccion)
                        {
                            registrarBoton((i as ToolStripBotonAccion).Boton);
                            
                        }
                    }
                    continue;
                }
                if (cc.HasChildren)
                {
                    loadHotKeys(cc.Controls);
                }
                else if (cc is BotonAccion)
                {
                    registrarBoton(cc as BotonAccion);
                    
                }
            }
        }
        public void registrarBoton(BotonAccion b)
        {
            if (b.HotKey != Shortcut.None && !b.RegistradoEnForm)
            {
                if (b.GlobalHotKey)
                {

                    if (!HotKeysEngine.add(b.HotKey, b.clickear))
                        MessageBox.Show("Se intento agregar un hotkeys repetido");
                }
                else

                    _list.Add(b);
                b.RegistradoEnForm = true;

            }
            b.FormContainer = this;
        }
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == 0x0312)
            {
                int id = m.WParam.ToInt32();
                HotKeysEngine.perform(id);


            }
        }
        
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            foreach(BotonAccion b in _list)
            {
                if (((int)b.HotKey) ==((int)keyData))
                {
                    b.PerformClick();
                    return true;
                }
                
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void InitializeComponent()
        {
            this.MenuPrincipal = new System.Windows.Forms.MenuStrip();
            this.mnuClientesLabelAcciones = new System.Windows.Forms.ToolStripLabel();
            this.SuspendLayout();
            // 
            // MenuPrincipal
            // 
            this.MenuPrincipal.BackColor = System.Drawing.SystemColors.Control;
            this.MenuPrincipal.BackgroundImage = global::gControls.Properties.Resources.FondoGris;
            this.MenuPrincipal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.MenuPrincipal.Name = "MenuPrincipal";
            this.MenuPrincipal.Size = new System.Drawing.Size(742, 24);
            this.MenuPrincipal.TabIndex = 0;
            this.MenuPrincipal.Text = "MenuPrincipal";
            // 
            // mnuClientesLabelAcciones
            // 
            this.mnuClientesLabelAcciones.Image = global::gControls.Properties.Resources.Buscar;
            this.mnuClientesLabelAcciones.Name = "mnuClientesLabelAcciones";
            this.mnuClientesLabelAcciones.Size = new System.Drawing.Size(173, 22);
            this.mnuClientesLabelAcciones.Text = "Acciones";
            this.mnuClientesLabelAcciones.ToolTipText = "Muestra un buscador de Clientes";
            // 
            // BaseForm
            // 
            this.ClientSize = new System.Drawing.Size(742, 431);
            this.Controls.Add(this.MenuPrincipal);
            this.KeyPreview = true;
            this.MainMenuStrip = this.MenuPrincipal;
            this.Name = "BaseForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        public ToolStripMenuItem crearMenuItem(Image img, string texto, string desc, EventHandler del, Keys shortcut = Keys.None,gManager.gcUsuario.PermisoEnum permiso=gManager.gcUsuario.PermisoEnum.Visitante , bool requierekey=false)
        {
            var btseg = new ToolStripMenuItem(img);
            btseg.ToolTipText = desc;
            btseg.Text = texto;
            btseg.ShortcutKeys = shortcut;
            btseg.Click += del;
            btseg.Enabled = gManager.CoreManager.Singlenton.ElUsuarioSuperaElPermiso(permiso);
            if(requierekey)
            {
                Registracion.BloquearConKey(btseg);
            }
            
            return btseg;
        }
        private ToolStripItem getDestinatarioLabel(string text, gManager.gcDestinatario.DestinatarioTipo t,ToolStripMenuItem menu)
        {
            var lbl = addTitulo(text, menu);
            //lbl.Paint += (o, pe) =>
            //{
                
            //    pe.ClipRectangle.Inflate(-1, -1);
            //    pe.Graphics.DrawRectangle(Pens.Black, pe.ClipRectangle);
            //};
            Acciones.CambioDestinatario += (o, e) =>
            {
                var c = o as gManager.gcDestinatario;
                if (c != null && c.Tipo == t)
                {
                    lbl.Text = c.Nombre;
                    lbl.Font = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);
                    lbl.BackgroundImage = Properties.Resources.FondoAzul;
                    lbl.BackgroundImageLayout = ImageLayout.Stretch;
                    colorear(menu);
                    menu.ShowDropDown();
                }
                else
                {
                    lbl.Text = text;
                    lbl.Font = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold);
                    lbl.BackgroundImage = Properties.Resources.FondoGris;
                    lbl.BackgroundImageLayout = ImageLayout.Stretch;
                }
            };
           
            return lbl;
        }
        public ToolStripItem addTitulo(string titulo, ToolStripMenuItem menu)
        {
            menu.DropDownItems.Add(new ToolStripSeparator());
            var lbl = new ToolStripLabel(titulo);
            lbl.Font = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold);
           // lbl.BackgroundImage = Properties.Resources.FondoGris;
            lbl.BackgroundImageLayout = ImageLayout.Stretch;
           
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            menu.DropDownItems.Add(lbl);
            menu.DropDownItems.Add(new ToolStripSeparator());
            return lbl;

        }
        public ToolStripMenuItem generarMenuClientes()
        {
            var mc = crearMenuItem(Properties.Resources.Cliente, "&Clientes", "Gestión de Clientes", null);
            
           getDestinatarioLabel("Seleccione un Cliente",gManager.gcDestinatario.DestinatarioTipo.Cliente,mc);

           mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Buscar,
               "&Buscar Cliente",
               "Busca un cliente",
               Acciones.BuscarCliente,
               Keys.F3, gManager.gcUsuario.PermisoEnum.Vendedor, false
               ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.VerCliente, 
                "&Ver Cliente", 
                "Muestra las compras, depositos y todo lo relacionado con el cliente seleccionado.",
                Acciones.VerCliente,
                Keys.Control | Keys.E, gManager.gcUsuario.PermisoEnum.Encargado,true 
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Editar, 
                "&Editar Cliente", 
                "Edita los datos del cliente seleccionado.",
                Acciones.EditarCliente,
                Keys.Alt | Keys.E, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.NuevoDestinatario,
               "&Nuevo Cliente",
               "Crea un nuevo cliente.",
               Acciones.CrearCliente,
               Keys.Alt | Keys.C, gManager.gcUsuario.PermisoEnum.Encargado
               ));
            //mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Paciente,
            //  "&Agregar Paciente",
            //  "Crea un nuevo paciente al cliente seleccionado.",
            //  Acciones.CrearPaciente,
            //  Keys.Alt | Keys.P, gManager.gcUsuario.PermisoEnum.Encargado
            //  ));
            //------------------------------------
            addTitulo("Acciones del Cliente", mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Vender,
                "&Vender a Cliente",
                "Crea una venta al cliente seleccionado.",
                Acciones.Vender,
                Keys.Control | Keys.F3, gManager.gcUsuario.PermisoEnum.Vendedor
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Vender,
                "&Vender a otro Cliente",
                "Busca un cliente y le realiza la venta.",
                Acciones.VenderA,
                Keys.Shift | Keys.F3 
                , gManager.gcUsuario.PermisoEnum.Vendedor
                ));
           
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Devolver, "&Devolución a Cliente", "Crear una devolución del cliente seleccionado",
               Acciones.Devolver,
              Keys.None, gManager.gcUsuario.PermisoEnum.Encargado
               ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Presupuestar, "&Presupuesto a Cliente", "Crear un presupuesto al cliente seleccionado",
              Acciones.Presupuestar,
             Keys.Control |Keys.Alt | Keys.F3, gManager.gcUsuario.PermisoEnum.Vendedor
              ));

            mc.DropDownItems.Add(new ToolStripSeparator());
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.DepositoCliente, "Dep&osito de Cliente", "Crear un deposito en la cuenta corriente del cliente seleccionado",
              Acciones.DepositoCliente,
             Keys.Alt | Keys.F3, gManager.gcUsuario.PermisoEnum.Encargado
              ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.ExtraccionCliente, "E&xtracción de Cliente", "Crear una extracción de la cuenta corriente del cliente seleccionado",
              Acciones.ExtraccionCliente,
             Keys.None, gManager.gcUsuario.PermisoEnum.Encargado
              ));

            var l=addTitulo("Use F3 para buscar", mc);
            l.Image = Properties.Resources.Buscar;
            
            return mc;
        }
        public ToolStripMenuItem generarMenuProveedores()
        {
            var mc = crearMenuItem(Properties.Resources.Proveedor, "&Proveedores", "Gestión de Proveedores", null);
            //mc.DropDownItems.Add(crearMenuItem(Properties.Resources.BuscarProveedor,
            //    "&Buscar un Proveedor",
            //    "Busca un Proveedor y lo selecciona para realizar acciones.",
            //    Acciones.BuscarProveedor,
            //    Keys.F2, gManager.gcUsuario.PermisoEnum.Contador
            //    ));

            getDestinatarioLabel("Seleccione un Proveedor", gManager.gcDestinatario.DestinatarioTipo.Proveedor, mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Buscar,
              "&Buscar Proveedor",
              "Busca un Proveedor y lo selecciona para realizar acciones.",
              Acciones.BuscarProveedor,
              Keys.F4, gManager.gcUsuario.PermisoEnum.Contador, false
              ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.VerProveedor,
                "&Ver Proveedor",
                "Muestra las compras, depositos y todo lo relacionado con el Proveedor seleccionado.",
                Acciones.VerProveedor,
                Keys.Control | Keys.O, gManager.gcUsuario.PermisoEnum.Contador,false
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Editar,
                "&Editar Proveedor",
                "Edita los datos del Proveedor seleccionado.",
                Acciones.EditarProveedor,
                Keys.Alt | Keys.O, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.NuevoDestinatario,
                "&Nuevo Proveedor",
                "Crea un nuevo Proveedor.",
                Acciones.CrearProveedor,
                Keys.Alt | Keys.V, gManager.gcUsuario.PermisoEnum.Contador
                ));
            addTitulo("Acciones del Proveedor", mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Comprar,
                "&Comprar a Proveedor",
                "Crea una compra al Proveedor seleccionado.",
                Acciones.Comprar,
                Keys.Control | Keys.F4, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Retornar, "&Retorno a Proveedor", "Crear un retirno de mercaderia al Proveedor seleccionado",
               Acciones.Retornar,
              Keys.None, gManager.gcUsuario.PermisoEnum.Contador
               ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Pedir, "&Pedido a Proveedor", "Crear un pedido al Proveedor seleccionado",
              Acciones.Pedir,
             Keys.Shift | Keys.F4, gManager.gcUsuario.PermisoEnum.Contador
              ));

            mc.DropDownItems.Add(new ToolStripSeparator());
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.DepositoProveedor, "Dep&osito de Proveedor", "Crear un deposito en la cuenta corriente del Proveedor seleccionado",
              Acciones.DepositoProveedor,
             Keys.Alt | Keys.F4, gManager.gcUsuario.PermisoEnum.Contador
              ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.ExtraccProveedor, "E&xtracción de Proveedor", "Crear una extracción de la cuenta corriente del Proveedor seleccionado",
              Acciones.ExtraccionProveedor,
             Keys.None, gManager.gcUsuario.PermisoEnum.Contador
              ));
            mc.DropDownItems.Add(new ToolStripSeparator());
            
            var l = addTitulo("Use F4 para buscar", mc);
            l.Image = Properties.Resources.Buscar;
            return mc;
        }
        public ToolStripMenuItem generarMenuSucursales()
        {
            var mc = crearMenuItem(Properties.Resources.Sucursal, "&Sucursales", "Gestión de Sucursales",null,Keys.None, gManager.gcUsuario.PermisoEnum.Visitante ,true);
            //mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Buscar,
            //    "&Buscar una Sucursal",
            //    "Busca una Sucursal y lo selecciona para realizar acciones.",
            //    Acciones.BuscarSucursal,
            //    Keys.F5, gManager.gcUsuario.PermisoEnum.Contador
            //    ));

            getDestinatarioLabel("Seleccione una Sucursal", gManager.gcDestinatario.DestinatarioTipo.Sucursal, mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Buscar,
             "&Buscar Sucursal",
             "Busca una sucursal y la selecciona para realizar acciones.",
             Acciones.BuscarSucursal,
             Keys.F5, gManager.gcUsuario.PermisoEnum.Contador, false
             ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Ver,
                "&Ver Sucursal",
                "Muestra las entradas, salidas y todo lo relacionado con la Sucursal seleccionada.",
                Acciones.VerSucursal,
                Keys.Control | Keys.A, gManager.gcUsuario.PermisoEnum.Contador,true 
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Editar,
                "&Editar Sucursal",
                "Edita los datos de la Sucursal seleccionada.",
                Acciones.EditarSucursal,
                Keys.Alt | Keys.A, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.NuevoDestinatario,
                "&Nueva Sucursal",
                "Crea una nueva Sucursal.",
                Acciones.CrearSucursal,
                Keys.Alt | Keys.U, gManager.gcUsuario.PermisoEnum.Contador
                ));
            addTitulo("Acciones de la Sucursal", mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Salida,
                "&Enviar a Sucursal",
                "Crea una salida de mercaderia a la Sucursal seleccionada.",
                Acciones.Salida,
                Keys.Control | Keys.F5, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Entrada, "&Entrada desde Sucursal", "Crear una entrada de mercaderia desde la sucursal seleccionada",
              Acciones.Entrada,
             Keys.Shift | Keys.F5, gManager.gcUsuario.PermisoEnum.Contador
              ));
            mc.DropDownItems.Add(new ToolStripSeparator());
            
            var l = addTitulo("Use F5 para buscar", mc);
            l.Image = Properties.Resources.Buscar;
            return mc;
        }
        public ToolStripMenuItem generarMenuResponsables()
        {
            var mc = crearMenuItem(Properties.Resources.Banco , "&Responsables", "Gestión de Responsables", null);
            getDestinatarioLabel("Seleccione un Responsable", gManager.gcDestinatario.DestinatarioTipo.Responsable, mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Buscar,
            "&Buscar responsable",
            "Busca un responsable y lo selecciona para realizar acciones.",
            Acciones.BuscarResponsable,
            Keys.F6, gManager.gcUsuario.PermisoEnum.Contador, false
            ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Ver,
                "&Ver Responsable",
                "Muestra las entradas y salidas de dinero del responsable seleccionado.",
                Acciones.VerResponsable,
                Keys.Control | Keys.L, gManager.gcUsuario.PermisoEnum.Contador,true 
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Editar,
                "&Editar Responsable",
                "Edita los datos del Responsable seleccionado.",
                Acciones.EditarResponsable,
                Keys.Alt | Keys.L, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.NuevoDestinatario,
                "&Nuevo Responsable",
                "Crea un nuevo Responsable.",
                Acciones.CrearResponsable,
                Keys.Alt | Keys.B, gManager.gcUsuario.PermisoEnum.Contador
                ));
            addTitulo("Acciones del Responsable", mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.DepositoResponsable,
                "&Deposito Responsable",
                "Crea una entrada de dinero por parte del Responsable seleccionado.",
                Acciones.DepositoResponsable,
                Keys.Control | Keys.F6, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.ExtraccRespon, "&Extracción de Responsable", "Crea una Extracción de dinero por parte del responsable seleccionado.",
              Acciones.ExtraccionResponsable,
             Keys.Shift | Keys.F6, gManager.gcUsuario.PermisoEnum.Contador
              ));
            mc.DropDownItems.Add(new ToolStripSeparator());
            
            var l = addTitulo("Use F6 para buscar", mc);
            l.Image = Properties.Resources.Buscar;
            
            return mc;
        }
        public ToolStripMenuItem generarMenuFinancieras()
        {
            var mc = crearMenuItem(Properties.Resources.Financiera, "&Financieras", "Gestión de Financieras", null,Keys.None,gManager.gcUsuario.PermisoEnum.Visitante,true);
           getDestinatarioLabel("Seleccione una Financiera", gManager.gcDestinatario.DestinatarioTipo.Financiera, mc);
           mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Buscar,
           "&Buscar Financiera",
           "Busca una financiera y la selecciona para realizar acciones.",
           Acciones.BuscarFinanciera,
           Keys.F7, gManager.gcUsuario.PermisoEnum.Contador, false
           ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Ver,
                "&Ver Financiera",
                "Muestra las ventas, financiaciones con la Financiera seleccionada.",
                Acciones.VerFinanciera,
                Keys.Control | Keys.I, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Editar,
                "&Editar Financiera",
                "Edita los datos de la Financiera seleccionada.",
                Acciones.EditarFinanciera,
                Keys.Alt | Keys.I, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.NuevoDestinatario,
                "&Nueva Financiera",
                "Crea un nueva Financiera.",
                Acciones.CrearFinanciera,
                Keys.Alt | Keys.F, gManager.gcUsuario.PermisoEnum.Contador
                ));
            addTitulo("Acciones de la Financiera", mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Porciento ,
                 "A&gregar cotización de Financiera",
                 "Agrega una cotización para una financiera.",
                 null,
                 Keys.Shift | Keys.F7, gManager.gcUsuario.PermisoEnum.Contador
                 ));
            mc.DropDownItems.Add(new ToolStripSeparator());
            
            var l = addTitulo("Use F7 para buscar", mc);
            l.Image = Properties.Resources.Buscar;
            return mc;
        }
        public ToolStripMenuItem generarMenuCajas()
        {
            var mc = crearMenuItem(Properties.Resources.Caja, "Ca&jas", "Gestión de Cajas", null);
           
            var lbl = addTitulo("Caja "+gManager.CoreManager.Singlenton.CajaActual.Caja, mc);
            gManager.CoreManager.Singlenton.CajaSeleccionada += (c) =>
            {
                
                    lbl.Text ="Caja "+c.Caja;
                    lbl.Font = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);
                    lbl.BackgroundImage = Properties.Resources.FondoGris;
                    lbl.BackgroundImageLayout = ImageLayout.Stretch;
               
            };

            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Ver,
               "&Caja Actual",
               "Muestra el estado de la caja actual.",
               Acciones.VerCaja,
               Keys.F1, gManager.gcUsuario.PermisoEnum.Encargado, false
               ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Caja,
               "Ver Caja&s",
               "Muestra el estado de la caja seleccionada.",
               Acciones.VerCajas,
               Keys.Control  | Keys.F1, gManager.gcUsuario.PermisoEnum.Contador,false
               ));
            //mc.DropDownItems.Add(crearMenuItem(Properties.Resources.CambiarCaja,
            //   "&Cambiar de Caja",
            //   "Busca una Caja y la selecciona para trabajar.",
            //   Acciones.BuscarCaja,
            //   Keys.None, gManager.gcUsuario.PermisoEnum.Contador
            //   ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.TerminarCaja,
                "&Terminar Caja",
                "Termina la caja actual e inicia la siguiente.",
                Acciones.TerminarCaja,
                Keys.Alt | Keys.F1, gManager.gcUsuario.PermisoEnum.Encargado
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Filtro,
               "Ras&trear un Movimiento",
               "Busca un movimiento especifico aplicando un filtro",
               Acciones.BuscarMovimientos,
               Keys.None, gManager.gcUsuario.PermisoEnum.Contador,true 
               ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Estadisticas,
               "Estadísticas",
               "Realiza un informe de estadísticas",
               null,
               Keys.None, gManager.gcUsuario.PermisoEnum.Contador,true
               ));
            var l = addTitulo("Use F1 para ver la caja", mc);
            l.Image = Properties.Resources.Buscar;
            
            return mc;
        }
        public ToolStripMenuItem generarMenuUsuarios()
        {
            var mc = crearMenuItem(Properties.Resources.Usuario, "&Usuarios", "Gestión de Usuarios", null);
            addTitulo(gManager.CoreManager.Singlenton.Usuario.Nombre, mc);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Buscar,
                "&Editar otro Usuario",
                "Busca un Usuario y lo selecciona para Editar.",
                Acciones.BuscarUsuarios,
                Keys.Control | Keys.F8, gManager.gcUsuario.PermisoEnum.Administrador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Seguridad,
                "&Cambiar mi Contraseña",
                "Permite cambiar la contraseña del usuario actual",
                Acciones.EditarMiUsuario,
                Keys.Alt | Keys.F8, gManager.gcUsuario.PermisoEnum.Encargado
                ));
            mc.DropDownItems.Add(new ToolStripSeparator());
           
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.NuevoDestinatario,
               "&Crear Nuevo Usuario",
               "Agrega un nuevo usuario al sistema.",
               Acciones.NuevoUsuario,
               Keys.Shift | Keys.F8, gManager.gcUsuario.PermisoEnum.Administrador
               ));
           
            return mc;
        }
        public ToolStripMenuItem generarMenuAyuda()
        {
            var mc = crearMenuItem(Properties.Resources.Info, "&Ayuda", "Ayuda, Documentación e información del sistema", null);
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Alarma,
               "&Ayuda en linea",
               "Busca un Usuario y lo selecciona para Editar.",
               null,
               Keys.None, gManager.gcUsuario.PermisoEnum.Administrador
               ));
            mc.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.Periodico,
               "Cuenta de Internet",
               "Gestiona el tiempo de uso y la cuenta de internet.",
               gControls.Acciones.VerRegistroKey
               ,
               Keys.None, gManager.gcUsuario.PermisoEnum.Administrador
               ));
            
            return mc;
        }
        public ToolStripMenuItem generarMenuProductos()
        {
            var mc = crearMenuItem(Properties.Resources.Producto, "Pro&ductos", "Gestión de Productos", null);
            var lbl = addTitulo("Seleccione un Producto", mc);
            
            Acciones.CambioProducto += (o, e) =>
            {
                var c = o as gManager.gcObjeto;
                if (c != null )
                {
                    lbl.Text = c.Nombre;
                    lbl.Font = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);
                    lbl.BackgroundImage = Properties.Resources.FondoAzul;
                    lbl.BackgroundImageLayout = ImageLayout.Stretch;
                    //mc.ShowDropDown();
                    colorear(mc);
                }
                else
                {
                    lbl.Text = "Seleccione un Producto";
                    lbl.Font = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold);
                    lbl.BackgroundImage = Properties.Resources.FondoGris;
                    lbl.BackgroundImageLayout = ImageLayout.Stretch;
                }
            };
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Buscar,
               "&Buscar Productos",
               "Busca un producto o servicio.",
               Acciones.BuscarProductos,
               Keys.F2, gManager.gcUsuario.PermisoEnum.Vendedor, false
               ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Ver,
                "&Ver",
                "Muestra todo lo relacionado al producto seleccionado.",
                Acciones.verProducto,
                Keys.Control | Keys.O, gManager.gcUsuario.PermisoEnum.Contador,true
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Editar,
                "&Editar",
                "Edita los datos del Producto seleccionado.",
                Acciones.EditarProducto,
                Keys.Alt | Keys.O, gManager.gcUsuario.PermisoEnum.Contador
                ));
           
            mc.DropDownItems.Add(new ToolStripSeparator());
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Categoria,
                 "Nueva Categoría",
                 "Crea una nueva categoría.",
                 Acciones.NuevaCategoria,
                 Keys.Alt | Keys.C, gManager.gcUsuario.PermisoEnum.Contador
                 ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Producto,
                "Nuevo Producto",
                "Crea una nuevo Producto.",
                Acciones.NuevoProducto,
                Keys.Alt | Keys.P, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Servicio,
                "Nuevo Servicio",
                "Crea una nuevo Servicio.",
                Acciones.NuevoServicio,
                Keys.Alt | Keys.S, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Grupo,
                "Nuevo Grupo",
                "Crea una nuevo Grupo.",
                Acciones.NuevoGrupo,
                Keys.Alt | Keys.G, gManager.gcUsuario.PermisoEnum.Contador
                ));
            mc.DropDownItems.Add(new ToolStripSeparator());
            mc.DropDownItems.Add(crearMenuItem(Properties.Resources.Precio,
                 "List de precios",
                 "Generar la lista de precios.",
                 Acciones.ListaPrecio,
                 Keys.None, gManager.gcUsuario.PermisoEnum.Contador,true
                 ));
            var l = addTitulo("Use F2 para buscar Productos", mc);
            l.Image = Properties.Resources.Buscar;
            return mc;
        }
        public ToolStripMenuItem generarMenuHerramientas()
        {
            var mc = crearMenuItem(gControls.Properties.Resources.Configuracion, "&Herramientas", "Herramientas y opciones de configuración", null);
            mc.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.Configuracion,
              "Leer excel",
              "Crea una lista a partir de un excel.",
              gControls.Acciones.LeerExcel
              ,
              Keys.None, gManager.gcUsuario.PermisoEnum.Contador, true
              ));
            mc.DropDownItems.Add(crearMenuItem(gControls.Properties.Resources.Producto,
            "Obtener Productos...",
            "Obtiene una lista de productos de internet.",
            gControls.Acciones.ObtenerProductos
            ,
            Keys.None, gManager.gcUsuario.PermisoEnum.Administrador, true
            ));
            return mc;
        }
        private void colorear(ToolStripMenuItem m)
        {
            Timer t = new Timer();
            t.Interval = 200;
            uint time=0;
            t.Tick += (o, e) =>
            {
                switch (time)
                {
                    case 0:
                        m.BackgroundImage = Properties.Resources.FondoAzul;
                        break;
                    case 1:
                        m.BackgroundImage = Properties.Resources.FondoGris;
                        break;
                    case 2:
                        m.BackgroundImage = Properties.Resources.FondoAzul;
                        break;
                    case 3:
                        m.BackgroundImage = Properties.Resources.FondoGris;
                        break;
                    default:
                        m.BackgroundImage = null;
                        t.Stop();
                        t.Dispose();
                        break;

                }
                time++;
            };
            t.Start();
        }
      
      

    }
}
