﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Formularios
{
    public partial class fEditorDecimal : Form
    {
        public fEditorDecimal()
        {
            InitializeComponent();
            Icon = Icon.FromHandle(Properties.Resources.Cantidad.GetHicon());
        }
        public decimal Numero
        {
            get
            {
                return editorDecimal1.ValorDecimal;
            }
            set
            {
                editorDecimal1.ValorDecimal = value;
            }
        }
        public void setRango(decimal min, decimal max)
        {
            editorDecimal1.Minimo = min;
            editorDecimal1.Maximo = max;
        }
        public void setDecimales(int dec)
        {
            editorDecimal1.LugaresDecimales = dec;
        }
        
    }
}
