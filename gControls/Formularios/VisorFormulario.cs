﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gControls.Formularios
{
    public class VisorFormulario
    {
        private fEditorContenedor _form = new fEditorContenedor();
        public string Titulo { get { return _form.Text; } set { _form.Text = value; } }
    }
}
