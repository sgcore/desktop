﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Formularios
{
    public partial class fEditorContenedor : Form
    {
        public event EventHandler Aceptado;
        public fEditorContenedor()
        {
            InitializeComponent();
            Icon = Icon.FromHandle(Properties.Resources.Editar.GetHicon());
        }
        Control _con;
        public Control Control
        {
            get{return _con;}
            set
            {
                _con = value;
                panelcontenedor.Controls.Clear();
                if (_con == null) return;
                panelcontenedor.Controls.Add(_con);
                ActiveControl = _con;
                _con.Dock = DockStyle.Fill;
            }
           
        }
        public void setSinGuardar()
        {
            btGuardar.Visible = false;
            btCancelar.Text = "Cerrar";
            btCancelar.Image = Properties.Resources.Aceptar16;
        }
        public void setConGuardar()
        {
            btGuardar.Visible = true;
            btCancelar.Text = "Cancelar";
            btCancelar.Image = Properties.Resources.Cancelar16;
        }

        private void btGuardar_Click(object sender, EventArgs e)
        {
            if (Aceptado != null) Aceptado(null, null);
        }
       
      

        
    }
}
