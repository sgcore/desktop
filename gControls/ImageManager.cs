﻿using gManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace gControls
{
    public class ImageManager
    {
        private static Hashtable _productos = new Hashtable();
        private static Hashtable _destinatarios = new Hashtable();
        private static string path = "Imagenes";
        //public static Image getImageProducto(gcObjeto o)
        //{
        //    string path = Path.Combine("Imagenes", "prod" + o.Id+".png");
        //    return openImage(path);
            
        //}
        //public static Image getImageDestinatario(gcDestinatario o)
        //{
        //    string path = Path.Combine("Imagenes", "dest" + o.Id + ".png");
        //    return openImage(path);

        //}
        private static string getPath(object o)
        {
            if (o is gcObjeto)
                return Path.Combine("Imagenes", "prod" + (o as gcObjeto).Id + ".png");
            else if (o is gcDestinatario)
                return Path.Combine("Imagenes", "dest" + (o as gcDestinatario).Id + ".png");
            return "";
        }
        public static Image getImageGeneric(object o)
        {
            //if (o is gcObjeto)
            //    return getImageProducto(o as gcObjeto);
            //else if (o is gcDestinatario)
            //    return getImageDestinatario(o as gcDestinatario);
            //return Properties.Resources.NoImage;
            return openImage(getPath(o));
        }
        public bool HasImage(object o)
        {
            return File.Exists(getPath(o));
        }
        public static List<gcObjeto> EstablecerImagenString(List<gcObjeto> l)
        {
           
            foreach (gcObjeto o in l)
            {
                o.Imagen64 = html.getProductoImageString(o);
            }
            return l;
        }
        public static void SaveImageProducto(Image img,gcObjeto o)
        {
            saveImage(img, "prod" + o.Id);
        }
        public static void SaveImageDestinatario(Image img, gcDestinatario o)
        {
            saveImage(img, "dest" + o.Id);
        }
        public static void saveImage(Image img,string key)
        {
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            try
            {
                Bitmap bmp=new Bitmap(img);
               bmp.Save(Path.Combine(path, key+".png"), System.Drawing.Imaging.ImageFormat.Png);
            }
            catch (Exception e)
            {
                gLogger.logger.Singlenton.addErrorMsg(e.Message);
            }
            

        }
        public static Image openImage(string path)
        {
            Image ret =  Properties.Resources.NoImage;
            if (!File.Exists(path)) return ret;
            using (var fs = File.OpenRead(path))
            using (var img = Image.FromStream(fs))
            {
                ret = new Bitmap(img,200,250);
            }
            return ret;
        }
    }
}
