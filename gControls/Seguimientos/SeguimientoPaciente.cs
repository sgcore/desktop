﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Seguimientos
{
    public partial class SeguimientoPaciente : UserControl
    {
        public SeguimientoPaciente()
        {
            InitializeComponent();
           

            
        }
        gManager.gcDestinatario _dest;
        public void setDestinatario(gManager.gcDestinatario d)
        {
            _dest = d;
            visorDestinatario1.Destinatario = d;
            visorDestinatario1.VerBotonSeguimiento = false;
            var f = new gManager.Filtros.FiltroTratamientos(d);
            f.addDestinatario(d);
            f.Tipos.Add(gManager.gcTratamiento.TipoTratamiento.Tratamiento);
            buscadorTratamientos1.Destinatario = d;
            buscadorTratamientos1.setFiltro(f);
            buscadorTratamientos1.reiniciar();
        }
        public event System.EventHandler CerrarSeguimiento;
        private void btCerrar_Click(object sender, EventArgs e)
        {
            if (CerrarSeguimiento != null) CerrarSeguimiento(this, e);
        }
       
    
    }
}
