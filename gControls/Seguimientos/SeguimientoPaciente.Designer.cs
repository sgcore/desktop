﻿namespace gControls.Seguimientos
{
    partial class SeguimientoPaciente
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeguimientoPaciente));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btCerrar = new gControls.BotonAccion();
            this.buscadorTratamientos1 = new gControls.Buscadores.BuscadorTratamientos();
            this.visorDestinatario1 = new gControls.Visores.VisorDestinatario();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Tratamiento");
            this.imageList1.Images.SetKeyName(1, "Recordatorio");
            this.imageList1.Images.SetKeyName(2, "Periodico");
            this.imageList1.Images.SetKeyName(3, "Tarea");
            this.imageList1.Images.SetKeyName(4, "Aviso");
            // 
            // btCerrar
            // 
            this.btCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCerrar.FormContainer = null;
            this.btCerrar.GlobalHotKey = false;
            this.btCerrar.HacerInvisibleAlDeshabilitar = false;
            this.btCerrar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCerrar.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btCerrar.Location = new System.Drawing.Point(778, 3);
            this.btCerrar.Name = "btCerrar";
            this.btCerrar.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btCerrar.RegistradoEnForm = false;
            this.btCerrar.RequiereKey = false;
            this.btCerrar.Size = new System.Drawing.Size(18, 17);
            this.btCerrar.TabIndex = 15;
            this.btCerrar.Titulo = "Cerrar Formulario";
            this.btCerrar.ToolTipDescripcion = "Cierra el formulario del movimiento";
            this.btCerrar.UseVisualStyleBackColor = true;
            this.btCerrar.Click += new System.EventHandler(this.btCerrar_Click);
            // 
            // buscadorTratamientos1
            // 
            this.buscadorTratamientos1.CustomDatos = false;
            this.buscadorTratamientos1.Destinatario = null;
            this.buscadorTratamientos1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscadorTratamientos1.Imagen = ((System.Drawing.Image)(resources.GetObject("buscadorTratamientos1.Imagen")));
            this.buscadorTratamientos1.ImagenTitulo = ((System.Drawing.Image)(resources.GetObject("buscadorTratamientos1.ImagenTitulo")));
            this.buscadorTratamientos1.Location = new System.Drawing.Point(0, 82);
            this.buscadorTratamientos1.Name = "buscadorTratamientos1";
            this.buscadorTratamientos1.VerBotonFiltro = false;
            this.buscadorTratamientos1.OcultarSiEstaVacio = false;
            this.buscadorTratamientos1.RequiereKey = false;
            this.buscadorTratamientos1.SeleccionGlobal = false;
            this.buscadorTratamientos1.Size = new System.Drawing.Size(799, 496);
            this.buscadorTratamientos1.TabIndex = 2;
            this.buscadorTratamientos1.Texto = "Buscar";
            this.buscadorTratamientos1.Titulo = "Tratamientos";
            this.buscadorTratamientos1.VerBotonAddAlerta = true;
            this.buscadorTratamientos1.VerBotonAddPeriodico = true;
            this.buscadorTratamientos1.VerBotonAddTarea = false;
            this.buscadorTratamientos1.VerBotonAddTratamiento = true;
            this.buscadorTratamientos1.VerBotonAgrupar = true;
            this.buscadorTratamientos1.VerImagenCard = false;
            // 
            // visorDestinatario1
            // 
            this.visorDestinatario1.BackColor = System.Drawing.Color.Transparent;
            this.visorDestinatario1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.visorDestinatario1.Destinatario = null;
            this.visorDestinatario1.Dock = System.Windows.Forms.DockStyle.Top;
            this.visorDestinatario1.Location = new System.Drawing.Point(0, 0);
            this.visorDestinatario1.Name = "visorDestinatario1";
            this.visorDestinatario1.Size = new System.Drawing.Size(799, 82);
            this.visorDestinatario1.TabIndex = 0;
            this.visorDestinatario1.VerBotonSeguimiento = false;
            // 
            // SeguimientoPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btCerrar);
            this.Controls.Add(this.buscadorTratamientos1);
            this.Controls.Add(this.visorDestinatario1);
            this.Name = "SeguimientoPaciente";
            this.Size = new System.Drawing.Size(799, 578);
            this.ResumeLayout(false);

        }

        #endregion

        private Visores.VisorDestinatario visorDestinatario1;
        private System.Windows.Forms.ImageList imageList1;
        private Buscadores.BuscadorTratamientos buscadorTratamientos1;
        private BotonAccion btCerrar;
    }
}
