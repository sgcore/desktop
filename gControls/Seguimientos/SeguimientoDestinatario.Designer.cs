﻿namespace gControls.Seguimientos
{
    partial class SeguimientoDestinatario
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.acordionContainer1 = new gControls.UI.AcordionContainer();
            this.buConsumoCli = new gControls.Buscadores.BuscadorConsumo();
            this.btCerrar = new gControls.BotonAccion();
            this.visorDestinatario1 = new gControls.Visores.VisorDestinatario();
            this.buPacientes = new gControls.Buscadores.BuscadorDestinatario();
            this.buMovimientosCli = new gControls.Buscadores.BuscadorMovimientos();
            this.buCuentasCorr = new gControls.Buscadores.BuscadorMovCuentaCorr();
            this.SuspendLayout();
            // 
            // acordionContainer1
            // 
            this.acordionContainer1.Alineacion = System.Windows.Forms.DockStyle.Right;
            this.acordionContainer1.AnchoFijo = 954;
            this.acordionContainer1.AutoScroll = true;
            this.acordionContainer1.BotoneraVisible = true;
            this.acordionContainer1.Control = this.buConsumoCli;
            this.acordionContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.acordionContainer1.ExpandedWidth = 954;
            this.acordionContainer1.Frozen = true;
            this.acordionContainer1.Imagen = null;
            this.acordionContainer1.Location = new System.Drawing.Point(0, 80);
            this.acordionContainer1.Name = "acordionContainer1";
            this.acordionContainer1.RequiereKey = false;
            this.acordionContainer1.Size = new System.Drawing.Size(954, 526);
            this.acordionContainer1.TabIndex = 15;
            this.acordionContainer1.Texto = null;
            // 
            // buConsumoCli
            // 
            this.buConsumoCli.CustomDatos = false;
            this.buConsumoCli.Destinatario = null;
            this.buConsumoCli.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buConsumoCli.Imagen = global::gControls.Properties.Resources.Seguimiento;
            this.buConsumoCli.ImagenTitulo = global::gControls.Properties.Resources.Seguimiento;
            this.buConsumoCli.Location = new System.Drawing.Point(0, 0);
            this.buConsumoCli.Name = "buConsumoCli";
            this.buConsumoCli.OcultarSiEstaVacio = true;
            this.buConsumoCli.RequiereKey = false;
            this.buConsumoCli.SeleccionGlobal = true;
            this.buConsumoCli.Size = new System.Drawing.Size(954, 495);
            this.buConsumoCli.TabIndex = 11;
            this.buConsumoCli.Tag = 0;
            this.buConsumoCli.Texto = "Consumo";
            this.buConsumoCli.Titulo = "Consumo";
            this.buConsumoCli.VerImagenCard = false;
            this.buConsumoCli.Visible = false;
            // 
            // btCerrar
            // 
            this.btCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCerrar.FormContainer = null;
            this.btCerrar.GlobalHotKey = false;
            this.btCerrar.HacerInvisibleAlDeshabilitar = false;
            this.btCerrar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCerrar.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btCerrar.Location = new System.Drawing.Point(936, 0);
            this.btCerrar.Name = "btCerrar";
            this.btCerrar.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btCerrar.RegistradoEnForm = false;
            this.btCerrar.RequiereKey = false;
            this.btCerrar.Size = new System.Drawing.Size(18, 17);
            this.btCerrar.TabIndex = 14;
            this.btCerrar.Titulo = "Cerrar Formulario";
            this.btCerrar.ToolTipDescripcion = "Cierra el formulario del movimiento";
            this.btCerrar.UseVisualStyleBackColor = true;
            this.btCerrar.Click += new System.EventHandler(this.btCerrar_Click);
            // 
            // visorDestinatario1
            // 
            this.visorDestinatario1.BackColor = System.Drawing.Color.Transparent;
            this.visorDestinatario1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.visorDestinatario1.Destinatario = null;
            this.visorDestinatario1.Dock = System.Windows.Forms.DockStyle.Top;
            this.visorDestinatario1.Location = new System.Drawing.Point(0, 0);
            this.visorDestinatario1.Name = "visorDestinatario1";
            this.visorDestinatario1.Size = new System.Drawing.Size(954, 80);
            this.visorDestinatario1.TabIndex = 0;
            this.visorDestinatario1.VerBotonSeguimiento = true;
            // 
            // buPacientes
            // 
            this.buPacientes.CustomDatos = false;
            this.buPacientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buPacientes.Imagen = global::gControls.Properties.Resources.Paciente;
            this.buPacientes.ImagenTitulo = global::gControls.Properties.Resources.Paciente;
            this.buPacientes.Location = new System.Drawing.Point(0, 0);
            this.buPacientes.Name = "buPacientes";
            this.buPacientes.OcultarSiEstaVacio = true;
            this.buPacientes.RequiereKey = false;
            this.buPacientes.SeleccionGlobal = true;
            this.buPacientes.Size = new System.Drawing.Size(784, 324);
            this.buPacientes.TabIndex = 8;
            this.buPacientes.Texto = "Pacientes";
            this.buPacientes.Titulo = "Pacientes de los cuales es propietario";
            this.buPacientes.VerImagenCard = false;
            this.buPacientes.Visible = false;
            // 
            // buMovimientosCli
            // 
            this.buMovimientosCli.CustomDatos = false;
            this.buMovimientosCli.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buMovimientosCli.Imagen = global::gControls.Properties.Resources.Venta;
            this.buMovimientosCli.ImagenTitulo = global::gControls.Properties.Resources.Venta;
            this.buMovimientosCli.Location = new System.Drawing.Point(0, 0);
            this.buMovimientosCli.MovimientosCajaACtual = false;
            this.buMovimientosCli.Name = "buMovimientosCli";
            this.buMovimientosCli.VerBotonFiltro = false;
            this.buMovimientosCli.OcultarSiEstaVacio = true;
            this.buMovimientosCli.RequiereKey = false;
            this.buMovimientosCli.SeleccionGlobal = true;
            this.buMovimientosCli.Size = new System.Drawing.Size(784, 324);
            this.buMovimientosCli.TabIndex = 10;
            this.buMovimientosCli.Texto = "Movimientos";
            this.buMovimientosCli.Titulo = "Ventas, depositos, etc";
            this.buMovimientosCli.VerImagenCard = false;
            // 
            // buCuentasCorr
            // 
            this.buCuentasCorr.CustomDatos = false;
            this.buCuentasCorr.Destinatario = null;
            this.buCuentasCorr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buCuentasCorr.Imagen = global::gControls.Properties.Resources.Cheque;
            this.buCuentasCorr.ImagenTitulo = global::gControls.Properties.Resources.Cheque;
            this.buCuentasCorr.Location = new System.Drawing.Point(0, 0);
            this.buCuentasCorr.Name = "buCuentasCorr";
            this.buCuentasCorr.OcultarSiEstaVacio = true;
            this.buCuentasCorr.RequiereKey = false;
            this.buCuentasCorr.SeleccionGlobal = true;
            this.buCuentasCorr.Size = new System.Drawing.Size(784, 466);
            this.buCuentasCorr.TabIndex = 9;
            this.buCuentasCorr.Texto = "Cuenta Corriente";
            this.buCuentasCorr.Titulo = "Movimientos de Cuenta Corriente";
            this.buCuentasCorr.VerImagenCard = false;
            this.buCuentasCorr.Visible = false;
            // 
            // SeguimientoDestinatario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.acordionContainer1);
            this.Controls.Add(this.btCerrar);
            this.Controls.Add(this.visorDestinatario1);
            this.Name = "SeguimientoDestinatario";
            this.Size = new System.Drawing.Size(954, 606);
            this.ResumeLayout(false);

        }

        #endregion

        private Visores.VisorDestinatario visorDestinatario1;
        private Buscadores.BuscadorDestinatario buPacientes;
        private Buscadores.BuscadorMovCuentaCorr buCuentasCorr;
        private Buscadores.BuscadorMovimientos buMovimientosCli;
        private Buscadores.BuscadorConsumo buConsumoCli;
        private BotonAccion btCerrar;
        private UI.AcordionContainer acordionContainer1;
    }
}
