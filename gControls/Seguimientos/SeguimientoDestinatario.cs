﻿using System.Windows.Forms;
using gManager;
using System.Linq;
namespace gControls.Seguimientos
{
    public partial class SeguimientoDestinatario : UserControl
    {
        public event System.EventHandler CerrarSeguimiento;
        public SeguimientoDestinatario()
        {
            InitializeComponent();
            gManager.CoreManager.Singlenton.DestinatarioSeleccionado += Singlenton_DestinatarioSeleccionado;
            visorDestinatario1.VerBotonSeguimiento = false;
           // buPacientes.MostrarBotonesHandle=buPacientes.ConfigBotonesComunes;
            buPacientes.ItemSeleccionado += cerrarSegui;
            buPacientes.OcultarSiEstaVacio = true;
            acordionContainer1.addToContainer(buMovimientosCli);
            acordionContainer1.addToContainer(buConsumoCli);
             acordionContainer1.addToContainer(buPacientes);
           acordionContainer1.addToContainer(buCuentasCorr);
            acordionContainer1.recargarBarra();
            
            
        }
        private void cerrarSegui(object o, System.EventArgs e)
        {
            if (CerrarSeguimiento != null) CerrarSeguimiento(this, e);
        }
        void Singlenton_DestinatarioSeleccionado(gcDestinatario d)
        {
            if (RecibirGlobal)
                setDestinatario(d);
        }
        public void setDestinatario(gcDestinatario d)
        {
            visorDestinatario1.Destinatario = d;
            buMovimientosCli.HideFirsColumn();
           // buPacientes.ParentDestinatario = d;
            
            buCuentasCorr.Destinatario = d;
            buCuentasCorr.iniciar();
            buConsumoCli.Destinatario = d;
            buConsumoCli.iniciar();
            
            if (d != null)
            {
                var f=new gManager.Filtros.FiltroMovimiento();
                f.Destinatario = d;
                f.setTodasLasCajas();
                buMovimientosCli.setFiltro(f);

                var f1 = new gManager.Filtros.FiltroDestinatario();
                f1.Parent = d.Id;
                f1.Tipos.Add(gcDestinatario.DestinatarioTipo.Paciente);
                buPacientes.setFiltro(f1);

                
            }
            acordionContainer1.recargarBarra();
            
        }
       
        public bool RecibirGlobal { get; set; }

        private void btCerrar_Click(object sender, System.EventArgs e)
        {
            if (CerrarSeguimiento != null) CerrarSeguimiento(this, e);
        }

    

       
    }
}
