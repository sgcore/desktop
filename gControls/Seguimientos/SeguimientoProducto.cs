﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using ZedGraph;
using gManager;

namespace gControls.Seguimientos
{
    public partial class SeguimientoProducto : UserControl
    {
        public event System.EventHandler CerrarSeguimiento;
        private gManager.Resumenes.SeguimientoProducto _obj;
        public SeguimientoProducto()
        {
            InitializeComponent();

        }
        private void cerrarSegui(object o, System.EventArgs e)
        {
            CerrarSeguimiento?.Invoke(this, e);
        }
        public void setProductoSeguimiento(gManager.Resumenes.SeguimientoProducto sp)
        {
            _obj = sp;
            if (_obj != null)
            {
                buscadorSeguimientoProducto1.setProductoSeguimiento(sp);
                configChart();
                vVendidos.setMonto(sp.Vendidos,"");
                vMontoVendidos.setMonto(sp.MontoVendidos);
                vComprados.setMonto(sp.Comprados, "");
                vMontoComprados.setMonto(sp.MontoComprados);
                //retornos
                vRetornados.setMonto(sp.Retornados, "");
                vMontoRetornados.setMonto(sp.MontoRetornados);
                //devueltos
                vDevueltos.setMonto(sp.Devueltos, "");
                vMontoDevueltos.setMonto(sp.MontoDevueltos);
                //entradas
                vEntradas.setMonto(sp.Entrados, "");
                //Salidas
                vSalidas.setMonto(sp.Salidos, "");

                vStock.setMonto(sp.stock, "");
                vGanancia.setMonto(sp.MontoVendidos + sp.MontoRetornados - sp.MontoComprados - sp.MontoDevueltos);
            }
          

        }
        void configChart()
        {


            z1.GraphPane.CurveList.Clear();
            z1.GraphPane.GraphObjList.Clear();


            var myPane = z1.GraphPane;
            myPane.XAxis.Title.Text = "Fecha";
            myPane.XAxis.Type = AxisType.Date;
            myPane.XAxis.Scale.Format = "dd-MMM-yy";
            z1.IsShowPointValues = true;
            myPane.Title.Text = "Movimiento de Stock";
            myPane.YAxis.Title.Text = "Stock";
            myPane.Y2Axis.Title.Text = "este no se que es";
           
            int i;
            List<ProductoResumen> l = _obj.Resumenes;
            double[] x = new double[l.Count];
            double [] y = new double[l.Count];
            for (i = 0; i < l.Count; i++)
            {
                x[i] = new ZedGraph.XDate (l[i].Fecha) ;
                y[i] =  (double)l[i].Saldo;
            }
           myPane.AddCurve("stock", x, y, Color.Red, SymbolType.Star);
           z1.ZoomOutAll(myPane);
            z1.AxisChange();
            z1.Invalidate();
        }
        private void CreateGraph(ZedGraphControl zg1, List<gManager.gcMovimiento> l)
        {
            // Get a reference to the GraphPane
            GraphPane myPane = zg1.GraphPane;

            // Set the titles
            myPane.Title.Text = "Ventas ";
            myPane.XAxis.Title.Text = "Fecha";
         

            // Make up some random data points
           // double x , y;
            PointPairList comp = new PointPairList();
            PointPairList vent = new PointPairList();
            PointPairList devo = new PointPairList();
            PointPairList pres = new PointPairList();
            PointPairList ret = new PointPairList();
            PointPairList sal = new PointPairList();
            PointPairList entr = new PointPairList();
            //ordenar
            l.Sort(delegate(gcMovimiento a, gcMovimiento b)
            {
                return a.Caja.CompareTo(b.Caja);
            });
            //agrupar por cajas
            SortedDictionary<double,List<gcMovimiento>> _t = new  SortedDictionary<double,List<gcMovimiento>>();
            foreach (gcMovimiento mov in l)
            {
                if (_t.ContainsKey((double)mov.Caja))
                {
                    (_t[(double)mov.Caja] as List<gcMovimiento>).Add(mov);
                }
                else
                {
                    var nl = new List<gcMovimiento>();
                    nl.Add(mov);
                    _t.Add((double)mov.Caja,nl);
                }
            }
            var _tt=_t.GetEnumerator();
            while(_tt.MoveNext())
            {
               var list=_tt.Current.Value as List<gcMovimiento>;
                double cont = 0;
                 PointPairList gene = null;
                foreach (gcMovimiento m in list)
                {
                    
                    foreach (gcObjetoMovido om in m.ObjetosMovidos)
                    {
                        if (om.Objeto == _obj.Producto)
                        {
                            cont += double.Parse(om.Cantidad.ToString());
                        }

                    }
                    if (cont > 0)
                    {

                        //x = (double)new XDate(m.Fecha);
                        //y = cont;
                       
                        switch (m.Tipo)
                        {
                            case gcMovimiento.TipoMovEnum.Compra:
                                gene = comp;
                                break;
                            case gcMovimiento.TipoMovEnum.Venta:
                                gene = vent;
                                break;
                            case gcMovimiento.TipoMovEnum.Devolucion:
                                gene = devo;
                                break;
                            case gcMovimiento.TipoMovEnum.Presupuesto:
                                gene = pres;
                                break;
                            case gcMovimiento.TipoMovEnum.Retorno:
                                gene = ret;
                                break;
                            case gcMovimiento.TipoMovEnum.Salida:
                                gene = sal;
                                break;
                            case gcMovimiento.TipoMovEnum.Entrada:
                                gene = entr;
                                break;
                            default:
                                continue;

                        }
                        
                    }
                }
                if (gene != null)
                {
                    gene.Add((double)_tt.Current.Key, cont);
                }
            }

            
            

            // Generate a red curve with diamond
            // symbols, and "My Curve" in the legend
            myPane.CurveList.Clear();
            //CurveItem myCurve = myPane.AddCurve("Cantidad ",
            //      list, Color.Red, SymbolType.Diamond);
            myPane.AddCurve("Ventas", vent, Color.Green);
            myPane.AddCurve("Compras", comp, Color.YellowGreen);
            myPane.AddCurve("Devoluciones", devo, Color.Salmon);
            myPane.AddCurve("Presupuestos", pres, Color.Blue);
            myPane.AddCurve("Retornos", ret, Color.Red);
            myPane.AddCurve("Entradas", entr, Color.Purple);
            myPane.AddCurve("Salidas", sal, Color.Violet);

            // Set the XAxis to date type
            myPane.XAxis.Type = AxisType.Linear;

            // Tell ZedGraph to refigure the axes since the data 
            // have changed
            zg1.AxisChange();
        }
    }
}
