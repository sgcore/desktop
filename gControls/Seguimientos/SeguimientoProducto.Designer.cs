﻿namespace gControls.Seguimientos
{
    partial class SeguimientoProducto
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeguimientoProducto));
            this.z1 = new ZedGraph.ZedGraphControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.vStock = new gControls.Visores.VisorMonto();
            this.vGanancia = new gControls.Visores.VisorMonto();
            this.vSalidas = new gControls.Visores.VisorMonto();
            this.vEntradas = new gControls.Visores.VisorMonto();
            this.vMontoDevueltos = new gControls.Visores.VisorMonto();
            this.vDevueltos = new gControls.Visores.VisorMonto();
            this.vMontoRetornados = new gControls.Visores.VisorMonto();
            this.vRetornados = new gControls.Visores.VisorMonto();
            this.vMontoComprados = new gControls.Visores.VisorMonto();
            this.vComprados = new gControls.Visores.VisorMonto();
            this.vMontoVendidos = new gControls.Visores.VisorMonto();
            this.vVendidos = new gControls.Visores.VisorMonto();
            this.buscadorSeguimientoProducto1 = new gControls.Buscadores.BuscadorSeguimientoProducto();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // z1
            // 
            this.z1.Dock = System.Windows.Forms.DockStyle.Top;
            this.z1.IsShowPointValues = true;
            this.z1.Location = new System.Drawing.Point(0, 0);
            this.z1.Name = "z1";
            this.z1.ScrollGrace = 0D;
            this.z1.ScrollMaxX = 0D;
            this.z1.ScrollMaxY = 0D;
            this.z1.ScrollMaxY2 = 0D;
            this.z1.ScrollMinX = 0D;
            this.z1.ScrollMinY = 0D;
            this.z1.ScrollMinY2 = 0D;
            this.z1.Size = new System.Drawing.Size(769, 251);
            this.z1.TabIndex = 1;
            this.z1.UseExtendedPrintDialog = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.vStock);
            this.panel1.Controls.Add(this.vGanancia);
            this.panel1.Controls.Add(this.vSalidas);
            this.panel1.Controls.Add(this.vEntradas);
            this.panel1.Controls.Add(this.vMontoDevueltos);
            this.panel1.Controls.Add(this.vDevueltos);
            this.panel1.Controls.Add(this.vMontoRetornados);
            this.panel1.Controls.Add(this.vRetornados);
            this.panel1.Controls.Add(this.vMontoComprados);
            this.panel1.Controls.Add(this.vComprados);
            this.panel1.Controls.Add(this.vMontoVendidos);
            this.panel1.Controls.Add(this.vVendidos);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(595, 251);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(174, 405);
            this.panel1.TabIndex = 3;
            // 
            // vStock
            // 
            this.vStock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vStock.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vStock.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vStock.ColorFondoTitulo = System.Drawing.Color.MediumSeaGreen;
            this.vStock.Dock = System.Windows.Forms.DockStyle.Top;
            this.vStock.ImageBox = ((System.Drawing.Image)(resources.GetObject("vStock.ImageBox")));
            this.vStock.ImageButton = null;
            this.vStock.ImagenFondoCero = null;
            this.vStock.ImagenFondoNegativo = null;
            this.vStock.ImagenFondoPositivo = null;
            this.vStock.Location = new System.Drawing.Point(0, 440);
            this.vStock.MinimumSize = new System.Drawing.Size(100, 32);
            this.vStock.Monto = "$0.000";
            this.vStock.Name = "vStock";
            this.vStock.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vStock.Size = new System.Drawing.Size(157, 40);
            this.vStock.TabIndex = 11;
            this.vStock.Titulo = "Stock";
            this.vStock.ToolTipDescripcion = "";
            this.vStock.UseColorMontos = false;
            this.vStock.VerBoton = false;
            this.vStock.VisibleImageBox = false;
            // 
            // vGanancia
            // 
            this.vGanancia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vGanancia.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vGanancia.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vGanancia.ColorFondoTitulo = System.Drawing.Color.LimeGreen;
            this.vGanancia.Dock = System.Windows.Forms.DockStyle.Top;
            this.vGanancia.ImageBox = ((System.Drawing.Image)(resources.GetObject("vGanancia.ImageBox")));
            this.vGanancia.ImageButton = null;
            this.vGanancia.ImagenFondoCero = null;
            this.vGanancia.ImagenFondoNegativo = null;
            this.vGanancia.ImagenFondoPositivo = null;
            this.vGanancia.Location = new System.Drawing.Point(0, 400);
            this.vGanancia.MinimumSize = new System.Drawing.Size(100, 32);
            this.vGanancia.Monto = "$0.000";
            this.vGanancia.Name = "vGanancia";
            this.vGanancia.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vGanancia.Size = new System.Drawing.Size(157, 40);
            this.vGanancia.TabIndex = 10;
            this.vGanancia.Titulo = "Ganancia";
            this.vGanancia.ToolTipDescripcion = "";
            this.vGanancia.UseColorMontos = false;
            this.vGanancia.VerBoton = false;
            this.vGanancia.VisibleImageBox = false;
            // 
            // vSalidas
            // 
            this.vSalidas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vSalidas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vSalidas.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vSalidas.ColorFondoTitulo = System.Drawing.Color.DarkGoldenrod;
            this.vSalidas.Dock = System.Windows.Forms.DockStyle.Top;
            this.vSalidas.ImageBox = ((System.Drawing.Image)(resources.GetObject("vSalidas.ImageBox")));
            this.vSalidas.ImageButton = null;
            this.vSalidas.ImagenFondoCero = null;
            this.vSalidas.ImagenFondoNegativo = null;
            this.vSalidas.ImagenFondoPositivo = null;
            this.vSalidas.Location = new System.Drawing.Point(0, 360);
            this.vSalidas.MinimumSize = new System.Drawing.Size(100, 32);
            this.vSalidas.Monto = "$0.000";
            this.vSalidas.Name = "vSalidas";
            this.vSalidas.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vSalidas.Size = new System.Drawing.Size(157, 40);
            this.vSalidas.TabIndex = 9;
            this.vSalidas.Titulo = "Salidas";
            this.vSalidas.ToolTipDescripcion = "";
            this.vSalidas.UseColorMontos = false;
            this.vSalidas.VerBoton = false;
            this.vSalidas.VisibleImageBox = false;
            // 
            // vEntradas
            // 
            this.vEntradas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vEntradas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vEntradas.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vEntradas.ColorFondoTitulo = System.Drawing.Color.Goldenrod;
            this.vEntradas.Dock = System.Windows.Forms.DockStyle.Top;
            this.vEntradas.ImageBox = ((System.Drawing.Image)(resources.GetObject("vEntradas.ImageBox")));
            this.vEntradas.ImageButton = null;
            this.vEntradas.ImagenFondoCero = null;
            this.vEntradas.ImagenFondoNegativo = null;
            this.vEntradas.ImagenFondoPositivo = null;
            this.vEntradas.Location = new System.Drawing.Point(0, 320);
            this.vEntradas.MinimumSize = new System.Drawing.Size(100, 32);
            this.vEntradas.Monto = "$0.000";
            this.vEntradas.Name = "vEntradas";
            this.vEntradas.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vEntradas.Size = new System.Drawing.Size(157, 40);
            this.vEntradas.TabIndex = 8;
            this.vEntradas.Titulo = "Entradas";
            this.vEntradas.ToolTipDescripcion = "";
            this.vEntradas.UseColorMontos = false;
            this.vEntradas.VerBoton = false;
            this.vEntradas.VisibleImageBox = false;
            // 
            // vMontoDevueltos
            // 
            this.vMontoDevueltos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vMontoDevueltos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vMontoDevueltos.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vMontoDevueltos.ColorFondoTitulo = System.Drawing.Color.MidnightBlue;
            this.vMontoDevueltos.Dock = System.Windows.Forms.DockStyle.Top;
            this.vMontoDevueltos.ImageBox = ((System.Drawing.Image)(resources.GetObject("vMontoDevueltos.ImageBox")));
            this.vMontoDevueltos.ImageButton = null;
            this.vMontoDevueltos.ImagenFondoCero = null;
            this.vMontoDevueltos.ImagenFondoNegativo = null;
            this.vMontoDevueltos.ImagenFondoPositivo = null;
            this.vMontoDevueltos.Location = new System.Drawing.Point(0, 280);
            this.vMontoDevueltos.MinimumSize = new System.Drawing.Size(100, 32);
            this.vMontoDevueltos.Monto = "$0.000";
            this.vMontoDevueltos.Name = "vMontoDevueltos";
            this.vMontoDevueltos.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vMontoDevueltos.Size = new System.Drawing.Size(157, 40);
            this.vMontoDevueltos.TabIndex = 7;
            this.vMontoDevueltos.Titulo = "Monto Devuelto";
            this.vMontoDevueltos.ToolTipDescripcion = "";
            this.vMontoDevueltos.UseColorMontos = false;
            this.vMontoDevueltos.VerBoton = false;
            this.vMontoDevueltos.VisibleImageBox = false;
            // 
            // vDevueltos
            // 
            this.vDevueltos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vDevueltos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vDevueltos.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vDevueltos.ColorFondoTitulo = System.Drawing.Color.MidnightBlue;
            this.vDevueltos.Dock = System.Windows.Forms.DockStyle.Top;
            this.vDevueltos.ImageBox = ((System.Drawing.Image)(resources.GetObject("vDevueltos.ImageBox")));
            this.vDevueltos.ImageButton = null;
            this.vDevueltos.ImagenFondoCero = null;
            this.vDevueltos.ImagenFondoNegativo = null;
            this.vDevueltos.ImagenFondoPositivo = null;
            this.vDevueltos.Location = new System.Drawing.Point(0, 240);
            this.vDevueltos.MinimumSize = new System.Drawing.Size(100, 32);
            this.vDevueltos.Monto = "$0.000";
            this.vDevueltos.Name = "vDevueltos";
            this.vDevueltos.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vDevueltos.Size = new System.Drawing.Size(157, 40);
            this.vDevueltos.TabIndex = 6;
            this.vDevueltos.Titulo = "Cantidad Devueltos";
            this.vDevueltos.ToolTipDescripcion = "";
            this.vDevueltos.UseColorMontos = false;
            this.vDevueltos.VerBoton = false;
            this.vDevueltos.VisibleImageBox = false;
            // 
            // vMontoRetornados
            // 
            this.vMontoRetornados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vMontoRetornados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vMontoRetornados.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vMontoRetornados.ColorFondoTitulo = System.Drawing.Color.DarkSlateBlue;
            this.vMontoRetornados.Dock = System.Windows.Forms.DockStyle.Top;
            this.vMontoRetornados.ImageBox = ((System.Drawing.Image)(resources.GetObject("vMontoRetornados.ImageBox")));
            this.vMontoRetornados.ImageButton = null;
            this.vMontoRetornados.ImagenFondoCero = null;
            this.vMontoRetornados.ImagenFondoNegativo = null;
            this.vMontoRetornados.ImagenFondoPositivo = null;
            this.vMontoRetornados.Location = new System.Drawing.Point(0, 200);
            this.vMontoRetornados.MinimumSize = new System.Drawing.Size(100, 32);
            this.vMontoRetornados.Monto = "$0.000";
            this.vMontoRetornados.Name = "vMontoRetornados";
            this.vMontoRetornados.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vMontoRetornados.Size = new System.Drawing.Size(157, 40);
            this.vMontoRetornados.TabIndex = 5;
            this.vMontoRetornados.Titulo = "Monto Retornado";
            this.vMontoRetornados.ToolTipDescripcion = "";
            this.vMontoRetornados.UseColorMontos = false;
            this.vMontoRetornados.VerBoton = false;
            this.vMontoRetornados.VisibleImageBox = false;
            // 
            // vRetornados
            // 
            this.vRetornados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vRetornados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vRetornados.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vRetornados.ColorFondoTitulo = System.Drawing.Color.DarkSlateBlue;
            this.vRetornados.Dock = System.Windows.Forms.DockStyle.Top;
            this.vRetornados.ImageBox = ((System.Drawing.Image)(resources.GetObject("vRetornados.ImageBox")));
            this.vRetornados.ImageButton = null;
            this.vRetornados.ImagenFondoCero = null;
            this.vRetornados.ImagenFondoNegativo = null;
            this.vRetornados.ImagenFondoPositivo = null;
            this.vRetornados.Location = new System.Drawing.Point(0, 160);
            this.vRetornados.MinimumSize = new System.Drawing.Size(100, 32);
            this.vRetornados.Monto = "$0.000";
            this.vRetornados.Name = "vRetornados";
            this.vRetornados.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vRetornados.Size = new System.Drawing.Size(157, 40);
            this.vRetornados.TabIndex = 4;
            this.vRetornados.Titulo = "Cantidad Retornados";
            this.vRetornados.ToolTipDescripcion = "";
            this.vRetornados.UseColorMontos = false;
            this.vRetornados.VerBoton = false;
            this.vRetornados.VisibleImageBox = false;
            // 
            // vMontoComprados
            // 
            this.vMontoComprados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vMontoComprados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vMontoComprados.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vMontoComprados.ColorFondoTitulo = System.Drawing.Color.SlateBlue;
            this.vMontoComprados.Dock = System.Windows.Forms.DockStyle.Top;
            this.vMontoComprados.ImageBox = ((System.Drawing.Image)(resources.GetObject("vMontoComprados.ImageBox")));
            this.vMontoComprados.ImageButton = null;
            this.vMontoComprados.ImagenFondoCero = null;
            this.vMontoComprados.ImagenFondoNegativo = null;
            this.vMontoComprados.ImagenFondoPositivo = null;
            this.vMontoComprados.Location = new System.Drawing.Point(0, 120);
            this.vMontoComprados.MinimumSize = new System.Drawing.Size(100, 32);
            this.vMontoComprados.Monto = "$0.000";
            this.vMontoComprados.Name = "vMontoComprados";
            this.vMontoComprados.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vMontoComprados.Size = new System.Drawing.Size(157, 40);
            this.vMontoComprados.TabIndex = 3;
            this.vMontoComprados.Titulo = "Monto Comprado";
            this.vMontoComprados.ToolTipDescripcion = "";
            this.vMontoComprados.UseColorMontos = false;
            this.vMontoComprados.VerBoton = false;
            this.vMontoComprados.VisibleImageBox = false;
            // 
            // vComprados
            // 
            this.vComprados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vComprados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vComprados.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vComprados.ColorFondoTitulo = System.Drawing.Color.SlateBlue;
            this.vComprados.Dock = System.Windows.Forms.DockStyle.Top;
            this.vComprados.ImageBox = ((System.Drawing.Image)(resources.GetObject("vComprados.ImageBox")));
            this.vComprados.ImageButton = null;
            this.vComprados.ImagenFondoCero = null;
            this.vComprados.ImagenFondoNegativo = null;
            this.vComprados.ImagenFondoPositivo = null;
            this.vComprados.Location = new System.Drawing.Point(0, 80);
            this.vComprados.MinimumSize = new System.Drawing.Size(100, 32);
            this.vComprados.Monto = "$0.000";
            this.vComprados.Name = "vComprados";
            this.vComprados.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vComprados.Size = new System.Drawing.Size(157, 40);
            this.vComprados.TabIndex = 2;
            this.vComprados.Titulo = "Cantidad Comprada";
            this.vComprados.ToolTipDescripcion = "";
            this.vComprados.UseColorMontos = false;
            this.vComprados.VerBoton = false;
            this.vComprados.VisibleImageBox = false;
            // 
            // vMontoVendidos
            // 
            this.vMontoVendidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vMontoVendidos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vMontoVendidos.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vMontoVendidos.ColorFondoTitulo = System.Drawing.Color.RoyalBlue;
            this.vMontoVendidos.Dock = System.Windows.Forms.DockStyle.Top;
            this.vMontoVendidos.ImageBox = ((System.Drawing.Image)(resources.GetObject("vMontoVendidos.ImageBox")));
            this.vMontoVendidos.ImageButton = null;
            this.vMontoVendidos.ImagenFondoCero = null;
            this.vMontoVendidos.ImagenFondoNegativo = null;
            this.vMontoVendidos.ImagenFondoPositivo = null;
            this.vMontoVendidos.Location = new System.Drawing.Point(0, 40);
            this.vMontoVendidos.MinimumSize = new System.Drawing.Size(100, 32);
            this.vMontoVendidos.Monto = "$0.000";
            this.vMontoVendidos.Name = "vMontoVendidos";
            this.vMontoVendidos.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vMontoVendidos.Size = new System.Drawing.Size(157, 40);
            this.vMontoVendidos.TabIndex = 1;
            this.vMontoVendidos.Titulo = "Monto Vendido";
            this.vMontoVendidos.ToolTipDescripcion = "";
            this.vMontoVendidos.UseColorMontos = false;
            this.vMontoVendidos.VerBoton = false;
            this.vMontoVendidos.VisibleImageBox = false;
            // 
            // vVendidos
            // 
            this.vVendidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vVendidos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.vVendidos.BotonAlineacion = System.Windows.Forms.DockStyle.Left;
            this.vVendidos.ColorFondoTitulo = System.Drawing.Color.RoyalBlue;
            this.vVendidos.Dock = System.Windows.Forms.DockStyle.Top;
            this.vVendidos.ImageBox = ((System.Drawing.Image)(resources.GetObject("vVendidos.ImageBox")));
            this.vVendidos.ImageButton = null;
            this.vVendidos.ImagenFondoCero = null;
            this.vVendidos.ImagenFondoNegativo = null;
            this.vVendidos.ImagenFondoPositivo = null;
            this.vVendidos.Location = new System.Drawing.Point(0, 0);
            this.vVendidos.MinimumSize = new System.Drawing.Size(100, 32);
            this.vVendidos.Monto = "$0.000";
            this.vVendidos.Name = "vVendidos";
            this.vVendidos.PermisoBoton = gManager.gcUsuario.PermisoEnum.Visitante;
            this.vVendidos.Size = new System.Drawing.Size(157, 40);
            this.vVendidos.TabIndex = 0;
            this.vVendidos.Titulo = "Cantidad Vendida";
            this.vVendidos.ToolTipDescripcion = "";
            this.vVendidos.UseColorMontos = false;
            this.vVendidos.VerBoton = false;
            this.vVendidos.VisibleImageBox = false;
            // 
            // buscadorSeguimientoProducto1
            // 
            this.buscadorSeguimientoProducto1.CustomDatos = false;
            this.buscadorSeguimientoProducto1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscadorSeguimientoProducto1.Imagen = ((System.Drawing.Image)(resources.GetObject("buscadorSeguimientoProducto1.Imagen")));
            this.buscadorSeguimientoProducto1.ImagenTitulo = ((System.Drawing.Image)(resources.GetObject("buscadorSeguimientoProducto1.ImagenTitulo")));
            this.buscadorSeguimientoProducto1.Location = new System.Drawing.Point(0, 251);
            this.buscadorSeguimientoProducto1.Name = "buscadorSeguimientoProducto1";
            this.buscadorSeguimientoProducto1.VerBotonFiltro = false;
            this.buscadorSeguimientoProducto1.OcultarSiEstaVacio = false;
            this.buscadorSeguimientoProducto1.RequiereKey = false;
            this.buscadorSeguimientoProducto1.SeleccionGlobal = true;
            this.buscadorSeguimientoProducto1.Size = new System.Drawing.Size(595, 405);
            this.buscadorSeguimientoProducto1.TabIndex = 2;
            this.buscadorSeguimientoProducto1.Texto = "Buscar";
            this.buscadorSeguimientoProducto1.Titulo = "Titulo";
            this.buscadorSeguimientoProducto1.VerImagenCard = false;
            // 
            // SeguimientoProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buscadorSeguimientoProducto1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.z1);
            this.Name = "SeguimientoProducto";
            this.Size = new System.Drawing.Size(769, 656);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl z1;
        private Buscadores.BuscadorSeguimientoProducto buscadorSeguimientoProducto1;
        private System.Windows.Forms.Panel panel1;
        private Visores.VisorMonto vVendidos;
        private Visores.VisorMonto vMontoComprados;
        private Visores.VisorMonto vComprados;
        private Visores.VisorMonto vMontoVendidos;
        private Visores.VisorMonto vMontoDevueltos;
        private Visores.VisorMonto vDevueltos;
        private Visores.VisorMonto vMontoRetornados;
        private Visores.VisorMonto vRetornados;
        private Visores.VisorMonto vSalidas;
        private Visores.VisorMonto vEntradas;
        private Visores.VisorMonto vStock;
        private Visores.VisorMonto vGanancia;
    }
}
