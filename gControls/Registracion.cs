﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    public static class Registracion
    {
        public static void BloquearConKey(Control c)
        {
            if (!gManager.CoreManager.Singlenton.Key.Habilitado)
            {
                if (c is BaseControl) ((BaseControl)c).RequiereKey = true;
                c.Enabled = false;
                UtilControls.ResaltarControl(c, 4);
            }
        }
        public static void BloquearConKey(ToolStripItem c)
        {
            if (!gManager.CoreManager.Singlenton.Key.Habilitado)
            {
                c.Enabled = false;
                UtilControls.ResaltarControl(c, 4);
                c.ToolTipText = "Requiere registración";


            }
        }
    }
}
