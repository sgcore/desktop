﻿using BrightIdeasSoftware;
using gControls.Formularios;
using gManager;
using gManager.Filtros;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    public class buscar
    {
        private static Formularios.fEditorContenedor _bdest;
        private static gcDestinatario _destsel = null;
        static OpenFileDialog fimg;
        
        //public static object BuscarEntity(EnumEntityTipo tipo,string desc="")
        //{
        //    var f = new Formularios.fBuscador(tipo);
        //    f.descripcion = desc;
        //    if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        return f.Seleccionado;
        //    }
        //    return null;
        //}
        public static gcDestinatario BuscarDestinatario(string titulo, gcDestinatario.DestinatarioTipo t, bool showfiltro=false)
        {
           if(_bdest==null)
            {
                _bdest=new Formularios.fEditorContenedor();
                var b = new Buscadores.BuscadorDestinatario();
                
                
                _bdest.Control=b;
                _bdest.Text = titulo;
                _bdest.Icon = System.Drawing.Icon.FromHandle((new System.Drawing.Bitmap(b.ImagenTitulo)).GetHicon());
                b.VerBotonFiltro = showfiltro;
                b.Titulo = titulo;
               

                b.ItemSeleccionado += (o, e) => { _destsel = o as gcDestinatario; _bdest.Hide(); };
            }
           var filtro = new FiltroDestinatario();
         
           filtro.Tipos.Add(t);
           (_bdest.Control as Buscadores.BuscadorDestinatario).setFiltro(filtro);
           _bdest.Text = titulo;
            _bdest.ShowDialog();
            return _destsel;
        }
       
        public static gcObjeto BuscarProducto(string titulo, FiltroProducto f)
        {
            var fo = new fEditorContenedor();
            var ob = new Buscadores.BuscadorProductos();
            ob.Titulo = f.Descripcion;
            fo.Text = titulo;
            fo.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Buscar.GetHicon());
            ob.setFiltro(f);
            fo.Control = ob;
            gcObjeto ret = null;
            ob.ItemSeleccionado += (o, e) =>
            {
                ret = o as gcObjeto;
                fo.DialogResult = DialogResult.Cancel;
            };
            if (fo.ShowDialog() == DialogResult.OK)
                return ob.Seleccionado as gcObjeto;
            return ret;

        }
        
        public static gcObjeto BuscarProductoEnConsumo(string titulo, gcDestinatario d)
        {
            if (d == null) return null;
            var fo = new fEditorContenedor();
            var ob = new Buscadores.BuscadorConsumo();
            ob.Destinatario = d;
            ob.Titulo = "Buscar productos de "+d.Nombre;
            ob.iniciar();
            fo.Control = ob;
            gcObjeto ret = null;
            ob.ItemSeleccionado += (o, e) =>
            {
                //No se puede usar el parametro o
                ret = ob.Seleccionado;
                fo.DialogResult = DialogResult.Cancel;
            };
            if (fo.ShowDialog() == DialogResult.OK)
                return ob.Seleccionado as gcObjeto;
            return ret;

        }
        public static gcMovimiento BuscarMovimiento(string titulo,bool global=false)
        {
            var f = new Formularios.fEditorContenedor();
            f.Text = "Buscar Movimientos";
            f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Compra.GetHicon());
            var obj = new Buscadores.BuscadorMovimientos();
            obj.SeleccionGlobal = global;
            if (global) f.setSinGuardar();
            obj.Titulo = titulo;
            obj.VerBotonFiltro = true;
            obj.setFiltro(Editar.EditarFiltro(new FiltroMovimiento()));
            f.Control = obj;
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return (gcMovimiento)obj.Seleccionado;
            }
            return null;
        }
        //public static List<gcCaja> BuscarCajas()
        //{
        //    var f = new Formularios.fEditorContenedor();
        //    f.Text = "Buscar Cajas";
        //    f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Banco.GetHicon());
        //    var obj = new Editores.EditorFiltroCaja();
        //    f.Control = obj;
        //    if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        return CoreManager.Singlenton.getCajas(obj.Filtro);
        //    }
        //    return null;
        //}
        public static List<gcCaja> BuscarCajas()
        {
          
            Buscadores.BuscadorCajas obj = null;
            obj= new Buscadores.BuscadorCajas();
            obj.Titulo = "Buscar Cajas";
            obj.ImagenTitulo = Properties.Resources.Buscar;
            obj.VerBotonFiltro = true;
            obj.setFiltro(new FiltroCaja(FiltroCaja.TipoFiltroCaja.Todas));
            obj.Lista.Sort(obj.Lista.GetColumn(0), SortOrder.Descending); ;

            fEditorContenedor f = new fEditorContenedor();
            f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Caja.GetHicon());
            f.Text = "Todas las cajas";
            f.Control = obj;
            List<gcCaja> lista=null;
            obj.Lista.SelectionChanged += (o, e) =>
            {
                lista = (o as VirtualObjectListView).SelectedObjects.OfType<gcCaja>().ToList();

            };
            obj.ItemSeleccionado += (o, e) =>
            {
                lista = new List<gcCaja>() { o as gcCaja };
                f.DialogResult = DialogResult.OK;
            };
           
            if (f.ShowDialog() == DialogResult.OK)
                return lista;
            return new List<gcCaja>();
        }
        public static gcCaja BuscarCaja()
        {
            var f = new Formularios.fEditorContenedor();
            f.Text = "Buscar Cajas";
            f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Caja.GetHicon());
            var obj = new Buscadores.BuscadorCajas();
            var filtro = new FiltroCaja(FiltroCaja.TipoFiltroCaja.Todas);
            obj.setFiltro(filtro);
            

            obj.Titulo = "Buscar Cajas";
            obj.ImagenTitulo = Properties.Resources.Buscar;
            obj.iniciar();
            f.Control = obj;
            gcCaja ret = null;
            obj.ItemSeleccionado += (o, e) =>
            {
                ret = o as gcCaja;
                f.DialogResult = DialogResult.Cancel;
            };
            f.ShowDialog();
            return ret;
        }
        public static gcUsuario BuscarUsuario()
        {
            var f = new Formularios.fEditorContenedor();
            f.Text = "Buscar Usuarios";
            f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Usuario.GetHicon());
            var obj = new Buscadores.BuscadorUsuarios();
            obj.Titulo = "Buscar Usuario para edición";
            f.Control = obj;
            obj.iniciar();
            gcUsuario _user=null;
            obj.ItemSeleccionado += (o, e) => { _user = o as gcUsuario; f.DialogResult = System.Windows.Forms.DialogResult.Cancel; };
            f.setSinGuardar();
            f.ShowDialog();
            return _user;
        }
        public static Image BuscarImage()
        {



            if (FileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = FileDialog.FileName;
                var im = ImageManager.openImage(path);
              
               return  im;

            }
            return null;
        }
        private static OpenFileDialog FileDialog
        {
            get
            {
                if (fimg == null)
                {
                    fimg = new OpenFileDialog();
                    fimg.Filter ="Todas las Imagenes|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff";
                }
                return fimg;
            }
        }

        public static void BuscarProductoEnCSV(string titulo)
        {
            var fo = new fEditorContenedor();
            var ob = new Buscadores.BuscadorProductoUpdate();
            ob.Titulo = titulo;
            fo.Text = titulo;
            fo.Control = ob;
            ob.iniciar();

            fo.ShowDialog();

        }

        //tratamientos
        public static void BuscarTratamientos(gcDestinatario d, gcTratamiento.TipoTratamiento t)
        {
            if (d == null) return;
            Buscadores.BuscadorTratamientos _b=null;
            if(!ver.existeForm("buscTrata")){
                _b=new Buscadores.BuscadorTratamientos();
            }
            var f = ver.crearForm("buscTrata", t.ToString() + " de " + d.Nombre, _b);
            _b = f.Control as Buscadores.BuscadorTratamientos;

            _b.setBotonAgregar(t);
            _b.setDestinatario(d, t);
            f.ShowDialog();
                CoreManager.Singlenton.seleccionarDestinatario(d);
        }
    }
}
