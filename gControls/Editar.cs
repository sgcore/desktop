﻿using gManager;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    public class Editar
    {
        public static decimal EditarDecimal(decimal m,string titulo="Editar Decimal",int decimales=2,decimal min=0, decimal max=9999999)
        {
            var f = new Formularios.fEditorDecimal();
            f.Text = titulo;
            f.Numero = m;
            f.setRango(min, max);
            f.setDecimales(decimales);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return f.Numero;
            }
            return m;
        }
        public static gcDestinatario EditarDestinatario(gcDestinatario d)
        {
            var c = new Editores.EditorDestinatario();
            c.ShowButtons = false;

            return EditarDestinatario(d, c);
            
        }
        public static gcDestinatario EditarDestinatario(gcDestinatario d, Editores.EditorDestinatario obj)
        {
            if (d == null) return null;
            var f = new Formularios.fEditorContenedor();
            if (d.Id < 1) { f.Text = "Crear " + d.Tipo.ToString(); f.Icon = Icon.FromHandle(Properties.Resources.NuevoDestinatario.GetHicon()); }
            else
            { f.Text = "Editar " + d.Tipo.ToString(); f.Icon = Icon.FromHandle(Properties.Resources.Editar.GetHicon()); }
            obj.Destinatario = d;
            f.Control = obj;
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var d1 = obj.Destinatario;
                if ((d1.Tipo == gcDestinatario.DestinatarioTipo.Paciente || d1.Tipo == gcDestinatario.DestinatarioTipo.Contacto)
                    && d1.Parent < 1)
                {
                    gLogger.logger.Singlenton.addWarningMsg("No se puede guardar si no esta relacionado a un cliente.");
                    return null;
                }
                obj.Destinatario.SaveMe();
                return obj.Destinatario;
            }
            return null;
        }
        public static void EditarProducto(gcObjeto o)
        {
            if (o == null) return;
            var f = new Formularios.fEditorContenedor();
            string tt = "Editar ";
            if (o.Id < 1) tt = "Crear ";
            f.Text = tt + o.Nombre;
            var c = new Editores.EditorObjeto();
            c.ShowButtons = false;
            c.Objeto = o;
            f.Control =(c);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                gcObjeto prod = c.Objeto;
                prod.SaveMe();
                if (c.ImageToSave != null)
                {
                    ImageManager.SaveImageProducto(c.ImageToSave, c.Objeto);
                    WebManager.Singlenton.UploadImagenProducto(prod, (op,ep) =>
                    {
                        gLogger.logger.Singlenton.addDebugMsg("Sincronizacion", "Se cambio la imagen de " + prod.Codigo + " en el servidor");
                    });
                }
                if (prod.Sincronizado)
                {
                    prod.Sincronizar();
                }
            }

        }
        public static void EditarTratamiento(gcTratamiento t)
        {
            if (t == null
                ) return;
            
            var f = new Formularios.fEditorContenedor();
            f.Text = t.Tipo.ToString() + " en " + t.PacienteNombre;
            f.Icon = Icon.FromHandle(Properties.Resources.Tarea.GetHicon());
            var obj = new Editores.EditorTratamiento();
            obj.Tratamiento = t;
            f.Control = (obj);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                t = obj.Tratamiento;
                if (!t.esValido)
                    gLogger.logger.Singlenton.addWarningMsg(f.Text+" no se puede agregar porque no puso titulo, descripción o mas de un minuto de retraso.");
                else
                {
                    
                    t.saveMe();
                   
                    CoreManager.Singlenton.SeleccionarTratamiento(t);
                }
                   
            }
        }
        public static void EditarUsuario(gcUsuario u)
        {
            if (u == null
                ) return;

            var f = new Formularios.fEditorContenedor();
            f.Text = "Editar "+u.Nombre;
            f.Icon = Icon.FromHandle(Properties.Resources.Tarea.GetHicon());
            var obj = new Editores.EditorUsuarios();
            obj.Usuario = u;
            
            f.Control = (obj);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
               u = obj.Usuario;
                if (u.Responsable==null || u.Central==null || String.IsNullOrEmpty(u.User) )
                    gLogger.logger.Singlenton.addWarningMsg("No se puede guardar este usuario por que no tiene todos los datos necesarios.");
                else
                    u.UpdateMe();
            }
        }
        public static gcObjetoMovido.MonedaEnum selectMoneda(gcObjetoMovido.MonedaEnum inicial)
        {
            //creo la ventana emergente
            Formularios.fOpciones f = new Formularios.fOpciones();
            //creo el control opcion
            Visores.VisorOpciones vo=new Visores.VisorOpciones();
            vo.OpcionSeleccionada.option = (int)inicial;
            //lleno con botones
            foreach (var r in Enum.GetValues(typeof(gcObjetoMovido.MonedaEnum)))
            {
                var b = vo.addOpcion((int)r, r.ToString(), Properties.Resources.Cerrar24);
                b.Checked = b.Opcion == (int)inicial;

            }
            vo.Dock = System.Windows.Forms.DockStyle.Fill;
            f.Controls.Add(vo);
            f.Text = "Seleccionar Moneda";
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                return (gcObjetoMovido.MonedaEnum) vo.OpcionSeleccionada.option;
            return inicial;
        }
        public static gcPago EditarEfectivo(gcPago p, string titulo = "Efectivo", gcPago sug = null, gcPago block = null)
        {
            Formularios.fEditorContenedor f = new Formularios.fEditorContenedor();
            var obj = new Editores.EditorEfectivo();
            obj.restringir(sug, block);
            obj.EstablecerTotal(p);
            f.Control = (obj);
            f.Text = titulo;
            f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Efectivo.GetHicon());
            gcPago.TipoPago tipo = gcPago.TipoPago.Efectivo;
            if (p != null) tipo = p.Tipo;
            if (f.ShowDialog()==System.Windows.Forms.DialogResult.OK)
            {
                p = obj.ObtenerPago(tipo);
                return p;
            }
            return null;
        }
        public static void ShowLoggerConfig()
        {
            gLogger.logger.Singlenton.ShowConfig();
        }
        public static gcObjetoMovido EditarFraccion(gcObjetoMovido om)
        {
            var obj = new Editores.EditorFraccion();
            obj.ObjetoMovido = om;
            var f = generarForm(om.NombreProducto, Properties.Resources.Fraccion, obj);
            if (f.ShowDialog()== DialogResult.OK)
            {
                om.Fraccionado = true;
                om = obj.ObjetoMovido;
                om.saveMe();
                return om;

            }
            om.Fraccionado = false;
            return om;
        }
        public static gcMovimiento EditarDescuento(gcMovimiento m)
        {

            var obj = new Editores.EditorDescuentos();
            obj.setMovimiento(m);
            var f = generarForm("Agregar un descuento", Properties.Resources.Porciento, obj);
            if (f.ShowDialog() == DialogResult.OK)
            {
                

            }
            return m;
        }
        public static void EditarConfigEditorMov()
        {
            var obj = new Editores.EditorConfigEditorMov();
            var f = generarForm("Configurar Editor", Properties.Resources.Configuracion, obj);
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                Properties.Settings.Default.Save();
        }
        private static  Formularios.fEditorContenedor generarForm(string Titulo, System.Drawing.Bitmap icon, System.Windows.Forms.Control control)
        {
            var f = new Formularios.fEditorContenedor();
            f.Text = Titulo;
            f.Icon = System.Drawing.Icon.FromHandle(icon.GetHicon());
            f.Control = (control);
            return f;
        }
        public static string EditarTexto(string titulo,string texto)
        {
            var f = new Formularios.fEditorContenedor();
            var t = new System.Windows.Forms.TextBox();
            t.Multiline = true;
            t.Size = new Size(400, 200);
            t.Text = texto;
            f.Text = titulo;
            f.Icon = Icon.FromHandle(Properties.Resources.Editar.GetHicon());
            f.Control = t;
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return t.Text;
            }
            return texto ;
        }
        public static gManager.Filtros.Filtro EditarFiltro(gManager.Filtros.Filtro ft)
        {
            if (ft == null) return null;
            var f = new Formularios.fEditorContenedor();
            f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Filtro.GetHicon());
            f.Text = "Filtrar movimientos";
            if (ft is gManager.Filtros.FiltroCaja)
            {
                var fw = new Editores.EditorFiltroCaja();
                fw.Filtro = (gManager.Filtros.FiltroCaja)ft;
                f.Control = (fw);
                if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return fw.Filtro;
                }

            }
            else if (ft is gManager.Filtros.FiltroMovimiento)
            {
                var fw = new Editores.EditorFiltroMovimiento();
                fw.Filtro = (gManager.Filtros.FiltroMovimiento)ft;
                f.Control = (fw);
                if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return fw.Filtro;
                }
            }
            else if (ft is gManager.Filtros.FiltroTratamientos)
            {
                var fw = new Editores.EditorFiltroTratamientos();
                fw.Filtro = (gManager.Filtros.FiltroTratamientos)ft;
                f.Control = (fw);
                if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return fw.Filtro;
                }
            }
            else if (ft is gManager.Filtros.FiltroProducto)
            {
                var fw = new Editores.EditorFiltroProducto();
                fw.Filtro = (gManager.Filtros.FiltroProducto)ft;
                f.Control = (fw);
                f.Text = "Filtrar Productos";
                if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return fw.Filtro;
                }
            }

            return ft;
           
            
            
        }
        public static int SeleccionarMetrica()
        {
            var f = new Formularios.fOpciones();
            f.Text = "Seleccionar el tipo de tratamiento";
            f.setDescripcion("El tipo de tratamiento le ayuda a diferenciar.");
            f.StartPosition = FormStartPosition.CenterScreen;
            foreach (gcObjeto.ObjetoMedida a in Enum.GetValues(typeof(gcObjeto.ObjetoMedida)))
            {
                f.addBoton(a.ToString(), (int)a, Properties.Resources.Cantidad);
            }
            if (f.ShowDialog() == DialogResult.OK)
            {
                return f.Seleccion;
            }
            return -1;
        }
        public static DateTime EditarFecha(DateTime fecha)
        {
            CustomControls.YearSelector obj= new CustomControls.YearSelector();
            obj.Year = fecha;
            var f = new Formularios.fEditorContenedor();
            f.Icon = Icon.FromHandle(Properties.Resources.Mes.GetHicon());
            f.Text = "Seleccione una fecha";
            f.Control = obj;
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return obj.Year;
            }

            return fecha;
            
            
        }

      
       
    }
}
