﻿using gControls.ControlesHtml.htmlObjects;
using gControls.Formularios;
using gManager;
using gManager.Filtros;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls
{
    public class ver
    {
        static Hashtable _forms = new Hashtable();
        static fEditorContenedor _vDest ;
        
        public static fEditorContenedor crearForm(string id,string titulo,System.Windows.Forms.Control obj,bool solocerrar=true)
        {
             fEditorContenedor f=null;
             if (_forms.ContainsKey(id))
             {
                 f = _forms[id] as fEditorContenedor;
             }
             else
             {
                 f = new fEditorContenedor();
                
                 if (solocerrar)
                 {
                     f.setSinGuardar();
                     f.FormClosing += (o, e) => { f.Hide(); e.Cancel = true; };
                 }else
                 {
                     f.FormClosing += (o, e) => { _forms.Remove(id); };
                 }
                 _forms.Add(id, f);
                 f.Control = obj;
             }
             f.Text = titulo;
            return f;
        }
        public static fOpciones Opciones(string titulo, string descr)
        {
            var f = new fOpciones();
            f.Text = titulo;
            f.setDescripcion(descr);
            f.StartPosition = FormStartPosition.CenterScreen;
            return f;
        }
        public static bool existeForm(string key)
        {
            return _forms.ContainsKey(key);
        }
        public static void VerDestinatarios(gManager.gcDestinatario d)
        {
            //if (!CoreManager.Singlenton.Key.Habilitado)
            //{
            //    gLogger.logger.Singlenton.addWarningMsg("Debe habilitar la administración para poder realizar esta acción");
            //    return;
            //}
            if (d == null) return;
            if (d.Tipo == gManager.gcDestinatario.DestinatarioTipo.Paciente)
            {
                VerTratamientos(d);
                return;
            }
            if (_vDest == null)
            {
                _vDest = new fEditorContenedor();
                _vDest.Icon = Icon.FromHandle(Properties.Resources.Ver.GetHicon());
                var obj = new Seguimientos.SeguimientoDestinatario();
                obj.CerrarSeguimiento += (o, e) => { _vDest.DialogResult = System.Windows.Forms.DialogResult.Cancel; };
                _vDest.Control = (obj);
            }
            var oo=_vDest.Control as Seguimientos.SeguimientoDestinatario;
            oo.setDestinatario(d);


            _vDest.setSinGuardar();
            _vDest.Text = "Seguimiento de " + (d != null ? d.Nombre : "Destinatarios");
            _vDest.ShowDialog();
        }
        public static void VerTratamientos(gManager.gcDestinatario d)
        {
            if (d == null) return;
            var f = new Formularios.fEditorContenedor();
            f.Icon = Icon.FromHandle(Properties.Resources.Tratamiento.GetHicon());
            var obp = new Seguimientos.SeguimientoPaciente();
            obp.setDestinatario(d);
            f.Control =(obp);
            f.setSinGuardar();
            f.Text = "Seguimiento de " + (d != null ? d.Nombre : "Destinatarios");
            f.ShowDialog();
        }
        public static void VerBilletes(gcPago p, string title = "TOTAL")
        {
            if (p == null) return;
            var f = new Formularios.fEditorContenedor();
            f.Icon = Icon.FromHandle(Properties.Resources.Efectivo.GetHicon());
            var obj = new Visores.VisorBilletes();
            obj.setPago(p, title);
            f.Control = (obj);
            f.setSinGuardar();
            f.Text = "Conteo de billetes";
            f.ShowDialog();
        }
        public static void VerAsistenteCompra(gManager.gcMovimiento m)
        {
            if (m.Tipo != gManager.gcMovimiento.TipoMovEnum.Compra) return;
            var f = new Formularios.fEditorContenedor();
            f.Icon = Icon.FromHandle(Properties.Resources.Compra.GetHicon());
            var obj = new Asistentes.AsistentePrecioCompra();
            obj.setObjetos(m.ObjetosMovidos);
            f.Control = (obj);
            f.setSinGuardar();
            f.Text = "Actualización de precios";
            f.ShowDialog();
        }
        public static void VerAsistenteCierreCaja()
        {
           
            var f = new Formularios.fEditorContenedor();
            f.Icon = Icon.FromHandle(Properties.Resources.Cerrar.GetHicon());
            var obj = new Asistentes.AsistenteCerrarCaja();
            obj.iniciar();
            f.Control = (obj);
            f.setSinGuardar();
            f.Text = "Cierre de caja";
            f.ShowDialog();
        }
        public static void VerListaPrecio(FiltroProducto filtro)
        {

            var f = new Formularios.fEditorContenedor();
            f.Icon = Icon.FromHandle(Properties.Resources.Precio.GetHicon());
            var obj = new ControlesHtml.CHtmlListaPrecio(filtro);
            f.Control = (obj);
            f.setSinGuardar();
            f.Text = "Lista de Precios";
            f.ShowDialog();
        }
        public static void VerCaja(gManager.gcCaja c)
        {
            VerCajas(new List<gcCaja> { c });
            
        }
        public static void VerCajas(List<gcCaja> lc)
        {
            if (lc == null) return;
            var obj = new Visores.VisorCajas();
            obj.verFiltro = true;
            bool crearEvento = !_forms.ContainsKey("vcajas");
            fEditorContenedor f = crearForm("vcajas", "Ver Cajas", obj);
            if(crearEvento)
                CoreManager.Singlenton.MovimientoSeleccionado += (m) => { f.DialogResult = System.Windows.Forms.DialogResult.Cancel; };
           
            (f.Control as Visores.VisorCajas).setCajas(lc);
            f.Show(); f.BringToFront();
        }
        public static void VerResumenVentasHtml(List<gcCaja> cajas)
        {
            var cr = new htmlResumenCategorias(cajas);
            CoreManager.Singlenton.GenerarInformeHtml(cr.ToString());

           

        }
        public static void VerResumenVentas(List<gcCaja> cajas)
        {
            if(cajas==null || cajas.Count==0)return ;
            string titulo = "Resumen de Ventas";
            if (cajas.Count == 1)
                titulo += " Caja " + cajas[0].Caja;
            else
                titulo += " Cajas " + cajas[0].Caja + " " + cajas[cajas.Count - 1];

             fEditorContenedor f;
             if (_forms.ContainsKey("vresventas")) { f = _forms["vresventas"] as fEditorContenedor; } 
             else 
             {
                 var obj = new Buscadores.BuscadorResumenTree();
                 //obj.setCajas(cajas, false);
                 obj.iniciar();
              
                 f = crearForm("vresventas","Resumen de Ventas",obj);
                 f.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.Banco.GetHicon());
                 
             }
          
             (f.Control as Buscadores.BuscadorResumenTree).Titulo = titulo;
             (f.Control as Buscadores.BuscadorResumenTree).setCajas(cajas, true);
             f.Show(); f.BringToFront();
        }
        public static void VerAsistenteTratamiento(gManager.gcDestinatario paciente, gManager.gcTratamiento.TipoTratamiento tipo)
        {
            if (paciente == null) return;
            var t = new gManager.gcTratamiento(paciente,tipo);
            Editar.EditarTratamiento(t);
            

        }
        public static void VerImageObjeto(object o)
        {
             var f = new Formularios.fEditorContenedor();
            f.Icon = Icon.FromHandle(Properties.Resources.Tratamiento.GetHicon());
            var obp = new System.Windows.Forms.PictureBox();
            
            f.Control =(obp);
            f.setSinGuardar();
            f.Text = "imagen";
           
            if(o is gManager.gcObjeto)
            {
                obp.Image = ImageManager.getImageGeneric(o as gcObjeto, Properties.Resources.nopic);
               
            }

            f.ShowDialog();
        }
        public static void VerProducto(gcObjeto o)
        {
            if (!CoreManager.Singlenton.Key.Habilitado)
            {
                gLogger.logger.Singlenton.addWarningMsg("Debe habilitar la administración para poder realizar esta acción");
                return;
            }
            if (o== null) return;
            var rp = new gManager.Resumenes.SeguimientoProducto(o);
            rp.setCajas(CoreManager.Singlenton.getCajas());
            Seguimientos.SeguimientoProducto obj=null;
            if (!existeForm("vsegprod"))
            {
                obj = new Seguimientos.SeguimientoProducto();
               
            }
          
            fEditorContenedor f = crearForm("vsegprod", "Seguimiento de Producto", obj);
            f.Icon = Icon.FromHandle(Properties.Resources.Ver.GetHicon());
            obj = f.Control as Seguimientos.SeguimientoProducto;
            var sp = new gManager.Resumenes.SeguimientoProducto(o);
            sp.setCajas(CoreManager.Singlenton.getCajas());
            obj.setProductoSeguimiento(sp);
            f.Show(); f.BringToFront();
           
        }
        public static void VerRegistro()
        {
            Visores.VisorKey obj=null;
            if (!existeForm("vregistro"))
            {
                obj = new Visores.VisorKey();
                
            }
            fEditorContenedor f = crearForm("vregistro", "Habilitación", obj);
            obj = f.Control as Visores.VisorKey;
            obj.UpdateTiempo();
            f.ShowDialog();
        }
        public static void verHtml(string p, string titulo)
        {
            gManager.CoreManager.Singlenton.GenerarInformeHtml(p);
            //Impresiones.generarArchivoHtml(p,titulo);
        }
        public static void verCaracteristicas(gcObjeto o)
        {
            Editores.EditorCaracteristicas obj = null;
            if (!existeForm("vregistro"))
            {
                obj = new Editores.EditorCaracteristicas();

            }
            fEditorContenedor f = crearForm("ecaract", "Caracteristicas", obj);
            obj = f.Control as Editores.EditorCaracteristicas;
            obj.SetObjeto(o);
            f.ShowDialog();
        }
        public static void verAvisos()
        {
          
        }
    }
}
