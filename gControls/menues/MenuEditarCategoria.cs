﻿using gManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gControls.menues
{
    public class MenuEditarCategoria: ToolStripMenuItem
    {
        EventHandler _seleccionar;
        
       public MenuEditarCategoria(EventHandler seleccionar):base("Editar", Properties.Resources.Editar, (o, e) => { seleccionar (o, e); Acciones.EditarProducto(null, null); })
        {
            _seleccionar = seleccionar;
            this.DropDownOpening += MenuEditarCategoria_DropDownOpening;
           
        }

        private void MenuEditarCategoria_DropDownOpening(object sender, EventArgs e)
        {
            _seleccionar(null, null);
             if (Acciones.Producto != null && (Acciones.Producto.Tipo == gcObjeto.ObjetoTIpo.Categoria || Acciones.Producto.Tipo == gcObjeto.ObjetoTIpo.Grupo))
            {
                if(DropDownItems.Count == 0)
                {
                    ToolStripMenuItem acasca = new ToolStripMenuItem("Actualizar en cascada", Properties.Resources.Sistema);
                    acasca.DropDownItems.Add(new ToolStripMenuItem("Actualizar costo", Properties.Resources.Precio, Acciones.ActualizarCascadaCosto));
                    acasca.DropDownItems.Add(new ToolStripMenuItem("Actualizar ganancia", Properties.Resources.Porciento, Acciones.ActualizarCascadaGanancia));
                    acasca.DropDownItems.Add(new ToolStripMenuItem("Actualizar Iva", Properties.Resources.Banco, Acciones.ActualizarCascadaIva));
                    acasca.DropDownItems.Add(new ToolStripMenuItem("Actualizar Dolar", Properties.Resources.Precio, Acciones.ActualizarCascadaDolar));
                    acasca.DropDownItems.Add(new ToolStripMenuItem("Actualizar Ideal", Properties.Resources.Cantidad, Acciones.ActualizarCascadaIdeal));
                    var estado = new ToolStripMenuItem("Cambiar Estado", Properties.Resources.Estadisticas);
                    estado.DropDownItems.Add(new ToolStripMenuItem("Normal", Properties.Resources.Desconectar, Acciones.ActualizarCascadaEstadoNormal));
                    estado.DropDownItems.Add(new ToolStripMenuItem("Publico", Properties.Resources.Conectado, Acciones.ActualizarCascadaEstadoPublico));
                    estado.DropDownItems.Add(new ToolStripMenuItem("Discontinuo", Properties.Resources.Error, Acciones.ActualizarCascadaEstadoDiscontinuo));
                    acasca.DropDownItems.Add(estado);
                    acasca.ToolTipText = "Actualiza a todos los productos y servicios que contiene.";
                    acasca.Enabled = CoreManager.Singlenton.Key.Habilitado;
                    DropDownItems.Add(acasca);
                }
                
            } else
            {
                DropDownItems.Clear();
            }
        }
    }
}
