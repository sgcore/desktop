﻿namespace gControls.Paneles
{
    partial class PanelCaja
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelCaja));
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblResponsable = new System.Windows.Forms.Label();
            this.btCancelREspo = new gControls.BotonAccion();
            this.botonAccion1 = new gControls.BotonAccion();
            this.btExtraccionResp = new gControls.BotonAccion();
            this.btDepositoRespon = new gControls.BotonAccion();
            this.btCerrarCaja = new gControls.BotonAccion();
            this.label3 = new System.Windows.Forms.Label();
            this.visorCaja1 = new gControls.Visores.VisorCaja();
            this.buCajaActual = new gControls.Buscadores.BuscadorMovimientos();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.lblResponsable);
            this.panel4.Controls.Add(this.btCancelREspo);
            this.panel4.Controls.Add(this.botonAccion1);
            this.panel4.Controls.Add(this.btExtraccionResp);
            this.panel4.Controls.Add(this.btDepositoRespon);
            this.panel4.Controls.Add(this.btCerrarCaja);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 205);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(469, 70);
            this.panel4.TabIndex = 87;
            // 
            // lblResponsable
            // 
            this.lblResponsable.BackColor = System.Drawing.Color.Transparent;
            this.lblResponsable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblResponsable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResponsable.Location = new System.Drawing.Point(126, 20);
            this.lblResponsable.Margin = new System.Windows.Forms.Padding(0);
            this.lblResponsable.Name = "lblResponsable";
            this.lblResponsable.Size = new System.Drawing.Size(196, 50);
            this.lblResponsable.TabIndex = 79;
            this.lblResponsable.Text = "Ninguno";
            this.lblResponsable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btCancelREspo
            // 
            this.btCancelREspo.BackColor = System.Drawing.Color.Transparent;
            this.btCancelREspo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCancelREspo.Dock = System.Windows.Forms.DockStyle.Right;
            this.btCancelREspo.FormContainer = null;
            this.btCancelREspo.GlobalHotKey = false;
            this.btCancelREspo.HacerInvisibleAlDeshabilitar = false;
            this.btCancelREspo.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCancelREspo.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btCancelREspo.Location = new System.Drawing.Point(322, 20);
            this.btCancelREspo.Name = "btCancelREspo";
            this.btCancelREspo.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btCancelREspo.RegistradoEnForm = false;
            this.btCancelREspo.Size = new System.Drawing.Size(34, 50);
            this.btCancelREspo.TabIndex = 78;
            this.btCancelREspo.Titulo = "Desactivar responsable.";
            this.btCancelREspo.ToolTipDescripcion = "Desactiva el responsable de los movimientos de dinero.";
            this.btCancelREspo.UseVisualStyleBackColor = false;
            this.btCancelREspo.Click += new System.EventHandler(this.btCancelREspo_Click);
            // 
            // botonAccion1
            // 
            this.botonAccion1.AutoSize = true;
            this.botonAccion1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonAccion1.Dock = System.Windows.Forms.DockStyle.Right;
            this.botonAccion1.Enabled = false;
            this.botonAccion1.FormContainer = null;
            this.botonAccion1.GlobalHotKey = false;
            this.botonAccion1.HacerInvisibleAlDeshabilitar = false;
            this.botonAccion1.HotKey = System.Windows.Forms.Shortcut.None;
            this.botonAccion1.Image = global::gControls.Properties.Resources.Actualizar;
            this.botonAccion1.Location = new System.Drawing.Point(356, 20);
            this.botonAccion1.Name = "botonAccion1";
            this.botonAccion1.Permiso = gManager.gcUsuario.PermisoEnum.Contador;
            this.botonAccion1.RegistradoEnForm = false;
            this.botonAccion1.Size = new System.Drawing.Size(55, 50);
            this.botonAccion1.TabIndex = 85;
            this.botonAccion1.Text = "Cambiar";
            this.botonAccion1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.botonAccion1.Titulo = "Cambiar Caja";
            this.botonAccion1.ToolTipDescripcion = "Establece como caja actual cualquier caja.| En cajas anteriores no se pueden real" +
    "izar movimientos.";
            this.botonAccion1.UseVisualStyleBackColor = true;
            this.botonAccion1.Click += new System.EventHandler(this.cambiarcaja_Click);
            // 
            // btExtraccionResp
            // 
            this.btExtraccionResp.AutoSize = true;
            this.btExtraccionResp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btExtraccionResp.Dock = System.Windows.Forms.DockStyle.Left;
            this.btExtraccionResp.Enabled = false;
            this.btExtraccionResp.FormContainer = null;
            this.btExtraccionResp.GlobalHotKey = false;
            this.btExtraccionResp.HacerInvisibleAlDeshabilitar = false;
            this.btExtraccionResp.HotKey = System.Windows.Forms.Shortcut.None;
            this.btExtraccionResp.Image = global::gControls.Properties.Resources.Extraccion;
            this.btExtraccionResp.Location = new System.Drawing.Point(59, 20);
            this.btExtraccionResp.Name = "btExtraccionResp";
            this.btExtraccionResp.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btExtraccionResp.RegistradoEnForm = false;
            this.btExtraccionResp.Size = new System.Drawing.Size(67, 50);
            this.btExtraccionResp.TabIndex = 83;
            this.btExtraccionResp.Text = "Extracción";
            this.btExtraccionResp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btExtraccionResp.Titulo = "Extracción de Dinero.";
            this.btExtraccionResp.ToolTipDescripcion = "Recaudación de dinero.";
            this.btExtraccionResp.UseVisualStyleBackColor = true;
            this.btExtraccionResp.Click += new System.EventHandler(this.btExtraccionResp_Click);
            // 
            // btDepositoRespon
            // 
            this.btDepositoRespon.AutoSize = true;
            this.btDepositoRespon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDepositoRespon.Dock = System.Windows.Forms.DockStyle.Left;
            this.btDepositoRespon.Enabled = false;
            this.btDepositoRespon.FormContainer = null;
            this.btDepositoRespon.GlobalHotKey = false;
            this.btDepositoRespon.HacerInvisibleAlDeshabilitar = false;
            this.btDepositoRespon.HotKey = System.Windows.Forms.Shortcut.None;
            this.btDepositoRespon.Image = global::gControls.Properties.Resources.Deposito;
            this.btDepositoRespon.Location = new System.Drawing.Point(0, 20);
            this.btDepositoRespon.Name = "btDepositoRespon";
            this.btDepositoRespon.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btDepositoRespon.RegistradoEnForm = false;
            this.btDepositoRespon.Size = new System.Drawing.Size(59, 50);
            this.btDepositoRespon.TabIndex = 82;
            this.btDepositoRespon.Text = "Deposito";
            this.btDepositoRespon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btDepositoRespon.Titulo = "Aporte de Dinero";
            this.btDepositoRespon.ToolTipDescripcion = "Ingresa dinero al sistema.";
            this.btDepositoRespon.UseVisualStyleBackColor = true;
            this.btDepositoRespon.Click += new System.EventHandler(this.btDepositoRespon_Click);
            // 
            // btCerrarCaja
            // 
            this.btCerrarCaja.AutoSize = true;
            this.btCerrarCaja.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCerrarCaja.Dock = System.Windows.Forms.DockStyle.Right;
            this.btCerrarCaja.Enabled = false;
            this.btCerrarCaja.FormContainer = null;
            this.btCerrarCaja.GlobalHotKey = false;
            this.btCerrarCaja.HacerInvisibleAlDeshabilitar = false;
            this.btCerrarCaja.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCerrarCaja.Image = global::gControls.Properties.Resources.Conectado24;
            this.btCerrarCaja.Location = new System.Drawing.Point(411, 20);
            this.btCerrarCaja.Name = "btCerrarCaja";
            this.btCerrarCaja.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btCerrarCaja.RegistradoEnForm = false;
            this.btCerrarCaja.Size = new System.Drawing.Size(58, 50);
            this.btCerrarCaja.TabIndex = 84;
            this.btCerrarCaja.Text = "Terminar";
            this.btCerrarCaja.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btCerrarCaja.Titulo = "Cerrar Caja";
            this.btCerrarCaja.ToolTipDescripcion = "Realiza el cambio de caja. terminando la actual e iniciando la siguiente.";
            this.btCerrarCaja.UseVisualStyleBackColor = true;
            this.btCerrarCaja.Click += new System.EventHandler(this.btCerrarCaja_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(469, 20);
            this.label3.TabIndex = 77;
            this.label3.Text = "ACCIONES EN CAJA";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // visorCaja1
            // 
            this.visorCaja1.BackColor = System.Drawing.Color.Transparent;
            this.visorCaja1.Dock = System.Windows.Forms.DockStyle.Top;
            this.visorCaja1.Location = new System.Drawing.Point(0, 0);
            this.visorCaja1.Name = "visorCaja1";
            this.visorCaja1.Size = new System.Drawing.Size(469, 205);
            this.visorCaja1.TabIndex = 0;
            // 
            // buCajaActual
            // 
            this.buCajaActual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buCajaActual.ImagenTitulo = global::gControls.Properties.Resources.Banco;
            this.buCajaActual.Location = new System.Drawing.Point(0, 275);
            this.buCajaActual.MovimientosCajaACtual = true;
            this.buCajaActual.Name = "buCajaActual";
            this.buCajaActual.OcultarBotonFiltro = false;
            this.buCajaActual.OcultarSiEstaVacio = true;
            this.buCajaActual.SeleccionGlobal = true;
            this.buCajaActual.Size = new System.Drawing.Size(469, 497);
            this.buCajaActual.TabIndex = 88;
            this.buCajaActual.Titulo = "Todos los movimientos de la caja actual.";
            // 
            // PanelCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.buCajaActual);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.visorCaja1);
            this.Name = "PanelCaja";
            this.Size = new System.Drawing.Size(469, 772);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Visores.VisorCaja visorCaja1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblResponsable;
        private BotonAccion btCancelREspo;
        private BotonAccion btExtraccionResp;
        private BotonAccion btDepositoRespon;
        private BotonAccion btCerrarCaja;
        private BotonAccion botonAccion1;
        private System.Windows.Forms.Label label3;
        private Buscadores.BuscadorMovimientos buCajaActual;


    }
}
