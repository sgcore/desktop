﻿namespace gControls.Paneles
{
    partial class PanelSucursales
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelSucursales));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSucursal = new System.Windows.Forms.Label();
            this.btCancelCliente = new gControls.BotonAccion();
            this.MovImageList = new System.Windows.Forms.ImageList(this.components);
            this.btSalida = new gControls.BotonAccion();
            this.btEntrada = new gControls.BotonAccion();
            this.label12 = new System.Windows.Forms.Label();
            this.buscador1 = new gControls.Buscadores.BuscadorDestinatarios();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblSucursal);
            this.panel1.Controls.Add(this.btCancelCliente);
            this.panel1.Controls.Add(this.btSalida);
            this.panel1.Controls.Add(this.btEntrada);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(811, 65);
            this.panel1.TabIndex = 85;
            // 
            // lblSucursal
            // 
            this.lblSucursal.BackColor = System.Drawing.Color.Transparent;
            this.lblSucursal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSucursal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSucursal.Location = new System.Drawing.Point(109, 20);
            this.lblSucursal.Margin = new System.Windows.Forms.Padding(0);
            this.lblSucursal.Name = "lblSucursal";
            this.lblSucursal.Size = new System.Drawing.Size(668, 45);
            this.lblSucursal.TabIndex = 79;
            this.lblSucursal.Text = "Ninguno";
            this.lblSucursal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btCancelCliente
            // 
            this.btCancelCliente.BackColor = System.Drawing.Color.Transparent;
            this.btCancelCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCancelCliente.Dock = System.Windows.Forms.DockStyle.Right;
            this.btCancelCliente.GlobalHotKey = false;
            this.btCancelCliente.HacerInvisibleAlDeshabilitar = false;
            this.btCancelCliente.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCancelCliente.ImageKey = "Cancelar";
            this.btCancelCliente.ImageList = this.MovImageList;
            this.btCancelCliente.Location = new System.Drawing.Point(777, 20);
            this.btCancelCliente.Name = "btCancelCliente";
            this.btCancelCliente.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btCancelCliente.Size = new System.Drawing.Size(34, 45);
            this.btCancelCliente.TabIndex = 78;
            this.btCancelCliente.Titulo = "Eliminar Selección";
            this.btCancelCliente.ToolTipDescripcion = "Elimina el cliente seleccionado para realizar acciones.";
            this.btCancelCliente.UseVisualStyleBackColor = false;
            this.btCancelCliente.Click += new System.EventHandler(this.btCancelCliente_Click);
            // 
            // MovImageList
            // 
            this.MovImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MovImageList.ImageStream")));
            this.MovImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MovImageList.Images.SetKeyName(0, "Compra");
            this.MovImageList.Images.SetKeyName(1, "Retorno");
            this.MovImageList.Images.SetKeyName(2, "Venta");
            this.MovImageList.Images.SetKeyName(3, "Devolucion");
            this.MovImageList.Images.SetKeyName(4, "Entrada");
            this.MovImageList.Images.SetKeyName(5, "Salida");
            this.MovImageList.Images.SetKeyName(6, "Extraccion");
            this.MovImageList.Images.SetKeyName(7, "Pedido");
            this.MovImageList.Images.SetKeyName(8, "Presupuesto");
            this.MovImageList.Images.SetKeyName(9, "Terminado");
            this.MovImageList.Images.SetKeyName(10, "Creado");
            this.MovImageList.Images.SetKeyName(11, "Editado");
            this.MovImageList.Images.SetKeyName(12, "Buscar");
            this.MovImageList.Images.SetKeyName(13, "Cancelar");
            this.MovImageList.Images.SetKeyName(14, "Deposito");
            // 
            // btSalida
            // 
            this.btSalida.AutoSize = true;
            this.btSalida.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btSalida.Dock = System.Windows.Forms.DockStyle.Left;
            this.btSalida.Enabled = false;
            this.btSalida.GlobalHotKey = false;
            this.btSalida.HacerInvisibleAlDeshabilitar = false;
            this.btSalida.HotKey = System.Windows.Forms.Shortcut.None;
            this.btSalida.ImageKey = "Salida";
            this.btSalida.ImageList = this.MovImageList;
            this.btSalida.Location = new System.Drawing.Point(54, 20);
            this.btSalida.Name = "btSalida";
            this.btSalida.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btSalida.Size = new System.Drawing.Size(55, 45);
            this.btSalida.TabIndex = 58;
            this.btSalida.Text = "Salida";
            this.btSalida.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btSalida.Titulo = "Devolucion de Mercaderia";
            this.btSalida.ToolTipDescripcion = "Devolución por parte del cliente de mercaderías";
            this.btSalida.UseVisualStyleBackColor = true;
            this.btSalida.Click += new System.EventHandler(this.btSalida_Click);
            // 
            // btEntrada
            // 
            this.btEntrada.AutoSize = true;
            this.btEntrada.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEntrada.Dock = System.Windows.Forms.DockStyle.Left;
            this.btEntrada.Enabled = false;
            this.btEntrada.GlobalHotKey = true;
            this.btEntrada.HacerInvisibleAlDeshabilitar = false;
            this.btEntrada.HotKey = System.Windows.Forms.Shortcut.None;
            this.btEntrada.ImageKey = "Entrada";
            this.btEntrada.ImageList = this.MovImageList;
            this.btEntrada.Location = new System.Drawing.Point(0, 20);
            this.btEntrada.Name = "btEntrada";
            this.btEntrada.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btEntrada.Size = new System.Drawing.Size(54, 45);
            this.btEntrada.TabIndex = 54;
            this.btEntrada.Text = "Entrada";
            this.btEntrada.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btEntrada.Titulo = "Venta a Cliente";
            this.btEntrada.ToolTipDescripcion = "Crea una venta al último cliente seleccionado.| Si utiliza el acceso directo o la" +
    " tecla Control al pulsar el botón, le preguntará el cliente de destino.";
            this.btEntrada.UseVisualStyleBackColor = true;
            this.btEntrada.Click += new System.EventHandler(this.btEntrada_Click);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Silver;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(811, 20);
            this.label12.TabIndex = 77;
            this.label12.Text = "ACCIONES CON SUCURSALES";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buscador1
            // 
            this.buscador1.BackColor = System.Drawing.Color.LightSeaGreen;
             this.buscador1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscador1.Location = new System.Drawing.Point(0, 65);
            this.buscador1.Name = "buscador1";
             this.buscador1.SeleccionGlobal = false;
            this.buscador1.Size = new System.Drawing.Size(811, 506);
            this.buscador1.TabIndex = 86;
            this.buscador1.Titulo = "Buscar Sucursales";
             // 
            // PanelSucursales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buscador1);
            this.Controls.Add(this.panel1);
            this.Name = "PanelSucursales";
            this.Size = new System.Drawing.Size(811, 571);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblSucursal;
        private BotonAccion btCancelCliente;
        private BotonAccion btSalida;
        private BotonAccion btEntrada;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ImageList MovImageList;
        private Buscadores.BuscadorDestinatarios buscador1;
    }
}
