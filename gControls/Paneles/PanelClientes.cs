﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Paneles
{
    public partial class PanelClientes : UserControl
    {
        public PanelClientes()
        {
            InitializeComponent();
            buscador1.ItemSeleccionado += buscador1_ItemSeleccionado;
        }

        void buscador1_ItemSeleccionado(object sender, EventArgs e)
        {
            Cliente = (gcDestinatario)buscador1.Seleccionado;
        }
        private gcDestinatario crear(gManager.gcDestinatario d, EnumEntityTipo t, gManager.gcMovimiento.TipoMovEnum tv)
        {
            if (ModifierKeys == Keys.Control)
                d = null;

            else if(d == null)
            {
                d = (gcDestinatario)buscador1.Seleccionado;
            }
            if (d == null)
            {
                d = (gManager.gcDestinatario)buscar.BuscarEntity(t);
            }
            if (d != null)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(tv, d));
            }
            return d;
        }
        gManager.gcDestinatario _cliente = null;
       
        private gcDestinatario Cliente
        {
            get
            {
                return _cliente;
            }
            set
            {
                _cliente = value;
                if (_cliente != null)
                {
                    lblCliente.Text = _cliente.Nombre;
                }
                else
                {
                    lblCliente.Text = "Ninguno";
                }
            }
        }
       
        
        private void btVenta_Click(object sender, EventArgs e)
        {
            
            Cliente = crear(Cliente, EnumEntityTipo.Cliente, gManager.gcMovimiento.TipoMovEnum.Venta);

        }

        private void btCancelCliente_Click(object sender, EventArgs e)
        {
            Cliente = null;
        }

        private void btDevolucion_Click(object sender, EventArgs e)
        {
            Cliente = crear(Cliente, EnumEntityTipo.Cliente, gManager.gcMovimiento.TipoMovEnum.Devolucion);
        }
        private void btPresupuesto_Click(object sender, EventArgs e)
        {
            Cliente = crear(Cliente, EnumEntityTipo.Cliente, gManager.gcMovimiento.TipoMovEnum.Presupuesto);
        }
        private void btDepositoCliente_Click(object sender, EventArgs e)
        {
            Cliente = crear(Cliente, EnumEntityTipo.Cliente, gManager.gcMovimiento.TipoMovEnum.Deposito);
        }

        private void btExtraccionCliente_Click(object sender, EventArgs e)
        {
            Cliente = crear(Cliente, EnumEntityTipo.Cliente, gManager.gcMovimiento.TipoMovEnum.Extraccion);
        }

        private void btEditar_Click(object sender, EventArgs e)
        {
            Editar.EditarDestinatario((gcDestinatario)buscador1.Seleccionado);
        }

        private void btCrearCliente_Click(object sender, EventArgs e)
        {
            Editar.EditarDestinatario(new gcDestinatario(gcDestinatario.DestinatarioTipo.Cliente));
           
        }

        private void btCrearContacto_Click(object sender, EventArgs e)
        {
            Editar.EditarDestinatario(new gcDestinatario(gcDestinatario.DestinatarioTipo.Contacto));
            buscador1.Actualizar() ;
        }

        private void btSeguimiento_Click(object sender, EventArgs e)
        {
            ver.VerDestinatarios((gcDestinatario)buscador1.Seleccionado);
            buscador1.Actualizar();
        }



    }
}
