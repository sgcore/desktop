﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Paneles
{
    public partial class PanelPrincipal : UI.AcordionContainer
    {
        public PanelPrincipal()
        {
            InitializeComponent();
            IniciarControles();
            
            gManager.CoreManager.Singlenton.MovimientoSeleccionado += Singlenton_MovimientoSeleccionado;
            gManager.CoreManager.Singlenton.MovimientoEliminado += Singlenton_MovimientoEliminado;

        }

        void Singlenton_MovimientoEliminado(gManager.gcMovimiento m)
        {
           switchElemento(accCajaActual);
        }

        void Singlenton_MovimientoSeleccionado(gManager.gcMovimiento m)
        {
            
            if (m.EsMovimientoDinero && !m.Terminado) switchElemento(accCajaActual);
            else if((m.EsMovimientoMercaderia || m.EsPresupuestooPedido) && !m.Terminado)switchElemento(accProductos);
        }
       
       
    }
}
