﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Paneles
{
    public partial class PanelProveedores : UserControl
    {
        public PanelProveedores()
        {
            InitializeComponent();
            buscador1.ItemSeleccionado += buscador1_ItemSeleccionado;
        }

        private void buscador1_ItemSeleccionado(object sender, EventArgs e)
        {
            Proveedor = (gcDestinatario)buscador1.Seleccionado;
        }
        gManager.gcDestinatario _proveedor = null;
        private gcDestinatario Proveedor
        {
            get
            {
                return _proveedor;
            }
            set
            {
                _proveedor = value;
                if (_proveedor != null)
                {
                    lblProveedor.Text = _proveedor.Nombre;
                }
                else
                {
                    lblProveedor.Text = "Ninguno";
                }
            }
        }
        private gcDestinatario crear(gManager.gcDestinatario d, EnumEntityTipo t, gManager.gcMovimiento.TipoMovEnum tv)
        {
            if (ModifierKeys == Keys.Control)
                d = null;

            else if (d == null)
            {
                d = (gcDestinatario)buscador1.Seleccionado;
            }
            if (d == null)
            {
                d = (gManager.gcDestinatario)buscar.BuscarEntity(t);
            }
            if (d != null)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(tv, d));
            }
            return d;
        }
        private void btCancelProveedor_Click(object sender, EventArgs e)
        {
            Proveedor = null;
        }
        private void btCompra_Click(object sender, EventArgs e)
        {
            Proveedor = crear(Proveedor, EnumEntityTipo.Proveedor, gManager.gcMovimiento.TipoMovEnum.Compra);
        }
        private void btRetorno_Click(object sender, EventArgs e)
        {
            Proveedor = crear(Proveedor, EnumEntityTipo.Proveedor, gManager.gcMovimiento.TipoMovEnum.Retorno);
        }

        private void btPedido_Click(object sender, EventArgs e)
        {
            Proveedor = crear(Proveedor, EnumEntityTipo.Proveedor, gManager.gcMovimiento.TipoMovEnum.Pedido);
        }
        private void btDepositoProv_Click(object sender, EventArgs e)
        {
            Proveedor = crear(Proveedor, EnumEntityTipo.Proveedor, gManager.gcMovimiento.TipoMovEnum.Extraccion);
        }

        private void btExtraccionProv_Click(object sender, EventArgs e)
        {
            Proveedor = crear(Proveedor, EnumEntityTipo.Proveedor, gManager.gcMovimiento.TipoMovEnum.Deposito);
        }

        private void btEditar_Click(object sender, EventArgs e)
        {
            Editar.EditarDestinatario((gcDestinatario)buscador1.Seleccionado);
        }

        private void btCrearCliente_Click(object sender, EventArgs e)
        {
            Editar.EditarDestinatario(new gcDestinatario(gcDestinatario.DestinatarioTipo.Proveedor));
        }

        private void btSeguimiento_Click(object sender, EventArgs e)
        {
            ver.VerDestinatarios((gcDestinatario)buscador1.Seleccionado);
        }




       
    }
}
