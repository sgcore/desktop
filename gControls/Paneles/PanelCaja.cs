﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Paneles
{
    public partial class PanelCaja : BaseControl
    {
        public PanelCaja()
        {
            InitializeComponent();
            gManager.CoreManager.Singlenton.PagoAgregado += Singlenton_PagoAgregado;
            gManager.CoreManager.Singlenton.PagoEliminado += Singlenton_PagoAgregado;
            gManager.CoreManager.Singlenton.CajaSeleccionada += Singlenton_CajaSeleccionada;
        }

        void Singlenton_CajaSeleccionada(gcCaja c)
        {
            iniciar();
            buCajaActual.iniciar();
        }

        void Singlenton_PagoAgregado(gManager.gcPago p)
        {
            iniciar();
        }

        public override void iniciar()
        {
            base.iniciar();
            this.Visible = gManager.CoreManager.Singlenton.ElUsuarioSuperaElPermiso(gManager.gcUsuario.PermisoEnum.Encargado);
            visorCaja1.setCaja(gManager.CoreManager.Singlenton.CajaActual);
            
        }
        private void cambiarcaja_Click(object sender, EventArgs e)
        {
            CoreManager.Singlenton.CambiarACaja((gcCaja)buscar.BuscarEntity(EnumEntityTipo.Cajas));
        }
        private void btDepositoRespon_Click(object sender, EventArgs e)
        {
            Responsable = crear(Responsable, EnumEntityTipo.Responsable, gManager.gcMovimiento.TipoMovEnum.Deposito);
        }

        private void btExtraccionResp_Click(object sender, EventArgs e)
        {
            Responsable = crear(Responsable, EnumEntityTipo.Responsable, gManager.gcMovimiento.TipoMovEnum.Extraccion);
        }
        private gcDestinatario crear(gManager.gcDestinatario d, EnumEntityTipo t, gManager.gcMovimiento.TipoMovEnum tv)
        {
            if (d == null)
            {
                d = (gManager.gcDestinatario)buscar.BuscarEntity(t);
            }
            if (d != null)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(tv, d));
            }
            return d;
        }
        
        gManager.gcDestinatario _responsable = null;
        private gcDestinatario Responsable
        {
            get
            {
                return _responsable;
            }
            set
            {
                _responsable = value;
                if (_responsable != null)
                {
                    lblResponsable.Text = _responsable.Nombre;
                }
                else
                {
                    lblResponsable.Text = "Ninguno";
                }
            }
        }

        private void btCerrarCaja_Click(object sender, EventArgs e)
        {
            ver.VerAsistenteCierreCaja();
        }

        private void btCancelREspo_Click(object sender, EventArgs e)
        {
            Responsable = null;
        }


       
    }
}
