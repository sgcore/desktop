﻿namespace gControls.Paneles
{
    partial class PanelPrincipal
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.accAdmin = new gControls.UI.AcordionElement();
            this.panelConfigSistema1 = new gControls.Paneles.PanelConfigSistema();
            this.accCajaActual = new gControls.UI.AcordionElement();
            this.panelCaja1 = new gControls.Paneles.PanelCaja();
            this.accClientes = new gControls.UI.AcordionElement();
            this.buscadorDestinatarios1 = new gControls.Buscadores.BuscadorDestinatarios();
            this.DropZone = new System.Windows.Forms.Panel();
            this.accProveedores = new gControls.UI.AcordionElement();
            this.buscadorDestinatarios2 = new gControls.Buscadores.BuscadorDestinatarios();
            this.accProductos = new gControls.UI.AcordionElement();
            this.panelProductosTree1 = new gControls.Paneles.PanelProductosTree();
            this.acordionElement2 = new gControls.UI.AcordionElement();
            this.buscadorDestinatarios3 = new gControls.Buscadores.BuscadorDestinatarios();
            this.accAdmin.DropZone.SuspendLayout();
            this.accAdmin.SuspendLayout();
            this.accCajaActual.DropZone.SuspendLayout();
            this.accCajaActual.SuspendLayout();
            this.accClientes.DropZone.SuspendLayout();
            this.accClientes.SuspendLayout();
            this.accProveedores.DropZone.SuspendLayout();
            this.accProveedores.SuspendLayout();
            this.accProductos.DropZone.SuspendLayout();
            this.accProductos.SuspendLayout();
            this.acordionElement2.DropZone.SuspendLayout();
            this.acordionElement2.SuspendLayout();
            this.SuspendLayout();
            // 
            // accAdmin
            // 
            this.accAdmin.BackColor = System.Drawing.Color.Transparent;
            this.accAdmin.Collapsed = true;
            this.accAdmin.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // accAdmin.DropZone
            // 
            this.accAdmin.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.accAdmin.DropZone.Controls.Add(this.panelConfigSistema1);
            this.accAdmin.DropZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accAdmin.DropZone.Location = new System.Drawing.Point(0, 32);
            this.accAdmin.DropZone.Name = "DropZone";
            this.accAdmin.DropZone.Size = new System.Drawing.Size(843, 136);
            this.accAdmin.DropZone.TabIndex = 1;
            this.accAdmin.fullHeight = true;
            this.accAdmin.GlobalHotKey = false;
            this.accAdmin.HotKey = System.Windows.Forms.Shortcut.None;
            this.accAdmin.Imagen = global::gControls.Properties.Resources.Configuracion;
            this.accAdmin.Location = new System.Drawing.Point(10, 160);
            this.accAdmin.Name = "accAdmin";
            this.accAdmin.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.accAdmin.Size = new System.Drawing.Size(758, 32);
            this.accAdmin.TabIndex = 3;
            this.accAdmin.Titulo = "CONFIGURACIÓN";
            this.accAdmin.ToolTipDescripcion = "Realiza administrativas como ser configuración del sistema,";
            // 
            // panelConfigSistema1
            // 
            this.panelConfigSistema1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelConfigSistema1.Location = new System.Drawing.Point(0, 0);
            this.panelConfigSistema1.Name = "panelConfigSistema1";
            this.panelConfigSistema1.Size = new System.Drawing.Size(843, 136);
            this.panelConfigSistema1.TabIndex = 0;
            // 
            // accCajaActual
            // 
            this.accCajaActual.BackColor = System.Drawing.Color.Transparent;
            this.accCajaActual.Collapsed = true;
            this.accCajaActual.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // accCajaActual.DropZone
            // 
            this.accCajaActual.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.accCajaActual.DropZone.Controls.Add(this.panelCaja1);
            this.accCajaActual.DropZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accCajaActual.DropZone.Location = new System.Drawing.Point(0, 32);
            this.accCajaActual.DropZone.Name = "DropZone";
            this.accCajaActual.DropZone.Size = new System.Drawing.Size(512, 761);
            this.accCajaActual.DropZone.TabIndex = 1;
            this.accCajaActual.fullHeight = true;
            this.accCajaActual.GlobalHotKey = true;
            this.accCajaActual.HotKey = System.Windows.Forms.Shortcut.F2;
            this.accCajaActual.Imagen = global::gControls.Properties.Resources.Cantidad;
            this.accCajaActual.Location = new System.Drawing.Point(10, 32);
            this.accCajaActual.Name = "accCajaActual";
            this.accCajaActual.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.accCajaActual.Size = new System.Drawing.Size(758, 32);
            this.accCajaActual.TabIndex = 5;
            this.accCajaActual.Titulo = "CAJA ACTUAL";
            this.accCajaActual.ToolTipDescripcion = "Muestra el estado de la caja actual y permite realizar algunas acciones.";
            // 
            // panelCaja1
            // 
            this.panelCaja1.AutoScroll = true;
            this.panelCaja1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCaja1.Location = new System.Drawing.Point(0, 0);
            this.panelCaja1.Name = "panelCaja1";
            this.panelCaja1.Size = new System.Drawing.Size(512, 761);
            this.panelCaja1.TabIndex = 0;
            // 
            // accClientes
            // 
            this.accClientes.BackColor = System.Drawing.Color.Transparent;
            this.accClientes.Collapsed = true;
            this.accClientes.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // accClientes.DropZone
            // 
            this.accClientes.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.accClientes.DropZone.Controls.Add(this.buscadorDestinatarios1);
            this.accClientes.DropZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accClientes.DropZone.Location = new System.Drawing.Point(0, 32);
            this.accClientes.DropZone.Name = "DropZone";
            this.accClientes.DropZone.Size = new System.Drawing.Size(758, 758);
            this.accClientes.DropZone.TabIndex = 1;
            this.accClientes.fullHeight = true;
            this.accClientes.GlobalHotKey = true;
            this.accClientes.HotKey = System.Windows.Forms.Shortcut.F1;
            this.accClientes.Imagen = global::gControls.Properties.Resources.Cliente;
            this.accClientes.Location = new System.Drawing.Point(10, 0);
            this.accClientes.Name = "accClientes";
            this.accClientes.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.accClientes.Size = new System.Drawing.Size(758, 32);
            this.accClientes.TabIndex = 6;
            this.accClientes.Titulo = "CLIENTES";
            this.accClientes.ToolTipDescripcion = "Gestión Clientes";
            // 
            // buscadorDestinatarios1
            // 
            this.buscadorDestinatarios1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscadorDestinatarios1.Location = new System.Drawing.Point(0, 0);
            this.buscadorDestinatarios1.Name = "buscadorDestinatarios1";
            this.buscadorDestinatarios1.OcultarBotonFiltro = false;
            this.buscadorDestinatarios1.SeleccionGlobal = false;
            this.buscadorDestinatarios1.Size = new System.Drawing.Size(758, 758);
            this.buscadorDestinatarios1.TabIndex = 0;
            this.buscadorDestinatarios1.TipoBuscador = gManager.gcDestinatario.DestinatarioTipo.Cliente;
            this.buscadorDestinatarios1.Titulo = "Buscar, agregar, editar y realizar acciones con clientes.";
            // 
            // DropZone
            // 
            this.DropZone.Location = new System.Drawing.Point(120, 131);
            this.DropZone.Name = "DropZone";
            this.DropZone.Size = new System.Drawing.Size(200, 100);
            this.DropZone.TabIndex = 1;
            // 
            // accProveedores
            // 
            this.accProveedores.BackColor = System.Drawing.Color.Transparent;
            this.accProveedores.Collapsed = true;
            this.accProveedores.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // accProveedores.DropZone
            // 
            this.accProveedores.DropZone.Controls.Add(this.buscadorDestinatarios2);
            this.accProveedores.DropZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accProveedores.DropZone.Location = new System.Drawing.Point(0, 32);
            this.accProveedores.DropZone.Name = "DropZone";
            this.accProveedores.DropZone.Size = new System.Drawing.Size(775, 758);
            this.accProveedores.DropZone.TabIndex = 1;
            this.accProveedores.fullHeight = true;
            this.accProveedores.GlobalHotKey = true;
            this.accProveedores.HotKey = System.Windows.Forms.Shortcut.F11;
            this.accProveedores.Imagen = global::gControls.Properties.Resources.Proveedor;
            this.accProveedores.Location = new System.Drawing.Point(10, 96);
            this.accProveedores.Name = "accProveedores";
            this.accProveedores.Permiso = gManager.gcUsuario.PermisoEnum.Contador;
            this.accProveedores.Size = new System.Drawing.Size(758, 32);
            this.accProveedores.TabIndex = 8;
            this.accProveedores.Titulo = "PROVEEDORES";
            this.accProveedores.ToolTipDescripcion = "Gestión Proveedores";
            // 
            // buscadorDestinatarios2
            // 
            this.buscadorDestinatarios2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscadorDestinatarios2.Location = new System.Drawing.Point(0, 0);
            this.buscadorDestinatarios2.Name = "buscadorDestinatarios2";
            this.buscadorDestinatarios2.OcultarBotonFiltro = false;
            this.buscadorDestinatarios2.SeleccionGlobal = false;
            this.buscadorDestinatarios2.Size = new System.Drawing.Size(775, 758);
            this.buscadorDestinatarios2.TabIndex = 0;
            this.buscadorDestinatarios2.TipoBuscador = gManager.gcDestinatario.DestinatarioTipo.Proveedor;
            this.buscadorDestinatarios2.Titulo = "Buscar, crear, editar y otras acciones con proveedores.";
            // 
            // accProductos
            // 
            this.accProductos.BackColor = System.Drawing.Color.Transparent;
            this.accProductos.Collapsed = true;
            this.accProductos.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // accProductos.DropZone
            // 
            this.accProductos.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.accProductos.DropZone.Controls.Add(this.panelProductosTree1);
            this.accProductos.DropZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accProductos.DropZone.Location = new System.Drawing.Point(0, 32);
            this.accProductos.DropZone.Name = "DropZone";
            this.accProductos.DropZone.Size = new System.Drawing.Size(468, 761);
            this.accProductos.DropZone.TabIndex = 1;
            this.accProductos.fullHeight = true;
            this.accProductos.GlobalHotKey = true;
            this.accProductos.HotKey = System.Windows.Forms.Shortcut.F3;
            this.accProductos.Imagen = global::gControls.Properties.Resources.Producto;
            this.accProductos.Location = new System.Drawing.Point(10, 64);
            this.accProductos.Name = "accProductos";
            this.accProductos.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.accProductos.Size = new System.Drawing.Size(758, 32);
            this.accProductos.TabIndex = 9;
            this.accProductos.Titulo = "PRODUCTOS";
            this.accProductos.ToolTipDescripcion = "Gestión de productos y servicios. Creación, edición y movimientos.";
            // 
            // panelProductosTree1
            // 
            this.panelProductosTree1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelProductosTree1.EditOnClick = false;
            this.panelProductosTree1.GruposVisible = false;
            this.panelProductosTree1.Location = new System.Drawing.Point(0, 0);
            this.panelProductosTree1.Name = "panelProductosTree1";
            this.panelProductosTree1.Size = new System.Drawing.Size(468, 761);
            this.panelProductosTree1.TabIndex = 0;
            // 
            // acordionElement2
            // 
            this.acordionElement2.BackColor = System.Drawing.Color.Transparent;
            this.acordionElement2.Collapsed = true;
            this.acordionElement2.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // acordionElement2.DropZone
            // 
            this.acordionElement2.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.acordionElement2.DropZone.Controls.Add(this.buscadorDestinatarios3);
            this.acordionElement2.DropZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.acordionElement2.DropZone.Location = new System.Drawing.Point(0, 32);
            this.acordionElement2.DropZone.Name = "DropZone";
            this.acordionElement2.DropZone.Size = new System.Drawing.Size(741, 758);
            this.acordionElement2.DropZone.TabIndex = 1;
            this.acordionElement2.fullHeight = true;
            this.acordionElement2.GlobalHotKey = false;
            this.acordionElement2.HotKey = System.Windows.Forms.Shortcut.None;
            this.acordionElement2.Imagen = global::gControls.Properties.Resources.Sucursal;
            this.acordionElement2.Location = new System.Drawing.Point(10, 128);
            this.acordionElement2.Name = "acordionElement2";
            this.acordionElement2.Permiso = gManager.gcUsuario.PermisoEnum.Contador;
            this.acordionElement2.Size = new System.Drawing.Size(758, 32);
            this.acordionElement2.TabIndex = 14;
            this.acordionElement2.Titulo = "SUCURSALES";
            this.acordionElement2.ToolTipDescripcion = "Entradas y salidas de mercaderias entre sucursales y depositos.";
            // 
            // buscadorDestinatarios3
            // 
            this.buscadorDestinatarios3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscadorDestinatarios3.Location = new System.Drawing.Point(0, 0);
            this.buscadorDestinatarios3.Name = "buscadorDestinatarios3";
            this.buscadorDestinatarios3.OcultarBotonFiltro = false;
            this.buscadorDestinatarios3.SeleccionGlobal = false;
            this.buscadorDestinatarios3.Size = new System.Drawing.Size(741, 758);
            this.buscadorDestinatarios3.TabIndex = 0;
            this.buscadorDestinatarios3.TipoBuscador = gManager.gcDestinatario.DestinatarioTipo.Sucursal;
            this.buscadorDestinatarios3.Titulo = "Titulo";
            // 
            // PanelPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.accAdmin);
            this.Controls.Add(this.acordionElement2);
            this.Controls.Add(this.accProveedores);
            this.Controls.Add(this.accProductos);
            this.Controls.Add(this.accCajaActual);
            this.Controls.Add(this.accClientes);
            this.Name = "PanelPrincipal";
            this.Size = new System.Drawing.Size(768, 790);
            this.Controls.SetChildIndex(this.accClientes, 0);
            this.Controls.SetChildIndex(this.accCajaActual, 0);
            this.Controls.SetChildIndex(this.accProductos, 0);
            this.Controls.SetChildIndex(this.accProveedores, 0);
            this.Controls.SetChildIndex(this.acordionElement2, 0);
            this.Controls.SetChildIndex(this.accAdmin, 0);
            this.accAdmin.DropZone.ResumeLayout(false);
            this.accAdmin.ResumeLayout(false);
            this.accCajaActual.DropZone.ResumeLayout(false);
            this.accCajaActual.ResumeLayout(false);
            this.accClientes.DropZone.ResumeLayout(false);
            this.accClientes.ResumeLayout(false);
            this.accProveedores.DropZone.ResumeLayout(false);
            this.accProveedores.ResumeLayout(false);
            this.accProductos.DropZone.ResumeLayout(false);
            this.accProductos.ResumeLayout(false);
            this.acordionElement2.DropZone.ResumeLayout(false);
            this.acordionElement2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UI.AcordionElement accAdmin;
        private PanelConfigSistema panelConfigSistema1;
        private UI.AcordionElement accCajaActual;
        private UI.AcordionElement accClientes;
        private UI.AcordionElement accProveedores;
        private PanelCaja panelCaja1;
        private UI.AcordionElement accProductos;
        private PanelProductosTree panelProductosTree1;
        private UI.AcordionElement acordionElement2;
        private System.Windows.Forms.Panel DropZone;
        private Buscadores.BuscadorDestinatarios buscadorDestinatarios1;
        private Buscadores.BuscadorDestinatarios buscadorDestinatarios2;
        private Buscadores.BuscadorDestinatarios buscadorDestinatarios3;
    }
}
