﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Paneles
{
    public partial class PanelAccionesMovimiento : UserControl
    {
        gcMovimiento _mov;
        public PanelAccionesMovimiento()
        {
            InitializeComponent();
            setMovimiento(null);
            
        }
        
        public void setMovimiento(gcMovimiento m)
        {
            _mov = m;
            btTerminar.BackgroundImage = null;
            editorPagos1.setMovimiento(m);
            this.Visible = _mov!=null;
            if (_mov != null)
            {
                if (_mov.Id < 1 && Properties.Settings.Default.cfgAutoguardarAlCrearMov)
                { _mov.saveMe(); reload(); return; }

               
                btEliminar.Visible = _mov.SePuedeEliminar;
                btEditar.Visible = _mov.SePuedeEditar;
               
                btPreimpresa.Visible = _mov.Terminado;
                btTerminar.Visible = _mov.SePuedeTerminar;
                if (btTerminar.Visible) btTerminar.BackgroundImage = Properties.Resources.Activo;
                btTicket.Visible = _mov.Terminado;
                btGuardar.Visible = _mov.Id < 1;
                btLimpiarPagos.Visible = !_mov.Terminado && _mov.TotalPagado > 0;
               
            }
            else
            {
               
                btEliminar.Visible = false;
                btPreimpresa.Visible = false;
                btTerminar.Visible = false;
                btTicket.Visible = false;
                btEditar.Visible = false;
                btGuardar.Visible = false;
                btLimpiarPagos.Visible = false;
                
                

            }
        }

        private void btTerminar_Click(object sender, EventArgs e)
        {
            if (_mov != null)
            {
                if (_mov.Estado == gcMovimiento.EstadoEnum.Creado)
                {
                    ver.VerAsistenteCompra(_mov);
                }
                _mov.TerminarOperacion();
            }
            reload();
            
        }
        private void reload()
        {
            CoreManager.Singlenton.seleccionarMovimiento(_mov);
        }

        private void btEditar_Click(object sender, EventArgs e)
        {
            if (_mov != null) _mov.EditarMovimiento();
            reload();
        }
        

       

        private void btGuardar_Click(object sender, EventArgs e)
        {
            if (_mov != null) _mov.saveMe();
            reload();
        }

        private void btEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Deséa eliminar este Movimiento?", "Eliminar definitivamente", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes
                && _mov.deleteMe())
            {
               
                setMovimiento(null);
                reload();
            }
        }

        

        

        private void btTicket_Click(object sender, EventArgs e)
        {
            Impresiones.ImprimirTicket(_mov, ModifierKeys == Keys.Control);
            //Impresiones.FacturaElectronica(_mov);
        }

        private void btLimpiarPagos_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Deséa eliminar todos los pagos?", "Limpiar los pagos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _mov.clearPagos();
                CoreManager.Singlenton.seleccionarMovimiento(_mov);
            }
            
        }

        private void btPreimpresa_Click(object sender, EventArgs e)
        {
             Impresiones.imprimirPreimpresa(_mov,ModifierKeys==Keys.Control);
        }

       
        
    }
}
