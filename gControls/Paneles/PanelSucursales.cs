﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;

namespace gControls.Paneles
{
    public partial class PanelSucursales : UserControl
    {
        public PanelSucursales()
        {
            InitializeComponent();
            buscador1.ItemSeleccionado += buscador1_ItemSeleccionado;
        }

        void buscador1_ItemSeleccionado(object sender, EventArgs e)
        {
            Sucursal = (gcDestinatario)buscador1.Seleccionado;
        }
        private gcDestinatario crear(gManager.gcDestinatario d, EnumEntityTipo t, gManager.gcMovimiento.TipoMovEnum tv)
        {
            if (ModifierKeys == Keys.Control)
                d = null;

            else if (d == null)
            {
                d = (gcDestinatario)buscador1.Seleccionado;
            }
            if (d == null)
            {
                d = (gManager.gcDestinatario)buscar.BuscarEntity(t);
            }
            if (d != null)
            {
                CoreManager.Singlenton.seleccionarMovimiento(gManager.CoreManager.Singlenton.crearMovimiento(tv, d));
            }
            return d;
        }
        gManager.gcDestinatario _sucursal = null;

        private gcDestinatario Sucursal
        {
            get
            {
                return _sucursal;
            }
            set
            {
                _sucursal = value;
                if (_sucursal != null)
                {
                    lblSucursal.Text = _sucursal.Nombre;
                }
                else
                {
                    lblSucursal.Text = "Ninguno";
                }
            }
        }

        private void btEntrada_Click(object sender, EventArgs e)
        {
            Sucursal = crear(Sucursal, EnumEntityTipo.Cliente, gManager.gcMovimiento.TipoMovEnum.Entrada);
        }

        private void btSalida_Click(object sender, EventArgs e)
        {
            Sucursal = crear(Sucursal, EnumEntityTipo.Cliente, gManager.gcMovimiento.TipoMovEnum.Salida);
        }

        private void btCancelCliente_Click(object sender, EventArgs e)
        {
            Sucursal = null;
        }
    }

}
