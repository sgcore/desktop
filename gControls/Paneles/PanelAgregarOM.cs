﻿using gManager;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace gControls.Paneles
{
    public partial class PanelAgregarOM : UserControl
    {
        public PanelAgregarOM()
        {
            InitializeComponent();
            CoreManager.Singlenton.ProductoSeleccionado += (p) =>
            {
                if (Visible)
                {
                    addProducto(p);
                }
            };

        }
        gcMovimiento _mov;
        public void setMovimiento(gcMovimiento m)
        {
            _mov = m;
            if (_mov != null)
            {
                Visible = !_mov.Terminado && !_mov.EsMovimientoDinero;
                txtCodigo.Enabled = !_mov.Terminado;
                nCantidad.Enabled = !_mov.Terminado;
                btActualizar.Enabled = !_mov.Terminado;
            }
            else
            {
                Visible = false;
                txtCodigo.Enabled = false;
                nCantidad.Enabled = false;
                btActualizar.Enabled = false;
            }
        }
        private void btConfig_Click(object sender, EventArgs e)
        {
            Editar.EditarConfigEditorMov();
        }
        
        public void addProducto(gcObjeto o)
        {
            if (_mov == null || o == null) return;
            if (!_mov.EsOwnerOrContador) return;
            if (o.Tipo == gcObjeto.ObjetoTIpo.Grupo && o.Tipo == gcObjeto.ObjetoTIpo.Categoria) return;
            if (_mov.Estado == gcMovimiento.EstadoEnum.Terminado) return;
            //devoluciones
            if (_mov.Tipo == gcMovimiento.TipoMovEnum.Retorno || _mov.Tipo == gcMovimiento.TipoMovEnum.Devolucion)
            {
                var con=CoreManager.Singlenton.getDestinatarioConsumo(_mov.Destinatario);
                bool find = false;
                foreach (ProductoResumen pr in con)
                {
                    
                    if (pr.Producto.Id == o.Id && pr.Cantidad >= nCantidad.Value)
                    {
                        find = true;
                    }
                }
                if (!find)
                {
                    gLogger.logger.Singlenton.addWarningMsg("No dispone de "+nCantidad.Value+" unidades de "+o.Nombre+ " para este movimiento");
                    return;
                }

            }

            gcObjetoMovido newom = new gcObjetoMovido(o, nCantidad.Value, _mov.DestinatarioTipo == gcDestinatario.DestinatarioTipo.Proveedor,_mov.Cotizacion);
            _mov.addNewObjetoMovido(newom);


            reload();
            if (Properties.Settings.Default.cfgResetCantValue > 0)
            {
                nCantidad.Value = Properties.Settings.Default.cfgResetCantValue;
            }
            if (Properties.Settings.Default.cfgSelCantidadAlAgregar)
                nCantidad.Select();

        }
        private void reload()
        {
            CoreManager.Singlenton.seleccionarMovimiento(_mov);
        }
        private void btBuscar_Click(object sender, EventArgs e)
        {
            showbuscar(txtCodigo.Text);
        }
        private void showbuscar(string t)
        {
            gcObjeto c = null;
            if (_mov != null && _mov.EsDevolucionORetorno)
            {
                c = buscar.BuscarProductoEnConsumo("Buscar Productos específicos", _mov.Destinatario);
            }
            else
            {
                var f = new gManager.Filtros.FiltroProducto();
                f.Tipos.Add(gManager.gcObjeto.ObjetoTIpo.Servicio);
                f.Tipos.Add(gManager.gcObjeto.ObjetoTIpo.Producto);
                f.Texto = t;
                c = buscar.BuscarProducto("Buscar Productos y servicios", f);
            }

            addProducto(c);
        }
        private void nCantidad_Enter(object sender, EventArgs e)
        {
            nCantidad.Select(0, nCantidad.ToString().Length);
        }

        private void nCantidad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtCodigo.Select();

                e.Handled = true;
                e.SuppressKeyPress = true;


            }

        }

        private void txtCodigo_Enter(object sender, EventArgs e)
        {
            txtCodigo.SelectAll();
        }

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var o = CoreManager.Singlenton.getProducto(txtCodigo.Text);
                if (o == null)
                {
                    txtCodigo.BackColor = Color.LightSalmon;
                    showbuscar(txtCodigo.Text);
                }
                else
                {
                    addProducto(o);
                    txtCodigo.BackColor = Color.LightSkyBlue;
                    txtCodigo.BackColor = SystemColors.Control;
                    txtCodigo.Text = "";

                }
                e.Handled = true;
                e.SuppressKeyPress = true;


            }
        }

        private void btCotizacion_Click(object sender, EventArgs e)
        {
            var d=Editar.EditarDecimal(_mov.Cotizacion, "Cotización de Movimiento");
            if (d > 0) { _mov.Cotizacion = d; _mov.saveMe(); reload(); }
            
        }

        private void nCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        private void btDestinatario_Click(object sender, EventArgs e)
        {
            if(_mov.Pagos.Count == 0)
            {
                var tipo = _mov.DestinatarioTipo;
                var a = buscar.BuscarDestinatario("buscar " + tipo.ToString(), tipo);
                if(a != null)
                {
                    _mov.setDestinatario(a);
                    CoreManager.Singlenton.seleccionarMovimiento(_mov);
                }
            }
            else
            {
                gLogger.logger.Singlenton.addWarningMsg("No puede cambiar el destinatario de un movimiento con pagos realizados");
            }
        }
    }
}
