﻿using gManager;
using System;
using System.Windows.Forms;

namespace gControls.Paneles
{
    public partial class SwitchPanel : BaseControl
    {
        ControlesHtml.ChtmlMovimiento pMovh = new ControlesHtml.ChtmlMovimiento();
        ControlesHtml.ChtmlHome pHome = new ControlesHtml.ChtmlHome();
        Control _currentControl = null;
       
        public SwitchPanel()
        {
            InitializeComponent();
            addControl(pMovh);
            addControl(pHome);
            CoreManager.Singlenton.MovimientoSeleccionado += m => switchto(pMovh);
            CoreManager.Singlenton.SesionFinalizada += u => goHome() ;
           
            CoreManager.Singlenton.InfoHtml += (t) =>
            {
                switchto(pHome);
                if (String.IsNullOrEmpty(t))
                {
                    pHome.goHome();
                } else
                {
                    pHome.setContent(t);
                }
               
            };
            goHome();
          
            
            
        }
        
        private void goHome()
        {
            switchto(pHome);
            pHome.goHome();
        }
        
        public void addControl(Control c)
        {
           
            c.Visible = false;
            Controls.Add(c);
            c.Dock = DockStyle.Fill;
        }

       /*
        public void remoteHtml(BackgroundWorker w)
        {
            //string coreweb = "http://localhost/gcore/api1/";
           string coreweb = "http://mascocitas.com/sgapi/web/";
           
           
              

               
                try
                {
                    var wc = new WebClient();
                    string result = wc.DownloadString(new Uri(coreweb));
                    if (pInfo.InvokeRequired)
                    {
                    gManager.CoreManager.InfoHtmlDelegate del = new CoreManager.InfoHtmlDelegate((t) => { pInfo.Text = t; });
                    pInfo.Invoke(del, result);
                    } else
                    {
                        pInfo.Text = result;
                    }
                   
                   
                }catch(Exception)
                {
                    if (pInfo.InvokeRequired)
                    {
                        CoreManager.InfoHtmlDelegate del = new CoreManager.InfoHtmlDelegate((t) => { pInfo.Text = t; });
                        pInfo.Invoke(del, _htmlPrincipal);
                    }
                    else
                    {
                        pInfo.Text = _htmlPrincipal;
                    }
                }

                
            
        }
        public override void iniciar()
        {
            base.iniciar();
            pInfo.Text = _htmlPrincipal;
           
            gLogger.TaskManager.CrearTareaAislada(remoteHtml);
            
            
        }
        public void ShowInfo(string i)
        {
            pInfo.Text = i;
            switchto(pInfo);
        }*/
       
      
        public void switchto(Control c)
        {
            foreach (Control cc in Controls)
            {
                if (cc == c )
                {
                    cc.Visible = true;
                    _currentControl = cc;
                }else
                cc.Visible =false;
            }
            BringToFront();
        }

    }
}
