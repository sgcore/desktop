﻿namespace gControls.Paneles
{
    partial class PanelAgregarOM
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.nCantidad = new System.Windows.Forms.NumericUpDown();
            this.btConfig = new gControls.BotonAccion();
            this.btCotizacion = new gControls.BotonAccion();
            this.btActualizar = new gControls.BotonAccion();
            this.txtCodigo = new gControls.WaterMarkTextBox();
            this.btDestinatario = new gControls.BotonAccion();
            ((System.ComponentModel.ISupportInitialize)(this.nCantidad)).BeginInit();
            this.SuspendLayout();
            // 
            // nCantidad
            // 
            this.nCantidad.DecimalPlaces = 3;
            this.nCantidad.Dock = System.Windows.Forms.DockStyle.Left;
            this.nCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nCantidad.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nCantidad.Location = new System.Drawing.Point(0, 0);
            this.nCantidad.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.nCantidad.Name = "nCantidad";
            this.nCantidad.Size = new System.Drawing.Size(57, 22);
            this.nCantidad.TabIndex = 13;
            this.nCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nCantidad.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.nCantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nCantidad.Enter += new System.EventHandler(this.nCantidad_Enter);
            this.nCantidad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nCantidad_KeyDown);
            this.nCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nCantidad_KeyPress);
            // 
            // btConfig
            // 
            this.btConfig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btConfig.Checked = false;
            this.btConfig.Dock = System.Windows.Forms.DockStyle.Left;
            this.btConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btConfig.FormContainer = null;
            this.btConfig.GlobalHotKey = false;
            this.btConfig.HacerInvisibleAlDeshabilitar = false;
            this.btConfig.HotKey = System.Windows.Forms.Shortcut.None;
            this.btConfig.Image = global::gControls.Properties.Resources.Cancelar16;
            this.btConfig.Location = new System.Drawing.Point(351, 0);
            this.btConfig.Name = "btConfig";
            this.btConfig.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btConfig.RegistradoEnForm = false;
            this.btConfig.RequiereKey = false;
            this.btConfig.Size = new System.Drawing.Size(80, 23);
            this.btConfig.TabIndex = 20;
            this.btConfig.Text = "Configurar";
            this.btConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btConfig.Titulo = "Configuración";
            this.btConfig.ToolTipDescripcion = "Configúra la forma en que agrega productos al movimiento";
            this.btConfig.UseVisualStyleBackColor = true;
            this.btConfig.Click += new System.EventHandler(this.btConfig_Click);
            // 
            // btCotizacion
            // 
            this.btCotizacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCotizacion.Checked = false;
            this.btCotizacion.Dock = System.Windows.Forms.DockStyle.Left;
            this.btCotizacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCotizacion.FormContainer = null;
            this.btCotizacion.GlobalHotKey = false;
            this.btCotizacion.HacerInvisibleAlDeshabilitar = false;
            this.btCotizacion.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCotizacion.Image = global::gControls.Properties.Resources.Cotizar16;
            this.btCotizacion.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCotizacion.Location = new System.Drawing.Point(284, 0);
            this.btCotizacion.Name = "btCotizacion";
            this.btCotizacion.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btCotizacion.RegistradoEnForm = false;
            this.btCotizacion.RequiereKey = false;
            this.btCotizacion.Size = new System.Drawing.Size(67, 23);
            this.btCotizacion.TabIndex = 19;
            this.btCotizacion.Text = "Cotizar";
            this.btCotizacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCotizacion.Titulo = "Crear Cotización";
            this.btCotizacion.ToolTipDescripcion = "Agrega una cotización en moneda extranjera.";
            this.btCotizacion.UseVisualStyleBackColor = true;
            this.btCotizacion.Click += new System.EventHandler(this.btCotizacion_Click);
            // 
            // btActualizar
            // 
            this.btActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btActualizar.Checked = false;
            this.btActualizar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btActualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btActualizar.FormContainer = null;
            this.btActualizar.GlobalHotKey = false;
            this.btActualizar.HacerInvisibleAlDeshabilitar = false;
            this.btActualizar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btActualizar.Image = global::gControls.Properties.Resources.Buscar16;
            this.btActualizar.Location = new System.Drawing.Point(217, 0);
            this.btActualizar.Name = "btActualizar";
            this.btActualizar.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btActualizar.RegistradoEnForm = false;
            this.btActualizar.RequiereKey = false;
            this.btActualizar.Size = new System.Drawing.Size(67, 23);
            this.btActualizar.TabIndex = 16;
            this.btActualizar.Text = "Buscar";
            this.btActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btActualizar.Titulo = "Buscar Productos";
            this.btActualizar.ToolTipDescripcion = "Busca un producto o servicio para agregar al movimiento";
            this.btActualizar.UseVisualStyleBackColor = true;
            this.btActualizar.Click += new System.EventHandler(this.btBuscar_Click);
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodigo.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtCodigo.Location = new System.Drawing.Point(57, 0);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(160, 22);
            this.txtCodigo.TabIndex = 14;
            this.txtCodigo.WaterMarkColor = System.Drawing.Color.Gray;
            this.txtCodigo.WaterMarkText = "Ingrese código o serial";
            this.txtCodigo.Enter += new System.EventHandler(this.txtCodigo_Enter);
            this.txtCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigo_KeyDown);
            // 
            // btDestinatario
            // 
            this.btDestinatario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDestinatario.Checked = false;
            this.btDestinatario.Dock = System.Windows.Forms.DockStyle.Left;
            this.btDestinatario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDestinatario.FormContainer = null;
            this.btDestinatario.GlobalHotKey = false;
            this.btDestinatario.HacerInvisibleAlDeshabilitar = false;
            this.btDestinatario.HotKey = System.Windows.Forms.Shortcut.None;
            this.btDestinatario.Image = global::gControls.Properties.Resources.Aceptar16;
            this.btDestinatario.Location = new System.Drawing.Point(431, 0);
            this.btDestinatario.Name = "btDestinatario";
            this.btDestinatario.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btDestinatario.RegistradoEnForm = false;
            this.btDestinatario.RequiereKey = false;
            this.btDestinatario.Size = new System.Drawing.Size(96, 23);
            this.btDestinatario.TabIndex = 21;
            this.btDestinatario.Text = "Destinatario";
            this.btDestinatario.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDestinatario.Titulo = "Destinatario";
            this.btDestinatario.ToolTipDescripcion = "Cambia el destinatario del movimiento";
            this.btDestinatario.UseVisualStyleBackColor = true;
            this.btDestinatario.Click += new System.EventHandler(this.btDestinatario_Click);
            // 
            // PanelAgregarOM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btDestinatario);
            this.Controls.Add(this.btConfig);
            this.Controls.Add(this.btCotizacion);
            this.Controls.Add(this.btActualizar);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.nCantidad);
            this.Name = "PanelAgregarOM";
            this.Size = new System.Drawing.Size(573, 23);
            ((System.ComponentModel.ISupportInitialize)(this.nCantidad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nCantidad;
        private WaterMarkTextBox txtCodigo;
        private BotonAccion btActualizar;
        private BotonAccion btCotizacion;
        private BotonAccion btConfig;
        private BotonAccion btDestinatario;
    }
}
