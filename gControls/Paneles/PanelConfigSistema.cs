﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gControls.Paneles
{
    public partial class PanelConfigSistema : BaseControl
    {
        public PanelConfigSistema()
        {
            InitializeComponent();
            Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
        }

        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }

       
    }
}
