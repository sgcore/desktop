﻿namespace gControls.Paneles
{
    partial class PanelProductosTree
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelProductosTree));
            this.ProdImageList = new System.Windows.Forms.ImageList(this.components);
            this.tw1 = new BrightIdeasSoftware.DataTreeListView();
            this.colNombre = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colPrecio = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colStock = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.colCodigo = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.txtBuscar = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuExpandir = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuContraer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuActualizar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuCrear = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuCrearProducto = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCrearServicio = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuCrearCategoria = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCrearGrupo = new System.Windows.Forms.ToolStripMenuItem();
            this.btEdit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btEliminar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btGlobalSel = new System.Windows.Forms.ToolStripButton();
            this.btHoldGrupos = new System.Windows.Forms.ToolStripButton();
            this.btSincronizar = new System.Windows.Forms.ToolStripButton();
            this.btDesincronizar = new System.Windows.Forms.ToolStripButton();
            this.lblFiltro = new System.Windows.Forms.ToolStripLabel();
            this.BuscadorGrupos = new gControls.Buscadores.BuscadorProductos();
            ((System.ComponentModel.ISupportInitialize)(this.tw1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProdImageList
            // 
            this.ProdImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ProdImageList.ImageStream")));
            this.ProdImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.ProdImageList.Images.SetKeyName(0, "Producto");
            this.ProdImageList.Images.SetKeyName(1, "Categoria");
            this.ProdImageList.Images.SetKeyName(2, "Grupo");
            this.ProdImageList.Images.SetKeyName(3, "Servicio");
            this.ProdImageList.Images.SetKeyName(4, "Precio");
            // 
            // tw1
            // 
            this.tw1.AllColumns.Add(this.colNombre);
            this.tw1.AllColumns.Add(this.colPrecio);
            this.tw1.AllColumns.Add(this.colStock);
            this.tw1.AllColumns.Add(this.colCodigo);
            this.tw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNombre,
            this.colPrecio,
            this.colStock,
            this.colCodigo});
            this.tw1.DataSource = null;
            this.tw1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tw1.FullRowSelect = true;
            this.tw1.IsSimpleDragSource = true;
            this.tw1.LargeImageList = this.ProdImageList;
            this.tw1.Location = new System.Drawing.Point(0, 25);
            this.tw1.Name = "tw1";
            this.tw1.OwnerDraw = true;
            this.tw1.RootKeyValueString = "";
            this.tw1.ShowGroups = false;
            this.tw1.Size = new System.Drawing.Size(623, 381);
            this.tw1.SmallImageList = this.ProdImageList;
            this.tw1.TabIndex = 0;
            this.tw1.UseCellFormatEvents = true;
            this.tw1.UseCompatibleStateImageBehavior = false;
            this.tw1.UseExplorerTheme = true;
            this.tw1.UseFilterIndicator = true;
            this.tw1.UseFiltering = true;
            this.tw1.View = System.Windows.Forms.View.Details;
            this.tw1.VirtualMode = true;
            this.tw1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.tw1_ItemSelectionChanged);
            this.tw1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tw1_KeyDown);
            this.tw1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tw1_MouseDoubleClick);
            // 
            // colNombre
            // 
            this.colNombre.AspectName = "Nombre";
            this.colNombre.MaximumWidth = 500;
            this.colNombre.MinimumWidth = 100;
            this.colNombre.Text = "Producto";
            this.colNombre.Width = 285;
            // 
            // colPrecio
            // 
            this.colPrecio.AspectName = "Precio";
            this.colPrecio.AspectToStringFormat = "{0:$0.00}";
            this.colPrecio.MaximumWidth = 70;
            this.colPrecio.Text = "Precio";
            this.colPrecio.Width = 70;
            // 
            // colStock
            // 
            this.colStock.AspectName = "Stock";
            this.colStock.AspectToStringFormat = "{0:0.00}";
            this.colStock.MaximumWidth = 70;
            this.colStock.Text = "Stock";
            // 
            // colCodigo
            // 
            this.colCodigo.AspectName = "Codigo";
            this.colCodigo.FillsFreeSpace = true;
            this.colCodigo.Text = "Codigo";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackgroundImage = global::gControls.Properties.Resources.FondoGris;
            this.toolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtBuscar,
            this.toolStripDropDownButton1,
            this.toolStripSeparator1,
            this.mnuCrear,
            this.btEdit,
            this.toolStripSeparator2,
            this.btEliminar,
            this.toolStripSeparator3,
            this.btGlobalSel,
            this.btHoldGrupos,
            this.btSincronizar,
            this.btDesincronizar,
            this.lblFiltro});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(623, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(150, 25);
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExpandir,
            this.mnuContraer,
            this.mnuActualizar});
            this.toolStripDropDownButton1.Image = global::gControls.Properties.Resources.Expandir;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 22);
            this.toolStripDropDownButton1.Text = "mnuExpandirContraer";
            this.toolStripDropDownButton1.ToolTipText = "Expandir/Contraer el arbol de categorias";
            // 
            // mnuExpandir
            // 
            this.mnuExpandir.Image = global::gControls.Properties.Resources.Expandir;
            this.mnuExpandir.Name = "mnuExpandir";
            this.mnuExpandir.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Right)));
            this.mnuExpandir.Size = new System.Drawing.Size(181, 22);
            this.mnuExpandir.Text = "Expandir";
            this.mnuExpandir.Click += new System.EventHandler(this.btExpand_Click);
            // 
            // mnuContraer
            // 
            this.mnuContraer.Image = global::gControls.Properties.Resources.Contraer;
            this.mnuContraer.Name = "mnuContraer";
            this.mnuContraer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Left)));
            this.mnuContraer.Size = new System.Drawing.Size(181, 22);
            this.mnuContraer.Text = "Contraer";
            this.mnuContraer.Click += new System.EventHandler(this.btContraer_Click);
            // 
            // mnuActualizar
            // 
            this.mnuActualizar.Image = global::gControls.Properties.Resources.Actualizar;
            this.mnuActualizar.Name = "mnuActualizar";
            this.mnuActualizar.Size = new System.Drawing.Size(181, 22);
            this.mnuActualizar.Text = "Actualizar";
            this.mnuActualizar.Click += new System.EventHandler(this.mnuActualizar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // mnuCrear
            // 
            this.mnuCrear.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCrearProducto,
            this.mnuCrearServicio,
            this.toolStripMenuItem1,
            this.mnuCrearCategoria,
            this.mnuCrearGrupo});
            this.mnuCrear.Image = global::gControls.Properties.Resources.Guardar;
            this.mnuCrear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuCrear.Name = "mnuCrear";
            this.mnuCrear.Size = new System.Drawing.Size(71, 22);
            this.mnuCrear.Text = "Nuevo";
            // 
            // mnuCrearProducto
            // 
            this.mnuCrearProducto.Image = global::gControls.Properties.Resources.Producto;
            this.mnuCrearProducto.Name = "mnuCrearProducto";
            this.mnuCrearProducto.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.P)));
            this.mnuCrearProducto.Size = new System.Drawing.Size(193, 22);
            this.mnuCrearProducto.Text = "Crear Producto";
            this.mnuCrearProducto.Click += new System.EventHandler(this.mnuCrearProducto_Click);
            // 
            // mnuCrearServicio
            // 
            this.mnuCrearServicio.Image = global::gControls.Properties.Resources.Servicio;
            this.mnuCrearServicio.Name = "mnuCrearServicio";
            this.mnuCrearServicio.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.mnuCrearServicio.Size = new System.Drawing.Size(193, 22);
            this.mnuCrearServicio.Text = "Crear Servicio";
            this.mnuCrearServicio.Click += new System.EventHandler(this.mnuCrearServicio_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(190, 6);
            // 
            // mnuCrearCategoria
            // 
            this.mnuCrearCategoria.Image = global::gControls.Properties.Resources.Categoria;
            this.mnuCrearCategoria.Name = "mnuCrearCategoria";
            this.mnuCrearCategoria.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.T)));
            this.mnuCrearCategoria.Size = new System.Drawing.Size(193, 22);
            this.mnuCrearCategoria.Text = "Crear Categoría";
            this.mnuCrearCategoria.Click += new System.EventHandler(this.mnuCrearCategoria_Click);
            // 
            // mnuCrearGrupo
            // 
            this.mnuCrearGrupo.Image = global::gControls.Properties.Resources.Grupo;
            this.mnuCrearGrupo.Name = "mnuCrearGrupo";
            this.mnuCrearGrupo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.G)));
            this.mnuCrearGrupo.Size = new System.Drawing.Size(193, 22);
            this.mnuCrearGrupo.Text = "Crear Grupo";
            this.mnuCrearGrupo.Click += new System.EventHandler(this.mnuCrearGrupo_Click);
            // 
            // btEdit
            // 
            this.btEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btEdit.Image = global::gControls.Properties.Resources.Editar;
            this.btEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEdit.Name = "btEdit";
            this.btEdit.Size = new System.Drawing.Size(23, 22);
            this.btEdit.Text = "Editar";
            this.btEdit.ToolTipText = "Edita el producto, categoria, grupo o servicio seleccionado";
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btEliminar
            // 
            this.btEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btEliminar.Image = global::gControls.Properties.Resources.Eliminar;
            this.btEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEliminar.Name = "btEliminar";
            this.btEliminar.Size = new System.Drawing.Size(23, 22);
            this.btEliminar.Text = "Eliminar";
            this.btEliminar.ToolTipText = "Eliminar de ser posible";
            this.btEliminar.Click += new System.EventHandler(this.btEliminar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btGlobalSel
            // 
            this.btGlobalSel.CheckOnClick = true;
            this.btGlobalSel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btGlobalSel.Image = global::gControls.Properties.Resources.Up_curve;
            this.btGlobalSel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGlobalSel.Name = "btGlobalSel";
            this.btGlobalSel.Size = new System.Drawing.Size(23, 22);
            this.btGlobalSel.Text = "Selección Global";
            this.btGlobalSel.ToolTipText = "Activar para editar la selección con doble-click";
            this.btGlobalSel.CheckedChanged += new System.EventHandler(this.btGlobalSel_CheckedChanged);
            // 
            // btHoldGrupos
            // 
            this.btHoldGrupos.CheckOnClick = true;
            this.btHoldGrupos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btHoldGrupos.Image = global::gControls.Properties.Resources.Fijar;
            this.btHoldGrupos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btHoldGrupos.Name = "btHoldGrupos";
            this.btHoldGrupos.Size = new System.Drawing.Size(23, 22);
            this.btHoldGrupos.Text = "Fijar";
            this.btHoldGrupos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btHoldGrupos.ToolTipText = "Mantiene visible el panel de grupos permitiendo agregar productos.";
            this.btHoldGrupos.CheckedChanged += new System.EventHandler(this.btHoldGrupos_CheckedChanged);
            // 
            // btSincronizar
            // 
            this.btSincronizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btSincronizar.Enabled = false;
            this.btSincronizar.Image = global::gControls.Properties.Resources.Conectar;
            this.btSincronizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSincronizar.Name = "btSincronizar";
            this.btSincronizar.Size = new System.Drawing.Size(23, 22);
            this.btSincronizar.Text = "Sincronizar";
            this.btSincronizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btSincronizar.ToolTipText = "Sincroniza este producto con el servidor en internet.";
            this.btSincronizar.Click += new System.EventHandler(this.btSincronizar_Click);
            // 
            // btDesincronizar
            // 
            this.btDesincronizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btDesincronizar.Enabled = false;
            this.btDesincronizar.Image = global::gControls.Properties.Resources.Desconectar;
            this.btDesincronizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDesincronizar.Name = "btDesincronizar";
            this.btDesincronizar.Size = new System.Drawing.Size(23, 22);
            this.btDesincronizar.Text = "Desincronizar";
            this.btDesincronizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btDesincronizar.ToolTipText = "Elimina el producto en el servidor.";
            this.btDesincronizar.Click += new System.EventHandler(this.btDesincronizar_Click);
            // 
            // lblFiltro
            // 
            this.lblFiltro.Name = "lblFiltro";
            this.lblFiltro.Size = new System.Drawing.Size(54, 22);
            this.lblFiltro.Text = "Sin filtrar";
            // 
            // BuscadorGrupos
            // 
            this.BuscadorGrupos.BackColor = System.Drawing.Color.LightGreen;
           this.BuscadorGrupos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BuscadorGrupos.Location = new System.Drawing.Point(0, 406);
            this.BuscadorGrupos.Name = "BuscadorGrupos";
            this.BuscadorGrupos.OcultarSiEstaVacio = false;
            this.BuscadorGrupos.SeleccionGlobal = true;
            this.BuscadorGrupos.Size = new System.Drawing.Size(623, 190);
            this.BuscadorGrupos.TabIndex = 2;
             this.BuscadorGrupos.Titulo = "Otros Productos del grupo";
            this.BuscadorGrupos.Visible = false;
            // 
            // PanelProductosTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tw1);
            this.Controls.Add(this.BuscadorGrupos);
            this.Controls.Add(this.toolStrip1);
            this.Name = "PanelProductosTree";
            this.Size = new System.Drawing.Size(623, 596);
            ((System.ComponentModel.ISupportInitialize)(this.tw1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BrightIdeasSoftware.DataTreeListView tw1;
        private BrightIdeasSoftware.OLVColumn colNombre;
        private BrightIdeasSoftware.OLVColumn colPrecio;
        private BrightIdeasSoftware.OLVColumn colStock;
        private System.Windows.Forms.ImageList ProdImageList;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox txtBuscar;
        private System.Windows.Forms.ToolStripButton btEdit;
        private System.Windows.Forms.ToolStripLabel lblFiltro;
        private Buscadores.BuscadorProductos BuscadorGrupos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton mnuCrear;
        private System.Windows.Forms.ToolStripMenuItem mnuCrearProducto;
        private System.Windows.Forms.ToolStripMenuItem mnuCrearServicio;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuCrearCategoria;
        private System.Windows.Forms.ToolStripMenuItem mnuCrearGrupo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btEliminar;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem mnuExpandir;
        private System.Windows.Forms.ToolStripMenuItem mnuContraer;
        private System.Windows.Forms.ToolStripMenuItem mnuActualizar;
        private System.Windows.Forms.ToolStripButton btGlobalSel;
        private System.Windows.Forms.ToolStripButton btHoldGrupos;
        private BrightIdeasSoftware.OLVColumn colCodigo;
        private System.Windows.Forms.ToolStripButton btSincronizar;
        private System.Windows.Forms.ToolStripButton btDesincronizar;

    }
}
