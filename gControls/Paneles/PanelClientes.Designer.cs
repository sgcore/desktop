﻿namespace gControls.Paneles
{
    partial class PanelClientes
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelClientes));
            this.MovImageList = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCliente = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.buscador1 = new gControls.Buscadores.BuscadorDestinatarios();
            this.btCancelCliente = new gControls.BotonAccion();
            this.btExtraccionCliente = new gControls.BotonAccion();
            this.btDepositoCliente = new gControls.BotonAccion();
            this.btPresupuesto = new gControls.BotonAccion();
            this.btDevolucion = new gControls.BotonAccion();
            this.btVenta = new gControls.BotonAccion();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MovImageList
            // 
            this.MovImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MovImageList.ImageStream")));
            this.MovImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MovImageList.Images.SetKeyName(0, "Compra");
            this.MovImageList.Images.SetKeyName(1, "Retorno");
            this.MovImageList.Images.SetKeyName(2, "Venta");
            this.MovImageList.Images.SetKeyName(3, "Devolucion");
            this.MovImageList.Images.SetKeyName(4, "Entrada");
            this.MovImageList.Images.SetKeyName(5, "Salida");
            this.MovImageList.Images.SetKeyName(6, "Extraccion");
            this.MovImageList.Images.SetKeyName(7, "Pedido");
            this.MovImageList.Images.SetKeyName(8, "Presupuesto");
            this.MovImageList.Images.SetKeyName(9, "Terminado");
            this.MovImageList.Images.SetKeyName(10, "Creado");
            this.MovImageList.Images.SetKeyName(11, "Editado");
            this.MovImageList.Images.SetKeyName(12, "Buscar");
            this.MovImageList.Images.SetKeyName(13, "Cancelar");
            this.MovImageList.Images.SetKeyName(14, "Deposito");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblCliente);
            this.panel1.Controls.Add(this.btCancelCliente);
            this.panel1.Controls.Add(this.btExtraccionCliente);
            this.panel1.Controls.Add(this.btDepositoCliente);
            this.panel1.Controls.Add(this.btPresupuesto);
            this.panel1.Controls.Add(this.btDevolucion);
            this.panel1.Controls.Add(this.btVenta);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(544, 69);
            this.panel1.TabIndex = 84;
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.Color.Transparent;
            this.lblCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(283, 20);
            this.lblCliente.Margin = new System.Windows.Forms.Padding(0);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(227, 49);
            this.lblCliente.TabIndex = 79;
            this.lblCliente.Text = "Ninguno";
            this.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Silver;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(544, 20);
            this.label12.TabIndex = 77;
            this.label12.Text = "ACCIONES CON CLIENTES";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buscador1
            // 
            this.buscador1.BackColor = System.Drawing.Color.LightSeaGreen;
             this.buscador1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscador1.Location = new System.Drawing.Point(0, 69);
            this.buscador1.Name = "buscador1";
            this.buscador1.OcultarSiEstaVacio = false;
            this.buscador1.SeleccionGlobal = false;
            this.buscador1.Size = new System.Drawing.Size(544, 469);
            this.buscador1.TabIndex = 86;
             this.buscador1.Titulo = "Buscar Cliente";
           
            // 
            // btCancelCliente
            // 
            this.btCancelCliente.BackColor = System.Drawing.Color.Transparent;
            this.btCancelCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCancelCliente.Dock = System.Windows.Forms.DockStyle.Right;
            this.btCancelCliente.GlobalHotKey = false;
            this.btCancelCliente.HacerInvisibleAlDeshabilitar = false;
            this.btCancelCliente.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCancelCliente.ImageKey = "Cancelar";
            this.btCancelCliente.ImageList = this.MovImageList;
            this.btCancelCliente.Location = new System.Drawing.Point(510, 20);
            this.btCancelCliente.Name = "btCancelCliente";
            this.btCancelCliente.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btCancelCliente.Size = new System.Drawing.Size(34, 49);
            this.btCancelCliente.TabIndex = 78;
            this.btCancelCliente.Titulo = "Eliminar Selección";
            this.btCancelCliente.ToolTipDescripcion = "Elimina el cliente seleccionado para realizar acciones.";
            this.btCancelCliente.UseVisualStyleBackColor = false;
            this.btCancelCliente.Click += new System.EventHandler(this.btCancelCliente_Click);
            // 
            // btExtraccionCliente
            // 
            this.btExtraccionCliente.AutoSize = true;
            this.btExtraccionCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btExtraccionCliente.Dock = System.Windows.Forms.DockStyle.Left;
            this.btExtraccionCliente.Enabled = false;
            this.btExtraccionCliente.GlobalHotKey = false;
            this.btExtraccionCliente.HacerInvisibleAlDeshabilitar = false;
            this.btExtraccionCliente.HotKey = System.Windows.Forms.Shortcut.None;
            this.btExtraccionCliente.ImageKey = "Extraccion";
            this.btExtraccionCliente.ImageList = this.MovImageList;
            this.btExtraccionCliente.Location = new System.Drawing.Point(220, 20);
            this.btExtraccionCliente.Name = "btExtraccionCliente";
            this.btExtraccionCliente.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btExtraccionCliente.Size = new System.Drawing.Size(63, 49);
            this.btExtraccionCliente.TabIndex = 81;
            this.btExtraccionCliente.Text = "Reintegro";
            this.btExtraccionCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btExtraccionCliente.Titulo = "Reintegro de dinero";
            this.btExtraccionCliente.ToolTipDescripcion = "Extrae dinero de su cuenta corriente.";
            this.btExtraccionCliente.UseVisualStyleBackColor = true;
            this.btExtraccionCliente.Click += new System.EventHandler(this.btExtraccionCliente_Click);
            // 
            // btDepositoCliente
            // 
            this.btDepositoCliente.AutoSize = true;
            this.btDepositoCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDepositoCliente.Dock = System.Windows.Forms.DockStyle.Left;
            this.btDepositoCliente.Enabled = false;
            this.btDepositoCliente.GlobalHotKey = false;
            this.btDepositoCliente.HacerInvisibleAlDeshabilitar = false;
            this.btDepositoCliente.HotKey = System.Windows.Forms.Shortcut.None;
            this.btDepositoCliente.ImageKey = "Deposito";
            this.btDepositoCliente.ImageList = this.MovImageList;
            this.btDepositoCliente.Location = new System.Drawing.Point(161, 20);
            this.btDepositoCliente.Name = "btDepositoCliente";
            this.btDepositoCliente.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btDepositoCliente.Size = new System.Drawing.Size(59, 49);
            this.btDepositoCliente.TabIndex = 80;
            this.btDepositoCliente.Text = "Deposito";
            this.btDepositoCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btDepositoCliente.Titulo = "Depósito de cliente";
            this.btDepositoCliente.ToolTipDescripcion = "agrega credito a su cuenta corriente.";
            this.btDepositoCliente.UseVisualStyleBackColor = true;
            this.btDepositoCliente.Click += new System.EventHandler(this.btDepositoCliente_Click);
            // 
            // btPresupuesto
            // 
            this.btPresupuesto.AutoSize = true;
            this.btPresupuesto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPresupuesto.Dock = System.Windows.Forms.DockStyle.Left;
            this.btPresupuesto.Enabled = false;
            this.btPresupuesto.GlobalHotKey = false;
            this.btPresupuesto.HacerInvisibleAlDeshabilitar = false;
            this.btPresupuesto.HotKey = System.Windows.Forms.Shortcut.None;
            this.btPresupuesto.ImageKey = "Presupuesto";
            this.btPresupuesto.ImageList = this.MovImageList;
            this.btPresupuesto.Location = new System.Drawing.Point(108, 20);
            this.btPresupuesto.Name = "btPresupuesto";
            this.btPresupuesto.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btPresupuesto.Size = new System.Drawing.Size(53, 49);
            this.btPresupuesto.TabIndex = 70;
            this.btPresupuesto.Text = "Presup.";
            this.btPresupuesto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btPresupuesto.Titulo = "Presupuesto a cliente";
            this.btPresupuesto.ToolTipDescripcion = "Crea un presupuesto a cliente.";
            this.btPresupuesto.UseVisualStyleBackColor = true;
            this.btPresupuesto.Click += new System.EventHandler(this.btPresupuesto_Click);
            // 
            // btDevolucion
            // 
            this.btDevolucion.AutoSize = true;
            this.btDevolucion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDevolucion.Dock = System.Windows.Forms.DockStyle.Left;
            this.btDevolucion.Enabled = false;
            this.btDevolucion.GlobalHotKey = false;
            this.btDevolucion.HacerInvisibleAlDeshabilitar = false;
            this.btDevolucion.HotKey = System.Windows.Forms.Shortcut.None;
            this.btDevolucion.ImageKey = "Devolucion";
            this.btDevolucion.ImageList = this.MovImageList;
            this.btDevolucion.Location = new System.Drawing.Point(53, 20);
            this.btDevolucion.Name = "btDevolucion";
            this.btDevolucion.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btDevolucion.Size = new System.Drawing.Size(55, 49);
            this.btDevolucion.TabIndex = 58;
            this.btDevolucion.Text = "Retorno";
            this.btDevolucion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btDevolucion.Titulo = "Devolucion de Mercaderia";
            this.btDevolucion.ToolTipDescripcion = "Devolución por parte del cliente de mercaderías";
            this.btDevolucion.UseVisualStyleBackColor = true;
            this.btDevolucion.Click += new System.EventHandler(this.btDevolucion_Click);
            // 
            // btVenta
            // 
            this.btVenta.AutoSize = true;
            this.btVenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btVenta.Dock = System.Windows.Forms.DockStyle.Left;
            this.btVenta.Enabled = false;
            this.btVenta.GlobalHotKey = true;
            this.btVenta.HacerInvisibleAlDeshabilitar = false;
            this.btVenta.HotKey = System.Windows.Forms.Shortcut.CtrlF1;
            this.btVenta.ImageKey = "Venta";
            this.btVenta.ImageList = this.MovImageList;
            this.btVenta.Location = new System.Drawing.Point(0, 20);
            this.btVenta.Name = "btVenta";
            this.btVenta.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btVenta.Size = new System.Drawing.Size(53, 49);
            this.btVenta.TabIndex = 54;
            this.btVenta.Text = "Venta";
            this.btVenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btVenta.Titulo = "Venta a Cliente";
            this.btVenta.ToolTipDescripcion = "Crea una venta al último cliente seleccionado.| Si utiliza el acceso directo o la" +
    " tecla Control al pulsar el botón, le preguntará el cliente de destino.";
            this.btVenta.UseVisualStyleBackColor = true;
            this.btVenta.Click += new System.EventHandler(this.btVenta_Click);
            // 
            // PanelClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buscador1);
            this.Controls.Add(this.panel1);
            this.Name = "PanelClientes";
            this.Size = new System.Drawing.Size(544, 538);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList MovImageList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCliente;
        private BotonAccion btCancelCliente;
        private BotonAccion btExtraccionCliente;
        private BotonAccion btDepositoCliente;
        private BotonAccion btPresupuesto;
        private BotonAccion btDevolucion;
        private BotonAccion btVenta;
        private System.Windows.Forms.Label label12;
        private Buscadores.BuscadorDestinatarios buscador1;

    }
}
