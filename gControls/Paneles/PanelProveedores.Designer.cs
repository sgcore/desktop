﻿namespace gControls.Paneles
{
    partial class PanelProveedores
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PanelProveedores));
            this.MovImageList = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblProveedor = new System.Windows.Forms.Label();
            this.btCancelProveedor = new gControls.BotonAccion();
            this.btExtraccionProv = new gControls.BotonAccion();
            this.btDepositoProv = new gControls.BotonAccion();
            this.btPedido = new gControls.BotonAccion();
            this.btRetorno = new gControls.BotonAccion();
            this.btCompra = new gControls.BotonAccion();
            this.label14 = new System.Windows.Forms.Label();
            this.buscador1 = new gControls.Buscadores.BuscadorDestinatarios();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MovImageList
            // 
            this.MovImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("MovImageList.ImageStream")));
            this.MovImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.MovImageList.Images.SetKeyName(0, "Compra");
            this.MovImageList.Images.SetKeyName(1, "Retorno");
            this.MovImageList.Images.SetKeyName(2, "Venta");
            this.MovImageList.Images.SetKeyName(3, "Devolucion");
            this.MovImageList.Images.SetKeyName(4, "Entrada");
            this.MovImageList.Images.SetKeyName(5, "Salida");
            this.MovImageList.Images.SetKeyName(6, "Deposito");
            this.MovImageList.Images.SetKeyName(7, "Extraccion");
            this.MovImageList.Images.SetKeyName(8, "Pedido");
            this.MovImageList.Images.SetKeyName(9, "Presupuesto");
            this.MovImageList.Images.SetKeyName(10, "Terminado");
            this.MovImageList.Images.SetKeyName(11, "Creado");
            this.MovImageList.Images.SetKeyName(12, "Editado");
            this.MovImageList.Images.SetKeyName(13, "Buscar");
            this.MovImageList.Images.SetKeyName(14, "Cancelar");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblProveedor);
            this.panel1.Controls.Add(this.btCancelProveedor);
            this.panel1.Controls.Add(this.btExtraccionProv);
            this.panel1.Controls.Add(this.btDepositoProv);
            this.panel1.Controls.Add(this.btPedido);
            this.panel1.Controls.Add(this.btRetorno);
            this.panel1.Controls.Add(this.btCompra);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(698, 70);
            this.panel1.TabIndex = 90;
            // 
            // lblProveedor
            // 
            this.lblProveedor.BackColor = System.Drawing.Color.Transparent;
            this.lblProveedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProveedor.Location = new System.Drawing.Point(283, 20);
            this.lblProveedor.Name = "lblProveedor";
            this.lblProveedor.Size = new System.Drawing.Size(381, 50);
            this.lblProveedor.TabIndex = 82;
            this.lblProveedor.Text = "Ninguno";
            this.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btCancelProveedor
            // 
            this.btCancelProveedor.BackColor = System.Drawing.Color.Transparent;
            this.btCancelProveedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCancelProveedor.Dock = System.Windows.Forms.DockStyle.Right;
            this.btCancelProveedor.GlobalHotKey = false;
            this.btCancelProveedor.HacerInvisibleAlDeshabilitar = false;
            this.btCancelProveedor.HotKey = System.Windows.Forms.Shortcut.None;
            this.btCancelProveedor.ImageKey = "Cancelar";
            this.btCancelProveedor.ImageList = this.MovImageList;
            this.btCancelProveedor.Location = new System.Drawing.Point(664, 20);
            this.btCancelProveedor.Name = "btCancelProveedor";
            this.btCancelProveedor.Permiso = gManager.gcUsuario.PermisoEnum.Visitante;
            this.btCancelProveedor.Size = new System.Drawing.Size(34, 50);
            this.btCancelProveedor.TabIndex = 81;
            this.btCancelProveedor.Titulo = "Cancelar Selección";
            this.btCancelProveedor.ToolTipDescripcion = "Elimina la selección de proveedor";
            this.btCancelProveedor.UseVisualStyleBackColor = false;
            this.btCancelProveedor.Click += new System.EventHandler(this.btCancelProveedor_Click);
            // 
            // btExtraccionProv
            // 
            this.btExtraccionProv.AutoSize = true;
            this.btExtraccionProv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btExtraccionProv.Dock = System.Windows.Forms.DockStyle.Left;
            this.btExtraccionProv.Enabled = false;
            this.btExtraccionProv.GlobalHotKey = false;
            this.btExtraccionProv.HacerInvisibleAlDeshabilitar = false;
            this.btExtraccionProv.HotKey = System.Windows.Forms.Shortcut.None;
            this.btExtraccionProv.ImageKey = "Extraccion";
            this.btExtraccionProv.ImageList = this.MovImageList;
            this.btExtraccionProv.Location = new System.Drawing.Point(220, 20);
            this.btExtraccionProv.Name = "btExtraccionProv";
            this.btExtraccionProv.Permiso = gManager.gcUsuario.PermisoEnum.Contador;
            this.btExtraccionProv.Size = new System.Drawing.Size(63, 50);
            this.btExtraccionProv.TabIndex = 68;
            this.btExtraccionProv.Text = "Reintegro";
            this.btExtraccionProv.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btExtraccionProv.Titulo = "Reintegro de Efectivo";
            this.btExtraccionProv.ToolTipDescripcion = "Devolución de dinero por parte del proveedor";
            this.btExtraccionProv.UseVisualStyleBackColor = true;
            this.btExtraccionProv.Click += new System.EventHandler(this.btExtraccionProv_Click);
            // 
            // btDepositoProv
            // 
            this.btDepositoProv.AutoSize = true;
            this.btDepositoProv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDepositoProv.Dock = System.Windows.Forms.DockStyle.Left;
            this.btDepositoProv.Enabled = false;
            this.btDepositoProv.GlobalHotKey = false;
            this.btDepositoProv.HacerInvisibleAlDeshabilitar = false;
            this.btDepositoProv.HotKey = System.Windows.Forms.Shortcut.None;
            this.btDepositoProv.ImageKey = "Deposito";
            this.btDepositoProv.ImageList = this.MovImageList;
            this.btDepositoProv.Location = new System.Drawing.Point(161, 20);
            this.btDepositoProv.Name = "btDepositoProv";
            this.btDepositoProv.Permiso = gManager.gcUsuario.PermisoEnum.Contador;
            this.btDepositoProv.Size = new System.Drawing.Size(59, 50);
            this.btDepositoProv.TabIndex = 66;
            this.btDepositoProv.Text = "Deposito";
            this.btDepositoProv.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btDepositoProv.Titulo = "Deposito en Proveedor";
            this.btDepositoProv.ToolTipDescripcion = "Deposito en proveedor implica una SALIDA de dinero de la caja.";
            this.btDepositoProv.UseVisualStyleBackColor = true;
            this.btDepositoProv.Click += new System.EventHandler(this.btDepositoProv_Click);
            // 
            // btPedido
            // 
            this.btPedido.AutoSize = true;
            this.btPedido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPedido.Dock = System.Windows.Forms.DockStyle.Left;
            this.btPedido.Enabled = false;
            this.btPedido.GlobalHotKey = false;
            this.btPedido.HacerInvisibleAlDeshabilitar = false;
            this.btPedido.HotKey = System.Windows.Forms.Shortcut.None;
            this.btPedido.ImageKey = "Pedido";
            this.btPedido.ImageList = this.MovImageList;
            this.btPedido.Location = new System.Drawing.Point(108, 20);
            this.btPedido.Name = "btPedido";
            this.btPedido.Permiso = gManager.gcUsuario.PermisoEnum.Contador;
            this.btPedido.Size = new System.Drawing.Size(53, 50);
            this.btPedido.TabIndex = 72;
            this.btPedido.Text = "Pedido";
            this.btPedido.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btPedido.Titulo = "Pedido a Proveedor";
            this.btPedido.ToolTipDescripcion = "Crea un formulario de pedido de mercaderias para enviar al proveedor.";
            this.btPedido.UseVisualStyleBackColor = true;
            this.btPedido.Click += new System.EventHandler(this.btPedido_Click);
            // 
            // btRetorno
            // 
            this.btRetorno.AutoSize = true;
            this.btRetorno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btRetorno.Dock = System.Windows.Forms.DockStyle.Left;
            this.btRetorno.Enabled = false;
            this.btRetorno.GlobalHotKey = false;
            this.btRetorno.HacerInvisibleAlDeshabilitar = false;
            this.btRetorno.HotKey = System.Windows.Forms.Shortcut.None;
            this.btRetorno.ImageKey = "Retorno";
            this.btRetorno.ImageList = this.MovImageList;
            this.btRetorno.Location = new System.Drawing.Point(53, 20);
            this.btRetorno.Name = "btRetorno";
            this.btRetorno.Permiso = gManager.gcUsuario.PermisoEnum.Contador;
            this.btRetorno.Size = new System.Drawing.Size(55, 50);
            this.btRetorno.TabIndex = 60;
            this.btRetorno.Text = "Retorno";
            this.btRetorno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btRetorno.Titulo = "Retorno de Mercaderia";
            this.btRetorno.ToolTipDescripcion = "Devolución de mercaderia a proveedor";
            this.btRetorno.UseVisualStyleBackColor = true;
            this.btRetorno.Click += new System.EventHandler(this.btRetorno_Click);
            // 
            // btCompra
            // 
            this.btCompra.AutoSize = true;
            this.btCompra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCompra.Dock = System.Windows.Forms.DockStyle.Left;
            this.btCompra.Enabled = false;
            this.btCompra.GlobalHotKey = true;
            this.btCompra.HacerInvisibleAlDeshabilitar = false;
            this.btCompra.HotKey = System.Windows.Forms.Shortcut.CtrlF11;
            this.btCompra.ImageKey = "Compra";
            this.btCompra.ImageList = this.MovImageList;
            this.btCompra.Location = new System.Drawing.Point(0, 20);
            this.btCompra.Name = "btCompra";
            this.btCompra.Permiso = gManager.gcUsuario.PermisoEnum.Contador;
            this.btCompra.Size = new System.Drawing.Size(53, 50);
            this.btCompra.TabIndex = 56;
            this.btCompra.Text = "Compra";
            this.btCompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btCompra.Titulo = "Compra a Proveedor";
            this.btCompra.ToolTipDescripcion = "Crea una Compra al último Proveedor seleccionado.| Si utiliza el acceso directo o" +
    " la tecla Control al pulsar el botón, le preguntará el Proveedor de destino.";
            this.btCompra.UseVisualStyleBackColor = true;
            this.btCompra.Click += new System.EventHandler(this.btCompra_Click);
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Silver;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(698, 20);
            this.label14.TabIndex = 80;
            this.label14.Text = "ACCIONES CON PROVEEDORES";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buscador1
            // 
            this.buscador1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.buscador1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buscador1.Location = new System.Drawing.Point(0, 70);
            this.buscador1.Name = "buscador1";
             this.buscador1.SeleccionGlobal = false;
            this.buscador1.Size = new System.Drawing.Size(698, 605);
            this.buscador1.TabIndex = 92;
            this.buscador1.Titulo = "Buscar Proveedor";
            // 
            // PanelProveedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buscador1);
            this.Controls.Add(this.panel1);
            this.Name = "PanelProveedores";
            this.Size = new System.Drawing.Size(698, 675);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList MovImageList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblProveedor;
        private BotonAccion btCancelProveedor;
        private BotonAccion btExtraccionProv;
        private BotonAccion btDepositoProv;
        private BotonAccion btPedido;
        private BotonAccion btRetorno;
        private BotonAccion btCompra;
        private System.Windows.Forms.Label label14;
        private Buscadores.BuscadorDestinatarios buscador1;
    }
}
