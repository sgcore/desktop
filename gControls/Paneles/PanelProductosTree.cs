﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gManager;
using BrightIdeasSoftware;
using System.Collections;
using System.Diagnostics;

namespace gControls.Paneles
{
    public partial class PanelProductosTree : BaseControl
    {
        public event CoreManager.ProductoDelegate ProductoSeleccionado;
        public PanelProductosTree()
        {
            InitializeComponent();
            Permiso = gcUsuario.PermisoEnum.Vendedor;
            this.tw1.CanExpandGetter = delegate(object x)
            {
                return (x is gManager.gcCategoria && (x as gManager.gcCategoria).SubItems.Count>0);
            };
            this.tw1.ChildrenGetter = delegate(object x)
            {
                gcObjeto dir = (gcObjeto)x;
                return dir.SubItems;
            };
            tw1.FormatCell += Productos_FormatCell;
            colNombre.ImageGetter = delegate(object rowObject)
            {
                gcObjeto s = (gcObjeto)rowObject;
                return s.Tipo.ToString();
            };
            colPrecio.ImageGetter = delegate(object rowObject)
            {
                gcObjeto s = (gcObjeto)rowObject;
                if (s is gcCategoria || s is gcGrupo)
                    return "";
                return "Precio";
            };
            BuscadorGrupos.ExposeLW.ModelCanDrop += delegate(object sender, ModelDropEventArgs e)
            {
                if (e.SourceModels != null && e.SourceModels.Count > 0)
                {
                    var m = e.SourceModels[0];
                    if (!(m is gcProducto || m is gcServicio))
                    {
                        e.InfoMessage = "Solo productos y servicios";
                        e.Effect = DragDropEffects.None;
                    }
                    else
                    {
                        e.Effect = DragDropEffects.Move;
                    }
                        
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            };
            BuscadorGrupos.ExposeLW.ModelDropped += delegate(object sender, ModelDropEventArgs e)
            {
                if (e.TargetModel == null) return;
                foreach (gcObjeto m in e.SourceModels)
                {
                    m.Grupo = ((gcObjeto)e.TargetModel).Grupo;
                    m.SaveMe();
                }
                BuscadorGrupos.setCustomList(((gcObjeto)e.TargetModel).GrupoContenedor.SubItems);

                

                
            };
            BuscadorGrupos.ExposeLW.KeyDown += delegate(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Delete && RequisitoPermiso(gcUsuario.PermisoEnum.Contador))
                {
                    var p=BuscadorGrupos.Seleccionado as gcObjeto ;
                    if (p!= null)
                    {
                        var g = p.GrupoContenedor;
                        p.Grupo = 0;
                        p.SaveMe();
                        BuscadorGrupos.setCustomList(g.SubItems);
                    }
                }
            };
            
        }

        
        public gcObjeto Seleccionado
        {
            get
            {
                return (gcObjeto) tw1.SelectedObject;
            }
        }
        private void seleccionar()
        {
            if (EditOnClick)
                btEdit_Click(null, null);
            else
                CoreManager.Singlenton.seleccionarProducto(Seleccionado);
            
            if (Seleccionado != null 
                && ProductoSeleccionado != null)
                ProductoSeleccionado(Seleccionado);
            
        }
        public bool EditOnClick { get; set; }
        public bool GruposVisible { get; set; }
        public void expandAll()
        {
            tw1.ExpandAll();
        }
        void TimedFilter(ObjectListView olv, string txt, int matchKind)
        {
            TextMatchFilter filter = null;
            if (!String.IsNullOrEmpty(txt))
            {
                string[] strs=txt.Trim().Split(" ".ToCharArray());
                if (strs.Length > 1)
                {
                    filter = TextMatchFilter.Regex(olv, strs);
                }
                else
                {
                    switch (matchKind)
                    {
                        case 0:
                        default:
                            filter = TextMatchFilter.Contains(olv, txt);
                            break;
                        case 1:
                            filter = TextMatchFilter.Prefix(olv, txt);
                            break;
                        case 2:
                            filter = TextMatchFilter.Regex(olv, txt);
                            break;
                    }
                }
            }
            // Setup a default renderer to draw the filter matches
            if (filter == null)
                olv.DefaultRenderer = null;
            else
            {
                olv.DefaultRenderer = new HighlightTextRenderer(filter);

                // Uncomment this line to see how the GDI+ rendering looks
                // olv.DefaultRenderer = new HighlightTextRenderer { Filter = filter, UseGdiTextRendering = false };
            }

            // Some lists have renderers already installed
            HighlightTextRenderer highlightingRenderer = olv.GetColumn(0).Renderer as HighlightTextRenderer;
            if (highlightingRenderer != null)
                highlightingRenderer.Filter = filter;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            olv.AdditionalFilter = filter;
            olv.Invalidate();
            stopWatch.Stop();

            IList objects = olv.Objects as IList;
            if (objects == null)
                this.lblFiltro.Text =
                    String.Format("Filtered in {0}ms", stopWatch.ElapsedMilliseconds);
            else
                this.lblFiltro.Text =
                    String.Format("Se filtraron {0} objetos y se encontraron {1} en {2}ms",
                                  objects.Count,
                                  olv.Items.Count,
                                  stopWatch.ElapsedMilliseconds);
        }
       
        private void tw1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            seleccionar();

        }
        private void tw1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                seleccionar();
            }
        }
       
       
        private void Productos_FormatCell(object sender, FormatCellEventArgs e)
        {
            gcObjeto customer = (gcObjeto)e.Model;
               
            if (e.ColumnIndex == this.colStock.Index)
            {

                 
                switch (customer.Tipo)
                {
                    case gcObjeto.ObjetoTIpo.Categoria:
                    case gcObjeto.ObjetoTIpo.Grupo:
                        e.SubItem.Text = "";
                        break;
                    case gcObjeto.ObjetoTIpo.Producto:
                        e.SubItem.Text = customer.Stock.ToString("0.00");
                        if (customer.Stock < 0)
                        {
                            e.SubItem.ForeColor = Color.Red;
                            e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                            // e.SubItem.ImageSelector = Properties.Resources.Down;
                        }
                        else if (customer.Stock > 0)
                        {
                            e.SubItem.ForeColor = Color.Black;
                            // e.SubItem.ImageSelector = Properties.Resources.Up;
                            e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);

                        }
                        else
                        {
                            e.SubItem.ForeColor = Color.LightGray;
                        }
                        break;
                    case gcObjeto.ObjetoTIpo.Servicio:

                        e.SubItem.Text = Math.Abs(customer.Stock).ToString("0.00");
                        e.SubItem.ForeColor = Color.DarkBlue;
                        //e.SubItem.ImageSelector = Properties.Resources.Like;
                        e.SubItem.Font = new Font("Arial", 8, FontStyle.Bold);
                        break;
                }


            }
            else if (e.ColumnIndex == this.colPrecio.Index)
            {
                if (customer.Tipo == gcObjeto.ObjetoTIpo.Categoria || customer.Tipo == gcObjeto.ObjetoTIpo.Grupo)
                    e.SubItem.Text = String.Empty;
            }
            else if (e.ColumnIndex == this.colNombre.Index)
            {
               if(customer.Tipo==gcObjeto.ObjetoTIpo.Producto || customer.Tipo==gcObjeto.ObjetoTIpo.Servicio){

                        e.SubItem.Font = new Font("Arial", 9, FontStyle.Bold);
                       
                }
            }
        }
       
        public override void iniciar()
        {
            base.iniciar();
            btEdit.Visible = RequisitoPermiso(gcUsuario.PermisoEnum.Contador);
            mnuCrear.Visible = RequisitoPermiso(gcUsuario.PermisoEnum.Contador);
            btEliminar.Visible=RequisitoPermiso(gcUsuario.PermisoEnum.Contador);
            Reload();
         
        }
        public void Reload()
        {
            tw1.Roots = (gManager.CoreManager.Singlenton.getCategoriasPrincipales);
            tw1.ExpandAll();
        }
        public override void cerrar()
        {
            base.cerrar();
            tw1.Roots=(null);
        }

       

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            this.TimedFilter(this.tw1, txtBuscar.Text, 0);
        }

        private void btExpand_Click(object sender, EventArgs e)
        {
            tw1.ExpandAll();
            txtBuscar.Text = "";
        }

        private void btContraer_Click(object sender, EventArgs e)
        {
            tw1.CollapseAll();
        }

        private void tw1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            var o = tw1.SelectedObject as gcObjeto;
            if (o != null)
            {
                var g = o.GrupoContenedor;
                if (o.Tipo == gcObjeto.ObjetoTIpo.Grupo)
                    g = o as gcGrupo;
                if (!GruposVisible)
                {
                     if (g!=null)
                    {
                        BuscadorGrupos.setCustomList(g.SubItems);
                        BuscadorGrupos.Visible = true;
                        BuscadorGrupos.Titulo = "GRUPO: " + g.Nombre;
                    }
                    else
                    {
                        BuscadorGrupos.Visible = false;
                    }
                }
               

            }
        }

        private void mnuCrearProducto_Click(object sender, EventArgs e)
        {
            crear(new gcProducto());
        }

        private void mnuCrearServicio_Click(object sender, EventArgs e)
        {
            crear(new gcServicio());
        }

        private void mnuCrearCategoria_Click(object sender, EventArgs e)
        {
            crear(new gcCategoria());
           
        }
        private void crear(gcObjeto o)
        {
            if (!RequisitoPermiso(gcUsuario.PermisoEnum.Contador)) return;
            if (Seleccionado != null)
            {
                if (o.Tipo != gcObjeto.ObjetoTIpo.Grupo)
                {
                    if (Seleccionado.Tipo == gcObjeto.ObjetoTIpo.Producto || Seleccionado.Tipo == gcObjeto.ObjetoTIpo.Servicio)
                    {
                        o.Parent = Seleccionado.Parent;
                    }
                    else
                    {
                        o.Parent = Seleccionado.Id;
                    }
                }

            }
            Editar.EditarProducto(o);
            Reload();
        }

        private void mnuCrearGrupo_Click(object sender, EventArgs e)
        {
            crear(new gcGrupo());
            
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (!RequisitoPermiso(gcUsuario.PermisoEnum.Contador)) return;
            Editar.EditarProducto(Seleccionado);
            Reload();
        }

        private void btEliminar_Click(object sender, EventArgs e)
        {
            if (!RequisitoPermiso(gcUsuario.PermisoEnum.Contador)) return;
            if (Seleccionado != null)
            {
                if(MessageBox.Show("¿Desea Eliminar?","Eliminar Elemento",MessageBoxButtons.OKCancel,MessageBoxIcon.Question)==DialogResult.OK &&  Seleccionado.DeleteMe())
                    Reload();
            }
        }

        private void mnuActualizar_Click(object sender, EventArgs e)
        {
            Update();
        }

        private void btGlobalSel_CheckedChanged(object sender, EventArgs e)
        {
            EditOnClick = btGlobalSel.Checked;
            if (EditOnClick)
            {
                btGlobalSel.Image = Properties.Resources.down_curve;
                
                btGlobalSel.ToolTipText = "Desactivar para agregar la selección al movimiento activo con doble-click";
            }
            else
            {
                btGlobalSel.Image = Properties.Resources.Up_curve;
                btGlobalSel.ToolTipText = "Activar para editar la selección con doble-click";
                
            }
        }

        private void btHoldGrupos_CheckedChanged(object sender, EventArgs e)
        {
            GruposVisible = btHoldGrupos.Checked;
            if (GruposVisible)
            {
                btHoldGrupos.Image = Properties.Resources.Fijado;
                btHoldGrupos.ToolTipText = "Desactivar para mostrar/ocultar los productos del grupo en cada selección";
            }
            else
            {
                btHoldGrupos.Image = Properties.Resources.Fijar;
                btHoldGrupos.ToolTipText = "Activar para mantener sin cambios el panel de grupos" + Environment.NewLine + "Es util para agrupar arrastrando." ;
            }
        }

        private void btSincronizar_Click(object sender, EventArgs e)
        {
            if(Seleccionado.Tipo==gcObjeto.ObjetoTIpo.Producto)
            WebManager.Singlenton.SincronizarProducto((gcProducto)Seleccionado);
        }

        private void btDesincronizar_Click(object sender, EventArgs e)
        {
            if (Seleccionado.Tipo == gcObjeto.ObjetoTIpo.Producto)
                WebManager.Singlenton.EliminarEnServidor((gcProducto)Seleccionado);
        }
        
    }
}
