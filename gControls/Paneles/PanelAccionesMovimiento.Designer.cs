﻿namespace gControls.Paneles
{
    partial class PanelAccionesMovimiento
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btPreimpresa = new gControls.BotonAccion();
            this.btGuardar = new gControls.BotonAccion();
            this.btTicket = new gControls.BotonAccion();
            this.btTerminar = new gControls.BotonAccion();
            this.btEditar = new gControls.BotonAccion();
            this.btLimpiarPagos = new gControls.BotonAccion();
            this.btEliminar = new gControls.BotonAccion();
            this.editorPagos1 = new gControls.Editores.EditorPagos();
            this.SuspendLayout();
            // 
            // btPreimpresa
            // 
            this.btPreimpresa.AutoSize = true;
            this.btPreimpresa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPreimpresa.Checked = false;
            this.btPreimpresa.Dock = System.Windows.Forms.DockStyle.Right;
            this.btPreimpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPreimpresa.FormContainer = null;
            this.btPreimpresa.GlobalHotKey = false;
            this.btPreimpresa.HacerInvisibleAlDeshabilitar = false;
            this.btPreimpresa.HotKey = System.Windows.Forms.Shortcut.None;
            this.btPreimpresa.Image = global::gControls.Properties.Resources.Preimpresa;
            this.btPreimpresa.Location = new System.Drawing.Point(674, 0);
            this.btPreimpresa.Name = "btPreimpresa";
            this.btPreimpresa.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btPreimpresa.RegistradoEnForm = false;
            this.btPreimpresa.RequiereKey = false;
            this.btPreimpresa.Size = new System.Drawing.Size(55, 43);
            this.btPreimpresa.TabIndex = 8;
            this.btPreimpresa.Text = "Preimp";
            this.btPreimpresa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btPreimpresa.Titulo = "Preimpresa";
            this.btPreimpresa.ToolTipDescripcion = "Imprime sobre un formulario preimpreso.|Si hace click mientras tiene presionado C" +
    "ontrol le pedira la dirección de la plantilla.";
            this.btPreimpresa.UseVisualStyleBackColor = true;
            this.btPreimpresa.Click += new System.EventHandler(this.btPreimpresa_Click);
            // 
            // btGuardar
            // 
            this.btGuardar.AutoSize = true;
            this.btGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btGuardar.Checked = false;
            this.btGuardar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGuardar.FormContainer = null;
            this.btGuardar.GlobalHotKey = false;
            this.btGuardar.HacerInvisibleAlDeshabilitar = false;
            this.btGuardar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btGuardar.Image = global::gControls.Properties.Resources.Guardar;
            this.btGuardar.Location = new System.Drawing.Point(729, 0);
            this.btGuardar.Name = "btGuardar";
            this.btGuardar.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btGuardar.RegistradoEnForm = false;
            this.btGuardar.RequiereKey = false;
            this.btGuardar.Size = new System.Drawing.Size(62, 43);
            this.btGuardar.TabIndex = 9;
            this.btGuardar.Text = "Guardar";
            this.btGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btGuardar.Titulo = "Guardar Formulario";
            this.btGuardar.ToolTipDescripcion = "Si el movimiento aun no esta guardado no puede agregar productos o pagos.";
            this.btGuardar.UseVisualStyleBackColor = true;
            this.btGuardar.Click += new System.EventHandler(this.btGuardar_Click);
            // 
            // btTicket
            // 
            this.btTicket.AutoSize = true;
            this.btTicket.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btTicket.Checked = false;
            this.btTicket.Dock = System.Windows.Forms.DockStyle.Right;
            this.btTicket.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTicket.FormContainer = null;
            this.btTicket.GlobalHotKey = false;
            this.btTicket.HacerInvisibleAlDeshabilitar = false;
            this.btTicket.HotKey = System.Windows.Forms.Shortcut.None;
            this.btTicket.Image = global::gControls.Properties.Resources.Ticket;
            this.btTicket.Location = new System.Drawing.Point(791, 0);
            this.btTicket.Name = "btTicket";
            this.btTicket.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btTicket.RegistradoEnForm = false;
            this.btTicket.RequiereKey = false;
            this.btTicket.Size = new System.Drawing.Size(53, 43);
            this.btTicket.TabIndex = 7;
            this.btTicket.Text = "Ticket";
            this.btTicket.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btTicket.Titulo = "Imprimir ticket";
            this.btTicket.ToolTipDescripcion = "Imprime el movimiento en un ticket. Debe configurar en cual impresora.";
            this.btTicket.UseVisualStyleBackColor = true;
            this.btTicket.Click += new System.EventHandler(this.btTicket_Click);
            // 
            // btTerminar
            // 
            this.btTerminar.AutoSize = true;
            this.btTerminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btTerminar.Checked = false;
            this.btTerminar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btTerminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTerminar.FormContainer = null;
            this.btTerminar.GlobalHotKey = false;
            this.btTerminar.HacerInvisibleAlDeshabilitar = false;
            this.btTerminar.HotKey = System.Windows.Forms.Shortcut.CtrlF10;
            this.btTerminar.Image = global::gControls.Properties.Resources.Terminar;
            this.btTerminar.Location = new System.Drawing.Point(844, 0);
            this.btTerminar.Name = "btTerminar";
            this.btTerminar.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btTerminar.RegistradoEnForm = false;
            this.btTerminar.RequiereKey = false;
            this.btTerminar.Size = new System.Drawing.Size(66, 43);
            this.btTerminar.TabIndex = 3;
            this.btTerminar.Text = "Terminar";
            this.btTerminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btTerminar.Titulo = "Terminar";
            this.btTerminar.ToolTipDescripcion = "Finaliza la operación actual.";
            this.btTerminar.UseVisualStyleBackColor = true;
            this.btTerminar.Click += new System.EventHandler(this.btTerminar_Click);
            // 
            // btEditar
            // 
            this.btEditar.AutoSize = true;
            this.btEditar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEditar.Checked = false;
            this.btEditar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditar.FormContainer = null;
            this.btEditar.GlobalHotKey = false;
            this.btEditar.HacerInvisibleAlDeshabilitar = false;
            this.btEditar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btEditar.Image = global::gControls.Properties.Resources.Editar;
            this.btEditar.Location = new System.Drawing.Point(910, 0);
            this.btEditar.Name = "btEditar";
            this.btEditar.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btEditar.RegistradoEnForm = false;
            this.btEditar.RequiereKey = false;
            this.btEditar.Size = new System.Drawing.Size(50, 43);
            this.btEditar.TabIndex = 4;
            this.btEditar.Text = "Editar";
            this.btEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btEditar.Titulo = "Editar Movimiento";
            this.btEditar.ToolTipDescripcion = "Habilita la edición para modificar el movimiento.";
            this.btEditar.UseVisualStyleBackColor = true;
            this.btEditar.Click += new System.EventHandler(this.btEditar_Click);
            // 
            // btLimpiarPagos
            // 
            this.btLimpiarPagos.AutoSize = true;
            this.btLimpiarPagos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btLimpiarPagos.Checked = false;
            this.btLimpiarPagos.Dock = System.Windows.Forms.DockStyle.Right;
            this.btLimpiarPagos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLimpiarPagos.FormContainer = null;
            this.btLimpiarPagos.GlobalHotKey = false;
            this.btLimpiarPagos.HacerInvisibleAlDeshabilitar = false;
            this.btLimpiarPagos.HotKey = System.Windows.Forms.Shortcut.None;
            this.btLimpiarPagos.Image = global::gControls.Properties.Resources.Reiniciar24;
            this.btLimpiarPagos.Location = new System.Drawing.Point(960, 0);
            this.btLimpiarPagos.Name = "btLimpiarPagos";
            this.btLimpiarPagos.Permiso = gManager.gcUsuario.PermisoEnum.Vendedor;
            this.btLimpiarPagos.RegistradoEnForm = false;
            this.btLimpiarPagos.RequiereKey = false;
            this.btLimpiarPagos.Size = new System.Drawing.Size(61, 43);
            this.btLimpiarPagos.TabIndex = 12;
            this.btLimpiarPagos.Text = "Limpiar";
            this.btLimpiarPagos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btLimpiarPagos.Titulo = "Limpiar Pagos";
            this.btLimpiarPagos.ToolTipDescripcion = "Elimina los pagos realizados.";
            this.btLimpiarPagos.UseVisualStyleBackColor = true;
            this.btLimpiarPagos.Click += new System.EventHandler(this.btLimpiarPagos_Click);
            // 
            // btEliminar
            // 
            this.btEliminar.AutoSize = true;
            this.btEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEliminar.Checked = false;
            this.btEliminar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEliminar.FormContainer = null;
            this.btEliminar.GlobalHotKey = false;
            this.btEliminar.HacerInvisibleAlDeshabilitar = false;
            this.btEliminar.HotKey = System.Windows.Forms.Shortcut.None;
            this.btEliminar.Image = global::gControls.Properties.Resources.Eliminar;
            this.btEliminar.Location = new System.Drawing.Point(1021, 0);
            this.btEliminar.Name = "btEliminar";
            this.btEliminar.Permiso = gManager.gcUsuario.PermisoEnum.Encargado;
            this.btEliminar.RegistradoEnForm = false;
            this.btEliminar.RequiereKey = false;
            this.btEliminar.Size = new System.Drawing.Size(61, 43);
            this.btEliminar.TabIndex = 5;
            this.btEliminar.Text = "Eliminar";
            this.btEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btEliminar.Titulo = "Eliminar";
            this.btEliminar.ToolTipDescripcion = "Borra completamente el Movimiento.";
            this.btEliminar.UseVisualStyleBackColor = true;
            this.btEliminar.Click += new System.EventHandler(this.btEliminar_Click);
            // 
            // editorPagos1
            // 
            this.editorPagos1.AutoScroll = true;
            this.editorPagos1.AutoSize = true;
            this.editorPagos1.Dock = System.Windows.Forms.DockStyle.Left;
            this.editorPagos1.Location = new System.Drawing.Point(0, 0);
            this.editorPagos1.Name = "editorPagos1";
            this.editorPagos1.Size = new System.Drawing.Size(750, 43);
            this.editorPagos1.TabIndex = 11;
            this.editorPagos1.Vertical = false;
            // 
            // PanelAccionesMovimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.editorPagos1);
            this.Controls.Add(this.btPreimpresa);
            this.Controls.Add(this.btGuardar);
            this.Controls.Add(this.btTicket);
            this.Controls.Add(this.btTerminar);
            this.Controls.Add(this.btEditar);
            this.Controls.Add(this.btLimpiarPagos);
            this.Controls.Add(this.btEliminar);
            this.Name = "PanelAccionesMovimiento";
            this.Size = new System.Drawing.Size(1082, 43);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BotonAccion btTicket;
        private BotonAccion btPreimpresa;
        private BotonAccion btEliminar;
        private BotonAccion btTerminar;
        private BotonAccion btEditar;
        private BotonAccion btGuardar;
        private BotonAccion btLimpiarPagos;
        private Editores.EditorPagos editorPagos1;
    }
}
