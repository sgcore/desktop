﻿namespace gControls.Paneles
{
    partial class PanelConfigSistema
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.chk = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chk
            // 
            this.chk.AutoSize = true;
            this.chk.BackColor = System.Drawing.Color.Transparent;
            this.chk.Checked = global::gControls.Properties.Settings.Default.cfgShowToolTips;
            this.chk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::gControls.Properties.Settings.Default, "cfgShowToolTips", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chk.Location = new System.Drawing.Point(13, 12);
            this.chk.Name = "chk";
            this.chk.Size = new System.Drawing.Size(318, 17);
            this.chk.TabIndex = 1;
            this.chk.Text = "Mostrar ToolTips de ayuda en los botones. (Requiere reiniciar)";
            this.chk.UseVisualStyleBackColor = false;
            this.chk.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // PanelConfigSistema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chk);
            this.Name = "PanelConfigSistema";
            this.Size = new System.Drawing.Size(643, 284);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chk;
    }
}
