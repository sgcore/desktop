DROP TABLE IF EXISTS [gcdestinatario];
CREATE TABLE `gcdestinatario` 
-- This table created by SQLite2009 Pro Enterprise Manager 
-- Osen Kusnadi - http://www.osenxpsuite.net 
-- Created date: 2015-03-09 17:53:53
( 
       Id INTEGER PRIMARY KEY AUTOINCREMENT ,
       Nombre TEXT,
       Direccion TEXT,
       Telefono TEXT,
       Celular TEXT,
       Mail TEXT,
       Cuit TEXT,
       Observaciones TEXT,
       Tipo INT,
       Parent INT,
       Nacimiento DATETIME,
       Estado INT,
       LastMod DATETIME
);
DROP TABLE IF EXISTS [gcmovimientos];
CREATE TABLE `gcmovimientos` 
-- This table created by SQLite2009 Pro Enterprise Manager 
-- Osen Kusnadi - http://www.osenxpsuite.net 
-- Created date: 2015-03-09 17:53:53
( 
       Id INTEGER PRIMARY KEY AUTOINCREMENT ,
       Origen INT,
       Destino INT,
       Usuario INT,
       Fecha DATETIME,
       Observaciones TEXT,
       Caja INT,
       Tipo INT,
       Estado INT,
       Codigo TEXT,
       Numero INT,
       LastMod DATETIME
);
DROP TABLE IF EXISTS [gcobjetos];
CREATE TABLE `gcobjetos` 
-- This table created by SQLite2009 Pro Enterprise Manager 
-- Osen Kusnadi - http://www.osenxpsuite.net 
-- Created date: 2015-03-09 17:53:53
( 
       Id INTEGER PRIMARY KEY AUTOINCREMENT ,
       Nombre TEXT,
       Descripcion TEXT,
       Codigo TEXT,
       Serial TEXT,
       Link TEXT,
       Grupo INT,
       Costo REAL,
       Ganancia REAL,
       Cara0 TEXT,
       Cara1 TEXT,
       Cara2 TEXT,
       Cara3 TEXT,
       Cara4 TEXT,
       Cara5 TEXT,
       Cara6 TEXT,
       Cara7 TEXT,
       Cara8 TEXT,
       Cara9 TEXT,
       Parent INT,
       tipo INT
);
DROP TABLE IF EXISTS [gcobjetosmovidos];
CREATE TABLE `gcobjetosmovidos` 
-- This table created by SQLite2009 Pro Enterprise Manager 
-- Osen Kusnadi - http://www.osenxpsuite.net 
-- Created date: 2015-03-09 17:53:53
( 
       Id INTEGER PRIMARY KEY AUTOINCREMENT ,
       Movimiento INT,
       Objeto INT,
       Cantidad REAL,
       Monto REAL,
       Observaciones TEXT,
       Parent INT,
       Cotizacion REAL,
       Moneda INT
);
DROP TABLE IF EXISTS [gcpagos];
CREATE TABLE `gcpagos` 
-- This table created by SQLite2009 Pro Enterprise Manager 
-- Osen Kusnadi - http://www.osenxpsuite.net 
-- Created date: 2015-03-09 17:53:53
( 
       Id INTEGER PRIMARY KEY AUTOINCREMENT ,
       Monto REAL,
       Observaciones TEXT,
       Tipo INT,
       Movimiento INT,
       m100 INT,
       m50 INT,
       m20 INT,
       m10 INT,
       m5 INT,
       m2 INT,
       m1 INT,
       m050 INT,
       m025 INT,
       m010 INT,
       m005 INT,
       m001 INT,
       Parent INT,
       Fechaini DATETIME,
       Fechafin DATETIME
);
DROP TABLE IF EXISTS [gcusuarios];
CREATE TABLE `gcusuarios` 
-- This table created by SQLite2009 Pro Enterprise Manager 
-- Osen Kusnadi - http://www.osenxpsuite.net 
-- Created date: 2015-03-09 17:53:53
( 
       Id INTEGER PRIMARY KEY AUTOINCREMENT ,
       Nombre TEXT,
       Descripcion TEXT,
       Pass TEXT,
       Permiso INT,
       Usuario TEXT,
       Central INT,
       Responsable INT
);
INSERT INTO gcdestinatario VALUES(1,'Central','Central generada por el sistema','Ingresar Telefono','Ingresar Celular','Ingresar Mail','Ingresar Cuit','Observaciones',2,0,'2014-10-10 12:00:00',0,'2014-10-10 12:00:00');
INSERT INTO gcdestinatario VALUES(2,'Responsable','Responsable generado por el sistema','Ingresar Telefono','Ingresar Celular','Ingresar Mail','Ingresar Cuit','Observaciones',3,0,'2014-10-10 12:00:00',0,'2014-10-10 12:00:00');
INSERT INTO gcusuarios VALUES(1,'Administrador','Usuario generado por el sistema','ad',5,'ad',1,2);
