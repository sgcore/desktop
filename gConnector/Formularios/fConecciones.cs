﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gConnector.Formularios
{
    public partial class fConecciones : Form
    {
        public fConecciones()
        {
            InitializeComponent();
            Icon = Icon.FromHandle(Properties.Resources.Iniciado.GetHicon());
            
            recargar();
        }

        private void btRecargar_Click(object sender, EventArgs e)
        {
            if (ConexionSeleccionada != null)
            {
                ConexionSeleccionada.Conectar();
                recargar();
            }
                
        }

        private void btMysql_Click(object sender, EventArgs e)
        {
            Formularios.fCrearConexionMysql f = new fCrearConexionMysql();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                gConnector.Connector.Singlenton.addConexion(f.Conexion);
            }
            recargar();
        }
        private void recargar()
        {
            lw1.Items.Clear();
            foreach (Conexion cnn in gConnector.Connector.Singlenton.Conexiones)
            {
                ListViewItem i = new ListViewItem(cnn.Nombre, (int)cnn.ConexionTipo);
                i.SubItems.Add(cnn.ConexionTipo.ToString());
                i.SubItems.Add(cnn.ConexionEstado.ToString());
                lw1.Items.Add(i);
            }
        }
        Conexion _selcnn;
        public Conexion ConexionSeleccionada 
        { 
            get { return _selcnn; } 
            set 
            { 
                _selcnn = value; 
                btAceptar.Enabled = _selcnn != null; 
            } 
        }

        private void lw1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                ConexionSeleccionada = Connector.Singlenton.getConexion(e.Item.Text);
            }
        }

        private void lw1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            if (ConexionSeleccionada != null && ConexionSeleccionada.Conectar().Conectado)
            {
                DialogResult = DialogResult.OK;
            }

            
        }

        private void btSqlite_Click(object sender, EventArgs e)
        {
            var f = new OpenFileDialog();
            f.Filter = "Base de Datos|*.db";
            if (f.ShowDialog() == DialogResult.OK)
            {
                Connector.Singlenton.addConexion(new ConexionSqlite(f.FileName));
            }
            recargar();
        }

       

       
    }
}
