﻿namespace gConnector.Formularios
{
    partial class fConecciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fConecciones));
            this.lw1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btRecargar = new System.Windows.Forms.Button();
            this.btMysql = new System.Windows.Forms.Button();
            this.btSqlite = new System.Windows.Forms.Button();
            this.btJSon = new System.Windows.Forms.Button();
            this.btCancelar = new System.Windows.Forms.Button();
            this.btAceptar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lw1
            // 
            this.lw1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lw1.FullRowSelect = true;
            this.lw1.HideSelection = false;
            this.lw1.LargeImageList = this.imageList1;
            this.lw1.Location = new System.Drawing.Point(12, 12);
            this.lw1.MultiSelect = false;
            this.lw1.Name = "lw1";
            this.lw1.Size = new System.Drawing.Size(460, 202);
            this.lw1.SmallImageList = this.imageList1;
            this.lw1.TabIndex = 0;
            this.lw1.UseCompatibleStateImageBehavior = false;
            this.lw1.View = System.Windows.Forms.View.Details;
            this.lw1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lw1_ItemSelectionChanged);
            this.lw1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lw1_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nombre";
            this.columnHeader1.Width = 250;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tipo";
            this.columnHeader2.Width = 93;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Estado";
            this.columnHeader3.Width = 110;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Sqlite");
            this.imageList1.Images.SetKeyName(1, "Mysql");
            this.imageList1.Images.SetKeyName(2, "Json");
            // 
            // btRecargar
            // 
            this.btRecargar.AutoSize = true;
            this.btRecargar.Image = ((System.Drawing.Image)(resources.GetObject("btRecargar.Image")));
            this.btRecargar.Location = new System.Drawing.Point(490, 11);
            this.btRecargar.Name = "btRecargar";
            this.btRecargar.Size = new System.Drawing.Size(77, 23);
            this.btRecargar.TabIndex = 5;
            this.btRecargar.Text = "Conectar";
            this.btRecargar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btRecargar.UseVisualStyleBackColor = true;
            this.btRecargar.Click += new System.EventHandler(this.btRecargar_Click);
            // 
            // btMysql
            // 
            this.btMysql.AutoSize = true;
            this.btMysql.ImageKey = "Mysql";
            this.btMysql.ImageList = this.imageList1;
            this.btMysql.Location = new System.Drawing.Point(490, 51);
            this.btMysql.Name = "btMysql";
            this.btMysql.Size = new System.Drawing.Size(77, 23);
            this.btMysql.TabIndex = 6;
            this.btMysql.Text = "MySql";
            this.btMysql.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btMysql.UseVisualStyleBackColor = true;
            this.btMysql.Click += new System.EventHandler(this.btMysql_Click);
            // 
            // btSqlite
            // 
            this.btSqlite.AutoSize = true;
            this.btSqlite.ImageKey = "Sqlite";
            this.btSqlite.ImageList = this.imageList1;
            this.btSqlite.Location = new System.Drawing.Point(490, 80);
            this.btSqlite.Name = "btSqlite";
            this.btSqlite.Size = new System.Drawing.Size(77, 23);
            this.btSqlite.TabIndex = 7;
            this.btSqlite.Text = "Sqlite";
            this.btSqlite.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSqlite.UseVisualStyleBackColor = true;
            this.btSqlite.Click += new System.EventHandler(this.btSqlite_Click);
            // 
            // btJSon
            // 
            this.btJSon.AutoSize = true;
            this.btJSon.ImageKey = "Json";
            this.btJSon.ImageList = this.imageList1;
            this.btJSon.Location = new System.Drawing.Point(490, 109);
            this.btJSon.Name = "btJSon";
            this.btJSon.Size = new System.Drawing.Size(77, 23);
            this.btJSon.TabIndex = 8;
            this.btJSon.Text = "JSon";
            this.btJSon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btJSon.UseVisualStyleBackColor = true;
            // 
            // btCancelar
            // 
            this.btCancelar.AutoSize = true;
            this.btCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btCancelar.Image")));
            this.btCancelar.Location = new System.Drawing.Point(136, 219);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(91, 38);
            this.btCancelar.TabIndex = 10;
            this.btCancelar.Text = "Cancelar";
            this.btCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCancelar.UseVisualStyleBackColor = true;
            // 
            // btAceptar
            // 
            this.btAceptar.AutoSize = true;
            this.btAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btAceptar.Image = ((System.Drawing.Image)(resources.GetObject("btAceptar.Image")));
            this.btAceptar.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btAceptar.Location = new System.Drawing.Point(367, 219);
            this.btAceptar.Name = "btAceptar";
            this.btAceptar.Size = new System.Drawing.Size(105, 38);
            this.btAceptar.TabIndex = 9;
            this.btAceptar.Text = "Seleccionar";
            this.btAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAceptar.UseVisualStyleBackColor = true;
            // 
            // fConecciones
            // 
            this.AcceptButton = this.btAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancelar;
            this.ClientSize = new System.Drawing.Size(575, 260);
            this.ControlBox = false;
            this.Controls.Add(this.btCancelar);
            this.Controls.Add(this.btAceptar);
            this.Controls.Add(this.btJSon);
            this.Controls.Add(this.btSqlite);
            this.Controls.Add(this.btMysql);
            this.Controls.Add(this.btRecargar);
            this.Controls.Add(this.lw1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fConecciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conexiones";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lw1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btRecargar;
        private System.Windows.Forms.Button btMysql;
        private System.Windows.Forms.Button btSqlite;
        private System.Windows.Forms.Button btJSon;
        private System.Windows.Forms.Button btCancelar;
        private System.Windows.Forms.Button btAceptar;

    }
}