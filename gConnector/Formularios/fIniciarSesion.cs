﻿using System;
using System.Windows.Forms;
using gConnector.Properties;
using System.Drawing;

namespace gConnector.Formularios
{
    public partial class fIniciarSesion : Form
    {
        public fIniciarSesion()
        {
            InitializeComponent();
            Icon = Icon.FromHandle(Properties.Resources.Iniciar24.GetHicon());
            
        }
        public void iniciar()
        {
            txtUsuario.Text = "";
            txtPass.Text = "";
            txtUsuario.Focus();


        }
       
        public string Usuario { get { return txtUsuario.Text; } }
        public string Pass { get { return txtPass.Text;} }

        public void setSiEsInternet(bool es)
        {
            if (es) BackgroundImage = Resources.Internet;
            else BackgroundImage = Resources.Local;
        }
        public void setForLogin(string user, string pass)
        {
            noGuardar = true;
            btcONEXion.Visible = false;
            checkBox1.Visible = false;
            checkBox1.Checked = false;
            txtPass.Text = pass;
            txtUsuario.Text = user;
            
        }
        public void setDescripcion(string desc)
        {
            lblDesc.Text =desc;
        }
        public bool showAndAccept()
        {
            return this.ShowDialog()==DialogResult.OK;
        }
        public bool noGuardar { get; set; }
        private void fIniciarSesion_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (noGuardar) return;
            Properties.Settings.Default.sesionRecordarme = checkBox1.Checked;
            Properties.Settings.Default.Save();
        }

        private void btcONEXion_Click(object sender, EventArgs e)
        {
            var cnn =gConnector.Connector.Singlenton.SelecionarConexion();
            if (cnn != null)
            {
               
                Connector.Singlenton.ConexionActiva = cnn;
                DialogResult = System.Windows.Forms.DialogResult.Cancel;
                
            }
            else
            {
                DialogResult = System.Windows.Forms.DialogResult.None;
            }
        }
    }
}
