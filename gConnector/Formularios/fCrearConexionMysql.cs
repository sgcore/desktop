﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gConnector.Formularios
{
    public partial class fCrearConexionMysql : Form
    {
        public fCrearConexionMysql()
        {
            InitializeComponent();
        }
        public string Servidor
        {
            get
            {
                return txtServer.Text;
            }
        }
        public string BaseDeDatos
        {
            get
            {
                return txtBasedeDatos.Text;
            }
        }
        public string Usuario
        {
            get
            {
                return txtUsuario.Text;
            }
        }
        public string Pass
        {
            get
            {
                return txtPass.Text;
            }
        }
        public Conexion Conexion
        {
            get
            {

                return new ConexionMySql(txtServer.Text, txtBasedeDatos.Text, txtUsuario.Text, txtPass.Text);


            }
            set
            {
                var _cnn = value;
                if (_cnn != null)
                {
                    //harcodeamos el tipo para obtener los servidores
                    switch (_cnn.ConexionTipo)
                    {

                        case Conexion.ConexionTipoEnum.MySql:
                            ConexionMySql cnn = (ConexionMySql)_cnn;
                            txtServer.Text = cnn.Servidor;
                            txtBasedeDatos.Text = cnn.BaseDeDatos;
                            txtUsuario.Text = cnn.UsuarioBD;
                            txtPass.Text = cnn.Pass;
                            break;



                    }

                }
            }

        }

        private void btTest_Click(object sender, EventArgs e)
        {
            if (Conexion.Conectar().Conectado)
            {
                MessageBox.Show("Conexión Activa", "Estado de la conexión", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Fallo la conexión", "Estado de la conexión", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
