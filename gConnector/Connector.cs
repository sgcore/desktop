﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Reflection;
namespace gConnector
{
    public class Connector
    {
        private static Connector _instance;
        private Hashtable _cnns = new Hashtable();
       
        
       
        private Connector() { 
            string db="sgcoredb.db";
            if(!File.Exists(db))
            {
                
            }
        }
        private void CopyResource(string resourceName, string file)
        {
           
        }
        public static Connector Singlenton
        {
            get
            {
                if (_instance == null) _instance = new Connector();
                return _instance;
            }
        }
        public void addConexion(Conexion cnn)
        {
            if (cnn == null) return;
            if (_cnns.ContainsKey(cnn.Nombre))
            {
                _cnns.Remove(cnn.Nombre);
            }
            _cnns.Add(cnn.Nombre, cnn);
            cnn.RepararTablas();


            ConexionActiva = cnn;
        }
        public Conexion getConexion(string name)
        {
            return (Conexion) _cnns[name];
        }
        public ICollection Conexiones
        {
            get
            {
                return _cnns.Values;
            }
        }
        public Conexion ConexionActiva { get; set; }
        public bool RequiereRelog { get; set; }
        public Conexion.ConexionEstadoEnum Iniciar()
        {
            
            Conexion.ConexionEstadoEnum t = Conexion.ConexionEstadoEnum.Desconectado;
            downloadsqliteFiles();
            if (ConexionActiva == null)
            {

                ConexionActiva = LevantarConexion();
                addConexion(ConexionActiva);
                if (ConexionActiva == null || !ConexionActiva.Conectar().Conectado) 
                     ConexionActiva = SelecionarConexion();
                if (ConexionActiva == null) return t;
            }
            if (ConexionActiva.Conectar().Conectado)
            {
                //ConexionActiva.login();
            }
            
            return ConexionActiva.ConexionEstado;
        }
        public void terminar()
        {
            ConexionActiva = null;
        }
        public Conexion.ConexionEstadoEnum Estado
        {
            get
            {
                if (ConexionActiva == null) return Conexion.ConexionEstadoEnum.Desconectado;
                return ConexionActiva.ConexionEstado;
            }
        }
        private Conexion LevantarConexion()
        {
            Conexion.ConexionTipoEnum  tipo=(Conexion.ConexionTipoEnum)Properties.Settings.Default.cnnBDTipo;
                switch (tipo)
                {
                    case Conexion.ConexionTipoEnum.MySql:
                        return new ConexionMySql(Properties.Settings.Default.cnnBDServer,
                            Properties.Settings.Default.cnnBDName,
                            Properties.Settings.Default.cnnBDUser,
                            Properties.Settings.Default.cnnBDPass);

                    case Conexion.ConexionTipoEnum.SqLite:
                        return createSqliteConection();
                }
                return null;
        }
        private Conexion createSqliteConection()
        {
            string path = Properties.Settings.Default.cnnBDPath;
            string basepath = AppDomain.CurrentDomain.BaseDirectory;
            string basedb = "sgcoredb.db";
            if (String.IsNullOrEmpty(path))
            {
                path = basepath + basedb;
                Properties.Settings.Default.cnnBDPath = path;
            }
           




            return new ConexionSqlite(path);
        }
        public Conexion SelecionarConexion()
        {
            Conexion cnn = null;
            Formularios.fConecciones f = new Formularios.fConecciones();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cnn = f.ConexionSeleccionada;
                if (cnn != null)
                {
                    gConnector.Connector.Singlenton.terminar();
                    ConexionActiva = cnn;
                    
                    cnn.SaveConfig();
                    RequiereRelog = true;
                }
            }


            return cnn;
        }
        public bool EstaConectado
        {
            get
            {
                if (Estado == Conexion.ConexionEstadoEnum.Desconectado) Iniciar();
                return Estado == Conexion.ConexionEstadoEnum.Conectado ||
                    Estado == Conexion.ConexionEstadoEnum.Autenticado ||
                    Estado == Conexion.ConexionEstadoEnum.Cargado;
            }
        }
        public Hashtable LoginWindows(string titulo,bool internet,string user="",string pass="")
        {
            Hashtable t = new Hashtable();
            var f = new gConnector.Formularios.fIniciarSesion();
            f.setForLogin(user, pass);
            f.Text = "Ingresar Cuenta";
            f.setSiEsInternet(internet);
            f.setDescripcion(titulo);
            if (f.showAndAccept())
            {
               
                t.Add("Usuario", f.Usuario);
                t.Add("Pass", f.Pass);
              
            }
            return t;
        }
        private void downloadsqliteFiles()
        {
            string basedb = "sgcoredb.db";
            //string interop = "SQLite.Interop.dll";
            //string sync = "sync.db";
            string basepath = AppDomain.CurrentDomain.BaseDirectory;
            downloadFileIfNotExist(basedb);
            /*if(Environment.Is64BitOperatingSystem)
            {
                downloadFileIfNotExist(interop, "x64" );
            } else
            {
                downloadFileIfNotExist(interop, "x32");
            }
            
            downloadFileIfNotExist(sync);*/

        }
        /// <summary>
        /// Guarda el archivo especificado en la raiz
        /// si usa subfolder debe terminar con un /
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="subfolder"></param>
        private void downloadFileIfNotExist(string filename, string subfolder = "")
        {
            if(!String.IsNullOrEmpty(subfolder))
            {
                try
                {
                    Directory.CreateDirectory(subfolder);
                }
                catch(Exception e)
                {
                    gLogger.logger.Singlenton.addErrorMsg("no se pudo crear" + subfolder + " " + e.Message);
                    return;
                }
                
            }
            
            string myfile = Path.Combine( AppDomain.CurrentDomain.BaseDirectory, subfolder, filename);
            if (!File.Exists(myfile))
            {
                downloadFile(filename, subfolder);
            }
        }
        public void downloadFile(string fileName, string subfolder, string target = "")
        {
            string remoteUri = "http://www.mascocitas.com/releases/";
            string myfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, subfolder, fileName);
            string myStringWebResource = null;
            if (File.Exists(myfile)) return;
            
            // Create a new WebClient instance.
            WebClient myWebClient = new WebClient();
            // Concatenate the domain with the Web resource filename.
            if (!String.IsNullOrEmpty(subfolder)) subfolder += "/";

            myStringWebResource = remoteUri + (!string.IsNullOrEmpty(target) ? target : subfolder + fileName);
            
            try
            {
                myWebClient.DownloadFile(myStringWebResource, myfile);
            }
            catch(Exception e)
            {
                gLogger.logger.Singlenton.addErrorMsg("no se pudo descargar " + fileName + e.Message);
            }
          
            

        }
        public IEnumerable ListaExcel(string fileName)
        {
            var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=Excel 12.0;", fileName);

            var adapter = new OleDbDataAdapter("SELECT * FROM [workSheetNameHere$]", connectionString);
            var ds = new DataSet();
            try
            {
                adapter.Fill(ds, "anyNameHere");
            }
            catch (Exception e)
            {
                gLogger.logger.Singlenton.addErrorMsg(e.Message);
                return null;
            }

            DataTable data = ds.Tables["anyNameHere"];
            var data1= ds.Tables["anyNameHere"].AsEnumerable();
            return data1;
        }

        public void resetUser()
        {
            Properties.Settings.Default.sesionUsuario = "";
            Properties.Settings.Default.Save();
        }
    }

}
