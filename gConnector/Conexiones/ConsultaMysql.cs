﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gConnector
{
    class ConsultaMysql : Consulta
    {
        public ConsultaMysql()
        {

        }
        public override string Movimientos
        {
            get { return "SELECT Id, Origen, Destino, Usuario, Fecha, Observaciones, Caja, Tipo, Estado, Codigo, Numero, LastMod, facnum, factipo FROM gcmovimientos "; }
        }
        public override string Destinatarios
        {
            get { return "SELECT  Id, Nombre, Direccion, Telefono, Celular, Mail, Cuit, Observaciones, Tipo, TipoIva, Parent, Nacimiento, Estado, LastMod FROM  gcdestinatario "; }
        }
        public override string Tratamientos
        {
            get { return "Select * from gcTratamientos "; }
        }
        public override string UltimaCaja
        {
            get { return "SELECT MAX(caja) as caja from gcmovimientos"; }
        }
        public override string Usuarios
        {
            get { return "SELECT  Id, Nombre, Descripcion, Pass, Permiso, Usuario, Central, Responsable FROM  gcusuarios "; }
        }
        public override string Pagos
        {
            get { return "SELECT  id, Monto, Observaciones, tipo, m1000, m500, m200, m100, m50, m20, m10, m5, m2, m1, m050, m025, m010, m005, m001, Parent, Fechaini, Fechafin, Movimiento FROM  gcpagos "; }
        }
        public override string ObjetosMovidos
        {
            get { return "SELECT Movimiento, Objeto, Cantidad, Monto, Observaciones, Parent, Cotizacion, Moneda, Id FROM gcobjetosmovidos "; }
        }
        public override string Productos
        {
            get { return "SELECT  Id, Nombre, Descripcion, Codigo, Serial, Link, Costo, Ganancia, Iva, Cara0, Cara1, Cara2, Cara3, Cara4, Cara5, Cara6, Cara7, Cara8, Cara9, Tipo, Grupo, Parent, Metrica, Dolar, Ideal, State, image, updated_at FROM  gcobjetos "; }
        }

        public override string Bloqueo => "SELECT Tipo, Usuario, Fecha, Estado, Token FROM gcbloqueo";
        public override string Desbloquear => "DELETE FROM gcbloqueo";
        public override string Bloquear(int tipo,int user, string token)
        {
            Hashtable t = new Hashtable();
            t.Add("Usuario", user);
            t.Add("Token", token);
            t.Add("Fecha", DateTime.Now);
            t.Add("Tipo", tipo);
            return InsertEntity(t, "gcbloqueo");
        }
        public override string InsertEntity(Hashtable campos,string tablename)
        {
            string t = "INSERT INTO " + tablename + " ";
            string cols = "(";
            string vals = "(";
            // string[] colums = new string[e.Campos.Count];
            
            //e.Campos.Keys.CopyTo(colums,0);
            var enumerator=campos.GetEnumerator();
            while(enumerator.MoveNext())
            {
                string label = (string)enumerator.Entry.Key;
                object value = enumerator.Value;
                if (label.ToLower() == "id")
                {
                    continue;
                }


                if (typeof(string) == value.GetType())
                    value = "'" + value + "'";
                else if (typeof(DateTime) == value.GetType())
                    value = fechaparaSQL((DateTime)value);
                else if (typeof(decimal) == value.GetType())
                    value = value.ToString().Replace(",", ".");
                else if (value is Enum)
                {
                    value = (int)value;
                }
                cols += label;
                vals += value;
                cols += ",";
                    vals += ",";
            }
            cols = cols.Remove(cols.Length-1)+ ")";
            vals = vals.Remove(vals.Length-1) + ")";

            return t + cols + " VALUES " + vals;
        }

        public override string UpdateEntity(Hashtable campos, string tablename)
        {
            string t = "UPDATE " + tablename + " set ";
           int id=0;
            //e.Campos.Keys.CopyTo(colums,0);
            var enumerator = campos.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string label = (string)enumerator.Entry.Key;
                object value = enumerator.Value;
                if (label.ToLower() == "id")
                {
                    id =(int)value;
                }


                if (typeof(string) == value.GetType())
                    value = "'" + value + "'";
                else if (typeof(DateTime) == value.GetType())
                    value = fechaparaSQL((DateTime)value);
                else if (typeof(decimal) == value.GetType())
                    value = value.ToString().Replace(",",".");
                else if (value is Enum)
                {
                    value = (int)value;
                }


                t += label + "=" + value + " ,";
            }
            t=t.Remove(t.Length - 2);
            t += " where id= " + id;

            return t;
        }
        public override string DeleteEntity(int id, string tablename)
        {
            return "DELETE FROM "+tablename +" where id ="+id
                ;
        }
        
    }
}
