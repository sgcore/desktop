﻿using gLogger;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Data;
using System.Xml;

namespace gConnector
{
    public class ConexionMySql : Conexion
    {
        public ConexionMySql(string server, string bdname, string user, string pass)
        {
            ConexionTipo = ConexionTipoEnum.MySql;
            ConexionServer = server;
            ConexionDbName = bdname;
            ConexionUser = user;
            ConexionPass = pass;
            _consulta = new ConsultaMysql();
            //Conectar();
        }

        public override DataTable Consulta(string sql)
        {
            MySqlConnection cnn = new MySqlConnection("SERVER=" + ConexionServer + "; DATABASE=" + ConexionDbName + "; UID=" + ConexionUser + "; PASSWORD=" + ConexionPass + ";");

            DataTable dat = new DataTable();
            try
            {
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand(sql, cnn);

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);

                //Execute query

                da.Fill(dat);
                //close connection

                cnn.Close();

            }
            catch (Exception ex)
            {
                logger.Singlenton.addErrorMsg(ex.Message);
                cnn.Close();
            }

            return dat;
        }

        public string Servidor
        {
            get
            {
                return ConexionServer;
            }
        }
        public string BaseDeDatos
        {
            get { return ConexionDbName; }
        }
        public string UsuarioBD
        {
            get { return ConexionUser; }
        }
        public string Pass
        {
            get { return ConexionPass; }
        }
        public override int Insertar(string sql)
        {
            int l = 0;
            MySqlConnection cnn = new MySqlConnection("SERVER=" + ConexionServer + "; DATABASE=" + ConexionDbName + "; UID=" + ConexionUser + "; PASSWORD=" + ConexionPass + ";");
            DataTable dat = new DataTable();
            try
            {
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = sql;
                //Assign the connection using Connection
                cmd.Connection = cnn;

                //Execute query
                cmd.ExecuteNonQuery();
                l = (int)cmd.LastInsertedId;
                //close connection
                cnn.Close();

            }
            catch (Exception ex)
            {
                logger.Singlenton.addErrorMsg(ex.Message);
                cnn.Close();
            }
            return l;
        }

        internal override bool CreateDb()
        {
            throw new NotImplementedException();
        }

        private void nonQuerySilent(string sql)
        {
            MySqlConnection cnn = new MySqlConnection("SERVER=" + ConexionServer + "; DATABASE=" + ConexionDbName + "; UID=" + ConexionUser + "; PASSWORD=" + ConexionPass + ";");

            DataTable dat = new DataTable();
            try
            {
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand("ALTER TABLE gcobjetos ADD State INT NOT NULL DEFAULT '0'", cnn);
                cmd.ExecuteNonQuery();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);

                //Execute query

                da.Fill(dat);
                //close connection

                cnn.Close();

            }
            catch (Exception)
            {
                cnn.Close();
            }
        }
        internal override bool RepararTablas()
        {
            Consulta("CREATE TABLE IF NOT EXISTS `gctratamientos` ("
                      + "`Id` int(11) NOT NULL auto_increment,"
                      + "`Usuario` int(11) default '0',"
                      + "`Paciente` int(11) default '0',"
                      + "`Titulo` text character set utf8 ,"
                      + "`Descripcion` varchar(255) character set utf8 default '',"
                      + "`Tipo` int(11) default NULL,"
                      + "`Parent` int(11) default NULL,"
                      + "`Fecha` datetime default NULL,"
                      + "`Proximo` datetime default NULL,"
                      + "`Estado` int(11) default NULL,"
                      + "KEY `ix_gcTratamientos_autoinc` (`Id`)"
                    + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tratamientos' AUTO_INCREMENT=1 ;");

            NonQuery("ALTER TABLE gcobjetos ADD image varchar(255) default NULL");
            NonQuery("ALTER TABLE gcobjetos ADD updated_at  datetime default NULL");
            NonQuery("ALTER TABLE gcobjetos ADD State INT NOT NULL DEFAULT '0'");
            NonQuery("ALTER TABLE gcobjetos ADD Metrica INT NOT NULL DEFAULT '0'");
            NonQuery("ALTER TABLE gcobjetos ADD Dolar INT NOT NULL DEFAULT '0'");
            NonQuery("ALTER TABLE gcobjetos ADD Ideal INT NOT NULL DEFAULT '0'");
            NonQuery("ALTER TABLE gcpagos ADD m1000 INT NOT NULL DEFAULT '0'");
            NonQuery("ALTER TABLE gcpagos ADD m500 INT NOT NULL DEFAULT '0'");
            NonQuery("ALTER TABLE gcpagos ADD m200 INT NOT NULL DEFAULT '0'");


            NonQuery("ALTER TABLE gcmovimientos ADD factipo INT NOT NULL DEFAULT '0'");
            NonQuery("ALTER TABLE gcmovimientos ADD facnum INT NOT NULL DEFAULT '0'");
            NonQuery("ALTER TABLE `gcmovimientos` ADD `cuitCasual` VARCHAR( 100) NOT NULL DEFAULT '0', ADD `nombreCasual` VARCHAR( 200 ) NOT NULL , ADD `domicilioCasual` VARCHAR( 200 ) NOT NULL ;");

            NonQuery("ALTER TABLE `gcobjetos` CHANGE COLUMN `tipo` `Tipo` INT(11) NULL DEFAULT '-1' , CHANGE COLUMN `iva` `Iva` DECIMAL(4,2) NULL DEFAULT '21.00' ;");
            NonQuery("ALTER TABLE `gcmovimientos` CHANGE COLUMN `id` `Id` INT(11) NOT NULL AUTO_INCREMENT ,CHANGE COLUMN `caja` `Caja` INT(11) NULL DEFAULT NULL ,CHANGE COLUMN `tipo` `Tipo` INT(11) NULL DEFAULT NULL ;");
            NonQuery("ALTER TABLE `gcdestinatario` CHANGE COLUMN `id` `Id` INT(11) NOT NULL ;");
            NonQuery("ALTER TABLE `gcpagos` CHANGE COLUMN `id` `Id` INT(11) NOT NULL AUTO_INCREMENT ,CHANGE COLUMN `tipo` `Tipo` INT(11) NULL DEFAULT '0' ;");
            NonQuery("ALTER TABLE `gcobjetosmovidos` CHANGE COLUMN `cantidad` `Cantidad` DECIMAL(19,4) NULL DEFAULT NULL ;");
            

            Consulta("CREATE TABLE IF NOT EXISTS `gcbloqueo` ("
                      + "`Usuario` int(11) default '0',"
                      + "`Token` text character set utf8 ,"
                      + "`Tipo` int(11) default NULL,"
                      + "`Fecha` datetime default NULL,"
                      + "`Estado` int(11) default NULL"
                    + ");");
            return false;

        }
        private bool NonQuery(string t)
        {
            MySqlConnection cnn = new MySqlConnection("SERVER=" + ConexionServer + "; DATABASE=" + ConexionDbName + "; UID=" + ConexionUser + "; PASSWORD=" + ConexionPass + ";");
            try
            {
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand(t, cnn);
                cmd.ExecuteNonQuery();
                cnn.Close();
                return true;
            }
            catch (Exception)
            {
                cnn.Close();
            }
            return false;
        }
        public override Conexion Conectar()
        {
            MySqlConnection cnn = new MySqlConnection("SERVER=" + ConexionServer + "; DATABASE=" + ConexionDbName + "; UID=" + ConexionUser + "; PASSWORD=" + ConexionPass + ";");

            try
            {
                cnn.Open();
                cnn.Close();
                ConexionEstado = ConexionEstadoEnum.Conectado;
            }
            catch (Exception ex)
            {
                logger.Singlenton.addErrorMsg(ex.Message);
                ConexionEstado = ConexionEstadoEnum.Desconectado;
                cnn.Close();
            }

            return this;
        }


    }
}
