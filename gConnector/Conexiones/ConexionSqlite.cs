﻿using gLogger;
using System;
using System.Collections;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace gConnector
{
    public class ConexionSqlite : Conexion
    {
        public ConexionSqlite(string path)
        {
            ConexionTipo = ConexionTipoEnum.SqLite;
            ConexionPath = path;
            _consulta = new ConsultaSqlite();
            Conectar();
        }
        public string Path { get { return ConexionPath; } }
        private SQLiteConnection createConection()
        {
            SQLiteConnection sqlite = null;
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + Path + ";datetimeformat=CurrentCulture");
            }
            catch (Exception e)
            {
                gLogger.logger.Singlenton.addErrorMsg(e.Message);
                if (sqlite != null)
                {
                    sqlite.Close();
                }
            }
            return sqlite;

        }
        public override DataTable Consulta(string sql)
        {
            SQLiteDataAdapter ad;
            
            DataTable dt = new System.Data.DataTable();
            if (!DBExist) return dt;
            SQLiteConnection sqlite = createConection();
            if (sqlite == null) return dt;


            try
            {


                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = sql;  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
            }
            catch (SQLiteException ex)
            {
                logger.Singlenton.addErrorMsg("SQLite: " + ex.Message);
            }
            sqlite.Close();
            return dt;
        }
        public override int Insertar(string sql)
        {
            decimal i = 0;
            if (!System.IO.File.Exists(Path))
            {
                logger.Singlenton.addErrorMsg("No selecciono una base de datos sqLite.");
                return (int)i;
            }

            var sqlite = new SQLiteConnection("Data Source=" + Path + ";datetimeformat=CurrentCulture");
            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = sql;  //set the passed query
                cmd.ExecuteScalar();
                cmd.CommandText = "select last_insert_rowid();";
                i = decimal.Parse(cmd.ExecuteScalar().ToString());
            }
            catch (SQLiteException ex)
            {
                logger.Singlenton.addErrorMsg("SQLite: " + ex.Message);
            }
            sqlite.Close();

            return (int)i;
        }
        public bool DBExist
        {
            get
            {
                if (!System.IO.File.Exists(Path))
                {
                    logger.Singlenton.addErrorMsg("No selecciono una base de datos sqLite.");
                    return false; ;
                }
                return true;
            }
        }
        private bool checInterop()
        {
            int index = Properties.Settings.Default.interop;
            
            string interop = "SQLite.Interop.dll";
            string[] targets = new string[]
            {
                "35-32",
                "35-64",
                "40-32",
                "40-64",
                "45-32",
                "45-64"
            };
            string myfile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, interop);
            if (index < 0)
            {
                if (File.Exists(myfile))
                {
                    return true;
                }
                index = 0;
            } else if(index >= targets.Length)
            {
                index = 0;
            }
            
            if (File.Exists(myfile))
            {
                File.Delete(myfile);
            }
            string target = targets[index];

            

            Connector.Singlenton.downloadFile(interop, "", "interops/" + target + "/" + interop);

            if (createConection() != null)
            {
                Properties.Settings.Default.interop = -1;
                Properties.Settings.Default.Save();
                return true;
            }
            index++;
            Properties.Settings.Default.interop = index;
            Properties.Settings.Default.Save();
            System.Windows.Forms.MessageBox.Show("No funciono con el interop " + target + ". reinicie el sistema para intentar otra opción", "Error de compatibilidad de sqlite", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            return false;
            
        }
        internal override bool CreateDb()
        {
            if (!System.IO.File.Exists(Path))
            {
                SQLiteConnection.CreateFile(Path);
                string t = Properties.Resources.sqlite;
                Consulta(t);
                return true;
            }
            return false;
        }
        internal override bool RepararTablas()
        {
            if(!checInterop())
            {
                return true;
            }
            var dat = Consulta("PRAGMA table_info(gcTratamientos);");
            if(dat.Rows.Count==0)
                Consulta("CREATE TABLE IF NOT EXISTS gcTratamientos (\"Id\" INTEGER PRIMARY KEY, \"Usuario\" INT DEFAULT (0) ,\"Paciente\" INT DEFAULT (0)  ,\"Descripcion\" TEXT,\"Tipo\" INT,\"Parent\" INT,\"Fecha\" DATETIME,\"Proximo\" DATETIME,\"Estado\" INT,\"TipoCustom\" INT  )");
            else
            {
                bool prox = false;
                bool Tipocust = false;
                bool Titulo = false;
                foreach (DataRow row in dat.Rows)
                {
                    prox = row["name"].ToString().ToLower() == "proximo" || prox;
                    Tipocust = row["name"].ToString().ToLower() == "tipocustom" || Tipocust;
                    Titulo = row["name"].ToString().ToLower() == "titulo" || Titulo;

                }
                if (!prox)
                    Consulta("ALTER TABLE gcTratamientos ADD COLUMN Proximo DATETIME;");
                if (!Tipocust)
                    Consulta("ALTER TABLE gcTratamientos ADD COLUMN TipoCustom INT;");
                if (!Titulo)
                    Consulta("ALTER TABLE gcTratamientos ADD COLUMN Titulo TEXT;");
            }
            var dat2 = Consulta("PRAGMA table_info(gcobjetos);");
            bool state = false;
            bool metrica = false;
            bool dolar = false;
            bool ideal = false;
            bool image = false;
            bool updated_at = false;
            foreach (DataRow row in dat2.Rows)
            {
                state = row["name"].ToString().ToLower() == "state" || state;
                metrica = row["name"].ToString().ToLower() == "metrica" || metrica;
                dolar = row["name"].ToString().ToLower() == "dolar" || dolar;
                ideal = row["name"].ToString().ToLower() == "ideal" || ideal;
                image = row["name"].ToString().ToLower() == "image" || image;
                updated_at = row["name"].ToString().ToLower() == "updated_at" || updated_at;

            }
            if (!state)
                Consulta("ALTER TABLE gcobjetos ADD COLUMN State int default '0';");
            if (!metrica)
                Consulta("ALTER TABLE gcobjetos ADD COLUMN Metrica int default '0';");
            if (!ideal)
                Consulta("ALTER TABLE gcobjetos ADD COLUMN Ideal int default '0';");
            if (!dolar)
                Consulta("ALTER TABLE gcobjetos ADD COLUMN Dolar int default '0';");
            if (!image)
                Consulta("ALTER TABLE gcobjetos ADD COLUMN image TEXT;");
            if (!updated_at)
                Consulta("ALTER TABLE gcobjetos ADD COLUMN updated_at DATETIME;");

            var dat3 = Consulta("PRAGMA table_info(gcpagos);");
            bool mil = false;
            foreach (DataRow row in dat3.Rows)
            {
                mil = row["name"].ToString().ToLower() == "m1000" || mil;

            }
            if (!mil)
            {
                Consulta("ALTER TABLE gcpagos ADD COLUMN m1000 int default '0';");
                Consulta("ALTER TABLE gcpagos ADD COLUMN m500 int default '0';");
                Consulta("ALTER TABLE gcpagos ADD COLUMN m200 int default '0';");
            }

            var dat1 = Consulta("PRAGMA table_info(gcbloqueo);");
            if (dat1.Rows.Count == 0)
                Consulta("CREATE TABLE IF NOT EXISTS gcbloqueo (\"Usuario\" INT DEFAULT (0) ,\"Token\" TEXT,\"Tipo\" INT,\"Fecha\" DATETIME,\"Estado\" INT )");

            var mov = Consulta("PRAGMA table_info(gcmovimientos);");
            bool factipo = false;
            bool facnum = false;
            foreach (DataRow row in mov.Rows)
            {
                factipo = row["name"].ToString().ToLower() == "factipo" || factipo;
                facnum = row["name"].ToString().ToLower() == "facnum" || facnum;

            }
            if (!factipo)
                Consulta("ALTER TABLE gcmovimientos ADD COLUMN factipo int default '0';");
            if (!facnum)
                Consulta("ALTER TABLE gcmovimientos ADD COLUMN facnum int default '0';");

            return false;

        }
        public override Conexion Conectar()
        {
            if (System.IO.File.Exists(Path))
                ConexionEstado = ConexionEstadoEnum.Conectado;
            return this;
        }

    }
}
