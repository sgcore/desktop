﻿
using gConnector.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace gConnector
{
    public abstract class Conexion
    {
        protected Consulta _consulta = new ConsultaSqlite();
       public Conexion() { }

        public enum ConexionTipoEnum { SqLite, MySql, JSon, XML, SqlServer, Excel, SqlCE, MSAccess, WebService }
        public enum ConexionEstadoEnum { Desconectado, Conectado, Autenticado,Cargado }
        protected string ConexionPath { get; set; }
        protected string ConexionServer { get; set; }
        protected string ConexionUser { get; set; }
        protected string ConexionPass { get; set; }
        protected string ConexionDbName { get; set; }
        public ConexionTipoEnum ConexionTipo { get; set; }
        public ConexionEstadoEnum ConexionEstado { get; set; }
        public bool Conectado
        {
            get
            {
                return ConexionEstado == ConexionEstadoEnum.Conectado;
            }
        }
        public bool Autenticado
        {
            get
            {
                return ConexionEstado == ConexionEstadoEnum.Autenticado;
            }
        }
        public bool Desconectado
        {
            get
            {
                return ConexionEstado == ConexionEstadoEnum.Desconectado;
            }
        }
        public bool Cargado
        {
            get { return ConexionEstado == ConexionEstadoEnum.Cargado; }
        }
        public string Nombre
        {
            get{
                if (ConexionPath != null && ConexionPath != "")
                    return ConexionPath;
                if (ConexionDbName != null && ConexionDbName != "" && ConexionServer != null && ConexionServer != "")
                    return ConexionDbName+"@"+ConexionServer;
               
                return "Desconocida";

            }
        }
        public Consulta Consultas
        {
            get { return _consulta; }
        }
        public abstract System.Data.DataTable Consulta(string sql);
        public int UltimaCaja
        {
            get
            {
                DataTable dat= Consulta(Consultas.UltimaCaja);
                foreach (DataRow row in dat.Rows)
                {
                    return (int)row["caja"];
                }
                return 0;
            }
        }
        public abstract int Insertar(string sql);
        internal abstract bool CreateDb();
        internal abstract bool RepararTablas();
        public abstract Conexion Conectar();
        internal void SaveConfig()
        {
            Properties.Settings.Default.cnnBDName = ConexionDbName;
            Properties.Settings.Default.cnnBDServer = ConexionServer;
            Properties.Settings.Default.cnnBDUser = ConexionUser;
            Properties.Settings.Default.cnnBDPass = ConexionPass;
            Properties.Settings.Default.cnnBDPath = ConexionPath;
            Properties.Settings.Default.cnnBDTipo = (int)ConexionTipo;
            Properties.Settings.Default.Save();
        }
        /// <summary>
        /// Guarda en una tabla y si la insertó devuelve el numero de id
        /// sino intenta obtener el id, sino 0;
        /// </summary>
        /// <param name="campos"></param>
        /// <param name="tablename"></param>
        /// <param name="justupdate"></param>
        /// <returns></returns>
        public int GuardarENtity(Hashtable campos, string tablename, bool justupdate)
        {
            if(!justupdate)
            {
                if(campos.ContainsKey("id"))
                {
                    campos.Remove("id");
                }
                return Insertar(Consultas.InsertEntity(campos, tablename));
            }
            Consulta(Consultas.UpdateEntity(campos, tablename));
            if (campos.ContainsKey("id"))
                return (int)campos["id"];

            return 0;

        }
        public bool deleteEntity(int id, string tablename)
        {
            Consulta(Consultas.DeleteEntity(id, tablename));
            return true;
        }

        #region CONSULTAS
        public DataTable ConsultarMovimientos(string where = "")
        {

            return Consulta(Consultas.Movimientos + where);

        }
        public DataTable ConsultarProductos(string where = "")
        {
            return Consulta(Consultas.Productos + where);
        }
        public DataTable ConsultarDestinatarios(string where = "")
        {
            return Consulta(Consultas.Destinatarios + where);
        }
        public DataTable ConsultarObjetosMovidos(string where = "")
        {
            return Consulta(Consultas.ObjetosMovidos + where);
        }
        public DataTable ConsultarPagos(string where = "")
        {
            return Consulta(Consultas.Pagos + where);
        }
        public DataTable ConsultarUsuarios(string where = "")
        {
            return Consulta(Consultas.Usuarios + where);
        }
        public DataTable ConsultarTratamientos(string whuere = "")
        {
            return Consulta(Consultas.Tratamientos + whuere);
        }
        #endregion
        private DataRow performLogin(string user, string pass)
        {
            Regex rg = new Regex("^[a-zA-Z0-9]+$");
            if (!rg.IsMatch(user + pass) )
            {
                return null;
            }
            DataTable dat = Consulta(Consultas.Usuarios + " where usuario='" + user + "' and pass='" + pass + "';");
            foreach (DataRow row in dat.Rows)
            {
                ConexionEstado = ConexionEstadoEnum.Autenticado;
                return row;
            }
            return null;
        }
        private DataRow performloginAuto()
        {
            string usr = Settings.Default.sesionUsuario;
            if (!String.IsNullOrEmpty(usr))
            {
                string[] res = Crypto.Decrypt(usr).Split("@".ToCharArray());
                if(res.Length == 2)
                {
                    return performLogin(res[0], res[1]);
                }
            }
            return null;
            
        }
        public DataRow login()
        {
            if (Conectar().Conectado)
            {
                DataRow row = performloginAuto();
                if(row == null)
                {
                    var f = new Formularios.fIniciarSesion();
                    f.Text = "Iniciar sesión en origen de datos";
                    f.setSiEsInternet(false);
                    f.setDescripcion("Ustede está conectado a " + Nombre);
                    f.iniciar();
                    if (f.showAndAccept())
                    {

                        row = performLogin(f.Usuario, f.Pass);
                        if (row != null && Settings.Default.sesionRecordarme)
                        {
                            Settings.Default.sesionUsuario = Crypto.Encrypt(f.Usuario + "@" + f.Pass);
                            Settings.Default.Save();
                        }
                    }
                }
                return row;
                
            }
            return null;
        }
        public DataRow bloqueado()
        {
            DataTable dat = Consulta(Consultas.Bloqueo);
            foreach (DataRow row in dat.Rows)
            {
                return row;
            }

            return null;
        }
        public bool TengoPermisoBloqueo(int user, string token)
        {
            DataRow row = bloqueado();
            return row == null
                || (row != null
                    && DBNull.Value != row["Usuario"]
                    && DBNull.Value != row["Token"]
                    && (string)row["Token"] == token
                    && (int)row["Usuario"] == user);
           
        }
        public bool desbloquearCierre(int user, string token)
        {
            if(TengoPermisoBloqueo(user, token))
            {
                Consulta(Consultas.Desbloquear);
                return true;
            }

            return false;
        }
        public bool bloquearCierre(int user, string token)
        {
            if (TengoPermisoBloqueo(user, token))
            {
                Consulta(Consultas.Bloquear(1, user, token));
                return true;
            }

            return false;
        }
        public void logout()
        {
            ConexionEstado = ConexionEstadoEnum.Conectado;
           
        }
       
       

    }
}
