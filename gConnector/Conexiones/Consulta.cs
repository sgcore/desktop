﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gConnector
{
    public abstract class Consulta
    {

        public abstract string Movimientos { get; }
        public abstract string Usuarios { get; }
        public abstract string Productos { get; }
        public abstract string Destinatarios { get; }
        public abstract string Tratamientos { get; }
        public abstract string ObjetosMovidos { get; }
        public abstract string Pagos { get; }
        public abstract string UltimaCaja { get; }
        public abstract string Bloqueo { get; }
        public abstract string Bloquear(int tipo, int user, string token);
        public abstract string Desbloquear { get; }
        public abstract string InsertEntity(Hashtable campos, string tablename);
        public abstract string UpdateEntity(Hashtable campos, string tablename);
        public abstract string DeleteEntity(int id, string tablename);

        protected string fechaparaSQL(DateTime d)
        {
            string t = "'";
            t += d.Year.ToString();
            t += "-";
            t += d.Month.ToString();
            t += "-";
            t += d.Day.ToString();
            t += " ";
            t += d.Hour.ToString();
            t += ":";
            t += d.Minute.ToString();
            t += ":";
            t += d.Second.ToString();
            t += "'";
            return t;
        }

    }
}
