﻿using System;
using System.Windows.Forms;

namespace gLogger
{
    public class gcMessage
    {

        private string _title = "";
        public enum MessageType { ERROR, WARNING, INFO, SYSTEM, DEBUG,SECURITY,CUSTOM }
        Formularios.fAviso _myform;
       
        public gcMessage(string msg)
        {
            Fecha = DateTime.Now;
            Tipo = MessageType.INFO;
            Mensaje = msg;

        }
        public DateTime Fecha { get; set; }
        System.Drawing.Image _img;

        public System.Drawing.Image CustomImage {
            get
            {
                if(_img == null)
                {
                    switch (Tipo)
                    {
                        case MessageType.ERROR:
                            _img = Properties.Resources.Error;
                            break;
                        case MessageType.SECURITY:
                            _img = Properties.Resources.Seguridad;
                            break;
                        case MessageType.WARNING:
                            _img = Properties.Resources.Warning;
                            break;
                        case MessageType.SYSTEM:
                            _img = Properties.Resources.Sistema;
                            break;
                        case MessageType.INFO:
                            _img = Properties.Resources.Info;
                            break;
                        case MessageType.DEBUG:
                            _img = Properties.Resources.Debug;
                            break;

                        default:
                            _img = Properties.Resources.Mensaje;
                            break;
                    }
                }
                return _img;
            }
            set
            {
                _img = value;
            }
        }
        public MessageType Tipo { get; set; }
        public string Titulo
        {
            get
            {
                if (_title == "") return Tipo.ToString();
                return _title;
            }
            set { _title = value; }
        }
        public string Mensaje { get; set; }
        public override string ToString()
        {
            return Titulo + ": " + Mensaje;
        }
        public string FechaToString()
        {
            DateTime now = DateTime.Now;
            TimeSpan p = now.Subtract(Fecha);
            if (p.TotalMinutes < 1) return "Hace menos de un minuto";
            else if (p.TotalMinutes < 60) return "Hace " + p.Minutes + " minutos";
            else if (p.TotalHours < 5) return "Hace " + p.Hours + " horas";
            else if (p.TotalDays < now.TimeOfDay.TotalDays) return "Hoy a las " + Fecha.ToShortTimeString();
            else if (p.TotalDays < now.TimeOfDay.TotalDays + 1) return "Ayer a las " + Fecha.ToShortTimeString();
            else if (p.TotalDays < now.TimeOfDay.TotalDays + 6) return "El " + Fecha.ToString("dddd") + " a las " + Fecha.ToShortTimeString();
            else return Fecha.ToLongDateString() + " a las " + Fecha.ToShortTimeString();
        }
        public void MessageBoxShow()
        {

           /* MessageBoxIcon icon = MessageBoxIcon.Information;
            switch (Tipo)
            {
                case MessageType.ERROR:
                    icon = System.Windows.Forms.MessageBoxIcon.Error;
                    break;
                case MessageType.WARNING:
                    icon = System.Windows.Forms.MessageBoxIcon.Warning;
                    break;
                case MessageType.SYSTEM:
                    icon = System.Windows.Forms.MessageBoxIcon.Asterisk;
                    break;
            }
           // System.Windows.Forms.MessageBox.Show(Mensaje, Titulo, System.Windows.Forms.MessageBoxButtons.OK, icon);*/
            if (_myform == null)
            {
                _myform = new Formularios.fAviso();
                _myform.setAviso(this);
                _myform.FormClosing += (o, e) =>
                {
                    _myform.Hide();
                    logger.Singlenton.AvisosMostrados--;
                    e.Cancel = true;
                };
            }
            logger.Singlenton.AvisosMostrados++;
            setNextPoss();
            _myform.StartPosition = FormStartPosition.Manual;
            _myform.Show(); 
            _myform.BringToFront();
        }
        private void setNextPoss()
        {
            int screenHeight = Screen.PrimaryScreen.WorkingArea.Height - _myform.Height - (_myform.Height * logger.Singlenton.AvisosMostrados);
            if (screenHeight < 0) screenHeight = Screen.PrimaryScreen.WorkingArea.Height - _myform.Height;
            int screenWidth = Screen.PrimaryScreen.WorkingArea.Width - _myform.Width;
            _myform.SetDesktopLocation(screenWidth,screenHeight);
           
        }
        public bool ShowMessage { get; set; }
       

    }
}
