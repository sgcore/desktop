﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gLogger.Formularios
{
    public partial class fConfiguracion : Form
    {
        public fConfiguracion()
        {
            InitializeComponent();
        }

        private void btAplicar_Click(object sender, EventArgs e)
        {
            logger.Singlenton.ShowMsgBox = Properties.Settings.Default.logShowMsgBox;
            logger.Singlenton.Enabled = Properties.Settings.Default.logActivo;
            Properties.Settings.Default.Save();
            Close();
        }
    }
}
