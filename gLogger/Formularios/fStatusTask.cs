﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gLogger.Formularios
{
    public partial class fStatusTask : Form
    {
        public fStatusTask()
        {
            InitializeComponent();
            Icon = Icon.FromHandle(Properties.Resources.Configuracion.GetHicon());
            Habilitado = true;
            CheckForIllegalCrossThreadCalls = false;
        }
        public string Titulo
        {
            get
            {
                return Text;
            }
            set
            {
                Text = value;
            }
        }
        public bool Habilitado
        {
            get;
            set;
        }
        public string Descripcion
        {
            get
            {
                return lblDesc.Text;
            }
            set
            {
                lblDesc.Text = value;
            }
        }
        public void setStatusBar(int p)
        {
            if (p > 100) p = 100;
            else if (p < 0) p = 0;
            lblPorciento.Text = p + " %";
            progressBar1.Value = p;
            if (!Visible) Show();
        }
        public bool Marquesina
        {
            get
            {
                return progressBar1.Style == ProgressBarStyle.Marquee;

            }
            set
            {
                if (value)
                {
                    progressBar1.Style = ProgressBarStyle.Marquee;
                    progressBar1.MarqueeAnimationSpeed = 30;
                }
                else
                {
                    progressBar1.Style = ProgressBarStyle.Continuous;
                    progressBar1.MarqueeAnimationSpeed = 0;
                }
            }
        }
       
       
       
    }
}
