﻿namespace gLogger.Formularios
{
    partial class fConfiguracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btAplicar = new System.Windows.Forms.Button();
            this.chkEnabled = new System.Windows.Forms.CheckBox();
            this.chkMostrarMesageBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btAplicar
            // 
            this.btAplicar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btAplicar.Location = new System.Drawing.Point(0, 132);
            this.btAplicar.Name = "btAplicar";
            this.btAplicar.Size = new System.Drawing.Size(439, 22);
            this.btAplicar.TabIndex = 5;
            this.btAplicar.Text = "Aplicar Cambios";
            this.btAplicar.UseVisualStyleBackColor = true;
            this.btAplicar.Click += new System.EventHandler(this.btAplicar_Click);
            // 
            // chkEnabled
            // 
            this.chkEnabled.AutoSize = true;
            this.chkEnabled.Checked = global::gLogger.Properties.Settings.Default.logActivo;
            this.chkEnabled.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::gLogger.Properties.Settings.Default, "logActivo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkEnabled.Location = new System.Drawing.Point(16, 8);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(177, 17);
            this.chkEnabled.TabIndex = 4;
            this.chkEnabled.Text = "Activar los mensajes del sistema";
            this.chkEnabled.UseVisualStyleBackColor = true;
            // 
            // chkMostrarMesageBox
            // 
            this.chkMostrarMesageBox.AutoSize = true;
            this.chkMostrarMesageBox.Checked = global::gLogger.Properties.Settings.Default.logShowMsgBox;
            this.chkMostrarMesageBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::gLogger.Properties.Settings.Default, "logShowMsgBox", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkMostrarMesageBox.DataBindings.Add(new System.Windows.Forms.Binding("Enabled", global::gLogger.Properties.Settings.Default, "logActivo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkMostrarMesageBox.Enabled = global::gLogger.Properties.Settings.Default.logActivo;
            this.chkMostrarMesageBox.Location = new System.Drawing.Point(16, 31);
            this.chkMostrarMesageBox.Name = "chkMostrarMesageBox";
            this.chkMostrarMesageBox.Size = new System.Drawing.Size(410, 17);
            this.chkMostrarMesageBox.TabIndex = 3;
            this.chkMostrarMesageBox.Text = "Mostrar un aviso cuando el sisema genere un mensaje de error o una advertencia";
            this.chkMostrarMesageBox.UseVisualStyleBackColor = true;
            // 
            // fConfiguracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 154);
            this.Controls.Add(this.btAplicar);
            this.Controls.Add(this.chkEnabled);
            this.Controls.Add(this.chkMostrarMesageBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "fConfiguracion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuración de mensajes del Sistema";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAplicar;
        private System.Windows.Forms.CheckBox chkEnabled;
        private System.Windows.Forms.CheckBox chkMostrarMesageBox;

    }
}