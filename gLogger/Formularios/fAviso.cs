﻿using System.Windows.Forms;

namespace gLogger.Formularios
{
    public partial class fAviso : Form
    {
        public fAviso()
        {
            InitializeComponent();
        }
        public void setAviso(gcMessage m)
        {
            Text = m.Titulo;
            lblDesc.Text = m.Mensaje;
            if (m.CustomImage != null)
                lblDesc.Image = m.CustomImage;
            else
            {
                switch (m.Tipo)
                {
                    case gLogger.gcMessage.MessageType.ERROR:
                         lblDesc.Image =Properties.Resources.Error;
                         BackgroundImage = Properties.Resources.FondoRojo;
                        break;
                    case gLogger.gcMessage.MessageType.WARNING:
                        lblDesc.Image =Properties.Resources.Warning;
                         BackgroundImage = Properties.Resources.FondoAmarillo;
                        break;
                    case gLogger.gcMessage.MessageType.SYSTEM:
                         lblDesc.Image =Properties.Resources.Sistema;
                         BackgroundImage = Properties.Resources.FondoVerde;
                        break;
                    case gLogger.gcMessage.MessageType.DEBUG:
                        lblDesc.Image = Properties.Resources.Debug;
                        BackgroundImage = Properties.Resources.FondoGris;
                        break;
                    case gLogger.gcMessage.MessageType.SECURITY:
                         lblDesc.Image = Properties.Resources.Seguridad;
                        BackgroundImage = Properties.Resources.FondoRojo;
                        break;
                    case gLogger.gcMessage.MessageType.INFO:
                         lblDesc.Image = Properties.Resources.Info;
                        BackgroundImage = Properties.Resources.FondoAzul ;
                        break;
                    case gLogger.gcMessage.MessageType.CUSTOM:
                        lblDesc.Image = Properties.Resources.Mensaje;
                        BackgroundImage = Properties.Resources.FondoVerde;
                        break;
                }
            }
           
           
        }
        public int Posicion { get; set; }
    }
}
