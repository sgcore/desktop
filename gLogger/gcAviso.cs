﻿using System;

namespace gLogger
{
    public class gcAviso:gcMessage
    {
        public gcAviso(string titulo,string descripcion,int id)
            : base(titulo)
        {
            Titulo = titulo;
            Id = id;

        }
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        
        public int Interval
        {
            get{
                return Math.Abs(End.Subtract(Start).Milliseconds);
            }
            
            
           
        }
    }
}
