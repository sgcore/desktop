﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gLogger
{
    public class TaskManager
    {
        private static TaskManager _instance;
        private System.ComponentModel.BackgroundWorker _worker = new System.ComponentModel.BackgroundWorker();
        private Queue<FutureTaskObject> _cola = new Queue<FutureTaskObject>();
        public delegate void FutureTask(System.ComponentModel.BackgroundWorker worker);
        public delegate void ProgressDelegate(int percent, string msg);
        private TaskManager() 
        {
            _worker.DoWork += _worker_DoWork;
            _worker.ProgressChanged += _worker_ProgressChanged;
            _worker.RunWorkerCompleted += _worker_RunWorkerCompleted;
            _worker.WorkerReportsProgress = true;
            _worker.WorkerSupportsCancellation = true;
            
        }

        public static void CrearTareaAislada(FutureTask metodo,System.ComponentModel.RunWorkerCompletedEventHandler finaltask=null, ProgressDelegate progress = null, string desc="Trabajando..." )
        {
            System.ComponentModel.BackgroundWorker _w = new System.ComponentModel.BackgroundWorker();
           _w.WorkerReportsProgress = progress != null;
            if (_w.WorkerReportsProgress)
            {
                _w.ProgressChanged += delegate (object sender, System.ComponentModel.ProgressChangedEventArgs e)
                {
                    progress?.Invoke(e.ProgressPercentage, desc);
                };
            }
            
            /*
            if (showProgress)
            {
                pb = new Formularios.fStatusTask();
                pb.Descripcion = desc;
            }
            */
            
            _w.DoWork += delegate(object sender, System.ComponentModel.DoWorkEventArgs e)
            {
                metodo(_w);
            };
             /*_w.ProgressChanged += delegate(object sender, System.ComponentModel.ProgressChangedEventArgs e)
            {
                
               if (pb != null)
                    pb.setStatusBar(e.ProgressPercentage);
            };*/
          
           _w.RunWorkerCompleted += delegate(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
                    
                finaltask?.Invoke(sender, e);

                _w.Dispose();
            };
            

             _w.RunWorkerAsync();
                
            
           
        }

        void _worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (_currFinaltask != null) _currFinaltask(sender,e);
            if (_pbf != null)
            {
                ProgressBar.Close();
                _pbf = null;
            
            }
            RunNextTask();
            
        }

        void _worker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
           if(_pbf!=null)
                ProgressBar.setStatusBar(e.ProgressPercentage);
        }
        private System.ComponentModel.RunWorkerCompletedEventHandler  _currFinaltask;
        void _worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (_cola.Count > 0)
            {
                FutureTaskObject t = _cola.Dequeue();
                 ProgressBar.Titulo = t.Titulo;
                    ProgressBar.Descripcion = t.Descripcion;
                   
                    if (Marquesina)
                    {
                        ProgressBar.Marquesina = true;
                    }
               
                _currFinaltask = t.FinalTask;
                
                 t.Metodo(_worker);
               
                
            }
        }
        public bool Marquesina
        {
            get
            {
                return ProgressBar.Marquesina;
            }
            set
            {
                ProgressBar.Marquesina = value;
            }
        }
        public System.ComponentModel.BackgroundWorker Worker
        {
            get
            {
                return _worker;
            }
        }
        public static TaskManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new TaskManager();
                return _instance;
            }
        }
        public void addTask(FutureTask metodo,string descripcion,string titulo="Estado de la tarea",System.ComponentModel.RunWorkerCompletedEventHandler final=null,bool showprogress=true )
        {
            FutureTaskObject t = new FutureTaskObject();
            t.Descripcion = descripcion;
            t.Metodo = metodo;
            t.FinalTask = final;
            t.Titulo = titulo;
            
            _cola.Enqueue(t);
            RunNextTask();
        }
        public void RunNextTask()
        {
            if (_worker.IsBusy || _cola.Count==0) return;
            _worker.RunWorkerAsync();
            
        }

        //form
        private Formularios.fStatusTask  _pbf;
        public Formularios.fStatusTask ProgressBar
        {
            get
            {
                if (_pbf == null)
                {
                    _pbf = new Formularios.fStatusTask();

                }
                return _pbf;
            }
        }
        private class FutureTaskObject
        {
            public FutureTask Metodo { get; set; }
            public System.ComponentModel.RunWorkerCompletedEventHandler FinalTask { get; set; }
            public string Descripcion { get; set; }
            public string Titulo { get;set;}
           
        }

    }
}
