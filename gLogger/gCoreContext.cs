﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace gLogger
{
    public class gCoreContext:ApplicationContext
    {
        private NotifyIcon TrayIcon;
        private ContextMenuStrip TrayIconContextMenu;
        private ToolStripMenuItem CloseMenuItem;
        private ToolStripMenuItem ReseteMenuItem;
        private ToolStripMenuItem LogoutMenuItem;
        public event EventHandler Reiniciando;
        public event EventHandler CerrandoSesion;

        Form _form;
     
        public gCoreContext(Form f)
        {
            Application.ApplicationExit += new EventHandler(this.OnApplicationExit);
            InitializeComponent();
            TrayIcon.Visible = true;
            _form = f;
            Icon = f.Icon;
            if (_form == null) return;
            _form.FormClosed += (o, e) => {
                Application.Exit(); ExitThread();
            };
            _form.Show();
            gLogger.logger.Singlenton.gcMessageAdded += (m) =>
            {
                if (m is gcAviso)
                {
                    InfoMessage(m.Titulo, m.Mensaje);
                    return;
                }

                if (m.ShowMessage)
                {
                    switch (m.Tipo)
                    {
                        case gcMessage.MessageType.CUSTOM:
                            InfoMessage(m.Titulo,m.Mensaje);
                            break;
                        case gcMessage.MessageType.WARNING:
                           // WarningMessage(m.Mensaje);
                            break;
                        case gcMessage.MessageType.ERROR:
                            // WarningMessage(m.Mensaje);
                            break;
                        default:
                            //InfoMessage(m.Titulo, m.Mensaje);
                            break;
                    }
                   
                }
            };
            
        }
       
        System.Drawing.Icon Icon
        {
            get { return TrayIcon.Icon; }
            set { TrayIcon.Icon = value; }
        }
        private void InitializeComponent()
        {
            TrayIcon = new NotifyIcon();

           
            TrayIcon.Text = "Sistema de Gestión Comercial";
           

            //The icon is added to the project resources.
            //Here I assume that the name of the file is 'TrayIcon.ico'
           

            //Optional - handle doubleclicks on the icon:
            TrayIcon.DoubleClick += TrayIcon_DoubleClick;

            //Optional - Add a context menu to the TrayIcon:
            TrayIconContextMenu = new ContextMenuStrip();
            CloseMenuItem = new ToolStripMenuItem();
            ReseteMenuItem = new ToolStripMenuItem();
            LogoutMenuItem = new ToolStripMenuItem();
            TrayIconContextMenu.SuspendLayout();

            // 
            // TrayIconContextMenu
            // 
            this.TrayIconContextMenu.Items.AddRange(new ToolStripItem[] {
            this.CloseMenuItem,this.ReseteMenuItem, this.LogoutMenuItem});
            this.TrayIconContextMenu.Name = "TrayIconContextMenu";
            this.TrayIconContextMenu.Size = new Size(153, 70);
            // 
            // CloseMenuItem
            // 
            this.CloseMenuItem.Name = "CloseMenuItem";
            this.CloseMenuItem.Size = new Size(152, 22);
            this.CloseMenuItem.Text = "Salir";
            this.CloseMenuItem.Click += new EventHandler(this.CloseMenuItem_Click);
            // 
            // ResetMenuItem
            // 
            this.ReseteMenuItem.Name = "ReseteMenuItem";
            this.ReseteMenuItem.Size = new Size(152, 22);
            this.ReseteMenuItem.Text = "Reiniciar";
            this.ReseteMenuItem.Click +=ReseteMenuItem_Click;
            // 
            // LogoutMenuItem
            // 
            this.LogoutMenuItem.Name = "LogoutMenuItem";
            this.LogoutMenuItem.Size = new Size(152, 22);
            this.LogoutMenuItem.Text = "Cerrar Sesión";
            this.LogoutMenuItem.Click += LogoutMenuItem_Click;

            TrayIconContextMenu.ResumeLayout(false);
            TrayIcon.ContextMenuStrip = TrayIconContextMenu;
        }

        private void LogoutMenuItem_Click(object sender, EventArgs e)
        {
            CerrandoSesion?.Invoke(null, null);
        }

        public void Reiniciar()
        {
            Reiniciando?.Invoke(null, null);



        }
        public void InfoMessage(string titulo, string desc)
        {
            TrayIcon.BalloonTipIcon = ToolTipIcon.Info;
            TrayIcon.BalloonTipText =
              desc;
            TrayIcon.BalloonTipTitle = titulo;
            TrayIcon.ShowBalloonTip(10000);
        }
        public void ErrorMessage( string desc)
        {
            TrayIcon.BalloonTipIcon = ToolTipIcon.Error;
            TrayIcon.BalloonTipText =
              desc;
            TrayIcon.BalloonTipTitle = "Se produjo un error";
            TrayIcon.ShowBalloonTip(10000);
        }
        
        public void WarningMessage(string desc)
        {
            TrayIcon.BalloonTipIcon = ToolTipIcon.Warning;
            TrayIcon.BalloonTipText =
              desc;
            TrayIcon.BalloonTipTitle = "¡Advertencia!";
            TrayIcon.ShowBalloonTip(10000);
        }
       

        private void OnApplicationExit(object sender, EventArgs e)
        {
            //Cleanup so that the icon will be removed when the application is closed
            TrayIcon.Visible = false;
        }

        private void TrayIcon_DoubleClick(object sender, EventArgs e)
        {
            //Here you can do stuff if the tray icon is doubleclicked
            InfoMessage("Sistema de Gestión Comercial", "Bienvenido");
        }

        private void CloseMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente quiere salir?",
                    "Salir del programa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                Application.Exit();
                ExitThread();
            }
        }
        private void ReseteMenuItem_Click(object sender, EventArgs e)
        {
            Reiniciar();
        }
        
    }
}
