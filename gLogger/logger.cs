﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gLogger
{
    public class logger
    {
        public event gcMessageDel gcMessageAdded;
        BlockingCollection<gcMessage> dataItems = new BlockingCollection<gcMessage>();
        public delegate void gcMessageDel(gcMessage msg);
        private static logger _instance;
        
      



        private logger()
        {
            
            Enabled = Properties.Settings.Default.logActivo;
            ShowMsgBox = Properties.Settings.Default.logShowMsgBox;

            
           
        }
        public int AvisosMostrados { get;set;}
        #region trayicon
      
      
        #endregion

        public bool Enabled { get; set; }
        public bool showDebug { get { return System.Diagnostics.Debugger.IsAttached; } }
        public bool ShowMsgBox { get; set; }
        public static logger Singlenton
        {
            get
            {
                if (_instance == null) _instance = new logger();
                return _instance;
            }
        }
        public void ShowConfig()
        {
            var f = new Formularios.fConfiguracion();
            f.ShowDialog();
        }
        public List<gcMessage> Mensajes => dataItems.ToList();
        public void clearMessages()
        {
            while (dataItems.Count > 0)
            {
                var obj = dataItems.Take();
            }
        }
        public gcMessage addMessage(gcMessage m)
        {
            if (m == null) return null;
            if (!Enabled) return m;
            dataItems.Add(m);
            var x = new Task(() =>
            gcMessageAdded?.Invoke(m)
            );
            x.Start();
            /*
            TaskManager.CrearTareaAislada((w) =>
            {
                //if( addAviso(m as gcAviso) || m is gcAviso &&)return m;
                //if (ShowMsgBox && (m.ShowMessage || m is gcAviso)) m.MessageBoxShow();


                //dispara el evento
                gcMessageAdded?.Invoke(m);
            });
           */

            return m;
        }

        public gcMessage addMessage(string msg, string title, gcMessage.MessageType tipo)
        {
            gcMessage m = new gcMessage(msg);
            m.Tipo = tipo;
            m.Titulo = title;
            return addMessage(m);

        }
        public gcMessage addCustomMessage(System.Drawing.Image img, string title, string msg){

            gcMessage m = new gcMessage(msg);
            m.Tipo = gcMessage.MessageType.CUSTOM;
            m.Titulo = title;
            m.CustomImage = img;
            return addMessage(m);

        }
        public gcMessage addMessage(string msg, string title)
        {
            gcMessage m = new gcMessage(msg);
            m.Titulo = title;
            return addMessage(m);

        }
        public gcMessage addMessage(string msg)
        {
            gcMessage m = new gcMessage(msg);
            return addMessage(m);

        }

        public gcMessage addErrorMsg(string msg)
        {
            gcMessage m = new gcMessage(msg);
            m.Tipo = gcMessage.MessageType.ERROR;
            m.ShowMessage = true;
            return addMessage(m);
           
        }
        public gcMessage addWarningMsg(string msg)
        {
            gcMessage m = new gcMessage(msg);
            m.Tipo = gcMessage.MessageType.WARNING;
            m.Titulo = "ADVERTENCIA";
            m.ShowMessage = true;
            return addMessage(m);
        }
        public void addDebugMsg(string title, string msg)
        {
            if (!showDebug) return;
            gcMessage m = new gcMessage(msg);
            m.Tipo = gcMessage.MessageType.DEBUG;
            m.Titulo = title;
            m.ShowMessage = false;
            addMessage(m);
        }
        public gcMessage addInfoMsg(string msg)
        {
            return addMessage(msg);
        }
        
       
    }
    
}
