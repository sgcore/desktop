﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gLogger
{
    public class AvisoManager
    {
        static AvisoManager _instance;
        Dictionary<int, gcAviso> _pendientes = new Dictionary<int, gcAviso>();
        Dictionary<int, gcAviso> _avisados = new Dictionary<int, gcAviso>();
        public event logger.gcMessageDel SiguienteAviso;
        public event logger.gcMessageDel AvisoAgregado;
     
      
      

        private AvisoManager() { }
       
       
       

       


        public static AvisoManager Singlenton
        {
            get
            {
                if (_instance == null) _instance = new AvisoManager();
                return _instance;
            }
        }
        /// <summary>
        /// Si esta caducado lo agrega a la lista de avisados y lo elimna de la lista de pendientes.
        /// si esta pendiente lo agrega a la lista de pendiente y lo elimina de la lista de avisados
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public bool addAviso(gcAviso m)
        {

            if (m == null) return false;
            if (m.Interval > 0)
            {
                addPendiente(m);
                removeAvisados(m.Id);
            }
            else
            {
                addAvisado(m);
                removePendiente(m.Id);
            }
            if (AvisoAgregado != null) AvisoAgregado(m);
            

            nextAviso();
            
            return true;

        }
        private void addPendiente(gcAviso a)
        {
            if (!_pendientes.ContainsKey(a.Id))
                _pendientes.Add(a.Id, a);
            else
            {
                _pendientes[a.Id] = a;
            }
        }
        private bool removePendiente(int id)
        {
            if (_pendientes.ContainsKey(id)) { _pendientes.Remove(id); return true; }
            return false;
        }
        private void addAvisado(gcAviso a)
        {
            if (!_avisados.ContainsKey(a.Id))
                _avisados.Add(a.Id, a);
            else
            {
                _avisados[a.Id] = a;
            }
        }
        private bool removeAvisados(int id)
        {
            if (_avisados.ContainsKey(id))
            { _avisados.Remove(id); return true; }
            return false;
        }
       
       
        public void removeAviso(int id)
        {
            if (removePendiente(id))
                nextAviso();
            else
                removeAvisados(id);
                   
        }
        public void nextAviso()
        {
            
               //limpio pendientes
                var caduco = _pendientes.Values.Where(x => x.Interval <= 0).ToList();
           
            
                foreach (var cc in caduco)
                {
                    removePendiente(cc.Id);

                }
                if (SiguienteAviso != null)
                {
                if (_pendientes.Count > 0) SiguienteAviso(Pendientes.First());
            }
       
          
           

        }
        
        public gcAviso AvisoProximo { get; set; }
        public List<gcAviso> Avisados
        {
            get
            {
                return _avisados.Values.OrderBy(x => x.Interval).ToList<gcAviso>();
            }
        }
        public List<gcAviso> Pendientes { get { return _pendientes.Values.OrderBy(x => x.Interval).ToList<gcAviso>(); } }
       



    }
}
